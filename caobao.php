<?php
// 应用入口文件

// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',true);
define('BIND_MODULE', 'Caobao');
// 静态文件生成目录 
define('HTML_PATH', './Html/');
define('DB_FIELD_CACHE',false);//关闭缓存
define('HTML_CACHE_ON',false);//关闭缓存
// 定义应用目录
define('APP_PATH','./App/');

// 引入ThinkPHP入口文件
require './Include/ThinkPHP.php';