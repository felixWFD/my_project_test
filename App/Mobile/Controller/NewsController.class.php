<?php
namespace Mobile\Controller;
/**
 * 新闻控制器
 */
class NewsController extends BaseController{
	
    /**
	 * 新闻列表
	 * 
	 */
    public function index(){
		$news   = D('News');
		
        // 搜索条件
		$catId    = I('get.cat');

		// 分类列表
		$catRes = $news->getNewsCat();
		$newsRes = $news->getNewsList(array('catId'=>$catId, 'm'=>0, 'n'=>8));

		$this->assign('cat', $catId);
        $this->assign('newsRes', $newsRes);
		$this->assign('catRes', $catRes);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('news');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
        $this->display();
    }
	
	/**
	 * 加载更多新闻
	 * 
	 */
	public function loadMoreNews(){
		$catId   = I('post.cat');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$news   = D('News');
		$newsRes = $news->getNewsList(array('catId'=>$catId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($newsRes as $key=>$vo){
			$newsRes[$key]['description'] = msubstr($vo['description'],0,30,'utf-8',false);
			$newsRes[$key]['dateline'] = date('m-d',$vo['create_time']);
		}
		echo json_encode($newsRes);
	}
	
	/**
	 * 新闻详细
	 * 
	 */
    public function detail(){
		$id = I('get.id');
		
		$news   = D('News');
		
		// 增加点击数
		$news->statistics($id);
		
		// 视频信息
		$data = $news->getNews($id);
		$data['content'] = CBWKeywordAddLink(htmlspecialchars_decode($data['content'])); // 关键词加链接
		$data['content'] = CBWImageUpdateAttribute($data['title'], $data['content']);    // 更新图片属性 
		
		// 把标识cbwurl替换为"预约维修"按钮
		if(!empty($data['link'])){
			$botton = '<span class="bespoke"><a href="'.$data['link'].'" target="_blank">预约维修</a></span>';
			$data['content'] = preg_replace('/{cbwurl}/sui', $botton , $data['content'], 3); 
		}
		
		$this->assign('news',$data);
		
		// 同分类下其他视频
		$newsRes = $news->getNewsByCat($data['id'], $data['cat_id']);
		$this->assign('newsRes',$newsRes);
		
		// SEO
		$this->assign('title', $data['title']);
		$this->assign('keywords', $data['title']);
		$this->assign('description', $data['description']);
		
        $this->display();
    }

}