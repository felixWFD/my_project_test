<?php
namespace Mobile\Controller;
/**
 * 维修系统控制器
 */
class SystemController extends BaseController{
	
	public function __construct(){   
		parent::__construct();
		
		$this->isUserLogin();
    }
	
    /**
	 * 获取首页信息
	 */
    public function index(){
		$orders = D('Orders');
		$us     = D('UserShop');
		
		$shopId = session('shop.id');
		
		if(empty($shopId)){
			$this->redirect("Users/logout");    //页面跳转
		}
		
		// 未读可以报价数量
		$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));
		
		$shop = $us->getShopByShopId($shopId);
		
		$level = CBWShopLevel($shop['exp']); // 等级换算
		$shop['level_image_name'] = $level['name'];
		$shop['level_image_num']  = $level['num'];
		$shop['level_level']      = $level['level'];
		$shop['integrity'] = $this->integrity();          // 店铺完善度
		
		$notices = $us->getShopNoticeCountByObject(array('shopId'=>$shopId, 'status'=>'0'));
		
		$this->assign('offerCount', $offerCount);
		$this->assign('notices', $notices);
		$this->assign('shop', $shop);
		$this->display();
    }
	
	// 店铺完善度
	private function integrity(){
		$shopId = session('shop.id');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		if($shop['is_main'] < 1){
			$shop = $us->getMainShopByUserId($shop['user_id']);
		}
		$inte = 10;
		if(!empty($shop['logo_image'])){
			$inte +=10;
		}
		if(!empty($shop['description'])){
			$inte +=10;
		}
		if(!empty($shop['content'])){
			$inte +=10;
		}
		if(!empty($shop['qq'])){
			$inte +=10;
		}
		if(!empty($shop['open_hours'])){
			$inte +=10;
		}
		if(!empty($shop['server_area'])){
			$inte +=10;
		}
		$model = $us->getUserModelByUserId($shop['user_id']);
		if(count($model) > 0){
			$inte +=30;
		}
		return $inte;
	}
	
	/**
	 * 店铺信息
	 */
    public function info(){
		$shopId = session('shop.id');
		
		$users = D('Users');
		$us    = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		$hours = explode(",", $shop['open_hours']);
		$shop['open_hours'] = $hours[0].':00-'.$hours[1].':00  '.(($shop['is_week']==1)?'周末营业':'周末休息');
		
		$user = $users->getUserById($shop['user_id']);

		$this->assign('shop', $shop);
		$this->assign('user', $user);
		$this->display();
    }
	
    /**
     * 跳到修改店铺形象
     */
	public function image(){
		$shopId = session('shop.id');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		if(!empty($shop['mod_image'])){
			$this->error('正在审核,请不要重复提交!');
		}
		$this->assign('shop', $shop);
		$this->display();
	}
	
	// 裁剪图片
	public function crop(){
		$crop = new \Think\CropAvatar(
		  './Public/Temp/',
		  './Public/User/',
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
		);
		$result  = $crop -> getResult();
		$message = $crop -> getMsg();
		if(!empty($result)){
			$us = D('UserShop');
			$shopId = session('shop.id');
			$shop = $us->getShopByShopId($shopId);
			$rt = $us->updateUserShop($shopId, array('mod_image'=>$result));
			if($rt['status'] > 0){
				unlink("./Public/User/".$shop['mod_image']);                   // 删除旧头像
			}
		}

		echo json_encode(array('state'  => 200, 'message' => $message, 'result' => $result));
	}
	
	/**
     * 修改店铺资料
     */
	public function edit(){
		$us = D('UserShop');
		$shop = $us->getShopByShopId();
		$shop['open_hours'] = explode(',', $shop['open_hours']);
		$this->assign("shop", $shop);
		$this->display();
	}
	
	/**
     * 更新店铺资料
     */
	public function updateinfo(){
		$shopId = session('shop.id');
		
		$hours_start   = I('post.hours_start');
		$hours_start   = $hours_start<=0?9:$hours_start;
		$hours_close   = I('post.hours_close');
		$hours_close   = $hours_close<=9?18:$hours_close;
		$server_area   = I('post.server_area');
		$server_area   = str_replace("|", ",", str_replace("，", ",", str_replace(" ", ",", $server_area)));    //空格/中文逗号/竖线替换成英文逗号
		
		$us = D('UserShop');
		$shop = $us->getShopByShopId($shopId);

		$data = array(
			'user_name'     => I('post.user_name'),
			'user_phone'    => I('post.user_phone'),
			'description'   => I('post.description'),
			'notice'        => I('post.notice'),
			'qq'            => I('post.qq'),
			'open_hours'    => $hours_start.','.$hours_close,
			'is_week'       => I('post.is_week'),
			'server_area'   => $server_area,
			'server_range'  => I('post.server_range'),
			'is_store'      => I('post.is_store'),
			'is_door'       => I('post.is_door'),
			'is_wechat'     => I('post.is_wechat'),
			'is_sms'        => I('post.is_sms'),
			'server_status' => I('post.server_status')
		);
		$rd = $us->updateUserShop($shopId, $data);
		if($rd['status'] == '1'){
			if($shop['is_main'] == 1){
				$shops = $us->getShopsByObject(array('userId'=>$shop['user_id'], 'main'=>0));
				$main = array(
					'description'   => I('post.description'),
					'notice'        => I('post.notice'),
					'open_hours'    => $hours_start.','.$hours_close,
					'is_week'       => I('post.is_week'),
					'server_area'   => $server_area,
					'server_range'  => I('post.server_range'),
					'is_store'      => I('post.is_store'),
					'is_door'       => I('post.is_door'),
					'is_wechat'     => I('post.is_wechat'),
					'is_sms'        => I('post.is_sms'),
					'server_status' => I('post.server_status')
				);
				foreach($shops as $vo){
					$us->updateUserShop($vo['id'], $main);
				}
			}
			$this->redirect("System/info");
		}else{
			$this->error('更新失败');
		}
	}
	
	/**
     * 修改店铺地址
     */
	public function address(){
		$us = D('UserShop');
		$shop = $us->getShopByShopId();
		
		// 三级联动
		$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("shop", $shop);
		$this->display();
	}
	
	
	
	/**
     * 更新店铺地址
     */
	public function updateShopAddress(){
		$shopId  = session('shop.id');
		$areaId  = I('post.area');
		$address = I('post.address');
		$lng     = I('post.lng');
		$lat     = I('post.lat');
		
		$us = D('UserShop');
		
		$mod = array('area_id'=>$areaId, 'address'=>$address, 'lng'=>$lng, 'lat'=>$lat);
		$rd = $us->updateUserShop($shopId, array('mod_address'=>json_encode($mod)));
		echo $rd['status'];exit;
	}
	
	/**
	 * 我的维修
	 */
    public function repair(){
		$shopId = session('shop.id');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');

		$data = $orders->getRepairByObject(array("shopId"=>$shopId, "m"=>0, "n"=>8));
		foreach($data as &$vo){
			if($vo['new_price'] !='0.00'){
				$money = $vo['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($vo['relation_id'] > 0 && $vo['color_id'] > 0){
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$color    = $repairs->getColorById($vo['color_id']);
				$vo['show_name']  = $relation['pro_name']." ".$color['name']." ".$relation['plan_name'];
				$vo['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
			}else{
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$vo['show_name']  = $vo['remarks'];
				$vo['show_img']   = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price'] = $money;
			
			// 订单是否已申诉
			$appeal = $orders->getOrderAppealByOrderId($vo['id']);
			if(empty($appeal)){
				$vo['appeal'] = 1;
			}
		}
		unset($vo);

		$this->assign("orders", $data);
		$this->display();
    }
	
	/**
	 * 加载更多我的维修数据
	 */
    public function loadMoreRepair(){
		$shopId  = session('shop.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		
		$data = $orders->getRepairByObject(array("shopId"=>$shopId, "m"=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			if($vo['new_price'] !='0.00'){
				$money = $vo['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($vo['relation_id'] > 0 && $vo['color_id'] > 0){
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$color    = $repairs->getColorById($vo['color_id']);
				$vo['show_name']  = $relation['pro_name']." ".$color['name']."<br />".$relation['plan_name'];
				$vo['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
			}else{
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$vo['show_name']  = $vo['remarks'];
				$vo['show_img']   = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price'] = $money;
			$vo['order_date'] = date('Y-m-d', $vo['order_date']);
			
			// 订单是否已申诉
			$appeal = $orders->getOrderAppealByOrderId($vo['id']);
			if(empty($appeal)){
				$vo['appeal'] = 1;
			}
		}
		unset($vo);
		echo json_encode($data);
    }
	
	/**
	 * 正在抢单
	 */
    public function offerIng(){
		$shopId = session('shop.id');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');

		$data = $orders->getOrderOfferIngByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'm'=>0, 'n'=>8));
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['pro_name']  = $relation['pro_name'];
				$vo['pro_name']  = $relation['pro_name'];
				$vo['color_name'] = $color['name'];
				$vo['plan_name'] = $relation['plan_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['id']);
		}
		unset($vo);
		
		// 未读可以报价数量
		$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));
		
		$this->assign("offerCount", $offerCount);
		$this->assign("offer", $data);
		$this->display('offer_ing');
    }
	
	/**
	 * 加载更多正在抢单数据
	 */
    public function loadMoreOfferIng(){
		$shopId  = session('shop.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		
		$data = $orders->getOrderOfferIngByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['pro_name']  = $relation['pro_name'];
				$vo['pro_name']  = $relation['pro_name'];
				$vo['color_name'] = $color['name'];
				$vo['plan_name'] = $relation['plan_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['id']);
			$vo['order_date']  = CBWTimeFormat($vo['order_date'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
    }
	
	/**
	 * 已抢订单
	 */
    public function offer(){
		$shopId = session('shop.id');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');

		$data = $orders->getOrderOfferByShopId($shopId, 0, 8);
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['pro_name']  = $relation['pro_name'];
				$vo['pro_name']  = $relation['pro_name'];
				$vo['color_name'] = $color['name'];
				$vo['plan_name'] = $relation['plan_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['id']);
		}
		unset($vo);
		
		// 未读可以报价数量
		$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));
		
		$this->assign("offerCount", $offerCount);
		$this->assign("offer", $data);
		$this->display('offer');
    }
	
	/**
	 * 加载更多已抢订单数据
	 */
    public function loadMoreOffer(){
		$shopId  = session('shop.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		
		$data = $orders->getOrderOfferByShopId($shopId, $counter*$num, $num);
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['pro_name']  = $relation['pro_name'];
				$vo['pro_name']  = $relation['pro_name'];
				$vo['color_name'] = $color['name'];
				$vo['plan_name'] = $relation['plan_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['id']);
			$vo['order_date']  = CBWTimeFormat($vo['order_date'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
    }
	
	/**
	 * 创建维修订单
	 */
    public function create(){
		$shopId  = session('shop.id');
		
		$us   = D('UserShop');
		$cat  = D('ProductCat');
		$area = D('Area');
		
		$shop = $us->getShopByShopId($shopId);
		if($shop['type_id'] != 2){
			$this->error("店铺权限不足", U('System/index'));exit;
		}
		
		$brands = $cat->getCatListById(10);
		
		//加载城市信息
        $info = $area->getAreaById($shop['area_id']);
		$p = empty($info['one_id'])?'选择省':$info['one_id'];
		$c = empty($info['two_id'])?'选择市':$info['two_id'];
		$a = empty($info['id'])?'选择县/区':$info['id'];
        $city = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$c = \Think\Area::city($city);
		
		$this->assign("brands", $brands);
		$this->assign("city", $c);
		$this->display();
    }

    /**
     * 执行品牌类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeBrandSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByBrandId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择型号</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['pro_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行型号类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeModelSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getColorByRepairId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择颜色</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行颜色类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeColorSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByModelId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择故障与方案</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['fault_name'].' - '.$vo['plan_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行故障关联change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeRelationSelect(){
        $id      = I('get.id');
		$colorId = I('get.color');
		$money   = 0;
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByRelationId($id);
		if(!empty($data)){
			$money = $data['money'];
			if(!empty($data['color_money'])){
				$moneys = json_decode($data['color_money']);
				$money = $moneys[$colorId];
			}
		}
        echo $money;
		exit;
    }
	
	/**
	 * 创建维修订单操作
	 */
    public function doCreateOrder(){
		$shopId     = session('shop.id');
		$way        = I('post.way');
		$money      = I('post.money');
		$uname      = I('post.uname');
		$mobile     = I('post.mobile');
		$password   = substr($mobile, -6);
		$areaId     = I('post.area');
		$address    = I('post.address');
		$colorId    = I('post.color');
		$relationId = I('post.relation');
		$remarks    = I('post.remarks');
		$orderSn    = CBWCreateOrderNumber();
		
		$users  = D('Users');
		$us     = D('UserShop');
		$orders = D('Orders');

		// 店铺信息
		$shop = $us->getShopByShopId($shopId);
		if($areaId == 0){
			$areaId = $shop['area_id'];
		}
		if($shop['type_id'] != 2){
			$this->error("店铺权限不足", U('System/index'));exit;
		}

		// 检查手机号码是否注册,否则先注册
		$user = $users->getUserByMobile($mobile);
		if(empty($user)){
			$rs = $users->regist($mobile, $password);
			if($rs['userId']>0){                       // 注册成功
				// 发送密码
				sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
				
				// 加载用户信息				
				$user = $users->getUserById($rs['userId']);
			}
		}
		
		if($user['id'] == $shop['user_id']){
			$this->error("不允许下单到自己的店铺", U('System/create'));
		}
		
		$data['user_id']       = $user['id'];
		$data['agent_id']      = $shop['user_id'];
        $data['order_sn']      = $orderSn;
        $data['order_date']    = time();
		$data['order_type']    = 1;
        $data['order_status']  = 1;
        $data['pay_status']    = 0;
		$data['area_id']       = $areaId;
		$data['address']       = $address;
		$data['reciver_user']  = $uname;
		$data['reciver_phone'] = $mobile;
		$data['deail_price']   = $money;
		$data['is_line']       = 1;
		$data['remarks']       = $remarks;
		$orderId = $orders->insertOrders($data);
		if($orderId){
			$data = array();
			$data['order_id']      = $orderId;
			$data['shop_id']       = $shop['id'];
			$data['color_id']      = $colorId;
			$data['relation_id']   = $relationId;
			$data['server_way']    = $way;
			$data['server_status'] = 2;
			$serverId = $orders->insertOrderServer($data);        // 添加维修订单附表
			if($serverId){
				$this->success('创建订单成功',U("Orders/rdetail", array('sn'=>$orderSn)));
			}
			$orders->deleteOrders($orderId);   // 下单失败,删除订单表数据
		}
		$this->error("创建订单失败.");
    }
	
	/**
	 * 维修检测
	 */
    public function rtest(){
		$orderId = I('get.id');
		$shopId  = session('shop.id');
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$cat       = D('ProductCat');
		$shippings = D('Shippings');
	
		// 订单信息
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($orderId);
		
		if($server['shop_id'] != $shopId){
			$this->error("该订单已调拨",U('System/repair'));
		}
		
		// 故障信息
		$relation = $repairs->getRepairByRelationId($server['relation_id']);

		if($relation['type_id'] > 0){
			$type  = $cat->getCat($relation['type_id']);           // 类型
		}
		if($relation['brand_id'] > 0){
			$brand = $cat->getCat($relation['brand_id']);          // 品牌
		}
		if($server['color_id'] > 0){
			$color = $repairs->getColorById($server['color_id']);  // 颜色
		}
		$order['type']         = $type['cat_name'];
		$order['brand']        = $brand['cat_name'];
		$order['color_id']     = $server['color_id'];
		$order['color_name']   = $color['name'];
		$order['warranty']     = $server['warranty'];
		$order['model']        = $relation['pro_name'];
		$order['fault']        = $relation['description'];
		$order['plan']         = $relation['plan_name'];
		$order['pro_id']       = $relation['pro_id'];
		$order['relation_ids'] = $server['relation_ids'];
		$order['new_price']    = $server['new_price'];
		$order['shop_remarks'] = $server['shop_remarks'];
		
		if($relation['pro_id'] > 0){
			// 重新评估select
			$relationRes = $repairs->getRepairByModelId($relation['pro_id']);
			// 重新评估故障信息
			if(!empty($server['relation_ids'])){
				$again = $repairs->getRepairByRelationId($server['relation_ids']);
				$order['again_plan_name'] = $again['plan_name'];
			}
		}
		
		if($server['server_way'] == 3){
			// 寄回物流公司信息
			$shipping = $shippings->getShippingCompanyById($server['return_way']);
			$order['return_name'] = $shipping['cname'];
			$order['return_num']  = $server['return_num'];
			// 寄回物流信息
			$data = array();
			if(!empty($order['return_num'])){
				$expresss = $shippings->getExpressByNumber($server['return_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['return'] = $data;
			
			// 寄出物流公司信息
			$shipping = $shippings->getShippingCompanyById($server['shipping_way']);
			$order['shipping_name'] = $shipping['cname'];
			$order['shipping_num']  = $server['shipping_num'];
			// 寄出物流信息
			$data = array();
			if(!empty($server['shipping_num'])){
				$expresss = $shippings->getExpressByNumber($server['shipping_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['shipping'] = $data;
		}
		
		$this->assign('relationRes',$relationRes);
		$this->assign('order',$order);
		
		$this->display();
    }
	
	/**
	 * 更新维修金额
	 */
	public function updateServerMoney(){
		$fault   = I('post.fault');
		$orderId = I('post.oid');
		$colorId = I('post.cid');
		$shopId  = session('shop.id');
		
		$orders = D('Orders');
		$repair = D('Repairs');
		$us     = D('UserShop');
		
		$server = $orders->getOrderServerByOrderId($orderId);
		$shop   = $us->getShopByShopId($shopId);

		$money = 0;
		$relation = $repair->getRepairByRelationId($fault);
		$plan     = $repair->getPlanByPlanId($relation['plan_ids']);
		if(!empty($plan['color_money'])){
			$moneys = json_decode($plan['color_money']);
			$money += $moneys[$colorId];
		}else{
			$money += $plan['money'];
		}
		echo number_format($money, 2, '.', '');
	}
	
	/**
	 * 重新评估
	 */
	public function updateServer(){
		$orderId = I('post.oid');
		$fault   = I('post.fault');
		$money   = I('post.money');
		$remarks = I('post.remarks');

		$orders = D('Orders');
		
		$rt = $orders->updateOrderServer($orderId, array('relation_ids'=>$fault, 'shop_remarks'=>$remarks, 'new_price'=>$money));
		if($rt['status'] > 0){
			echo 1;exit; 
		}else{
			echo 0;exit; 
		}
	}
	
	/**
	 * 关闭订单
	 */
    public function close(){
		$orderId = I('post.id');
		
		$orders = D('Orders');
		
		$order = $orders->getOrderById($orderId);
		
		$rt = $orders->closeOrders($orderId);
		if($rt['status'] > 0){
			// 取消维修订单或回收订单,给店铺发送一条短信
			sendSmsByAliyun($order['reciver_phone'], array('order'=>$order['order_sn']), 'SMS_116591037'); // 取消维修订单
			
			echo 1;exit;
		}
		echo 0;exit;
    }
	
	// 提交申诉 
	public function doAppeal(){
		$orderId = I('post.id');
		$reason  = I('post.reason');
		
		$orders = D('Orders');
		
		$appeal = array(
			'order_id'      => $orderId,
			'reason'        => $reason,
			'appeal_date'   => time(),
			'appeal_status' => 1
		);
		$rt = $orders->insertOrderAppeal($appeal);
		if($rt['status'] > 0){
			$this->success('提交申诉成功');
		}else{
			$this->error('提交申诉失败');
		}
	}
	
	/**
	 * 完成回收订单
	 */
    public function complete(){
		$shopId  = session('shop.id');
		$orderId = I('get.id');
		
		$orders = D('Orders');
		$us     = D('UserShop');
		
		$rt = $orders->updateOrders($orderId, array('pay_date'=>time(), 'ok_date'=>time(), 'pay_status'=>1, 'order_status'=>2));
		if($rt['status'] > 0){
			$shop = $us->getShopByShopId($shopId);
			$us->updateUserShop($shopId, array('exp'=>$shop['exp']+10, 'recovery_count'=>$shop['recovery_count']+1));
			$this->redirect('System/recovery');exit;
		}
		$this->error("操作失败.");	
    }
	
	/**
	 * 完成保障卡维修订单页面
	 */
    public function security(){
		$orderId = I('get.id');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
	
		// 订单信息
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($orderId);

		// 故障信息
		$relation = $repairs->getRepairByRelationId($server['relation_id']);

		$type  = $cat->getCat($relation['type_id']);   // 类型
		$brand = $cat->getCat($relation['brand_id']);  // 品牌
		$order['type']     = $type['cat_name'];
		$order['brand']    = $brand['cat_name'];
		$order['color_id'] = $server['color_id'];
		$order['model']    = $relation['pro_name'];
		$order['fault']    = $relation['description'];
		$order['plan']     = $relation['plan_name'];
		$order['pro_id']   = $relation['pro_id'];

		$this->assign('order',$order);
		
		$this->display();
    }
	
	/**
	 * 完成保障卡维修订单操作
	 */
    public function doSecurity(){
		$orderId = I('post.id');
		
		// 判断有没有文件上传
		$upload1 = I('post.upload1');
		$upload2 = I('post.upload2');
        if(empty($upload1) || empty($upload2)){
			$this->error("请上传照片.");
        }
		
		$orders = D('Orders');
		$users  = D('Users');
		$us     = D('UserShop');
		
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($order['id']);
		if($order['order_status'] == 1 && $server['security'] == 1){
			$rt = $orders->updateOrders($order['id'], array('order_status'=>2, 'pay_status'=>1, 'pay_date'=>time(), 'pay_mode'=>4));
			if($rt['status'] > 0){
				// 保存图片
				$img1 = CBWBase64Upload($upload1, './Public/Order/security/');
				$img2 = CBWBase64Upload($upload2, './Public/Order/security/');
				$orders->updateOrderServer($order['id'], array('security_img'=>$img1.','.$img2));
				
				$server = $orders->getOrderServerByOrderId($order['id']);
				$shop   = $us->getShopByShopId($server['shop_id']);
				$users->appendMoney($shop['user_id'], $order['id'], $order['deail_price'], 2, '');        // 增加店铺用户账户金额
				$users->insertFrozenMoney($shop['user_id'], $order['id'], $order['deail_price'], 2, '');  // 增加店铺用户冻结金额
				
				$this->redirect('System/repair');	
			}
		}
		$this->error("完成订单失败.");
    }
	
	/**
	 * 我的回收
	 */
    public function recovery(){
		$shopId = session('shop.id');

		$orders  = D('Orders');;
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
		
		$data = $orders->getRecoveryByObject(array("shopId"=>$shopId, "m"=>0, "n"=>8));
		foreach($data as &$vo){
			$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
			if($recovery['new_price'] !='0.00'){
				$money = $recovery['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($recovery['reco_id'] > 0){
				$repair = $repairs->getRepairById($recovery['reco_id']);
				$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				$type     = $cat->getCat($repair['type_id']);
				$brand    = $cat->getCat($repair['brand_id']);
				$vo['show_name'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$repair['list_image'];
				$vo['show_desc'] = $attrs;
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price']   = $money;
			$vo['shop_id']      = $recovery['shop_id'];
			$vo['reco_status']  = $recovery['reco_status'];
			$vo['reco_way']     = $recovery['reco_way'];
			
			// 订单是否已申诉
			$appeal = $orders->getOrderAppealByOrderId($vo['id']);
			if(empty($appeal)){
				$vo['appeal'] = 1;
			}
		}
		unset($vo);

		$this->assign("orders", $data);
		$this->display();
    }
	
	/**
	 * 加载更多我的回收数据
	 */
    public function loadMoreRecovery(){
		$shopId  = session('shop.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders  = D('Orders');;
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
		
		$data = $orders->getRecoveryByObject(array("shopId"=>$shopId, "m"=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
			if($recovery['new_price'] !='0.00'){
				$money = $recovery['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($recovery['reco_id'] > 0){
				$repair = $repairs->getRepairById($recovery['reco_id']);
				$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				$type     = $cat->getCat($repair['type_id']);
				$brand    = $cat->getCat($repair['brand_id']);
				$vo['show_name'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$repair['list_image'];
				$vo['show_desc'] = $attrs;
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price']   = $money;
			$vo['shop_id']      = $recovery['shop_id'];
			$vo['reco_status']  = $recovery['reco_status'];
			$vo['reco_way']     = $recovery['reco_way'];
			$vo['order_date'] = date('Y-m-d', $vo['order_date']);
			
			// 订单是否已申诉
			$appeal = $orders->getOrderAppealByOrderId($vo['id']);
			if(empty($appeal)){
				$vo['appeal'] = 1;
			}
		}
		unset($vo);
		echo json_encode($data);
    }
	
	/**
	 * 我的回收检测评估
	 */
    public function assess(){
		$orderId = I('get.id');		
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$cat       = D('ProductCat');
		$shippings = D('Shippings');

		// 订单信息
		$order    = $orders->getOrderById($orderId);
		$recovery = $orders->getOrderRecoveryByOrderId($orderId);
		$repair   = $repairs->getRepairById($recovery['reco_id']);
		$type     = $cat->getCat($repair['type_id']);
		$brand    = $cat->getCat($repair['brand_id']);
		$recovery['type']       = $type['cat_name'];
		$recovery['brand']      = $brand['cat_name'];
		$recovery['model']      = $repair['pro_name'];
		$recovery['attrs']      = implode('|', $repairs->getAttribute($recovery['reco_attr']));
		$recovery['again_attr'] = implode('|', $repairs->getAttribute($recovery['new_attr']));
		
		// 基本情况
		$basic    = $repairs->getBasicByModelId($recovery['reco_id']);

		// 功能情况
		$function = $repairs->getFunctionByModelId($recovery['reco_id']);

		// 其他情况
		$other    = $repairs->getOtherByModelId($recovery['reco_id']);
		
		if($recovery['reco_way'] == 3){
			// 寄回物流公司信息
			$shipping = $shippings->getShippingCompanyById($recovery['return_way']);
			$order['return_name'] = $shipping['cname'];
			$order['return_num']  = $recovery['return_num'];
			// 寄回物流信息
			$data = array();
			if(!empty($order['return_num'])){
				$expresss = $shippings->getExpressByNumber($recovery['return_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['return'] = $data;
			
			// 寄出物流公司信息
			$shipping = $shippings->getShippingCompanyById($recovery['shipping_way']);
			$order['shipping_name'] = $shipping['cname'];
			$order['shipping_num']  = $recovery['shipping_num'];
			// 寄出物流信息
			$data = array();
			if(!empty($recovery['shipping_num'])){
				$expresss = $shippings->getExpressByNumber($recovery['shipping_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['shipping'] = $data;
		}
		
		$this->assign('other',$other);
		$this->assign('function',$function);
		$this->assign('basic',$basic);
		$this->assign('order',$order);
		$this->assign('recovery',$recovery);
		
		$this->display();
    }
	
	/**
	 * 更新回收服务
	 */
    public function updateRecovery(){
		$orderId  = I('post.id');
		$money    = I('post.money');
		$ids      = I('post.ids');

		$orders = D('Orders');
		
		$rt = $orders->updateOrderRecovery($orderId, array('new_price'=>$money, 'new_attr'=>$ids));
		if($rt['status'] > 0){
			echo 1;exit;
		}else{
			echo 0;exit;
		}
    }
	
	/**
	 * 计算评估金额
	 */
    public function compute(){
		$modelId = I('post.model');
		$ids     = I('post.ids');
		$ids     = implode(",", $ids);

		$repairs = D('Repairs');
		
		$money = $repairs->computeMoney($modelId, $ids);

		echo json_encode(array('ids'=>$ids, 'money'=>$money));exit;
    }
	
	/**
	 * 我发布的
	 */
    public function products(){
		$shopId = session('shop.id');
		$this->display();
    }
	
	/**
	 * 我卖出的
	 */
    public function shopping(){
		$shopId = session('shop.id');
		$this->display();
    }
	
	/**
	 * 店铺使用视频
	 */
    public function mv(){
		$this->display();
	}
	
	/**
	 * 店铺设置
	 */
    public function reset(){
		$shopId  = session('shop.id');
		$typeId  = I('get.type');
		$typeId  = empty($typeId)?10:$typeId;
		$brandId = I('get.brand');
		$modelId = I('get.model');
		
		$cat     = D('ProductCat');
		$us      = D('UserShop');
		$repairs = D('Repairs');

		$shop    = $us->getShopByShopId($shopId);                        // 店铺信息
		$types   = $cat->getCatListById(3);                              // 类型
		$brands  = $us->getUserBrandByTypeId($shop['user_id'], $typeId);          // 品牌
		$brandId = empty($brandId)?$brands[0]['id']:$brandId;
		if(!empty($brandId)){
			$models   = $us->getUserModelByBrandId($shop['user_id'], $brandId);   // 型号
			$modelId  = empty($modelId)?$models[0]['id']:$modelId;
		}
		// 弹窗数据
		$brandRes = $cat->getCatListById($typeId);
		$umodels  = $us->getUserModelByBrandId($shop['user_id'], $brandRes[0]['id']);
		$modelRes = $repairs->getRepairByBrandId($brandRes[0]['id']);
		foreach($modelRes as &$vo){
			foreach($umodels as $v){
				if($vo['id'] == $v['model_id']){
					$vo['on'] = 1;
				}
			}
		}
		unset($vo);
		
		$this->assign("modelRes", $modelRes);
		$this->assign("brandRes", $brandRes);
		$this->assign("models", $models);
		$this->assign("brands", $brands);
		$this->assign("types", $types);
		$this->assign("model", $modelId);
		$this->assign("brand", $brandId);
		$this->assign("type", $typeId);
		$this->display();
    }
	
	/**
	 * 加载型号
	 */
	public function loadModel(){
		$shopId  = session('shop.id');
		$brandId = I('post.id');
		
		$us      = D('UserShop');
		$repairs = D('Repairs');

		$shop = $us->getShopByShopId($shopId);                         // 店铺信息
		
		$models = $us->getUserModelByBrandId($shop['user_id'], $brandId);
		$data   = $repairs->getRepairByBrandId($brandId);
		foreach($data as $key=>$vo){
			foreach($models as $v){
				if($vo['id'] == $v['model_id']){
					$data[$key]['on'] = 1;
				}
			}
		}
		echo json_encode($data);exit;
	}
	
	/**
	 * 保存维修设备
	 */
	public function saveModel(){
		$shopId  = session('shop.id');
		$brandId = I('post.brand');
		$models  = I('post.model');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);                         // 店铺信息
		
		$smodel = $us->getUserModelByBrandId($shop['user_id'], $brandId);
		foreach($smodel as $key=>$vo){
			if(!in_array($vo['model_id'],$models)){
				$us->deleteShopModel($shop['user_id'], $brandId, $vo['model_id']);
			}
			$smodels[$key] = $vo['model_id'];
		}
		foreach($models as $modelId){
			if(!in_array($modelId, $smodels)){
				$data['user_id']  = $shop['user_id'];
				$data['brand_id'] = $brandId;
				$data['model_id'] = $modelId;
				$us->insertUserModel($data);
			}
		}		
		echo 1;exit;
	}
	
	/**
	 * 我的评价
	 */
    public function comment(){
		$shopId = session('shop.id');
		
		$comments = D('Comments');
		
		$data = $comments->getCommentsByObject(array('shopId'=>$shopId, 'm'=>0, 'n'=>8));
		
		$this->assign('comments', $data);
		$this->display();
    }
	
	/**
	 * 加载更多我的评价
	 */
    public function loadMoreComment(){
		$shopId = session('shop.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$comments = D('Comments');
		
		$data = $comments->getCommentsByObject(array('shopId'=>$shopId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as $key=>$vo){
			$data[$key]['comment_time'] = date('Y-m-d', $vo['comment_time']);
		}
		echo json_encode($data);
    }
	
	/**
	 * 回复评价
	 */
    public function reply(){
		$shopId    = session('shop.id');
		$commentId = I('post.id');
		$content   = I('post.content');
		
		$comments = D('Comments');
		
		$data['comment_id']    = $commentId; 
		$data['admin_user_id'] = $shopId; 
		$data['reply_time']    = time(); 
		$data['content']       = $content; 
		$rt = $comments->insertReply($data);
		echo $rt;
    }
	
	/**
	 * 下载海报
	 */
    public function poster(){
		$poster = D('Poster');
		$data = $poster->getPosterByObject();
		$this->assign("data", $data);
		$this->display();
    }
	
	// 生成海报
	public function createPoster(){
		$us = D('UserShop');
		$pr = D('Poster');
		
		$dir        = date("Ymd");
		$shopId     = session('shop.id');
		$posterId   = I('post.id');
		$shop       = $us->getShopByShopId($shopId);
		$poster     = $pr->getPosterById($posterId);
		$saveFile   = "./Public/Temp/".$dir."/poster_".$shop['id']."_".$posterId.".jpg"; // 海报文件
		$tempFile   = "./Public/Poster/".$poster['image'];                               // 模板文件
		$qrcodeFile = "./Public/Temp/".$dir."/qrcode_".$shop['id'].".jpg";               // 二维码文件

		if(!file_exists($saveFile)){
			// 生成公众号关注二维码
			$wechatObj = new \Think\WechatApi();
			for($i=0; $i<100; $i++){
				$qrcode = $wechatObj->qrcodeCreate($shop['id'], 2);
				if(!empty($qrcode)){
					break;
				}
			}

			$image = new \Think\Image();  //图片处理类
			// 添加二维码
			$image->open($tempFile)->water($qrcodeFile, array(235,5063))->save($saveFile);
		}
		echo $dir."/poster_".$shop['id']."_".$posterId.".jpg";exit;
	}
	
	/**
	 * 系统说明
	 */
    public function introduce(){
		$article = D('Article');
		$introduce = $article->getArticleList(12);
		$this->assign("introduce", $introduce);
		$this->display();
    }
	
	// 我的通知
	public function notice(){
		$shopId = session('shop.id');
		$status = I('get.status');
		
		$us = D('UserShop');

		$notices = $us->getShopNoticeByObject(array('shopId'=>$shopId, 'status'=>$status, 'm'=>0, 'n'=>8));
		
		$this->assign('status', $status);
		$this->assign("notices", $notices);
		$this->display();
	}
	
	/**
	 * 加载更多我的通知
	 */
    public function loadMoreNotice(){
		$shopId  = session('shop.id');
		$status  = I('post.status');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$us = D('UserShop');
		
		$data = $us->getShopNoticeByObject(array('shopId'=>$shopId, 'status'=>$status, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as $key=>$vo){
			$data[$key]['create_time'] = date('Y-m-d H:i:s', $vo['create_time']);
		}
		echo json_encode($data);
    }
	
	public function doReadNotice(){
		$shopId = session('shop.id');
		
		$us = D('UserShop');
		
		$us->updateShopNotice("shop_id=$shopId", array('status'=>1));
		$this->redirect("System/notice"); 
	}
	
	// 专属客服
	public function service(){
		$shopId  = session('shop.id');
		
		$model = M();
		$us    = D('UserShop');

		$shop = $us->getShopByShopId($shopId);
		$data = $model->table('__ADMIN_USERS__ AS u')
							->field('u.*,i.*')
							->join('__ADMIN_INFO__ AS i ON i.uid=u.id')
							->where('u.id='.$shop['service_id'])
							->find();
		$this->assign("service", $data);
		$this->display();
	}
	
	// 通知详情
	public function noticedetail(){
		$noticeId = I('get.id');
		
		$us = D('UserShop');

		$data = $us->getShopNoticeByNoticeId($noticeId);
		if($data['status'] == 0){
			$us->updateShopNotice("id=$noticeId", array('status'=>1));
		}
		$this->assign("notice", $data);
		$this->display();
	}

}