<?php
namespace Mobile\Controller;
/**
 * 商品控制器
 */
class ProductController extends BaseController{
	
    /**
	 * 首页
	 * 
	 */
    public function index(){
		$cat      = D('ProductCat');
		$products = D('Products');
		$ads      = D('Advs');
		$search   = D('Search');
		
		// 热搜词
		$hots = $search->getWords(10);
		
		// 搜索条件
		$groupId = session('user.groupid');
		$vip     = session('user.vip');

		// Banner
		$banners = $ads->getAdvsByCat(9, 1);
		
		
        // 新品推荐
		$news = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'new'=>'is_new', 'groupby'=>1, 'sort'=>'new', 'm'=>0, 'n'=>4));
		
		// 外配
		$outsides   = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>6, 'hot'=>'is_hot', 'groupby'=>1, 'm'=>0, 'n'=>7));
		
		// 内配
		$insides   = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>5, 'hot'=>'is_hot', 'groupby'=>1, 'm'=>0, 'n'=>7));		
		
		// 工具
		$tools   = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>9, 'hot'=>'is_hot', 'groupby'=>1, 'm'=>0, 'n'=>7));
		
		$this->assign('hots', $hots);
		$this->assign('banners',$banners);
		$this->assign('news', $news);
		$this->assign('outside', $outsides[0]);
		$this->assign('outsides', $outsides);
		$this->assign('inside', $insides[0]);
		$this->assign('insides', $insides);
		$this->assign('tool', $tools[0]);
		$this->assign('tools', $tools);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('product');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

        $this -> display();
    }
	
	/**
	 * 分类页
	 * 
	 */
	public function category(){
		$cat = D('ProductCat');
		$category = $cat->getAllCategory(1);     // 商城所有分类
		$this->assign('category', $category);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('product');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

        $this -> display();
	}
	
	/**
	 * 列表页
	 * 
	 */
	public function lists(){
		$cat      = D('ProductCat');
		$products = D('Products');
		$search   = D('Search');
		
		// 热搜词
		$hots = $search->getWords(10);

		// 搜索条件
		$groupId  = session('user.groupid');
		$vip      = session('user.vip');
		$classId  = I('get.class');
		$typeId   = I('get.type');
		$brandId  = I('get.brand');
		$modelId  = I('get.model');
		$min	  = I('get.min');
		$max	  = I('get.max');
		$sort     = I('get.sort');
		$keyword  = I('get.keyword');
		
		if(!empty($classId)){
			$types = $cat->getCatListById($classId);
		}
		if(!empty($typeId)){
			$type = $cat->getCat($typeId);
			$brands = $cat->getCatListById($typeId);
		}
		if(!empty($brandId)){
			$brand = $cat->getCat($brandId);
			$models = $cat->getCatListById($brandId);
		}
		if(!empty($modelId)){
			$model = $cat->getCat($modelId);
		}
		
		$data = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$modelId, 'groupby'=>1, 'min'=>$min, 'max'=>$max, 'sort'=>$sort, 'keyword'=>$keyword, 'm'=>0, 'n'=>8));
		
		$this->assign('hots', $hots);
		$this->assign('type', $type);
        $this->assign('brand', $brand);
		$this->assign('model', $model);
        $this->assign('products', $data);
		$this->assign('types', $types);
		$this->assign('brands', $brands);
		$this->assign('models', $models);
		$this->assign('classId', $classId);
		$this->assign('typeId', $typeId);
		$this->assign('brandId', $brandId);
		$this->assign('modelId', $modelId);
		$this->assign('min', $min);
		$this->assign('max', $max);
		$this->assign('sort', $sort);
		$this->assign('keyword', $keyword);
		
		//SEO
		$title       = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
		$keywords    = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, '.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发商城';
		$description = '草包网商城专业提供'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'系列原厂品质配件。无论大小配件均由维修工程师测试合格方可寄出，请广大用户放心购买。';
		switch($classId){
			case 5:
				$title    = (empty($model['cat_name'])?$brand['cat_name']:$brand['cat_name'].$model['cat_name']).$type['cat_name'].'配件批发网';
				$keywords = (empty($model['cat_name'])?$brand['cat_name']:$brand['cat_name'].$model['cat_name']).$type['cat_name'].'配件网, '.(empty($model['cat_name'])?$brand['cat_name']:$model['cat_name']).$type['cat_name'].'配件批发平台';
				$strings  = empty($type['cat_name'])?$class['cat_name']:$type['cat_name'];
				if(!empty($brands)){
					$names = array();
					foreach($brands as $key=>$vo){
						$names[$key] = $vo['cat_name'];
					}
					$strings = implode('、', $names);
				}
				if(!empty($models)){
					$names = array();
					foreach($models as $key=>$vo){
						$names[$key] = $vo['cat_name'];
					}
					$strings = implode('、', $names);
				}
				$description = '草包网商城专业提供'.$strings.'系列原厂品质配件。无论大小配件均由维修工程师测试合格方可寄出，请广大用户放心购买。';
				break;  
			case 6:
				$title    = '手机'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
				$keywords = '手机'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, 手机'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发平台';
				$strings  = empty($type['cat_name'])?$class['cat_name']:$type['cat_name'];
				if(!empty($types)){
					$names = array();
					foreach($types as $key=>$vo){
						$names[$key] = $vo['cat_name'];
					}
					$strings = implode('、', $names);
				}
				$description = '草包网商城专业提供'.$strings.'系列原厂品质配件。无论大小配件均由维修工程师测试合格方可寄出，请广大用户放心购买。';
				break;  
			case 8:
				$title    = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
				$keywords = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, '.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发平台';
				if(!empty($brand)){
					$description = '草包网您提供'.$brand['cat_name'].'手机导购，'.$brand['cat_name'].'手机推荐，'.$brand['cat_name'].'手机评价，'.$brand['cat_name'].'手机图片，'.$brand['cat_name'].'手机好不好，怎么样等产品信息，了解更多'.$brand['cat_name'].'手机导购信息，就上草包网，正品行货，全国联保！';
				}else{
					$description = '草包网商城（www.caobao.com)提供正品行货手机。新品手机、手机推荐、手机最新报价、促销、测评、图片、价格行情、评价咨询等购物信息，更多更全手机品牌尽在草包网！';
				}
				break;  
			case 9:
				$title       = '手机数码维修'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
				$keywords    = '手机数码维修'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, 手机数码维修'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发平台';
				$description = '草包网商城专业提供手机数码维修工具，拆机工具，各类螺丝刀.品质可靠，物美价廉，所售产品均属正品，受广大客户一致好评。';
				break;
		}
        $this->assign("title", $title);
        $this->assign("keywords",$keywords);
        $this->assign("description",$description);

        $this -> display();
	}
	
	/**
	 * 加载更多商品
	 * 
	 */
	public function loadMoreProduct(){
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		// 查询商品的条件
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		$classId = I('post.class');
		$typeId  = I('post.type');
		$brandId = I('post.brand');
		$modelId = I('post.model');
		$min	 = I('post.min');
		$max	 = I('post.max');
		$sort    = I('post.sort');
		$keyword = I('post.keyword');
		
		$products = D('Products');
		$data = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$modelId, 'groupby'=>1, 'min'=>$min, 'max'=>$max, 'sort'=>$sort, 'keyword'=>$keyword, 'm'=>$counter*$num, 'n'=>$num));
		echo json_encode($data);
	}
	
	/**
	 * 商品详细页
	 * 
	 */
    public function detail(){
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$id      = I('get.id');
		$shareId = I('get.u');
		
		// 分享链接带用户参数,保存分享用户编号
		if(!empty($shareId)){
			session('shareid', $shareId);
		}

		$products = D('Products');
		$comments = D('Comments');
		$orders   = D('Orders');
		$users    = D('Users');
		$us       = D('UserShop');
		$area     = D('Area');
		
		// 商品信息
		$data = $products->getRelationByRelationId($id);
		!isset($data) && $this->redirect('Empty/index');
		$data['price'] = $data['one_price'];
		if($groupId == 1){
			if($data['two_price'] > 0){
				$data['price'] = $data['two_price'];
			}
			if($num >= 5 && $data['four_price'] > 0){
				$data['price'] = $data['four_price'];
			}
		}
		if($vip == 1){
			if($data['three_price'] > 0){
				$data['price'] = $data['three_price'];
			}elseif($data['two_price'] > 0){
				$data['price'] = $data['two_price'];
			}
		}
		
		$attrs = explode("|", $data['pro_attr']);
		$attribute = $products->getAttributeByProductId($data['id']);
		$attr = $products->getProductAttrByAttributeId($attribute[0]['id']);
		$attrId = $attr[0]['id'];
		foreach($attr as $vo){
			if(in_array($vo['id'], $attrs)){
				$attrId = $vo['id'];
			}
		}
		if($attrId > 0){
			$data['album'] = $products->getAlbumByAttrId($attrId);
		}
		
		$data['attrs'] = $products->getProductAttrByProductId($data['id']);
		$data['attr_name'] = $products->getAttrNameByAttrId($data['pro_attr']);
		
		// 关联工具
		$links = $products->getProductLinkByProductId($data['id'], $groupId, $vip, 3);
		
		// 商品评论
		$commentLength = $comments->getCommentCountByProductId($data['id']);
		$commentsRes   = $comments->getCommentByProductId($data['id'], 0, 5);
		foreach($commentsRes as &$vo){
			$replies = $comments->getRepliesByCommentId($vo['id']);
			$op      = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$number = 1;
			$attr   = '默认';
			if(!empty($op['pro_attr'])){
				$number = $op['pro_number'];
				$attr   = $products->getAttrNameByAttrId($op['pro_attr']);
			}
			$vo['number']  = $number;
			$vo['attr']    = $attr;
			$vo['replies'] = $replies['content'];
			
			if($vo['user_id'] > 0){
				$user = $users->getUserAndInfoById($vo['user_id']);
				$vo['city']  = $user['city_addr'];
				$vo['uname'] = $user['uname'];
				$vo['thumb'] = $user['thumb'];
			}
		}
		unset($vo);
		
		// 店铺信息
		if($data['shop_id'] > 0){
			$shop = $us->getShopByShopId($data['shop_id']);
		}
		
		// 商品参数
		$paramRes = $products->getProductParamById($data['id']);
		
		// 增加点击数
		$products->statistics($data['rid'], 'click'); 
		
		// 还购买了
		$buyRes = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'typeId'=>$data['type_id'], 'groupby'=>1, 'sort'=>'new', 'm'=>0, 'n'=>5));
		
		// 生成公众号关注二维码
		$wechatObj = new \Think\WechatApi();
		$package = $wechatObj->getSignPackage();
		
		$this->assign('attrs',$attrs);
		$this->assign('product',$data);
		$this->assign('links',$links);
		$this->assign('commentLength',$commentLength);
		$this->assign('commentsRes',$commentsRes);
		$this->assign('shop',$shop);
		$this->assign('paramRes',$paramRes);
		$this->assign('buyRes',$buyRes);
		$this->assign("package",$package);
		$this->assign("agent",CBWUserAgent());
		
		// SEO
		$this->assign('title', $data['pro_name']);
		$this->assign('keywords', $data['keywords']);
		$this->assign('description', $data['description']);
		
        $this->display();
    }
	
	// 获取商品关联编号
	public function getId(){
		$proId = I('post.pro_id');
		$attrs = I('post.attrs');
		
		$products = D('Products');
		
		$relation = $products->getRelationByProductIdAndAttrId($proId, $attrs);
		if($relation['id'] > 0){
			echo $relation['id'];exit; 
		}
		echo 0;exit;
	}

	// 加入购物车
	public function addCart(){
		$id      = I('post.id');
		$num     = intval(I('post.num'));
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		
		$products = D('Products');
		$us       = D('UserShop');

		$relation = $products->getRelationByRelationId($id);
		$product  = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		$shopId   = $relation['shop_id'];
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			$_SESSION['gws'][$shopId]['shop'] = $shop;
		}
		
		//判断session中是否存在相同的数据,存在则购物车数量不相加
		if(!array_key_exists($id, $_SESSION['gws'][$shopId]['cart'])){
			$_SESSION['gws_count']++;
		}
		
		$_SESSION['gws'][$shopId]['cart'][$id] = array(
			'id'          => $id,
			'num'         => $product['num'],
			'pro_price'   => $product['price'],
			'two_price'   => $relation['two_price'],
			'three_price' => $relation['three_price'],
			'pro_amount'  => number_format($product['price']*$product['num'], 2, '.', ''),
			'attr_id'     => $relation['pro_attr'],
			'attr_name'   => $products->getAttrNameByAttrId($relation['pro_attr']),
			'list_image'  => $relation['list_image'],
			'pro_name'    => $relation['pro_name'],
			'is_card'     => $relation['is_card'],
			'virtual'     => $relation['virtual_money'],
			'is_shipping' => $relation['is_shipping']
		);
		
		echo $_SESSION['gws_count'];
		exit;
	}

	// 立即购买
	public function buyCart(){
		$id      = I('post.id');
		$num     = intval(I('post.num'));
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		
		$products = D('Products');
		$us       = D('UserShop');

		$relation = $products->getRelationByRelationId($id);
		$product  = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		$shopId   = $relation['shop_id'];
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			$_SESSION['gws'][$shopId]['shop'] = $shop;
		}
		
		$cnum = $_SESSION['gws'][$shopId]['cart'][$id]['num']+$num;
		if($relation['pro_stock'] < $cnum){
			$cnum = $relation['pro_stock'];
		}
		
		//判断session中是否存在相同的数据,存在则购物车数量不相加
		if(!array_key_exists($id, $_SESSION['gws'][$shopId]['cart'])){
			$_SESSION['gws_count']++;
		}
		
		$_SESSION['gws'][$shopId]['cart'][$id] = array(
			'id'          => $id,
			'num'         => $product['num'],
			'pro_price'   => $product['price'],
			'two_price'   => $relation['two_price'],
			'three_price' => $relation['three_price'],
			'pro_amount'  => number_format($product['price']*$product['num'], 2, '.', ''),
			'attr_id'     => $relation['pro_attr'],
			'attr_name'   => $products->getAttrNameByAttrId($relation['pro_attr']),
			'list_image'  => $relation['list_image'],
			'pro_name'    => $relation['pro_name'],
			'is_card'     => $relation['is_card'],
			'virtual'     => $relation['virtual_money'],
			'is_shipping' => $relation['is_shipping']
		);
		
		echo $id;
		exit;
	}
		
	/**
	 * 商品评论
	 * 
	 */
	public function comments(){
		$id = I('get.id');
		
		$comments = D('Comments');
		$orders   = D('Orders');
		$products = D('Products');
		$users    = D('Users');
		$area     = D('Area');
		
		// 商品信息
		$data = $products->getProductById($id);
		
		// 商品评论
		$commentLength = $comments->getCommentCountByProductId($id);
		$this->assign('commentLength',$commentLength);
		$commentsRes = $comments->getCommentByProductId($id, 0, 8);
		foreach($commentsRes as &$vo){
			$replies = $comments->getRepliesByCommentId($vo['id']);
			$op      = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$number = 1;
			$attr   = '默认';
			if(!empty($op['pro_attr'])){
				$number = $op['pro_number'];
				$attr   = $products->getAttrNameByAttrId($op['pro_attr']);
			}
			$vo['number']  = $number;
			$vo['attr']    = $attr;
			$vo['replies'] = $replies['content'];
			
			if($vo['user_id'] > 0){
				$user = $users->getUserAndInfoById($vo['user_id']);
				$vo['city']  = $user['city_addr'];
				$vo['uname'] = $user['uname'];
				$vo['thumb'] = $user['thumb'];
			}
		}
		unset($vo);
		$this->assign('commentsRes',$commentsRes);
		
		// SEO
		$this->assign('title', $data['pro_name']);
		$this->assign('keywords', $data['keywords']);
		$this->assign('description', $data['description']);
		
        $this->display();
	}
		
	/**
	 * 加载更多评论
	 * 
	 */
	public function loadMoreComments(){
		$id      = I('post.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$comments = D('Comments');
		$orders   = D('Orders');
		$products = D('Products');
		$users    = D('Users');
		$area     = D('Area');
		
		$data = $comments->getCommentByProductId($id, $counter*$num, $num);
		foreach($data as &$vo){
			$replies = $comments->getRepliesByCommentId($vo['id']);
			$op      = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$number = 1;
			$attr   = '默认';
			if(!empty($op['pro_attr'])){
				$number = $op['pro_number'];
				$attr   = $products->getAttrNameByAttrId($op['pro_attr']);
			}
			$vo['number']  = $number;
			$vo['attr']    = $attr;
			$vo['replies'] = $replies['content'];
			
			if($vo['user_id'] > 0){
				$user = $users->getUserAndInfoById($vo['user_id']);
				$vo['city']  = $user['city_addr'];
				$vo['uname'] = $user['uname'];
				$vo['thumb'] = $user['thumb'];
			}
		}
		unset($vo);
		echo json_encode($data);
	}
		
	/**
	 * 商品介绍
	 * 
	 */
	public function introduce(){
		$id = I('get.id');
		
		$products = D('Products');
		
		// 商品信息
		$data = $products->getProductById($id);
		$this->assign('data',$data);
		
		// 商品参数
		$paramRes = $products->getProductParamById($id);
		$this->assign('paramRes',$paramRes);
		
		// SEO
		$this->assign('title', $data['pro_name']);
		$this->assign('keywords', $data['keywords']);
		$this->assign('description', $data['description']);
		
        $this->display();
	}

}