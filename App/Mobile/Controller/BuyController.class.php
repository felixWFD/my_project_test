<?php
namespace Mobile\Controller;
/**
 * 购买控制器
 */
class BuyController extends BaseController{
    /**
	 * 购物车
	 */
	public function cart(){
		$gws     = session('gws');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');

		$products = D('Products');

        foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				$product = $products->getProductPriceByRelationId($v['id'], $v['num'], $groupId, $vip);
				$v['pro_price']  = $product['price'];
				$v['num']        = $product['num'];
				$v['pro_amount'] = number_format($product['price']*$product['num'], 2, '.', '');
				$v['attr_name']  = $products->getAttrNameByAttrId($v['attr_id']);
			}
			unset($v);
        }
		unset($vo);
		$_SESSION['gws'] = $gws;
        $this->assign("gws",$gws);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('buy');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	/**
	 * 更新购买数量
	 */
    public function changeNumber(){
		$id      = I('post.id');
		$num     = I('post.num');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$gws     = session('gws');

		$products = D('Products');
		
		$data = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				if($v['id'] == $id){
					$v['num']       = $data['num'];
					$v['pro_price'] = $data['price'];
				}
			}
		}
		$_SESSION['gws'] = $gws;
        echo json_encode($data);
        exit;
	}
	
	/**
	 * 删除选中购物车的商品
	 */
	public function deleteCart(){
		$ids = I('post.ids');
		$gws = session('gws');
		if(empty($gws)){
			exit;
		}

		foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				foreach($ids as $id){
					if($v['id'] == $id){
						unset($vo['cart'][$id]);
						$_SESSION['gws_count']--;
					}
				}
			}
		}
		foreach($gws as $key=>&$vo){
			if(empty($vo['cart'])){
				unset($gws[$key]);
			}
		}
		$_SESSION['gws'] = $gws;
        echo 1;
        exit;
	}
	
    /**
	 * 订单确认
	 */
    public function collect(){
		$ids     = I('post.ids');
		$ids     = empty($ids)?I('get.ids'):$ids;
        $userId  = session('user.userid');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$gws     = session('gws');

		if(!empty($ids)){
			session('buynow', $ids);
		}
		$buynow  = explode(",", session('buynow'));
		
        empty($userId) && $this->redirect('Users/login', array('controller'=>'Buy', 'action'=>'cart'));
        empty($gws) && $this->error("购物车为空",U('Buy/cart'));
		empty($buynow) && $this->error("未选择需要购买的商品",U('Buy/cart'));
		
		$users     = D('Users');
		$ua        = D('UserAddress');
		$shippings = D('Shippings');
		$products  = D('Products');
		$us        = D('UserShop');
		
		$user     = $users->getUserAndInfoById($userId);      // 会员信息
		$address  = $ua->getAddressByUserId($userId);         // 收货地址
		foreach($address as $vo){
			if($vo['is_default'] == 1){
				$provinceId = $vo['one_id'];
				break;
			}
		}
		$shipping = $shippings->getShippings();                // 物流信息
		foreach($shipping as $vo){
			if($vo['is_default'] == 1){
				$shippingId = $vo['id'];
				break;
			}
		}

		//商品清单
		$money = 0;              // 商品总价
		$goods = array();
		foreach($gws as $shopId=>$vo){
			if($shopId > 0){
				$shop = $us->getShopByShopId($shopId);
				if($shop['user_id'] == $userId){
					$this->error("不允许购买自己店铺商品",U('Buy/cart'));
				}
			}
			$cmoney     = 0;  // 卡类商品总价
			$smoney     = 0;  // 店铺商品总价
			$virtual    = 0;  // 赠送草包豆总数量
			$isshipping = 0;  // 是否包邮
			$i          = 0;
			foreach($vo['cart'] as $k=>$v){
				if(in_array($v['id'],$buynow)){
					if($v['num'] <= 0){
						$this->error("商品缺库存",U('Buy/cart'));
					}

					$smoney  += $v['pro_price']*$v['num'];
					$money   += $v['pro_price']*$v['num'];
					$virtual += $v['virtual']*$v['num'];
					
					// 店铺与代理没有草包豆奖励
					if($groupId || $user['is_agent']){
						$virtual = 0;
					}
					// 是否卡商品
					if($v['is_card'] == 1){
						$cmoney += $v['pro_price']*$v['num'];
					}
					// 商品包邮 或 卡类商品 或 满99元 包邮
					if($v['is_shipping'] == 1 || $smoney == $cmoney || ($smoney-$cmoney) >= 99){
						$isshipping = 1;
					}
					
					// 计算运费
					$freight = $shippings->getShippingsByProvinceId($provinceId, $shippingId, $shopId, $isshipping);
					if($i == 0){
						$money += $freight;
					}
					
					$goods[$shopId]['cart'][$k] = $v;
					$goods[$shopId]['shop']     = $vo['shop'];
					$goods[$shopId]['other']    = array('smoney'=>$smoney, 'virtual'=>$virtual, 'freight'=>$freight);
					$i++;
				}
			}
        }
		
		$this->assign("user", $user);
		$this->assign("address", $address);
		$this->assign("shipping",$shipping);
		$this->assign("money",number_format($money, 2, '.', ''));
		$this->assign("isshipping",$isshipping);
		$this->assign("goods",$goods);
		$this->assign("ids",$ids);
		
		//三级联动
		$p = empty($default['one_id'])?'选择省':$default['one_id'];
		$c = empty($default['two_id'])?'选择市':$default['two_id'];
		$a = empty($default['area_id'])?'选择县/区':$default['area_id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		$this->assign("city", $city);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('buy');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
	
		$this->display();
    }
	
	/**
	 * 更新地址
	 */
	public function updateAdderss(){
		
		$userId = session('user.userid');
		$id     = I('post.id');
		
		$data['user_id']       = $userId;
		$data['reciver_user']  = I('post.reciver_user');
		$data['reciver_phone'] = I('post.reciver_phone');
		$data['area_id']       = I('post.area');
		$data['street']        = I('post.street');
		
		$userAddress = D('UserAddress');
		if(empty($id)){
			$rs = $userAddress->insertAddress($data);
		}else{
			$rs = $userAddress->updateAdderss($id, $data);
		}
		$this->redirect('Buy/collect');
	}
	
	/**
	 * 地址更改,更新物流运费
	 */
	public function loadShipping(){
		$id         = I('post.adds');
		$shippingId = I('post.shipping_id');
		$buynow     = explode(",", session('buynow'));

		$ua        = D('UserAddress');
		$shippings = D('Shippings');
		
		$address = $ua->getAddressById($id);
		$gws     = session('gws');
		$data    = array();
		foreach($gws as $key=>$vo){
			$cmoney     = 0;  // 卡类商品总价
			$smoney     = 0;  // 店铺商品总价
			$isshipping = 0;  // 是否包邮
			foreach($vo['cart'] as $v){
				if(in_array($v['id'],$buynow)){
					// 判断是否包邮
					if($v['is_shipping'] == 1){
						$isshipping = 1;
					}
					// 是否卡商品
					if($v['is_card'] == 1){
						$cmoney += $v['pro_price']*$v['num'];
					}
					$smoney += $v['pro_price']*$v['num'];
				}
			}
			// 卡类商品 或 满99元 包邮
			if($smoney == $cmoney || ($smoney-$cmoney) >= 99){
				$isshipping = 1;
			}
			// 计算运费
			$data[] = array(
				'id'      => $key,
				'freight' => $shippings->getShippingsByProvinceId($address['one_id'], $shippingId, $key, $isshipping)
			);
        }
		echo json_encode($data);
        exit;
	}
	
	/**
	 * 提交订单
	 */
	public function submitOrder(){
		$userId     = session('user.userid');
		$groupId    = session('user.groupid');
		$vip        = session('user.vip');
		$ids        = I('post.ids');
		$buynow     = explode(",", session('buynow'));
		$virtual    = I('post.virtual');
		$addressId  = I('post.adds');
		$shippingId = I('post.shipping_id');
		$remarks    = I('post.remarks');
		$gws        = session('gws');
		$agentId    = 0;
		
		empty($userId) && $this->redirect('Users/login');
		empty($gws) && $this->error("购物车为空",U('Buy/cart'));
		empty($addressId) && $this->error("请填写完整地址",U('Buy/cart'));

		$users     = D('Users');
		$ua        = D('UserAddress');
		$uw        = D('UserWechat');
		$shippings = D('Shippings');
		$orders    = D('Orders');
		
		$user     = $users->getUserAndInfoById($userId);              // 会员信息
		$address  = $ua->getAddressById($addressId);                  // 收货地址
		$shipping = $shippings->getShippingByShippingId($shippingId); // 物流信息
		$wechat   = $uw->getWechatByUserId($user['id']);              // 微信信息
		if(!empty($wechat) && $wechat['fid'] > 0){
			$agentId   = $wechat['fid'];
		}
		
		$orderSn = CBWCreateOrderNumber();
		$data    = array(
			'user_id'       => $userId,
			'agent_id'      => $agentId,
			'order_sn'      => $orderSn,
			'order_date'    => time(),
			'order_status'  => 1,
			'pay_status'    => 0,
			'area_id'       => $address['area_id'],
			'address'       => $address['street'],
			'reciver_user'  => $address['reciver_user'],
			'reciver_phone' => $address['reciver_phone'],
			'send_time'     => 0,
			'is_invoice'    => 0
		);
			
		// 组织订单数组
		$i = 0;
		foreach($gws as $shopId=>$vo){
			$smoney     = 0;  // 店铺商品总价
			$cmoney     = 0;  // 卡类商品总价
			$isshipping = 0;  // 是否包邮
			
			foreach($vo['cart'] as $v){
				if(in_array($v['id'],$buynow)){
					$smoney += $v['pro_price']*$v['num'];
					// 是否卡商品
					if($v['is_card'] == 1){
						$cmoney += $v['pro_price']*$v['num'];
					}
					// 商品包邮 或 卡类商品 或 满99元 包邮
					if($v['is_shipping'] == 1 || $smoney == $cmoney || ($smoney-$cmoney) >= 99){
						$isshipping = 1;
					}
					
					$goods[$i]['pro_id']        = $v['id'];
					$goods[$i]['pro_price']     = $v['pro_price'];
					$goods[$i]['two_price']     = $v['two_price'];
					$goods[$i]['three_price']   = $v['three_price'];
					$goods[$i]['pro_number']    = $v['num'];
					$goods[$i]['pro_attr']      = $v['attr_id'];
					$goods[$i]['virtual_money'] = $v['virtual'];
					$i++;
				}
			}
			
			$schedule[$shopId]['shop_id']      = $shopId;
			$schedule[$shopId]['shipping_way'] = $shipping['sid'];
			$schedule[$shopId]['is_shipping']  = $isshipping;
			$schedule[$shopId]['remarks']      = $remarks[$shopId];
			
			//计算运费
			$fee = $shippings->getShippingsByProvinceId($address['one_id'], $shipping['id'], $shopId, $isshipping);
			$schedule[$shopId]['shipping_fee'] = $fee;

			// 是否使用虚拟币
			$vmoney = 0;
			if($virtual > 100){
				if($user['virtual_money'] < $virtual){
					$this->error("您输入草包豆的数量超过您的库存,请重新下单!",U('Buy/cart'));
					exit;
				}elseif($virtual > ($smoney*100/2)){
					$this->error("使用草包豆不能超过订单总额的50%,请重新下单!",U('Buy/cart'));
					exit;
				}
				$discount = $virtual/100;
				$vmoney   = $virtual;
			}
			$schedule[$shopId]['virtual_money'] = $vmoney;
			$schedule[$shopId]['shop_money'] = $smoney+$fee-$discount;
			$data['deail_price'] += $smoney+$fee-$discount;
        }
		
		if($i > 0){
			$orderId = $orders->insertOrders($data);
			if($orderId){
				// 添加订单附表
				$orders->insertOrderSchedule($schedule, $orderId);
				// 添加购物订单表
				$orders->insertOrderProducts($goods, $orderId);
				// 扣除虚拟币
				if($virtual > 100){
					$users->decreaseVirtual($userId, $virtual, '1', $orderSn);
				}
				
				// 清除SESSION
				session('buynow', NULL);
				foreach($gws as &$vo){
					foreach($vo['cart'] as &$v){
						foreach($buynow as $id){
							if($v['id'] == $id){
								unset($vo['cart'][$id]);
								$_SESSION['gws_count']--;
							}
						}
					}
				}
				foreach($gws as $key=>&$vo){
					if(empty($vo['cart'])){
						unset($gws[$key]);
					}
				}
				$_SESSION['gws'] = $gws;
				
				$this->redirect('Buy/pay', array('id'=>$orderSn));
				exit;
			}else{
				$this->error('订单生成失败,请联系管理员',U('Buy/cart'));
				exit;
			}
		}
		$this->error('订单生成失败,请联系管理员',U('Buy/cart'));
		exit;
	}
	
	/**
     * 选择支付方式
     */
	public function pay(){
		$sn     = I('get.id');
		$userId = session('user.userid');
		if($userId ==""){
			$this->redirect('Users/login');
			exit;
        }
		
		$orders = D('Orders');
		$users  = D('Users');
		
		$order = $orders->getOrderBySn($sn);
		if(empty($order)){
			$this->error('订单错误(价格变更?/订单号错误?)', U('Orders/index'));
		}
		$user = $users->getUserAndInfoById($userId);
		$this->assign("user",$user);
		$this->assign("order",$order);
	
		$this->assign("subject","草包网订单");
		$this->assign("body",$this->getOrderBody($order['id']));
		$this->assign("showurl",$CBW_URL);
		$this->assign("money",$orders->getOrderMoneyById($order['id']));
		$this->assign("agent",CBWUserAgent());
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('buy');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	/**
     * 检查订单支付状态
     */
	public function orderQuery(){
		$sn = I('post.trade_no');
		$orders = D('Orders');
		$order = $orders->getOrderBySn($sn);
		echo json_encode($order);
        exit;
	}
	
	/**
     * 京东快捷支付方式页面
     */
	public function jdpay(){
		$this->isUserLogin();
		
		$sn     = I('post.trade_no');
		$userId = session('user.userid');
		
		$orders = D('Orders');
		$users  = D('Users');
		
		$order = $orders->getOrderBySn($sn);
		$money = $orders->getOrderMoneyById($order['id']);
		$cards = $users->getCardByUserId($userId);
		$dcard = $users->getDefaultCardByUserId($userId);
		$user  = $users->getUserAndInfoById($userId);

		$this->assign("order",$order);
		$this->assign("money",$money);
		$this->assign("cards",$cards);
		$this->assign("dcard",$dcard);
		$this->assign("user",$user);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('buy');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	/**
     * 京东快捷支付方式页面
     */
	public function addCard(){
		$this->isUserLogin();

		$name		= I('post.name');
		$idcardno	= I('post.idcardno');
		$bankcardno	= I('post.bankcardno');
		$bankid	    = I('post.bankid');
		$cardtype	= I('post.cardtype');
		$exp	    = I('post.exp');
		$cvv2	    = I('post.cvv2');
		$mobile		= I('post.mobile');
		$imgcode	= I('post.imgcode');
		$userId     = session('user.userid');
		$isdefault  = 1;

		if(!empty($imgcode)){                                            // 检查图形验证码
			$verify = new \Think\Verify(); 
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-1));exit;
			}
		}
		
		$users = D('Users');
		$banks = D('Banks');
		
		$check = $users->checkCardByUserIdCardNo($userId, $bankcardno);
		if($check['status'] == -1){                                       // 检查账号中该银行卡是否存在
			echo json_encode(array('status'=>-2));exit;
		}
		
		if(empty($name) || empty($idcardno)){
			$user = $users->getUserAndInfoById($userId);
			$name	   = $user['true_name'];
			$idcardno  = $user['identity'];
			$isdefault = 0;
		}

		$bankcard = new \Think\BankCard();
		$auth = $bankcard->verifyCard(array('name'=>$name, 'idcardno'=>$idcardno, 'bankcardno'=>$bankcardno, 'tel'=>$mobile));
		if($auth['isok'] == 1){
			if($auth['code'] == 1){
				$users->updateUserInfo(array('user_id'=>$userId, 'true_name'=>$name, 'identity'=>$idcardno));
				list($month, $year) = explode('-', $exp);
				$data = array(
					'user_id'      => $userId,
					'bank_id'      => $bankid,
					'card_type'    => $cardtype,
					'card_no'      => $bankcardno,
					'card_mobile'  => $mobile,
					'card_exp'     => substr($year, -2).$month,
					'card_cvv2'    => $cvv2,
					'is_default'   => $isdefault
				);
				$rt = $users->insertCard($data);
				if($rt['status'] > 0){
					$bank = $banks->getBankByBankId($bankid);
					echo json_encode(
						array(
							'status'  => $rt['status'],
							'code'    => $auth['code'],
							'type'    => $cardtype,
							'cardno'  => substr($bankcardno, -4),
							'picture' => $bank['picture'],
							'name'    => hidestr($name, 'name'),
							'mobile'  => substr($mobile, -4)
						)
					);
					exit;
				}
			}
		}
		echo json_encode(array('status'=>0, 'code'=>$auth['code']));exit;
	}
	
	/**
     * 余额支付方式页面
     */
	public function surplus(){
		$this->isUserLogin();
		
		$sn     = I('post.trade_no');
		$userId = session('user.userid');
		
		$orders = D('Orders');
		$users  = D('Users');
		
		$order = $orders->getOrderBySn($sn);
		$user = $users->getUserAndInfoById($userId);
		
		$this->assign("order",$order);
		$this->assign("user",$user);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('buy');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	/**
     * 余额支付操作
     */
	public function doSurplus(){
		$sn          = I('post.sn');
		$payPassword = I('post.password');
		$userId      = session('user.userid');
		if($userId ==""){
			echo "-1";exit;
        }
		
		$orders   = D('Orders');
		$users    = D('Users');
		
		$order = $orders->getOrderBySn($sn);
		$money = $orders->getOrderMoneyById($order['id']);
		$user  = $users->getUserAndInfoById($order['user_id']);
		if($user['pay_password'] == ''){
			echo "-2";exit; // 请先设置支付密码
		}
		$password = sha1($payPassword);
		
		if($user['pay_password'] != $password){
			echo "-3";exit; // 支付密码错误,请重新输入
		}
		if($user['money'] < $money){
			echo "-4";exit; // 账户余额不足
		}
		if($order['pay_status'] == 0 && $money > 0){
			$rt = $orders->updateOrders($order['id'], array('pay_status'=>1, 'pay_date'=>time(), 'pay_mode'=>4));
			if($rt['status'] > 0){
				// 余额扣除
				$users->decreaseMoney($order['user_id'], $order['id'], $money, 7, '');
				
				// 支付完成后,订单操作
				$orders->successPayOrderOperation($order['id']);
				
				echo json_encode($order);exit; // 支付成功
			}
		}else{
			echo "-5";exit; // 重复支付/支付金额错误
		}
		echo "0";exit; // 未知错误
	}
	
	/**
     * 获取订单商品信息
     */
	private function getOrderBody($orderId){
		$orders = D('Orders');
		$order = $orders->getOrderById($orderId);
		if($order['order_type'] == '0'){
			$products = D('Products');
			$orderProducts = $orders->getOrderProductsByOrderId($orderId);
			$data = array();
			foreach($orderProducts as $vo){
				$data[] = $vo['pro_name'];
			}
			return implode(",",$data);
		}elseif($order['order_type'] == '1'){
			$server = $orders->getOrderServerByOrderId($orderId);
			$ids = array();
			if(!empty($server['relation_ids'])){
				$ids = explode(',', $server['relation_ids']);
			}else{
				$ids[] = $server['relation_id'];
			}
			$repairs = D('Repairs');
			$data = array();
			for($i=0; $i<count($ids);$i++){
				$repair = $repairs->getRepairByRelationId($ids[$i]);
				$data[] = $repair['pro_name'].$repair['plan_name'];
			}
			return implode(",",$data);
		}
		return false;
	}
	
}