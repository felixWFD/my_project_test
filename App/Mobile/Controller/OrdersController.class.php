<?php
namespace Mobile\Controller;
/**
 * 订单控制器
 */
class OrdersController extends BaseController{

	/**
	 * 获取订单列表
	 */
	public function index(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		$us        = D('UserShop');
		$cards     = D('Cards');

		$data = $orders->getOrderList(array("userId"=>$userId, 'type'=>0, "m"=>0, "n"=>10));
		foreach($data as &$vo){
			$schedule = $orders->getOrderScheduleByOrderId($vo['id']);
			$product = $orders->getOrderProductsByOrderId($vo['id']);
			foreach($schedule as &$v){
				// 店铺信息
				if($v['shop_id'] > 0){
					$shop = $us->getShopByShopId($v['shop_id']);
				}
				$v['shop'] = $shop;
				
				// 商品信息
				$newArray = array();
				$i        = 0;
				foreach($product as $pro){
					if($pro['shop_id'] == $v['shop_id']){
						if($pro['is_card'] == '1'){
							$card = $cards->getCardsByObject(array('order_id'=>$vo['id']));
							$newArray[$i]['cards']  = $card;
						}
						$newArray[$i]['show_id']    = $pro['pro_id'];
						$newArray[$i]['show_name']  = $pro['pro_name'];
						$newArray[$i]['show_img']   = '/Public/Product/thumb/'.$pro['list_image'];
						$newArray[$i]['show_desc']  = $products->getAttrNameByAttrId($pro['pro_attr']);
						$newArray[$i]['show_price'] = $pro['pro_price'];
						$newArray[$i]['show_num']   = $pro['pro_number'];
						$newArray[$i]['status']     = $pro['status'];
						$newArray[$i]['is_card']    = $pro['is_card'];
						$newArray[$i]['service']    = $service->getServiceByOrderId($pro['order_id'], $pro['pro_id']);
						$i++;
					}
				}
				$v['product'] = $newArray;
			}
			unset($v);
			$vo['schedule'] = $schedule;	
		}
		unset($vo);

		$this->assign("orders", $data);
		$this->display();
	}
	
	/**
	 * 加载更多订单列表
	 */
	public function loadMoreShopping(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		$us        = D('UserShop');
		$cards     = D('Cards');

		$data = $orders->getOrderList(array("userId"=>$userId, 'type'=>0, "m"=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$schedule = $orders->getOrderScheduleByOrderId($vo['id']);
			$product = $orders->getOrderProductsByOrderId($vo['id']);
			foreach($schedule as &$v){
				// 店铺信息
				if($v['shop_id'] > 0){
					$shop = $us->getShopByShopId($v['shop_id']);
				}
				$v['shop'] = $shop;
				
				// 商品信息
				$newArray = array();
				$i        = 0;
				foreach($product as $pro){
					if($pro['shop_id'] == $v['shop_id']){
						if($pro['is_card'] == '1'){
							$card = $cards->getCardsByObject(array('order_id'=>$vo['id']));
							$newArray[$i]['cards']  = $card;
						}
						$newArray[$i]['show_id']    = $pro['pro_id'];
						$newArray[$i]['show_name']  = $pro['pro_name'];
						$newArray[$i]['show_img']   = '/Public/Product/thumb/'.$pro['list_image'];
						$newArray[$i]['show_desc']  = $products->getAttrNameByAttrId($pro['pro_attr']);
						$newArray[$i]['show_price'] = $pro['pro_price'];
						$newArray[$i]['show_num']   = $pro['pro_number'];
						$newArray[$i]['status']     = $pro['status'];
						$newArray[$i]['is_card']    = $pro['is_card'];
						$newArray[$i]['service']    = $service->getServiceByOrderId($pro['order_id'], $pro['pro_id']);
						$i++;
					}
				}
				$v['product'] = $newArray;
			}
			unset($v);
			$vo['schedule'] = $schedule;
			$vo['order_date'] = date("m-d", $vo['order_date']);
		}
		unset($vo);
		echo json_encode($data);exit;
	}
	
	/**
	 * 维修订单列表
	 */
	public function repair(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$shippings = D('Shippings');

		$data = $orders->getOrderList(array("userId"=>$userId, 'type'=>1, "m"=>0, "n"=>8));
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['new_price'] !='0.00'){
				$money = $server['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$vo['show_name'] = $relation['pro_name']." ".$server['name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$relation['list_image'];
				$vo['show_desc'] = $relation['plan_name'];
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price']    = $money;
			$vo['shop_id']       = $server['shop_id'];
			$vo['server_status'] = $server['server_status'];
			$vo['server_way']    = $server['server_way'];
			$vo['shipping_num']  = $server['shipping_num'];
		}
		unset($vo);
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('shipping', $shipping);
		$this->assign("title", "维修订单");
		$this->display();
	}
	
	/**
	 * 加载更多维修订单列表
	 */
	public function loadMoreRepair(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');

		$data = $orders->getOrderList(array("userId"=>$userId, 'type'=>1, "m"=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['new_price'] !='0.00'){
				$money = $server['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$vo['show_name'] = $relation['pro_name']." ".$server['name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$relation['list_image'];
				$vo['show_desc'] = $relation['plan_name'];
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price']    = $money;
			$vo['shop_id']       = $server['shop_id'];
			$vo['server_status'] = $server['server_status'];
			$vo['server_way']    = $server['server_way'];
			$vo['shipping_num']  = $server['shipping_num'];
			$vo['order_date']    = date("m-d", $vo['order_date']);
		}
		unset($vo);
		echo json_encode($data);exit;
	}
	
	/**
	 * 回收订单列表
	 */
	public function recovery(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$shippings = D('Shippings');
		$cat       = D('ProductCat');
		
		$data = $orders->getOrderList(array("userId"=>$userId, 'type'=>2, "m"=>0, "n"=>8));
		foreach($data as &$vo){
			$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
			if($recovery['new_price'] !='0.00'){
				$money = $recovery['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($recovery['reco_id'] > 0){
				$repair = $repairs->getRepairById($recovery['reco_id']);
				$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				$type     = $cat->getCat($repair['type_id']);
				$brand    = $cat->getCat($repair['brand_id']);
				$vo['show_name'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$repair['list_image'];
				$vo['show_desc'] = $attrs;
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price']   = $money;
			$vo['shop_id']      = $recovery['shop_id'];
			$vo['reco_status']  = $recovery['reco_status'];
			$vo['reco_way']     = $recovery['reco_way'];
			$vo['shipping_num'] = $recovery['shipping_num'];
		}
		unset($vo);

		// 物流库
        $shipping = $shippings->getCommonShippingCompany();

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('shipping', $shipping);
		$this->assign("title", "回收订单");
		$this->display();
	}
	
	/**
	 * 加载更多回收订单列表
	 */
	public function loadMoreRecovery(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$cat     = D('ProductCat');

		$data = $orders->getOrderList(array("userId"=>$userId, 'type'=>2, "m"=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
			if($recovery['new_price'] !='0.00'){
				$money = $recovery['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($recovery['reco_id'] > 0){
				$repair = $repairs->getRepairById($recovery['reco_id']);
				$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				$type     = $cat->getCat($repair['type_id']);
				$brand    = $cat->getCat($repair['brand_id']);
				$vo['show_name'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$repair['list_image'];
				$vo['show_desc'] = $attrs;
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['show_price']   = $money;
			$vo['shop_id']      = $recovery['shop_id'];
			$vo['reco_status']  = $recovery['reco_status'];
			$vo['reco_way']     = $recovery['reco_way'];
			$vo['shipping_num'] = $recovery['shipping_num'];
			$vo['order_date']   = date("m-d", $vo['order_date']);
		}
		unset($vo);
		echo json_encode($data);exit;
	}
	
	/**
	 * 商城订单详情
	 */
	public function odetail(){
		$sn     = I('get.sn');
		$userId = session('user.userid');
		$shopId = session('shop.id');
	
		$orders    = D('Orders');
		$products  = D('Products');
		$shippings = D('Shippings');
		$cards     = D('Cards');
		$service   = D('Service');
		$us        = D('UserShop');
		
		$order    = $orders->getOrderBySn($sn);
		$schedule = $orders->getOrderScheduleByOrderId($order['id']);
		$product  = $orders->getOrderProductsByOrderId($order['id']);
		if(empty($order)){
			$this->error('操作失败(价格变更/订单号错误?)', U('Orders/index'));
		}
		$ids = array();
		foreach($schedule as &$vo){
			// 店铺信息
			if($vo['shop_id'] > 0){
				$ids[] = $vo['shop_id'];
				$shop  = $us->getShopByShopId($vo['shop_id']);
			}
			$vo['shop'] = $shop;
			
			// 商品信息
			$newArray = array();
			$i        = 0;
			foreach($product as $v){
				if($v['shop_id'] == $vo['shop_id']){
					if($v['is_card'] == '1'){
						$card = $cards->getCardsByObject(array('order_id'=>$order['id']));
						$newArray[$i]['cards']  = $card;
					}
					$newArray[$i]['show_id']    = $v['pro_id'];
					$newArray[$i]['show_name']  = $v['pro_name'];
					$newArray[$i]['show_img']   = '/Public/Product/thumb/'.$v['list_image'];
					$newArray[$i]['show_desc']  = $products->getAttrNameByAttrId($v['pro_attr']);
					$newArray[$i]['show_price'] = $v['pro_price'];
					$newArray[$i]['show_num']   = $v['pro_number'];
					$newArray[$i]['status']     = $v['status'];
					$newArray[$i]['service']    = $service->getServiceByOrderId($v['order_id'], $v['pro_id']);
					$i++;
				}
			}
			$vo['product'] = $newArray;
			
			// 物流信息
			$shipping = $shippings->getShippingCompanyById($vo['shipping_way']);
			$vo['cname'] = $shipping['cname'];
			
			$data = array();
			if(!empty($vo['shipping_num'])){
				$expresss = $shippings->getExpressByNumber($vo['shipping_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $k=>$c){
					$data[$k]['time']    = $c['time']; 
					$data[$k]['context'] = $c['context'];
				}
			}
			$vo['shipping'] = $data;
		}
		unset($vo);
		
		if($order['user_id'] != $userId && !in_array($shopId, $ids)){
			$this->error('非法操作', U('Orders/index'));
		}
		
		$this->assign("order", $order);
		$this->assign("schedule", $schedule);
		$this->display();
	}
	
	/**
	 * 维修订单详情
	 */
	public function rdetail(){
		$sn     = I('get.sn');
		$userId = session('user.userid');
	
		$us        = D('UserShop');
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$shippings = D('Shippings');
		$cat       = D('ProductCat');
		
		$order  = $orders->getOrderBySn($sn);
		$server = $orders->getOrderServerByOrderId($order['id']);
		if($server['new_price'] !='0.00'){
			$money = $server['new_price'];
		}else{
			$money = $order['deail_price'];
		}
		$order['relation_ids'] = $server['relation_ids'];
		$order['shop_remarks'] = $server['shop_remarks'];
		$order['warranty'] = $server['warranty'];
		$order['money'] = $money;
		
		$ids  = array();
		$test = array();
		if(!empty($server['relation_ids'])){
			$ids = explode(',', $server['relation_ids']);
		}else{
			$ids[] = $server['relation_id'];
		}
		for($i=0; $i<count($ids);$i++){
			$repair = $repairs->getRepairByRelationId($ids[$i]);
			$test[] = $repair['plan_name'];
		}
		$order['test'] = $test;
		
		if($server['relation_id'] > 0){
			$relation = $repairs->getRepairByRelationId($server['relation_id']);
			$type     = $cat->getCat($relation['type_id']);
			$brand    = $cat->getCat($relation['brand_id']);
			$order['type_name']  = $type['cat_name'];
			$order['brand_name'] = $brand['cat_name'];
			$order['model_name'] = $relation['pro_name'];
			$order['color_name'] = $server['name'];
			$order['plan_name']  = $relation['plan_name'];
			
		}
		$order['server_status'] = $server['server_status'];
		$order['server_way']    = $server['server_way'];
		
		// 寄回物流公司信息
		$shipping = $shippings->getShippingCompanyById($server['return_way']);
		$order['return_name'] = $shipping['cname'];
		$order['return_num']  = $server['return_num'];
		// 寄回物流信息
		$data = array();
		if(!empty($order['return_num'])){
			$expresss = $shippings->getExpressByNumber($server['return_num']);
			$content = CBWOjectArray(json_decode($expresss['content']));
			foreach($content as $key=>$vo){
				$data[$key]['time']    = $vo['time']; 
				$data[$key]['context'] = $vo['context'];
			}
		}
		$order['return'] = $data;
		
		// 寄出物流公司信息
		$shipping = $shippings->getShippingCompanyById($server['shipping_way']);
		$order['shipping_name'] = $shipping['cname'];
		$order['shipping_num']  = $server['shipping_num'];
		// 寄出物流信息
		$data = array();
		if(!empty($server['shipping_num'])){
			$expresss = $shippings->getExpressByNumber($server['shipping_num']);
			$content = CBWOjectArray(json_decode($expresss['content']));
			foreach($content as $key=>$vo){
				$data[$key]['time']    = $vo['time']; 
				$data[$key]['context'] = $vo['context'];
			}
		}
		$order['shipping'] = $data;

		// 报价列表
		$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
		foreach($offer as &$vo){
			$shop = $us->getShopByShopId($vo['shop_id']);
			$distance = CBWGetDistance($shop['lng'], $shop['lat'], $server['lng'], $server['lat']);
			$vo['distance']   = number_format($distance/1000, 2, '.', '');
			$vo['shop_name']  = $shop['shop_name'];
			$vo['user_name']  = $shop['user_name'];
			$vo['user_phone'] = $shop['user_phone'];
			$vo['address']    = $shop['city_addr'].$shop['addr'].$shop['address'];
			$vo['logo_image'] = $shop['logo_image'];
		}
		unset($vo);

		$this->assign("order", $order);
		$this->assign("offer", $offer);
		$this->assign("userId", $userId);
		$this->assign("title", "维修订单详情");
		$this->display();
	}
	
	/**
	 * 更新订单中标店铺
	 */
	public function updateOfferShop(){
		echo 1;exit;
		$orderId = I('post.id');
		$shopId  = I('post.sid');
		$userId  = session('user.userid');
		
		$orders = D('Orders');
		
		$order = $orders->getOrderById($orderId);
		if($order['order_status'] == 1 && $order['is_offer'] == 1){
			$offer = $orders->getOrderOfferByOrderId($orderId, 0, 5);
			foreach($offer as $vo){
				if($vo['selected'] == 1){
					if($vo['shop_id'] == $shopId){
						echo 0;exit;
					}else{
						$orders->updateOrderOffer($vo['id'], array('selected'=>0));
					}
				}else{
					if($vo['shop_id'] == $shopId){
						$orders->updateOrderOffer($vo['id'], array('selected'=>1));
					}
				}
			}
			if($order['order_type'] == 1){
				$orders->updateOrderServer($orderId, array('shop_id'=>$shopId));
			}elseif($order['order_type'] == 2){
				$orders->updateOrderRecovery($orderId, array('shop_id'=>$shopId));
			}
			echo 1;exit;
		}
		echo 0;exit;
	}
	
	/**
	 * 回收订单详情
	 */
	public function redetail(){
		$sn = I('get.sn');
		$userId = session('user.userid');
	
		$us        = D('UserShop');
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$shippings = D('Shippings');
		$cat       = D('ProductCat');
		
		$order    = $orders->getOrderBySn($sn);
		$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
		if($recovery['new_price'] !='0.00'){
			$money = $recovery['new_price'];
		}else{
			$money = $recovery['deail_price'];
		}
		if($recovery['reco_id'] > 0){
			$repair = $repairs->getRepairById($recovery['reco_id']);
			$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
			$type     = $cat->getCat($repair['type_id']);
			$brand    = $cat->getCat($repair['brand_id']);
			$order['attrs']      = $attrs;
			$order['type_name']  = $type['cat_name'];
			$order['brand_name'] = $brand['cat_name'];
			$order['pro_name']   = $repair['pro_name'];
		}
		$order['reco_status'] = $recovery['reco_status'];
		$order['reco_way']    = $recovery['reco_way'];
		$order['reco_pay']    = $recovery['reco_pay'];

		// 报价列表
		$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
		foreach($offer as &$vo){
			$shop = $us->getShopByShopId($vo['shop_id']);
			$distance = CBWGetDistance($shop['lng'], $shop['lat'], $recovery['lng'], $recovery['lat']);
			$vo['distance']   = number_format($distance/1000, 2, '.', '');
			$vo['shop_name']  = $shop['shop_name'];
			$vo['user_name']  = $shop['user_name'];
			$vo['user_phone'] = $shop['user_phone'];
			$vo['address']    = $shop['city_addr'].$shop['addr'].$shop['address'];
			$vo['logo_image'] = $shop['logo_image'];
		}
		unset($vo);

		// 寄回物流公司信息
		$shipping = $shippings->getShippingCompanyById($recovery['return_way']);
		$order['return_name'] = $shipping['cname'];
		$order['return_num']  = $recovery['return_num'];
		// 寄回物流信息
		$data = array();
		if(!empty($order['return_num'])){
			$expresss = $shippings->getExpressByNumber($recovery['return_num']);
			$content = CBWOjectArray(json_decode($expresss['content']));
			foreach($content as $key=>$vo){
				$data[$key]['time']    = $vo['time']; 
				$data[$key]['context'] = $vo['context'];
			}
		}
		$order['return'] = $data;
		
		// 寄出物流公司信息
		$shipping = $shippings->getShippingCompanyById($recovery['shipping_way']);
		$order['shipping_name'] = $shipping['cname'];
		$order['shipping_num']  = $recovery['shipping_num'];
		// 寄出物流信息
		$data = array();
		if(!empty($recovery['shipping_num'])){
			$expresss = $shippings->getExpressByNumber($recovery['shipping_num']);
			$content = CBWOjectArray(json_decode($expresss['content']));
			foreach($content as $key=>$vo){
				$data[$key]['time']    = $vo['time']; 
				$data[$key]['context'] = $vo['context'];
			}
		}
		$order['shipping'] = $data;
		
		$this->assign("order", $order);
		$this->assign("offer", $offer);
		$this->assign("userId", $userId);
		$this->assign("title", "回收订单详情");
		$this->display();
	}
	
	/**
	 * 物流信息
	 */
	public function shipping(){
		$way = I('get.way');
		$num = I('get.num');
		
		$shippings = D('Shippings');
		
		if($way > 0 && !empty($num)){
			$shipping = $shippings->getShippingCompanyById($way);
			$data     = array();
			$expresss = $shippings->getExpressByNumber($num);
			$content  = CBWOjectArray(json_decode($expresss['content']));
			foreach($content as $k=>$c){
				$data[$k]['time']    = $c['time']; 
				$data[$k]['context'] = $c['context'];
			}
			$shipping['list'] = $data;
		}

		$this->assign("shipping", $shipping);
		$this->assign("num", $num);
		$this->display();
	}
	
	/**
	 * 关闭交易
	 */
	public function close(){
		$this->isUserLogin();
		
		$userId  = session('user.userid');
		$orderId = I('get.id');

		$orders = D('Orders');
		$users  = D('Users');
		$us     = D('UserShop');
		
		$order = $orders->getOrderById($orderId);
		if($order['user_id'] == $userId){
			$rt = $orders->closeOrders($orderId);
			if($rt['status'] > 0){
				if($order['order_type'] == '1'){
					$server = $orders->getOrderServerByOrderId($order['id']);
					$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
					if(!empty($offer)){
						foreach($offer as $vo){
							if($vo['is_free'] == 0 && $vo['virtual_money'] > 0){
								$shop = $us->getShopByShopId($vo['shop_id']);
								$users->giveVirtual($shop['user_id'], $vo['virtual_money'], 6, $order['order_sn']);      // 增加草包豆
							}
						}
					}
					$this->redirect('Orders/repair');exit;
				}else if($order['order_type'] == '2'){
					$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
					$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
					if(!empty($offer)){
						foreach($offer as $vo){
							if($vo['is_free'] == 0 && $vo['virtual_money'] > 0){
								$shop = $us->getShopByShopId($vo['shop_id']);
								$users->giveVirtual($shop['user_id'], $vo['virtual_money'], 6, $order['order_sn']);      // 增加草包豆
							}
						}
					}
					$this->redirect('Orders/recovery');exit;
				}
				$this->redirect('Orders/index');exit;
			}else{
				$this->error('关闭失败');
			}
		}else{
			$this->error('关闭失败(非法账户)');
		}
	}
	
	/**
	 * 线下支付
	 */
	public function linePay(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$sn     = I('get.trade_no');
		
		$orders = D('Orders');
		
		$order  = $orders->getOrderBySn($sn);
		
		// 变更服务状态
		$orders->updateOrderRecovery($order['id'], array('reco_status'=>3));
		// 变更订单状态
		$orders->updateOrders($order['id'], array('ok_date'=>time(),'order_status'=>2));
		
		$this->redirect('Orders/redetail', array('sn'=>$order['order_sn']));
	}
	
	/**
	 * 提取卡密
	 */
	public function extracts(){
		$this->isUserLogin();
		
		$userId  = session('user.userid');
		$orderId = I('get.id');
		
		$orders  = D('Orders');
		$cards   = D('Cards');
		$service = D('Service');
		
		$order  = $orders->getOrderById($orderId);
		$number = $cards->getCardsCountByObject(array('order_id'=>$order['id']));

		if($order['order_status'] == 1){
			if($order['user_id'] == $userId){
				if($number == 0){
					$products = $orders->getOrderProductsByOrderId($order['id']);
					$i = 0;
					foreach($products as $vo){
						if($vo['is_card'] == 1){
							$iservice = $service->getServiceByOrderId($order['id'], $vo['pro_id']);
							if(empty($iservice)){
								$card = $cards->getCardsByObject(array('order_id'=>0,'m'=>0, 'n'=>$vo['pro_number']));
								if(count($card) < $vo['pro_number']){
									$this->error('提取失败,卡密库存不足');exit;
								}
								foreach($card as $v){
									$cards->updateCards($v['id'], array('user_id'=>$order['user_id'], 'order_id'=>$order['id']));
								}
							}else{
								$this->error('提取失败');exit;
							}
						}
						$i++;
					}
					if($i == 1){
						$orders->updateOrders($order['id'], array('ok_date'=>time(), 'send_date'=>time(), 'order_status'=>5));
					}
					$this->redirect('Orders/odetail', array('sn'=>$order['order_sn']));
				}else{
					$this->error('已提取');
				}
			}else{
				$this->error('用户有误,');
			}
		}else{
			$this->error('订单有误');
		}
	}
	
	/**
	 * 收货
	 */
	public function sign(){
		$this->isUserLogin();
		
		$userId  = session('user.userid');
		$orderId = I('get.id');
		$shopId  = I('get.sid');
		$virtual = 0;
		
		$orders   = D('Orders');
		$users    = D('Users');
		$us       = D('UserShop');
		$products = D('Products');
		
		$order = $orders->getOrderById($orderId);
		if($order['order_status'] == 1){
			if($order['user_id'] == $userId){
				if($order['order_type'] == '0'){
					$rt = $orders->updateOrderSchedule($orderId, $shopId, array('send_date'=>time(), 'shop_status'=>3));
					if($rt['status'] > 0){
						$schedule = $orders->getOrderScheduleByOrderId($orderId);
						$product  = $orders->getOrderProductsByOrderId($orderId);
						$i        = 0;
						$j        = 0;
						foreach($schedule as $vo){
							if($vo['shop_status'] == 3){
								$i++;
							}
							if($vo['shop_id'] == $shopId){
								foreach($product as $v){
									if($vo['shop_id'] == $v['shop_id']){
										if($v['virtual_money'] > 0){
											$virtual += $v['virtual_money'];
										}
										$products->statistics($v['pro_id'], 'sale');
									}
								}
								
								// 解除该订单店铺冻结金额
								if($shopId > 0){
									$shop = $us->getShopByShopId($shopId);
									$users->removeFrozenMoney(0,  $shop['user_id'], $orderId);
								}
							}
							$j++;
						}
						if($i == $j){
							$orders->updateOrders($orderId, array('ok_date'=>time(), 'order_status'=>2));
						}
						if($virtual > 0){
							$users->giveVirtual($userId, $virtual, 1, $order['order_sn']);
						}
						$this->redirect('Orders/index');
					}else{
						$this->error('收货失败');
					}
				}elseif($order['order_type'] == '1'){
					$rt = $orders->updateOrders($orderId, array('ok_date'=>time(), 'order_status'=>5));
					if($rt['status'] > 0){
						$server = $orders->getOrderServerByOrderId($order['id']);
						if($server['new_price'] !='0.00'){
							$money = $server['new_price'];
						}else{
							$money = $order['deail_price'];
						}
						$virtual = round($money/2);
						if($virtual > 0){
							$users->giveVirtual($userId, $virtual, 1, $order['order_sn']);
						}
						$this->redirect('Orders/index');
					}else{
						$this->error('收货失败');
					}
				}
			}else{
				$this->error('收货失败(非法账户)');
			}
		}else{
			$this->error('订单状态异常');
		}
	}
	
	/**
	 * 发货
	 */
	public function send(){
		$this->isUserLogin();
		
		$userId  = session('user.userid');
		$orderId = I('post.id');
		$way     = I('post.shipping_way');
		$number  = trim(I('post.shipping_num'));
		
		$orders = D('Orders');
		
		$order = $orders->getOrderById($orderId);
		if($order['user_id'] == $userId){
			$rt = 0;
			if($order['order_type'] == '1'){
				$rt = $orders->updateOrderServer($order['id'], array('shipping_way'=>$way, 'shipping_num'=>$number));
			}else{
				$rt = $orders->updateOrderRecovery($order['id'], array('shipping_way'=>$way, 'shipping_num'=>$number));
			}
			if($rt['status'] > 0){
				//快递100 订阅请求
				$shippings = D('Shippings');
				$shipping = $shippings->getShippingCompanyById($way);
				kd100($shipping['com'], $number);
				
				if($order['order_type'] == '1'){
					$this->redirect('Orders/repair');
				}else{
					$this->redirect('Orders/recovery');
				}
			}else{
				$this->error('发货失败');
			}
		}else{
			$this->error('非法账户');
		}
	}
	
	/**
	 * 评价
	 */
	public function comment(){
		$this->isUserLogin();
		$orderSn = I('get.sn');
		
		$orders   = D('Orders');
		$products = D('Products');
		$repairs  = D('Repairs');
		
		$order = $orders->getOrderBySn($orderSn);
		if($order['order_type'] == '0'){       // 购物订单
			$orderProducts = $orders->getOrderProductsByOrderId($order['id']);
			$list = array();
			foreach($orderProducts as $k=>$v){
				$list[$k]['show_id']    = $v['pro_id'];
				$list[$k]['show_name']  = $v['pro_name'];
				$list[$k]['show_img']   = '/Public/Product/thumb/'.$v['list_image'];
				$list[$k]['show_desc']  = $products->getAttrNameByAttrId($v['pro_attr']);
				$list[$k]['status']     = $v['status'];
			}
			$order['list'] = $list;
		}elseif($order['order_type'] == '1'){  // 维修订单
			$server = $orders->getOrderServerByOrderId($order['id']);
			$relation = $repairs->getRepairByRelationId($server['relation_id']);
			$list = array();
			$list['show_name']  = $relation['pro_name']." ".$server['name'];
			$list['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
			$list['show_desc']  = $relation['plan_name'];
			$list['status']     = 1;
			$order['list'][0] = $list;
		}elseif($order['order_type'] == '2'){  // 回收订单
			$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
			$repair = $repairs->getRepairById($recovery['reco_id']);
			if($recovery['new_attr'] !=''){
				$attrs = $repairs->getAttribute($recovery['new_attr']);
			}else{
				$attrs = $repairs->getAttribute($recovery['reco_attr']);
			}
			$list = array();
			$list['show_name']  = $repair['pro_name'];
			$list['show_img']   = '/Public/Repair/thumb/'.$repair['list_image'];
			$list['show_desc']  = implode("|",$attrs);
			$list['status']     = 1;
			$order['list'][0] = $list;
		}

		$this->assign("order", $order);
		$this->display();
	}
	
	/**
	 * 提交评价
	 */
	public function submitComment(){
		$this->isUserLogin();
		$userId   = session('user.userid');
		$orderId  = I('post.id');
		$score    = I('post.score');
		$ids      = I('post.ids');
		$contents = I('post.content');
		$give     = 0;
		
		$orders   = D('Orders');
		$comments = D('Comments');
		$products = D('Products');
		$us       = D('UserShop');
		
		$order = $orders->getOrderById($orderId);
		if($order['is_comment'] == 1){
			$this->error('重复评价');
		}
		if($order['user_id'] == $userId){
			foreach($contents as $key=>$content){
				if(strlen($content) > 20){
					$give = 1;
				}
				$productId = empty($ids[$key])?0:$ids[$key];
				$shopId = 0;
				if($order['order_type'] == 0){
					$relation = $products->getRelationByRelationId($productId);
					$shopId = $relation['shop_id'];
				}elseif($order['order_type'] == 1){
					$server = $orders->getOrderServerByOrderId($order['id']);
					$shopId = $server['shop_id'];
				}elseif($order['order_type'] == 2){
					$server = $orders->getOrderRecoveryByOrderId($order['id']);
					$shopId = $server['server_id'];
				}
				$data = array();
				$data['order_id']     = $orderId;
				$data['shop_id']      = $shopId;
				$data['pro_id']       = $productId;
				$data['user_id']      = $userId;
				$data['score']        = $score;
				$data['content']      = $content;
				$data['comment_time'] = time();
				$comments->insertComment($data);
				
				if($order['order_type'] == 0){
					$products->statistics($productId, 'comment');
				}
			}
			if($give){
				// 赠送虚拟币
				$users = D('Users');
				$users->giveVirtual($userId, 25, 5, $order['order_sn']);
			}
			if($order['order_type'] == 1 || $order['order_type'] == 2){
				// 维修/回收订单,好评+3;中评+1;差评-3
				if($score[0] == 1){
					$type   = 1;
					$score2 = 3;
				}elseif($score[0] == 2){
					$type   = 1;
					$score2 = 1;
				}elseif($score[0] == 3){
					$type   = 0;
					$score2 = 3;
				}
				$us->updateShopScore($shopId, $type, $score2);
			}
			$orders->updateOrders($orderId, array('is_comment'=>1));
			$this->redirect('Orders/succeed');
		}else{
			$this->error('评价失败(非法账户)');
		}
	}
	
	/**
	 * 评价提交后页面
	 */
	public function succeed(){
		$this->isUserLogin();
		$this->display();
	}
}