<?php
namespace Mobile\Controller;
/**
 * 视频控制器
 */
class VideoController extends BaseController{
	
    /**
	 * 视频列表
	 * 
	 */
    public function index(){
		$cat   = D('ProductCat');
		$video = D('Video');
		
        // 查询视频的条件
		$typeId    = I('get.type');
		$brandId   = I('get.brand');
		$modelId   = I('get.model');

		// 分类列表
		$typeRes = $cat->getCatListById(5);
		if(!empty($typeId)){
			$brandRes = $cat->getCatListById($typeId);
		}
		if(!empty($brandId)){
			$catRes = $cat->getCatListById($brandId);
		}
		$videoRes = $video->getVideoList(array('typeId'=>$typeId, 'brandId'=>$brandId, 'modelId'=>$modelId, 'm'=>0, 'n'=>8));

		$this->assign('type', $typeId);
        $this->assign('brand', $brandId);
		$this->assign('model', $modelId);
        $this->assign('videoRes', $videoRes);
		$this->assign('typeRes', $typeRes);
		$this->assign('brandRes', $brandRes);
		$this->assign('catRes', $catRes);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('video');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
        $this->display();
    }
	
	/**
	 * 加载视频品牌
	 * 
	 */
	public function loadVideoBrand(){
		$typeId = I('post.type');
		
		$cat = D('ProductCat');
		
		$data = $cat->getCatListById($typeId);
		echo json_encode($data);
	}
	
	/**
	 * 加载视频型号
	 * 
	 */
	public function loadVideoModel(){
		$brandId = I('post.brand');
		
		$cat = D('ProductCat');
		
		$data = $cat->getCatListById($brandId);
		echo json_encode($data);
	}
	
	/**
	 * 加载更多视频
	 * 
	 */
	public function loadMoreVideo(){
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		$typeId  = I('post.type');
		$brandId = I('post.brand');
		$modelId = I('post.model');
		
		$video = D('Video');
		
		$data = $video->getVideoList(array('typeId'=>$typeId, 'brandId'=>$brandId, 'modelId'=>$modelId, 'm'=>$counter*$num, 'n'=>$num));
		echo json_encode($data);
	}
	
	/**
	 * 视频详情
	 * 
	 */
    public function detail(){
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$id      = I('get.id');
		
		$video = D('Video');
		
		// 增加点击数
		$video->statistics($id);
		
		// 视频信息
		$data = $video->getVideo($id);
		$this->assign('video',$data);
		
		// 同分类下其他视频
		$videoRes = $video->getVideoByCat($data['id'], $data['cat_id']);
		$this->assign('videoRes',$videoRes);
		
		// 相关商品
		$products = D('Products');
		$productRes = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'typeId'=>$data['type_id'], 'groupby'=>1, 'sort'=>'new', 'm'=>0, 'n'=>4));
		$this->assign('productRes',$productRes);
		
		// SEO
		$this->assign('title', $data['title']);
		$this->assign('keywords', $data['keywords']);
		$this->assign('description', $data['description']);
		
        $this->display();
    }

}