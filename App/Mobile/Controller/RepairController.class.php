<?php
namespace Mobile\Controller;
/**
 * 维修控制器
 */
class RepairController extends BaseController{
	
	/**
	 * 第一步 选择类型
	 */
	public function select(){
		//清除SESSION
		session('server', null);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('repair');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	/**
	 * 第二部 选择品牌
	 */
	public function two(){
		//清除SESSION
		session('server', null);
		$cat     = D('ProductCat');
		$repairs = D('Repairs');
		$us      = D('UserShop');
		
		$typeId = I('get.type');
		
		if(empty($typeId)){
			$this->redirect("Repair/select");
		}
		
		$brandRes = $cat->getCatListById($typeId);
		
		// 面包屑导航数据
		$step = $repairs->steps($typeId);

		$this->assign('step',$step);
		$this->assign('brandRes',$brandRes);
		$this->assign('typeId',$typeId);
		
		// 保存SESSION
		session('server.typeId', $typeId);
		
		//SEO
		foreach($brandRes as $vo){
			$keyword[] = $vo['cat_name'].$step['type']."维修";
		}
        $this->assign("title","专业修".$step['type']."、草包网".$step['type']."维修平台");
        $this->assign("keywords",implode(",", $keyword));
        $this->assign("description","草包网支持国内外主流".$step['type']."快修服务，维修方式为：上门服务，到店服务，邮寄维修服务，自助维修服务.");
		
		$this->display();
	}
	
	/**
	 * 第三步 选择型号
	 */
	public function three(){
		//清除SESSION
		session('server', null);
		
		$repairs = D('Repairs');
		$us      = D('UserShop');
		
		$typeId   = I('get.type');
		$brandId  = I('get.brand');
		$serverId = I('get.id');
		
		if(empty($typeId)){
			$this->redirect("Repair/select");
		}else{
			$repairRes = $repairs->getRepairByBrandId($brandId);
		}
		// 面包屑导航数据
		$step = $repairs->steps($typeId, $brandId);

		$this->assign('step',$step);
		$this->assign('repairRes',$repairRes);
		$this->assign('typeId',$typeId);
		$this->assign('brandId',$brandId);

		// 保存SESSION
		session('server.typeId', $typeId);
		session('server.brandId', $brandId);
		
		//SEO
        $this->assign("title",$step['brand'].$step['type']."维修报价_".$step['brand'].$step['type']."维修价格表");
        $this->assign("keywords",$step['brand'].$step['type']."维修报价,".$step['brand'].$step['type']."维修价格表");
        $this->assign("description","提供最专业的".$step['brand'].$step['type']."维修服务,".$step['brand'].$step['type']."维修报价,".$step['brand'].$step['type']."维修价格表。支持全国上门、邮寄到店三种服务方式。维修".$step['brand'].$step['type']."上草包网，统一真实报价、安全可靠、原厂配件。");
		
		$this->display();
	}
	
	/**
	 * 第四步 选择颜色
	 */
	public function four(){
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$shopId  = I('get.sid');
		$server  = session('server');
		
		$repairs = D('Repairs');
		$us      = D('UserShop');
		
		// 指定店铺维修
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			if(!empty($shop)){
				session('server.shopId', $shopId);
			}
		}
		
		if(empty($modelId) || empty($server)){
			$this->redirect("Repair/select");
		}else{
			$colorRes = $repairs->getColorByRepairId($modelId);
		}

		// 面包屑导航数据
		$step = $repairs->steps($typeId, $brandId, $modelId);

		$this->assign('step',$step);
		$this->assign('colorRes',$colorRes);
		$this->assign('typeId',$typeId);
		$this->assign('brandId',$brandId);
		$this->assign('modelId',$modelId);
		
		// 保存SESSION
		session('server.typeId',  $typeId);
		session('server.brandId', $brandId);
		session('server.modelId', $modelId);
		
		//SEO
        $this->assign("title",$step['model']."维修价格_".$step['model']."维修多少钱");
        $this->assign("keywords",$step['model']."维修价格,".$step['model']."维修多少钱");
        $this->assign("description","提供最专业的".$step['model'].$step['type']."维修服务,查询".$step['model'].$step['type']."维修报价,".$step['model'].$step['type']."维修多少钱。支持全国上门、邮寄到店三种服务方式，维修".$step['model'].$step['type']."上草包网。");
		
		$this->display();
	}
	
	/**
	 * 第五步 选择故障分类
	 */
	public function five(){
		$repairs = D('Repairs');
		
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$colorId = I('get.color');
		$server  = session('server');
		
		if(empty($colorId) || empty($server)){
			$this->redirect("Repair/select");
		}
		
		$catRes = $repairs->getCatByTypeId($typeId, $modelId);
		
		// 面包屑导航数据
		$step = $repairs->steps($typeId, $brandId, $modelId, $colorId);

		$this->assign('step',$step);
		$this->assign('catRes',$catRes);
		$this->assign('typeId',$typeId);
		$this->assign('brandId',$brandId);
		$this->assign('modelId',$modelId);
		$this->assign('colorId',$colorId);
		
		// 保存SESSION
		session('server.colorId', $colorId);
		
		//SEO
        $this->assign("title",$step['color'].'色'.$step['model']."维修价格_".$step['color'].'色'.$step['model']."维修多少钱");
        $this->assign("keywords",$step['color'].'色'.$step['model']."维修价格,".$step['color'].'色'.$step['model']."维修多少钱");
        $this->assign("description","提供最专业的".$step['color'].'色'.$step['model'].$step['type']."维修服务,查询".$step['color'].'色'.$step['model'].$step['type']."维修报价,".$step['color'].'色'.$step['model'].$step['type']."维修多少钱。支持全国上门、邮寄到店三种服务方式，维修".$step['color'].'色'.$step['model'].$step['type']."上草包网。");
		
		$this->display();
	}
	
	/**
	 * 第六步 选择故障
	 */
	public function six(){
		$repairs = D('Repairs');
		$us      = D('UserShop');
		
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$colorId = I('get.color');
		$catId   = I('get.cat');
		$server  = session('server');
		
		if(empty($catId) || empty($server)){
			$this->redirect("Repair/select");
		}else{
			$faultRes = $repairs->getFaultByModelIdAndCatId($modelId, $catId);
		}

		// 面包屑导航数据
		$step = $repairs->steps($typeId, $brandId, $modelId, $colorId, $catId);

		$this->assign('step',$step);
		$this->assign('faultRes',$faultRes);
		$this->assign('typeId',$typeId);
		$this->assign('brandId',$brandId);
		$this->assign('modelId',$modelId);
		$this->assign('colorId',$colorId);
		$this->assign('catId',$catId);
		
		// 保存SESSION
		session('server.catId', $catId);
		
		//SEO
		foreach($faultRes as $vo){
			$faults[] = $vo['fault_name'];
		}
        $this->assign("title",$step['color'].'色'.$step['model'].implode("_" , $faults)."维修价格_多少钱");
        $this->assign("keywords",$step['color'].'色'.$step['model'].implode("_" , $faults)."维修价格,".$step['color'].'色'.$step['model'].implode("_" , $faults)."维修多少钱");
        $this->assign("description","提供最专业的".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."维修服务,查询".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."维修报价,".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."维修多少钱。支持全国上门、邮寄到店三种服务方式，维修".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."上草包网。");
		
		$this->display();
	}
	
	// 第七步 填写备注
	public function serven(){
		$faultId = I('get.fault');
		$server  = session('server');
		
		// 保存SESSION
		if($faultId > 0){
			session('server.faultId', $faultId);
		}
		if(empty($server)){
			$this->redirect("Repair/select");
		}
		
		$repairs = D('Repairs');
		$ua      = D('UserAddress');
        $city=session('location.city');
        $city=explode('市',$city)[0];
        if(in_array($city,['北京','上海','广州','深圳'])){
            $is_show_tip=1;
        }else{
            $is_show_tip=0;
        }

		// 步骤 面包屑导航
		$step = $repairs->steps($server['typeId'], $server['brandId'], $server['modelId'], $server['colorId'], $server['catId']);
		$this->assign('step',$step);
 		
		// SEO
        $seo = D('Seo');
        $identSeo = $seo->getSeo('repair');
        $this->assign('is_show_tip',$is_show_tip);
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	// 生成订单
	public function create(){
		$brand      = I('post.brand');
		$model      = I('post.model');
		$color      = I('post.color');
		$remarks    = I('post.remarks');
		$way        = I('post.way');
		$username   = I('post.username');
		$mobile     = I('post.mobile');
		$smscode    = I('post.smscode');
		$city       = I('post.city');
		$address    = I('post.address');
		$supplement = I('post.supplement');
		$district   = I('post.district');
		$lng        = I('post.lng');
		$lat        = I('post.lat');
		$server     = session('server');
		$userId     = session('user.userid');

		$repairs = D('Repairs');
		$orders  = D('Orders');
		$uc      = D('UserCode');
		$users   = D('Users');
		$area    = D('Area');

		if(empty($server)){
			$this->error("请勿重复提交订单!", U('Orders/repair'));exit;
		}
		
		// 城市信息
		$citys = $area->getCityByName($city);
		$areas = $area->getAreaByAreaName($district, $citys['id']);

		// 用户信息
		if(empty($userId)){
			$u = $uc->check($mobile, $smscode);   // 验证短信验证码
			if($u == 1){
				$user = $users->getUserByMobile($mobile);
				if(empty($user)){
					$u = $uc->check($mobile, $smscode);   // 验证短信验证码
					if($u == 1){
						$password  = substr($mobile, -6);
						$rs = $users->regist($mobile, $password);
						if($rs['userId']>0){                       // 注册成功
							$user = $users->getUserByMobile($mobile);
							
							// 发送密码
							sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
	
							// 加载用户信息				
							$userId = $rs['userId'];
						}else{
							$this->error('注册失败');exit;
						}
					}
				}else{
					$userId = $user['id'];
				}
			}else{
				$this->error("短信验证码错误!", U('Orders/repair'));exit;
			}
			$users->saveLogin($user);  // 保存会员登录信息
			$uc->del($mobile);         // 删除user_code表相关记录
		}
		if($userId <= 0){
			$this->error("用户信息错误!", U('Orders/repair'));exit;
		}

		// 维修信息
		if($server['modelId'] > 0 && $server['faultId'] > 0){
			$relation = $repairs->getFaultAllByModelIdAndFaultId($server['modelId'], $server['faultId']);
		}
		
		// 生成订单
		if(empty($server['orderId'])){
			$isFree = 0;
			if($server['shopId'] > 0){
				$isFree = 1;
			}
			$data = array(
				'user_id'       => $userId,
				'agent_id'      => 0,
				'order_sn'      => CBWCreateOrderNumber(),
				'order_type'    => 1,
				'order_date'    => time(),
				'deail_price'   => 0,
				'order_status'  => 1,
				'pay_status'    => 0,
				'area_id'       => $areas['id'],
				'address'       => $address.$supplement,
				'reciver_user'  => $username,
				'reciver_phone' => $mobile,
				'is_free'       => $isFree,
				'remarks'       => $brand.' '.$model.' '.$color.' '.$remarks
			);
			$orderId = $orders->insertOrders($data);
			if($orderId){
				session('server.orderId', $orderId);
				$da = array();
				$da['order_id']      = $orderId;
				$da['shop_id']       = 0;
				$da['relation_id']   = $relation['id'];
				$da['color_id']      = $server['colorId'];
				$da['server_way']    = $way;
				$da['server_status'] = 1;
				$da['lng']           = $lng;
				$da['lat']           = $lat;
				$orders->insertOrderServer($da);           // 添加维修订单表
			}
		}else{
			$orderId = session('server.orderId');
		}
		
		// 推送消息
		if($server['message'] == ''){
			$this->message($orderId, $lng, $lat, $way, $server['shopId']);
		}
		
		$this->redirect('Repair/server', array('id'=>$orderId));exit;
	}
	
	/**
	 * 选择服务
	 */
    public function server(){
		$orderId = I('get.id');

		$repairs = D('Repairs');
		$orders  = D('Orders');
		
		// 订单信息
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($orderId);
		
		// 设置订单报价已读
		$offers = $orders->getOrderOfferByOrderId($orderId, 0, 5);
		foreach($offers as $vo){
			$orders->updateOrderOffer($vo['id'], array('status'=>1));
		}
		
		// 维修信息
		$money = 0;
		if($server['relation_id'] > 0 && $server['color_id'] > 0){
			$relation = $repairs->getRepairByRelationId($server['relation_id']);
			$money = $relation['money'];
			if(!empty($relation['color_money'])){
				$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
				$money = number_format($colormoneys[$server['color_id']], 2, '.', '');
			}
		}
		$relation['money'] = $money;
       
	    // 可报价数量
		$offerCount = $orders->getOrderOfferIngCountByOrderId($orderId);
       
		$this->assign('order',$order);
		$this->assign('server',$server);
		$this->assign('relation',$relation);
		$this->assign('offerCount',$offerCount);
 		
		// 清空SESSION
		session('server', NULL);
 		
		// SEO
        $seo = D('Seo');
		$identSeo = $seo->getSeo('repair');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
        $this->display();
    }
	
	// 加载报价
	public function loadOffer(){
		$id  = I('post.id');
		$lng = I('post.lng');
		$lat = I('post.lat');
		$len = I('post.len');
		$len = empty($len)?0:$len;
		
		$orders = D('Orders');
		$us     = D('UserShop');
		
		// 当前报价列表
		$data = $orders->getOrderOfferByOrderId($id, $len, 5);
		foreach($data as &$vo){
			$shop = $us->getShopByShopId($vo['shop_id']);
			$distance = CBWGetDistance($shop['lng'], $shop['lat'], $lng, $lat);
			$vo['distance']   = number_format($distance/1000, 2, '.', '');
			$vo['type_id']    = $shop['type_id'];
			$vo['shop_name']  = $shop['shop_name'];
			$vo['user_name']  = $shop['user_name'];
			$vo['user_phone'] = $shop['user_phone'];
			$vo['address']    = $shop['address'];
			$vo['lng']        = $shop['lng'];
			$vo['lat']        = $shop['lat'];
			$vo['logo_image'] = $shop['logo_image'];
			$vo['exp']        = $shop['exp'];
			$level = CBWShopLevel($shop['exp']); // 等级换算
			$vo['level_image_name'] = $level['name'];
			$vo['level_image_num']  = $level['num'];
			$vo['level_level']      = $level['level'];
			$way = 1;
			if($vo['is_door'] == 1){
				$way = 2;
			}
			$vo['server_way'] = $way;
		}
		unset($vo);
		echo json_encode($data);
	}
	
	// 确认店铺服务
	public function confirmShopServer(){
		$orderId  = I('post.oid');
		$shopId   = I('post.sid');
		$way      = I('post.way');
		$warranty = I('post.warranty');

		$orders  = D('Orders');
		$users   = D('Users');
		$us      = D('UserShop');
		$repairs = D('Repairs');
		
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($order['id']);
		if($server['server_status'] == 2){
			$this->redirect('Orders/rdetail', array('sn'=>$order['order_sn']));exit;
		}
		
		if($way == 3){
			// 维修信息
			$money = 0;
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$money = $relation['money'];
				if(!empty($relation['color_money'])){
					$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
					$money = number_format($colormoneys[$server['color_id']], 2, '.', '');
				}
			}
			$orders->updateOrderServer($order['id'], array('shop_id'=>29, 'server_status'=>2, 'server_way'=>3, 'warranty'=>$warranty));
			$orders->updateOrders($order['id'], array('deail_price' =>$money));
			
			// 检查订单是否有报价,有报价则退还
			$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
			if(!empty($offer)){
				foreach($offer as $vo){
					if($vo['is_free'] == 0 && $vo['virtual_money'] > 0){
						$shop = $us->getShopByShopId($vo['shop_id']);
						$users->giveVirtual($shop['user_id'], $vo['virtual_money'], 6, $order['order_sn']);      // 增加草包豆
					}
				}
			}
		}else{
			$shop  = $us->getShopByShopId($shopId);
			$offer = $orders->getOrderOfferByOrderIdAndShopId($order['id'], $shopId);
			$sway = 1;
			if($offer['is_door'] == 1){
				$sway = 2;
			}
			$rt = $orders->updateOrderServer($order['id'], array('shop_id'=>$shopId, 'server_status'=>2, 'server_way'=>$sway, 'warranty'=>$offer['warranty']));
			if($rt['status'] > 0){
				$this->pushOrderMessage($order['id'], $shopId);  // 推送通知
				$orders->updateOrders($order['id'], array('deail_price' =>$offer['offer'], 'is_offer' =>0, 'is_offer_selected' =>1));
				$orders->updateOrderOffer($offer['id'], array('selected'=>1));
				if($order['is_offer'] == 0){
					$users->decreaseVirtual($shop['user_id'], 1000, 4, $order['order_sn']);  // 扣除虚拟币
				}

				// 设置订单报价已读
				$offers = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
				foreach($offers as $vo){
					$orders->updateOrderOffer($vo['id'], array('status'=>1));
				}
			
				// 推送订单
				/*if($shop['user_id'] == 14762){
					Vendor('ExternalApi.jikexiuApi');
					$jkx = new \jikexiuApi();
					$jkx->push($order);
				}*/
			}
		}

		$this->redirect('Orders/rdetail', array('sn'=>$order['order_sn']));exit;
	}
}