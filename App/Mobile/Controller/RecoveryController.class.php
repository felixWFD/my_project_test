<?php
namespace Mobile\Controller;
/**
 * 回收控制器
 */
class RecoveryController extends BaseController{
	
	/**
	 * 第一步 选择类型
	 */
	public function select(){
		//清除SESSION
		session('server', null);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('recovery');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display();
	}
	
	/**
	 * 第二部 选择品牌
	 */
	public function brand(){
		//清除SESSION
		session('server', null);
		
		$cat     = D('ProductCat');
		$repairs = D('Repairs');
		
		$typeId = I('get.type');
		
		// 面包屑导航数据
		$step = $repairs->steps($typeId);
		
		// 品牌
		$brandRes = $cat->getCatListById($typeId);
		foreach($brandRes as $vo){
			$keyword[] = $vo['cat_name'].$step['type']."回收";
		}

		$this->assign('step',$step);
		$this->assign('brandRes',$brandRes);
		$this->assign('typeId',$typeId);
		
		// 保存SESSION
		session('server.typeId', $typeId);
		
		//SEO
        $this->assign("title",$step['type']."回收、草包网".$step['type']."回收平台");
        $this->assign("keywords",implode(",", $keyword));
        $this->assign("description","草包网支持国内外主流".$step['type']."回收服务，维修方式为：上门服务，到店服务，邮寄回收服务.");
		
		$this->display();
	}
	
	/**
	 * 第三步 选择型号
	 */
	public function model(){
		$Repairs = D('Repairs');
		$us      = D('UserShop');
		
		$typeId   = I('get.type');
		$brandId  = I('get.brand');
		
		// 正常下单
		$recoveryRes = $Repairs->getRepairByBrandId($brandId);
		
		// 面包屑导航数据
		$step = $Repairs->steps($typeId, $brandId);

		$this->assign('step',$step);
		$this->assign('recoveryRes',$recoveryRes);
		$this->assign('typeId',$typeId);
		$this->assign('brandId',$brandId);
		$this->assign('serverId',$serverId);
		
		// 保存SESSION
		session('server.typeId', $typeId);
		session('server.brandId', $brandId);
		
		//SEO
		$title       = "二手".$step['brand'].$step['type']."回收|二手".$step['brand'].$step['type']."回收价格|二手".$step['brand'].$step['type']."估价 –草包网";
		$keywords    = "二手".$step['brand'].$step['type']."回收, 二手".$step['brand'].$step['type']."回收价格, 二手".$step['brand'].$step['type']."回收价格";
		$description = "草包网 - 二手".$step['brand'].$step['type']."回收频道，提供二手".$step['brand'].$step['type']."在线估价，二手".$step['brand'].$step['type']."回收价格表,价格走势供您参考，卖二手".$step['brand'].$step['type']."就上草包网。";
        $this->assign("title", str_replace('苹果平板', 'ipad', $title));
        $this->assign("keywords", str_replace('苹果平板', 'ipad', $keywords));
        $this->assign("description", str_replace('苹果平板', 'ipad', $description));
		
		$this->display();
	}
	
	/**
	 * 第四步 回收评估
	 */
	public function assess(){
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$shopId  = I('get.sid');
		
		if(empty($modelId)){
			$this->redirect("Recovery/select");
		}
		
		$repairs = D('Repairs');
		$us      = D('UserShop');
		
		// 指定店铺维修
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			if(!empty($shop)){
				session('server.shopId', $shopId);
			}
		}
		
		// 基本情况
		$basic    = $repairs->getBasicByModelId($modelId);
		
		// 功能情况
		$function = $repairs->getFunctionByModelId($modelId);
		
		// 其他情况
		$other    = $repairs->getOtherByModelId($modelId);
		
		// 面包屑导航数据
		$step = $repairs->steps($typeId, $brandId, $modelId);

		$this->assign('step',$step);
		$this->assign('other',$other);
		$this->assign('function',$function);
		$this->assign('basic',$basic);
		$this->assign('typeId',$typeId);
		$this->assign('brandId',$brandId);
		$this->assign('modelId',$modelId);
		
		// 保存
		session('server.typeId',  $typeId);
		session('server.brandId', $brandId);
		session('server.modelId', $modelId);
		
		//SEO
		$title       = "二手".$step['type'].$step['model']."回收价格|二手手机型号估价|二手手机型号能卖多少钱 – 草包网";
		$keywords    = "二手".$step['type'].$step['model']."回收价格,二手".$step['type'].$step['model']."估价,二手".$step['type'].$step['model']."能卖多少钱";
		$description = "专业回收二手".$step['type'].$step['model']."，提供二手".$step['type'].$step['model']."估价、二手".$step['type'].$step['model']."回收价格、二手".$step['type'].$step['model']."价格走势、价格查询等服务。卖二手".$step['type'].$step['model']."上草包网，二手".$step['type'].$step['model']."估价、回收第一选择！";
        $this->assign("title", str_replace('平板', 'ipad ', $title));
        $this->assign("keywords", str_replace('平板', 'ipad ', $keywords));
        $this->assign("description", str_replace('平板', 'ipad ', $description));
		
		$this->display();
	}
	
	/**
	 * 第五步 用户信息
	 */
	public function information(){
		$ids    = I('post.ids');
		$server = session('server');
		
		$repairs = D('Repairs');
		$banks   = D('Banks');
        $city=session('location.city');
        $city=explode('市',$city)[0];
        if(in_array($city,['北京','上海','广州','深圳'])){
            $is_show_tip=1;
        }else{
            $is_show_tip=0;
        }
		// 面包屑导航数据
		$step = $repairs->steps($server['typeId'], $server['brandId'], $server['modelId']);
		
		// 银行列表
		$bank = $banks->getBank();
        $this->assign('is_show_tip',$is_show_tip);
		$this->assign('step',$step);
		$this->assign('bank',$bank);
		$this->assign('typeId',$server['typeId']);
		$this->assign('brandId',$server['brandId']);
		$this->assign('modelId',$server['modelId']);
		
		// 保存SESSION
		session('server.ids', $ids);
		
		//SEO
		$title       = "二手".$step['type'].$step['model']."回收价格|二手手机型号估价|二手手机型号能卖多少钱";
		$keywords    = "二手".$step['type'].$step['model']."回收价格,二手".$step['type'].$step['model']."估价,二手".$step['type'].$step['model']."能卖多少钱";
		$description = "专业回收二手".$step['type'].$step['model']."，提供二手".$step['type'].$step['model']."估价、二手".$step['type'].$step['model']."回收价格、二手".$step['type'].$step['model']."价格走势、价格查询等服务。卖二手".$step['type'].$step['model']."上草包网，二手".$step['type'].$step['model']."估价、回收第一选择！";
        $this->assign("title", str_replace('平板', 'ipad ', $title));
        $this->assign("keywords", str_replace('平板', 'ipad ', $keywords));
        $this->assign("description", str_replace('平板', 'ipad ', $description));
		
		$this->display();
	}
	
	// 生成订单
	public function create(){
		$brand      = I('post.brand');
		$model      = I('post.model');
		$condition  = I('post.condition');
		$memory     = I('post.memory');
		$overhaul   = I('post.overhaul');
		$remarks    = I('post.remarks');
		$username   = I('post.username');
		$mobile     = I('post.mobile');
		$payway     = I('post.payway');
		$bank       = I('post.bank');
		$idcart     = I('post.idcart');
		$zfb        = I('post.zfb');
		$smscode    = I('post.smscode');
		$city       = I('post.city');
		$address    = I('post.address');
		$supplement = I('post.supplement');
		$district   = I('post.district');
		$lng        = I('post.lng');
		$lat        = I('post.lat');
		$way        = I('post.way');
		$server     = session('server');
		$userId     = session('user.userid');

		$repairs = D('Repairs');
		$orders  = D('Orders');
		$uc      = D('UserCode');
		$users   = D('Users');
		$area    = D('Area');

		if(empty($server)){
			$this->error("请勿重复提交订单!", U('Orders/repair'));exit;
		}
		
		// 城市信息
		$citys = $area->getCityByName($city);
		$areas = $area->getAreaByAreaName($district, $citys['id']);

		// 用户信息
		if(empty($userId)){
			$u = $uc->check($mobile, $smscode);   // 验证短信验证码
			if($u == 1){
				$user = $users->getUserByMobile($mobile);
				if(empty($user)){
					$u = $uc->check($mobile, $smscode);   // 验证短信验证码
					if($u == 1){
						$password  = substr($mobile, -6);
						$rs = $users->regist($mobile, $password);
						if($rs['userId']>0){                       // 注册成功
							$user = $users->getUserByMobile($mobile);
							
							// 发送密码
							sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
	
							// 加载用户信息				
							$userId = $rs['userId'];
						}else{
							$this->error('注册失败');exit;
						}
					}
				}else{
					$userId = $user['id'];
				}
			}else{
				$this->error("短信验证码错误!", U('Orders/repair'));exit;
			}
			$users->saveLogin($user);  // 保存会员登录信息
			$uc->del($mobile);         // 删除user_code表相关记录
		}
		if($userId <= 0){
			$this->error("用户信息错误!", U('Orders/repair'));exit;
		}
		
		// 收款信息
		if($payway == 'bank'){
			$pay = $username.' '.$bank.' '.$idcart;
		}else{
			$pay = $username.' 支付宝 '.$zfb;
		}
		
		// 备注
		if(empty($server['ids'])){
			$remarks = array($brand, $model, $condition, $memory, $overhaul, $remarks);
			$remarks = implode(" | ", array_filter($remarks));
		}
		
		// 生成订单
		if(empty($server['orderId'])){
			$isFree = 0;
			if($server['shopId'] > 0){
				$isFree = 1;
			}
			$data = array(
				'user_id'       => $userId,
				'agent_id'      => 0,
				'order_sn'      => CBWCreateOrderNumber(),
				'order_type'    => 2,
				'order_date'    => time(),
				'deail_price'   => 0,
				'order_status'  => 1,
				'pay_status'    => 0,
				'area_id'       => $areas['id'],
				'address'       => $address.$supplement,
				'reciver_user'  => $username,
				'reciver_phone' => $mobile,
				'is_free'       => $isFree,
				'remarks'       => $remarks
			);
			$orderId = $orders->insertOrders($data);
			if($orderId){
				session('server.orderId', $orderId);
				$da = array();
				$da['order_id']    = $orderId;
				$da['shop_id']     = 0;
				$da['reco_id']     = $server['modelId'];
				$da['reco_way']    = $way;
				$da['reco_attr']   = $server['ids'];
				$da['reco_status'] = 1;
				$da['reco_pay']    = $pay;
				$da['lng']         = $lng;
				$da['lat']         = $lat;
				$orders->insertOrderRecovery($da);           // 添加回收订单表
			}
		}else{
			$orderId = session('server.orderId');
		}
		
		// 推送消息
		if($server['message'] == ''){
			$this->message($orderId, $lng, $lat, $way, $server['shopId']);
		}

		$this->redirect('Recovery/server', array('id'=>$orderId));exit;
	}
	
	/**
	 * 选择服务
	 */
    public function server(){
		$orderId = I('get.id');

		$repairs = D('Repairs');
		$orders  = D('Orders');
		
		// 订单信息
		$order    = $orders->getOrderById($orderId);
		$recovery = $orders->getOrderRecoveryByOrderId($orderId);
		
		// 设置订单报价已读
		$offers = $orders->getOrderOfferByOrderId($orderId, 0, 5);
		foreach($offers as $vo){
			$orders->updateOrderOffer($vo['id'], array('status'=>1));
		}
		
		// 维修信息
		if($recovery['reco_id'] > 0){
			$repair = $repairs->getRepairById($recovery['reco_id']);
			$repair['money'] = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
		}
       
	    // 可报价数量
		$offerCount = $orders->getOrderOfferIngCountByOrderId($orderId);

		$this->assign('order',$order);
		$this->assign('recovery',$recovery);
		$this->assign('repair',$repair);
		$this->assign('offerCount',$offerCount);
 		
		// 清空SESSION
		session('server', NULL);
		
		//SEO
        $this->assign("title",$recovery['pro_name']."回收价格_回收多少钱");
        $this->assign("keywords",$recovery['pro_name']."回收价格");
        $this->assign("description","草包网提供".$recovery['pro_name']."回收价格，让大家了解".$recovery['pro_name']."回收多少钱，详细的".$recovery['pro_name']."回收报价让您更放心。");
		
        $this->display();
    }
	
	// 加载报价
	public function loadOffer(){
		$id  = I('post.id');
		$lng = I('post.lng');
		$lat = I('post.lat');
		$len = I('post.len');
		$len = empty($len)?0:$len;
		
		$orders = D('Orders');
		$us     = D('UserShop');
		
		// 当前报价列表
		$data = $orders->getOrderOfferByOrderId($id, $len, 5);
		foreach($data as &$vo){
			$shop = $us->getShopByShopId($vo['shop_id']);
			$distance = CBWGetDistance($shop['lng'], $shop['lat'], $lng, $lat);
			$vo['distance']   = number_format($distance/1000, 2, '.', '');
			$vo['type_id']    = $shop['type_id'];
			$vo['shop_name']  = $shop['shop_name'];
			$vo['user_name']  = $shop['user_name'];
			$vo['user_phone'] = $shop['user_phone'];
			$vo['address']    = $shop['city_addr'].$shop['addr'].$shop['address'];
			$vo['lng']        = $shop['lng'];
			$vo['lat']        = $shop['lat'];
			$vo['logo_image'] = $shop['logo_image'];
			$vo['exp']        = $shop['exp'];
			$level = CBWShopLevel($shop['exp']); // 等级换算
			$vo['level_image_name'] = $level['name'];
			$vo['level_image_num']  = $level['num'];
			$vo['level_level']      = $level['level'];
			$way = 1;
			if($vo['is_door'] == 1){
				$way = 2;
			}
			$vo['reco_way'] = $way;
		}
		unset($vo);
		echo json_encode($data);
	}
	
	// 确认店铺服务
	public function confirmShopServer(){
		$orderId  = I('post.oid');
		$shopId   = I('post.sid');
		$way      = I('post.way');
		$warranty = I('post.warranty');

		$orders  = D('Orders');
		$users   = D('Users');
		$us      = D('UserShop');
		$repairs = D('Repairs');
		
		$order    = $orders->getOrderById($orderId);
		$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
		if($recovery['reco_status'] == 2){
			$this->redirect('Orders/redetail', array('sn'=>$order['order_sn']));exit;
		}
		
		if($way == 3){
			// 回收信息
			$money = 0;
			if($recovery['reco_id'] > 0 && !empty($recovery['reco_attr'])){
				$money = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
			}
			$orders->updateOrderRecovery($order['id'], array('shop_id'=>29, 'reco_status'=>2, 'reco_way'=>3));
			$orders->updateOrders($order['id'], array('deail_price' =>$money));
			
			// 检查订单是否有报价,有报价则退还
			$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
			if(!empty($offer)){
				foreach($offer as $vo){
					if($vo['is_free'] == 0 && $vo['virtual_money'] > 0){
						$shop = $us->getShopByShopId($vo['shop_id']);
						$users->giveVirtual($shop['user_id'], $vo['virtual_money'], 6, $order['order_sn']);      // 增加草包豆
					}
				}
			}
		}else{
			$shop  = $us->getShopByShopId($shopId);
			$offer = $orders->getOrderOfferByOrderIdAndShopId($order['id'], $shopId);
			$sway = 1;
			if($offer['is_door'] == 1){
				$sway = 2;
			}
			$rt = $orders->updateOrderRecovery($order['id'], array('shop_id'=>$shopId, 'reco_status'=>2, 'reco_way'=>$sway));
			if($rt['status'] > 0){
				$this->pushOrderMessage($order['id'], $shopId);  // 推送通知
				$orders->updateOrders($order['id'], array('deail_price' =>$offer['offer'], 'is_offer' =>0, 'is_offer_selected' =>1));
				$orders->updateOrderOffer($offer['id'], array('selected'=>1));
				if($order['is_offer'] == 0){
					$users->decreaseVirtual($shop['user_id'], 1000, 4, $order['order_sn']);  // 扣除虚拟币
				}

				// 设置订单报价已读
				$offers = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
				foreach($offers as $vo){
					$orders->updateOrderOffer($vo['id'], array('status'=>1));
				}
			
				// 推送订单
				/*if($shop['user_id'] == 14762){
					Vendor('ExternalApi.jikexiuApi');
					$jkx = new \jikexiuApi();
					$jkx->push($order);
				}*/
			}
		}
		
		$this->redirect('Orders/redetail', array('sn'=>$order['order_sn']));exit;
	}
	
}