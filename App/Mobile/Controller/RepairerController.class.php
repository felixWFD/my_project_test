<?php
namespace Mobile\Controller;
/**
 * 维修店铺控制器
 */
class RepairerController extends BaseController{
	
	/**
	 * 列表
	 */
	public function index(){
		$cityId   = I('get.city');
		$areaId   = I('get.area');
		$typeId   = I('get.type');
		$brandId  = I('get.brand');
		$cityName = I('get.name');
		
		$a   = D('Area');
		$cat = D('ProductCat');
		$us  = D('UserShop');

		if(empty($cityId)){
			if(empty($name)){
				$cityName = '深圳市';
			}
			$city = $a->getCityByName($cityName);
			$cityId = $city['id'];
		}else{
			$city = $a->getCityById($cityId);
		}
		$citys = $a->getCityByProvinceId($city['one_id']);
		$areas = $a->getAreaByCityId($city['id']);
		$check = 0;
		foreach($areas as $key=>$vo){
			$areas[$key]['shopc'] = $us->getShopsCountByObject(array('area'=>$vo['id']));
			if($vo['id'] == $areaId){
				$check = 1;
			}
		}
		$areas = CBWArraySort($areas,'shopc',SORT_DESC);
		if($check == 0){
			$areaId = $areas[0]['id'];
		}
		$area = $a->getAreaById($areaId);

		// 分类列表
		$types = $cat->getCatListById(3);
		if(!empty($typeId)){
			$brands = $cat->getCatListById($typeId);
			$type = $cat->getCat($typeId);
		}
		if(!empty($brandId)){
			$brand = $cat->getCat($brandId);
		}
		
		// 店铺列表
		$data = $us->getRepairerByObj(array('area'=>$areaId, 'type'=>$typeId, 'brand'=>$brandId, 'm'=>0, 'n'=>8));
		foreach($data as &$vo){
			$sbrand = $us->getUserBrandByUserId($vo['user_id']);
			$bname = array();
			foreach($sbrand as $v){
				$bname[] = $v['cat_name'];
			}
			$vo['brands'] = implode(" ", array_unique($bname));
			$level = CBWShopLevel($vo['exp']); // 等级换算
			$vo['level_image_name'] = $level['name'];
			$vo['level_image_num']  = $level['num'];
			$vo['level_level']      = $level['level'];
		}
		unset($vo);
		
		$this->assign("shops", $data);
		$this->assign("brands", $brands);
		$this->assign("types", $types);
		$this->assign("areas", $areas);
		$this->assign("area", $area);
		$this->assign("citys", $citys);
		$this->assign("city", $city);
		$this->assign("area_id", $areaId);
		$this->assign("city_id", $cityId);
		$this->assign("type_id", $typeId);
		$this->assign("brand_id", $brandId);
		
		//SEO
		$this->assign("title", $city['addr'].$area['addr'].$brand['cat_name'].$type['cat_name']."维修点_".$city['addr'].$area['addr']."哪里有".$brand['cat_name'].$type['cat_name']."维修店");
		$this->assign("keywords", $city['addr'].$area['addr'].$brand['cat_name'].$type['cat_name']."维修点,".$area['addr']."哪里有".$brand['cat_name'].$type['cat_name']."维修店");
		$this->assign("description", "草包网授权".$city['addr'].$area['addr']."数百家维修点，查找".$area['addr']."附近哪里有专业修".$brand['cat_name'].$type['cat_name']."的维修店最全面，价格最真实，安全有保障的维修服务，尽在草包网。");
		
		$this->display();
	}
	
	/**
	 * 加载店铺品牌
	 * 
	 */
	public function loadShopBrand(){
		$typeId = I('post.type');
		
		$cat = D('ProductCat');
		
		$data = $cat->getCatListById($typeId);
		echo json_encode($data);
	}
	
	/**
	 * 加载更多维修店
	 * 
	 */
	public function loadUserShops(){
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		$areaId  = I('post.area');
		$typeId  = I('post.type');
		$brandId = I('post.brand');

		$us = D('UserShop');
		
		$data = $us->getRepairerByObj(array('area'=>$areaId, 'type'=>$typeId, 'brand'=>$brandId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$sbrand = $us->getUserBrandByUserId($vo['user_id']);
			$bname = array();
			foreach($sbrand as $v){
				$bname[] = $v['cat_name'];
			}
			$vo['brands'] = implode(" ", array_unique($bname));
			$level = CBWShopLevel($vo['exp']); // 等级换算
			$vo['level_image_name'] = $level['name'];
			$vo['level_image_num']  = $level['num'];
			$vo['level_level']      = $level['level'];
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 详细
	 */
	public function detail(){
		$shopId = I('get.id');

		$us       = D('UserShop');
		$cat      = D('ProductCat');
		$comments = D('Comments');
		
		$shop = $us->getShopByShopId($shopId);
		if(empty($shop) || $shop['shop_status']==0){
			$this->_empty();exit;
		}
		
		$level = CBWShopLevel($shop['exp']); // 等级换算
		$shop['level_image_name'] = $level['name'];
		$shop['level_image_num']  = $level['num'];
		$shop['level_level']      = $level['level'];
		
		$shop['user_name']     = CBWSubSurname($shop['user_name']);
		
		$hours = explode(",", $shop['open_hours']);
		$shop['open_time']     = empty($hours[0])?'8':$hours[0];
		$shop['close_time']    = empty($hours[1])?'18':$hours[1];
		
		$shop['comment_count'] = $comments->getCommentsCountByObject(array('shopId'=>$shop['id']));
		$shop['comment']       = $comments->getCommentsByObject(array('shopId'=>$shop['id'], 'm'=>0, 'n'=>5));
		
		// 维修/回收品牌列表
		$types  = $cat->getCatListById(3);
		$typeId = $types[0]['id'];
		$brands = $us->getUserBrandByTypeId($shop['user_id'], $typeId);
		if(!empty($brands)){
			$brandId = $brands[0]['id'];
			$models  = $us->getUserModelByBrandId($shop['user_id'], $brandId);
		}
		
		//SEO
		$sbrand = $us->getUserBrandByUserId($shop['user_id']);
		$bname = array();
		foreach($sbrand as $v){
			$bname[] = $v['cat_name'];
		}
        $this->assign("title", $shop['shop_name']."维修店_".$shop['city'].$shop['area'].$shop['shop_name']."维修点");
        $this->assign("keywords", $shop['shop_name']."维修店,".$shop['city'].$shop['area'].$shop['shop_name']."维修点");
        $this->assign("description", $shop['shop_name']."维修店，位于".$shop['province'].$shop['city'].$shop['area'].$shop['address']."，专业修".implode("、", array_unique($bname))."。草包网平台保障，快速维修。");

		$this->assign('shop', $shop);
		$this->assign('types', $types);
		$this->assign('typeId', $typeId);
		$this->assign('brands', $brands);
		$this->assign('brandId', $brandId);
		$this->assign('models', $models);
		$this->display();
	}
	
	// 加载品牌
	public function loadBrands(){
		$typeId = I('post.id');
		$shopId = I('post.shop_id');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);                         // 店铺信息
		$data = $us->getUserBrandByTypeId($shop['user_id'], $typeId);

		echo json_encode($data);
	}
	
	// 加载型号
	public function loadModels(){
		$brandId = I('post.id');
		$shopId  = I('post.shopId');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);                         // 店铺信息
		$data = $us->getUserModelByBrandId($shop['user_id'], $brandId);
		
		echo json_encode($data);
	}
	
	// 店铺报价
	public function offer(){
		$this->isUserLogin();
		
		$orderId = I('get.id');
		$shopId  = I('get.sid');
		$userId  = session('user.userid');
		
		$orders   = D('Orders');
		$users    = D('Users');
		$repairs  = D('Repairs');
		$cat      = D('ProductCat');
		$us       = D('UserShop');
		$products = D('Products');

		$shop   = $us->getShopByShopId($shopId);
		$ings   = $orders->getOrderOfferIngByOrderId($orderId);
		$ids = array();
		foreach($ings as $vo){
			$ids[] = $vo['shop_id'];
		}
		if(!in_array($shopId, $ids) || $shop['user_id'] != $userId){
			$this->error('非法操作');exit;
		}
		
		$ing = $orders->getOrderOfferIngByOrderIdAndShopId($orderId, $shopId);
		if($ing['status'] == 0){
			$orders->updateOrderOfferIng($orderId, $shopId, array('status'=>1));
		}

		$order  = $orders->getOrderById($orderId);
		$user   = $users->getUserAndInfoById($order['user_id']);   // 顾客信息
		$suser  = $users->getUserAndInfoById($shop['user_id']);    // 店铺用户信息
		
		if($order['order_type'] == 1){
			$server = $orders->getOrderServerByOrderId($orderId);
			$order['server_way'] = $server['server_way'];
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$brand    = $cat->getCat($relation['brand_id']);
			}
			$lng = $server['lng'];
			$lat = $server['lat'];
			// 维修信息
			if($server['relation_id'] > 0 && $server['color_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$money = $relation['money'];
				if(!empty($relation['color_money'])){
					$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
					$money = $colormoneys[$server['color_id']];
				}
				if($money > 0){
					$money = $money-25;
				}
			}
		}else{
			$recovery = $orders->getOrderRecoveryByOrderId($orderId);
			$order['server_way'] = $recovery['reco_way'];
			if($recovery['reco_id'] > 0 && !empty($recovery['reco_attr'])){
				$repair   = $repairs->getRepairById($recovery['reco_id']);
				$brand    = $cat->getCat($repair['brand_id']);
				$recovery['pro_name'] = $repair['pro_name'];
				$recovery['attrs'] = implode('|', $repairs->getAttribute($recovery['reco_attr']));
			}
			$lng = $recovery['lng'];
			$lat = $recovery['lat'];
			// 回收信息
			if($recovery['reco_id'] > 0){
				$money = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
			}
		}

		// 报价信息
		$count    = $orders->getOrderOfferCountByOrderId($orderId);
		$offer    = $orders->getOrderOfferByOrderIdAndShopId($orderId, $shopId);
		$selected = $orders->getOrderOfferSelectedByOrderId($orderId);
		$distance = CBWGetDistance($shop['lng'], $shop['lat'], $lng, $lat);
		$offers   = $orders->getOrderOfferByOrderId($orderId, 0, 5);
		foreach($offers as &$vo){
			$data = $us->getShopByShopId($vo['shop_id']);
			$vo['logo_image'] = $data['logo_image'];
			$vo['shop_name']  = $data['shop_name'];
			$vo['offer_date'] = CBWTimeFormat($vo['offer_date'], 'm.d');
		}
		unset($vo);

		// 充值草包豆
		$product = $products->getProductAttrByProductId(722);
		$product = $product[0];
		foreach($product['list'] as $key=>&$vo){
			if($key == 0){
				$vo['virtual'] = $vo['attr_value']*100;
			}else{
				$vo['virtual']      = $vo['attr_value']*100;
				$vo['give_virtual'] = $key*10*$vo['attr_value'];
			}
		}
		unset($vo);

		$this->assign('order', $order);
		$this->assign('user', $user);
		$this->assign('suser', $suser);
		$this->assign('server', $server);
		$this->assign('recovery', $recovery);
		$this->assign('relation', $relation);
		$this->assign('brand', $brand);
		$this->assign('shop', $shop);
		$this->assign('count', $count);
		$this->assign('offer', $offer);
		$this->assign('selected', $selected);
		$this->assign('offers', $offers);
		$this->assign('product', $product);
		$this->assign('lng', $lng);
		$this->assign('lat', $lat);
		$this->assign('money', number_format($money, 2, '.', ''));
		$this->assign('distance', number_format($distance/1000, 2, '.', ''));

		//SEO
		$this->assign('title', "我要报价");
		$this->assign('keywords', "我要报价");
		$this->assign('description', "我要报价");

		$this->display();
	}
	
	// 加载维修师报价
	public function loadOffer(){
		$id  = I('post.id');

		$orders = D('Orders');
		$us     = D('UserShop');
		
		// 订单报价列表
		$data = $orders->getOrderOfferByOrderId($id, 0, 5);
		foreach($data as &$vo){
			$shop = $us->getShopByShopId($vo['shop_id']);
			$vo['shop_name']  = $shop['shop_name'];
			$vo['logo_image'] = $shop['logo_image'];
		}
		unset($vo);
		echo json_encode($data);
	}
	
	// 提交店铺报价
	public function submitOffer(){
		$this->isUserLogin();

		$orderId  = I('post.id');
		$shopId   = I('post.shop_id');
		$userId   = session('user.userid');
		$offer    = I('post.offer');
		$warranty = I('post.warranty');
		$remarks  = I('post.remarks');
		
		
		$users  = D('Users');
		$us     = D('UserShop');
		$orders = D('Orders');
		
		$shop = $us->getShopByShopId($shopId);
		$user = $users->getUserAndInfoById($shop['user_id']);
		$order = $orders->getOrderById($orderId);
		
		/*
		$virtualMoney = 1000;
		if($offer <=100){
			$virtualMoney = 500;
		}
		if($shop['free_status'] == 0 && $shop['type_id'] != 2){
			if($user['virtual_money'] < $virtualMoney){
				echo -1; exit;  // 草包豆数量不足
			}
			$isFree = 0;
		}else{
			$isFree       = 1;
			$virtualMoney = 0;
		}
		if($shop['type_id'] == 2){
			$isFree       = 1;
			$virtualMoney = 0;
		}
		if($order['is_free'] == 1){  // 维修师个人页面下单免草包豆
			$isFree       = 1;
			$virtualMoney = 0;
		}
		*/
		
		$isFree       = 1;
		$virtualMoney = 0;
		
		if($shop['user_id'] != $userId){
			echo -2; exit;  // 非法操作
		}
		$count = $orders->getOrderOfferCountByOrderId($orderId);
		if($count >= 3){
			echo -3; exit;  // 报价人数已满
		}
		$checkOffer = $orders->getOrderOfferByOrderIdAndShopId($orderId, $shopId);
		if(!empty($checkOffer)){
			echo -4; exit;  // 不允许重复报价
		}
		
		if($order['order_status'] != 1){
			echo -5; exit;  // 订单状态异常(已取消或已完成)
		}
		if($order['is_offer'] == 0){
			echo -6; exit;  // 订单已关闭报价
		}
		
		$data = array(
			'order_id'      => $orderId,
			'shop_id'       => $shopId,
			'offer'         => $offer,
			'warranty'      => $warranty,
			'virtual_money' => $virtualMoney,
			'offer_date'    => time(),
			'is_free'       => $isFree,
			'remarks'       => $remarks
		);
		$offerId = $orders->insertOrderOffer($data);
		if($offerId > 0){
			$orders->updateOrderOfferIng($orderId, $shop['id'], array('is_offer'=>1));
			if($isFree == 0){
				$users->decreaseVirtual($shop['user_id'], $virtualMoney, 4, $order['order_sn']);  // 扣除虚拟币
			}else{
				$us->updateUserShop($shop['id'], array('free_status'=>0));
			}
			$count = $orders->getOrderOfferCountByOrderId($orderId);
			if($count >= 3){
				$orders->updateOrders($orderId, array('is_offer'=>0));
			}
			
			// 给顾客发送短信消息
			if(!empty($order['reciver_phone'])){
				//sendSmsByAliyun($order['reciver_phone'], array(), 'SMS_116561091'); // 给顾客发送下单通知短信
			}
			
			$data = array(
				'reciver_phone' =>$order['reciver_phone'],
				'shop_name'     =>$shop['shop_name'],
				'logo_image'    =>$shop['logo_image'],
				'warranty'      =>$warranty,
				'remarks'       =>$remarks,
				'offer'         =>number_format($offer, 2, '.', '')
			);
			echo json_encode($data); exit;  // 报价成功
		}
		echo 0; exit;  // 报价失败,未知错误
	}

	/**
	 * 评价列表
	 */
	public function comments(){
		$shopId = I('get.id');
		
		$users    = D('Users');
		$us       = D('UserShop');
		$comments = D('Comments');
		
		$shop = $us->getShopByShopId($shopId);
		if(empty($shop)){
			$this->_empty();
			exit;
		}
	
		$level = CBWShopLevel($shop['exp']); // 等级换算
		$shop['level_image_name'] = $level['name'];
		$shop['level_image_num']  = $level['num'];
		$shop['level_level']      = $level['level'];
		
		$shop['user_name']     = CBWSubSurname($shop['user_name']);
		
		$hours = explode(",", $shop['open_hours']);
		$shop['open_time']     = empty($hours[0])?'8':$hours[0];
		$shop['close_time']    = empty($hours[1])?'18':$hours[1];
		
		$shop['comment_count'] = $comments->getCommentsCountByObject(array('shopId'=>$shopId));
		$shop['comment']       = $comments->getCommentsByObject(array('shopId'=>$shopId, 'm'=>0, 'n'=>8));
		
		$this->assign('shop', $shop);
		
		//SEO
		$sbrand = $us->getUserBrandByUserId($shop['user_id']);
		$bname = array();
		foreach($sbrand as $v){
			$bname[] = $v['cat_name'];
		}
        $this->assign("title", $shop['shop_name']."维修店_".$shop['city'].$shop['area'].$shop['shop_name']."维修点");
        $this->assign("keywords", $shop['shop_name']."维修店,".$shop['city'].$shop['area'].$shop['shop_name']."维修点");
        $this->assign("description", $shop['shop_name']."维修店，位于".$shop['province'].$shop['city'].$shop['area'].$shop['address']."，专业修".implode("、", array_unique($bname))."。草包网平台保障，快速维修。");

		
		$this->display();
	}
	
	/**
	 * 加载更多评价
	 * 
	 */
	public function loadMoreComments(){
		$shopId  = I('post.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$comments = D('Comments');
		
		$data = $comments->getCommentsByObject(array('shopId'=>$shopId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['comment_time'] = date("Y-m-d", $vo['comment_time']);
		}
		echo json_encode($data);
	}
}