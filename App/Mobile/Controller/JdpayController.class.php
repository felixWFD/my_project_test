<?php
namespace Mobile\Controller;
/**
 * 京东快捷支付 支付模块
 * @version 2016-08-22
 */
class JdpayController extends BaseController {
	
	//在类初始化方法中，引入相关类库    
	public function __construct() {
		Vendor('JdPay.JdPayDes');
		Vendor('JdPay.JdPayService');
		Vendor('JdPay.JdPayData');
	}
	
	// 网银在线签约
	public function sign(){
		$cardId  = I('post.trade_card');
		$tradeNo = I('post.trade_no');
		
		$users  = D('Users');
		$orders = D('Orders');
		  
		$card = $users->getCardById($cardId);

		$card_bank      = $card['ident'];                         // 银行编码       必填
		$card_type      = $card['card_type'];                     // 卡类型         必填 (信用卡：C借记卡：D)
		$card_no        = $card['card_no'];                       // 卡号           必填
		$card_exp       = $card['card_exp'];                      // 信用卡有效期   选填 (1604)
		$card_cvv2      = $card['card_cvv2'];                     // 信用卡校验码   选填 (753)
		$card_name      = $card['true_name'];                     // 持卡人姓名     必填
		$card_idtype    = 'I';                                    // 持卡人证件类型 必填 (I:身份证)
		$card_idno      = $card['identity'];                      // 持卡人证件号码 必填
		$card_phone     = $card['card_mobile'];                   // 持卡人电话号码 必填
		$trade_type     = 'V';                                    // 交易类型       必填 (V：签约)
		$trade_no       = $tradeNo;                               // 交易号         必填
		$money          = $orders->getOrderMoney($tradeNo);       // 金额           必填
		$trade_currency = 'CNY';                                  // 货币类型       必填 (CNY)
		
		$base = new \JdPayDataBase();
		$data_xml = $base->signXml($card_bank,$card_type,
										$card_no,$card_exp,$card_cvv2,
										$card_name,$card_idtype,$card_idno,
										$card_phone,$trade_type,$trade_no,
										$money*100,$trade_currency);
		$service = new \JdPayService();
		//发起交易至快捷支付
		$resp = $service->trade($data_xml);
		//解析响应结果
		$data = $service->operate($resp);
		echo json_encode($data);
	}
	
	// 网银在线查询
	public function query(){
		$trade_type = 'Q';                     // 交易类型       必填 (Q：查询)
		$trade_no   = I('post.trade_no');      // 交易号         必填
		
		$base = new \JdPayDataBase();
		$data_xml = $base->queryXml($trade_type,$trade_no);

		$service = new \JdPayService();
		//发起交易至快捷支付
		$resp = $service->trade($data_xml);
		//解析响应结果
		$data = $service->operate($resp);
		echo json_encode($data);
	}
	
	// 网银在线消费
	public function pay(){
		$cardId  = I('post.trade_card');
		$tradeNo = I('post.trade_no');
		$smsCode = I('post.smscode');
		
		$users  = D('Users');
		$orders = D('Orders');
		
		$card = $users->getCardById($cardId);
		
		$card_bank      = $card['ident'];                         // 银行编码       必填
		$card_type      = $card['card_type'];                     // 卡类型         必填 (信用卡：C借记卡：D)
		$card_no        = $card['card_no'];                       // 卡号           必填
		$card_exp       = $card['card_exp'];                      // 信用卡有效期   选填 (1604)
		$card_cvv2      = $card['card_cvv2'];                     // 信用卡校验码   选填 (753)
		$card_name      = $card['true_name'];                     // 持卡人姓名     必填
		$card_idtype    = 'I';                                    // 持卡人证件类型 必填 (I:身份证)
		$card_idno      = $card['identity'];                      // 持卡人证件号码 必填
		$card_phone     = $card['card_mobile'];                   // 持卡人电话号码 必填
		$trade_type     = 'S';                                    // 交易类型       必填 (S：消费)
		$trade_no       = $tradeNo;                               // 交易号         必填
		$money          = $orders->getOrderMoney($tradeNo);       // 金额           必填
		$trade_currency = 'CNY';                                  // 货币类型       必填 (CNY)
		$trade_date     = date("Ymd",time());                     // 日期           选填 (20140402)
		$trade_time     = date("His",time());                     // 时间           选填 (183000)
		$trade_notice   = '';                                     // 通知地址       选填 (如果填写，则异步发送结果通知到指定地址)
		$ordbody        = $orders->getOrderBody($tradeNo);        // 备注           选填
		$trade_code     = $smsCode;                               // 验证码         必填 (消费时，请输入真实手机验证码)

		$base = new \JdPayDataBase();
		$data_xml = $base->payXml($card_bank,$card_type,$card_no,
										$card_exp,$card_cvv2,$card_name,
										$card_idtype,$card_idno,$card_phone,
										$trade_type,$trade_no,$money*100,
										$trade_currency,$trade_date,$trade_time,
										$trade_notice,$ordbody,$trade_code);
		$service = new \JdPayService();
		//发起交易至快捷支付
		$resp = $service->trade($data_xml);
		//解析响应结果
		$data = $service->operate($resp);
		echo json_encode($data);
	}
	
	// 网银在线退款
	public function refund(){
		
		$orders = D('Orders');
		
		$trade_type     = 'R';                             // 交易类型       必填 (R：退款)
		$trade_no       = I('post.trade_no');              // 交易号         必填
		$trade_oid      = I('post.trade_oid');             // 原交易号       必填
		$money          = I('post.money');                 // 金额           必填              
		$trade_currency = 'CNY';                           // 货币类型       必填 (CNY)
		$trade_date     = date("Ymd",time());              // 日期           选填 (20140402)
		$trade_time     = date("His",time());              // 时间           选填 (183000)
		$trade_notice   = '';                              // 通知地址       选填 (如果填写，则异步发送结果通知到指定地址)
		$ordbody        = $orders->getOrderBody($tradeNo); // 备注	       选填
		
		$base = new \JdPayDataBase();
		$data_xml = $base->refundXml($trade_type,$trade_no,$trade_oid
										,$money*100,$trade_currency,$trade_date,
										$trade_time,$trade_notice,$ordbody);
		$service = new \JdPayService();
		//发起交易至快捷支付
		$resp = $service->trade($data_xml);
		//解析响应结果
		$data = $service->operate($resp);
		echo json_encode($data);
	}
	
}