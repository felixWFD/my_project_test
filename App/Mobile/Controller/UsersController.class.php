<?php
namespace Mobile\Controller;
/**
 * 会员控制器
 */
class UsersController extends BaseController{
    /**
     * 跳去登录界面
     */
	public function login($type = null){
		$userId = session('user.userid');
		
		if($userId != ""){
			$this->redirect("Users/index");
		}

		if(!empty($type)){
			if($type == 'weixin'){
				$config = C("THINK_SDK_WEIXIN");
				$wechatObj = new \Think\WechatApi($config['APPKEY'], $config['APPSECRET']);
				redirect($wechatObj->getOauth2URL());
			}
		}

        $login_times=session('login_times');
        if(!empty($login_times)) {
            $this->assign('times', $login_times);
        }
        $tencent_config=C('TENCENT_IMG_CODE');
        $this->assign("appid",$tencent_config['appid']);
		$this->assign("agent",CBWUserAgent());
		$this->display('Users/login');
	}

    /**
	 * 授权回调地址
	 * @type 类型 
	 * @code void 
	 * @return void 
	 */
    public function callback($code = null){
        (empty($code)) && $this->error('参数错误');

        $config = C("THINK_SDK_WEIXIN");
		$wechatObj = new \Think\WechatApi($config['APPKEY'], $config['APPSECRET']);
        $token = $wechatObj->accessTokenGet2($code);
        //获取当前登录用户信息
        if(is_array($token)){
			$chat = $wechatObj->infoQuery($token['openid']);
			if(is_array($chat)){
				$uw = D('UserWechat');
				$wechat = $uw->getWechatByUnionId($chat['unionid']);
				if(!empty($wechat['user_id'])){
					$users = D('Users');
					$user = $users->getUserById($wechat['user_id']);
					$users->saveLogin($user);
					$this->redirect("Users/index");
				}else{
					$data = array(
						'openId'    =>$chat['openid'],
						'partnerId' =>$chat['unionid'],
						'nickName'  =>$chat['nickname'],
						'picURL'    =>base64_encode($chat['headimgurl']),
						'platform'  =>'wechat'
					);
					$this->redirect("Users/binding", $data);
				}
			}else{
				$this->error('获取微信用户信息失败');
			}
        }else{
			$this->error('微信返回数据有误');
		}
    }

	/**
	 * 微信扫码登录绑定手机界面
	 */
	public function binding(){
		$weId      = I('get.id');
		$openId    = I('get.openId');
		$partnerId = I('get.partnerId');
		$nickName  = I('get.nickName');
		$picURL    = base64_decode(I('get.picURL'));
		$platform  = I('get.platform');

		if(!empty($weId)){
			$uw = D('UserWechat');
			$wechat = $uw->getWechatByWechatId($weId);
			$openId    = $wechat['openid'];
			$partnerId = $wechat['unionid'];
			$nickName  = $wechat['nickname'];
			$picURL    = $wechat['headimgurl'];
			$platform  = 'wechat';
			
		}
	
		$this->assign('openId', $openId);
		$this->assign('partnerId', $partnerId);
		$this->assign('nickName', $nickName);
		$this->assign('picURL', $picURL);
		$this->assign('platform', $platform);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Users/binding');
	}

	/**
	 * 检查手机号码是否有发送记录
	 */
	public function checkMobileSms(){		
		$mobile = I('post.mobile');
		$usercode = D('UserCode');
		$data = $usercode->get($mobile);
		echo json_encode($data);
	}

	/**
	 * 微信扫码登录绑定手机界面
	 */
	public function doBindingMobile(){
		$rd        = array('status'=>-1, 'msg'=>'绑定失败');
		$mobile    = I('post.mobile');
		$password  = substr($mobile, -6);
		$smscode   = I('post.smscode');
		$openId    = I('post.openId');
		$partnerId = I('post.partnerId');
		$nickName  = I('post.nickName');
		$picURL    = base64_decode(I('post.picURL'));
		$platform  = I('post.platform');
		
		$users    = D('Users');
		$usercode = D('UserCode');
		$uw       = D('UserWechat');
		
		if(empty($mobile) || empty($partnerId)){
			$rd = array('status'=>-2, 'msg'=>'参数错误');
		}else{
			$uc = $usercode->check($mobile, $smscode);   // 验证短信验证码
			if($uc == -1){
				$rd = array('status'=>-3, 'msg'=>'短信验证码超时');
			}else if($uc == -2){
				$rd = array('status'=>-4, 'msg'=>'短信验证码错误');
			}else{
				$user = $users->getUserByMobile($mobile);
				if(empty($user)){
					$rs = $users->regist($mobile, $password);
					if($rs['userId']>0){                       // 注册成功
						// 发送密码
						sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
						
						// 加载用户信息				
						$user = $users->getUserAndInfoById($rs['userId']);
						
						// 保存会员昵称
						$nickName = preg_replace("/[^\x{4e00}-\x{9fa5}]/iu",'',$nickName);  // 过滤特殊字符
						if(!empty($nickName)){
							$uname = strlen($nickName)>21?msubstr($nickName, 0, 7, 'utf-8', false):$nickName;
							if(!empty($uname)){
								$users->updateUser(array('user_id'=>$rs['userId'], 'uname'=>$uname));
							}
						}
						
						// 保存微信头像为会员头像
						/*if($picURL != '' && !empty($picURL)){
							$head = CBWDownloadFile($picURL, './Public/User/');
							if(!empty($head)){
								if(filesize('./Public/User/'.$head) > 0){
									$users->updateUserInfo(array('user_id'=>$rs['userId'], 'thumb'=>$head));
									@unlink('./Public/User/'.$user['thumb']);
								}else{
									@unlink('./Public/User/'.$head);
								}
							}
						}*/
					}
				}
				
				$wechat = $uw->getWechatByUnionId($partnerId);
				if(empty($wechat)){
					$data = array(
						'user_id'    => $user['id'], 
						'openid'     => $openId,
						'unionid'    => $partnerId,
						'nickname'   => $nickName,
						'headimgurl' => $picURL
					);
					$uw->insertWechat($data);
				}else{
					$fid = ($wechat['fid'] == $user['id'])?0:$wechat['fid'];
					if($wechat['user_id']){  // user_id不为0,则检查用户是否是店铺或代理用户(店铺用户或代理用户不能成为粉丝)
						$wuser = $users->getUserById($wechat['user_id']);
						if($wuser['group_id'] > 0 || $wuser['is_agent'] > 0){
							$fid = 0;
						}
					}
					$data = array(
						'fid'        => $fid,
						'user_id'    => $user['id'], 
						'openid'     => $openId,
						'nickname'   => $nickName,
						'headimgurl' => $picURL
					);
					$uw->updateWechat($wechat['id'], $data);
				}

				$users->saveLogin($user);  // 保存会员登录信息
				$usercode->del($mobile);   // 删除短信验证码相关记录
				$rd = array('status'=>$user['id']);
			}
		}
		echo json_encode($rd);
	}

	/**
	 * APP端微信授权登录操作
	 */
	public function AppWechatLogin(){
		$users = D('Users');
		$uw    = D('UserWechat');
		
		if(!empty($_POST)){
			$token     = $_POST['token'];
			$openid    = $_POST['openid'];
			$sign      = $_POST['sign'];
			$_userjson = json_decode($token, true);
			if(isset($_userjson['openid'])){           
			   $userjson = $_userjson;          
		    }else{
				$ch = curl_init();
				$str ='https://api.weixin.qq.com/sns/userinfo?access_token='.$token.'&openid='.$openid;
				curl_setopt($ch, CURLOPT_URL, $str);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
				$output = curl_exec($ch);          
				$output = json_decode($output, true);
				if(isset($output['openid'])){
					$userjson=$output;              
				}
			}
			$wechat = $uw->getWechatByUnionId($userjson['unionid']);
			$data = array(
				'openid'         => $openid,
				'sign'           => $sign,
				'unionid'        => $userjson['unionid'],
				'nickname'       => $userjson['nickname'],
				'sex'       	 => $userjson['sex'],
				'country'        => $userjson['country'],
				'province'       => $userjson['province'],
				'city'       	 => $userjson['city'],
				'headimgurl'     => $userjson['headimgurl'],
				'subscribe'      => $userjson['subscribe'],
				'subscribe_time' => $userjson['subscribe_time']
			);
			if(empty($wechat)){
				$uw->insertWechat($data);
			}else{
				if(!empty($wechat['user_id'])){
					$uw->updateWechat($wechat['id'], $data);
				}
			}
		}else{
			$openid = I('openid');
			$wechat = $uw->getWechatByOpenId($openid);
			if(!empty($wechat['user_id'])){
				$user   = $users->getUserById($wechat['user_id']);
				if(!empty($user)){
					$users->saveLogin($user);
				}			
				$this->redirect("Users/index");
			}else{
				$this->redirect("Users/binding", array('id'=>$wechat['id']));
			}
		}
	}

    /*
     * 生成验证码
     */
    public function imgCode(){
        // 检查图形验证码
        $verify = new \Think\Verify();
        $verify->useImgBg = true;//开启验证码背景
        //$verify->useZh = true;//中文验证码
        $verify->entry(1);
    }

	/**
	 * 验证登陆
	 * 
	 */
	public function doLogin(){
		$rd = array('status'=>-1);
		if (!IS_POST) {
			$rd["status"] = -2;  // 非法请求!
		}else{
            //验证码验证
            if(session('login_times')){
                //验证码验证
                if(session('login_times')){
                    // 检查图形验证码
                    $configuration=C('TENCENT_IMG_CODE');
                    $Ticket = I('ticket'); //票据
                    $Randstr =I('randstr');//随机串
                    $UserIP = get_client_ip(); //客户端ip

                    $params = array(
                        "aid" => $configuration['appid'],//appid
                        "AppSecretKey" => $configuration['AppSecretKey'], //AppSecretKey
                        "Ticket" => $Ticket,
                        "Randstr" => $Randstr,
                        "UserIP" => $UserIP
                    );
                    $paramstring = http_build_query($params);
                    $content = verifyTencentImgCode($paramstring,1,0);
                    $result = json_decode($content,true);
                    if($result){
                        if($result['response']!=1){
                            echo json_encode(['status'=>-5,'msg'=>$result['err_msg']]) ;
                            exit;
                        }
                    }else{
                        echo json_encode(['status'=>-5,'msg'=>'请求失败！']) ;
                        exit;
                    }
                }
            }

			$loginType  = I('loginType');
			$loginName  = I('loginName');
			$loginPwd   = I('loginPwd');
			$loginCode  = I('loginCode');
			$controller = session('jump.controller');
			$action     = session('jump.action');
			if(!empty($controller) && !empty($action)){
				$rd["refer"] = U($controller.'/'.$action);
			}else{
				$rd["refer"] = U('Users/index');
			}

			$users = D('Users');
			
			if($loginType == '1'){			
				$user = $users->checkLogin($loginName, $loginPwd);
				if($user['status'] == 0){
					$rd["status"] = -3;  // 登陆失败，账号被锁
				}else if($user['status'] == -1){
					$rd["status"] = 0;  // 登陆失败，账号或密码错误
					$rd["failed"] = $users->saveLoginFailed($loginName);  // 登陆失败次数
                    session('login_times',$rd["failed"]);
				}else{
					$users->saveLogin($user);  // 保存会员登录信息
					if($user['login_failed'] > 0){
						$users->updateUser(array('user_id'=>$user['id'], 'login_failed'=>0));  // 重置登录失败次数
					}
					$rd["status"] = 1;
				}
			}else{
				$usercode = D('UserCode');
				$uc = $usercode->check($loginName, $loginCode);   // 验证短信验证码
				if($uc == -1){
					$rd["status"] = -4;  // 登陆失败，验证码超时
				}else if($uc == -2){
					$rd["status"] = -5;  // 登陆失败，验证码错误
				}else{
					$usercode->del($loginName);   // 删除user_code表相关记录
					$user = $users->getUserByMobile($loginName);
					$users->saveLogin($user);  // 保存会员登录信息
					if($user['login_failed'] > 0){
						$users->updateUser(array('user_id'=>$user['id'], 'login_failed'=>0));  // 重置登录失败次数
					}
					$rd["status"] = 1;
				}
			}
		}
		echo json_encode($rd);
	}
	
	/**
     * 注册界面
     * 
     */
	public function regist(){
        $tencent_config=C('TENCENT_IMG_CODE');
        $this->assign("appid",$tencent_config['appid']);
		$this->display('Users/regist');
	}
    /**
     * 注册界面
     *
     */
    public function checkTencentImgCode(){
        // 检查图形验证码
        $configuration=C('TENCENT_IMG_CODE');
        $Ticket = I('ticket'); //票据
        $Randstr =I('randstr');//随机串
        $UserIP = get_client_ip(); //客户端ip

        $params = array(
            "aid" => $configuration['appid'],//appid
            "AppSecretKey" => $configuration['AppSecretKey'], //AppSecretKey
            "Ticket" => $Ticket,
            "Randstr" => $Randstr,
            "UserIP" => $UserIP
        );
        $paramstring = http_build_query($params);
        $content = verifyTencentImgCode($paramstring,1,0);
        $result = json_decode($content,true);
        if($result){
            if($result['response']!=1){
                echo json_encode(['status'=>-5,'msg'=>$result['err_msg']]) ;
                exit;
            }else{
                echo json_encode(['status'=>1,'msg'=>'验证成功！']) ;
                exit;
            }
        }else{
            echo json_encode(['status'=>-5,'msg'=>'请求失败！']) ;
            exit;
        }
    }

	/**
	 * 新用户注册
	 */
	public function doRegist(){
		$rs = array();
		$loginName  = I('post.loginName');
		$loginPwd   = I('post.loginPwd');
		$codeword   = I('post.codeword');
		$controller = session('jump.controller');
		$action     = session('jump.action');

		$usercode = D('UserCode');
		$uc = $usercode->check($loginName, $codeword);   // 验证短信验证码
		if($uc == -1){
			$rs["status"] = -3;
			$rs['msg'] = '短信验证码超时';
		}else if($uc == -2){
			$rs["status"] = -4;
			$rs['msg'] = '短信验证码错误';
		}else{
			$users = D('Users');
			$rs = $users->regist($loginName, $loginPwd);
			if($rs['userId']>0){                       // 注册成功	
				// 删除短信验证码相关记录
				$usercode->del($loginName);
				// 加载用户信息				
				$user = $users->getUserById($rs['userId']);
				if(!empty($user)){
					$users->saveLogin($user);  // 保存会员登录信息
					if(!empty($controller) && !empty($action)){
						$rs["refer"] = U($controller.'/'.$action);
					}else{
						$rs["refer"] = U('Users/index');
					}
				}
			}	
		}
		echo json_encode($rs);
	}

	/**
	 * 用户退出
	 */
	public function logout(){
		cookie(null,'cbw_');
		session('user', NULL);
		session('jump', NULL);
		session('gws', NULL);
		session('gws_count', 0);
		
		// 店铺系统退出
		cookie(null,'system_');
		session('shop', NULL);
		
		$this->redirect("Users/login");    //页面跳转
	}
	
	/**
	 * 忘记密码
	 */
    public function forget(){
        $tencent_config=C('TENCENT_IMG_CODE');
        $this->assign("appid",$tencent_config['appid']);
    	$this->display();
    }
    
    /**
     * 找回密码
     */
    public function findPass(){
    	$rs = array();
		$loginName = I('post.loginName');
		$loginPwd  = I('post.loginPwd');
		$codeword  = I('post.codeword');
		
		$usercode = D('UserCode');
		$uc = $usercode->check($loginName, $codeword);   // 验证短信验证码
		if($uc == -1){
			$rs["status"] = -3;
			$rs['msg'] = '短信验证码超时';
		}else if($uc == -2){
			$rs["status"] = -4;
			$rs['msg'] = '短信验证码错误';
		}else{
			$users = D('Users');
			$user = $users->getUserByMobile($loginName);
			$rs = $users->updatePass($user['id'], $loginPwd);
			if($rs['status']>0){            // 修改密码成功	
				// 删除短信验证码相关记录
				$usercode->del($loginName);
				$rs["refer"] = U('Users/login');
			}	
		}
		echo json_encode($rs);
    }
    
    /**
     * 会员中心页面
     */
	public function index(){
		$this->isUserLogin();
		
		$users  = D('Users');
		$orders = D('Orders');
		$uw     = D('UserWechat');
		$us     = D('UserShop');

        session('login_times',null);//重置登录次数
		$userId = session('user.userid');
		$shopId = session('shop.id');
		
		$user = $users->getUserAndInfoById($userId);
		$user['allow'] = $user['money'] - $user['frozen_money'];

		// 店铺信息
		if($user['group_id'] > 0){
			$shop = $us->getShopByUserId($userId);
		}
		
		// 店铺未读可以报价数量
		if($shopId > 0){
			$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));
		}
		
		// 我的粉丝数量
		$user['fans'] = $uw->getWechatsCountByUserId($user['id']);
		$this->assign("user", $user);
		
		// 用户中心订单数
		$orderCount = $orders->getOrderCount(array('userId'=>$user['id']));
		
		$this->assign("shop", $shop);
		$this->assign("orderCount", $orderCount);
		$this->assign('offerCount', $offerCount);

		$this->display();
	}
    
    /**
     * 分享
     */
	public function share(){
		$dir     = date("Ym");
		$dir2    = date("Ymd");
		$userId  = session('user.userid');
		$shopId  = session('shop.id');
		$imgFile = $dir."/share_".$shopId.".png";
		$groupId = session('user.groupid');
		($groupId != 1) && $this->error('该用户组权限不足');
		empty($userId) && $this->error('参数错误');
		
		$users = D('Users');
		$us    = D('UserShop');
		
		$user = $users->getUserAndInfoById($userId);
		$this->assign("user",$user);
		
		$wechatObj = new \Think\WechatApi();
		$package = $wechatObj->getSignPackage();

		if(!file_exists("./Public/Share/".$imgFile)){
			// 生成公众号关注二维码
			for($i=0; $i<100; $i++){
				$qrcode = $wechatObj->qrcodeCreate($shopId, 2);
				if(!empty($qrcode)){
					break;
				}
			}
		
			$image = new \Think\Image();  //图片处理类

			$tempFile      = "./Public/User/qrcode.png";                           // 模板文件
			$saveFile      = "./Public/Share/".$dir."/share_".$shopId.".png";      // 保存文件
			$qrcodeFile    = "./Public/Temp/".$dir2."/qrcode_".$shopId.".jpg";     // 二维码文件
			$qrcodeFile240 = "./Public/Temp/".$dir2."/qrcode_".$shopId."_240.jpg"; // 二维码文件240
			
			// 添加二维码
			$image->open($qrcodeFile)->thumb(240, 240, \Think\Image::IMAGE_THUMB_SCALE)->save($qrcodeFile240);
			$image->open($tempFile)->water($qrcodeFile240, array(158,243))->save($saveFile);
			
			// 添加头像
			if(!empty($user['thumb'])){
				$headImg = "./Public/User/".$user['thumb'];
				$image->open($headImg)->thumb(66, 66, \Think\Image::IMAGE_THUMB_SCALE)->save($headImg);
			}else{
				$headImg = "./Public/User/default100.jpg";
			}
			$image->open($saveFile)->water($headImg, array(80,62))->save($saveFile);
			
			//添加昵称
			$image->open($saveFile)->text($user['uname'], './Public/Mobile/font/PingFang Normal.ttf', 20, '#000000', array(175,80))->save($saveFile);
		}
		
		$this->assign("imgFile",$imgFile);
		$this->assign("package",$package);
		$this->assign("shopId",$shopId);
		$this->assign("agent",CBWUserAgent());
		$this->assign("dir",$dir);
		$this->assign("title", "您身边的手机维修专家 -草包网");
		$this->display();
	}
    
    /**
     * 福利规则
     */
	public function explain(){
		$this->isUserLogin();
		$groupId = session('user.groupid');
		($groupId != 1) && $this->error('该用户组权限不足');
		
		$this->display();
	}
	
    /**
     * 跳到修改用户信息
     */
	public function info(){
		$this->isUserLogin();
		$users = D('Users');
		$user = $users->getUserAndInfoById();
		$this->assign("user", $user);
		$this->display();
	}
	
    /**
     * 跳到修改用户头像
     */
	public function avatar(){
		$this->isUserLogin();
		$this->display();
	}
	
	// 裁剪图片
	public function crop(){
		$userId = session('user.userid');
		
		$crop = new \Think\CropAvatar(
		  './Public/Temp/',
		  './Public/User/',
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
		);
		$result  = $crop -> getResult();
		$message = $crop -> getMsg();
		if(!empty($result)){
			$users = D('Users');
			$user = $users->getUserAndInfoById($userId);
			$rt = $users->updateUserInfo(array('user_id'=>$userId, 'thumb'=>$result));
			if($rt['status'] > 0){
				unlink("./Public/User/".$user['thumb']);                   // 删除旧头像
			}
		}

		echo json_encode(array('state'  => 200, 'message' => $message, 'result' => $result));
	}
	
	/**
     * 修改用户资料
     */
	public function edit(){
		$this->isUserLogin();
		$userId   = session('user.userid');
		$groupId  = session('user.groupid');
		
		$users = D('Users');
		$area  = D('Area');
		
		$user = $users->getUserAndInfoById();
		
		// 三级联动
		$info = $area->getAreaById($user['area_id']);
		$p = empty($info['one_id'])?'选择省':$info['one_id'];
		$c = empty($info['two_id'])?'选择市':$info['two_id'];
		$a = empty($info['id'])?'选择县/区':$info['id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("user", $user);
		$this->assign("groupId", $groupId);
		$this->display();
	}
	
	/**
     * 更新用户资料
     */
	public function updateinfo(){
		$this->isUserLogin();
		$userId   = session('user.userid');
		$groupId  = session('user.groupid');
		$uname    = I('post.uname');
		$uname    = strlen($uname)>21?msubstr($uname, 0, 7, 'utf-8', false):$uname;
		$truename = I('post.true_name');
		$sex      = I('post.sex');
		$area     = I('post.area');
		$address  = I('post.address');
		
		$users = D('Users');
		$u = $users->updateUser(array('user_id'=>$userId, 'uname'=>$uname));
		if($groupId == '0'){
			$i = $users->updateUserInfo(array('user_id'=>$userId, 'true_name'=>$truename, 'sex'=>$sex, 'area_id'=>$area, 'address'=>$address));
		}else{
			$i = $users->updateUserInfo(array('user_id'=>$userId, 'sex'=>$sex));
		}
		
		if($u['status'] == '1' || $i['status'] == '1'){
			$this->redirect("Users/info");
		}else{
			$this->error('更新失败');
		}
	}
	
	/**
     * 好友管理
     */
	public function friends(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$groupId = session('user.groupid');
		($groupId != 1) && $this->error('该用户组权限不足');

		$orders = D('Orders');
		$uw     = D('UserWechat');
		$friends = $uw->getWechatsByUserId($userId);
		foreach($friends as &$vo){
			$money   = 0;
			$harvest = 0;
			$wait    = 0;
			if(!empty($vo['user_id'])){
				$os = $orders->getOrderList(array('userId'=>$vo['user_id'], 'type'=>0, 'status'=>5, 'date'=>$vo['fans_time']));
				foreach($os as $v){
					$money   += $v['deail_price']-$v['shipping_fee'];
					$harvest += ($v['deail_price']-$v['shipping_fee'])*0.02;
				}
			}
			$vo['nickname'] = empty($vo['nickname'])?'暂缺':$vo['nickname'];
			$vo['money']    = number_format($money, 2, '.', '');
			$vo['harvest']  = number_format($harvest, 2, '.', '');
			$vo['times']    = date("Y-m-d", $vo['fans_time']);
		}
		unset($vo);
		$this->assign("friends", $friends);	
		
		$this->display();
	}
	
	/**
	 * 加载更多粉丝
	 * 
	 */
	public function loadMoreFriends(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$orders = D('Orders');
		$uw     = D('UserWechat');
		$fans = $uw->getWechatsByUserId($userId, $counter*$num, $num);
		foreach($fans as &$vo){
			$money   = 0;
			$harvest = 0;
			$wait    = 0;
			if(!empty($vo['user_id'])){
				$os = $orders->getOrderList(array('userId'=>$vo['user_id'], 'type'=>0, 'status'=>5, 'date'=>$vo['fans_time']));
				foreach($os as $v){
					$money   += $v['deail_price']-$v['shipping_fee'];
					$harvest += ($v['deail_price']-$v['shipping_fee'])*0.02;
				}
			}
			$vo['nickname'] = empty($vo['nickname'])?'暂缺':$vo['nickname'];
			$vo['money']    = number_format($money, 2, '.', '');
			$vo['times']    = date("Y-m-d", $vo['fans_time']);
			$vo['harvest']  = number_format($harvest, 2, '.', '');
		}
		unset($vo);
		echo json_encode($fans);
	}
	
	/**
     * 草包豆管理
     */
	public function virtuals(){
		$this->isUserLogin();
		$userId = session('user.userid');
		$month  = I('get.month');
		$month  = empty($month)?date('m'):$month;
		
		$users = D('Users');
		$user = $users->getUserAndInfoById();
		$virtual = $users->getVirtualByObj(array('userId'=>$userId, 'month'=>$month, 'm'=>0, 'n'=>8));
		$this->assign("virtual", $virtual);
		$this->assign("user", $user);
		$this->assign("year", date('Y'));
		$this->assign("month", $month);
		
		$this->display();
	}
	
	/**
     * 更多草包豆记录
     */
	public function loadMoreVirtual(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$month   = I('post.month');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$users = D('Users');
		$data = $users->getVirtualByObj(array('userId'=>$userId, 'month'=>$month, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as $key=>$vo){
			$data[$key]['md'] = date('m-d',$vo['create_time']);
			$data[$key]['hi'] = date('H:i',$vo['create_time']);
		}
		echo json_encode($data);
	}
	
	/**
     * 草包豆充值
     */
	public function recharge(){
		$this->isUserLogin();
		
		$userId = session('user.userid');

		$users    = D('Users');
		$products = D('Products');
		
		$user    = $users->getUserAndInfoById($userId);
		$product = $products->getProductAttrByProductId(722);
		$product = $product[0];
		foreach($product['list'] as $key=>&$vo){
			if($key == 0){
				$vo['virtual'] = $vo['attr_value']*100;
			}else{
				$vo['virtual']      = $vo['attr_value']*100;
				$vo['give_virtual'] = $key*10*$vo['attr_value'];
			}
		}
		unset($vo);

		$this->assign("user", $user);
		$this->assign("product", $product);
		$this->assign("title", "账户充值");
		$this->display();
	}
	
    /**
     * 充值操作
     */
	public function doRecharge(){
		$this->isUserLogin();
		
		$id      = I('post.id');
		$payLink = I('post.link');
		$userId  = session('user.userid');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$areaId  = 3008;

		$orders   = D('Orders');
		$products = D('Products');
		$users    = D('Users');
		$us       = D('UserShop');
		$ua       = D('UserAddress');
		
		$user = $users->getUserById($userId);
		if($user['group_id'] > 0){
			$shop = $us->getMainShopByUserId($user['id']);
			$areaId       = $shop['area_id'];
			$address      = $shop['address'];
			$reciverName  = $shop['user_name'];
			$reciverPhone = $shop['user_phone'];
		}else{
			// 默认地址
			$addr = $ua->getDefaultAddressByUserId($userId);
			if(!empty($addr)){
				$areaId       = $addr['area_id'];
				$address      = $addr['street'];
				$reciverName  = $addr['reciver_user'];
				$reciverPhone = $addr['reciver_phone'];
			}
		}
		
		// 商品信息
		$product = $products->getRelationByProductIdAndAttrId(722, $id);
		
		$orderSn = CBWCreateOrderNumber();
		$data    = array(
			'user_id'       => $userId,
			'agent_id'      => 0,
			'order_sn'      => $orderSn,
			'order_type'    => 0,
			'order_date'    => time(),
			'deail_price'   => $product['one_price'],
			'order_status'  => 1,
			'pay_status'    => 0,
			'area_id'       => $areaId,
			'address'       => $address,
			'reciver_user'  => $reciverName,
			'reciver_phone' => $reciverPhone,
			'send_time'     => 0,
			'is_invoice'    => 0,
			'pay_link'      => $payLink
		);
		$orderId = $orders->insertOrders($data);
		if($orderId){
			// 添加订单附表
			$schedule[0]['shop_id']       = 0;
			$schedule[0]['shipping_way']  = $shipping['sid'];
			$schedule[0]['is_shipping']   = 1;
			$schedule[0]['remarks']       = '';
			$schedule[0]['shipping_fee']  = 0;
			$schedule[0]['virtual_money'] = 0;
			$schedule[0]['shop_money']    = $product['one_price'];
			$orders->insertOrderSchedule($schedule, $orderId);
			
			// 添加购物订单表
			$goods[0]['pro_id']        = $product['id'];
			$goods[0]['pro_price']     = $product['one_price'];
			$goods[0]['two_price']     = $product['two_price'];
			$goods[0]['three_price']   = $product['three_price'];
			$goods[0]['pro_number']    = 1;
			$goods[0]['pro_attr']      = $product['pro_attr'];
			$goods[0]['virtual_money'] = 0;
			$orders->insertOrderProducts($goods, $orderId);

			$this->redirect('Buy/pay', array('id'=>$orderSn));
			exit;
		}else{
			$this->error('订单生成失败,请联系管理员',U('Buy/cart'));
			exit;
		}
	}
	
	/**
     * 余额管理
     */
	public function money(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$users  = D('Users');
		$orders = D('Orders');
		
		$user = $users->getUserAndInfoById();
		$user['allow']  = $user['money'] - $user['frozen_money'];
		$this->assign("user", $user);
		
		$data = $users->getMoneyByObj(array('userId'=>$userId, 'm'=>0, 'n'=>12));
		foreach($data as &$vo){
			if($vo['order_id'] > 0){
				$vo['order'] = $orders->getOrderById($vo['order_id']);
			}
		}
		unset($vo);
		
		$this->assign("moneys", $data);
		
		$this->display();
	}
	
	/**
     * 余额收支明细
     */
	public function moneys(){
		$this->isUserLogin();
		$userId = session('user.userid');
		$month  = I('get.month');
		$month  = empty($month)?date('m'):$month;
		
		$users  = D('Users');
		$orders = D('Orders');
		
		$user = $users->getUserAndInfoById();
		$data = $users->getMoneyByObj(array('userId'=>$userId, 'month'=>$month, 'm'=>0, 'n'=>8));
		foreach($data as &$vo){
			if($vo['order_id'] > 0){
				$vo['order'] = $orders->getOrderById($vo['order_id']);
			}
		}
		unset($vo);
		
		$this->assign("moneys", $data);
		$this->assign("user", $user);
		$this->assign("year", date('Y'));
		$this->assign("month", $month);
		
		$this->display();
	}
	
	/**
     * 更多余额收支明细
     */
	public function loadMoreMoneys(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$month   = I('post.month');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$users = D('Users');
		$orders = D('Orders');
		
		$data = $users->getMoneyByObj(array('userId'=>$userId, 'month'=>$month, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			if($vo['order_id'] > 0){
				$vo['order'] = $orders->getOrderById($vo['order_id']);
			}
			$vo['md'] = date('m-d',$vo['create_time']);
			$vo['hi'] = date('H:i',$vo['create_time']);
		}
		unset($vo);
		echo json_encode($data);
	}
	
	/**
     * 余额提现
     */
	public function withdraw(){
		$this->isUserLogin();
		$userId = session('user.userid');
		$id     = I('get.id');
		
		$users = D('Users');
		$user = $users->getUserAndInfoById($userId);
		$user['alipay'] = unserialize($user['alipay']);
		$user['allow']  = $user['money'] - $user['frozen_money'];
		if(!empty($id)){
			$user['card'] = $users->getCardById($id);
		}else{
			$user['card'] = $users->getDefaultCardByUserId($userId);
		}

		$this->assign("user", $user);
		
		$this->display();
	}
	
	/**
     * 更改银行卡
     */
	public function mybank(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$users = D('Users');
		$cards = $users->getCardByUserId($userId);
		$this->assign("cards", $cards);
		
		$this->display();
	}
	
	/**
     * 提现提交
     */
	public function saveWithdraw(){
		$this->isUserLogin();
		$userId   = session('user.userid');
		$typeId   = I('post.type');
		$cardId   = I('post.card');
		$money    = I('post.money');
		$password = I('post.password');
		
		$users = D('Users');
		$user = $users->getUserAndInfoById($userId);
		$allow = $user['money'] - $user['frozen_money'];
		
		if($user['pay_password'] != sha1($password)){
			$this->error("支付密码错误", U("Users/withdraw"));exit;
		}
		if($money > $allow){
			$this->error("账户余额不足", U("Users/withdraw"));exit;
		}
		$data = array();
		$data['user_id']     = $userId;
		$data['type_id']     = $typeId;

		if($typeId == '1'){
			$data['infos']   = $user['alipay'];
		}elseif($typeId == '2'){
			$card = $users->getCardById($cardId);
			$data['infos']   = serialize(array('name'=>$card['true_name'], 'card_id'=>$card['id'],'bank_name'=>$card['name'],'card_no'=>$card['card_no']));
		}
		$data['money']       = $money;
		$data['create_time'] = time();
		$data['status']      = 0;
		$rt = $users->insertWithdraw($data);
		if($rt['status'] > 0){
			$users->decreaseMoney($userId, 0, $money, 4, '提现'); // 扣除账户余额
			$this->redirect('Users/money');
		}
		$this->error("提现失败", U("Users/withdraw"));exit;
	}
	
	/**
     * 支付管理
     */
	public function pay(){
		$this->isUserLogin();
		
		$users = D('Users');
		
		$user = $users->getUserAndInfoById();
		$user['alipay'] = unserialize($user['alipay']);
		$user['card']   = $users->getCardByUserId($user['id']);
		
		$this->assign("user", $user);
		$this->display();
	}
	
	/**
     * 添加快捷卡页面
     */
	public function addcard(){
		$this->isUserLogin();
		
		$users = D('Users');		
		
		$user = $users->getUserAndInfoById();

		$this->assign("user", $user);
		$this->display();
	}
	
	/**
     * 修改快捷卡页面
     */
	public function editcard(){
		$this->isUserLogin();
		
		$id = I('get.id');
		
		$users = D('Users');		
		
		$user = $users->getUserAndInfoById();
		$card = $users->getCardById($id);
		
		$this->assign("user", $user);
		$this->assign("card", $card);
		$this->display();
	}
	
	/**
     * 更新快捷卡
     */
	public function updateCard(){
		$this->isUserLogin();

		$cardid     = I('post.cardid');
		$name		= I('post.name');
		$idcardno	= I('post.idcardno');
		$bankcardno	= I('post.bankcardno');
		$bankid	    = I('post.bankid');
		$cardtype	= I('post.cardtype');
		$exp	    = I('post.exp');
		$cvv2	    = I('post.cvv2');
		$mobile		= I('post.mobile');
		$imgcode	= I('post.imgcode');
		$userId     = session('user.userid');
		$isdefault  = 1;

		if(!empty($imgcode)){                                            // 检查图形验证码
			$verify = new \Think\Verify(); 
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-1));exit;
			}
		}
		
		$users = D('Users');
		$banks = D('Banks');
		
		$check = $users->checkCardByUserIdCardNo($userId, $bankcardno);
		if($check['status'] == -1 && empty($cardid)){                    // 当添加时,检查账号中是否存在该银行卡
			echo json_encode(array('status'=>-2));exit;
		}
		
		if(empty($name) || empty($idcardno)){
			$user = $users->getUserAndInfoById($userId);
			$name	   = $user['true_name'];
			$idcardno  = $user['identity'];
			$isdefault = 0;
		}

		$bankcard = new \Think\BankCard();
		$auth = $bankcard->verifyCard(array('name'=>$name, 'idcardno'=>$idcardno, 'bankcardno'=>$bankcardno, 'tel'=>$mobile));
		$auth['isok'] = 1;
		$auth['code'] = 1;
		if($auth['isok'] == 1){
			if($auth['code'] == 1){
				if(empty($cardid)){
					$users->updateUserInfo(array('user_id'=>$userId, 'true_name'=>$name, 'identity'=>$idcardno));
					list($month, $year) = explode('-', $exp);
					$data = array(
						'user_id'      => $userId,
						'bank_id'      => $bankid,
						'card_type'    => $cardtype,
						'card_no'      => $bankcardno,
						'card_mobile'  => $mobile,
						'card_exp'     => substr($year, -2).$month,
						'card_cvv2'    => $cvv2,
						'is_default'   => $isdefault
					);
					$rt = $users->insertCard($data);
					if($rt['status'] > 0){
						echo json_encode(array('status'=>$rt['status']));exit;
					}
				}else{
					$data = array(
						'card_mobile'  => $mobile,
						'card_exp'     => substr($year, -2).$month,
						'card_cvv2'    => $cvv2
					);
					$rt = $users->updateCard($cardid, $data);
					if($rt['status'] > 0){
						echo json_encode(array('status'=>$rt['status']));exit;
					}
				}
			}
		}
		echo json_encode(array('status'=>0));exit;
	}
	
	/**
	 * 删除银行卡
	 */
    public function delCard(){
		$this->isUserLogin();

		$users = D('Users');
		
		$id = I('get.id');
		$users->deleteCard($id);
		$this->redirect("Users/pay");
    }
	
	/**
     * 更新支付宝账号
     */
	public function updateAlipay(){
		$this->isUserLogin();
		$userId = session('user.userid');
		$name   = I('post.name');
		$alipay = I('post.alipay');

		$users = D('Users');
		$rt = $users->updateUserInfo(array('user_id'=>$userId, 'alipay'=>serialize(array('name'=>$name, 'alipay'=>$alipay))));
		if($rt['status'] > 0){
			$this->redirect('Users/pay');
		}
		$this->error("更新失败", U("Users/pay"));exit;
	}
	
	/**
     * 安全中心
     */
	public function security(){
		$this->isUserLogin();
		$users = D('Users');
		$user = $users->getUserAndInfoById();
		$user['level'] = CBWSecurityLevel($user);
		$this->assign("user", $user);
		$this->display();
	}
	
	/**
     * 发送绑定/解绑邮件
     */
	public function bindEmailSend(){
		$this->isUserLogin();
		$userId   = session('user.userid');
		$mail     = I('post.email');
		$template = I('post.template');
		$action   = I('post.action');
		
		$users  = D('Users'); 
		$user = $users->getUserAndInfoById();
		
		if($action == 'bind'){
			// 检查账号是否已绑定邮箱
			if($user['email_state'] == 1){
				echo "-1";exit;
			}
			// 检查邮箱是否已绑定账号
			$rd = $users->checkUserEmail($mail);
			if($rd['status'] == '-1'){
				echo "-2";exit;
			}
			$code = md5($mail.md5($user['password']));
			$users->updateUser(array('user_id'=>$userId, 'email'=>$mail, 'active_code'=>$code));
		}elseif($action == 'unbind'){
			// 检查是否未绑定
			if($user['email_state'] == 0){
				echo "-1";exit;
			}
			$code = $user['active_code'];
		}
		
		// 获取邮件模板内容
		$system = D('System');
		$data = $system->loadEmailTemplate($template);
		
		$url = CBWDomain().'/Users/bindmail/id/'.$userId.'/action/'.$action.'/code/'.$code;
		$send = CBWSendMail($mail, $data['subject'], $data['content'], 'template', $url);
		echo $send;
	}
	
	/**
     * 绑定/解绑邮箱操作
     */
	public function bindmail(){
		$userId   = I('get.id');
		$code     = I('get.code');
		$action   = I('get.action');
		
		$users = D('Users');
		$user = $users->getUserAndInfoById($userId);
		
		if($action == 'bind'){
			if($user['active_code'] == $code){
				if($user['email_state'] == '1'){
					$this->error('邮箱已经绑定了,亲还要绑定多一次嘛?', U('Users/security'));
				}else{
					$users->updateUser(array('user_id'=>$userId, 'email_state'=>1));
					$this->redirect('Users/security');
				}
			}else{
				$this->error('参数错误,请重新发送绑定邮件.', U('Users/security'));
			}
		}elseif($action == 'unbind'){
			if($user['active_code'] == $code){
				if($user['email_state'] == '0'){
					$this->error('邮箱已经解绑了,亲还要解绑多一次嘛?', U('Users/security'));
				}else{
					if($user['mobile_state'] == '1'){
						$users->updateUser(array('user_id'=>$userId, 'email'=>'', 'email_state'=>0, 'active_code'=>''));
						$this->redirect('Users/security');
					}else{
						$this->error('解绑失败,解绑邮箱需要先绑定手机.', U('Users/security'));
					}
				}
			}else{
				$this->error('参数错误,请重新发送绑定邮件.', U('Users/security'));
			}
		}
	}
	
	/**
     * 绑定/解绑手机号码操作
     */
	public function bindmobile(){
		$mobile   = I('post.mobile');
		$code     = I('post.code');
		$action   = I('post.action');
		
		$users = D('Users');
		$usercode = D('UserCode');
		
		$user   = $users->getUserAndInfoById();
		$mobile = empty($mobile)?$user['mobile']:$mobile;
		$uc = $usercode->check($mobile, $code);   // 验证短信验证码
		if($uc == -1){
			echo json_encode(array('status'=>-1, 'msg'=>'短信验证码超时'));exit;
		}else if($uc == -2){
			echo json_encode(array('status'=>-2, 'msg'=>'短信验证码错误'));exit;
		}
		if($action == 'bind'){
			if($user['mobile_state'] == 1){
				echo json_encode(array('status'=>-3, 'msg'=>'账号已绑定手机'));exit;
			}
			$rd = $users->checkUserMobile($mobile);
			if($rd['status'] == '-1'){
				echo json_encode(array('status'=>-4, 'msg'=>'手机已绑定账号'));exit;
			}
			$rs = $users->updateUser(array('user_id'=>$user['user_id'], 'mobile'=>$mobile, 'mobile_state'=>1));
			if($rs['status'] == 1){
				$usercode->del($mobile);     // 删除短信验证码相关记录
				echo json_encode(array('status'=>1, 'msg'=>'绑定成功'));exit;
			}
		}elseif($action == 'unbind'){
			if($user['email_state'] == 0){
				echo json_encode(array('status'=>-3, 'msg'=>'解绑手机需要先绑定邮箱'));exit;
			}
			$rs = $users->updateUser(array('user_id'=>$user['user_id'], 'mobile'=>'', 'mobile_state'=>0));
			if($rs['status'] == 1){
				$usercode->del($mobile);     // 删除短信验证码相关记录
				echo json_encode(array('status'=>1, 'msg'=>'解绑成功'));exit;
			}
		}
		echo json_encode(array('status'=>0, 'msg'=>'操作失败'));exit;
	}
	
	/**
     * 更新密码
     */
	public function updatepwd(){
		$this->isUserLogin();
		$password    = I('post.password');
		$newPassword = I('post.newPassword');
		
		$users = D('Users');
		
		$user   = $users->getUserAndInfoById();
		if($user['password'] == sha1($password)){
			$rs = $users->updatePass($user['id'], $newPassword);
			if($rs['status'] == 1){
				echo json_encode(array('status'=>1, 'msg'=>'修改成功'));exit;
			}else{
				echo json_encode(array('status'=>0, 'msg'=>'修改失败'));exit;
			}
		}else{
			echo json_encode(array('status'=>-1, 'msg'=>'旧密码错误'));exit;
		}
	}
	
	/**
     * 设置支付密码
     */
	public function payPassword(){
		$this->isUserLogin();
		$code     = I('post.code');
		$password = I('post.password');
		
		$users    = D('Users');
		$usercode = D('UserCode');
		
		$user   = $users->getUserAndInfoById();
		$uc = $usercode->check($user['mobile'], $code);   // 验证短信验证码
		if($uc == -1){
			echo json_encode(array('status'=>-2, 'msg'=>'短信验证码超时'));exit;
		}else if($uc == -2){
			echo json_encode(array('status'=>-3, 'msg'=>'短信验证码错误'));exit;
		}
		$rs = $users->updateUser(array('user_id'=>$user['user_id'], 'pay_password'=>sha1($password)));
		if($rs['status'] == 1){
			$usercode->del($user['mobile']);     // 删除短信验证码相关记录
			echo json_encode(array('status'=>1, 'msg'=>'设置成功'));exit;
		}
		echo json_encode(array('status'=>0, 'msg'=>'设置失败'));exit;
	}
	
	/**
     * 实名认证
     */
	public function auth(){
		$this->isUserLogin();

		$users = D('Users');
		
		$user = $users->getUserAndInfoById($userId);
		if(!empty($user['true_name']) && !empty($user['identity'])){
			$this->error('账号已实名认证');
		}

		$this->display();
	}
	
	/**
     * 实名认证第二步
     */
	public function authnext(){
		$this->isUserLogin();

		$users = D('Users');
		
		$name     = I('post.name');
		$idcardno = I('post.idcardno');
		
		$user = $users->getUserAndInfoById($userId);
		if(!empty($user['true_name']) && !empty($user['identity'])){
			$this->error('账号已实名认证');
		}
		
		if(empty($name) || empty($idcardno)){
			$this->error('非法操作');
		}
		
		$this->assign("name", $name);
		$this->assign("idcardno", $idcardno);
		$this->display();
	}
	
	/**
     * 实名认证提交
     */
	public function submitAuth(){
		$this->isUserLogin();

		$name		= I('post.name');
		$idcardno	= I('post.idcardno');
		$bankcardno	= I('post.bankcardno');
		$bankid	    = I('post.bankid');
		$cardtype	= I('post.cardtype');
		$exp	    = I('post.exp');
		$cvv2	    = I('post.cvv2');
		$mobile		= I('post.mobile');
		$imgcode	= I('post.imgcode');
		$userId     = session('user.userid');

		if(!empty($imgcode)){                                            // 检查图形验证码
			$verify = new \Think\Verify(); 
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-1));exit;
			}
		}
		
		$users = D('Users');
		$banks = D('Banks');
		
		$check = $users->checkCardByUserIdCardNo($userId, $bankcardno);
		if($check['status'] == -1){                                       // 检查账号中该银行卡是否存在
			echo json_encode(array('status'=>-2));exit;
		}
		
		$user = $users->getUserAndInfoById($userId);
		if(!empty($user['true_name']) && !empty($user['identity'])){       // 检查账号是否已经实名认证
			echo json_encode(array('status'=>-3));exit;
		}

		$bankcard = new \Think\BankCard();
		$auth = $bankcard->verifyCard(array('name'=>$name, 'idcardno'=>$idcardno, 'bankcardno'=>$bankcardno, 'tel'=>$mobile));
		if($auth['isok'] == 1){
			if($auth['code'] == 1){
				$users->updateUserInfo(array('user_id'=>$userId, 'true_name'=>$name, 'identity'=>$idcardno));
				list($month, $year) = explode('-', $exp);
				$data = array(
					'user_id'      => $userId,
					'bank_id'      => $bankid,
					'card_type'    => $cardtype,
					'card_no'      => $bankcardno,
					'card_mobile'  => $mobile,
					'card_exp'     => substr($year, -2).$month,
					'card_cvv2'    => $cvv2,
					'is_default'   => 1
				);
				$rt = $users->insertCard($data);
				if($rt['status'] > 0){
					echo json_encode(array('status'=>$rt['status']));exit;
				}
			}
		}
		echo json_encode(array('status'=>0));exit;
	}
	
	/**
     * 支持银行
     */
	public function supportbank(){
		$this->isUserLogin();

		$banks = D('Banks');
		
		$deposits = $banks->getBank();
		$credits  = $banks->getBankByCredit();
		
		$this->assign("credits", $credits);
		$this->assign("deposits", $deposits);
		$this->display();
	}
	
	/**
     * 报价通知
     */
	public function offers(){
		$userId = session('user.userid');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
		$us      = D('UserShop');

		$data = $orders->getOrderOfferByUserId($userId, 0, 8);
		foreach($data as &$vo){
			$order = $orders->getOrderById($vo['order_id']);
			$vo['order_type'] = $order['order_type'];
			$vo['order_date'] = $order['order_date'];
			$vo['remarks']    = $order['remarks'];
			if($order['order_type'] == 1){
				$server = $orders->getOrderServerByOrderId($vo['order_id']);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$color    = $repairs->getColorById($server['color_id']);
					$vo['pro_name']   = $relation['pro_name'];
					$vo['color_name'] = $color['name'];
					$vo['plan_name']  = $relation['plan_name'];
				}
				$shop = $us->getShopByShopId($server['shop_id']);
			}elseif($order['order_type'] == 2){
				$recovery = $orders->getOrderRecoveryByOrderId($vo['order_id']);
				if($recovery['reco_id'] > 0){
					$repair = $repairs->getRepairById($recovery['reco_id']);
					$type   = $cat->getCat($repair['type_id']);
					$brand  = $cat->getCat($repair['brand_id']);
					$vo['pro_name'] = $repair['pro_name'];
					$vo['attrs']    = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				}
				$shop = $us->getShopByShopId($recovery['shop_id']);
			}
			$vo['shop_name']   = $shop['shop_name'];
			$vo['logo_image']  = $shop['logo_image'];
		}
		unset($vo);

		$this->assign("offers", $data);
		$this->assign("title", "报价通知");
		$this->display();
	}
	
	/**
	 * 加载更多报价通知
	 */
	public function loadMoreOffers(){
		$userId = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		$num     = intval($num?$num:8);
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
		$users   = D('Users');
		$us      = D('UserShop');
		
		$data = $orders->getOrderOfferIngByUserId(array('userId'=>$userId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$order = $orders->getOrderById($vo['order_id']);
			$vo['order_date'] = CBWTimeFormat($order['order_date'], 'm.d');
			$vo['order_type'] = $order['order_type'];
			$vo['order_date'] = $order['order_date'];
			$vo['remarks']    = $order['remarks'];
			if($order['order_type'] == 1){
				$server = $orders->getOrderServerByOrderId($vo['order_id']);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$color    = $repairs->getColorById($server['color_id']);
					$vo['pro_name']   = $relation['pro_name'];
					$vo['color_name'] = $color['name'];
					$vo['plan_name']  = $relation['plan_name'];
				}
				$shop = $us->getShopByShopId($server['shop_id']);
			}elseif($order['order_type'] == 2){
				$recovery = $orders->getOrderRecoveryByOrderId($vo['order_id']);
				if($recovery['reco_id'] > 0){
					$repair = $repairs->getRepairById($recovery['reco_id']);
					$type   = $cat->getCat($repair['type_id']);
					$brand  = $cat->getCat($repair['brand_id']);
					$vo['pro_name'] = $repair['pro_name'];
					$vo['attrs']    = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				}
				$shop = $us->getShopByShopId($recovery['shop_id']);
			}
			$vo['shop_name']   = $shop['shop_name'];
			$vo['logo_image']  = $shop['logo_image'];
		}
		unset($vo);
		
		echo json_encode($data);
	}
	
}