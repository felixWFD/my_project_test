<?php
namespace Mobile\Controller;
/**
 * 基础控制器
 */
use Think\Controller;
class BaseController extends Controller {
	public function __construct(){
		parent::__construct();
		// 系统配置
		$system = D('System');
		$GLOBALS['CONFIG'] = $system->loadConfigs();
		$this->assign('CONF',$GLOBALS['CONFIG']);
		
		// 如果COOKIE['cbw_auto']存在，并且用户不在登录状态
		$cbw = cookie('cbw_auto');
        if(isset($cbw) && !session('user.userid')){
            $cbw = explode('|',CBWCheckAutoLogin($cbw));
			$users = D('Users');
			$user = $users->getUserById($cbw[0]);
			if($user['status'] > 0){
				//$ip  = ip2long(I('server.REMOTE_ADDR'));
				//if($cbw[3] == $ip){
					// 保存登录SESSION
					session(array('name'=>'userLogin', 'prefix'=>'user'));
					session('userid',   $cbw[0]);
					session('groupid',  $cbw[1]);
					session('vip',      $cbw[2]);
					session('username', cookie('cbw_uname'));
				//}
			}
        }
		// 如果COOKIE['system_auto']存在，并且店铺不在登录状态
		$system = cookie('system_auto');
        if(isset($system) && !session('shop.id')){
            $system = explode('|',CBWCheckAutoLogin($system));
            //$ip  = ip2long(I('server.REMOTE_ADDR'));
            //if($system[1] == $ip){
				// 保存登录SESSION
				session(array('name'=>'shopLogin', 'prefix'=>'shop'));
				session('id',   $system[0]);
				session('name', cookie('system_sname'));
            //}
        }
		
		$cityId = $this->getDefaultCity();
		$area = D('Area');
		$currCity = $area->getCityById($cityId);
		$this->assign('currCity',$currCity);

		$this->assign("CBW_URL",CBWDomain());
	}
	
	/**
	* 推送消息到店铺
	* 包含(微信,手机短信)
	* @param $orderId 订单编号
	* @param $lng     经度
	* @param $lat     纬度
	* @param $shopId  店铺编号
	* @param $way     服务方式
	* @return
	*/
    public function message($orderId, $lng, $lat, $way, $shopId=0){
		$us      = D('UserShop');
		$uw      = D('UserWechat');
		$cat     = D('ProductCat');
		$repairs = D('Repairs');
		$orders  = D('Orders');
		
		$offer = 0;
		$warranty = 0;

		if($orderId > 0){
			$order = $orders->getOrderById($orderId);
            //order_type  0：购物订单  1：维修订单  2：回收订单
			if($order['order_type'] == 1){
				$first = "我需要手机维修服务，请亲及时报出你能维修的真实价格并联系我，谢谢！";
				$server = $orders->getOrderServerByOrderId($orderId);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$offer    = $relation['money'];
					if(!empty($relation['color_money'])){
						$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
						$offer = number_format($colormoneys[$server['color_id']], 2, '.', '');
					}
					$warranty = $relation['warranty'];
					$modelId  = $relation['product_id'];
					$type     = $cat->getCat($relation['type_id']);
					$brand    = $cat->getCat($relation['brand_id']);
					$keyword3 = $type['cat_name']." ".$brand['cat_name']." ".$relation['pro_name']." ".$server['name']." ".$relation['plan_name'];
				}else{
					$keyword3 = $order['remarks'];
				}
			}elseif($order['order_type'] == 2){
				$first = "我需要手机回收服务，请亲及时报出你能维修的真实价格并联系我，谢谢！";
				$recovery = $orders->getOrderRecoveryByOrderId($orderId);
				if($recovery['reco_id'] > 0){
					$repair = $repairs->getRepairById($recovery['reco_id']);
					$modelId  = $repair['id'];
					$type     = $cat->getCat($repair['type_id']);
					$brand    = $cat->getCat($repair['brand_id']);
					$keyword3 = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
					$offer    = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
				}else{
					$keyword3 = $order['remarks'];
				}
			}
			
			// 推送消息(微信/通知/手机短信)
			$wechatObj = new \Think\WechatApi();

			if($shopId > 0){
				$shops[0] = $us->getShopByShopId($shopId);
			}else{
				$shops = $us->getShopsByLngLat($lng, $lat, 0, 99, $way);
			}
			$i = 0;
			$ids = array();
			foreach($shops as $key=>$vo){
				if($vo['server_range'] > 0){
					$distance = CBWGetDistance($vo['lng'], $vo['lat'], $lng, $lat);
					$s = round($distance/1000);
					if($s > $vo['server_range']){
						continue;
					}
				}
				
				// 直营店直接报价
				if($vo['type_id'] == 2){
					$data = array(
						'order_id'      => $orderId,
						'shop_id'       => $vo['id'],
						'offer'         => $offer,
						'warranty'      => $warranty,
						'virtual_money' => 0,
						'offer_date'    => time(),
						'is_free'       => 1,
						'remarks'       => ''
					);
					$offerId = $orders->insertOrderOffer($data);
					if($offerId > 0){
						$orders->updateOrderOfferIng($orderId, $vo['id'], array('is_offer'=>1));
						if($i >= 5){
							$orders->updateOrders($orderId, array('is_offer'=>0));
							break;
						}
					}
				}
				
				// 店铺信息
				$isSwitch = 1;
				$umodel = $us->getUserModelByUserId($vo['user_id']);
				if($modelId > 0 && count($umodel) > 0){
					$model = $us->getUserModelByModelId($vo['user_id'], $modelId);
					if(empty($model)){
						$isSwitch = 0;
					}
				}
				
				if($isSwitch == 1){
					// 微信
					$wechat = $uw->getWechatByUserId($vo['user_id']);
					if(!empty($wechat['fakeid'])){
						$data = array(
							"touser"      => $wechat['fakeid'],
							"template_id" => "x-XCV1YsEyEjmY38EK20z3lJj7i_8mgOKscF0CvWgt0",
							"url"         => CBWDomain()."/offer/".$order["id"]."-".$vo['id'].".html",
							"topcolor"    => "#FF0000",
							"data"        => array(
								"first" => array(
									"value" => $first,
									"color" => "#173177"
								),
								"keyword1" => array(
									"value" => $order["order_sn"],
									"color" => "#173177"
								),
								"keyword2" => array(
									"value" => date("Y-m-d H:i:s"),
									"color" => "#173177"
								),
								"keyword3" => array(
									"value" => $keyword3,
									"color" => "#173177"
								),
								"keyword4" => array(
									"value" => "请点此链接跳转至订单页面，报价并联系客户，快速报价有助于提高首页排名。",
									"color" => "#173177"
								),
								"remark" => array(
									"value" => "点击详情报价",
									"color" => "#173177"
								)
							)
						);
						$wechatObj->sendTemplateMessage($data);
					}
					
					// 通知
					$content = '<p>我需要手机维修服务，请亲及时报出你能维修的真实价格并联系我，谢谢！</p>';
					$content .= '<p>业务：'.$order["order_sn"].'</p>';
					$content .= '<p>时间：'.date("Y-m-d H:i:s").'</p>';
					$content .= '<p>提醒内容：'.$keyword3.'</p>';
					$content .= '<p>处理建议：请点此链接跳转至订单页面，报价并联系客户，快速报价有助于提高首页排名。 <a href="'.CBWDomain().'/offer/'.$order["id"]."-".$vo['id'].'.html">点击报价</a></p>';
					$notice  = array('shop_id'=>$vo['id'], 'title'=>'业务信息通知，订单号：'.$order["order_sn"], 'content'=>$content, 'create_time'=>time());
					//$us->insertShopNotice($notice);
					
					// 手机短信
					//sendSmsByAliyun($vo['user_phone'], array(), 'SMS_116561090');  // 报价通知
					
					$i++;
					$ids[$key]['id']      = $vo['id'];
					$ids[$key]['user_id'] = $vo['user_id'];
				}
			}
			
			// 更新订单信息
			if(!empty($ids)){
				foreach($ids as $vo){
					$orders->insertOrderOfferIng(array('shop_id'=>$vo['id'], 'user_id'=>$vo['user_id'], 'order_id'=>$orderId));
				}
			}
			
			if($i > 0){
				session('server.message', 1);
			}
		}
    }
	
	/**
	* 推送消息到店铺(接单通知)
	* 包含(微信,手机短信)
	* @param $orderId 订单编号
	* @param $shopId  店铺编号
	* @return
	*/
    public function pushOrderMessage($orderId, $shopId){
		$us      = D('UserShop');
		$uw      = D('UserWechat');
		$orders  = D('Orders');
		$repairs = D('Repairs');
		
		$offer = 0;
		$warranty = 0;
		
		if($orderId > 0){
			$order = $orders->getOrderById($orderId);
			if($order['order_type'] == 1){
				$server = $orders->getOrderServerByOrderId($orderId);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$offer    = $relation['money'];
					if(!empty($relation['color_money'])){
						$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
						$offer = number_format($colormoneys[$server['color_id']], 2, '.', '');
					}
					$warranty = $relation['warranty'];
				}
				$first = "您有新的维修订单，请及时登录系统与顾客联系并处理。";
				$url   = CBWDomain()."/system/repair.html";
				$type  = "维修订单";
				if($server['server_way'] == 1){
					$type .= "-到站维修";
				}elseif($server['server_way'] == 2){
					$type .= "-上门维修";
				}
			}elseif($order['order_type'] == 2){
				$recovery = $orders->getOrderRecoveryByOrderId($orderId);
				if($recovery['reco_id'] > 0){
					$offer = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
				}
				$first = "您有新的回收订单，请及时登录系统与顾客联系并处理。";
				$url   = CBWDomain()."/system/recovery.html";
				$type  = "回收订单";
				if($server['reco_way'] == 1){
					$type .= "-到站回收";
				}elseif($server['server_way'] == 2){
					$type .= "-上门回收";
				}
			}
			
			$shop = $us->getShopByShopId($shopId);
			
			// 直营店直接报价
			if($shop['type_id'] == 2){
				$data = array(
					'order_id'      => $orderId,
					'shop_id'       => $shop['id'],
					'offer'         => $offer,
					'warranty'      => $warranty,
					'virtual_money' => 0,
					'offer_date'    => time(),
					'is_free'       => 1,
					'remarks'       => ''
				);
				$offerId = $orders->insertOrderOffer($data);
				if($offerId > 0){
					$orders->updateOrderOfferIng($orderId, $shop['id'], array('is_offer'=>1));
					$orders->updateOrders($orderId, array('is_offer'=>0));
				}
			}
				
			// 微信通知
			if($shop['is_wechat'] == 1){
				$wechat = $uw->getWechatByUserId($shop['user_id']);
				if(!empty($wechat['fakeid'])){
					$data = array(
						"touser"      => $wechat['fakeid'],
						"template_id" => "CjyDOCYBnCBKxs8jhMG5EwZYp6nnfSWCvPr5t5OgpQY",
						"url"         => $url,
						"topcolor"    => "#FF0000",
						"data"        => array(
							"first" => array(
								"value" => $first,
								"color" => "#173177"
							),
							"keyword1" => array(
								"value" => $order["order_sn"],
								"color" => "#173177"
							),
							"keyword2" => array(
								"value" => $type,
								"color" => "#173177"
							),
							"keyword3" => array(
								"value" => $order['reciver_user']." ".$order['reciver_phone']." ".$order['pro_addr'].$order['city_addr'].$order['addr'].$order['address'],
								"color" => "#173177"
							),
							"remark" => array(
								"value" => "如果问题请联系客服：400-6233-866",
								"color" => "#173177"
							)
						)
					);
					
					$wechatObj = new \Think\WechatApi();
					$wechatObj->sendTemplateMessage($data);
				}
			}
			
			// 手机短信通知
			if($shop['is_sms'] == 1){
				sendSmsByAliyun($shop['user_phone'], array('name'=>$order['reciver_user'],'tel'=>$order['reciver_phone']), 'SMS_116591038');  // 新订单
			}
		}
    }
	
	/**
	 * 空操作处理
	 */
    public function _empty(){
        header("HTTP/1.0 404 Not Found");//使HTTP返回404状态码 
        $this->display("Public:404");
    }
	
	/**
     * 发送短信验证码
     */
	public function sendSms(){
		$mobile     = I('post.mobile');         // 手机号码
		$imgcode    = I('post.imgcode');        // 图形验证码
		$tempid     = I('post.tempid');         // 短信模板编号
		$testid     = I('post.testid');         // 测试编号
		$ischeck    = I('post.ischeck');        // 是否检查手机号码
		$ip         = I('server.REMOTE_ADDR');  // 获取IP地址
		$code       = mt_rand(100000, 999999);  // 生成验证码
		$nowtime    = time();                   // 当前时间
		$code_count = 1;
		
		//记录测试信息
		/*if(!empty($testid)){
			$modelTest = M("test");
			$data=array('mobile'=>$mobile,'val'=>$testid);
			if($modelTest->create($data)){
				$modelTest->add();
			}
		}else{
			$modelTest = M("test");
			$data=array('mobile'=>$mobile,'val'=>$tempid.'-33');
			if($modelTest->create($data)){
				$modelTest->add();
			}
		}*/
		
		if($ischeck == 1){
			$users    = D('Users');
			$user = $users->getUserByMobile($mobile);
			if(empty($user)){
				echo json_encode(array('status'=>-1, 'msg'=>'手机号码不存在'));exit;
			}
		}
		
		if(empty($tempid)){
			echo json_encode(array('status'=>-2, 'msg'=>'短信模板为空'));exit;
		}
		
		if(!empty($imgcode)){
			// 检查图形验证码
			$verify = new \Think\Verify();
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-3, 'msg'=>'图形验证码错误！请重试'));exit;
			}
		}
		
		$smsip    = D('SmsIp');
		$usercode = D('UserCode');
		
		$sms = $smsip->get($ip);
		if(is_array($sms)){
			if($sms['sms_count'] >= 6){
				echo json_encode(array('status'=>-4, 'msg'=>'您当日IP累计获取验证码已达上限，请您次日再试'));exit;
			}
			$smsip->edit(array('id'=>$sms['id'], 'sms_count'=>$sms['sms_count']+1));         // 更新IP操作次数
		}else{
			$sms_count = 1;
			$smsip->insert(array('ip'=>$ip, 'add_time'=>$nowtime, 'sms_count'=>$sms_count)); // 插入IP操作次数
		}
		
		$codeinfo = $usercode->get($mobile);
		if(is_array($codeinfo)){
			if($codeinfo['code_count'] >= 6){
				echo json_encode(array('status'=>-5, 'msg'=>'当日手机号码累计获取验证码已达上限，请您次日再试'));exit;
			}
			if ($nowtime - $codeinfo['add_time'] < 90){
				echo json_encode(array('status'=>-6, 'msg'=>'操作过于频繁，请您稍后再试'));exit;
			}
			$code = $codeinfo['code'];
			$code_count = $codeinfo['code_count']+1;
		}

		// 发送模板短信
		$result = sendSmsByAliyun($mobile, array('code'=>$code), $tempid); // 验证码
		if($result == NULL ) {
			echo json_encode(array('status'=>-7, 'msg'=>'验证失败'));exit;
		}
		if($result->Message != 'OK'){
			echo json_encode(array('status'=>-8, 'msg'=>'发送失败'));exit; //  $datas=$result->statusCode."<br>";//echo "error msg:".$result->statusMsg;
		}else{
			if($code_count == 1){
				$usercode->insert(array('account'=>$mobile, 'code'=>$code, 'add_time'=>$nowtime, 'code_count'=>$code_count)); // 插入手机发送次数
			}else{
				$usercode->edit(array('id'=>$codeinfo['id'], 'code_count'=>$code_count));                                      // 更新手机发送次数
			}
			echo json_encode(array('status'=>$code_count, 'msg'=>'发送成功'));exit;
		}
		echo json_encode(array('status'=>0, 'msg'=>'未知错误'));exit;
	}
	
    /**
	 * 验证模块的短信验证码
	 */
	public function checkVerifySms(){
		$code   = I('post.code');
		$mobile = I('post.mobile');
		
		$uc = D('UserCode');

		// 检查验证码
		$rt = $uc->check($mobile, $code);
		if($rt > 0){
			echo true;exit;
		}
		echo false;exit;
	}
	
	/**
     * 检查银行卡归属哪家银行
     */
	public function checkCard(){
		$banks = D('Banks');

		$cardNo = I('post.card_no');

		$card8 = substr($cardNo, 0, 8); 
		$card  = $banks->getBankCardByNumber($card8);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		$card6 = substr($cardNo, 0, 6); 
		$card  = $banks->getBankCardByNumber($card6);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		$card5 = substr($cardNo, 0, 5); 
		$card  = $banks->getBankCardByNumber($card5);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		$card4 = substr($cardNo, 0, 4); 
		$card  = $banks->getBankCardByNumber($card4);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		echo false;
	}

	/**
	 * 产生中文验证码图片
	 * @return [source]
	 */
	public function getChinaVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		$verify = new \Think\Verify($config);
		$verify->useZh = true;
        $verify->fontttf = 'msyh.ttf';
		$verify->entry();
	}
	
	/**
	 * 产生数字验证码图片
	 * 
	 */
	public function getVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		// 导入Image类库
    	$verify = new \Think\Verify($config);
    	$verify->entry();
    }
	
	/**
	 * 产生数学验证码图片
	 * 
	 */
	public function getArithVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		// 导入Image类库
    	$verify = new \Think\Verify($config);
		$verify->arith = true;
		$verify->fontttf = 'msyh.ttf';
    	$verify->entry();
    }
   
    /**
	 * AJAX验证模块的码校验
	 */
	public function checkAjaxVerify(){
		
		$code = I('post.code');
		$verify = new \Think\Verify();

		// 检查验证码
		if ($verify->check($code)){
			echo true;exit;
		}
		echo false;exit;
	}
	
    /**
	 * 验证模块的码校验
	 */
	public function checkVerify(){
		
		$code = I('post.code');
		$verify = new \Think\Verify();

		// 检查验证码
		if ($verify->check($code)){
			return true;
		}
		return false;
	}
	
    /**
     * 核对单独的验证码
	 * $re = false 的时候不是ajax返回
	 * @param  boolean $re [description]
	 * @return [type]      [description]
	 */
	public function checkCodeVerify($re = true){
		$code = I('code');
		$verify = new \Think\Verify(array('reset'=>false));    
		$rs =  $verify->check($code);		
		if ($re == false) return $rs;
		else $this->ajaxReturn(array('status'=>(int)$rs));
	}
	
	/**
	 * 设置所在城市
	 */
	public function setDefaultCity($cityId){
		session('cityId',$cityId);
		setcookie("cityId", $cityId, time()+3600*24*90);
	}
	
	/**
	 * 定位所在城市
	 */
	public function getDefaultCity(){
		$area = D('Area');
		return $area->getDefaultCity();
	}
	
	/**
	 * 获取城市区号
	 */
    public function getCityNumber(){
		
		$city   = I('post.city');
		$area   = D('Area');
		$data = $area->getCityByName($city);
		echo $data['area_no'];
    }
	
	/**
     * ajax程序验证,只要不是会员都返回-999
     */
    public function isUserLogin() {
    	$userId = session('user.userid');
		if (empty($userId)){
			if(IS_AJAX){
				$this->ajaxReturn(array('status'=>-999,'url'=>'Users/login'));
			}else{
				$this->redirect("Users/login");
			}
		}
	}
	
    /**
	 * 单个上传图片不缩略图片
	 */
    public function uploadPic(){
		$upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/Temp/";                         // 图片保存文件夹路径
        $upload->maxSize = 2097152;                           // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                               // 设置附件上传目录
        $img = $upload->upload();
		
        echo $img['Filedata']['savepath'].$img['Filedata']['savename'];
    }
	
    /**
	 * 单个上传图片缩略图片
	 */
    public function uploadThumbPic(){
		$upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/Temp/";                         // 图片保存文件夹路径
        $upload->maxSize = 2097152;                           // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                               // 设置附件上传目录
        $img = $upload->upload();
		
		//图像缩略图
		$image = new \Think\Image();
		$image->open($rootPath.$img['Filedata']['savepath'].$img['Filedata']['savename']);
		$image->thumb('320', '320')->save($rootPath.$img['Filedata']['savepath'].$img['Filedata']['savename']);
		
        echo $img['Filedata']['savepath'].$img['Filedata']['savename'];
    }

    /**
     * 执行城市三级联动下拉框获取操作
     */
    public function changeSelect(){
		
        $id   = I('post.id');
		$type = I('post.type');

		$area = D('Area');

        if($type == 'province'){
			$list = $area->getCityByProvinceId($id);
		}else{
			$list = $area->getAreaByCityId($id);
		}
		$str = '';
		foreach($list as $vo){
			$str .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
		}
        echo $str;
		exit;
    }
}