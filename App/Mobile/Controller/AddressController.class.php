<?php
namespace Mobile\Controller;
/**
 * 地址控制器
 */
class AddressController extends BaseController{
    /**
	 * 地址列表
	 */
    public function index(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$userAddress = D('UserAddress');
		$address = $userAddress->getAddressByUserId($userId);
		
		// 三级联动
		$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$c = \Think\Area::city($cityArray);
		
		$this->assign("city", $c);
		$this->assign("address", $address);
	
		$this->display();
    }
	
	/**
	 * 修改地址
	 */
	public function edit(){
		
		$userId = session('user.userid');
		$id     = I('get.id');
		
		$userAddress = D('UserAddress');
		$address = $userAddress->getAddressById($id);
		
		// 三级联动
		$p = empty($address['one_id'])?'选择省':$address['one_id'];
		$c = empty($address['two_id'])?'选择市':$address['two_id'];
		$a = empty($address['area_id'])?'选择县/区':$address['area_id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("address", $address);
		
		$this->display();
	}
	
	/**
	 * 更新地址
	 */
	public function updateAdderss(){
		
		$userId = session('user.userid');
		$id     = I('post.id');
		
		$data['user_id']       = $userId;
		$data['reciver_user']  = I('post.reciver_user');
		$data['reciver_phone'] = I('post.reciver_phone');
		$data['area_id']       = I('post.area');
		$data['street']        = I('post.street');
		
		$userAddress = D('UserAddress');
		if(empty($id)){
			$rs = $userAddress->insertAddress($data);
		}else{
			$rs = $userAddress->updateAdderss($id, $data);
		}
		$this->redirect('Address/index');
	}
	
	/**
	 * 编辑地址
	 */
    public function address(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$userAddress = D('UserAddress');
		$address = $userAddress->getAddressByUserId($userId);
		$this->assign("address", $address);
	
		$this->display();
    }
	
	/**
	 * 删除地址
	 */
    public function deleteAddress(){
		$this->isUserLogin();
		$userId = session('user.userid');
		$ids    = I('post.ids');
		
		$userAddress = D('UserAddress');
		$default = $userAddress->getDefaultAddressByUserId($userId);
		for($i=0;$i<count($ids);$i++){
			if($ids[$i] == $default['id']){
				continue;
			}
			$userAddress->deleteAdderss($ids[$i]);
		}
		echo 1;exit;
    }
	
	/**
	 * POST加载地址内容
	 */
	public function organize(){
		$id = I('post.id');
		
		$userAddress = D('UserAddress');
		$area        = D('Area');
		
		$address = $userAddress->getAddressById($id);
		$provinces = $area->getProvince();
		$htmlProvince = '<option value="0">选择省</option>';
		foreach($provinces as $vo){
			if($address['one_id'] == $vo['id']){
				$htmlProvince .= '<option value="'.$vo['id'].'" selected>'.$vo['addr'].'</option>';
			}else{
				$htmlProvince .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
			}
		}
		$citys = $area->getCityByProvinceId($address['one_id']);
		$htmlCity = '<option value="0">选择市</option>';
		foreach($citys as $vo){
			if($address['two_id'] == $vo['id']){
				$htmlCity .= '<option value="'.$vo['id'].'" selected>'.$vo['addr'].'</option>';
			}else{
				$htmlCity .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
			}
		}
		$areas = $area->getAreaByCityId($address['two_id']);
		$htmlArea = '<option value="0">选择县/区</option>';
		foreach($areas as $vo){
			if($address['area_id'] == $vo['id']){
				$htmlArea .= '<option value="'.$vo['id'].'" selected>'.$vo['addr'].'</option>';
			}else{
				$htmlArea .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
			}
		}
		
		echo json_encode(array('address'=>$address,'provinces'=>$htmlProvince,'citys'=>$htmlCity,'areas'=>$htmlArea));
	}
}