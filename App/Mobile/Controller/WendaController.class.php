<?php
namespace Mobile\Controller;
/**
 * 问答控制器
 */
class WendaController extends BaseController{
	
    /**
	 * 问答列表
	 * 
	 */
    public function index(){
		$wenda   = D('Wenda');
		
		// 分类列表
		$wendaRes = $wenda->getWendaCat();
		foreach($wendaRes as $key=>$vo){
			$wendaRes[$key]['list'] = $wenda->getWendaList($vo['id']);
		}

        $this->assign('wendaRes', $wendaRes);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('wenda');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
        $this->display();
    }
	
	/**
	 * 问答详细
	 * 
	 */
    public function detail(){
		$id = I('get.id');
		
		$wenda   = D('Wenda');
		
		// 增加点击数
		$wenda->statistics($id);
		
		// 视频信息
		$data = $wenda->getWenda($id);
		$this->assign('wenda',$data);

		// SEO
		$this->assign('title', $data['title']);
		$this->assign('keywords', $data['keywords']);
		$this->assign('description', $data['description']);
		
        $this->display();
    }

}