<?php
namespace Mobile\Controller;
/**
 * 首页控制器
 */
class IndexController extends BaseController{
	
    /**
	 * 获取首页信息
	 */
    public function index(){
		
		// Banner
		$ads = D('Advs');
		$catAds = $ads->getAdvsByCat(6, 1);
		$this->assign('catAds',$catAds);
		
		// 配件商店
		$products = D('Products');
		
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$product = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'groupby'=>1, 'hot'=>1, 'm'=>0, 'n'=>5));
		$this->assign("product",$product);
		
		// 我的通知
		$orders = D('Orders');
		$users  = D('Users');

		$userId = session('user.userid');
		$user = $users->getUserAndInfoById($userId);
		if(!empty($user)){
			if($user['group_id'] > 0){
				$offerCount = $orders->getOrderOfferIngCountByUserId(array('userId'=>$userId, 'status'=>0));
			}else{
				$offerCount = $orders->getOrderOfferCountByUserId($userId);
			}
		}
		
		$this->assign("userId",$userId);
		$this->assign("offerCount",$offerCount);
		$this->assign("user",$user);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->assign('controller',CONTROLLER_NAME);
		$this->display();
    }
	
	/**
	 * 在线客服
	 */
	public function service(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
	}
	
	/**
	 * 加盟邀请函
	 */
	public function invitation(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
	}
	
	/**
	 * 店铺加盟介绍页面
	 */
    public function special(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		$this->display();
    }
	
	/**
	 * 店铺加盟流程
	 */
    public function process(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
    }
	
	/**
	 * 店铺加盟选择类型
	 */
    public function joins(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
    }
	
	/**
	 * 店铺加盟申请页面
	 */
    public function recruit(){
		$type   = I('get.type');
		$userId = session('user.userid');
		
		$users = D('Users');
		
		if(isset($userId)){
			$user = $users->getUserById($userId);
			if($user['group_id'] != '0'){
				$this->error("该账号已开通店铺,请不要重复申请", U('users/index'));exit;
			}else{
				$recruit = $users->getRecruitByUserId($userId);
				if(!empty($recruit)){
					$this->error("您已经有招募申请在处理,还要重复申请吗?", U('users/index'));exit;
				}
			}
		}

		// 三级联动
		$areaId = session('recruit.areaid');
		if($areaId > 0){
			$area = D('Area');
			$info = $area->getAreaById($areaId);
			$cityArray = array('province'=>$info['one_id'], 'city'=>$info['two_id'], 'area'=>$info['id']);
			$city = \Think\Area::city($cityArray);
		}else{
			$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
			$city = \Think\Area::city($cityArray);
		}
		
		$this->assign("city", $city);
		$this->assign("userId", $userId);
		$this->assign("type", $type);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
    }
	
	// 裁剪图片
	public function crop(){
		$crop = new \Think\CropAvatar(
			'./Public/Temp/',
			'./Public/Temp/crop/',
			isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
			isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
			isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
		);
		$result  = $crop -> getResult();
		$message = $crop -> getMsg();
		echo json_encode(array('state'  => 200, 'type' => $_POST['uptype'], 'message' => $message, 'result' => $result));
	}
	
	/**
	 * 获取城市区号
	 */
    public function getCityNumber(){
		$city   = I('post.city');
		$modelCity = M('addr_city');
		$data = $modelCity->where("addr like '%".$city."%'")->find();
		echo $data['area_no'];
    }
	
	
	/**
	 * 添加维修招募申请操作
	 */
	public function insertRecruit(){
		$rs        = array();
		$userId    = session('user.userid');
		$mobile    = I('post.mobile');
		$shopName  = I('post.shop_name');
		$userName  = I('post.user_name');
		$isStore   = I('post.is_store');
		$isDoor    = I('post.is_door');
		$main      = I('post.main');	
		$areaId    = I('post.area');
		$address   = I('post.address');
		
		// 保存Session
		session(array('name'=>'shopRecruit', 'prefix'=>'recruit'));
		session('mobile',  $mobile);
		session('sname',   $shopName);
		session('uname',   $userName);
		session('store',   $isStore);
		session('door',    $isDoor);
		session('main',    $main);
		session('areaid',  $areaId);
		session('address', $address);

		$users    = D('Users');
		$usercode = D('UserCode');
		
		if(empty($userId)){   // 未登陆
			$loginPwd  = substr($mobile, -6);
			$codeword  = I('post.smscode');
			$uc = $usercode->check($mobile, $codeword);   // 验证短信验证码
			if($uc == -1){
				$rs["status"] = -1;
				$rs['msg']    = '短信验证码超时';
				echo json_encode($rs);exit;
			}else if($uc == -2){
				$rs["status"] = -2;
				$rs['msg']    = '短信验证码错误';
				echo json_encode($rs);exit;
			}else{
				$user = $users->getUserByMobile($mobile);
				if(empty($user['id'])){   // 未注册
					$rd = $users->regist($mobile, $loginPwd);
					$userId = $rd['userId'];

					// 发送密码
					sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
				}else{
					$userId = $user['id'];
				}
			}
			// 删除短信验证码相关记录
			$usercode->del($mobile);
		}
		if($userId > 0){
			$user = $users->getUserById($userId);
			if($user['group_id'] != '0'){
				$rs["status"] = -3;
				$rs['msg']    = '该账号已开通店铺,请不要重复申请';
			}else{
				$recruit = $users->getRecruitByUserId($userId);
				if(!empty($recruit)){
					$rs["status"] = -4;
					$rs['msg']    = '您已经有招募申请在处理,还要重复申请吗?';
				}else{
					$data = array();
					$data['user_id']     = $userId;
					$data['type']        = I('post.type');
					$data['shop_name']   = $shopName;
					$data['user_name']   = $userName;
					$data['is_store']    = $isStore;
					$data['is_door']     = $isDoor;
					$data['main']        = $main;	
					$data['area_id']     = $areaId;
					$data['address']     = $address;
					$data['address_deg'] = I('post.lng').','.I('post.lat');

					// 判断有没有文件上传 - 正面
					$positive = I('post.positive');
					if (!empty($positive)){
						$imgPositive  = CBWBase64Upload($positive, './Public/Repair/data/');
						if($imgPositive){
							$data['identity_positive'] = $imgPositive;
						}
					}
					
					// 判断有没有文件上传 - 反面
					$back = I('post.back');
					if (!empty($back)){
						$imgBack = CBWBase64Upload($back, './Public/Repair/data/');
						if($imgBack){
							$data['identity_back'] = $imgBack;
						}
					}
					
					// 判断有没有文件上传 - 半身照
					$body = I('post.body');
					if (!empty($body)){
						$imgBody = CBWBase64Upload($body, './Public/Repair/data/');
						if($imgBody){
							$data['identity_body'] = $imgBody;
						}
					}
					
					// 判断有没有文件上传
					$business = I('post.business');
					if (!empty($business)){
						$imgBusiness = CBWBase64Upload($business, './Public/Repair/data/');
						if($imgBusiness){
							$data['business'] = $imgBusiness;
						}
					}
					
					// 判断有没有文件上传
					$external = I('post.external');
					if (!empty($external)){
						$imgExternal = CBWBase64Upload($external, './Public/Repair/data/');
						if($imgExternal){
							$data['external'] = $imgExternal;
						}
					}
		
					$rd = $users->insertRecruit($data);
					if($rd['status'] > 0){
						$rs["status"] = $rd['status'];
					}else{
						// 提交失败,删除上传图片
						unlink("./Public/Repair/data/".$data['identity_positive']); // 身份证明 - 正面
						unlink("./Public/Repair/data/".$data['identity_back']);     // 身份证明 - 反面
						unlink("./Public/Repair/data/".$data['identity_body']);     // 身份证明 - 半身照
						unlink("./Public/Repair/data/".$data['business']);          // 营业执照
						unlink("./Public/Repair/data/".$data['external']);          // 店铺图片
					}
				}
			}
		}else{
			$rs["status"] = 0;
			$rs['msg']    = '申请失败失败,请联系客服';
		}
		echo json_encode($rs);
	}
	
	/**
	 * 店铺加盟申请成功页
	 */
    public function success(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
    }

}