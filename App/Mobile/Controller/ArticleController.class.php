<?php
namespace Mobile\Controller;
/**
 * 文章控制器
 */
class ArticleController extends BaseController{
	
    /**
	 * 文章列表
	 * 
	 */
    public function index(){
		$article   = D('Article');
		
		// 分类列表
		$articleRes = $article->getArticleCat();
		foreach($articleRes as $key=>$vo){
			$articleRes[$key]['list'] = $article->getArticleList($vo['id']);
		}

        $this->assign('articleRes', $articleRes);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('article');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
        $this->display();
    }
	
	/**
	 * 文章详细
	 * 
	 */
    public function detail(){
		$id = I('get.id');
		
		$article   = D('Article');
		
		// 增加点击数
		$article->statistics($id);
		
		// 视频信息
		$data = $article->getArticle($id);
		$this->assign('article',$data);

		// SEO
		$this->assign('title', $data['title']);
		$this->assign('keywords', $data['keywords']);
		$this->assign('description', $data['description']);
		
        $this->display();
    }

}