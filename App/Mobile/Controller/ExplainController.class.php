<?php
namespace Mobile\Controller;

class ExplainController extends BaseController {
	
    //说明书列表
    public function index(){
		
		$modelExplain = M('explain');
		
        // 获取查询产品的类型
		$explainRes = $modelExplain->order("create_time DESC")->limit(0, 10)->select();	
		
		$this->assign('explainRes', $explainRes);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('explain');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
        $this->display();
		
    }
	
	/**
	 * 加载更多说明书
	 * 
	 */
	public function loadMoreExplain(){
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$modelExplain = M('explain');

		$explainRes = $modelExplain->order("create_time DESC")->limit($counter*$num, $num)->select();
		foreach($explainRes as $key=>$vo){
			$explainRes[$key]['description'] = msubstr($vo['description'],0,30,'utf-8',false);
			$explainRes[$key]['dateline'] = date('m/d',$vo['create_time']);
		}
		echo json_encode($explainRes);
	}
	
    //说明书详细
    public function detail(){
       
		$modelExplain = M('explain');
		
		$id = I('get.id');

		//增加点击数
		$modelExplain->where('id = '.$id)->setInc('clicktimes',1);
				
		$data = $modelExplain->where('id='.$id)->find();
		if(!is_array($data)){
			$this->redirect('Empty/index');
		}
		$data['content'] = CBWKeywordAddLink(htmlspecialchars_decode($data['content'])); // 关键词加链接
		$data['content'] = CBWImageUpdateAttribute($explain['title'], $data['content']); // 更新图片属性 
		$this->assign('explain',$data);

		// SEO
		$this->assign('title', $data['title']);
		$this->assign('keywords', $data['title']);
		$this->assign('description', $data['description']);
		
        $this->display();
		
    }

}
