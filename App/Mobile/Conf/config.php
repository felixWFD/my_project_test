<?php
return array(
	//'配置项'=>'配置值'
	'URL_CASE_INSENSITIVE'  =>  true,                      // 默认false 表示URL区分大小写 true则表示不区分大小写
	'URL_MODEL'             =>  2,                         // URL访问模式,可选参数0、1、2、3,代表以下四种模式：
    // 0 (普通模式); 1 (PATHINFO 模式); 2 (REWRITE  模式); 3 (兼容模式)  默认为PATHINFO 模式
	
	'URL_ROUTER_ON'   => true, //开启路由
	'URL_ROUTE_RULES' => array( //定义路由规则
		
		'/^product\/(\d+)_(\d+)_(\d+)_(\d+)$/'     => 'product/lists?class=:1&type=:2&brand=:3&model=:4',
		'/^product\/(\d+)_(\d+)_(\d+)$/'           => 'product/lists?class=:1&type=:2&brand=:3',
		'/^product\/(\d+)_(\d+)$/'                 => 'product/lists?class=:1&type=:2',
		'/^product\/(\d+)$/'                       => 'product/lists?class=:1',
		'/^product$/'                              => 'product/index',
		'/^detail\/(\d+)$/'                        => 'product/detail?id=:1',
		'/^comments\/(\d+)$/'                      => 'product/comments?id=:1',
		'/^introduce\/(\d+)$/'                     => 'product/introduce?id=:1',
		
		'/^video_list\/(\d+)_(\d+)_(\d+)$/'        => 'video/index?type=:1&brand=:2&model=:3',
		'/^video_list\/(\d+)_(\d+)$/'              => 'video/index?type=:1&brand=:2',
		'/^video_list\/(\d+)$/'                    => 'video/index?type=:1',
		'/^video_list$/'                           => 'video/index',
		'/^video\/(\d+)$/'                         => 'video/detail?id=:1',
		
		'/^news_list\/(\d+)$/'                     => 'news/index?cat=:1',
		'/^news_list$/'                            => 'news/index',
		'/^news\/(\d+)$/'                          => 'news/detail?id=:1',
		
		'/^repair\/(\d+)-(\d+)-(\d+)-(\d+)-(\d+)$/'             => 'repair/six?type=:1&brand=:2&model=:3&color=:4&cat=:5',
		'/^repair\/(\d+)-(\d+)-(\d+)-(\d+)$/'                   => 'repair/five?type=:1&brand=:2&model=:3&color=:4',
		'/^repair\/(\d+)-(\d+)-(\d+)$/'                         => 'repair/four?type=:1&brand=:2&model=:3',
		'/^repair\/(\d+)-(\d+)$/'                               => 'repair/three?type=:1&brand=:2',
		'/^repair\/(\d+)$/'                                     => 'repair/two?type=:1',
		
		'/^recovery\/(\d+)-(\d+)-(\d+)$/'                   => 'recovery/assess?type=:1&brand=:2&model=:3',
		'/^recovery\/(\d+)-(\d+)$/'                         => 'recovery/model?type=:1&brand=:2',
		'/^recovery\/(\d+)$/'                               => 'recovery/brand?type=:1',
		
		'/^repairer_list\/(\d+)-(\d+)-(\d+)-(\d+)$/'        => 'repairer/index?city=:1&area=:2&type=:3&brand=:4',
		'/^repairer_list\/(\d+)-(\d+)-(\d+)$/'              => 'repairer/index?city=:1&area=:2&type=:3',
		'/^repairer_list\/(\d+)-(\d+)$/'                    => 'repairer/index?city=:1&area=:2',
		'/^repairer_list\/(\d+)$/'                          => 'repairer/index?city=:1',
		'/^repairer_list$/'                                 => 'repairer/index',
		'/^repairer\/(\d+)$/'                               => 'repairer/detail?id=:1',
		'/^offer\/(\d+)-(\d+)$/'                            => 'repairer/offer?id=:1&sid=:2',
		
		'/^article_list$/'                            => 'article/index',
		'/^article\/(\d+)$/'                          => 'article/detail?id=:1',
		
		'/^wenda_list$/'                            => 'wenda/index',
		'/^wenda\/(\d+)$/'                          => 'wenda/detail?id=:1',
		
		'/^explain_list\/(\d+)$/'                  => 'explain/index?cat=:1',
		'/^explain\/(\d+)$/'                       => 'explain/detail?id=:1',
    ),
);