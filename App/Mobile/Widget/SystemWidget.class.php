<?php
namespace Mobile\Widget;
use       Think\Controller;

class SystemWidget extends Controller {
	
    public function menu() {
		$userId = session('user.userid');
		$shopId = session('shop.id');
		
		$orders  = D('Orders');
		$users   = D('Users');
		$repairs = D('Repairs');
		
		$user = $users->getUserAndInfoById($userId);
		$offerCount = 0;
		if(!empty($user)){
			if($user['group_id'] > 0){
				$offerCount = $orders->getOrderOfferIngCountByUserId(array('userId'=>$userId, 'status'=>0));
			}else{
				$offerCount = $orders->getOrderOfferCountByUserId($userId);
			}
		}
		
		// 未读可以报价数量
		$offerShopCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));
		
		// 
		if($userId > 0){
			$offer = $orders->getOrderOfferByUserId($userId, 0, 1);
			if(!empty($offer)){
				$order = $orders->getOrderById($offer[0]['order_id']);
				if($order['order_type'] == 1){
					$server = $orders->getOrderServerByOrderId($order['id']);
					if($server['relation_id'] > 0){
						$relation = $repairs->getRepairByRelationId($server['relation_id']);
						$order['pro_name'] = $relation['pro_name'].'维修订单';
					}else{
						$order['pro_name'] = '维修订单';
					}
				}elseif($order['order_type'] == 2){
					$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
					if($recovery['reco_id'] > 0){
						$repair = $repairs->getRepairById($recovery['reco_id']);
						$order['pro_name'] = $repair['pro_name'].'回收订单';
					}else{
						$order['pro_name'] = '回收订单';
					}
				}
			}
		}
		
		$this->assign('user', $user);
		$this->assign('offerCount', $offerCount);
		$this->assign('offerShopCount', $offerShopCount);
		$this->assign('controller', CONTROLLER_NAME);
		$this->assign('order', $order);
		$this->assign('action', ACTION_NAME);
        $this->display('Public:menu');
    }

}