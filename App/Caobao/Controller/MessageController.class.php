<?php   
namespace Caobao\Controller;
use       Think\Controller;

class MessageController extends MyController{
     public function __construct() {

        parent::__construct();
       
    }

    /**
     * 消息List
     * @date 2015-01-22
     * @return void 
     */
    public function messageList(){

        $modelMessage = M('message');

        // 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '' && $key == 'keyword') {
                    continue;
                }

                $map[$key] = $val;
            }
        }

        // 查询总记录数
        $getPageCounts = $modelMessage->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $messageList = $modelMessage->where($map)->order('id DESC')->limit($page->firstRow, $page->listRows)->select();

        $pageShow = $page->show();
        $this->assign('page', $pageShow);
        $this->assign('messageList', $messageList);
        $this->display();

    }

    /**
     * 添加消息
     * @date 2015-01-22
     * @return void 
     */
    public function addMessage(){
        $this->display();
    }

    /**
     * 执行添加消息
     * @date 2015-01-22
     * @return void 
     */
    public function doAddMessage(){
        
        $modelMessage = M("message");
        if($modelMessage->create()){
            $res=$modelMessage->add();
            if($res){
                $this->success("添加成功",U("Message/messageList"));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      
    }

    /**
     * 修改消息
     * @date 2015-01-22
     * @return void 
     */
    public function editMessage(){
        $id = I('get.id');
        $modelMessage = M("message");
        $list = $modelMessage->where("id =".$id)->find();       

        $this->assign("list",$list);
        $this->display();

    }

    /**
     * 执行修改消息
     * @date 2015-01-22
     * @return void 
     */
    public function updateMessage(){
	
		$modelMessage = M("message");

        if($modelMessage->create()){
            $rt=$modelMessage->save();
             if($rt){
                $this->success("修改成功",U("Message/messageList"));
                exit;
            }else{
				$this->error("提交表单无修改");
			}
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除消息
     * @date 2015-01-22
     * @return void 
     */
    public function delMessage(){
		
        $id = I('get.id');
        $modelMessage = M("message");
        $ls = $modelMessage->where("id =".$id)->delete();
        if($ls){
			$modelMessageUser = M("message_user");
			$modelMessageUser->where("msg_id =".$id)->delete();   //删除信息关联的用户信息表
            $this->success("删除成功",U("Message/messageList"));
        }else{
            $this->error("删除失败");
        }
    }

    /**
     * 发送消息页面
     * @date 2015-01-22
     * @return void 
     */
    public function sendMessage(){
        $id = I('get.id');
        $modelMessage = M("message");
        $list = $modelMessage->where("id =".$id)->find();

        $this->assign("list",$list);
        $this->display();

    }

    /**
     * 发送消息操作
     * @date 2015-01-22
     * @return void 
     */
    public function doSendMessage(){
        $id = I('post.id');
		$user_id = I('post.user_id');
		
		$modelUsers = M('users');
        $modelMessageUser = M("user_message");
		
		if(empty($user_id)){
			$users = $modelUsers->field('id')->select();
			$data = array();
			foreach($users as $key=>$item){
				$data[$key]['msg_id'] = $id;
				$data[$key]['is_read'] = 0;
				$data[$key]['send_date'] = time();
				$data[$key]['user_id'] = $item['id'];
			}
			$rt = $modelMessageUser->addAll($data);
			if($rt){
				$this->success("发送成功",U("Message/messageList"));
			}else{
				$this->error("发送失败",U("Message/messageList"));
			}
		}else{
			$data = array();
			$data['msg_id'] = $id;
			$data['is_read'] = 0;
			$data['send_date'] = time();
			$data['user_id'] = $user_id;
			
			if($modelMessageUser->create($data)){
				$rt = $modelMessageUser->add();
				if($rt){
					$this->success("发送成功",U("Message/messageList"));
				}else{
					$this->error("发送失败",U("Message/messageList"));
				}
			}
		}

    }

}
