<?php
namespace  Caobao\Controller;

class UserRepairsController extends MyController
{
    /**
     * 构造函数：执行本类初始化操作 
     * @date 2015-01-17
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 获取维修师模板，显示维修师列表
     * @date 2015-01-18 
     * @return void 
     */
    public function index()
    {
        $model      = M();
		$modelUsers = M('users');
		
		$province = I('get.province');
		$city     = I('get.city');
		$area     = I('get.area');
		$types    = I('get.types');
		$server_status = I('get.server_status');
		$shop_status   = I('get.shop_status');
		$start  = I('get.startdate');
		$end  = I('get.enddate');
		$keyword  = I('get.keyword');
		$review   = I('get.review');
		
		$where = "s.id > 0";
		if($province > 0 && $province != '选择省'){
			$where .= " AND p.id =".$province;
		}
		if($city > 0 && $city != '选择市'){
			$where .= " AND c.id =".$city;
		}
		if($area > 0 && $area != '选择县/区'){
			$where .= " AND a.id =".$area;
		}
		if($types!='' && $types != '类型'){
			$where .= " AND s.type_id =".$types;
		}
		if($server_status!='' && $server_status != '服务状态'){
			$where .= " AND s.server_status =".$server_status;
		}
		if($shop_status!='' && $shop_status != '店铺状态'){
			$where .= " AND s.shop_status =".$shop_status;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint   = strtotime($end.' 23:59:59');
			$where .= " AND (s.create_shop BETWEEN $startint AND $endint)";
		}
		if(!empty($keyword)){
			$user = $modelUsers->where("account='$keyword' OR mobile='$keyword'")->find();
			$where .= " AND (";
			if(!empty($user['id'])){
				$where .= "u.id=".$user['id']." OR u.parent_id=".$user['id']." OR ";
			}
			$where .= "u.account like '%".$keyword."%' OR u.mobile like '%".$keyword."%' OR s.shop_name like '%".$keyword."%' OR s.user_name like '%".$keyword."%' OR s.user_phone like '%".$keyword."%')";
		}
		if(!empty($review)){
			$where .= " AND (s.mod_address<>'' OR s.mod_image<>'')";
		}
        $pageCount = $model->table('__USER_SHOP__ AS s')
							->join('__USERS__ AS u ON u.id=s.user_id')
							->join('__USER_INFO__ AS i ON i.user_id=u.id')
							->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->where($where)
							->count();	
        $pageSize  = 10;
        $page = new \Think\Page($pageCount, $pageSize);
        // 获取维修师数据
		$repairRes = $model->table('__USER_SHOP__ AS s')
							->field('s.*,u.account,u.mobile, u.parent_id, a.addr,c.addr as city_addr,p.addr as pro_addr')
							->join('__USERS__ AS u ON u.id=s.user_id')
							->join('__USER_INFO__ AS i ON i.user_id=u.id')
							->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->where($where)
							->limit($page->firstRow, $page->listRows)
							->order('s.id DESC')
							->select();
		foreach($repairRes as $key=>$vo){
			$repairRes[$key]['inte'] = $this->integrity($vo['id']);
		}
		
		// 等待审核数量
		$reviewCount = $model->table('__USER_SHOP__')->where("mod_address<>'' OR mod_image<>''")->count();
		
		//加载城市信息
		if(empty($province)){
			$province = '选择省';
		}
		if(empty($city)){
			$city = '选择市';
		}
		if(empty($area)){
			$area = '选择县/区';
		}
		$cityArray = array('province'=>$province, 'city'=>$city, 'area'=>$area);
		$c = \Think\Area::city($cityArray);
		
        $this->assign('page', $page->show());
		$this->assign('keyword', $keyword);
		$this->assign('type', $type);
		$this->assign('area', ($area=='选择县/区'?0:$area));
		$this->assign('city', ($city=='选择市'?0:$city));
		$this->assign('province', ($province=='选择省'?0:$province));
        $this->assign('repairRes', $repairRes);
		$this->assign("reviewCount", $reviewCount);
		$this->assign("citys", $c);
		$this->assign('start', $start);
		$this->assign('end', $end);
        $this->display('repair_list');
    }
	
	/**
     * 地址变更 或 店铺LOGO修改,审核操作
     * @date 2016-02-16
     * @return void 
     */
	public function review(){
		
		$mod = I('get.mod');
		$id  = I('get.id');
		
		$us = M('User_shop');
		$shop = $us->where("id=$id")->find();
		
		if($mod == 'agree'){
			$data = array();
			if(!empty($shop['mod_address'])){
				$address = (array)json_decode($shop['mod_address']);
				$data['area_id']     = $address['area_id'];
				$data['address']     = $address['address'];
				$data['lng']         = $address['lng'];
				$data['lat']         = $address['lat'];
				$data['mod_address'] = '';
			}
			if(!empty($shop['mod_image'])){
				$data['logo_image']  = $shop['mod_image'];
				$data['mod_image']   = '';
				unlink("./Public/User/".$shop['logo_image']);        //删除图片
			}
			$rt = $us->where("id=$id")->save($data);
			if ($rt > 0){
				if(!empty($shop['mod_image']) && $shop['is_main'] == 1){
					$shops = $us->where("user_id=".$shop['user_id']." AND is_main=0")->select();
					foreach($shops as $vo){
						$us->where("id=".$vo['id'])->save(array('logo_image'=>$shop['mod_image']));
						unlink("./Public/User/".$vo['logo_image']);        //删除图片
					}
				}
				$this->redirect("UserRepairs/index", array('review'=>1));
			}
		}elseif($mod == 'oppose'){
			if(!empty($shop['mod_image'])){
				unlink("./Public/User/".$shop['mod_image']);        //删除图片
			}
			$rt = $us->where("id=$id")->save(array('mod_address'=>'', 'mod_image'=>''));
			if ($rt > 0){
				$this->redirect("UserRepairs/index", array('review'=>1));
			}
		}
		$this->error('删除失败');
	}
	
	/**
     * 导出店铺
     * @date 2016-02-16
     * @return void 
     */
	public function export(){

		$model = M();

		$province = I('get.province');
		$city     = I('get.city');
		$area     = I('get.area');
		$integr   = I('get.integr');
		
		$where = "s.type_id<>2";
		if($province > 0){
			$where .= " AND p.id =".$province;
		}
		if($city > 0){
			$where .= " AND c.id =".$city;
		}
		if($area > 0){
			$where .= " AND a.id =".$area;
		}

		$shops = $model->table('__USER_SHOP__ AS s')
							->field('s.id,s.shop_name,s.user_name,s.user_phone,s.address,a.addr,c.addr as city_addr,p.addr as pro_addr')
							->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->where($where)
							->order('s.id DESC')
							->select();
		$i = 0;
		foreach($shops as $vo){
			$inte = $this->integrity($vo['id']);
			if($integr == 1){
				if(!empty($vo['user_phone']) && $inte > 60){
					$xlsData[$i]['id']       = $vo['id'];
					$xlsData[$i]['name']     = $vo['user_name'];
					$xlsData[$i]['sname']    = $vo['shop_name'];
					$xlsData[$i]['mobile']   = $vo['user_phone'];
					$xlsData[$i]['province'] = $vo['pro_addr'];
					$xlsData[$i]['city']     = $vo['city_addr'];
					$xlsData[$i]['address']  = $vo['addr'].$vo['address'];
					$xlsData[$i]['inte']     = $inte;
					$i++;
				}
			}elseif($integr == 2){
				if(!empty($vo['user_phone']) && $inte <= 60){
					$xlsData[$i]['id']       = $vo['id'];
					$xlsData[$i]['name']     = $vo['user_name'];
					$xlsData[$i]['sname']    = $vo['shop_name'];
					$xlsData[$i]['mobile']   = $vo['user_phone'];
					$xlsData[$i]['province'] = $vo['pro_addr'];
					$xlsData[$i]['city']     = $vo['city_addr'];
					$xlsData[$i]['address']  = $vo['addr'].$vo['address'];
					$xlsData[$i]['inte']     = $inte;
					$i++;
				}
			}else{
				$xlsData[$i]['id']       = $vo['id'];
				$xlsData[$i]['name']     = $vo['user_name'];
				$xlsData[$i]['sname']    = $vo['shop_name'];
				$xlsData[$i]['mobile']   = $vo['user_phone'];
				$xlsData[$i]['province'] = $vo['pro_addr'];
				$xlsData[$i]['city']     = $vo['city_addr'];
				$xlsData[$i]['address']  = $vo['addr'].$vo['address'];
				$xlsData[$i]['inte']     = $inte;
				$i++;
			}
		}
		
		$xlsName  = "shops";
        $xlsCell  = array(
			array('id','序号'),
			array('name','姓名'),
			array('sname','门店名称'),
			array('mobile','电话'),
			array('province','省'),
			array('city','市'),
			array('address','详细地址'),
			array('inte','信息完整度(%)')
        );
		
        CBWExportExcel($xlsName,$xlsCell,$xlsData);
	}
	
	/**
     * 发送短信
     * @date 2016-02-16
     * @return void 
     */
	public function sms(){

		$modelShop = M('user_shop');
	
		$sms = new \Think\Sms(); // 初始化
		
		$shops = $modelShop->where('is_main=1')->select();
		foreach($shops as $key=>$vo){
			$inte = $this->integrity($vo['id']);
			if(!empty($vo['user_phone']) && $inte <= 60){
				// 发送模板短信
				$sms->sendSMS($vo['user_phone'],'joiny3');
			}
		}
		$this->success('发送成功', U('UserRepairs/index'));
	}
	
	private function integrity($id){
		$modelUserShop = M('user_shop');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($id);
		if($shop['is_main'] < 1){
			$shop = $us->getMainShopByUserId($shop['user_id']);
		}
		$inte = 10;
		if(!empty($shop['logo_image'])){
			$inte +=10;
		}
		if(!empty($shop['description'])){
			$inte +=10;
		}
		if(!empty($shop['content'])){
			$inte +=10;
		}
		if(!empty($shop['qq'])){
			$inte +=10;
		}
		if(!empty($shop['open_hours'])){
			$inte +=10;
		}
		if(!empty($shop['server_area'])){
			$inte +=10;
		}
		$model = $us->getUserModelByUserId($shop['user_id']);
		if(count($model) > 0){
			$inte +=30;
		}
		return $inte;
	}
	
    /**
     * 修改店铺状态
     * @date 2016-02-16
     * @return void 
     */
    public function repairStatusEdit ()
    {
        $id = I('post.id');
		$field = I('post.field');
		$val = I('post.val');
		
		if($field == 'shop_status'){
			$data['shop_status'] = $val;
		}else{
			$data['server_status'] = $val;
		}
		
        $modelUserShop = M('user_shop');
		$rt = $modelUserShop->where('id='.$id)->save($data);
        if ($rt > 0){
            echo 1;exit;
        }else{
            echo 0;exit;
        }
    }

    /**
     * 删除单个维修店
     * @date 2015-01-18
     * @return void 
     */
    public function repairDelete ()
    {
        $id = I('get.id');
		
		$modelUser = M('users');
		$modelUserShop = M('user_shop');
		
		$shop = $modelUserShop->where("id=".$id)->find();
		$rt = $modelUserShop->where("id=".$id)->delete();
        if ($rt > 0){
			$modelUser->where('id='.$shop['user_id'])->save(array('group_id'=>0));
			if(!empty($shop['business'])){
				unlink("./Public/Repair/data/".$shop['business']);  //营业执照
			}
			$this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 批量删除维修店
     * @date 2015-01-18
     * @return void 
     */
    public function batchDeleteRepairs ()
    {
        $ids = I('post.ids');
        

        $modelUser = M('users');
		$modelUserShop = M('user_shop');
		
		foreach($ids as $uid){
			$shop = $modelUserShop->where("id=".$uid)->find();
			$uids[] = $shop['user_id'];
			if(!empty($shop['business'])){
				unlink("./Public/Repair/data/".$shop['business']);                   //营业执照
			}
			$modelUserShop->where("id=".$uid)->delete();
		}
		
		$uidsArray = implode(', ', $uids);
        $rt = $modelUser->where("id IN({$uidsArray})")->save(array('group_id'=>0));

        if ($rt > 0){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 维修师基本资料修改
     * @date 2015-01-18
     * @return void 
     */
    public function repairEdit ()
    {
        $id = I('get.id');
		
		$model = M();
		$modelArea = M('addr_area');
		
		//维修师信息
		$shop = $model->table('__USER_SHOP__ AS s')
							->field('s.*,i.identity_positive,i.identity_back,i.identity_body')
							->join('__USERS__ AS u ON u.id=s.user_id')
							->join('__USER_INFO__ AS i ON i.user_id=u.id')
							->where('s.id='.$id)
							->find();
		// 变更地址信息
		if(!empty($shop['mod_address'])){
			$address = (array)json_decode($shop['mod_address']);
			$adds = $model->table('__ADDR_AREA__ AS a')
							->field('a.addr,c.addr AS city_addr,p.addr AS pro_addr')
							->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id=a.one_id')
							->where('a.id='.$address['area_id'])
							->find();
			$address['address'] = $adds['pro_addr']." ".$adds['city_addr']." ".$adds['addr']." ".$address['address'];
		}
		
		// 客服信息
		$services = $model->table('__ADMIN_USERS__ AS u')
							->field('u.*,i.*')
							->join('__ADMIN_INFO__ AS i ON i.uid=u.id')
							->join('__AUTH_GROUP_ACCESS__ AS a ON a.uid=u.id')
							->join('__AUTH_GROUP__ AS g ON g.id=a.group_id')
							->where('g.id=3')
							->select();
		$service = '';
		foreach($services as &$vo){
			if($vo['id'] == $shop['service_id']){
				$vo['is_selected'] = 1;
				$service = $vo;
			}
		}

		//加载城市信息
		$area = $modelArea->where('id='.$shop['area_id'])->find();
        if ( ! empty($area)){
            $city = array('province'=>$area['one_id'], 'city'=>$area['two_id'], 'area'=>$area['id']);
        }else{
            $city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
        }
		$c = \Think\Area::city($city);
		
        $this->assign('shop', $shop);
        $this->assign('address', $address);
        $this->assign('services', $services);
        $this->assign('service', $service);
		$this->assign("area", $area);
        $this->assign("city", $c);
        $this->display('repair_edit');
    }

    /**
     * 执行维修师资料修改操作
     * @date 2015-01-18
     * @return void 
     */
    public function doRepairEdit()
    {
        $id = I('post.id');
		$user_id = I('post.user_id');
		
        $modelInfo = M('user_info');
        $modelShop = M('user_shop');
		
		//判断是否有身份证正面上传,有上传则删除旧图片
		$identity_positive = I('post.identity_positive');
		$org_positive = I('post.org_positive');
		if(!empty($identity_positive)){
			unlink("./Public/User/".$org_positive);        //删除旧图片
			$info['identity_positive'] = $identity_positive;
		}else{
			$info['identity_positive'] = $org_positive;
		}
		
		//判断是否有身份证反面上传,有上传则删除旧图片
		$identity_back = I('post.identity_back');
		$org_back = I('post.org_back');
		if(!empty($identity_back)){
			unlink("./Public/User/".$org_back);        //删除旧图片
			$info['identity_back'] = $identity_back;
		}else{
			$info['identity_back'] = $org_back;
		}
		
		//判断是否有身份证半身照上传,有上传则删除旧图片
		$identity_body = I('post.identity_body');
		$org_body = I('post.org_body');
		if(!empty($identity_body)){
			unlink("./Public/User/".$org_body);        //删除旧图片
			$info['identity_body'] = $identity_body;
		}else{
			$info['identity_body'] = $org_body;
		}
		$affectedInfo = $modelInfo->where('user_id='.$user_id)->save($info);
		
		$shop['type_id']   = I('post.type_id');
		$shop['shop_name'] = I('post.shop_name');
		$shop['area_id']   = I('post.area');
		$shop['address']   = I('post.address');
		$shop['lng']       = I('post.lng');
		$shop['lat']       = I('post.lat');
		$shop['is_store']  = I('post.is_store');
		$shop['is_door']   = I('post.is_door');
		$shop['exp']       = I('post.exp');
		
		//判断是否有店铺形象上传,有上传则删除旧图片
		$logo = I('post.logo_image');
		$org_logo = I('post.org_logo');
		if(!empty($logo)){
			unlink("./Public/User/".$org_logo);        //删除旧图片
			$shop['logo_image'] = $logo;
		}else{
			$shop['logo_image'] = $org_logo;
		}
		//判断是否有营业执照上传,有上传则删除旧图片
		$business = I('post.business');
		$org_business = I('post.org_business');
		if(!empty($business)){
			unlink("./Public/User/".$org_business);        //删除旧图片
			$shop['business'] = $business;
		}else{
			$shop['business'] = $org_business;
		}
		//判断是否有店铺图片上传,有上传则删除旧图片
		$shop_image = I('post.shop_image');
		$shopimage_back = I('post.shopimage_back');
		if(!empty($shop_image)){
			unlink("./Public/User/".$shopimage_back);        //删除旧图片
			$shop['shop_image'] = $shop_image;
		}else{
			$shop['shop_image'] = $shopimage_back;
		}
		
        $affectedShop = $modelShop->where('id='.$id)->save($shop);
		
        if ($affectedInfo || $affectedShop) {
            $this->success('修改成功'); 
        }else{
            $this->error('修改失败');
        }
    }

    /**
     * 执行维修师修改操作
     * @date 2015-01-18
     * @return void 
     */
    public function doRepairEditService(){
        $shopId    = I('post.id');
		$serviceId = I('post.service_id');
		
        $modelShop = M('user_shop');
		
        $affectedShop = $modelShop->where('id='.$shopId)->save(array('service_id'=>$serviceId));
        if($affectedShop) {
            $this->success('修改成功'); 
        }else{
            $this->error('修改失败');
        }
    }

    /**
     * 获取客服信息
     * @date 2015-01-18
     * @return void 
     */
    public function changeServiceSelect(){
		$serviceId = I('post.service_id');
		
        $model = M();
		
        $data = $model->table('__ADMIN_USERS__ AS u')
							->field('u.*,i.*')
							->join('__ADMIN_INFO__ AS i ON i.uid=u.id')
							->where('u.id='.$serviceId)
							->find();
        echo json_encode($data);exit;
    }

    /**
     * 上传图片
     * 此方法被店铺修改页，上传缩略图所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/User/"; //图片保存文件夹路径
        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
		//裁剪图像
		$image = new \Think\Image();
		$image->open($rootPath.$imgUrl);
		$image->thumb('1000', '1000')->save($rootPath.$imgUrl); //生产缩略图
		
        echo $imgUrl;
    }

    /**
     * 发送通知操作
     * @date 2015-01-18
     * @return void 
     */
    public function doSendNotice()
    {
        $shopId    = I('post.id');
		$title     = I('post.title');
		$content   = I('post.content');
		$type      = I('post.type');
		$score     = I('post.score');
		$isSms     = I('post.is_sms');
		$isShop    = I('post.is_shop');
		$isAllShop = I('post.is_all_shop');
		
		$us = D('UserShop');
		
		$sms = new \Think\Sms();
		
		if($isShop == 1){
			$shop = $us->getShopByShopId($shopId);
			$rt = $us->insertShopNotice(array('shop_id'=>$shopId, 'title'=>$title, 'content'=>$content, 'is_sms'=>$isSms, 'create_time'=>time()));
			if($rt['status'] > 0){
				if($isSms){
					$sms->setContent($title);
					$sms->sendSMS($shop['user_phone']);
				}
				if($type >= 0 && $score > 0){
					$us->updateShopScore($shopId, $type, $score, $title);
				}
				$this->success('发送成功');exit;
			}
		}elseif($isAllShop == 1){
			$shops = $us->getShopsByObject();
			foreach($shops as $vo){
				$rt = $us->insertShopNotice(array('shop_id'=>$vo['id'], 'title'=>$title, 'content'=>$content, 'is_sms'=>$isSms, 'create_time'=>time()));
				if($rt['status'] > 0){
					if($isSms){
						$sms->setContent($title);
						$sms->sendSMS($vo['user_phone']);
					}
				}
			}
			$this->success('发送成功');exit;
		}
        $this->error('发送失败');exit;
    }

    /**
     * 获取维修师招募模板，显示维修师招募列表
     * @date 2015-01-18 
     * @return void 
     */
    public function recruit()
    {
        $model = M();
		$keyword = I('get.keyword');
		$is_verify = I('get.is_verify');
		$field = I('get.field');

		$where = "u.id>0";		
		if(!empty($is_verify) && $is_verify != '查找全部'){
			$where .= " and r.is_verify =".$is_verify;
		}
		if(!empty($keyword) && !empty($field) && $field !='查找全部')
		{
			$where .= " and u.".$field." like '%".$keyword."%'";
		}
        $pageCount = $model->table('__USER_RECRUIT__ AS r')
							->join('__USERS__ AS u ON u.id=r.user_id')
							->join('__USER_INFO__ AS i ON i.user_id=u.id')
							->join('__ADDR_AREA__ AS a ON a.id=r.area_id')
							->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id=a.one_id')
							->where($where)
							->count();
        $pageSize  = 10;
        $page = new \Think\Page($pageCount, $pageSize);
        // 获取维修师数据
		$repairRes = $model->table('__USER_RECRUIT__ AS r')
							->field('u.*,r.*, i.tel, i.area_id,p.addr as province,c.addr as city,a.addr as area')
							->join('__USERS__ AS u ON u.id=r.user_id')
							->join('__USER_INFO__ AS i ON i.user_id=u.id')
							->join('__ADDR_AREA__ AS a ON a.id=r.area_id')
							->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id=a.one_id')
							->where($where)
							->limit($page->firstRow, $page->listRows)
							->order('r.id DESC')
							->select();
        $this->assign('page', $page->show());
		$this->assign('keyword', $keyword);
		$this->assign('field', $field);
		$this->assign('is_verify', $is_verify);
        $this->assign('repairRes', $repairRes);
        $this->display('repair_recruit_list');
    }

    /**
     * 维修师招募基本资料修改
     * @date 2015-01-18
     * @return void 
     */
    public function recruitEdit ()
    {
        $id = I('get.id');
		
        $model = M();
		$repair = $model->table('__USER_RECRUIT__ AS r')
							->field('u.*,r.*, i.tel,p.addr as province,c.addr as city,a.addr as area')
							->join('__USERS__ AS u ON u.id=r.user_id')
							->join('__USER_INFO__ AS i ON i.user_id=u.id')
							->join('__ADDR_AREA__ AS a ON a.id=r.area_id')
							->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id=a.one_id')
							->where('r.id='.$id)
							->find();
        $this->assign('repair', $repair);
        $this->display('repair_recruit_edit');
    }

    /**
     * 删除单个维修师申请
     * @date 2015-01-18
     * @return void 
     */
    public function recruitDelete ()
    {
        $id = I('get.id');

        // 实例化数据表模型
        $modelRecruit = M('user_recruit');
		$recruit = $modelRecruit->field("user_id,business,identity_positive,external")->where("id=".$id)->find();
		
		unlink("./Public/Repair/data/".$recruit['business']);                   //营业执照
		unlink("./Public/Repair/data/".$recruit['identity_positive']);          //删除身份证正面图片
		unlink("./Public/Repair/data/".$recruit['external']);                   //店铺照片
		
        $affectedRecruit = $modelRecruit->where("id=".$id)->delete();

        if ($affectedRecruit > 0){
			$modelUsers = M('users');
			$user = $modelUsers->field('uname,email,mobile,mobile_state,email_state')->where('id='.$recruit['user_id'])->find();
			//发送短信
			if(!empty($user['mobile']) && $user['mobile_state'] == '1'){
				sendSmsByAliyun($user['mobile'], array(), 'SMS_116591009');
			}
			
			//发送邮件
			if(!empty($user['email']) && $user['email_state'] == '1'){
				sendEmail($user['uname'], 'email_shop_no');
			}
			$this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 批量删除维修师申请
     * @date 2015-01-18
     * @return void 
     */
    public function batchDeleteRecruit ()
    {
        $uids = I('post.uids');
        $uidsArray = implode(', ', $uids);

        // 实例化数据表模型
        $modelRecruit = M('user_recruit');
		
		foreach($uids as $uid){
			$info = $modelRecruit->field("business,identity_positive,external")->where("id=".$uid)->find();
			unlink("./Public/Repair/data/".$info['business']);                   //营业执照
			unlink("./Public/Repair/data/".$info['identity_positive']);          //删除身份证正面图片
			unlink("./Public/Repair/data/".$info['external']);                   //店铺照片

			$affectedRows = $modelRecruit->where("id=".$uid)->delete();
		}
   
        if ($affectedRows > 0){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 添加新申请的维修师
     * @date 2015-01-18
     * @return [type] [description]
     */
    public function add_repair()
    {
        $id = I('get.id');
		
		$modelUserRecruit = M('user_recruit');
		$modelUsers       = M('users');
		$modelUserInfo    = M('user_info');
		$modelUserShop    = M('user_shop');
		$modelUserWechat  = M('user_wechat');
		$modelAddrArea    = M('addr_area');
		
		$recruit = $modelUserRecruit->where('id='.$id)->find();
		$user    = $modelUsers->where('id='.$recruit['user_id'])->find();
		
		$data['user_id']     = $recruit['user_id'];
        $data['type_id']     = $recruit['type'];
		$data['shop_name']   = $recruit['shop_name'];
		$data['password']    = $user['password'];
		$data['user_name']   = $recruit['user_name'];
		$data['user_phone']  = $user['mobile'];
		$data['area_id']     = $recruit['area_id'];
		$data['address']     = $recruit['address'];
		$deg = explode(',',$recruit['address_deg']);
		$data['lng']         = $deg[0];
		$data['lat']         = $deg[1];
		$data['is_main']     = 1;
		$data['is_store']    = $recruit['is_store'];
		$data['is_door']     = $recruit['is_door'];
		
		if (!empty($recruit['external'])){//店铺门店图片
			$data['shop_image'] = $recruit['external'];
			$subName = date('Ym', time());
			CBWFileMake('./Public/User/'.$subName);  // 检查路径文件夹是否存在,不存在则创建
			copy('./Public/Repair/data/'.$recruit['external'],'./Public/User/'.$recruit['external']); //拷贝到新目录
        }
		if (!empty($recruit['business'])){//营业执照
			$data['business'] = $recruit['business'];
			copy('./Public/Repair/data/'.$recruit['business'],'./Public/User/'.$recruit['business']); //拷贝到新目录
        }
        $data['create_shop'] = time();
		if($modelUserShop->create($data)){
			$rt = $modelUserShop->add();
			if($rt > 0){				
				// 朋友圈信息
				$wechat = $modelUserWechat->where("user_id=".$recruit['user_id'])->find();
				if(!empty($wechat) && $wechat['fid'] > 0){//父级会员编号
					$modelUserWechat->where('id='.$wechat['id'])->save(array('fid'=>0)); // 店铺不成为粉丝
				}
				
				if(!empty($recruit['identity_positive'])){//身份证照片(正面)
					copy('./Public/Repair/data/'.$recruit['identity_positive'],'./Public/User/'.$recruit['identity_positive']); //拷贝到新目录
					$info['identity_positive'] = $recruit['identity_positive'];
				}
				if(!empty($recruit['identity_back'])){//身份证照片(反面)
					copy('./Public/Repair/data/'.$recruit['identity_back'],'./Public/User/'.$recruit['identity_back']); //拷贝到新目录
					$info['identity_back'] = $recruit['identity_back'];
				}
				if(!empty($recruit['identity_body'])){//证件图片 半身照
					copy('./Public/Repair/data/'.$recruit['identity_body'],'./Public/User/'.$recruit['identity_body']); //拷贝到新目录
					$info['identity_body'] = $recruit['identity_body'];
				}
				$modelUserInfo->where('user_id='.$recruit['user_id'])->save($info);
				$modelUsers->where('id='.$recruit['user_id'])->save(array('group_id'=>1));//group_id 用户组 默认0普通用户 1店铺用户
				
				//删除申请
				$modelUserRecruit->where("id=".$id)->delete();
				unlink("./Public/Repair/data/".$recruit['business']);                   //营业执照
				unlink("./Public/Repair/data/".$recruit['identity_positive']);          //删除身份证正面图片
				unlink("./Public/Repair/data/".$recruit['external']);                   //店铺图片
				
				//发送短信
				if(!empty($user['mobile']) && $user['mobile_state'] == '1'){
					sendSmsByAliyun($user['mobile'], array(), 'SMS_116565969');
					sendSmsByAliyun($user['mobile'], array(), 'SMS_126352499');
				}
				
				//发送邮件
				if(!empty($user['email']) && $user['email_state'] == '1'){
					sendEmail($user['email'], 'email_shop_yes');
				}

				$this->success('维修师添加成功',U('UserRepairs/index'));
				exit;
			}
		}
		$this->error('维修师添加失败');
    }

    /**
     * 执行城市三级联动下拉框获取操作
     * @date 2015-01-18
     * @return void 
     */
    public function changeSelect()
    {
        $id = I('post.id');
		$type = I('post.type');

        // 实例化数据模型
		$modelCity = M('addr_city');
        $modelArea = M('addr_area');
        if($type == 'province'){
			$list = $modelCity->where("one_id=".$id)->select();
		}else{
			$list = $modelArea->where("two_id=".$id)->select();
		}
		
        if(!is_array($list)){
			exit;
		}
		$str = '';
		foreach($list as $item){
			$str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
		}
        echo $str;
		exit;
    }
	
}
