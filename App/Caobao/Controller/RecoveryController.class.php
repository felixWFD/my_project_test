<?php
namespace Caobao\Controller;
use       Think\Controller;

header('Content-Type:text/html; charset=utf-8');

class RecoveryController extends MyController
{

    /**
     * 构造函数，初始化
     * @date 2015-01-16
     */
    public function __construct()
    {		
        parent::__construct();
    }
	
    /**
     * 评估内容列表
     */
    public function recoveryContent()
    {
        $modelContent = M('recovery_content');
		
		// 一级内容
		$contents = $modelContent->where("pid=0")->select();
        $this->assign('contents', $contents);
		
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部类型' && $key == 'types') {
                    continue;
                }

                if ($val == '查找全部上级目录' && $key == 'pid') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		
        $contentList = $modelContent->where($map)->select();
        $this->assign('contentList', $contentList);
		$this->assign('map', $map);
        $this->display('recovery_content_list');
    }

    /**
     * 添加评估内容页面
     */
    public function addRecoveryContent()
    {
        $modelContent = M('recovery_content');
		
		// 一级内容
		$contents = $modelContent->where("pid=0")->select();
        $this->assign('contents', $contents);
		
        $this->display('recovery_content_add');
    }

    /**
     * 添加评估内容操作
     */
    public function doRecoveryContentAdd()
    {
        $modelContent = M('recovery_content');
		
		$count = $modelContent->where("name='".I('post.name')."'")->count();
		if($count){
			$this->error("已存在相同数据");
		}
		
		$data = array();
		$data['pid']         = I('post.pid');
		$data['name']        = I('post.name');
		$data['brother']     = I('post.brother');
		$data['types']       = I('post.types');
		$data['description'] = I('post.description');
		$data['options']     = empty($data['brother'])?'0':'1';
		
		if($modelContent->create($data)){
			$rt = $modelContent->add();
			if($rt){
				$this->success("添加成功.",U('Recovery/recoveryContent/pid/'.$data['pid'].'/types/'.$data['types']));
			}else{
				$this->error("添加失败.");
			}	
		}else{
			$this->error("添加失败,创建数据对象出错.");
		}

    }

    /**
     * 修改评估内容页面
     */
    public function editRecoveryContent()
    {
		$id = I('get.id');
        $modelContent = M('recovery_content');
		
		// 一级内容
		$contents = $modelContent->where("pid=0")->select();
        $this->assign('contents', $contents);
		
		//内容信息
		$content = $modelContent->where("id=".$id)->find();
		$this->assign("content",$content);
		
        $this->display('recovery_content_edit');
    }

    /**
     * 修改评估内容操作
     */
    public function doRecoveryContentEdit()
    {
        $modelContent = M('recovery_content');
		
		$data = array();
		$data['id']          = I('post.id');
		$data['pid']         = I('post.pid');
		$data['name']        = I('post.name');
		$data['brother']     = I('post.brother');
		$data['types']       = I('post.types');
		$data['description'] = I('post.description');
		$data['options']     = empty($data['brother'])?'0':'1';
		
		if($modelContent->create($data)){
			$rt = $modelContent->save();
			if($rt){
				$this->success("修改成功.",U('Recovery/recoveryContent/pid/'.$data['pid'].'/types/'.$data['types']));
			}else{
				$this->error("修改失败.");
			}	
		}else{
			$this->error("修改失败,创建数据对象出错.");
		}

    }

    /**
     * 删除评估内容操作
     */
    public function doRecoveryContentDel()
    {
		$id = I('get.id');
		
        $modelContent = M('recovery_content');
		$content = $modelContent->where("id=".$id)->find();
		$affectedRows = $modelContent->where(array('id' => $id))->delete();       //删除商品表信息

		if($affectedRows){
			
			//删除所属下级内容
			$list = $modelContent->where("pid=".$id)->select();
			if(is_array($list)){
				foreach($list as $vo){
					$modelContent->where(array('id' => $id))->delete();		
				}
			}
			$this->success("添加成功.",U('Recovery/recoveryContent/pid/'.$content['pid'].'/types/'.$content['types']));
		}else{
			$this->error("删除失败.");
		}

    }

    /**
     * 评估关联列表
     */
    public function recoveryRelation()
    {
		$modelRepairs = M('repairs');
		$modelContent = M('recovery_content');
		$modelRelation = M('recovery_relation');
		
		// 回收商品列表
		$recoveryList = $modelRepairs->field("id,pro_name")->select();
        $this->assign('recoveryList', $recoveryList);
		
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部回收商品' && $key == 'pro_id') {
                    continue;
                }

                $map[$key] = $val;
            }
        }

		$count = $modelRelation->where($map)->count();
		$page = new \Think\Page($count,20,$map); 
		$relationList = $modelRelation->where($map)->limit($page->firstRow.','.$page->listRows)->order('id ASC')->select();
		$show  = $page->show();
		
		foreach($relationList as $key=>$vo){
			$recovery = $modelRepairs->field("pro_name")->where('id='.$vo['pro_id'])->find();
			$relationList[$key]['pro_name'] = $recovery['pro_name'];
			
			$content = $modelContent->field("name")->where('id='.$vo['type_id'])->find();
			$relationList[$key]['content_name'] = $content['name'];
		}
		
		$this->assign("page",$show);
		$this->assign("relationList",$relationList);
		$this->assign('map', $map);
        $this->display('recovery_relation_list');
    }

    /**
     * 添加评估关联页面
     */
    public function addRecoveryRelation()
    {
		// 回收商品列表
		$modelRepairs = M('repairs');
		$recoveryList = $modelRepairs->field("id,pro_name")->select();
        $this->assign('recoveryList', $recoveryList);
		
		//评估内容列表
		$modelContent = M('recovery_content');
		$contentList = $modelContent->field("id,name")->where('pid<>0 ')->select();
        $this->assign('contentList', $contentList);
		
        $this->display('recovery_relation_add');
    }

    /**
     * 添加评估关联操作
     */
    public function doRecoveryRelationAdd()
    {
        $modelRelation = M('recovery_relation');
		
		$data = array();
		$data['pro_id']      = I('post.pro_id');
		$data['type_id']     = I('post.type_id');
		$data['project_id']  = I('post.project_id');
		$data['money']       = I('post.money');
		$data['is_show']     = I('post.is_show');
		
		if($modelRelation->create($data)){
			$rt = $modelRelation->add();
			if($rt){
				$this->success("添加成功.",U('Recovery/recoveryRelation/pro_id/'.$data['pro_id']));
			}else{
				$this->error("添加失败.");
			}	
		}else{
			$this->error("添加失败,创建数据对象出错.");
		}

    }

    /**
     * 修改评估关联页面
     */
    public function editRecoveryRelation()
    {
		$id = I('get.id');
        $modelRelation = M('recovery_relation');
		
		// 回收商品列表
		$modelRepairs = M('repairs');
		$recoveryList = $modelRepairs->field("id,pro_name")->select();
        $this->assign('recoveryList', $recoveryList);
		
		//评估内容列表
		$modelContent = M('recovery_content');
		$contentList = $modelContent->field("id,name")->where('pid<>0 ')->select();
        $this->assign('contentList', $contentList);
		
		//关联信息
		$relation = $modelRelation->where("id=".$id)->find();
		$this->assign("relation",$relation);
		
        $this->display('recovery_relation_edit');
    }

    /**
     * 修改评估关联操作
     */
    public function doRecoveryRelationEdit()
    {
        $modelRelation = M('recovery_relation');
		
		$data = array();
		$data['id']          = I('post.id');
		$data['pro_id']      = I('post.pro_id');
		$data['type_id']     = I('post.type_id');
		$data['project_id']  = I('post.project_id');
		$data['money']       = I('post.money');
		$data['is_show']     = I('post.is_show');
		
		if($modelRelation->create($data)){
			$rt = $modelRelation->save();
			if($rt){
				$this->success("修改成功.",U('Recovery/recoveryRelation/pro_id/'.$data['pro_id']));
			}else{
				$this->error("修改失败.");
			}	
		}else{
			$this->error("修改失败,创建数据对象出错.");
		}

    }

    /**
     * 删除评估关联操作
     */
    public function doRecoveryRelationDel()
    {
		$id = I('get.id');
		
        $modelRelation = M('recovery_relation');
		$relation = $modelRelation->where("id=".$id)->find();
		$affectedRows = $modelRelation->where(array('id' => $id))->delete();       //删除商品表信息

		if($affectedRows){
			$this->success("删除成功.",U('Recovery/recoveryRelation/pro_id/'.$relation['pro_id']));	
		}else{
			$this->error("删除失败.");
		}

    }
	
	/**
     * 执行快速修改关联状态
     * @return void
     */
    public function postRelationEdit()
    {
        $getId = $_POST['id'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		$data['is_show'] = $getVal;

        // 修改商品信息
        $modelRelation = M('recovery_relation');
        if ($modelRelation->create($data)) 
		{
            $modelRelation->save();
			echo "1";
			exit;
        }
		else
		{
			echo "2";
			exit;
		}

    }

    /**
     * relationCopy 复制关联
     * @date 2015-01-22
     * @return void 
     */

    public function recoveryRelationCopy(){
		
		//回收商品
		$modelRepairs = M('repairs');
		$recoveryRes = $modelRepairs->select();
		$this->assign('recoveryRes', $recoveryRes);
		
    	$this->display('recovery_relation_copy');
    }

    /**
     * doRelationCopy 执行复制关联
     * @date 2015-01-22
     * @return void 
     */

    public function doRecoveryRelationCopy()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		$pro_id = I('post.pro_id');
		$copy_id = I('post.copy_id');
       
        $modelRelation = M("recovery_relation");
		$count = $modelRelation->where('pro_id='.$copy_id)->count();
		if($count > 0){
			$this->error("复制失败,此回收商品已有相关评估关联.");
			exit;
		}
		
		$relationRes = $modelRelation->where('pro_id='.$pro_id)->select();
		foreach($relationRes as $key=>$vo){
			$data = array();
			$data['pro_id']     = $copy_id;
			$data['type_id']    = $vo['type_id'];
			$data['project_id'] = $vo['project_id'];
			$data['money']      = $vo['money'];
			$data['is_show']    = $vo['is_show'];
			$modelRelation->create($data);
			$rt = $modelRelation->add();
			if($rt){
				$this->success('复制成功.', U('Recovery/recoveryRelation', array('pro_id'=>$copy_id)));xit;
			}
		}
		$this->error("复制失败");exit;
    }

    /**
     * 伪代码，没有此方法多文件上传插件出错
     * @return void
     */
    public function index()
    {

    }
	
}
