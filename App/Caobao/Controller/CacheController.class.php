<?php
namespace Caobao\Controller;
use       Think\Controller;

/**
 * CacheController
 * 缓存管理
 */
class CacheController extends MyController {
	
	/**
     * 构造方法
     */
	public function __construct() {
		parent::__construct();
	}
	
    /**
     * 缓存信息
     * @return
     */
    public function index() {
        $caches = array(
            'Admincache'   => RUNTIME_PATH . 'Cache/Admin/',
            "Adminlogs"    => RUNTIME_PATH . 'Logs/Admin/',
			
            'Homecache'    => RUNTIME_PATH . 'Cache/Home',
            "Homelogs"     => RUNTIME_PATH . 'Logs/Home/',
			
			'Forumcache'    => RUNTIME_PATH . 'Cache/Forum',
            "Forumlogs"     => RUNTIME_PATH . 'Logs/Forum/',
			
			'Mobilecache'    => RUNTIME_PATH . 'Cache/Mobile',
            "Mobilelogs"     => RUNTIME_PATH . 'Logs/Mobile/',
			
            "WebTemp"      => RUNTIME_PATH . 'Temp/',
			"WebRuntime"   => RUNTIME_PATH . '~runtime.php/',
            "Webdata"      => RUNTIME_PATH . 'Data/_fields/'
        );
        $this->assign('caches', $caches);
        $this->display();
    }

    /**
     * 删除缓存
     * @return
     */
    public function deleteCache() {
        if (empty($_GET['caches'])) {
            echo '0';
			exit;
        }
        foreach ($_GET['caches'] as $cache) {
            if (isset($cache)) {
                del_dir_or_file($cache);
            }
        }

        echo '1';
		exit;
    }
}
