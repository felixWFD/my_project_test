<?php
namespace Caobao\Controller;
use       Think\Controller;

/**
 * 保障卡控制器
 * 
 * @author: Sun
 * @date: 2015-01-22
 */
class CardsController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */
    public function __construct() {
        parent::__construct();
       
    }

    /**
     * 保障卡列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$cards  = D("Cards");
		$users  = D("Users");
		$orders = D("Orders");
		
		// 分页处理
        if(isset($_GET)){
            foreach($_GET as $key => $val){
                if($val == '' && $key == 'brand_name'){
                    continue;
                }
                $map[$key] = $val;
            }
        }
		$count = $cards->getCardsCountByObject();
		$page = new \Think\Page($count,10,$map); 
		$data = $cards->getCardsByObject(array('m'=>$page->firstRow, 'n'=>$page->listRows));
		foreach($data as &$vo){
			if($vo['user_id'] > 0){
				$vo['user']  = $users->getUserById($vo['user_id']);
			}
			if($vo['order_id'] > 0){
				$vo['order'] = $orders->getOrderById($vo['order_id']);
			}
		}
		unset($vo);
		$this->assign("cards",$data);
		$show  = $page->show();
		$this->assign("page",$show);
		
		$this->display('cards_list');
    }

    /**
     * 添加保障卡
     * @date 2015-01-22
     * @return void 
     */

    public function cardsAdd(){
		
    	$this->display('cards_add');
    }

    /**
     * 执行添加保障卡
     * @date 2015-01-22
     * @return void 
     */

    public function doCardsAdd(){
    	if (!IS_POST) {
			exit('页面错误~');
		}
		
		$cards = D('Cards');
		
		// 判断有没有文件上传
        if (!empty($_FILES['excel']['name'])){
            $upload = new \Think\Upload();                          // 实例化上传类
            // 设置附件上传类型    
            $upload->exts      = array('xls', 'xlsx');
            $upload->subName   = '';
            $upload->rootPath  = './Public/';
            $upload->savePath  = 'Temp/';                           // 设置上传目录    
            $upload->saveName  = date('YmdHis') . mt_rand(100, 999);

            // 上传文件     
            $info  = $upload->uploadOne($_FILES['excel']);
			vendor("PHPExcel.PHPExcel");
			$fileName = './Public/Temp/'.$info['savename'];
			$objReader = \PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load($fileName,$encode='utf-8');
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();       // 取得总行数
			$highestColumn = $sheet->getHighestColumn(); // 取得总列数
			$j = $k =0;
			for($i=2;$i<=$highestRow;$i++){
				$no   = $objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue();
				$code = $objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue();
				if(!empty($no) && !empty($code)){
					$ck = $cards->getCardsByCardNo($no);
					if(empty($ck)){
						$cards->insertCards(array('card_no'=>$no, 'card_code'=>$code));
						$j++;
					}else{
						$k++;
					}
				}
			}
            unlink($fileName);
        }
		$this->success('添加成功 '.$j.' 个,失败 '.$k.' 个', 'index');
    }
 
    /**
     * 删除保障卡
     * @date 2015-01-22
     * @return void 
     */
    public function deleteCards(){
		
        $id = I('get.id');
        $cards = D('Cards');
        $rt = $cards->deleteCards($id);
        if($rt['status']){
            $this->success("删除成功",U("Cards/index"));
        }else{
            $this->error("删除失败");
        }
    }

}