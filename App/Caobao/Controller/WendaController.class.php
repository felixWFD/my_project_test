<?php   
namespace Caobao\Controller;
use       Think\Controller;

    /**
     * 问答管理
     * 
     * @author: Sun
     * @date: 2015-01-12
     */

 class WendaController extends MyController{
     public function __construct() {

        parent::__construct();
       
    }

    /**
     * 问答类别分类列表 Category wenda_cat and display wenda_cat
     * @date 2015-01-22
     * @return void 
     */
    public function categoryList(){
        
         $db = M("wenda_cat");
         $count = $db->count();
         $page = new \Think\Page($count,10); 
         $list = $db -> limit($page->firstRow.','.$page->listRows)->select();
         //dump($list);
         $this->assign("list",$list);
         $show  = $page->show();
         $this->assign("page",$show);
         $this->display();
    }

    /**
     * 执行添加问答类别
     * @date 2015-01-22
     * @return void 
     */
    public function doAddCategory(){        
        $db = M("wenda_cat");
        if($db->create()){
            $res=$db->add();
            if($res){
                $this->success("添加成功");
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }
        
    }

    /**
     * 修改问答类别
     * @date 2015-01-22
     * @return void 
     */
    public function editCategory(){
        $id = $_GET['id'];
        $data = M("wenda_cat");
        $list = $data->where("id =".$id)->find();       

        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行修改问答类别
     * @date 2015-01-22
     * @return void 
     */
    public function doEditCategory(){
        $db = M("wenda_cat");
        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("Wenda/categoryList"));
                exit;
            }
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除问答类别
     * @date 2015-01-22
     * @return void 
     */
    public function delCategory(){
        $id = $_GET['id'];
        $db = M("wenda_cat");
        $ls = $db->where("id =".$id)->delete();

        if($ls){
            $this->success("删除成功",U("Wenda/categoryList"));
        }else{
            $this->error("删除失败");
        }
    }

    public function del(){
        $ids = array();
        $ids = $_GET['id'];
        $data = implode(",",$ids);
        $db = M("wenda_cat");
        $list= $db ->where("id in (".$data.")")->delete();
        if($list){
            echo 1;
        }else{
            echo 0;
        }
    }

    /**
     * 添加问答
     * @date 2015-01-22
     * @return void 
     */
    public function addWenda(){
		$db = M("wenda_cat");
        $list = $db ->where("is_show=1")->select();
      
        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行添加问答
     * @date 2015-01-22
     * @return void 
     */
    public function doAddWenda(){
		
		//如果上传了图片
		if (isset($_FILES)) {
			if (($_FILES['file']['error']) == 0) {
				$_POST['thumbnail']=$this->upload();
			}
		}
        
        $_POST['create_time']= time();
		$_POST['opt_user'] = $_SESSION['id'];

        $db = M("wenda_contents");
        if($db->create()){
            $rt=$db->add();
            if($rt){
				CBWBaiduPush(array(C('SITE_URL')."/wenda/".$rt.".html"));        // 百度SEO推送
				CBWBaiduPush(array(C('MOBILE_URL')."/wenda/".$rt.".html"), 'm'); // 百度SEO推送
                $this->success("添加成功",U("Wenda/wenda"));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      
    }

    
    /**
     * 单图片上传
     * @date 2015-01-22
     * @return void 
     */
 	public function upload(){
		
		$upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/Wenda/"; //图片保存文件夹路径
		$upload->maxSize = 3145728;             // 设置附件上传大小
		$upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
		$upload->rootPath = $rootPath;
		$upload->savePath = '';                          // 设置附件上传目录    // 上传文件       
		$info = $upload->uploadOne($_FILES['file']);
		$simg = $info['savepath'].$info['savename'];
		
		//裁剪图像
		$image = new \Think\Image();
		$image->open($rootPath.$simg);
		$image->thumb('250', '140')->save($rootPath.$simg); //生产缩略图
		
		if (!$info) {
			// 上传错误提示错误信息
			$this->error($upload->getError());
		} else {
			// 上传成功 获取上传文件信息
			return $info['savepath'].$info['savename'];
		}
		
 	}

    /**
     * 修改问答
     * @date 2015-01-22
     * @return void 
     */
    public function editWenda(){
		
        $id = $_GET['id'];
        $data = M("wenda_contents");
        $list = $data->where("id =".$id)->find();       

		$db = M("wenda_cat");
        $catlist = $db ->where("is_show=1")->select();
	
        $this->assign("list",$list);
		$this->assign("catlist",$catlist);
        $this->display();

    }

    /**
     * 问答List
     * @date 2015-01-22
     * @return void 
     */
    public function wenda(){
		
		//问答分类
		$modelCat = M('wenda_cat');
		$catList = $modelCat->where('is_show=1')->select();
		$this->assign("catList",$catList);
		
		$modelWenda = M('wenda_contents');
		
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部问答' && $key == 'cat_id') {
                    continue;
                }

                if ($val == '' && $key == 'title') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		if($_SESSION['id'] == 6 || $_SESSION['id'] == 7 || $_SESSION['id'] == 8){
			$map['opt_user'] = $_SESSION['id'];
		}
		
		$count = $modelWenda->where($map)->count();
        $page = new \Think\Page($count,15,$map); 
		
		$list = $modelWenda->where($map)
                        ->limit($page->firstRow.','.$page->listRows)
                        ->order('create_time DESC')
                        ->select();
		foreach($list as $key=>$item){
			$cat = $modelCat->where("id=".$item['cat_id'])->find();
			$list[$key]['cat_name'] = $cat['cat_name'];
		}
        $this->assign("list",$list);
        $show  = $page->show();
        $this->assign("page",$show);
        $this->display();

    }

    /**
     * 执行修改问答
     * @date 2015-01-22
     * @return void 
     */
    public function updateWenda(){
	
		$db = M("wenda_contents");
		$wenda = $db->field("opt_user,thumbnail")->where("id =".$_POST['id'])->find();
		//图片处理,无上传则保留原来图片,有上传则清除旧图片
		if (isset($_FILES)) {
			if (($_FILES['file']['error']) == 0) {
				$img = $this->upload();
				if($img){
					$_POST['thumbnail'] = $img;
					if(!empty($wenda['thumbnail'])){
						unlink('Public/Wenda/' . $wenda['thumbnail']);
					}
				}else{
					$_POST['thumbnail'] = $wenda['thumbnail'];
				}
			}
		}
		if($_SESSION['id'] != $wenda['opt_user'] && ($_SESSION['id']==6 || $_SESSION['id']==7 || $_SESSION['id']==8)){
			$this->error("不是您添加的问答");
		}
        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("Wenda/wenda"));
                exit;
            }else{
				$this->error("提交表单无修改");
			}
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除问答
     * @date 2015-01-22
     * @return void 
     */
    public function delWenda(){
		if($_SESSION['id'] == 6 || $_SESSION['id'] == 7 || $_SESSION['id'] == 8){
			$this->error("删除失败,请联系管理员删除");
		}
        $id = $_GET['id'];
        $db = M("wenda_contents");
		$info = $db->field("thumbnail")->where("id =".$id)->find();
        $ls = $db->where("id =".$id)->delete();
        if($ls){
			if(!empty($info['thumbnail'])){
				unlink('Public/Wenda/' . $info['thumbnail']);
			}
            $this->success("删除成功",U("Wenda/wenda"));
        }else{
            $this->error("删除失败");
        }
    }


}
