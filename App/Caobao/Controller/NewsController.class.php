<?php   
namespace Caobao\Controller;
use       Think\Controller;

    /**
     * 新闻管理
     * 
     * @author: Sun
     * @date: 2015-01-12
     */

 class NewsController extends MyController{
     public function __construct() {

        parent::__construct();
       
    }

    /**
     * 新闻类别分类列表 Category news_cat and display news_cat
     * @date 2015-01-22
     * @return void 
     */
    public function categoryList(){
        
         $db = M("news_cat");
         $count = $db->count();
         $page = new \Think\Page($count,10); 
         $list = $db -> limit($page->firstRow.','.$page->listRows)->select();
         //dump($list);
         $this->assign("list",$list);
         $show  = $page->show();
         $this->assign("page",$show);
         $this->display();
    }

    /**
     * 添加新闻类别 display adding news category
     * @date 2015-01-22
     * @return void 
     */
    public function addNewsCategory(){
        $this->display();
    }

    /**
     * 执行添加新闻类别 display adding news category
     * @date 2015-01-22
     * @return void 
     */
    public function addCategory(){        
        $db = M("news_cat");
        if($db->create()){
            $res=$db->add();
            if($res){
                $this->success("添加成功");
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }
        
    }

    /**
     * 修改新闻类别 editCategory as
     * @date 2015-01-22
     * @return void 
     */
    public function editCategory(){
        $id = $_GET['id'];
        $data = M("news_cat");
        $list = $data->where("id =".$id)->find();       
        //dump($id);
        //dump($list);
        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行修改新闻类别
     * @date 2015-01-22
     * @return void 
     */
    public function categoryUpdate(){
        $db = M("news_cat");
        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("News/categoryList"));
                exit;
            }
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除新闻类别
     * @date 2015-01-22
     * @return void 
     */
    public function delCategory(){
        $id = $_GET['id'];
        $db = M("news_cat");
        $ls = $db->where("id =".$id)->delete();

        if($ls){
            $this->success("删除成功",U("News/categoryList"));
        }else{
            $this->error("删除失败");
        }
    }

    public function del(){
        $ids = array();
        $ids = $_GET['id'];
        $data = implode(",",$ids);
        $db = M("news_cat");
        $list= $db ->where("id in (".$data.")")->delete();
        if($list){
            echo 1;
        }else{
            echo 0;
        }
    }

    /**
     * 添加新闻
     * @date 2015-01-22
     * @return void 
     */
    public function addNews(){
		$db = M("news_cat");
        $list = $db ->where("is_show=1")->select();
      
        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行添加新闻
     * @date 2015-01-22
     * @return void 
     */
    public function doAddNews(){
		
		//如果上传了图片
		if (isset($_FILES)) {
			if (($_FILES['file']['error']) == 0) {
				$_POST['thumbnail']=$this->upload();
			}
		}
        
        $_POST['create_time'] = time();
		$_POST['update_time'] = time();
		$_POST['opt_user']    = $_SESSION['id'];

        $db = M("news_contents");
        if($db->create()){
            $rt=$db->add();
            if($rt){
				CBWBaiduPush(array(C('SITE_URL')."/news/".$rt.".html"));           // 百度SEO推送
				CBWBaiduPush(array(C('MOBILE_URL')."/news/".$rt.".html"), 'm');    // 百度SEO推送
                $this->success("添加成功",U("News/news"));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      
    }

    
    /**
     * 单图片上传
     * @date 2015-01-22
     * @return void 
     */
 	public function upload(){
		
		$upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/News/"; //图片保存文件夹路径
		$upload->maxSize = 3145728;             // 设置附件上传大小
		$upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
		$upload->rootPath = $rootPath;
		$upload->savePath = '';                          // 设置附件上传目录    // 上传文件       
		$info = $upload->uploadOne($_FILES['file']);
		$simg = $info['savepath'].$info['savename'];
		
		//裁剪图像
		$image = new \Think\Image();
		$image->open($rootPath.$simg);
		$image->thumb('250', '140')->save($rootPath.$simg); //生产缩略图
		
		if (!$info) {
			// 上传错误提示错误信息
			$this->error($upload->getError());
		} else {
			// 上传成功 获取上传文件信息
			return $info['savepath'].$info['savename'];
		}
		
 	}

    /**
     * 修改新闻
     * @date 2015-01-22
     * @return void 
     */
    public function editNews(){
        $id = $_GET['id'];
        $data = M("news_contents");
        $list = $data->where("id =".$id)->find();       

		$db = M("news_cat");
        $catlist = $db ->where("is_show=1")->select();
	
        $this->assign("list",$list);
		$this->assign("catlist",$catlist);
        $this->display();

    }

    /**
     * 新闻List
     * @date 2015-01-22
     * @return void 
     */
    public function news(){
		
		//新闻分类
		$modelCat = M('news_cat');
		$catList = $modelCat->where('is_show=1')->select();
		$this->assign("catList",$catList);
		
		$modelNews  = M('news_contents');
		$modelUsers = M('admin_users');
		
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key=>$val) {
                if ($val == '查找全部新闻' && $key == 'cat_id') {
                    continue;
                }

                if ($val == '' && $key == 'title') {
                    continue;
                }
				if($key == 'title'){
					$map[$key] = array('like', '%'.$val.'%');
				}else{
					$map[$key] = $val;
				}
            }
        }
		$count = $modelNews->where($map)->count();
        $page = new \Think\Page($count,15); 
		$list = $modelNews->where($map)
                        ->limit($page->firstRow.','.$page->listRows)
                        ->order('create_time DESC')
                        ->select();
		foreach($list as $key=>$item){
			$cat = $modelCat->where("id=".$item['cat_id'])->find();
			$list[$key]['cat_name'] = $cat['cat_name'];
			$cat = $modelUsers->where("id=".$item['opt_user'])->find();
			$list[$key]['user_name'] = $cat['username'];
		}
        $this->assign("list",$list);
        $show  = $page->show();
        $this->assign("page",$show);
        $this->display();

    }

    /**
     * 执行修改新闻
     * @date 2015-01-22
     * @return void 
     */
    public function updateNews(){
	
		$db = M("news_contents");
		
		//图片处理,无上传则保留原来图片,有上传则清除旧图片
		if (isset($_FILES)) {
			if (($_FILES['file']['error']) == 0) {
				$img = $this->upload();
				$oldInfo = $db->field("thumbnail")->where("id =".$_POST['id'])->find();
				if($img){
					$_POST['thumbnail'] = $img;
					if(!empty($oldInfo['thumbnail'])){
						unlink('Public/News/' . $oldInfo['thumbnail']);
					}
				}else{
					$_POST['thumbnail'] = $oldInfo['thumbnail'];
				}
			}
		}
		
		$_POST['update_time'] = time();
		$_POST['opt_user']    = $_SESSION['id'];
		
        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("News/news"));
                exit;
            }else{
				$this->error("提交表单无修改");
			}
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除新闻
     * @date 2015-01-22
     * @return void 
     */
    public function delNews(){
        $id = $_GET['id'];
        $db = M("news_contents");
		$info = $db->field("thumbnail")->where("id =".$id)->find();
        $ls = $db->where("id =".$id)->delete();
        if($ls){
			if(!empty($info['thumbnail'])){
				unlink('Public/News/' . $info['thumbnail']);
			}
            $this->success("删除成功",U("News/news"));
        }else{
            $this->error("删除失败");
        }
    }


}
