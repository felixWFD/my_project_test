<?php
namespace Caobao\Controller;
use       Think\Controller;

class KeywordsController extends MyController
{
    public function __construct(){
        parent::__construct();
    }

    //获得关键词列表
    public function index(){

        $modelKeywords = M('keywords');
        // 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '' && $key == 'keyword') {
                    continue;
                }
                $map[$key] = $val;
            }
        }

        // 查询总记录数
        $getPageCounts = $modelKeywords->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $keyList = $modelKeywords->where($map)->order('sort ASC,id ASC')->limit($page->firstRow, $page->listRows)->select();

        $pageShow = $page->show();
        $this->assign('page', $pageShow);
        $this->assign('keyList', $keyList);
        $this->display();
    }

    //修改关键词信息
    public function editKeywords(){
		
		$id = I('get.id');
        $modelKeyword = M('keywords');
        $data = $modelKeyword->where("id = $id")->find();
        $this->assign('data', $data);
		
        $this->display('editKeywords');
    }

    //修改关键词信息操作
    public function doEditKeywords(){
		
		$id = I('post.id');
        $keyword = I('post.keyword');
		$links = I('post.links');
		
        if (empty($keyword) || empty($links)) {
            $this->error('内容为空,修改失败');
            exit;
        }
		
		$data = array();
		$data['keyword'] = $keyword;
        $data['links']   = $links;
		$data['sort']    = I('post.sort');

		$modelKeywords = M('keywords');
        //提交保存
        $rt = $modelKeywords->where("id=$id")->save($data);
        if ($rt > 0) {
            $this->success('修改成功', U('Keywords/index'));
        } else {
            $this->error('修改失败');
        }
    }

    //添加关键词
    public function addKeywords(){

        $this->display('addKeywords');
    }
	
	//添加关键词操作
	public function doAddKeywords(){
		
		$keyword = I('post.keyword');
		$links = I('post.links');
		
        if (empty($keyword) || empty($links)) {
            $this->error('内容为空,添加失败');
            exit;
        }
		
		$data = array();
		$data['keyword'] = $keyword;
        $data['links']   = $links;
		$data['sort']    = I('post.sort');

        $modelKeywords = M('keywords');
        if ($modelKeywords->create($data)) {
            $rt = $modelKeywords->add();
            if ($rt) {
                $this->success('添加成功', U('Keywords/index'));
                exit;
            } else {
                $this->error('添加失败');
            }
        } else {
            $this->error('上传出错。');
        }
	}

    //删除单个关键词
    public function delKeywords()
    {
        $id = I('get.id');
        $modelKeywords = M('keywords');
        $rt = $modelKeywords->where("id=$id")->delete();
        if ($rt > 0) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }


    }
	
	/**
     * 执行商品修改操作
     * @return void
     */
    public function postShowEdit(){
        $getId = I('post.id');
		$getVal = I('post.val');

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		$data['is_show'] = $getVal;

        $modelKeywords = M('keywords');
        if ($modelKeywords->create($data)) {
            $modelKeywords->save();
			echo "1";
			exit;
        }else{
			echo "0";
			exit;
		}

    }
	
}
