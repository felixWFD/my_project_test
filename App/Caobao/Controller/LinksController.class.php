<?php
namespace Caobao\Controller;
use       Think\Controller; 

class LinksController extends MyController{
     public function __construct() {

        parent::__construct();
       
    }

 	/**
     * 友情链接列表
     * @date 2015-01-22
     * @return void 
     */
    public function linksManager(){
        
         $db = M("links");
         $count = $db->count();
         $page = new \Think\Page($count,10); 
         $list = $db -> limit($page->firstRow.','.$page->listRows)->select();

         $this->assign("list",$list);
         $show  = $page->show();
         $this->assign("page",$show);
         $this->display();
    }

    /**
     * 添加友情链接
     * @date 2015-01-22
     * @return void 
     */
    public function addLinks(){
        $this->display();
    }

    /**
     * 执行添加友情链接
     * @date 2015-01-22
     * @return void 
     */
    public function doAddLinks(){
		
        $db = M("links");
        if($db->create()){
            $res=$db->add();
            if($res){
                $this->success("添加成功");
            }else{
                $this->error("添加失败");
            }
        }else{
            $this->error("添加失败");
        }      
    }

    
    /**
     * 上传图片
     * 此方法被友情链接页，上传图片所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/Links/"; //图片保存文件夹路径
        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
        echo $imgUrl;
    }

    /**
     * 修改友情链接
     * @date 2015-01-22
     * @return void 
     */
    public function editLinks(){
        $id = $_GET['id'];
        $data = M("links");
        $list = $data->where("id =".$id)->find();       
        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行修改友情链接
     * @date 2015-01-22
     * @return void 
     */
    public function doEditLinks(){
	
		$db = M("links");
        
		//有上传则清除旧图片
		//判断是否有缩略图上传,有上传则删除旧图片
		$picture = $_POST['picture'];
		$org_picture = $_POST['org_picture'];
		if(!empty($picture)){
			unlink("./Public/Links/".$org_picture);        //删除旧图片
			$_POST['picture'] = $picture;
		}else{
			$_POST['picture'] = $org_picture;
		}

        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("Links/linksManager"));
                exit;
            }else{
				$this->error("提交表单无修改");
			}
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除友情链接
     * @date 2015-01-22
     * @return void 
     */
    public function delLinks(){
        $id = $_GET['id'];
        $db = M("links");
		$info = $db->field("picture")->where("id =".$id)->find();
		if(!empty($info['picture'])){
			unlink('Public/Links/' . $info['picture']);
		}
		
        $ls = $db->where("id =".$id)->delete();
        if($ls){
            $this->success("删除成功",U("Links/linksManager"));
        }else{
            $this->error("删除失败");
        }
    }/**
     * 批量删除友情链接
     * @date 2015-01-18
     * @return void 
     */
    public function batchDeleteLinks ()
    {
        $ids = I('post.ids');
        $idsArray = implode(',', $ids);

        // 实例化数据表模型
        $modelLinks = M('links');
		
		foreach($ids as $id)
		{
			$info = $modelLinks->field("picture")->where("id=".$id)->find();
			unlink("./Public/Links/".$info['picture']);                    //删除头像图片
		}
        $affectedRows = $modelLinks->where("id IN({$idsArray})")->delete();

        if ($affectedRows > 0)
        {
            $this->success('删除成功');
        }
        else
        {
            $this->error('删除失败');
        }
    }
    



}