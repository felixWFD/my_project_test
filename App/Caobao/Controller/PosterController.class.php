<?php
namespace Caobao\Controller;
use       Think\Controller;

/**
 * 海报控制器
 * 
 * @author: Sun
 * @date: 2015-01-22
 */
class PosterController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */
    public function __construct() {
        parent::__construct();
       
    }

    /**
     * 海报列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){

		$poster = D("Poster");
		
		// 分页处理
        if(isset($_GET)){
            foreach($_GET as $key => $val){
                if($val == '' && $key == 'brand_name'){
                    continue;
                }
                $map[$key] = $val;
            }
        }
		$count = $poster->getPosterCountByObject();
		$page = new \Think\Page($count,10,$map); 
		$data = $poster->getPosterByObject(array('m'=>$page->firstRow, 'n'=>$page->listRows));
		$this->assign("data",$data);
		$show  = $page->show();
		$this->assign("page",$show);
		
		$this->display('poster_list');
    }

    /**
     * 添加海报
     * @date 2015-01-22
     * @return void 
     */

    public function posterAdd(){
    	$this->display('poster_add');
    }

    /**
     * 执行添加海报
     * @date 2015-01-22
     * @return void 
     */

    public function doPosterAdd(){
    	if (!IS_POST) {
			exit('页面错误~');
		}
		
		$poster = D('Poster');

		$data = array(
			'name'   => I('post.name'),
			'image'  => I('post.image'),
			'status' => I('post.status')
		);
		
        $rt = $poster->insertPoster($data);
		if($rt['status'] > 0){
			$this->success('添加成功',U('Poster/index'));exit;
		} else {
			$this->error('添加失败');
		}
    }

    /**
     * 编辑海报
     * @date 2015-01-22
     * @return void 
     */

    public function posterEdit(){
		$id = I('get.id');
		$poster = D('Poster');
		$data = $poster->getPosterById($id);
		$this->assign("data",$data);
    	$this->display('poster_edit');
    }

    /**
     * 执行编辑海报
     * @date 2015-01-22
     * @return void 
     */

    public function doPosterEdit(){
    	if (!IS_POST) {
			exit('页面错误~');
		}
		
		$poster = D('Poster');
		
		$postId = I('post.id');
		$data   = array(
			'name'   => I('post.name'),
			'status' => I('post.status')
		);
		
		//判断是否有图上传,有上传则删除旧图片
		$image     = I('post.image');
		$org_image = I('post.org_image');
		if(!empty($image)){
			unlink("./Public/Poster/".$org_image);        //删除旧图片
			$data['image'] = $image;
		}else{
			$data['image'] = $org_image;
		}
			
        $rt = $poster->updatePoster($postId, $data);
		if($rt['status'] > 0){
			$this->success('编辑成功',U('Poster/index'));exit;
		} else {
			$this->error('编辑失败');
		}
    }
 
    /**
     * 删除海报
     * @date 2015-01-22
     * @return void 
     */
    public function deletePoster(){
		
        $id = I('get.id');
        $poster = D('Poster');
        $rt = $poster->deletePoster($id);
        if($rt['status']){
            $this->success("删除成功",U("Poster/index"));
        }else{
            $this->error("删除失败");
        }
    }

    /**
     * 上传图片
     * 此方法被添加/修改页，上传图片所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload(){
        $upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/Poster/";                       //图片保存文件夹路径
        $upload->maxSize = 3145728;                           // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                               // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
        echo $imgUrl;
    }

}