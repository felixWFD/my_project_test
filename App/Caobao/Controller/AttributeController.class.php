<?php
namespace Caobao\Controller;
use       Think\Controller;

    /**
     * Attribute
     * 
     * @author: Sun
     * @date: 2015-01-22
     */

class AttributeController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * AttributeList 属性列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$modelAttribute = M("attribute");
	
		$count = $modelAttribute->count();
		$page = new \Think\Page($count,20); 
		$list = $modelAttribute->limit($page->firstRow.','.$page->listRows)->order('sort ASC')->select();
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		$this->display('attribute_list');
    }

    /**
     * attributeAdd 添加属性
     * @date 2015-01-22
     * @return void 
     */

    public function attributeAdd(){
		
    	$this->display('attribute_add');
    }

    /**
     * doAttributeAdd 执行添加属性
     * @date 2015-01-22
     * @return void 
     */

    public function doAttributeAdd()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		
		$data = array();
        $data['attr_name'] = I('post.attr_name');
		$data['attr_ident'] = I('post.attr_ident');
		$data['sort'] = I('post.sort');
       
        $modelAttribute = M("attribute");
        if($modelAttribute->create($data)){
            $res=$modelAttribute->add();
            if($res){
                $this->success('添加成功.'.$lastID,U("Attribute/index"));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * attributeEdit 修改属性
     * @date 2015-01-22
     * @return void 
     */
    public function attributeEdit(){
        
		$id = $_GET['id'];
        $modelAttribute = M("attribute");
        $attribute = $modelAttribute->where("id =".$id)->find();  
		$this->assign("attribute",$attribute);
		
        $this->display('attribute_edit');
    }

    
     /**
     * doAttributeEdit 执行修改属性
     * @date 2015-01-22
     * @return void 
     */
    public function doAttributeEdit()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$data = array();
		$data['id'] = I('post.id');
        $data['attr_name'] = I('post.attr_name');
		$data['attr_ident'] = I('post.attr_ident');
		$data['sort'] = I('post.sort');
		
        $modelAttribute = M("attribute");
		if($modelAttribute->save($data))
		{
			$this->success('修改成功.'.$lastID,U("Attribute/index"));
		}
		else
		{
			$this->error('修改失败！');
		}
    }
	
    /**
     * attributeDelete 删除属性
     * @date 2015-01-22
     * @return void 
     */
    public function attributeDelete(){
        $id = $_GET['id'];
        $modelAttribute = M("attribute");
        $ls = $modelAttribute->where("id =".$id)->delete();
        if($ls){
            $this->success("删除成功",U("Attribute/index"));
        }else{
            $this->error("删除失败");
        }
    }


}