<?php
namespace Caobao\Controller;
use       Think\Controller;

header('Content-Type:text/html; charset=utf-8');

class ProductController extends MyController
{

    /**
     * 构造函数，初始化
     * @date 2015-01-16
     */
    public function __construct()
    {
		//此处为解决Uploadify在火狐下出现http 302错误 重新设置SESSION
		//if (isset($_REQUEST['session_id'])) {
		//	session_id($_REQUEST['session_id']);
		//	session_start();
		//}
		
        parent::__construct();
    }

    /**
     * 普通商品列表
     * @date 2015-01-15
     * @return void
     */
    public function index()
    {
        // 获取商品类别
		$classify_id = I('get.classify_id');
		$type_id     = I('get.type_id');
		$brand_id    = I('get.brand_id');
		$cat_id      = I('get.cat_id');
		$pro_brand   = I('get.pro_brand');
		$is_on_sale  = I('get.is_on_sale');
		$is_card     = I('get.is_card');
		$is_shop     = I('get.is_shop');
		$is_new      = I('get.is_new');
		$is_hot      = I('get.is_hot');
		$is_shipping = I('get.is_shipping');
		$pro_name    = I('get.pro_name');
		
        $modelCats = M('product_cat');
		$classifyRes = $modelCats->where("fid=1")->select();
		$this->assign('classifyRes', $classifyRes);
		
		//商品品牌
		$modelBrand = M('product_brand');
		$brand = $modelBrand->field("id,brand_name")->where("is_show=1")->order("sort ASC,id DESC")->select();
		$this->assign('brand', $brand);
		
        $modelProducts = M('products');
        $map['recycle'] = 0;
		$where = "recycle=0";
		
		if ($classify_id != '请选择分类' && $classify_id != '') {
			$map['classify_id'] = $classify_id;
			$where .= " AND classify_id=".$classify_id;
			$typeRes = $modelCats->where("is_show=1 AND fid=".$classify_id)->select();
			$this->assign('typeRes', $typeRes);
			$this->assign('classify_id', $classify_id);
		}

		if ($type_id != '请选择' && $type_id != '') {
			$map['type_id'] = $type_id;
			$where .= " AND type_id=".$type_id;
			$brandRes = $modelCats->where("is_show=1 AND fid=".$type_id)->select();
			$this->assign('brandRes', $brandRes);
			$this->assign('type_id', $type_id);
		}

		if ($brand_id != '请选择' && $brand_id != '') {
			$map['brand_id'] = $brand_id;
			$where .= " AND brand_id=".$brand_id;
			$catRes = $modelCats->where("is_show=1 AND fid=".$brand_id)->select();
			$this->assign('catRes', $catRes);
			$this->assign('brand_id', $brand_id);
		}

		if ($cat_id != '请选择' && $cat_id != '') {
			$map['cat_id'] = $cat_id;
			$where .= " AND cat_id=".$cat_id;
			$this->assign('cat_id', $cat_id);
		}

		if ($pro_brand != '查找品牌' && $pro_brand != '') {
			$map['pro_brand'] = $pro_brand;
			$where .= " AND pro_brand=".$pro_brand;
		}

		if ($is_on_sale != '是否上架' && $is_on_sale != '') {
			$map['is_on_sale'] = $is_on_sale;
			$where .= " AND is_on_sale=".$is_on_sale;
		}
		
		if ($is_card != '是否卡类商品' && $is_card != '') {
			$map['is_card'] = $is_card;
			$where .= " AND is_card=".$is_card;
		}

		if ($is_shop != '是否店铺专供' && $is_shop != '') {
			$map['is_shop'] = $is_shop;
			$where .= " AND is_shop=".$is_shop;
		}
		
		if ($is_new != '是否新品' && $is_new != '') {
			$map['is_new'] = $is_new;
			$where .= " AND is_new=".$is_new;
		}
		
		if ($is_hot != '是否热销' && $is_hot != '') {
			$map['is_hot'] = $is_hot;
			$where .= " AND is_hot=".$is_hot;
		}
		
		if ($is_shipping != '是否包邮' && $is_shipping != '') {
			$map['is_shipping'] = $is_shipping;
			$where .= " AND is_shipping=".$is_shipping;
		}

		if ($pro_name != '') {
			$map['pro_name'] = $pro_name;
			$where .= " AND pro_name like '%".$pro_name."%'";
			$this->assign('pro_name', $pro_name);
		}

        // 查询总记录数
        $getPageCounts = $modelProducts->where($where)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);
        $productsList = $modelProducts->where($where)->order('id DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($productsList as $key=>$vo){
			$classify = $modelCats->where("id=".$vo['classify_id'])->find();
			$productsList[$key]['classify'] = $classify['cat_name'];
		}
        $pageShow = $page->show();
		
        $this->assign('page', $pageShow);
        $this->assign('productsList', $productsList);
        $this->display('index');
    }

    /**
     * 获取添加商品页模板
     * @return void
     */
    public function productAdd()
    {
		// 商品分类
        $modelCat = M('product_cat');
        $cats = $modelCat->where('fid=1')->select();
        $this->assign('cats', $cats);
		
		//获取工具分类商品
		$modelProducts = M('products');
		$linkRes = $modelProducts->field('id,pro_name')->where('type_id=51')->select();
		$this->assign('linkRes', $linkRes);
		
		//商品品牌
		$modelBrand = M('product_brand');
		$brand = $modelBrand->field("id,brand_name")->where("is_show=1")->order("sort ASC,id DESC")->select();
		$this->assign('brand', $brand);
		
        $this->display('product_add');
    }

    /**
     * 执行产品添加操作
     * @date 2015-01-16
     * @return void
     */
    public function doAddProduct()
    {

        // 处理基本通用信息，包含fetcher
		$post = $_POST;
		$product = array();
		$product['classify_id']   = $post['classify_id'];
        $product['type_id']       = $post['type_id'];
        $product['brand_id']      = $post['brand_id'];
        $product['cat_id']        = $post['cat_id'];
		$product['pro_brand']     = $post['pro_brand'];
        $product['pro_name']      = $post['pro_name'];
        $product['pro_subname']   = $post['pro_subname'];
        $product['pro_price']     = $post['pro_price'];
		$product['pro_sn']        = $post['pro_sn'];
        $product['stock_num']     = $post['stock_num'];
        $product['keywords']      = $post['keywords'];
        $product['description']   = $post['description'];
        $product['is_card']       = empty($post['is_card'])?0:1;
        $product['is_shop']       = empty($post['is_shop'])?0:1;
        $product['is_new']        = empty($post['is_new'])?0:1;
		$product['is_hot']        = empty($post['is_hot'])?0:1;
		$product['is_shipping']   = empty($post['is_shipping'])?0:1;
		$product['is_quality']    = empty($post['is_quality'])?0:1;
		$product['is_return']     = empty($post['is_return'])?0:1;
		$product['is_buy']        = empty($post['is_buy'])?0:1;
		$product['sale_money']    = $post['sale_money'];
		$product['warranty']      = $post['warranty'];
		$product['virtual_money'] = $post['virtual_money'];
		$product['is_on_sale']    = $post['is_on_sale'];
        $product['list_image']    = $post['list_image'];
        $product['feacher']       = $post['feacher'];
        $product['add_time']      = time();
        $product['update_time']   = time();

        // 当商品名为空时，拒绝添加
        if (empty($product['pro_name'])) {
            $this->error('商品添加失败');
            exit;
        }
		
        $modelProducts = M('products');
        if ($modelProducts->create($product)) {
            $rt = $modelProducts->add();
            if ($rt) {
				//写入关联商品表
				if(!empty($post['linkproducts'])){
					$modelProductLink = M('product_link');
					$links = explode(',',$post['linkproducts']);
					foreach($links as $v){
						$link = array();
						$link['pro_id'] = $rt;
						$link['link_pro_id'] = $v;
						$modelProductLink->data($link)->add();
					}
				}
				
				CBWBaiduPush(array(C('SITE_URL')."/detail/".$rt.".html"));           // 百度SEO推送
				CBWBaiduPush(array(C('MOBILE_URL')."/detail/".$rt.".html"), 'm');    // 百度SEO推送
                // 写入日志
                $cLog = new \Caobao\Controller\LogController();
                $cLog->setLog('商品（ID:' . $rt . '）添加成功');
                $this->success('商品添加成功',U('Product/index',array('classify_id'=>$post['classify_id'], 'type_id'=>$post['type_id'], 'brand_id'=>$post['brand_id'], 'cat_id'=>$post['cat_id'])));
                exit;
            } else {
                $this->error('商品添加失败');
            }

        } else {
            $this->error('上传出错。');
        }

    }
	
	/**
     * 获取修改商品页模板
     * @return void
     */
    public function productEdit()
    {
		
        $id = I('get.id');
		$model = M();
		
		//商品品牌
		$modelBrand = M('product_brand');
		$brand = $modelBrand->field("id,brand_name")->where("is_show=1")->order("sort ASC,id DESC")->select();
		$this->assign('brand', $brand);
		
        // 获取产品详细信息
        $modelProducts = M('products');
		$productRes = $modelProducts->where("id='{$id}'")->find();
		
		// 商品归属店铺信息
		if($productRes['shop_id'] > 0){
			$shop = $model->table('__USER_SHOP__ AS us')
					->field('us.user_id,us.shop_name,us.user_name,us.user_phone,u.account,u.mobile')
					->join('__USERS__ AS u ON u.id=us.user_id')
					->where('us.id='.$productRes['shop_id'])
					->find();
			$this->assign('shop', $shop);
		}
		
		//获取商品关联表
		$modelProductLink = M('product_link');
		$links = $modelProductLink->where("pro_id='{$id}'")->select();
		foreach($links as $vo){
			$link[] = $vo['link_pro_id'];
		}
		$linkRes = $modelProducts->field('id,pro_name')->where('type_id=51')->select();
		if(!empty($link)){
			foreach($linkRes as $vo){
				if(in_array($vo['id'],$link)){
					$rightRes[] = $vo;
				}else{
					$leftRes[] = $vo;
				}
			}
		}else{
			$leftRes = $linkRes;
		}
		$this->assign('linkproducts', implode(',',$link));
		$this->assign('leftRes', $leftRes);
		$this->assign('rightRes', $rightRes);
		
		//商品分类
		$modelCat = M('product_cat');
		$classify_cats = $modelCat->where('fid=1')->select();
        $type_cats = $modelCat->where('fid='.$productRes['classify_id'])->select();
		$brand_cats = $modelCat->where('fid='.$productRes['type_id'])->select();
		$model_cats = $modelCat->where('fid='.$productRes['brand_id'])->select();
		
        // 将商品分类变量分配到模板
		$this->assign('classify_cats', $classify_cats);
		$this->assign('type_cats', $type_cats);
		$this->assign('brand_cats', $brand_cats);
        $this->assign('model_cats', $model_cats);
		
        $this->assign('pro', $productRes);
        $this->display('product_edit');
    }

    /**
     * 执行商品修改操作
     * @return void
     */
    public function doProductEdit()
    {
		$post = $_POST;

		$modelProduct = M('products');
		
		$product = array();
		$getProductId = $_POST['id'];
		
		$product['id']            = $getProductId;
		$product['classify_id']   = $post['classify_id'];
        $product['type_id']       = $post['type_id'];
        $product['brand_id']      = $post['brand_id'];
        $product['cat_id']        = $post['cat_id'];
		$product['pro_brand']     = $post['pro_brand'];
        $product['pro_name']      = $post['pro_name'];
        $product['pro_subname']   = $post['pro_subname'];
        $product['pro_price']     = $post['pro_price'];
		$product['pro_sn']        = $post['pro_sn'];
		$product['stock_num']     = $post['stock_num'];
        $product['keywords']      = $post['keywords'];
        $product['description']   = $post['description'];
        $product['is_card']       = empty($post['is_card'])?0:1;
        $product['is_shop']       = empty($post['is_shop'])?0:1;
        $product['is_new']        = empty($post['is_new'])?0:1;
        $product['is_hot']        = empty($post['is_hot'])?0:1;
		$product['is_shipping']   = empty($post['is_shipping'])?0:1;
		$product['is_quality']    = empty($post['is_quality'])?0:1;
		$product['is_return']     = empty($post['is_return'])?0:1;
		$product['is_buy']        = empty($post['is_buy'])?0:1;
		$product['sale_money']    = $post['sale_money'];
		$product['warranty']      = $post['warranty'];
		$product['adv_position']  = $post['adv_position'];
		$product['virtual_money'] = $post['virtual_money'];
		$product['is_on_sale']    = $post['is_on_sale'];
        $product['feacher']       = $post['feacher'];
        $product['update_time']   = time();

		if (empty($product['pro_name'])) {
            $this->error('商品更改无效');
            exit;
        }
		if($product['adv_position'] > 0){
			$modelProduct->where("classify_id=".$post['classify_id']." AND adv_position=".$product['adv_position'])->save(array('adv_position'=>0));
		}
		
		//判断是否有缩略图上传,有上传则删除旧图片
		$list_image = $post['list_image'];
		$org_list_image = $post['org_list_image'];
		if(!empty($list_image)){
			unlink("./Public/Product/thumb/".$org_list_image);        //删除旧图片
			$product['list_image'] = $list_image;
		}else{
			$product['list_image'] = $org_list_image;
		}
		
        // 修改通用信息
		$updateProduct = $modelProduct->data($product)->save();
		
		// 修改关联商品信息
		if(!empty($post['linkproducts'])){
			$modelProductLink = M('product_link');
			$modelProductLink->where("pro_id='{$getProductId}'")->delete();
			$links = explode(',',$post['linkproducts']);
			foreach($links as $v){
				$link = array();
				$link['pro_id'] = $getProductId;
				$link['link_pro_id'] = $v;
				$updateLink = $modelProductLink->data($link)->add();
			}
		}
		
        if ($updateProduct || $updateLink) {
			// 写入日志
			$cLog = new \Caobao\Controller\LogController();
			$cLog->setLog('商品（ID:' . $getProductId . '）修改成功');
			$this->success('商品修改成功',U('Product/index',array('classify_id'=>$post['classify_id'], 'type_id'=>$post['type_id'], 'brand_id'=>$post['brand_id'], 'cat_id'=>$post['cat_id'])));
			exit;
		} else {
			$this->error('商品添加失败');
		}

    }
	
	/**
     * 执行商品修改操作
     * @return void
     */
    public function postProductEdit()
    {
        $getId = $_POST['id'];
		$getType = $_POST['type'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		
		switch ($getType)
		{
			case 'name':
				$data['pro_name'] = $getVal;
				break;
			case 'virtual_money':
				$data['virtual_money'] = $getVal;
				break;
			case 'is_on_sale':
				$data['is_on_sale'] = $getVal;
				break;
			case 'is_shop':
				$data['is_shop'] = $getVal;
				break;
			case 'is_new':
				$data['is_new'] = $getVal;
				break;
			case 'is_hot':
				$data['is_hot'] = $getVal;
				break;
			case 'is_shipping':
				$data['is_shipping'] = $getVal;
				break;
			case 'is_quality':
				$data['is_quality'] = $getVal;
				break;
			case 'is_return':
				$data['is_return'] = $getVal;
				break;
		}

        // 修改商品信息
        $modelProduct = M('products');
        if ($modelProduct->create($data)) 
		{
            $modelProduct->save();
			echo "1";
			exit;
        }
		else
		{
			echo "0";
			exit;
		}

    }

	/**
     * 删除商品主图操作
     * @return void
     */
    public function deleteAlbum()
    {
        $getId = I('get.id');

		$modelAlbum = M('product_album');

		$album = $modelAlbum->where("id = ".$getId)->find(); //获取Album编号的数据
		$affectedRows = $modelAlbum -> where("id = ".$getId) -> delete();     //删除Album表数据
		unlink("./Public/Product/images/goods_img/".$album['images']);        //删除相册图片
		unlink("./Public/Product/images/source_img/".$album['images']);       //删除相册图片
		unlink("./Public/Product/images/thumb_img/".$album['images']);        //删除相册图片
		
        if ($affectedRows) {
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('商品主图图片（ID:' . $getId . '）,删除成功');
            $this->success('主图删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 商品库存管理
     * @date 2015-01-15
     * @return void
     */
    public function stock()
    {
		$keyword = I('get.keyword');
		
        $model = M();
		
		$where = "r.id<>0 AND p.is_on_sale=1";
		if(!empty($keyword)){
			$where .= " AND p.pro_name like '%".$keyword."%'";
		}

        // 查询总记录数
        $getPageCounts = $model->table('__PRODUCT_RELATION__ AS r')
					->join('__PRODUCTS__ AS p ON p.id=r.pro_id')
					->where($where)
					->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);
		$pageShow = $page->show();
		$relations = $model->table('__PRODUCT_RELATION__ AS r')
					->field('r.*,p.pro_name')
					->join('__PRODUCTS__ AS p ON p.id=r.pro_id')
					->where($where)
					->order('r.pro_stock ASC')
					->limit($page->firstRow, $page->listRows)
					->select();
        foreach($relations as &$vo){
			$attrs = explode("|",$vo['pro_attr']);
			$data = array();
			foreach($attrs as $attr){
				$attribute = $model->table('__PRODUCT_ATTR__ AS pa')
					->field('pa.attr_value')
					->join('__ATTRIBUTE__ AS a ON a.id=pa.attr_id')
					->where("pa.id=$attr")
					->find();
				$data[] = $attribute['attr_value'];
			}
			$vo['pro_attr_name'] = implode(" ", $data);
		}
		
        $this->assign('page', $pageShow);
        $this->assign('relations', $relations);
		$this->assign('keyword', $keyword);
        $this->display('product_stock_list');
    }

    /**
     * 导入商品库存信息
     * @date 2015-01-15
     * @return void
     */
    public function import()
    {
        $this->display('product_stock_import');
    }

    /**
     * 执行导入商品库存操作
     * @date 2015-01-22
     * @return void 
     */

    public function doImport(){
    	if (!IS_POST) {
			exit('页面错误~');
		}
		
		$modelProducts = M('products');
		$modelRelation = M('product_relation');
		
		// 判断有没有文件上传
        if (!empty($_FILES['excel']['name'])){
            $upload = new \Think\Upload();                          // 实例化上传类
            // 设置附件上传类型    
            $upload->exts      = array('xls', 'xlsx');
            $upload->subName   = '';
            $upload->rootPath  = './Public/';
            $upload->savePath  = 'Temp/';                           // 设置上传目录    
            $upload->saveName  = date('YmdHis') . mt_rand(100, 999);

            // 上传文件     
            $info  = $upload->uploadOne($_FILES['excel']);
			vendor("PHPExcel.PHPExcel");
			$fileName = './Public/Temp/'.$info['savename'];
			$objReader = \PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load($fileName,$encode='utf-8');
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();       // 取得总行数
			$highestColumn = $sheet->getHighestColumn(); // 取得总列数
			$j = $k =0;
			for($i=3;$i<=$highestRow;$i++){
				$id    = $objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue();
				$pid   = $objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue();
				$stock = $objPHPExcel->getActiveSheet()->getCell("E".$i)->getValue();
				if(!empty($id) && !empty($pid)){
					$relation = $modelRelation->where("id=$id")->find();
					$product  = $modelProducts->where("id=$pid")->find();
					if(!empty($relation) && !empty($product)){
						$modelRelation->data(array('id'=>$id, 'pro_stock'=>$stock))->save();
						$modelProducts->data(array('id'=>$pid, 'stock_num'=>$product['stock_num']+$stock))->save();
						$j++;
					}else{
						$k++;
					}
				}
			}
            unlink($fileName);
        }
		$this->success('添加成功 '.$j.' 个,失败 '.$k.' 个', 'stock');
    }

    /**
     * 执行导出商品库存操作
     * @date 2015-01-15
     * @return void
     */
    public function doExport()
    {
		$model = M();

		$relations = $model->table('__PRODUCT_RELATION__ AS r')
					->field('r.*,p.pro_name')
					->join('__PRODUCTS__ AS p ON p.id=r.pro_id')
					->where('r.id<>0 AND p.is_on_sale=1')
					->order('r.pro_id ASC')
					->select();
		
		$i = 0;
		foreach($relations as $vo){
			$attrs = explode("|",$vo['pro_attr']);
			$data = array();
			foreach($attrs as $attr){
				$attribute = $model->table('__PRODUCT_ATTR__ AS pa')
					->field('pa.attr_value')
					->join('__ATTRIBUTE__ AS a ON a.id=pa.attr_id')
					->where("pa.id=$attr")
					->find();
				$data[] = $attribute['attr_value'];
			}
			
			$xlsData[$i]['id']            = $vo['id'];
			$xlsData[$i]['pro_id']        = $vo['pro_id'];
			$xlsData[$i]['pro_name']      = $vo['pro_name'];
			$xlsData[$i]['pro_attr_name'] = implode(" ", $data);
			$xlsData[$i]['pro_stock']     = $vo['pro_stock'];
			$i++;
		}
		
		$xlsName  = "products";
        $xlsCell  = array(
			array('id','序号'),
			array('pro_id','商品编号'),
			array('pro_name','商品名称'),
			array('pro_attr_name','商品属性'),
			array('pro_stock','库存')
        );
		
        CBWExportExcel($xlsName,$xlsCell,$xlsData);
    }

    /**
     * 修改商品库存信息
     * @date 2015-01-15
     * @return void
     */
    public function productStockEdit()
    {
		$id = I('get.id');
		
		$model = M();
		$modelStock    = M('product_stock');
		$modelSupplier = M('product_supplier');
		
		$relation = $model->table('__PRODUCT_RELATION__ AS r')
					->field('r.*,p.pro_name')
					->join('__PRODUCTS__ AS p ON p.id=r.pro_id')
					->where('r.id='.$id)
					->find();
		$attrs = explode("|",$relation['pro_attr']);
		$data = array();
		foreach($attrs as $attr){
			$attribute = $model->table('__PRODUCT_ATTR__ AS pa')
				->field('pa.attr_value')
				->join('__ATTRIBUTE__ AS a ON a.id=pa.attr_id')
				->where("pa.id=$attr")
				->find();
			$data[] = $attribute['attr_value'];
		}
		$relation['pro_attr_name'] = implode(" ", $data);
		
		$suppliers = $modelSupplier->select();
		
		// 查询总记录数
        $getPageCounts = $modelStock->where("product_id=$id")->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize);
		$pageShow = $page->show();
		$stock = $modelStock->where("product_id=$id")->order('id DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($stock as &$vo){
			if($vo['supplier_id'] > 0){
				$supplier = $modelSupplier->where('id='.$vo['supplier_id'])->find();
				$vo['supplier_name'] = $supplier['name'];
			}else{
				$vo['supplier_name'] = '无';
			}
		}
		
		$this->assign('id', $id);
		$this->assign('relation', $relation);
		$this->assign('page', $pageShow);
		$this->assign('stock', $stock);
		$this->assign('suppliers', $suppliers);
        $this->display('product_stock_edit');
    }

    /**
     * 执行添加商品库存变更记录操作
     * @return void
     */
    public function doProductStockEdit()
    {
		$stock = array();
		$stock['product_id']  = I('post.product_id');
        $stock['supplier_id'] = I('post.supplier_id');
        $stock['type_id']     = I('post.type_id');
        $stock['number']      = I('post.number');
		$stock['one_price']   = I('post.one_price');
        $stock['two_price']   = I('post.two_price');
        $stock['three_price'] = I('post.three_price');
        $stock['four_price']  = I('post.four_price');
		$stock['remarks']     = I('post.remarks');
        $stock['create_time'] = time();
		
		$modelStock = M('product_stock');
        if($modelStock->create($stock)) {
            $rt = $modelStock->add();
            if($rt) {
				// 更新商品库存
				$modelRelation = M('product_relation');
				$modelProducts = M('products');
				$relation = $modelRelation->where("id=".$stock['product_id'])->find();
				$product  = $modelProducts->where("id=".$relation['pro_id'])->find();
				$data = array();
				$data['id'] = $stock['product_id'];
				if($stock['type_id'] == 1){
					$data['pro_stock'] = $relation['pro_stock'] + $stock['number'];
					$modelProducts->data(array('id'=>$product['id'], 'stock_num'=>$product['stock_num']+$stock['number']))->save();
				}else{
					$data['pro_stock'] = $relation['pro_stock'] - $stock['number'];
					$modelProducts->data(array('id'=>$product['id'], 'stock_num'=>$product['stock_num']-$stock['number']))->save();
				}
				if($stock['one_price'] > 0){
					$data['one_price'] = $stock['one_price'];
				}
				if($stock['two_price'] > 0){
					$data['two_price'] = $stock['two_price'];
				}
				if($stock['three_price'] > 0){
					$data['three_price'] = $stock['three_price'];
				}
				if($stock['four_price'] > 0){
					$data['four_price'] = $stock['four_price'];
				}
				$modelRelation->data($data)->save();
				
                // 写入日志
				$cLog = new \Caobao\Controller\LogController();
				$cLog->setLog('商品（ID:' . $getProductId . '）库存修改成功');
				$this->success('商品库存修改成功',U('Product/stock'));
                exit;
            }else{
                $this->error('商品库存修改失败');
            }
        }else{
            $this->error('上传出错。');
        }
    }
	
	/**
     * 商品供应商管理
     * @return void
     */
    public function supplier()
    {
		$keyword = I('get.keyword');
		
        $model = M();

		$where = "s.id<>0";
		if(!empty($keyword)){
			$where .= " AND s.name like '%".$keyword."%'";
		}
		
        // 查询总记录数
        $getPageCounts = $model->table('__PRODUCT_SUPPLIER__ AS s')
							->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->where($where)
							->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);
		$pageShow = $page->show();
		$suppliers = $model->table('__PRODUCT_SUPPLIER__ AS s')
							->field('s.*,a.addr,c.addr as city_addr,p.addr as pro_addr')
							->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->where($where)
							->order('id ASC')
							->limit($page->firstRow, $page->listRows)
							->select();
		
        $this->assign('page', $pageShow);
        $this->assign('suppliers', $suppliers);
		$this->assign('keyword', $keyword);
        $this->display('product_supplier_list');
    }
	
	/**
     * 添加商品供应商页面
     * @return void
     */
    public function supplierAdd()
    {
		$city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
        $c = \Think\Area::city($city);
        $this->assign('city', $c);
        $this->display('product_supplier_add');
    }
	
	/**
     * 添加商品供应商操作
     * @return void
     */
    public function doAddSupplier()
    {
		$data = array();
		$data['name']    = I('post.name');
        $data['tel']     = I('post.tel');
        $data['email']   = I('post.email');
        $data['area_id'] = I('post.area');
		$data['address'] = I('post.address');
		
		$modelSupplier = M('product_supplier');
        if($modelSupplier->create($data)) {
            $rt = $modelSupplier->add();
            if($rt) {
				$this->success('商品供应商添加成功',U('Product/supplier'));
                exit;
            }else{
                $this->error('商品供应商添加失败');
            }
        }else{
            $this->error('上传出错。');
        }
    }
	
	/**
     * 编辑商品供应商页面
     * @return void
     */
    public function supplierEdit()
    {
		$id = I('get.id');
		
        $modelSupplier = M('product_supplier');
		$modelArea     = M('addr_area');
		
		$supplier = $modelSupplier->where("id=$id")->find();
		$area     = $modelArea->where('id='.$supplier['area_id'])->find();
		$city = array('province'=>$area['one_id'], 'city'=>$area['two_id'], 'area'=>$area['id']);
        $c = \Think\Area::city($city);
       
	    $this->assign('city', $c);
        $this->assign('supplier', $supplier);
        $this->display('product_supplier_edit');
    }
	
	/**
     * 编辑商品供应商操作
     * @return void
     */
    public function doSupplierEdit()
    {
		$data = array();
		$data['id']    = I('post.id');
		$data['name']    = I('post.name');
        $data['tel']     = I('post.tel');
        $data['email']   = I('post.email');
        $data['area_id'] = I('post.area');
		$data['address'] = I('post.address');
		
		$modelSupplier = M('product_supplier');
		$updateSupplier = $modelSupplier->data($data)->save();
        if($updateSupplier) {
			$this->success('商品供应商修改成功',U('Product/supplier'));
            exit;
        }else{
            $this->error('商品供应商修改失败');
			exit;
        }
    }
	
	/**
     * 删除商品供应商操作
     * @return void
     */
    public function deleteSupplier()
    {
		$id = I('get.id');
		
        $modelSupplier = M('product_supplier');
		
		$supplier = $modelSupplier->where("id=$id")->delete();
		if($supplier) {
			$this->success('商品供应商删除成功',U('Product/supplier'));
            exit;
        }else{
            $this->error('商品供应商删除失败');
			exit;
        }
    }

    /**
     * 回收站商品列表
     * @date 2015-01-15
     * @return void
     */
    public function productRecycle()
    {
        // 获取商品类别
		$cat_id = I('get.cat_id');
        $modelCats = M('product_cat');
		
		$productCats = $modelCats->select();
		$tree = new \Think\Tree($productCats);
		$str = "<option value=\$id \$selected>\$spacer\$cat_name</option>"; //生成的形式
		$strCats = $tree->getTree(0,$str, $cat_id);
		
        $modelProducts = M('products');
        $map['recycle'] = 1;

        // 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部' && $key == 'cat_id') {
                    continue;
                }

                if ($val == '全部' && $key == 'is_on_sale') {
                    continue;
                }

                if ($val == '' && $key == 'keyword') {
                    continue;
                }

                $map[$key] = $val;
            }
        }

        // 查询总记录数
        $getPageCounts = $modelProducts->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $productsList = $modelProducts->where($map)->order('id DESC')->limit($page->firstRow, $page->listRows)->select();

        if (!empty($map)) {
            foreach ($map as $k => $v) {
                // $page->parameter .= "$k=".urlencode($v).'&';
            }
        }

        $pageShow = $page->show();
        $this->assign('strCats', $strCats);
        $this->assign('page', $pageShow);
        $this->assign('productsList', $productsList);
        $this->display('product_recycle_list');
    }

    /**
     * 骑过Ajax获取产品类别
     * @return string 返回json格式字符串
     */
    public function searchCatByAjax()
    {
        $getCatId = I('post.cat_id');
        $getCatName = I('post.pro_name');

        $where['cat_id'] = $getCatId;
        if (!empty($getCatName)) {
            $where['pro_name'] = array('LIKE', "%{$getCatName}%");
        }

        $modelProduct = M('products');
        $productRes = $modelProduct->field('id, pro_name')->where($where)->select();

        $this->ajaxReturn($productRes);
    }

    /**
     * 将商品添加到回收站
     * @date 2015-01-16
     */
    public function addProductToRecycle()
    {
        $getProductId = I('get.id');
        $modelProduct = M('products');
        $affectedRows = $modelProduct->where("id='{$getProductId}'")->data(array('recycle' => '1'))->save();

        if ($affectedRows) {
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('商品（ID:' . $getProductId . '）已成功加入回收站');
            $this->success('商品已加入回收站');
        } else {
            $this->error('操作失败');
        }
    }

    /**
     * 将产品从回收站移出
     * @return void
     */
    public function removeProductToRecycle()
    {
        $getProductId = I('get.id');
        $modelProduct = M('products');
        $affectedRows = $modelProduct->where("id='{$getProductId}'")->data(array('recycle' => '0'))->save();

        if ($affectedRows) {
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('商品（ID:' . $getProductId . '）已从回收站移出');
            $this->success('商品移出回收站');
        } else {
            $this->error('操作失败');
        }
    }
	
	/**
     * 删除回收站单个商品
     * @return void
     */
    public function deleteProduct()
    {
        $getProductId = I('get.id');

        $modelProducts = M('products');
		$productInfo = $modelProducts -> where("id = $getProductId") -> find();

		// 删除商品数据
        $affectedRows = $modelProducts->where(array('id' => $getProductId))->delete();       //删除商品表信息
		unlink("./Public/Product/thumb/".$productInfo['list_image']);                 //删除商品缩略图
		
        if ($affectedRows) {
			$modelAlbum    = M('product_album');
			$modelAttr     = M('product_attr');
			$modelRelation = M('product_relation');
			$modelParam    = M('product_param');
			
			// 删除商品关联表
			$attrList = $modelAttr->where("pro_id = $getProductId")->select(); //获取商品编号关联的Attr表数据
			if(is_array($attrList) && !empty($attrList))
			{
				foreach($attrList as $key=>$item)
				{
					$albumList = $modelAlbum->where("attr_id = ".$item['id'])->select(); //根据Attr获取商品关联的Album表数据
					if(is_array($albumList) && !empty($albumList))
					{
						foreach($albumList as $k=>$val)
						{
							$modelAlbum -> where("id = ".$val['id']) -> delete();     //删除Album表数据
							unlink("./Public/Product/images/goods_img/".$val['images']);        //删除相册图片
							unlink("./Public/Product/images/source_img/".$val['images']);       //删除相册图片
							unlink("./Public/Product/images/thumb_img/".$val['images']);        //删除相册图片
						}
						$modelAttr -> where("id = ".$item['id']) -> delete();     //删除Attr表数据
					}
				}
			}
			
			//删除relation表数据
			$relationCounts = $modelRelation->where("pro_id = $getProductId")->count();
			if($relationCounts > 0){
				$modelRelation -> where("pro_id = $getProductId") -> delete();     
			}
			
			//删除Param表数据据
			$paramCounts = $modelParam->where("pro_id = $getProductId")->count();
			if($paramCounts > 0){
				$modelParam -> where("pro_id = $getProductId") -> delete();     
			}
			
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('商品（ID:' . $getProductId . '）删除成功');
            $this->success('商品删除成功');
        } else {
            $this->error('删除失败');
        }
    }
	
	/**
     * 批量删除回收站的商品
     * @return void
     */
    public function batchRecycle ()
    {
        if ( ! isset($_POST['batch_option']))
        {
            $this->error('非法操作');
            exit;
        }

        if ( $_POST['batch_option'] == -1)
        {
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
        $where = "id in (" . implode(', ', $getIds) . ') ';
        $modelProducts = M('products');

        switch ($_POST['batch_option'])
        {
            case 1:
                $data['recycle'] = 0;
                $affectedRows = $modelP->where($where)->data($data)->save();
                $jumpURL = 'index';
                break;
            case 2:
				$modelAlbum = M('product_album');
				$modelAttr = M('product_attr');
				$modelRelation = M('product_relation');
				$modelParam = M('product_param');
 
				foreach($getIds as $id)
				{
					$attrList = $modelAttr->where("pro_id = $id")->select(); //获取商品编号关联的Attr表数据
					if(is_array($attrList) && !empty($attrList))
					{
						foreach($attrList as $key=>$item)
						{
							$albumList = $modelAlbum->where("attr_id = ".$item['id'])->select(); //根据Attr获取商品关联的Album表数据
							if(is_array($albumList) && !empty($albumList))
							{
								foreach($albumList as $k=>$val)
								{
									$modelAlbum -> where("id = ".$val['id']) -> delete();     //删除Album表数据
									unlink("./Public/Product/images/goods_img/".$val['images']);        //删除相册图片
									unlink("./Public/Product/images/source_img/".$val['images']);       //删除相册图片
									unlink("./Public/Product/images/thumb_img/".$val['images']);        //删除相册图片
								}
								$modelAttr -> where("id = ".$item['id']) -> delete();     //删除Attr表数据
							}
						}
					}
					
					//删除relation表数据
					$relationCounts = $modelRelation->where("pro_id = $id")->count();
					if($relationCounts > 0){
						$modelRelation -> where("pro_id = $id") -> delete();     
					}
					
					//删除Param表数据据
					$paramCounts = $modelParam->where("pro_id = $id")->count();
					if($paramCounts > 0){
						$modelParam -> where("pro_id = $id") -> delete();     
					}
					
					//删除商品缩略图
					$productInfo = $modelProducts -> where("id = $id") -> find();
					unlink("./Public/Product/thumb/".$productInfo['list_image']); 
					
				}
				
				$affectedRows = $modelProducts -> where($where) -> delete();       //批量删除商品
                $jumpURL='';
                break;
        }

        if ($affectedRows)
        {
            $this->success('执行成功', $jumpURL);
        }
        else
        {
            $this->error('执行失败');
        }
    }

    /**
     * 执行产品类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeProductSelect()
    {
        $catId = I('get.id');
        $modelProductCat = M('product_cat');

        $cats = $modelProductCat->where("fid = '{$catId}'")->select();
		if(!is_array($cats)){
			exit;
		}
		$retString = '';
		foreach($cats as $key=>$item){
			$retString .= '<option value="'.$item['id'].'">'.$item['cat_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 添加商品参数，获取参数添加页面
     * @return [type] [description]
     */
    public function productParamAdd()
    {
        $getProId = I('get.id');
		
        $modelParam = M('product_param');
        $paramRes = $modelParam->where("pro_id = '{$getProId}'")->select();
        $this->assign('paramRes', $paramRes);
		
		$modelProducts = M('products');
        $products = $modelProducts->where("id = '{$getProId}'")->find();
        $this->assign('products', $products);

        $this->display('product_param_add');
    }

    /**
     * 执行参数添加操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function doAddParamByAjax()
    {
        if (I('post.pro_id') == '' || I('post.param_title') == '' || I('post.param_content') == '') {
            $this->error('警告：非法操作');
            exit;
        }

        $pro_id = I('post.pro_id');
        $modelParam = M('product_param');
        if ($modelParam->create()) {
            if ($modelParam->add()) {
                $this->success('添加成功！');
            } else {
                $this->error('添加失败');
            }
        }
    }


    /**
     * 执行参数编辑操作
     * @date 2015-01-16
     * @return void
     */
    public function doEditParam()
    {
        $modelParam = M('product_param');

        if ($modelParam->create()) {
            if ($modelParam->save()) {
                $jumpURL = 'productParamAdd/id/' . I('post.pro_id');
                $this->success('修改成功', $jumpURL);
                exit;
            }
        }
        $this->error('修改失败');
    }

    /**
     * 编辑商品参数，获取参数编辑页面
     * @return void
     */
    public function editParam()
    {
        $getParamId = I('get.id');
		
        $modelParam = M('product_param');
        $paramRes = $modelParam->where("id='{$getParamId}'")->find();
        $this->assign('paramRes', $paramRes);
		
        $this->display('product_param_edit');
    }

    /**
     * 执行删除参数操作
     * @date 2015-01-16
     * @return void
     */
    public function doDeleteParam()
    {
        $getParamId = I('get.id');
        $modelParam = M('product_param');
        $affectedRows = $modelParam->where("id = {$getParamId}")->delete();

        if ($affectedRows > 0) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 价格与库存管理 列表
     * @date 2015-01-16
     * @return void
     */
    public function relationList()
    {	
        $pro_id = I('get.id');
		$modelProducts = M('products');
		$products = $modelProducts->field("classify_id,type_id,brand_id,cat_id")->where("id=".$pro_id)->find();
		$this->assign("products",$products);
		
		$modelRelation = M('product_relation');
		$modelAttr = M('product_attr');
        $relationRes = $modelRelation->where("pro_id=".$pro_id)->select();
		foreach($relationRes as $key=>$vo){
			if(!empty($vo['pro_attr'])){
				$ids = explode("|",$vo['pro_attr']);
				$data = array();
				foreach($ids as $id){
					$attr = $modelAttr->where("id=".$id)->find();
					$data[] = $attr['attr_value'];
				}
				$relationRes[$key]['pro_attr_name'] = implode("|",$data);
			}
		}
        $this->assign('relationRes', $relationRes);
		$this->assign('pro_id', $pro_id);
        $this->display('product_relation_list');
    }

    /**
     * 添加商品属性关联
     * @return [type] [description]
     */
    public function addRelation()
    {
        $id = I('get.pro_id');
		
		//组合商品属性
		$model = M();
		$modelAttribute = M('attribute');
		$modelAttr = M('product_attr');
        $groups = $model->table('__PRODUCT_ATTR__ AS pa')->field('pa.attr_id')->join('__ATTRIBUTE__ AS a ON a.id=pa.attr_id')->where("pa.pro_id=".$id)->group('pa.attr_id')->order('a.sort ASC')->select();   //按attr_id分组查询,返回商品的属性
		$attrRes = array();
		foreach($groups as $key=>$vo){
			$attribute = $modelAttribute->where('id='.$vo['attr_id'])->find();
			$attrRes[$key]['attr_name'] = $attribute['attr_name'];
			$attrs = $modelAttr->field("id,attr_value")->where("attr_id=".$vo['attr_id']." AND pro_id=".$id)->select();
			$attrRes[$key]['attr'] = $attrs;
		}
        $this->assign('attrRes', $attrRes);
		
        $this->display('product_relation_add');
    }

    /**
     * 执行商品属性关联添加操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function doRelationAdd()
    {
        if (I('post.pro_id') == '') {
            $this->error('警告：非法操作');
            exit;
        }
		$data = array();
        $data['pro_id']      = I('post.pro_id');
		$data['pro_attr']    = implode("|",I('post.attr_id'));
		$data['one_price']   = I('post.one_price');
		$data['two_price']   = I('post.two_price');
		$data['three_price'] = I('post.three_price');
		$data['four_price']  = I('post.four_price');
        $data['pro_stock']   = I('post.pro_stock');
        $modelRelation = M('product_relation');
        if ($modelRelation->create($data)) {
			$id = $modelRelation->add();
            if ($id) {
				//商品库存处理
				$modelProducts = M('products');
				$modelProducts->where('id = '.$data['pro_id'])->setInc('stock_num',$data['pro_stock']);   //加
                $this->success('添加成功！',U('Product/relationList/id/'.I('post.pro_id')));
            } else {
                $this->error('添加失败');
            }
        }
    }

    /**
     * 编辑商品属性关联
     */
    public function editRelation()
    {
        $id = I('get.id');
        $modelRelation = M('product_relation');
        $relation = $modelRelation->where("id=".$id)->find();
		$pro_attr = explode("|",$relation['pro_attr']);
		
		//组合商品属性
		$model = M();
		$modelAttribute = M('attribute');
		$modelAttr = M('product_attr');
        $groups = $model->table('__PRODUCT_ATTR__ AS pa')->field('pa.attr_id')->join('__ATTRIBUTE__ AS a ON a.id=pa.attr_id')->where("pa.pro_id=".$relation['pro_id'])->group('pa.attr_id')->order('a.sort ASC')->select();   //按attr_id分组查询,返回商品的属性
		$attrRes = array();
		foreach($groups as $key=>$vo){
			$attribute = $modelAttribute->where('id='.$vo['attr_id'])->find();
			$attrRes[$key]['attr_name'] = $attribute['attr_name'];
			$attrs = $modelAttr->field("id,attr_value")->where("attr_id=".$vo['attr_id']." AND pro_id=".$relation['pro_id'])->select();
			foreach($attrs as $k=>$v){
				if(in_array($v['id'],$pro_attr)){
					$attrs[$k]['selected'] = 1;
				}
			}
			$attrRes[$key]['attr'] = $attrs;
		}

        $this->assign('attrRes', $attrRes);
		$this->assign('relation', $relation);
        $this->display('product_relation_edit');
    }

    /**
     * 执行商品属性关联编辑操作
     */
    public function doEditRelation()
    {
		$id = I('post.id');
		$pro_id = I('post.pro_id');
		$data = array();
		$data['pro_attr']    = implode("|",I('post.attr_id'));
		$data['one_price']   = I('post.one_price');
		$data['two_price']   = I('post.two_price');
		$data['three_price'] = I('post.three_price');
		$data['four_price']  = I('post.four_price');
        $data['pro_stock']   = I('post.pro_stock');

		$modelRelation = M('product_relation');
		$relationId = $modelRelation->where("id=".$id)->data($data)->save();
        if ($relationId) {
			
			//商品库存处理,先减去旧库存值,再加上现修改的库存值
			$modelProducts = M('products');
			$modelProducts->where('id = '.$pro_id)->setDec('stock_num',I('post.old_stock'));   //减
			$modelProducts->where('id = '.$pro_id)->setInc('stock_num',I('post.pro_stock'));   //加
			
			$this->success('修改成功',U('Product/relationList/id/'.I('post.pro_id')));
			exit;
        }else{
			$this->error('修改失败');
			exit;
		}
       
    }

    /**
     * 执行删除商品属性关联操作
     * @date 2015-01-16
     * @return void
     */
    public function doDeleteRelation()
    {
        $id = I('get.id');
		
        $modelRelation = M('product_relation');
		$relation = $modelRelation->where("id=".$id)->find();
        $affectedRows = $modelRelation->where("id=".$id)->delete();
		
        if ($affectedRows > 0) {
            $this->success('删除成功',U('Product/relationList/id/'.$relation['pro_id']));
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 属性管理 列表
     * @date 2015-01-16
     * @return void
     */
    public function attrList()
    {	
        $id = I('get.id');
		$modelProducts = M('products');
		$products = $modelProducts->field("classify_id,type_id,brand_id,cat_id")->where("id=".$id)->find();
		$this->assign("products",$products);
		
		$model = M();
        $attrRes = $model->table('__PRODUCT_ATTR__ AS pa')->field('pa.*,a.attr_name')->join('__ATTRIBUTE__ AS a ON a.id=pa.attr_id')->where("pa.pro_id=".$id)->order('a.sort ASC, pa.sort ASC')->select();
        $this->assign('attrRes', $attrRes);
		$this->assign('id', $id);
        $this->display('product_attr_list');
    }

    /**
     * 添加商品属性
     */
    public function addAttr()
    {
        $id = I('get.id');
		
		$modelAttribute = M('attribute');
        $attrRes = $modelAttribute->order('sort ASC')->select();
        $this->assign('attrRes', $attrRes);
		
        $this->display('product_attr_add');
    }

    /**
     * 执行商品属性添加操作
     */
    public function doAttrAdd()
    {
		$data = array();
        $data['pro_id'] = I('post.pro_id');
		$data['attr_id'] = I('post.attr_id');
        $data['attr_value'] = I('post.attr_value');
		$data['sort'] = I('post.attr_sort');

        $modelAttr = M('product_attr');
        if ($modelAttr->create($data)) {
			$getAttrId = $modelAttr->add();
            if ($getAttrId) {
				// 处理颜色属性对应的小图
				$modelAlbum = M('product_album');
				$albumData = array();
				$advs = I('post.advs');
				foreach ($advs as $key => $val) {
					$albumData[$key]['attr_id'] = $getAttrId;
					$albumData[$key]['images'] = $val;
				}
				// 添加颜色对应的图片
				$modelAlbum->addAll($albumData);
				
                $this->success('添加成功！',U('Product/attrList/id/'.I('post.pro_id')));
            } else {
                $this->error('添加失败');
            }
        }
    }

    /**
     * 执行删除商品属性操作
     * @date 2015-01-16
     * @return void
     */
    public function doDeleteAttr()
    {
        $id = I('get.id');
		
        $modelAttr = M('product_attr');
		$attr = $modelAttr->where("id=".$id)->find();
        $affectedRows = $modelAttr->where("id=".$id)->delete();
		
        if ($affectedRows > 0) {
			//清除颜色属性的小图
			$modelAlbum = M('product_album');
			$albums = $modelAlbum->where("attr_id=".$id)->select();
			foreach($albums as $item){
				$modelAlbum->where("id=".$item['id'])->delete();
				unlink("./Public/Product/images/goods_img/".$item['images']);        //删除相册图片
				unlink("./Public/Product/images/source_img/".$item['images']);       //删除相册图片
				unlink("./Public/Product/images/thumb_img/".$item['images']);        //删除相册图片
			}
            $this->success('删除成功',U('Product/attrList/id/'.$attr['pro_id']));
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 编辑商品属性
     */
    public function editAttr()
    {
        $id = I('get.id');
        $modelAttr = M('product_attr');
        $attrRes = $modelAttr->where("id=".$id)->find();
		$this->assign('attrRes', $attrRes);
		
		$modelAttribute = M('attribute');
        $attributeRes = $modelAttribute->order('sort ASC')->select();
        $this->assign('attributeRes', $attributeRes);
		
		//颜色相关主图
		$modelAlbum = M('product_album');
		$albumRes = $modelAlbum->where("attr_id=".$id)->order("id ASC")->select();
        $this->assign('albumRes', $albumRes);
		
        $this->display('product_attr_edit');
    }

    /**
     * 执行商品属性编辑操作
     */
    public function doEditAttr()
    {
		$id = I('post.id');
		$pro_id = I('post.pro_id');
		$attr_id = I('post.attr_id');
		$attr_value = I('post.attr_value');
		$attr_sort = I('post.attr_sort');
		$ids = I('post.ids');
		$sort = I('post.sort');
		$new_img = I('post.new_img');
		$new_sort = I('post.new_sort');
		
		$modelAttr = M('product_attr');
		$attrId = $modelAttr->where("id=".$id)->save(array('attr_id'=>$attr_id,'attr_value'=>$attr_value,'sort'=>$attr_sort));
		
		$modelAlbum = M('product_album');
		foreach($ids as $v){
			$ablumEdit = $modelAlbum->where("id=".$v)->save(array('sort'=>$sort[$v]));
		}
		if(is_array($new_img) && !empty($new_img)){
			foreach($new_img as $key=>$img){
				$modelAlbum->create(array('attr_id'=>$id,'images'=>$img,'sort'=>$new_sort[$key]));
				$ablumAdd = $modelAlbum->add();
			}
		}
		if ($attrId || $ablumEdit || $ablumAdd) {
			$this->success('修改成功',U('Product/attrList/id/'.$pro_id));
			exit;
        }else{
			$this->error('修改失败');
			exit;
		}
       
    }

    /**
     * 上传图片
     * 此方法被商品添加/修改页，上传缩略图所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/Product/thumb/"; //图片保存文件夹路径
        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
		// 生成的缩略图
		$image = new \Think\Image();
		$image->open($rootPath.$imgUrl);
		$image->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save($rootPath.$imgUrl); //生产缩略图
		
        echo $imgUrl;
    }

    /**
     * 图片上传操作，此方法用于多文件上传插件，返回Ajax字符串
     * 此方法被商品颜色添加页所调用
     * @date 2015-01-15
     * @return void
     */
    public function advUpload()
    {

        $upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/Product/images/";               // 图片保存文件夹路径
        $upload->maxSize = 3145728;                           // 设置图片上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置图片上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = 'source_img/';                    // 设置图片上传目录
        $img = $upload->upload();
		$imgUrl = $img['Filedata']['savepath'].$img['Filedata']['savename'];
		$imgNew = substr($imgUrl,11);                         //除去source_img/
		
		$image = new \Think\Image();  //图片处理类
		
		// 查找配置参数
        $modelConfig = M('sys_config');
        $comfigRes = $modelConfig->where("cname LIKE 'water%'")->select();
        $arrayConfig = $this->_arr2ToArr1($comfigRes);
		if($arrayConfig['water']){
			//图片水印处理
			$image->open($rootPath.$imgUrl)->water('./Public/water/'.$arrayConfig['water_img'],$arrayConfig['water_position'])->save($rootPath.$imgUrl); 
		}
		if($imgUrl){
			//等比例缩放图片
			$image->open($rootPath.$imgUrl)->thumb(480, 480, \Think\Image::IMAGE_THUMB_SCALE)->save($rootPath.'goods_img/'.$imgNew);
			$image->open($rootPath.$imgUrl)->thumb(150, 150, \Think\Image::IMAGE_THUMB_SCALE)->save($rootPath.'thumb_img/'.$imgNew);
		}
        echo $imgNew;
    }

    /**
     * 批量执行产品列表
     * @return void 
     */
    public function batchDo ()
    {
        if ( ! isset($_POST['batch_option']))
        {
            $this->error('非法操作');
            exit;
        }

        if ( $_POST['batch_option'] == -1)
        {
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
        $where = "id in (" . implode(', ', $getIds) . ') ';
        $modelP = M('products');

        switch ($_POST['batch_option'])
        {
            case 1:
                $data['recycle'] = 1;
                break;
            case 2:
                $data['is_on_sale'] = 0;
                break;
            case 3:
                $data['is_on_sale'] = 1;
                break;
        }

        $affectedRows = $modelP->where($where)->data($data)->save();
        if ($affectedRows)
        {
            $this->success('执行成功');
        }
        else
        {
            $this->error('执行失败');
        }

    }

    /**
     * 执行城市三级联动下拉框获取操作
     * @date 2015-01-18
     * @return void 
     */
    public function changeSelect()
    {
        $id = I('post.id');
		$type = I('post.type');

        // 实例化数据模型
		$modelCity = M('addr_city');
        $modelArea = M('addr_area');
        if($type == 'province'){
			$list = $modelCity->where("one_id=".$id)->select();
		}else{
			$list = $modelArea->where("two_id=".$id)->select();
		}
		
        if(!is_array($list)){
			exit;
		}
		$str = '';
		foreach($list as $item){
			$str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
		}
        echo $str;
		exit;
    }

	
	/**
	 * 功能 获取树形结构   注意：callback和level参数禁止传值
	 * @param $model  string      表
	 * @param $id     int         当前ID
	 * @param $fields string    返回字段  前三个字段分别对应id,父id,名称  至少要包含前三个字段，返回的fullname是格式化的名称，如果表中本来有fullname字段建议做别名
	 * @param $condition  max    查询条件  
	 * @param $orderby  string    排序
	 * @param $self boolean     是否包含当前ID数据
	 * @param $onlyson  boolean    是否只返回下级
	 * @param $return   string    返回数据类型，ids表示只返回id集合  tree返回树形结构数据
	 * @return $datas array        返回的数据
	 */
	protected function treeStructure($model='cb_product_cat',$id=0,$fields='',$condition='',$orderby='',$self=true,$onlyson=false,$return='tree',$callback=false,$level=0){
				//验证参数类型
				if(!is_string($model) || !is_int($id) || !is_string($fields) || !(is_string($condition) || is_array($condition)) || !is_string($orderby) || !is_bool($self) || !is_bool($onlyson) || !in_array($return,array('ids','tree'))) return array();
		
				//验证参数值
				$fields_arr=explode(',',$fields);
		if(empty($model) || empty($fields) || count($fields_arr)<3)return array();
		
		//获取 id,父id,名称对应表中的字段
		foreach ($fields_arr as $k=>$f){
			$f=trim($f);
			$f_arr=explode('as',$f);
			$f_arr[0]=trim($f_arr[0]);
			$f_arr[1]=trim($f_arr[1]);
			$fields_arr[$k]=array('field'=>$f_arr[0],'alias'=>$f_arr[1]);
			if($k==2)break;
		}
		$field_id=$fields_arr[0]['field'];
		$field_pid=$fields_arr[1]['field'];
		$field_name=$fields_arr[2]['field'];
		
		
		$model=strtolower($model);
		
		//查询条件
		if(!empty($condition)){
			if(is_array($condition))$map=$map1=$map2=$condition;
			if(is_string($condition))$map['_string']=$map1['_string']=$map2['_string']=$condition;
		}
		
		
		$map[$field_pid]=$id;
		
		//查询
		if(empty($orderby)){
			$list=M()->table($model)->field($fields)->where($map)->select();
		}else{
			$list=M()->table($model)->field($fields)->where($map)->order($orderby)->select();
		}
		
		$datas=array();
		
		//临时字段变量
		$field_level=to_guid_string('level');
		$field_last=to_guid_string('last');
		$field_son_num=to_guid_string('son_num');
		$field_all_son_num=to_guid_string('all_son_num');
		
		if(!$callback && $self && $id!=0){
			$map1[$field_id]=$id;
			$info=M()->table($model)->field($fields)->where($map1)->find();
			$info[$field_level]=$level;
			$info[$field_last]=1;
			$info[$field_son_num]=count($list);
			$datas[]=$info;
			$level++;
		}
		
		if($onlyson){
			foreach ($list as $k=>$v){
				$v[$field_level]=$level;
				$v[$field_last]=(count($list)-1)==$k?1:0;
				$v[$field_son_num]=0;
				if($level==0)$info[$field_all_son_num]=0;
				$datas[]=$v;
			}
		}else{
			if(count($list)>0){
				foreach ($list as $k=>$v){
					$v[$field_level]=$level;
					$v[$field_last]=(count($list)-1)==$k?1:0;
					$map2[$field_pid]=$v[$field_id];
					$v[$field_son_num]=M()->table($model)->where($map2)->count();//获取下级数量
					
					$next_id=intval($v[$field_id]);
					$next_level=$level+1;
					$datasx=$this->treeStructure($model,$next_id,$fields,$condition,$orderby,true,false,$return,true,$next_level);
					if($level==0){
						$v[$field_all_son_num]=count($datasx);//获取所有子级数量
					}
					$datas[]=$v;
					if(!empty($datasx)){
						foreach ($datasx as $v1){
							$datas[]=$v1;
						}
					}
				}
			}
		}
		
		if(!$callback && $self && $id!=0){
			$datas[0][$field_all_son_num]=count($datas)-1;
		}
		if(!$callback){
			$ids=array();
			$all_son_num=0;
			foreach ($datas as $k=>$v){
				$ids[]=$v[$field_id];
				
				if($v[$field_level]==0){
					$all_son_num=$v[$field_all_son_num];
					$i=0;
					$v['fullname']=$v[$field_name];
				}else{
					$i++;
					$prev_num=$v[$field_level]-1;
					if($prev_num>0){
						$prev_icon_t=$i==$all_son_num?'└ ':'│ ';
						$prev_icon=str_repeat('    '.$prev_icon_t,$prev_num);
					}else{
						$prev_icon='';
					}
					$icon=$v[$field_last] && $v[$field_son_num]==0?'    └ ':'    ├ ';
					$v['fullname']=$prev_icon.$icon.$v[$field_name];
				}
				unset($v[$field_level],$v[$field_last],$v[$field_son_num]);
				if(isset($v[$field_all_son_num]))unset($v[$field_all_son_num]);
				$datas[$k]=$v;
			}
			
			if($return=='ids'){
				return $ids;
			}else{
				return $datas;
			}
		}else{
			return $datas;
		}
		
	}
}
