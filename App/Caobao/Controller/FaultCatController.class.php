<?php
namespace Caobao\Controller;
use       Think\Controller;

class FaultCatController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * faultList 故障列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$cat_id = I('get.cat_id');
		
		//类型列表
		$modelCats = M('product_cat');
		$repairCats = $modelCats->where(array('fid'=>'3'))->select();
		$this->assign('repairCats', $repairCats);
		
		$modelCat = M("repair_fault_cat");
		// 分页处理
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部关联类型' && $key == 'cat_id') {
                    continue;
                }
				
				if ($val == '' && $key == 'name') {
                    continue;
                }
				
                $map[$key] = $val;
            }
        }
		$count = $modelCat->where($map)->count();
		$page = new \Think\Page($count,20,$map); 
		$list = $modelCat->where($map)->limit($page->firstRow.','.$page->listRows)->order('sort DESC,id ASC')->select();
		foreach($list as $key=>$vo){
			foreach($repairCats as $v){
				if($vo['cat_id'] == $v['id']){
					$list[$key]['cat_name'] = $v['cat_name'];
				}
			}
		}
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		
		$this->assign('cat_id', $cat_id);
		$this->display('cat_list');
    }

    /**
     * catAdd 添加故障分类
     * @date 2015-01-22
     * @return void 
     */

    public function catAdd(){
				
		//类型列表
		$modelCats = M('product_cat');
		$repairCats = $modelCats->where(array('fid'=>'3'))->select();
		$this->assign('repairCats', $repairCats);
		
    	$this->display('cat_add');
    }

    /**
     * doCatAdd 执行添加故障分类
     * @date 2015-01-22
     * @return void 
     */

    public function doCatAdd()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		$data['cat_id'] = I('post.cat_id');
        $data['name'] = I('post.name');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');
       
        $modelCat = M("repair_fault_cat");
        if($modelCat->create($data)){
            $res=$modelCat->add();
            if($res){
                $this->success('添加成功.' . $lastID, 'index/cat_id/'.$data['cat_id']);
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * catEdit 修改故障分类
     * @date 2015-01-22
     * @return void 
     */
    public function catEdit(){
		
        $id = I('get.id');
		
        $modelCat = M("repair_fault_cat");
        $cat = $modelCat->where("id =".$id)->find();  
		$this->assign("cat",$cat);
		
		//类型列表
		$modelCats = M('product_cat');
		$repairCats = $modelCats->where(array('fid'=>'3'))->select();
		$this->assign('repairCats', $repairCats);
		
        $this->display('cat_edit');
    }

    
     /**
     * doCatEdit 执行修改故障分类
     * @date 2015-01-22
     * @return void 
     */
    public function doCatEdit()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$data['id'] = I('post.id');
		$data['cat_id'] = I('post.cat_id');
        $data['name'] = I('post.name');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');

        $modelCat = M("repair_fault_cat");
		if($modelCat->save($data)){
			$this->success('修改成功.'.$lastID, 'index/cat_id/'.$data['cat_id']);
		}else{
			$this->error('修改失败！');
		}
    }
	
    /**
     * delCat 删除故障分类
     * @date 2015-01-22
     * @return void 
     */
    public function delCat(){
        $id = $_GET['id'];
        $modelCat = M("repair_fault_cat");
        $ls = $modelCat->where("id =".$id)->delete();

        if($ls){
            $this->success("删除成功",U("FaultCat/index"));
        }else{
            $this->error("删除失败");
        }
    }

    /**
     * 执行快速修改操作
     * @return void
     */
    public function postCatEdit()
    {
        $getId = $_POST['id'];
		$getType = $_POST['type'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		
		switch ($getType){
			case 'sort':
			  $data['sort'] = $getVal;
			  break;
			case 'is_show':
			  $data['is_show'] = $getVal;
			  break;
		}

        // 修改信息
        $modelCat = M('repair_fault_cat');
        if ($modelCat->create($data)) {
            $modelCat->save();
			echo "1";
			exit;
        }else{
			echo "0";
			exit;
		}

    }



}