<?php
namespace Caobao\Controller;
use       Think\Controller;
/**
 * 	后台管理员模块控制器
 *  
 * 	作者： 	HJP
 * 	时间：	2015-01-04
 */
class AdministratorController extends MyController {

	/**
     * 构造方法
     * 初始化 管理员设置 类下的一些数据
     *
     * @author  HJP
     * @date    2015-01-11
     */
	public function __construct() {
		parent::__construct();
	}

	/**
     * 获取管理员列表
     *
     * @author HJP
     * @date    2015-01-12
     * @return  void
     */
	public function index() {
	
        $modelUser   = D('UserRelation');
		
        $countItems = $modelUser->relation(true)->field('password',true)->count();

        $pageSize = 20;
        $page = new \Think\Page($countItems, $pageSize);
        // 设置分页
        $pageShow = $page->show();
        
        // 遍历所有管理员用户
		$users = $modelUser->relation(true)->field('password',true)
						   ->order('id ASC')
                           ->limit($page->firstRow.','.$page->listRows)->select();
		
        $this->assign('pageShow', $pageShow);
        $this->assign('users', $users);
        $this->display('admin_user_list');
	}

    /**
     * 获取用户修改页面
     * @return void
     */
    public function userEdit() {
        $userId = I('get.id');

        $modelUser = D('UserRelation');
        $userInfo = $modelUser->relation(true)->field('password',true)->find($userId);

        // 获取用户组列表
        $groups = $this->_getGroupList();
        $this->assign('groups', $groups);
        $this->assign('userInfo', $userInfo);
        $this->assign('group_id', $userInfo['gro'][0]['id']);
        $this->display('admin_user_edit');
    }

    /**
     * 修改用户信息
     * @return void
     */
    public function doUserEdit (){
		if (!IS_POST) {
			exit('页面错误~');
		}
        $orgPassword    = I('post.org_pass');
        $newPassword    = I('post.password');
        $newRePassword  = I('post.repassword');

        if(!empty($newPassword)){
            $encodePassword = sha1($newPassword);

            if(strcmp($newPassword, $newRePassword) != 0){
                $this->error('错误：密码不一致！');
                exit;
            }

            if(strcmp($orgPassword, $encodePassword) == 0){
                $this->error('提示：不能与原密码相同');
                exit;
            }

            $data['password'] = $encodePassword;
        }
		
		$groupId = I('post.group_id');
		if(!$groupId)$this->error('请选择用户组！');

        $data['id'] = I('post.user_id');
        $data['username'] = I('post.username');
        $data['status'] = I('post.status');
		
		//判断是否有缩略图上传,有上传则删除旧图片
		$thumbnail = I('post.thumbnail');
		$org_thumbnail = I('post.org_thumbnail');
		if(!empty($thumbnail)){
			unlink("./Public/User/thumb/".$org_thumbnail);        //删除旧头像
			$info['thumbnail'] = $thumbnail;
		}else{
			$info['thumbnail'] = $org_thumbnail;
		}
		$info['true_name'] = I('post.true_name');
		$info['email'] = I('post.email');
		$info['phone'] = I('post.phone');
		$info['qq'] = I('post.qq');
		
        $modelUser = M('admin_users');
		$modelInfo = M('admin_info');

		$edituser = $modelUser->save($data);
		$editinfo = $modelInfo->where("uid=".$data['id'])->save($info);
		if(false !== $edituser || 0 !== $edituser || false !== $editinfo || 0 !== $editinfo){
			$modelGroupaccess = M();
			$res = $modelGroupaccess->execute('UPDATE '.C('DB_PREFIX').'auth_group_access SET group_id='.$groupId.' WHERE uid='.$data['id']);//更新group_access表数据  
			
			$this->success('修改成功.' . $lastID, 'index');
		}else{
			$this->error('修改失败！');
		}
   
    }

    /**
     * 私有方法，多个页面都需要得到用户组列表
     * @return array 所有用户组列表
     */
    private function _getGroupList()
    {
        $modelGroup = M('auth_group');
        return $modelGroup->field('id, title')->select();
    }

    /**
     * 添加管理员，获取添加管理员界面 
     * 
     * @return void
     */
    public function userAdd() {
		$groups = $this->_getGroupList();
		$this->assign('groups', $groups);
        $this->display('admin_user_add');
    }

    /**
     * 修改用账号状态
     * 1 表示启用， 0表示禁用
     * 
     * @return void
     */
    public function toggleStatus (){
        $userID = I('get.id');
        $prevStatus = I('get.prev');
        $prevUserId = I('session.id');

        // 验证权限
        if ($userID == $prevUserId){
            $this->error('自己不能修改自己');
            exit;
        }

        $modelUsers = M('admin_users');
        $res = $modelUsers->where("id=" . $userID)->save(array('status' => ! $prevStatus));

        if ($res){
            // 写入日志
            $log = new \Caobao\Controller\LogController();
            $statusStr = empty($prevStatus) ? '启用' : '禁用';
            $log->setLog('用户（UID：' . $userID . '）账户状态被' . $statusStr);
            $this->success('修改成功.');
        }else{
            $this->error('修改失败.');
        }
    }

    /**
     * 批量删除用户
     * 
     * @return void 
     */
    public function batchDelete(){
        $userIdsArr = I('post.user_ids');
        $userIdsStr = implode(', ', $userIdsArr);

        $modelUser = M('admin_users');
        $modelInfo = M('admin_info');

        $res1 = $modelUser->delete($userIdsStr);
        if ($res1){
			foreach($userIdsArr as $id){
				$info = $modelInfo->where("uid='{$id}'")->select();
				if(!empty($info['thumbnail'])){
					unlink("./Public/User/thumb/".$info['thumbnail']);        //删除头像
				}
			}
			$modelInfo->where("uid in (" . $userIdsStr . ")")->delete();
			
            // 记入日志
            $objLog = new \Caobao\Controller\LogController();
            $objLog->setLog('管理员用户（UID：' . $userIdsStr . '）帐户删除成功');

            $this->success('删除成功.');
        }else{
            $this->error('删除失败.');
        }
    }

    /**
     * 添加管理员
     * 用户加密方式为 sha1() 40位加密算法，将插入ID插入到admin_info表
     *
     * @author HJP
     * @date    2015-01-12
     */
    public function doUserAdd(){
		if (!IS_POST) {
			exit('页面错误~');
		}
        // 获取客户端提交的数据，整合保存入数据库
        $data['username'] = I('post.username');
        $data['password'] = I('post.password', '', sha1);
        $data['create_time'] = time();
        $data['last_login_time'] = time();
        $data['last_login_ip'] = ip2long(I('server.REMOTE_ADDR'));
		
		$groupId = I('post.group_id');
		if(!$groupId)$this->error('请选择用户组！');
		
        $modelUser = M('admin_users');

        if ($modelUser->create($data)){
            // 执行插入操作
            $lastID = $modelUser->add();
		
            // 执行成功需要将插入的ID插入用户详细表
            if ($lastID > 0){
                $modelUserInfo = M('admin_info');
				$info['uid'] = $lastID;
				//判断是否有缩略图上传
				$thumbnail = I('post.thumbnail');
				if(!empty($thumbnail)){
					$info['thumbnail'] = $thumbnail;
				}
				$info['true_name'] = I('post.true_name');
				$info['email'] = I('post.email');
				$info['phone'] = I('post.phone');
				$info['qq'] = I('post.qq');
                $resInfo = $modelUserInfo->add($info);
				
                $modelGroupaccess = M('auth_group_access');
                $resGroup = $modelGroupaccess->add(array('uid'=>$lastID, 'group_id'=>$groupId));
				
                if ($resInfo && $resGroup){
                    // 实例化日志类，添加日志
                    $setLog = new \Caobao\Controller\LogController();
                    $setLog->setLog('用户（UID: ' . $lastID . '）添加成功！');

                    $this->success('用户添加成功.' . $lastID, 'index');
                    exit;
                }
            }
        }
        // 执行失败跳回添加页
        $this->error('用户添加失败');
    }

    /**
     * 单个删除用户
     * 
     * @param  通过GET传入的ID
     * @return void
     */
    public function userDelete($id) {
        $userId = I('get.id', 0);

        $modelUsers  = M('admin_users');
        $modelInfo   = M('admin_info');

        // 判断自己不能删除自己
        $myId = I('session.id');
        if ($myId == $userId){
            $this->error('自己不能删除自己');
            exit;
        }

        $res1 = $modelUsers->delete($userId);
        if ($res1){
			$info = $modelInfo->where("uid='{$userId}'")->select();
			if(!empty($info['thumbnail'])){
				unlink("./Public/User/thumb/".$info['thumbnail']);        //删除头像
			}
			$modelInfo->where("uid='{$userId}'")->delete();
			
            $this->success('删除成功！');
        }else{
            $this->error('删除失败！');
        }
    }

    /**
     * 添加管理员Ajax验证
     * 
     * @return void
     */
    public function checkUserExists () {

        $modelUser = M('admin_users');
        $res = $modelUser->field('id')->where("username = '{$_GET['username']}'")->select();

        if ( ! $res){
            echo 1;
        }else{
            echo 0;
        }
    }

    /**
     * 上传图片
     * 此方法被管理员添加/修改页，上传头像所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload(){
        $upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/User/thumb/"; //图片保存文件夹路径
        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
		//裁剪图像
		$image = new \Think\Image();
		$image->open($rootPath.$imgUrl);
		$image->thumb('283', '283')->save($rootPath.$imgUrl); //生产缩略图
		
        echo $imgUrl;
    }


}