<?php
namespace Caobao\Controller;
use       Think\Controller;

/**
 * 银行信息
 * @author HJP
 * @version 2015-01-19
 */
class BankController extends  MyController{

    /**
     * 构造方法，用于权限管理操作
     * @date 2015-01-19
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * 银行列表
     * @return void 
     */
    public function index(){
		
        $modelBank = M('bank');
		$map = array();
        if ( ! empty($_GET))
        {
            foreach ($_GET as $key=>$val)
            {
                if ( ! empty($val))
                {
                    $map[$key] = array('LIKE', '%' . $val . '%');
                }
            }
        }
        // 查询总记录数
        $getPageCounts = $modelBank->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $bankList = $modelBank->where($map)->order('id DESC')->limit($page->firstRow, $page->listRows)->select();

        $pageShow = $page->show();
        $this->assign('page', $pageShow);
        $this->assign('bankList', $bankList);
        $this->display('bank_list');
    }

    /**
     * 获取添加银行页模板
     * @return void
     */
    public function bankAdd()
    {

        $this->display('bank_add');
    }

    /**
     * 添加银行
     * @date 2015-01-18
     * @return [type] [description]
     */
    public function doAddBank()
    {      

        $data = array();
        $data['name']    = I('post.name');
        $data['ident']   = I('post.ident');
        $data['credit']  = I('post.credit');
		$data['pinyin']  = I('post.pinyin');
        $data['is_show'] = I('post.is_show');

        // 判断有没有文件上传
        if ( ! empty($_FILES['picture']['name'])){
            $upload = new \Think\Upload();                          // 实例化上传类
            // 设置附件上传类型    
            $upload->exts      = array('jpg', 'gif', 'png', 'jpeg');
            $upload->subName   = '';
            $upload->rootPath  = './Public/';
            $upload->savePath  = 'Bank/';                           // 设置附件上传目录    
            $upload->saveName  = date('YmdHis') . mt_rand(100, 999);

            // 上传文件     
            $info   =   $upload->uploadOne($_FILES['picture']);
			$data['picture'] = $info['savename'];
        }
        // 执行添加操作
        $modelBank = M('bank');
        if ($modelBank->create($data))
        {
            $modelBank->add();
			$this->success('银行添加成功', 'index');
        }else{
			$this->success('银行添加成功', 'index');
		}
        
    }

    /**
     * 银行信息修改
     * @date 2015-01-18
     * @return void 
     */
    public function bankEdit ()
    {
        $getId = I('get.id');

        $modelBank = M('bank');
        $bankInfoRes = $modelBank->where("id='{$getId}'")->find();

        $this->assign('bank', $bankInfoRes);
        $this->display('bank_edit');
    }

    /**
     * 执行银行信息修改操作
     * @date 2015-01-18
     * @return void 
     */
    public function doBankEdit()
    {
        $getId = I('post.id');
		$data = array();
        $data['id']      = $getId;
        $data['name']    = I('post.name');
        $data['ident']   = I('post.ident');
		$data['credit']  = I('post.credit');
		$data['pinyin']  = I('post.pinyin');
        $data['is_show'] = I('post.is_show');

        // 判断有没有文件上传
        if ( ! empty($_FILES['picture']['name'])){
            $upload = new \Think\Upload();                          // 实例化上传类
            // 设置附件上传类型    
            $upload->exts      = array('jpg', 'gif', 'png', 'jpeg');
            $upload->subName   = '';
            $upload->rootPath  = './Public/';
            $upload->savePath  = 'Bank/';                           // 设置附件上传目录    
            $upload->saveName  = date('YmdHis') . mt_rand(100, 999);

            // 上传文件     
            $info   =   $upload->uploadOne($_FILES['picture']);
			unlink('./Public/' . $info['savepath'] .I('post.old_picture'));  //删除旧文件
            $data['picture'] = $info['savename'];
            
        }else{
			$data['picture'] = I('post.old_picture');
		}
        // 执行修改
		$modelBank = M('bank');

        if ($modelBank->create($data)){
			$modelBank->save();
            $this->success('修改成功');
        }else{
            $this->error('修改失败');
        }
    }

    /**
     * 删除银行信息
     * @date 2015-01-22
     * @return void 
     */
    public function bankDelete(){
        $id = $_GET['id'];
        $modelBank = M("bank");
		$info = $modelBank->field("picture")->where("id =".$id)->find();
		if(!empty($info['picture'])){
			unlink('Public/Bank/' . $info['picture']);
		}
        
		$ls = $modelBank->where("id =".$id)->delete();
        if($ls){
            $this->success("删除成功",U("Bank/index"));
        }else{
            $this->error("删除失败");
        }
    }

 }
