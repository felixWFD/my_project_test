<?php
namespace Caobao\Controller;
use       Think\Controller;
/**
 * 后台用户登录模块
 *
 * 作者：  Eden
 * 日期：  2015-01-05
 */
class LoginController extends Controller {

    /**
     * 获取用户登录界面
     * 
     * @author HJP
     * @date   2015-01-11
     */
	public function index() {
		$this->display('login');
	}

    /**
     * 验证登录
     *
     * @author HJP
     * @date   2015-01-11
     */
    public function checkLogin() {
		if (!IS_POST) {
			$this->error('非法请求！');
		}
        // 用户名和密码是否验证通过
        $username = CBWAddslashes(I('post.username'));
        $password = CBWAddslashes(I('post.password'));
		
        $admin = M('admin_users');
        $res = $admin->field('id, password, status, login_failed, login_times')->where(array('username'=>$username))->find();
        if($res){
			if($res['status'] == 0){
				$this->error('你的帐户被锁定，请联系管理员');
				exit;
			}
			if($res['password'] != sha1($password)){
				$loginFailed = $res['login_failed']+1;
				if($loginFailed > 7){
					$admin->where("id=".$res['id'])->save(array('status'=>0));  // 账号锁定
					$this->error('账号或密码错误,账号已被锁定');
				}else{
					$admin->where("id=".$res['id'])->save(array('login_failed'=>$loginFailed));  // 保存登录失败次数
					$this->error('账号或密码错误,剩余'.(7-$loginFailed).'次机会');
				}
				exit;
			}
			if($res['login_failed'] > 0){
				$admin->where("id=".$res['id'])->save(array('login_failed'=>0));  // 重置登录失败次数
			}
			
            // 将用户的登录信息保存在SESSION中
            session('id', $res['id']);                  // 用户ID
            session('username', $username);     // 用户名
            session('status', $res['status']);          // 用户状态
            session('isLogin', true);                   // 是否为登录状态

            // 当“记住密码”项不为空时，使用Cookie存储用户信息
            $remember = I('post.remember');
            if ( ! empty($remember)) {

                // 检查用户是否登录的一个加密字符串，将在验证用户是否登录时比对
                $blue_symbol = md5('RIBBON');

                cookie('id', $res['id'], array('prefix' => 'blue_', 'expire' => 3600));
                cookie('username', $username, array('prefix' => 'blue_', 'expire' => 3600));
                cookie('status', $res['status'], array('prefix' => 'blue_', 'expire' => 3600));
                cookie('symbol', $blue_symbol, array('prefix' => 'blue_', 'expire' => 3600));
            } 

            // 修改管理此次登录的状态
            $upData['last_login_time'] = time();
            $upData['last_login_ip'] = ip2long(I('server.REMOTE_ADDR'));
            $upData['login_times'] = $res['login_times'] + 1;

            $admin->where('id = ' . $res['id'])->save($upData);
	
            // 登录成功，跳转到主页
			$this->redirect('Index/home');
        } else {
            $this->error('您输入的登录账号不存在!');
        }
    }

    
    /**
     * 方法功能：后台用户退出登录
     *
     * @author HJP
     * @date 2010-01-11
     */
    public function logOut() {
        // 删除后台需要存储的ID信息
        session('id', NULL);
        session('username', NULL);
        session('status', NULL);
        session('isLogin', NULL);

        // 清楚Cookie中保存的信息
        cookie(NULL, 'blue_');

        $this->redirect('Login/index');
    }
}