<?php
namespace Caobao\Controller;
use       Think\Controller;

    /**
     * Brand
     * 
     * @author: Sun
     * @date: 2015-01-22
     */

 class BrandController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * BrandList 品牌列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$modelBrand = M("product_brand");
		// 分页处理
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {

                if ($val == '' && $key == 'brand_name') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		$count = $modelBrand->where($map)->count();
		$page = new \Think\Page($count,20,$map); 
		$list = $modelBrand->where($map)->limit($page->firstRow.','.$page->listRows)->order('sort ASC,id ASC')->select();
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		
		$this->display('brand_list');
    }

    /**
     * brandAdd 添加品牌
     * @date 2015-01-22
     * @return void 
     */

    public function brandAdd(){
		
    	$this->display('brand_add');
    }

    /**
     * doBrandAdd 执行添加品牌
     * @date 2015-01-22
     * @return void 
     */

    public function doBrandAdd()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		$data = array();
        $data['brand_name'] = I('post.brand_name');
		$data['description'] = I('post.description');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');
       
        $modelBrand = M("product_brand");
        if($modelBrand->create($data)){
            $res=$modelBrand->add();
            if($res){
                $this->success('添加成功.' . $lastID, 'index');
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * brandEdit 修改品牌
     * @date 2015-01-22
     * @return void 
     */
    public function brandEdit(){
        
		$id = I('get.id');
        $modelBrand = M("product_brand");
        $brand = $modelBrand->where("id =".$id)->find();  
		$this->assign("brand",$brand);
		
        $this->display('brand_edit');
    }

    
     /**
     * doBrandEdit 执行修改品牌
     * @date 2015-01-22
     * @return void 
     */
    public function doBrandEdit()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
		$data = array();
		$data['id'] = I('post.id');
        $data['brand_name'] = I('post.brand_name');
		$data['description'] = I('post.description');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');
		
        $modelBrand = M("product_brand");
		if($modelBrand->save($data)){
			$this->success('修改成功.' . $lastID, 'index');
		}else{
			$this->error('修改失败！');
		}
    }

 
    /**
     * brandDelete 删除品牌
     * @date 2015-01-22
     * @return void 
     */
    public function brandDelete(){
		
        $id = I('get.id');
        $modelBrand = M("product_brand");
        $ls = $modelBrand->where("id =".$id)->delete();
        if($ls){
            $this->success("删除成功",U("Brand/index"));
        }else{
            $this->error("删除失败");
        }
    }





}