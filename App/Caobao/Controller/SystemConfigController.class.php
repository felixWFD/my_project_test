<?php
namespace Caobao\Controller;
use       Think\Controller;


class SystemConfigController extends MyController{
	
	public function __construct() {
        parent::__construct();
    }
    function webConfig(){
        $sysConfig = M('sys_config');
        $row = $sysConfig -> select();
        $this -> assign('data',$row);
        $this -> display();
    }

    //保存配置
    function uploadConfig()
    {
        $data = $_POST;

        $save = M('sys_config');
		
        foreach($data as $key =>$value){
            $d['cvalue'] = $value;
            $save ->where("cname='$key'") -> save($d);
        }

    }

	//上传logo
    public function upLogo()
    {
        $upload             =     new \Think\Upload();  // 实例化上传类
        $upload->maxSize    =     3145728 ;             // 设置附件上传大小
        $upload->exts       =     array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath   =     "Public/Admin/img/Logo/";
        $upload->savePath   =      '';                          // 设置附件上传目录    // 上传文件
        $info   =   $upload->upload();

        $imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
        $db = M('sys_config');
        $data['cvalue'] = $imgUrl;
        $id = $_GET['id'];
		
		//保存
        if($id == 1){
            $db ->where("cname='logo'") -> save($data);
        }else if($id == 2){
            $db ->where("cname='logo2'") -> save($data);
        }else if($id == 3){
            $db ->where("cname='logo3'") -> save($data);
        }else{}
		
		echo $imgUrl;

    }

	/**
     * 上传水印图片
     * 此方法被商品添加页
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();  // 实例化上传类

        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = "./Public/water/";
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
        echo $imgUrl;
    }


}
