<?php
namespace Caobao\Controller;
use       Think\Controller;

class OrderInfoController extends MyController
{

    public function __construct()
    {
        parent::__construct();
    }

    //新首页显示
    function index()
    {


        $db = M('orders');
        $users = M('users');

        $order_sn=I('order_sn',null,'trim,stripslashes,htmlspecialchars');
        $order_num=I('order_num',null,'trim,stripslashes,htmlspecialchars');
        $order_status=I('order_status',-1,'intval');
        $order_type=I('order_type',-1,'intval');
        $user_id=I('user_id',-1,'intval');
        $province = I('get.province');
        $city     = I('get.city');
        $area     = I('get.area');
        $time_start=I('time_start',null,'trim');
        $time_end=I('time_end',null,'trim');

        if(in_array($order_status,[0,1,2,3,4])){
            $where = ' o.order_status = ' . $order_status . ' ';
        }else{
            $where = ' order_status !=0 ';
        }
        if(strlen($order_sn)>0){
            $where .=' and o.order_sn LIKE "%' . $order_sn  . '%" ';
        }
        if(strlen($order_num)>0){
            $where .=' and o.shipping_num LIKE "%' . $order_num . '%" ';
        }
        if(in_array($order_type,[0,1,2])){
            $where .=' and o.order_type =  ' . $order_type . ' ';
        }
        if($user_id>0){
            $where .=' and o.user_id= ' . $user_id . ' ';
        }

        if($province > 0 && $province != '选择省'){
            $where .= " AND p.id = ".$province;
        }
        if($city > 0 && $city != '选择市'){
            $where .= " AND c.id = ".$city;
        }
        if($area > 0 && $area != '选择县/区'){
            $where .= " AND a.id = ".$area;
        }
        if(strlen($time_start)>0)
            $where .=' and o.order_date >= ' . strtotime($time_start) . ' ';

        if(strlen($time_end)>0)
            $where .=' and o.order_date < ' . strtotime($time_end) . ' ';


        $count = $db->table('__ORDERS__ AS o')
            ->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
            ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
            ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
            ->where($where)
            ->field('o.*, a.addr,c.addr as city_addr,p.addr as pro_addr')
            ->count();

        $page = new \Think\Page($count, 10);
        $showPage = $page->show();

        $res = $db->table('__ORDERS__ AS o')
            ->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
            ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
            ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
            ->where($where)
            ->order('order_date DESC')
            ->limit($page->firstRow . ',' . $page->listRows)
            ->field('o.*, a.addr,c.addr as city_addr,p.addr as pro_addr')
            ->select();


        foreach($res as &$vo){
            $userId = $vo['user_id'];
            $u = $users -> where("id = $userId") -> find();
            $vo['uname'] = $u['uname'];
        }

        $dc = M('shippings');
        $dd = M('shipping_company');
        $shipping = $dc->field('sid')->select();
        foreach ($shipping as &$vo) {
            $company[] = $dd->find($vo['sid']);
        }


        //加载城市信息
        $city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
        $c = \Think\Area::city($city);

        $this->assign('company', $company);
        $this->assign('data', $res);
        $this->assign('page', $showPage);

        $this->assign('area', $area);
        $this->assign("city", $city);
        $this->assign('province', $province);
        $this->assign("city", $c);

        $this->assign("time_start", $time_start);
        $this->assign("time_end", $time_end);

        $this->assign("order_sn", $order_sn);
        $this->assign("order_num", $order_num);

        $this->assign("order_status", $order_status);
        $this->assign("order_type", $order_type);
        $this->display();

    }

//    //首页显示
//    function old_index()
//    {
//
//        $db = M('orders');
//        $users = M('users');
//
//        // 订单号查询
//        if (isset($_GET['sn_sub'])) {
//            $sn = '%' . $_GET['order_sn'] . '%';
//            $count = $db->where("order_status !=0 AND order_sn LIKE '$sn'")->count();
//            $page = new \Think\Page($count, 10);
//            $showPage = $page->show();
//            $res = $db->where("order_status !=0 AND order_sn LIKE '$sn'")->order('order_date DESC')->limit($page->firstRow . ',' . $page->listRows)->select();
//
//            //物流单号查询
//        } else if (isset($_GET['num_sub'])) {
//            $num = '%' . $_GET['order_num'] . '%';
//            $count = $db->where("order_status !=0 AND shipping_num LIKE '$num'")->count();
//            $page = new \Think\Page($count, 10);
//            $showPage = $page->show();
//            $res = $db->where("order_status !=0 AND shipping_num LIKE '$num'")->order('order_date DESC')->limit($page->firstRow . ',' . $page->listRows)->select();
//
//            //按订单状态查询
//        } else if ((isset($_GET['status_sub'])) && (($_GET['order_status']) != 999)) {
//            $status = $_GET['order_status'];
//            $count = $db->where("order_status = '$status'")->count();
//            $page = new \Think\Page($count, 10);
//            $showPage = $page->show();
//            $res = $db->where("order_status !=0 AND order_status  = '$status'")->order('order_date DESC')->limit($page->firstRow . ',' . $page->listRows)->select();
//			//按订单类型查询
//        } else if ((isset($_GET['type_sub'])) && (($_GET['order_type']) != 999)) {
//            $order_type = $_GET['order_type'];
//            $count = $db->where("order_status !=0 AND order_type = '$order_type'")->count();
//            $page = new \Think\Page($count, 10);
//            $showPage = $page->show();
//            $res = $db->where("order_status !=0 AND order_type  = '$order_type'")->order('order_date DESC')->limit($page->firstRow . ',' . $page->listRows)->select();
//			//按会员编号查询
//        } else if (isset($_GET['id'])) {
//            $user_id = $_GET['id'];
//            $count = $db->where("order_status !=0 AND user_id = '$user_id'")->count();
//            $page = new \Think\Page($count, 10);
//            $showPage = $page->show();
//            $res = $db->where("order_status !=0 AND user_id  = '$user_id'")->order('order_date DESC')->limit($page->firstRow . ',' . $page->listRows)->select();
//
//        } //查询全部
//        else {
//            $count = $db->where("order_status !=0")->count();    //获取总页数
//            $page = new \Think\Page($count, 10);
//            $showPage = $page->show();
//            $res = $db->where("order_status !=0")->order('order_date DESC')->limit($page->firstRow . ',' . $page->listRows)->select();
//        }
//
//		foreach($res as &$vo){
//			$userId = $vo['user_id'];
//			$u = $users -> where("id = $userId") -> find();
//			$vo['uname'] = $u['uname'];
//		}
//
//        $dc = M('shippings');
//        $dd = M('shipping_company');
//        $shipping = $dc->field('sid')->select();
//        foreach ($shipping as &$vo) {
//            $company[] = $dd->find($vo['sid']);
//        }
//
//
//        $this->assign('company', $company);
//
//        $this->assign('data', $res);
//        $this->assign('page', $showPage);
//
//        $this->display();
//
//    }
	
    /**
     * 批量执行删除订单
     * @return void 
     */
    public function batchDoDelete ()
    {
        if ( ! isset($_POST['batch_option']))
        {
            $this->error('非法操作');
            exit;
        }

        if ( $_POST['batch_option'] == -1)
        {
            $this->error('请选择执行操作');
            exit;
        }

        $ids = I('post.ids');
		$modelOrders = M('orders');
		foreach($ids as $key=>$value){
			$order = $modelOrders->field('order_type')->where("id=".$value)->find();
			if($order['order_type']=='0'){
				$modelOrderProducts = M('order_products');
				$modelOrderProducts->where("order_id=".$value)->delete();
			}elseif($order['order_type']=='1'){
				$modelOrderServer = M('order_server');
				$modelOrderServer->where("order_id=".$value)->delete();
			}elseif($order['order_type']=='2'){
				
			}elseif($order['order_type']=='3' || $order['order_type']=='4'){
				$modelOrderParts = M('order_parts');
				$modelOrderParts->where("order_id=".$value)->delete();
			}
		}
        $where = "id in (" . implode(', ', $ids) . ') ';

        $affectedRows = $modelOrders->where($where)->delete();
        if ($affectedRows)
        {
            $this->success('执行成功');
        }
        else
        {
            $this->error('执行失败');
        }

    }
	

    //获得订单的收货地址
    public function getUserAddress()
    {
        $getAddId = I('post.id');
        $db = M('user_address');
        $Address = $db->where("id = '{$getAddId}'")->find();
        $this->assign('addr', $Address);

        $this->ajaxReturn($Address);
    }

	
    //购买订单信息
    public function orderInfo()
    {

        $id = $_GET['id'];
        $price = $_GET['price'];

        //1 通过$id 获取购物信息
        //2 通过购物信息的物品id 获取物品的信息

        $order_products = M('order_products');
        $products = M('products');
        $order = M('orders');

        $orders = $order->alias('o')
						->field('o.*,a.addr,p.addr as pro_addr,c.addr as city_addr')
						->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
						->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
						->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
						->where(" o.order_type=0 and o.id = ".$id)
						->find();
        if($orders){
            //查询买的商品列表
            $order_prodects_list = $order_products->where(" order_id = $id ")->select();
            //查询买的商品详情

            $worth = 0;   //商品总价值
            foreach ($order_prodects_list as &$or) {
                $worth += $or['pro_price']*$or['pro_number'];
                $row = $products->find($or['pro_id']);
                foreach ($row as $key => $value) {
                    $or[$key] = $value;

                }
            }
        }

        if(strlen($worth)>0) $worth=[];
        if(strlen($price)>0) $price=[];
        if(count($order_prodects_list)==0) $order_prodects_list=[];
        if(count($orders)==0) $orders=[];

		$worth = sprintf("%01.2f", $worth);
        $this->assign('worth',$worth);
        $this->assign('price', $price);
        $this->assign('data', $order_prodects_list);
        $this->assign('order', $orders);
        $this->display();
    }


    //维修订单信息
    public function repairInfo()
    {

        $id = $_GET['id'];
		$price = $_GET['price'];
        $order = M('orders');
        $exist_order = $order->where("id = $id")->count();
		if($exist_order){
        //查询买家的地址信息
        $orders = $order->alias('o')
                ->field('o.*,a.addr,p.addr as pro_addr,c.addr as city_addr')
                ->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
                ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
                ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
                ->where(" o.order_type=1 and o.id = ".$id)
                ->find();

		//查询维修商品信息
		$modelOrderServer = M('order_server');
		$server = $modelOrderServer->where("order_id=".$id)->find();    //订单子表server信息
            if($server['relation_id']>0) {
                $modelRelation = M('repair_relation');
                $info = $modelRelation->alias("r")
                    ->field("c.cat_name,p.list_image,p.pro_name,f.name as fault_name,p2.name as plan_name")
                    ->join('__REPAIRS__ p ON r.product_id = p.id')
                    ->join('__PRODUCT_CAT__ c ON p.brand_id = c.id')
                    ->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
                    ->join('__REPAIR_PLAN__ p2 ON r.plan_ids = p2.id')
                    ->where("r.id=" . $server['relation_id'])
                    ->find();

                $modelColor = M('repair_color');
                $color = $modelColor->where('id=' . $server['color_id'])->find();
                $info['color'] = $color['name'];

                //维修师/维修店信息
                $modelShop = M('user_shop');
                $repair = $modelShop->alias('s')
                    ->field('s.*,a.addr as area,p.addr as province,c.addr as city')
                    ->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
                    ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
                    ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
                    ->where("s.id = " . $server['shop_id'])
                    ->find();
            }
        }
        if(count($info)==0) $info=[];
        if(count($server)==0) $server=[];
        if(count($repair)==0) $repair=[];
        if(count($price)==0) $price=[];
        if(count($orders)==0) $orders=[];

        $this->assign("info", $info);
        $this->assign("server", $server);
		$this->assign("repair", $repair);
		$this->assign('price', $price);
        $this->assign('order', $orders);
        $this->display();
    }
	

    //回收订单信息
    public function recoveryInfo()
    {

        $id = $_GET['id'];
		$price = $_GET['price'];
        $order = M('orders');
        $exist_order = $order->where("id = $id")->count();
		if($exist_order){
            //查询卖家的地址信息
            $orders = $order->alias('o')
                ->field('o.*,a.addr,p.addr as pro_addr,c.addr as city_addr')
                ->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
                ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
                ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
                ->where(" o.order_type=2 and o.id = ".$id)
                ->find();
        
            //查询回收商品信息
            $modelOrderRecovery = M('order_server');
            $server = $modelOrderRecovery->where("order_id=".$orders['id'])->find();    //订单子表server信息
            if($server['relation_id']>0) {
                $modelRelation = M('recovery_relation');
                $info = $modelRelation->alias("r")
                    ->field("c.cat_name,p.list_image,p.pro_name,f.name as fault_name,p2.name as plan_name")
                    ->join('__REPAIRS__ p ON r.product_id = p.id')
                    ->join('__PRODUCT_CAT__ c ON p.brand_id = c.id')
                    ->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
                    ->join('__REPAIR_PLAN__ p2 ON r.plan_ids = p2.id')
                    ->where("r.id=" . $server['relation_id'])
                    ->buildSql();

                $modelColor = M('repair_color');
                $color = $modelColor->where('id=' . $server['color_id'])->find();
                $info['color'] = $color['name'];

            //回收门店信息
            $modelShop = M('user_shop');
            $recovery = $modelShop->alias('s')
                            ->field('s.*,a.addr as area,p.addr as province,c.addr as city')
                            ->join('__ADDR_AREA__ AS a ON a.id = s.area_id')
                            ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
                            ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
                            ->where("s.id = ".$server['shop_id'])
                            ->find();
            }
        }
        if(count($info)==0) $info=[];
        if(count($server)==0) $server=[];
        if(count($recovery)==0) $recovery=[];
        if(count($price)==0) $price=[];
        if(count($orders)==0) $orders=[];

        $this->assign("info",$info);
        $this->assign("server",$server);
		$this->assign("recovery",$recovery);
		$this->assign('price', $price);
        $this->assign('order', $orders);
        $this->display();
    }

    //添加物流信息
    public function shippingAdd()
    {
        $admin_id = $_SESSION['id'];
		$_POST['send_date']=time();
        $db = M('orders');
        $row = $db->save($_POST);
        if ($row > 0) {
            echo $row;

            // 写入日志
            $log = new \Caobao\Controller\LogController();
            $log->setLog('用户（UID：' . $admin_id . '）对订单id为' . $_POST['id'] . '的订单增加了' . $_POST['shipping_way'] . '物流' . $_POST['shipping_num']);

        }
    }

    //获得物流信息
    public function shippingGet()
    {
        $db = M('orders');
        $row = $db->where($_POST)->find();
        $this->ajaxReturn($row);
    }

    //取消订单
    public function OrderCancel()
    {

        $admin_id = $_SESSION['id'];    //当前登陆的管理员
        $order_products = M('order_products');
        $products = M('products');
		$modelRelation = M('product_relation');
        $db = M('orders');
        $id = $_POST['id'];    //要操作的订单id
        //1.如果已经支付成功 且没发货 则将钱返还,物品入库,后将订单状态设为已取消0
        $row = $db->where("id = $id")->find();

        if (!empty($row)) {  //数据库查询到订单信息

            //查询买的商品id和数量
            $order_prodects_list = $order_products->where("order_id = $id")->select();
            if ($row['order_status'] == 0) {    //订单是已经 取消状态
                echo '订单已经是取消状态!';
            } else if ($row['order_status'] == 5) {    //订单是 完成状态
                echo '订单已经完成!无法取消';
            }else if ($row['order_status'] == 2) {    //查询订单状态是 未发货 状态
                
				if ($row['pay_status'] == 0) {    //如果是未付款的订单

                    //入库
                    foreach ($order_prodects_list as $or) {    //查询订单下所有的商品id和数量
                        $productsId = $or['pro_id'];
                        $productsNum = $or['pro_number'];
						$productsColor = $or['pro_color'];
                        $products->where("id=".$productsId)->setInc('stock_num', $productsNum);    //入库
						$modelRelation->where("pro_attr=".$or['pro_attr']." AND pro_id=".$or['pro_id'])->setInc('pro_stock',$or['pro_number']); //入库
                    }

                    $data['order_status'] = 0;
                    $db->where("id = $id")->save($data);    //订单状态设为取消状态

                } else if ($row['pay_status'] == 1) {    //如果是支付的订单
                    $pay = $row['deail_price'];    //获得所消费的金额
                    //返还的方法  debug 未实现

                    //入库
                    foreach ($order_prodects_list as $or) {    //查询订单下所有的商品id和数量
                        $result = $products->where("id=".$or['pro_id'])->setInc('stock_num', $or['pro_number']);    //入库
						$modelRelation->where("pro_attr=".$or['pro_attr']." AND pro_id=".$or['pro_id'])->setInc('pro_stock',$or['pro_number']); //入库
                    }
                    if ($result > 0) {

                        $data['order_status'] = 0;
                        $closeStatus = $db->where("id = $id")->save($data);    //订单状态设为取消状态
                        if ($closeStatus > 0) {
                            echo '订单取消成功!';
                            // 写入日志
                            $log = new \Caobao\Controller\LogController();
                            $log->setLog('用户（UID：' . $admin_id . '）取消了id为' . $_POST['id'] . '的订单增');
                        } else {
                            echo '订单取消失败!';
                        }

                    } else {
                        echo '订单取消失败!';
                    }

                }else{
                    echo '订单取消失败!';
                }

            } else if ($row['order_status'] == 1) {    //未支付
                //入库
                foreach ($order_prodects_list as $or) {    //查询订单下所有的商品id和数量
                    $productsId = $or['pro_id'];
                    $productsNum = $or['pro_number'];
					$productsColor = $or['pro_color'];
                    $result = $products->where("id=".$productsId)->setInc('stock_num', $productsNum);    //入库
					$modelColor->where("pro_id=".$productsId." AND name='".$productsColor."'")->setInc('stock_num',$productsNum); //颜色表加库存
                }
                if ($result > 0) {

                    $data['order_status'] = 0;
                    $closeStatus = $db->where("id = $id")->save($data);    //订单状态设为取消状态
                    if ($closeStatus > 0) {
                        echo '订单取消成功!';
                        // 写入日志
                        $log = new \Caobao\Controller\LogController();
                        $log->setLog('用户（UID：' . $admin_id . '）取消了id为' . $_POST['id'] . '的订单增');
                    } else {
                        echo '订单取消失败!';
                    }

                } else {
                    echo '订单取消失败!!';
                }


            } else if ($row['order_status'] == 3) {    //已发货
                echo '订单已发货,无法取消!';
            }
        }



    }

//    //无效订单列表
//    public function invalidOrder(){
//        $db = M('orders');
//        $users = M('users');
//        $count = $db->where("order_status = 0")->count();    //获取总页数
//        $page = new \Think\Page($count, 10);
//        $showPage = $page->show();
//        $res = $db->where("order_status = 0")->order('id ASC')->limit($page->firstRow . ',' . $page->listRows)->select();
//        foreach($res as &$vo){
//            $userId = $vo['user_id'];
//            $u = $users -> where("id = $userId") -> find();
//            $vo['uname'] = $u['uname'];
//        }
//        //$this->assign('company', $company);
//
//        $this->assign('data', $res);
//        $this->assign('page', $showPage);
//
//        $this->display();
//
//    }

    //新无效订单列表
    public function invalidOrder(){
        $db = M('orders');
        $users = M('users');
        //获取总页数
        $count = $db->table('__ORDERS__ AS o')
            ->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
            ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
            ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
            ->where(" o.order_status = 0 ")
            ->field('o.*, a.addr,c.addr as city_addr,p.addr as pro_addr')
            ->count();

        $page = new \Think\Page($count, 10);
        $showPage = $page->show();
        //分页取数据
        $res = $db->table('__ORDERS__ AS o')
            ->join('__ADDR_AREA__ AS a ON a.id = o.area_id')
            ->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
            ->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
            ->where(" o.order_status = 0 ")
            ->field('o.*, a.addr,c.addr as city_addr,p.addr as pro_addr')
            ->order('id ASC')
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();

        foreach($res as &$vo){
            $u = $users -> where("id = {$vo['user_id']}") -> find();
            $vo['uname'] = $u['uname'];
        }

        $this->assign('data', $res);
        $this->assign('page', $showPage);

        $this->display();

    }
    /**
     * 执行城市三级联动下拉框获取操作
     * @date 2015-01-18
     * @return void
     */
    public function changeSelect()
    {
        $id = I('post.id');
        $type = I('post.type');

        // 实例化数据模型
        $modelCity = M('addr_city');
        $modelArea = M('addr_area');
        if($type == 'province'){
            $list = $modelCity->where("one_id=".$id)->select();
        }else{
            $list = $modelArea->where("two_id=".$id)->select();
        }

        if(!is_array($list)){
            exit;
        }
        $str = '';
        foreach($list as $item){
            $str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
        }
        echo $str;
        exit;
    }

}
