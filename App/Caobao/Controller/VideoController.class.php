<?php
namespace Caobao\Controller;
use       Think\Controller;

class VideoController extends MyController
{
    public function __construct()
    {
        parent::__construct();
    }

    //获得视频列表
    public function index()
    {
		// 获取商品类别
		$classify_id = I('get.classify_id');
		$type_id = I('get.type_id');
		$brand_id = I('get.brand_id');
		$cat_id = I('get.cat_id');
		
        $modelCats = M('product_cat');
		
		$classifyRes = $modelCats->where("is_show=1 AND fid=1")->select();
		$this->assign('classifyRes', $classifyRes);
		
		if ($classify_id != '请选择分类' && $classify_id != '') {
			$typeRes = $modelCats->where("is_show=1 AND fid=".$classify_id)->select();
			$this->assign('typeRes', $typeRes);
			$this->assign('classify_id', $classify_id);
		}

		if ($type_id != '请选择' && $type_id != '') {
			$map['type_id'] = $type_id;
			$where .= " AND type_id=".$type_id;
			$brandRes = $modelCats->where("is_show=1 AND fid=".$type_id)->select();
			$this->assign('brandRes', $brandRes);
			$this->assign('type_id', $type_id);
		}

		if ($brand_id != '请选择' && $brand_id != '') {
			$map['brand_id'] = $brand_id;
			$where .= " AND brand_id=".$brand_id;
			$catRes = $modelCats->where("is_show=1 AND fid=".$brand_id)->select();
			$this->assign('catRes', $catRes);
			$this->assign('brand_id', $brand_id);
		}

		if ($cat_id != '请选择' && $cat_id != '') {
			$map['cat_id'] = $cat_id;
			$where .= " AND cat_id=".$cat_id;
			$this->assign('cat_id', $cat_id);
		}
		
        $modelVideo = M('video');
		
        // 查询总记录数
        $getPageCounts = $modelVideo->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $videoList = $modelVideo->where($map)->order('id DESC')->limit($page->firstRow, $page->listRows)->select();

        $pageShow = $page->show();
        $this->assign('page', $pageShow);
        $this->assign('videoList', $videoList);
        $this->display('video_list');
    }

    //修改视频信息
    public function videoEdit()
    {
		$id = I('get.id');
        $modelVideo = M('video');
        $data = $modelVideo->where("id = $id")->find();
        $this->assign('data', $data);
		
		// 此处将分类变量分配到模板,视频分类引用商品分类
		$modelCat = M('product_cat');
		$modelProducts = M('products');
        $typeRes = $modelCat->where('fid=5')->select();
		
		$where = "is_on_sale=1";
		if($data['type_id'] != ''){
			$brandRes = $modelCat->where('fid='.$data['type_id'])->select();
			$where .= " AND type_id=".$data['type_id'];
		}
		if($data['brand_id'] != ''){
			$modelRes = $modelCat->where('fid='.$data['brand_id'])->select();
			$where .= " AND brand_id=".$data['brand_id'];
		}
		if($data['cat_id'] != ''){
			$where .= " AND cat_id=".$data['cat_id'];
		}
		
		$products = $modelProducts->where($where)->select();
		$this->assign('typeRes', $typeRes);
		$this->assign('brandRes', $brandRes);
        $this->assign('modelRes', $modelRes);
		$this->assign('products', $products);
		
        $this->display('video_edit');
    }

    //修改视频信息操作
    public function doEditVideo()
    {
		$id = I('post.id');
        $title = I('post.title');
		$bimg = I('post.bimg');
		
		// 当标题为空时，拒绝修改
        if (empty($title)) {
            $this->error('视频修改失败');
            exit;
        }
		
		// 处理基本通用信息
		$data = array();
		$data['title']    = $title;
		$data['type_id']   = I('post.type_id');
		$data['brand_id']   = I('post.brand_id');
        $data['cat_id']   = I('post.cat_id');
		$data['product_id']   = I('post.product_id');
        $data['weburl']   = I('post.weburl');
		$data['keywords']   = I('post.keywords');
		$data['description']   = I('post.description');
		$data['content']   = I('post.content');
        $data['status']   = I('post.status');

        //如果上传了图片
        if (isset($_FILES)) {
			if (($_FILES['simg']['error']) == 0) {
				$upload = new \Think\Upload();  // 实例化上传类
				$rootPath = "./Public/Video/"; //图片保存文件夹路径
				$upload->maxSize = 3145728;             // 设置附件上传大小
				$upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
				$upload->rootPath = $rootPath;
				$upload->savePath = '';                          // 设置附件上传目录    // 上传文件       
				$info = $upload->uploadOne($_FILES['simg']);
				$simg = $info['savepath'].$info['savename'];
				if (!$info) {
					// 上传错误提示错误信息
					$this->error($upload->getError());
				} else {
					// 上传成功 获取上传文件信息
					$data['simg'] = $info['savepath'].$info['savename'];
				}
				unlink('Public/Video/'.$bimg);   //删除旧图片
			}
		}
		$modelVideo = M('video');
        //提交保存
        $row = $modelVideo->where("id=$id")->save($data);
        if ($row > 0) {
            $this->success('修改成功', U('Video/index/cat_id/'.I('post.cat_id')));
        } else {
            $this->error('修改失败');
        }
    }

    //添加视频
    public function videoAdd()
    {
		// 此处将分类变量分配到模板,视频分类引用商品分类
		$modelCat = M('product_cat');
        $typeRes = $modelCat->where('fid=5')->select();
        $this->assign('typeRes', $typeRes);

        $this->display('video_add');
    }
	
	//添加视频操作
	public function doAddVideo(){
		
		$title = I('post.title');
		// 当标题为空时，拒绝添加
        if (empty($title)) {
            $this->error('视频添加失败');
            exit;
        }
		
		// 处理基本通用信息
		$data = array();
		$data['title']    = $title;
		$data['type_id']   = I('post.type_id');
		$data['brand_id']   = I('post.brand_id');
        $data['cat_id']   = I('post.cat_id');
		$data['product_id']   = I('post.product_id');
        $data['weburl']   = I('post.weburl');
		$data['keywords']   = I('post.keywords');
		$data['description']   = I('post.description');
		$data['content']   = I('post.content');
		$data['create_time']   = time();
        $data['status']   = I('post.status');

		if (isset($_FILES)) {
			if (($_FILES['simg']['error']) == 0) {
				$upload = new \Think\Upload();  // 实例化上传类
				$rootPath = "./Public/Video/"; //图片保存文件夹路径
				$upload->maxSize = 3145728;             // 设置附件上传大小
				$upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
				$upload->rootPath = $rootPath;
				$upload->savePath = '';                          // 设置附件上传目录    // 上传文件       
				$info = $upload->uploadOne($_FILES['simg']);
				$simg = $info['savepath'].$info['savename'];
				if (!$info) {
					// 上传错误提示错误信息
					$this->error($upload->getError());
				} else {
					// 上传成功 获取上传文件信息
					$data['simg'] = $info['savepath'].$info['savename'];
				}
			}
		}

        $modelVideo = M('video');
        if ($modelVideo->create($data)) {
            $rt = $modelVideo->add();
            if ($rt) {
				CBWBaiduPush(array(C('SITE_URL')."/video/".$rt.".html"));        // 百度SEO推送
				CBWBaiduPush(array(C('MOBILE_URL')."/video/".$rt.".html"), 'm'); // 百度SEO推送
                $this->success('视频添加成功', U('Video/index/cat_id/'.I('post.cat_id')));
                exit;
            } else {
                $this->error('视频添加失败');
            }

        } else {
            $this->error('上传出错。');
        }

	}

    //删除单个视频信息
    public function delVideo()
    {
        $id = I('get.id');
        $modelVideo = M('video');
        $row = $modelVideo->where("id=$id")->find();
        $simg = $row['simg'];

        //删除文件
        unlink('Public/Video/'.$simg);
        //删除数据
        $row = $modelVideo->where("id=$id")->delete();
        if ($row > 0) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }


    }

    /**
     * 执行商品类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeCatSelect()
    {
        $catId = I('post.id');
		$type = I('post.type');
        $modelProductCat = M('product_cat');
		$modelProducts = M('products');
		
		$data = array();
		
        $cats = $modelProductCat->where("fid = '{$catId}'")->select();
		if(is_array($cats)){
			$opt = '';
			foreach($cats as $key=>$vo){
				$opt .= '<option value="'.$vo['id'].'">'.$vo['cat_name'].'</option>';
			}
			$data['cat'] = $opt;
		}
		
		$products = $modelProducts->field("id,pro_name")->where($type."=".$catId)->select();
		if(is_array($products)){
			$opt = '';
			foreach($products as $key=>$vo){
				$opt .= '<option value="'.$vo['id'].'">'.$vo['pro_name'].'</option>';
			}
			$data['products'] = $opt;
		}
		
        echo json_encode($data);
		exit;
    }
	
}
