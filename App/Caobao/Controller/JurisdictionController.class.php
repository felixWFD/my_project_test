<?php
namespace Caobao\Controller;
use       Think\Controller;
/**
 * 	后台用户组模块控制器
 * 	包含用户组管理以及后台权限角色分配
 *  
 * 	作者： 	Eden
 * 	时间：	2015-01-04
 */
class JurisdictionController extends MyController {

	/**
     * 构造方法
     * 初始化 用户组设置 类下的一些数据
     *
     * @author  HJP
     * @date    2015-01-11
     */
	public function __construct() {
		parent::__construct();
	}
	
	/**
     * 获取用户组列表
     *
     * @author HJP
     * @date    2015-01-12
     * @return  void
     */
	public function index() {
	
		$modelGroup  = M('auth_group');

        // 还需要获取用户的用户组
        $groups = $modelGroup ->field('id, title, description, status')->order('id ASC')->select();
		
        $this->assign('groups', $groups);
        $this->display('group_list');
	}
	
	/**
     * 添加用户组，获取添加用户组界面 
     * 
     * @return void
     */
    public function groupAdd() {
        $this->display('group_add');
    }
	
    /**
     * 添加用户组Ajax验证
     * 
     * @return void
     */
    public function checkGroupExists () {

        $modelGroup = M('auth_group');
        $res = $modelGroup->field('id')->where("title = '{$_GET['title']}'")->select();

        if ( ! $res)
        {
            echo 1;
        }
        else {
            echo 0;
        }
    }
	
    /**
     * 添加用户组
     *
     * @author HJP
     * @date    2015-01-12
     */
    public function doGroupAdd()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
        // 获取客户端提交的数据，整合保存入数据库
        $data['title'] = I('post.title');
        $data['description'] = I('post.description');
        $data['status']  = '1';

        $modelGroup = M('auth_group');

        if ($modelGroup->create($data))
        {
            // 执行插入操作
            $lastID = $modelGroup->add();

            if ($lastID > 0) 
            {

				// 实例化日志类，添加日志
				$setLog = new \Caobao\Controller\LogController();
				$setLog->setLog('用户组（ID: ' . $lastID . '）添加成功！');

				$this->success('用户组添加成功.' . $lastID, 'index');
				exit;
            }
        }
        // 执行失败跳回添加页
        $this->error('用户组添加失败');
    }

    /**
     * 获取用户组修改页面
     * @return void
     */
    public function groupEdit() {
        $groupId = I('get.id');

        $modelGroup = M('auth_group');
        $groupInfo = $modelGroup->find($groupId);

        $this->assign('groupInfo', $groupInfo);
        $this->display('group_edit');
    }

    /**
     * 修改用户信息
     * @return void
     */
    public function doGroupEdit ()
    {
		if (!IS_POST) {
			exit('页面错误~');
		}
        $data['id'] = I('post.group_id');
        $data['title'] = I('post.title');
        $data['description'] = I('post.description');
        $data['status'] = I('post.status');

        $modelGroup = M('auth_group');
        if ($modelGroup->save($data))
        {
            $this->success('修改成功.' . $lastID, 'index');
            exit;
        }

        $this->error('修改失败！');
    }
	
    /**
     * 删除用户组
     * 
     * @param  通过GET传入的ID
     * @return void
     */
    public function groupDelete($id) {
        $groupId = I('get.id', 0);

        $modelGroup  = M('auth_group');

        $res1 = $modelGroup->delete($groupId);

        if ($res1)
        {
            $this->success('删除成功！');
        }
        else
        {
            $this->error('删除失败！');
        }
    }
	
    /**
     * 修改用户组状态
     * 1 表示启用， 0表示禁用
     * 
     * @return void
     */
    public function toggleGroupStatus ()
    {
        $groupID = I('get.id');
        $prevStatus = I('get.prev');

        $modelGroup = M('auth_group');
        $res = $modelGroup->where("id=" . $groupID)->save(array('status' => ! $prevStatus));

        if ($res)
        {
            // 写入日志
            $log = new \Caobao\Controller\LogController();
            $statusStr = empty($prevStatus) ? '启用' : '禁用';
            $log->setLog('用户组（ID：' . $userID . '）账户状态被' . $statusStr);
            $this->success('修改成功.');
        }
        else
        {
            $this->error('修改失败.');
        }
    }

	/**
	 * 	用户组权限授权
	 *
	 * 	@author HJP
	 * 	@date   2015-04-03
	 */
	public function setRule()
	{
		$groupID = I('get.id');
		$modelGroup = M('auth_group');
		$ruleString=$modelGroup->where('id='.$groupID)->getField('rules'); //获取用户组的rules
		
		$this->assign('gid', $groupID);
		$this->assign('rulesarray', explode(',',$ruleString));
		$this->assign('rulelist', $this->_getRuleList());
		$this->display('rule_set');
	}
	
    /**
     * 修改用户组权限授权信息
     * @return void
     */
    public function doRuleSet ()
    {
		if (!IS_POST) {
			exit('页面错误~');
		}
        $groupId = I('post.gid');
        $rule=I('post.rule');
        $data['rules'] = implode(',',$rule);
		
        $modelGroup = M('auth_group');
		$rs = $modelGroup->where(array('id'=>$groupId))->save($data);

		if($rs !==false)
        {
            $this->success('授权成功.' . $lastID, 'index');
            exit;
        }

        $this->error('授权失败！');
    }

	/**
	 * 	方法功能：用户组规则列表
	 *
	 * 	@author HJP
	 * 	@date   2015-00-05
	 */
	public function ruleList() 
	{
		$this->assign('rulelist', $this->_getRuleList());
		$this->display('rule_list');

	}
	
	/**
	 * 	方法功能：创建后台用户组规则
	 *
	 * 	@author HJP
	 * 	@date   2015-01-11
	 */
	public function ruleAdd() 
	{
		
		$ruleId = I('get.id');
		$modelRule = M('auth_rule');
		
		//顶级规则数组
		$ruleArray = $modelRule->where(array('pid'=>0))->select();
		$this->assign('ruleArray', $ruleArray);
		
		$this->assign('ruleId',$ruleId);
		$this->display('rule_add');
	}
	
	/**
	 * 	添加用户组规则
	 *
	 * 	@author HJP
	 * 	@date   2015-01-11
	 */
	public function doRuleAdd() 
	{
		
		if (!IS_POST) {
			exit('页面错误~');
		}
        // 获取客户端提交的数据，整合保存入数据库
        $data['pid'] = I('post.pid');
		$data['name'] = I('post.name');
		$data['title'] = I('post.title');
        $data['status'] = I('post.status');

        $modelRule = M('auth_rule');

        if ($modelRule->create($data))
        {
            // 执行插入操作
            $lastID = $modelRule->add();

            if ($lastID > 0) 
            {

				// 实例化日志类，添加日志
				$setLog = new \Caobao\Controller\LogController();
				$setLog->setLog('用户组规则（ID: ' . $lastID . '）添加成功！');

				$this->success('用户组规则组添加成功.' . $lastID, 'ruleList');
				exit;
            }
        }
        // 执行失败跳回添加页
        $this->error('用户组规则添加失败');
	}
	
    /**
     * 获取用户组规则修改页面
     * @return void
     */
    public function ruleEdit() {
        $ruleId = I('get.id');

        $modelRule = M('auth_rule');
        $ruleInfo = $modelRule->find($ruleId);
        $this->assign('ruleInfo', $ruleInfo);
		
		//顶级规则数组
		$ruleArray = $modelRule->where(array('pid'=>0))->select();
		$this->assign('ruleArray', $ruleArray);
        $this->display('rule_edit');
    }

    /**
     * 修改用户组规则信息
     * @return void
     */
    public function doRuleEdit ()
    {
		if (!IS_POST) {
			exit('页面错误~');
		}
        $data['id'] = I('post.rule_id');
		$data['name'] = I('post.name');
        $data['title'] = I('post.title');
        $data['status'] = I('post.status');
		$data['pid'] = I('post.pid');

        $modelRule = M('auth_rule');
        if ($modelRule->save($data))
        {
            $this->success('修改成功.' . $lastID, 'ruleList');
            exit;
        }

        $this->error('修改失败！');
    }
	
	/**
     * 删除用户规则
     * 
     * @param  通过GET传入的ID
     * @return void
     */
    public function ruleDelete($id) 
	{
        $ruleId = I('get.id', 0);

        $modelRule  = M('auth_rule');

        $res1 = $modelRule->delete($ruleId);

        if ($res1)
        {
			//检查是否有下级规则,有则批量删除
			$ruleArray = $modelRule->field('id')->where(array('pid'=>$ruleId))->select();
			if(is_array($ruleArray) && !empty($ruleArray))
			{
				foreach($ruleArray as $key=>$item)
				{
					$modelRule->delete($item['id']);
				}
			}
            $this->success('删除成功！');
        }
        else
        {
            $this->error('删除失败！');
        }
    }
	
	/**
     * 私有方法，获取所有规则列表
     * @return array 所有规则列表
     */
    private function _getRuleList()
    {
        $modelRule = M('auth_rule');
		$ruleArray = $modelRule->field('id, name, title')->where(array('pid'=>0, 'status'=>1))->select();
		foreach($ruleArray as $key=>$rule)
		{
			$ruleArray[$key]['lowers'] = $modelRule->field('id, name, title')->where(array('pid'=>$rule['id'], 'status'=>1))->select();
		}
        return $ruleArray;
    }


}