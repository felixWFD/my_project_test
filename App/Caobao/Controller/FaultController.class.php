<?php
namespace Caobao\Controller;
use       Think\Controller;

class FaultController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * faultList 故障列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$product_id = I('get.product_id');
		
		$modelFault = M("repair_fault");
		$modelCat = M("repair_fault_cat");
		// 分页处理
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
				
                if ($val == '查找全部关联商品' && $key == 'product_id') {
                    continue;
                }

                if ($val == '' && $key == 'name') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		$count = $modelFault->where($map)->count();
		$page = new \Think\Page($count,20,$map); 
		$list = $modelFault->where($map)->limit($page->firstRow.','.$page->listRows)->order('sort DESC,id ASC')->select();
		foreach($list as $key=>$vo){
			$cat = $modelCat->where('id='.$vo['cat_id'])->find();
			$list[$key]['cat_name'] = $cat['name'];
		}
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		
		//维修商品
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
		$this->assign('product_id', $product_id);
		$this->display('fault_list');
    }

    /**
     * faultAdd 添加故障
     * @date 2015-01-22
     * @return void 
     */

    public function faultAdd(){
		
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
		//故障分类
		$modelCat = M('repair_fault_cat');
		$modelProductCat = M('product_cat');
		$cat = $modelCat->select();
		foreach($cat as $key=>$vo){
			$pro_cat = $modelProductCat->where('id='.$vo['cat_id'])->find();
			$cat[$key]['name'] = $pro_cat['cat_name'].'-'.$vo['name'];
		}
		$this->assign('cat', $cat);
		
    	$this->display('fault_add');
    }

    /**
     * doFaultAdd 执行添加故障
     * @date 2015-01-22
     * @return void 
     */

    public function doFaultAdd()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		$data['product_id'] = I('post.product_id');
		$data['cat_id'] = I('post.cat_id');
        $data['name'] = I('post.name');
		$data['description'] = I('post.description');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');
       
        $modelFault = M("repair_fault");
        if($modelFault->create($data)){
            $res=$modelFault->add();
            if($res){
                $this->success('添加成功.' . $lastID, 'index/product_id/'.$data['product_id']);
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * faultEdit 修改故障
     * @date 2015-01-22
     * @return void 
     */
    public function faultEdit(){
		
        $id = I('get.id');
		
        $modelFault = M("repair_fault");
		
        $fault = $modelFault->where("id =".$id)->find();  
		$this->assign("fault",$fault);
		
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
		//故障分类
		$modelCat = M('repair_fault_cat');
		$modelProductCat = M('product_cat');
		$cat = $modelCat->select();
		foreach($cat as $key=>$vo){
			$pro_cat = $modelProductCat->where('id='.$vo['cat_id'])->find();
			$cat[$key]['name'] = $pro_cat['cat_name'].'-'.$vo['name'];
		}
		$this->assign('cat', $cat);
		
        $this->display('fault_edit');
    }

    
    /**
     * doFaultEdit 执行修改故障
     * @date 2015-01-22
     * @return void 
     */
    public function doFaultEdit()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
		$data['id'] = I('post.id');
        $data['product_id'] = I('post.product_id');
		$data['cat_id'] = I('post.cat_id');
        $data['name'] = I('post.name');
		$data['description'] = I('post.description');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');

        $modelFault = M("repair_fault");
		if($modelFault->save($data))
		{
			$this->success('修改成功.' . $lastID, 'index/product_id/'.$data['product_id']);
		}
		else
		{
			$this->error('修改失败！');
		}
    }

    /**
     * 执行快速修改操作
     * @return void
     */
    public function postFaultEdit()
    {
        $getId = $_POST['id'];
		$getType = $_POST['type'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		
		switch ($getType)
		{
			case 'sort':
			  $data['sort'] = $getVal;
			  break;
			case 'is_show':
			  $data['is_show'] = $getVal;
			  break;
		}

        // 修改信息
        $modelFault = M('repair_fault');
        if ($modelFault->create($data)) 
		{
            $modelFault->save();
			echo "1";
			exit;
        }
		else
		{
			echo "0";
			exit;
		}

    }
	
    /**
     * faultDelete 删除故障
     * @date 2015-01-22
     * @return void 
     */
    public function faultDelete(){
        
		$id = $_GET['id'];
        $ls = M("repair_fault")->where("id =".$id)->delete();
        if($ls){
			M("repair_relation")->where("fault_id =".$id)->delete();   // 删除关联表相关数据
            $this->success("删除成功",U("Fault/index/product_id/".$data['product_id']));
        }else{
            $this->error("删除失败");
        }
    }

    /**
     * faultCopy 复制故障
     * @date 2015-01-22
     * @return void 
     */

    public function faultCopy(){
		
		//维修商品
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
    	$this->display('fault_copy');
    }

    /**
     * doFaultCopy 执行复制故障
     * @date 2015-01-22
     * @return void 
     */

    public function doFaultCopy()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		$product_id = I('post.product_id');
		$copy_id = I('post.copy_id');
       
        $modelFault = M("repair_fault");
		$count = $modelFault->where('product_id='.$copy_id)->count();
		if($count > 0){
			$this->error("复制失败,此维修商品已有相关故障.");
			exit;
		}
		
		$faultRes = $modelFault->where('product_id='.$product_id)->select();
		foreach($faultRes as $key=>$vo){
			$data = array();
			$data['cat_id']      = $vo['cat_id'];
			$data['name']        = $vo['name'];
			$data['product_id']  = $copy_id;
			$data['description'] = $vo['description'];
			$data['sort']        = $vo['sort'];
			$data['is_show']     = $vo['is_show'];
			$modelFault->create($data);
			$rt = $modelFault->add();
		}

        if($rt){
			$this->success('添加成功.', U('Fault/index', array('product_id'=>$copy_id)));
		}else{
			$this->error("添加失败");
		}      
    }



}