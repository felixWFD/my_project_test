<?php
namespace Caobao\Controller;
use       Think\Controller;

class SearchController extends MyController 
{
    public function index (){

        $modelSearch = M('search');
        $pageCounts = $modelSearch->count();
        $pageSize = 10;

        $page = new \Think\Page($pageCounts, $pageSize);
        $kw = $modelSearch->order("times DESC")->limit($page->firstRow, $page->listRows)->select();

        $pageShow = $page->show();

        $this->assign('page', $pageShow);
        $this->assign('counts', $pageCounts);
        $this->assign('kw', $kw);
        $this->display('index');
    }

    public function saveAddKT ()
    {
        $getData = $_POST;

        $modelK = M('search');
        if ($modelK -> create($getData))
        {
            $lastId = $modelK->add();
            if ($lastId)
            {
                echo 1;
                exit;
            } 
        }
        
        echo 0;
    }

    public function batchDelete ()
    {
        $getKeyIds = I('post.ids');

        $where = implode(', ', $getKeyIds);
        $where = "id in (" . $where . ')';
        $modelSearch = M('search');
        $affectedRows = $modelSearch->where($where)->delete();
        if ($affectedRows)
        {
            $this->success('删除成功');
            exit;
        }
        $this->error('删除失败');
    }

    public function delete ()
    {
        $getKeyId = I('get.id');

        $modelK = M('search');
        if ($modelK->delete($getKeyId))
        {
            $this->success('删除成功');
            exit;
        }
        $this->error('删除失败');
    }

    public function saveTimes()
    {
        $modelSearch = M('search');
        $affectedRows = $modelSearch->data($_GET)->save();

        if ($affectedRows)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function saveKeyword ()
    {
        $model = M('search');
        if ($model->create())
        {
            if ($model->save())
            {
                die('1');
            }
        }
        echo 0;
    }
}