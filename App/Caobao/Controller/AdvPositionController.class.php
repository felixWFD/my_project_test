<?php
namespace Caobao\Controller;
use       Think\Controller;

 /*
  *  广告管理
  *
  *
  */
class AdvPositionController extends MyController{
 	 public function __construct() {

        parent::__construct();
       
    }
    //添加广告位页
 	public function addCate(){
 		$this->display();
 	}
 	//执行添加广告位
 	public function doAddCate(){
 		
 		$db = M("adv_position");
 		if($db->create()){
 			$rt=$db->add();
            if($rt){
            	$this->success("添加成功",U("AdvPosition/cateList"));
            }else{
            	$this->error("添加失败");
            }
 		}else{
             $this->error("添加失败");
 		}
 	}
 	//添加广告页面
 	public function addAdv(){
 		$db = M("adv_position");
 		$result = $db->select();
 		foreach($result as &$v){
            $v["catename"]=($v['category']==1?'移动端':'电脑端').$v['name']."规格: 宽".$v['adv_width']."高".$v['adv_height'];
 		}
        $product =M("products");
        $list = $product->field("id,pro_name")->where("classify_id in(5,6) AND is_on_sale=1 AND recycle=0")->select();
        $this->assign("list",$list);
 		$this->assign("data",$result);
 		$this->display();
 	}
 	//执行添加广告
 	public function insertAdv(){
 		$_POST['thumbnail']=$this->upload();
 		$db =M("advs");

 		$_POST['add_time'] = time();
         
 		if($db->create($_POST)){
 			$rt = $db->add();
 			 if($rt){
            	$this->success("添加成功",U("AdvPosition/advList"));
            	exit;
            }
 		
             
 		}
 		$this->error("添加失败");
 		
 	}
 	//单图片上传
 	public function upload(){
 		$upload = new \Think\Upload();
 		$upload->maxSize = 3145728;
 		$upload->exts    =array('jpg','gif','png','jpeg');
 		$upload->rootPath="Public/Imgadv/"; 
 		$upload->savePath="";
		$files['Filedata'] = $_FILES['file'];
 		$info=$upload->upload($files);
 		if(!$info) {                                         
	        $this->error($upload->getError());
	    }else{                                                 // 上传成功 获取上传文件信息    
	        foreach($info as $file){ 
				return $file['savepath'].$file['savename'];
	        }
	    }
 	}
 	//广告管理
 	public function advList(){
 		 $model = M();
 		 //分页
 		 $count = $model->table("__ADVS__ as a,__ADV_POSITION__ as p")->where("a.position_id=p.id")->count();
 		 $page  = new \Think\Page($count,10);
 		 $list  = $model->field("a.id,a.position_id,a.adv_name,a.is_show,p.id as pid,p.category,p.name,p.adv_width,p.adv_height")
                        ->table("__ADVS__ as a,__ADV_POSITION__ as p")
                        ->where("a.position_id=p.id")
                        ->limit($page->firstRow.','.$page->listRows)
                        ->order('a.add_time DESC')
                        ->select();
		 $show  = $page->show();
 		 $this->assign("page",$show);
 		 $this->assign("data",$list);
 		 $this->display();
 	}

 	//编辑广告位
 	public function editAdv(){
 		$id = $_GET['id'];
 		$data = M("advs");
 		$list = $data->where("id =".$id)->find();
 		$db = M("adv_position");
 		$result = $db->select();
 		foreach($result as &$v){
            $v["catename"]=($v['category']==1?'移动端':'电脑端').$v['name']."规格: 宽".$v['adv_width']."高".$v['adv_height'];
 		}
        $product =M("products");
        $productlist = $product->field("id,pro_name")->where("classify_id in(5,6)  AND is_on_sale=1 AND recycle=0")->select();
        $this->assign("product",$productlist);
 		$this->assign("res",$result);
 		$this->assign("data",$list);
 		$this->display();
 	}
 	//执行修改保存
    public function editSave(){

    	$mdl = M("advs");
		
		//图片处理,无上传则保留原来图片,有上传则清除旧图片
		if (isset($_FILES)) {
			if (($_FILES['file']['error']) == 0) {
				$img = $this->upload();
				if($img){
					$_POST['thumbnail'] = $img;
					unlink('./Public/Imgadv/'.I('post.old_thumbnail'));
				}else{
					$_POST['thumbnail'] = I('post.old_thumbnail');
				}
			}
		}	
		
    	if($mdl->create()){
    		$rt=$mdl->save();
    		if($rt){
            	$this->success("修改成功",U("AdvPosition/advList"));
            	exit;
            }
    	}
        $this->error("修改失败");
    }
    //删除广告
    public function delAdv(){
    	$id = $_GET['id'];
    	$db = M("advs");
    	$ls = $db->where("id =".$id)->delete();

    	if($ls){
			$info = $db->field("thumbnail")->where("id =".$id)->find();
			if(!empty($info['thumbnail'])){
				unlink('Public/Imgadv/' . $info['thumbnail']);
			}
    		$this->success("删除成功",U("AdvPosition/advList"));
    	}else{
    		$this->error("删除失败");
    	}
    }
    //广告分类列表
    public function cateList(){
    	
         $db = M("adv_position");
         $count = $db->count();
         $page = new \Think\Page($count,10); 
         $list = $db->order("id ASC")->limit($page->firstRow.','.$page->listRows)->select();
 		 $this->assign("list",$list);
 		 $show  = $page->show();
 		 $this->assign("page",$show);
 		 $this->display();
    }
    //修改广告位
    public function editCate(){
    	$id = I("get.id");
    	$db = M("adv_position");
    	$list = $db->find($id);
    	$this->assign("list",$list);
    	$this->display();
    }
    //执行修改广告位
    public function cateSave(){
    	$db = M("adv_position");
    	if($db->create()){
    		$rt=$db->save();
    		if($rt){
            	$this->success("修改成功",U("AdvPosition/cateList"));
            	exit;
            }
    	}
		$this->error("修改失败");

    }
    //删除广告分类
    public function delCate(){
    	$id = I("get.id");
    	$db = M("adv_position");
    	$ls = $db->delete($id);

    	if($ls){
    		$this->success("删除成功",U("AdvPosition/cateList"));
    	}else{
    		$this->error("删除失败");
    	}
    }
    public function delall(){
    	$ids = array();
        $ids = $_GET['id'];
    	$data = implode(",",$ids);
    	$db = M("adv_position");
    	$list= $db ->where("id in (".$data.")")->delete();
    	if($list){
    		echo 1;
    	}else{
    		echo 0;
    	}
    }
 }