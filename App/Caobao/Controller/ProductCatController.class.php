<?php
namespace Caobao\Controller;
use       Think\Controller;

header('Content-Type:text/html; charset=utf-8');

class ProductCatController extends MyController
{

    /**
     * 构造函数，初始化
     * @date 2015-01-16
     */
    public function __construct()
    {		
        parent::__construct();
    }

    /**
     * 商品分类列表，获取商品列表模板
     * @date 2015-01-15
     * @return void
     */
    public function catList($fid = 0)
    {
        $modelCats = M('product_cat');
		$catList = $modelCats ->where("fid='{$fid}'") ->select();
        $this->assign('catList', $catList);
        $this->display('cat_list');
    }

    /**
     * 商品分类列表，获取子分类列表
     * @date 2015-01-15
     * @return void
     */
    public function catChildList()
    {
        if (!isset($_POST['fid'])) {
            $_POST['fid'] = 0;
        }
		$fid = $_POST['fid'];
        $modelCats = M('product_cat');
		$catList = $modelCats ->where("fid='{$fid}'") ->select();
		$data = json_encode($catList);
		echo $data;
    }
	
    /**
     * 获取添加商品分类界面
     * @return void
     */
    public function catAdd()
    {	
		$cat_id = I('get.id');

		$modelCats = M('product_cat');
		$fidcats = $modelCats->where("id=$cat_id")->find();
		$this->assign('fidcats', $fidcats);
        $this->display('cat_add');
    }

    /**
     * 执行添加商品分类操作
     * @date 2015-01-16
     * @return void
     */
    public function doCatAdd()
    {
        if (!isset($_POST['is_show'])) {
            $_POST['is_show'] = 0;
        }
        $modelProductCat = M('product_cat');
        if ($modelProductCat->create($_POST)) {
            if ($modelProductCat->add()) {
                $this->success('商品分类添加成功', U('ProductCat/catList'));
                exit;
            }
        }
        $this->error('商品类别添加失败');
    }

    /**
     * 获取修改商品分类界面
     * @return void
     */
    public function catEdit()
    {
		$getCatId = I('get.id');
		
		$modelCats = M('product_cat');
		
		$cat = $modelCats ->where("id='{$getCatId}'")->find();
		$fidcats = $modelCats->where("id=".$cat['fid'])->find();
		
		$this->assign('cat', $cat);
		$this->assign('fidcats', $fidcats);
        $this->display('cat_edit');
    }

    /**
     * 执行修改商品分类操作
     * @date 2015-01-16
     * @return void
     */
    public function doCatEdit()
    {
        if (!isset($_POST['is_show'])) {
            $_POST['is_show'] = 0;
        }
		
		//判断是否有缩略图上传,有上传则删除旧图片
		if(!empty($_POST['icon_image'])){
			unlink("./Public/Product/icon/".$_POST['org_icon_image']);        //删除旧图片
		}else{
			$_POST['icon_image'] = $_POST['org_icon_image'];
		}
		
        $modelProductCat = M('product_cat');
        if ($modelProductCat->create($_POST)) {
            if ($modelProductCat->save()) {
                $this->success('商品分类修改成功', U('ProductCat/catList'));
                exit;
            }
        }
        $this->error('商品类别修改失败');
    }

    /**
     * 执行删除商品分类操作
     * @date 2015-01-16
     * @return void
     */
    public function doCatDel()
    {
        $getCatId = I('get.id');
		
        $modelProduct = M('products');
		$modelProductCat = M('product_cat');
		
		$products = $modelProduct->where('cat_id='.$getCatId)->select();
		if(!is_array($products)){
			$cat = $modelProductCat->where("id = {$getCatId}")->find();
			$affectedRows = $modelProductCat->where("id = {$getCatId}")->delete();
			if ($affectedRows) {
				if(!empty($cat['icon_image'])){
					unlink("./Public/Product/icon/".$cat['icon_image']);        //删除图标
				}
          	    $this->success('商品分类删除成功', U('ProductCat/catList'));
			} else {
				$this->error('删除失败');
			}
			
		}else{
			$this->error('删除商品类别失败,该类别下存在商品.');
		}

    }

    /**
     * AJAX获取添加商品分类级别
     * @return void
     */
    public function postCatLevel()
    {
		$fid = I('post.fid');
		$modelCats = M('product_cat');
		
		$cats = $modelCats->field('level')->where('id='.$fid)->find();
		
		if($cats['level'] > 0){
			echo $cats['level'];
			exit;
		}else{
			echo '0';
			exit;
		}
		
    }

    /**
     * 上传图标
     * 此方法被分类添加/修改页，上传图标所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/Product/icon/"; //图片保存文件夹路径
        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
        echo $imgUrl;
    }

    /**
     * 伪代码，没有此方法多文件上传插件出错
     * @return void
     */
    public function index()
    {

    }

}
