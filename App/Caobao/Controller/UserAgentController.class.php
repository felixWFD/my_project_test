<?php
namespace  Caobao\Controller;

class UserAgentController extends MyController
{
    /**
     * 构造函数：执行本类初始化操作 
     * @date 2015-01-17
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 获取代理模板，显示代理列表
     * @date 2015-01-18 
     * @return void 
     */
    public function index()
    {
        $model      = M();
		$modelUsers = M('users');
		
		$province = I('get.province');
		$city     = I('get.city');
		$area     = I('get.area');
		$keyword  = I('get.keyword');
		
		$where = "u.id > 0";
		if($province > 0 && $province != '选择省'){
			$where .= " AND p.id =".$province;
		}
		if($city > 0 && $city != '选择市'){
			$where .= " AND c.id =".$city;
		}
		if($area > 0 && $area != '选择县/区'){
			$where .= " AND a.id =".$area;
		}
		if(!empty($keyword)){
			$where .= " AND (u.name like '%".$keyword."%' OR u.mobile like '%".$keyword."%')";
		}
        $pageCount = $model->table('__USER_AGENT__ AS u')
							->join('__ADDR_AREA__ AS a ON a.id = u.area_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->where($where)
							->count();	
        $pageSize  = 10;
        $page = new \Think\Page($pageCount, $pageSize);
        // 获取代理数据
		$agents = $model->table('__USER_AGENT__ AS u')
							->field('u.*, a.addr,c.addr as city_addr,p.addr as pro_addr')
							->join('__ADDR_AREA__ AS a ON a.id = u.area_id')
							->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
							->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
							->where($where)
							->limit($page->firstRow, $page->listRows)
							->order('u.id DESC')
							->select();
							
		//加载城市信息
        $city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$c = \Think\Area::city($city);
		
        $this->assign('page', $page->show());
		$this->assign('keyword', $keyword);
		$this->assign('type', $type);
		$this->assign('area', $area);
		$this->assign('city', $city);
		$this->assign('province', $province);
        $this->assign('agents', $agents);
		$this->assign("city", $c);
        $this->display('agent_list');
    }

    /**
     * 删除
     * @date 2015-01-18
     * @return void 
     */
    public function del ()
    {
       
    }

    /**
     * 添加
     * @date 2015-01-18
     * @return void
     */
    public function add ()
    {
		$user_id = I('get.id');
		
        $city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
        $c = \Think\Area::city($city);
        $this->assign('city', $c);
		$this->assign('user_id', $user_id);
        $this->display('agent_add');
    }

    /**
     * 执行添加操作
     * @date 2015-01-18
     * @return [type] [description]
     */
    public function doAdd()
    {	
		$users = D('Users');
		$ua    = D('UserAgent');
		$uw    = D('UserWechat');
		
        $data = array();
		$data['user_id']    = I('post.user_id');
        $data['name']       = I('post.name');
        $data['mobile']     = I('post.mobile');
        $data['area_id']    = I('post.area');
        $data['address']    = I('post.address');
        $data['image']      = I('post.image');
        $data['start_time'] = time();
		$data['end_time']   = strtotime("+12 month");
		$data['status']     = 1;

        // 添加代理表, 获取用户ID
		$rt = $ua->insertUserAgent($data);
        if($rt['status'] > 0){
			// 修改用户信息
			$users->updateUser(array('user_id'=>$data['user_id'], 'is_agent'=>1));
			
			// 朋友圈信息
			$wechat = $uw->getWechatByUserId($data['user_id']);
			if(!empty($wechat) && $wechat['fid'] > 0){
				$uw->updateWechat($wechat['id'], array('fid'=>0));   // 代理不为粉丝
			}
			
			$this->success('添加成功', 'index');exit;
        }
        $this->error('添加失败');
    }
	
    /**
     * 修改
     * @date 2015-01-18
     * @return void 
     */
    public function edit ()
    {
        $id = I('get.id');
		
		$ua    = D('UserAgent');
		$aread = D('Area');
		
		//代理信息
		$agent = $ua->getAgentByAgentId($id);

		//加载城市信息
		$area = $aread->getAreaById($agent['area_id']);
        if (!empty($area)){
            $city = array('province'=>$area['one_id'], 'city'=>$area['two_id'], 'area'=>$area['id']);
        }else{
            $city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
        }
		$c = \Think\Area::city($city);
		
        $this->assign('agent', $agent);
        $this->assign("city", $c);
        $this->display('agent_edit');
    }

    /**
     * 执行修改操作
     * @date 2015-01-18
     * @return void 
     */
    public function doEdit()
    {
        $id = I('post.id');
		
        $ua = D('UserAgent');
		
		$data = array();
		$data['name']    = I('post.name');
		$data['mobile']  = I('post.mobile');
		$data['area_id'] = I('post.area');
		$data['address'] = I('post.address');
		
		$image     = I('post.image');
		$org_image = I('post.org_image');
		if(!empty($image)){
			unlink("./Public/User/".$org_image);        //删除旧图片
			$data['image'] = $image;
		}else{
			$data['image'] = $org_image;
		}
		
        $rt = $ua->updateUserAgent($id, $data);
        if ($rt['status'] > 0) {
            $this->success('修改成功');exit;
        }
        $this->error('修改失败');
    }

    /**
     * 开启/禁用代理
     * @date 2015-01-18
     * @return void 
     */
    public function agentStatusEdit ()
    {
        $agentId = I('post.id');
		$val     = I('post.val');
		
		$ua = D('UserAgent');
		
		$rt = $ua->updateUserAgent($agentId, array('status'=>$val));
        if($rt['status'] > 0) {
            echo "1";exit;
        }
        echo "0";exit;
    }

    /**
     * 上传图片
     * 此方法被店铺修改页，上传缩略图所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/User/";                         // 图片保存文件夹路径
        $upload->maxSize = 3145728;                           // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                               // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
		//裁剪图像
		$image = new \Think\Image();
		$image->open($rootPath.$imgUrl);
		$image->thumb('1000', '1000')->save($rootPath.$imgUrl); //生产缩略图
		
        echo $imgUrl;
    }

    /**
     * 执行城市三级联动下拉框获取操作
     * @date 2015-01-18
     * @return void 
     */
    public function changeSelect()
    {
        $id = I('post.id');
		$type = I('post.type');

        // 实例化数据模型
		$modelCity = M('addr_city');
        $modelArea = M('addr_area');
        if($type == 'province'){
			$list = $modelCity->where("one_id=".$id)->select();
		}else{
			$list = $modelArea->where("two_id=".$id)->select();
		}
		
        if(!is_array($list)){
			exit;
		}
		$str = '';
		foreach($list as $item){
			$str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
		}
        echo $str;
		exit;
    }
	
}
