<?php
namespace Caobao\Controller;
use       Think\Controller;

class PlanController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * planList 方案列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$product_id = I('get.product_id');
		
		$modelPlan = M("repair_plan");

		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
				
                if ($val == '查找全部关联商品' && $key == 'product_id') {
                    continue;
                }

                if ($val == '' && $key == 'name') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		
		$count = $modelPlan->where($map)->count();
		$page = new \Think\Page($count,10,$map); 
		$list = $modelPlan->where($map)-> limit($page->firstRow.','.$page->listRows)->order('sort ASC,id ASC')->select();
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
		$this->assign('product_id', $product_id);
		
		$this->display('plan_list');
    }

    /**
     * planAdd 添加方案
     * @date 2015-01-22
     * @return void 
     */

    public function planAdd(){
			
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
    	$this->display('plan_add');
    }

    /**
     * doPlanAdd 执行添加方案
     * @date 2015-01-22
     * @return void 
     */

    public function doPlanAdd()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		
		$data = array();
		$data['product_id'] = I('post.product_id');
        $data['name'] = I('post.name');
		
		$money = I('post.money');
		if(is_array($money)){
			$data['money'] = '0';
			$data['color_money'] = json_encode(I('post.money'));
		}else{
			$data['money'] = $money;
			$data['color_money'] = '';
		}
		$data['warranty'] = I('post.warranty');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');
		
        $modelPlan = M("repair_plan");
        if($modelPlan->create($data)){
            $res=$modelPlan->add();
            if($res){
                $this->success('添加成功.' . $lastID, U('Plan/index',array('product_id'=>I('post.product_id'))));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * planEdit 修改方案
     * @date 2015-01-22
     * @return void 
     */
    public function planEdit(){
        $id = $_GET['id'];
        $modelPlan = M("repair_plan");
        $plan = $modelPlan->where("id =".$id)->find();
		if(!empty($plan['color_money'])){
			$moneys = json_decode($plan['color_money']);
			$modelColor = M("repair_color");
			$i = 0;
			foreach($moneys as $key=>$val){
				$color = $modelColor->where("id=".$key)->find();
				$moneyList[$i]['id'] = $color['id'];
				$moneyList[$i]['name'] = $color['name'];
				$moneyList[$i]['money'] = number_format($val, 2, '.', '');
				$i++;
			}
		}
		
		$this->assign("moneyList",$moneyList);
		$this->assign("plan",$plan);
		
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
        $this->display('plan_edit');
    }

    
     /**
     * doPlanEdit 执行修改方案
     * @date 2015-01-22
     * @return void 
     */
    public function doPlanEdit()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}

		$data = array();
		$data['id'] = I('post.id');
        $data['product_id'] = I('post.product_id');
        $data['name'] = I('post.name');
		
		$money = I('post.money');
		if(is_array($money)){
			$data['money'] = '0';
			$data['color_money'] = json_encode(I('post.money'));
		}else{
			$data['money'] = $money;
			$data['color_money'] = '';
		}
		$data['warranty'] = I('post.warranty');
        $data['sort'] = I('post.sort');
        $data['is_show'] = I('post.is_show');
		
        $modelPlan = M("repair_plan");
		if($modelPlan->save($data)){
			$this->success('修改成功.' . $lastID, U('Plan/index',array('product_id'=>I('post.product_id'))));
		}else{
			$this->error('修改失败！');
		}
    }

    /**
     * 执行快速修改操作
     * @return void
     */
    public function postPlanEdit()
    {
        $getId = $_POST['id'];
		$getType = $_POST['type'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		
		switch ($getType)
		{
			case 'sort':
			  $data['sort'] = $getVal;
			  break;
			case 'is_show':
			  $data['is_show'] = $getVal;
			  break;
		}

        // 修改信息
        $modelPlan = M('repair_plan');
        if ($modelPlan->create($data)) {
            $modelPlan->save();
			echo "1";
			exit;
        }else{
			echo "0";
			exit;
		}
    }
    /**
     * planDelete 删除方案
     * @date 2015-01-22
     * @return void 
     */
    public function planDelete(){
		
        $id = $_GET['id'];
        $ls = M("repair_plan")->where("id =".$id)->delete();
        if($ls){
			M("repair_relation")->where("plan_ids =".$id)->delete();   // 删除关联表相关数据
            $this->success("删除成功",U("Plan/index"));
        }else{
            $this->error("删除失败");
        }
    }

	

    /**
     * 执行商品change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changePlanSelect()
    {
        $id = I('post.id');
		$modelRepairs = M('repairs');
        $modelColor = M('repair_color');

        $color = $modelColor->where("repair_id =".$id)->order("id ASC")->select();
		$data = json_encode($color);
		echo $data;
		exit;
    }

    /**
     * planCopy 复制方案
     * @date 2015-01-22
     * @return void 
     */

    public function planCopy(){
		
		//维修商品
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
    	$this->display('plan_copy');
    }

    /**
     * doPlanCopy 执行复制方案
     * @date 2015-01-22
     * @return void 
     */

    public function doPlanCopy()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
		$product_id = I('post.product_id');
		$copy_id = I('post.copy_id');
       
        $modelPlan = M("repair_plan");
		$count = $modelPlan->where('product_id='.$copy_id)->count();
		if($count > 0){
			$this->error("复制失败,此维修商品已有相关维修方案.");
			exit;
		}
		
		$planRes = $modelPlan->where('product_id='.$product_id)->select();
		foreach($planRes as $key=>$vo){
			$data = array();
			$data['name']        = $vo['name'];
			$data['money']       = 0;
			$data['color_money'] = '';
			$data['product_id']  = $copy_id;
			$data['warranty']    = $vo['warranty'];
			$data['sort']        = $vo['sort'];
			$data['is_show']     = $vo['is_show'];
			$modelPlan->create($data);
			$rt = $modelPlan->add();
		}

        if($rt){
			$this->success('添加成功.', U('Plan/index', array('product_id'=>$copy_id)));
		}else{
			$this->error("添加失败");
		}      
    }
}