<?php
namespace Caobao\Controller;
use       Think\Controller;

class ForumController extends MyController{
	
	/**
     * 构造函数，初始化
     * @date 2016-03-19
     */
    public function __construct(){		
        parent::__construct();
    }

 	// 版块管理
 	public function forum(){
		
	  	$modelForum = M('bbs_forum');
		$map = array();
        if(!empty($_GET)){
            foreach ($_GET as $key=>$val){
                $map[$key] = array('eq', $val);
            }
        }else{
			$map['fid'] = array('eq', 0);
		}

        // 查询总记录数
        $getPageCounts = $modelForum->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize);
        $forumRes = $modelForum->where($map)->order('displayorder ASC')->limit($page->firstRow, $page->listRows)->select();
        $pageShow = $page->show();
        $this->assign('page', $pageShow);
        $this->assign('forumRes', $forumRes);
		
		// 上级版块
		$fid = I('get.fid');
		if(!empty($fid)){
			$modelForum = M('bbs_forum');
			$sup = $modelForum->where('id='.$fid)->find();
			$this->assign('sup', $sup);
		}
		
        $this->display();
 	}

 	// 添加版块页面
 	public function forumAdd(){
		
		$fid = I('get.fid');
		if(!empty($fid)){
			$modelForum = M('bbs_forum');
			$forum = $modelForum->where('id='.$fid)->find();
			$forum['level'] = $forum['level']+1;
			$this->assign('forum', $forum);
		}
		
        $this->display('forum_add');
 	}

    // 添加版块操作
    public function doAddForum(){      

        $data = array();
        $data['name']         = I('post.name');
        $data['description']  = I('post.description');
        $data['tags']         = I('post.tags');
        $data['displayorder'] = I('post.displayorder');
        $data['auditthread']  = I('post.auditthread');
        $data['modthread']    = I('post.modthread');
        $data['status']       = I('post.status');
        $data['fid']          = I('post.fid');
        $data['level']        = I('post.level');

        // 判断有没有文件上传
        if (!empty($_FILES['icon']['name'])){		
			$upload = new \Think\Upload();  // 实例化上传类
			$rootPath = "./Public/bbs/icon/"; //图片保存文件夹路径
			$upload->maxSize = 3145728;             // 设置附件上传大小
			$upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
			$upload->rootPath = $rootPath;
			$info = $upload->uploadOne($_FILES['icon']);
			$imgUrl = $info['savepath'].$info['savename'];
			$data['icon'] = $imgUrl;
        }
        // 执行添加操作
        $modelForum = M('bbs_forum');
        if ($modelForum->create($data)){
            $rt = $modelForum->add();
			if($rt){
				$this->success('版块添加成功', U('Forum/forum', array('fid'=>$data['fid'])));exit;
			}
        }
		$this->error('版块添加失败', U('Forum/forum', array('fid'=>$data['fid'])));exit;
    }

 	// 编辑版块页面
 	public function forumEdit(){
		
		$id = I('get.id');
		
		$modelForum = M('bbs_forum');
		$forum = $modelForum->where('id='.$id)->find();
		$this->assign('forum', $forum);
		
        $this->display('forum_edit');
 	}
	
	// 删除版块操作
	public function forumDelete(){
		
		$id = I('get.id');
		
		$modelForum = M('bbs_forum');
		$modelThread = M('bbs_thread');
		
		$count = $modelThread->where('fid='.$id)->count();
		if($count>0){
			$this->error('该版块存在相关主题,不允许删除', U('Forum/forum', array('fid'=>$data['fid'])));exit;
		}
		$rt = $modelForum->where('id='.$id)->delete();
		if($rt){
			$this->success('版块删除成功', U('Forum/forum', array('fid'=>$data['fid'])));exit;
		}
		$this->error('版块删除失败', U('Forum/forum', array('fid'=>$data['fid'])));exit;
	}

    // 编辑版块操作
    public function doEditForum(){      

		$fid = I('post.fid');
		
        $data = array();
		$data['id']           = I('post.id');
        $data['name']         = I('post.name');
        $data['description']  = I('post.description');
        $data['tags']         = I('post.tags');
        $data['displayorder'] = I('post.displayorder');
        $data['auditthread']  = I('post.auditthread');
        $data['modthread']    = I('post.modthread');
        $data['status']       = I('post.status');
		
		// 判断有没有文件上传
        if (!empty($_FILES['icon']['name'])){
            $upload = new \Think\Upload();  // 实例化上传类
			$rootPath = "./Public/bbs/icon/"; //图片保存文件夹路径
			$upload->maxSize = 3145728;             // 设置附件上传大小
			$upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
			$upload->rootPath = $rootPath;
			$info = $upload->uploadOne($_FILES['icon']);
			$data['icon'] = $info['savepath'].$info['savename'];
			
			unlink('./Public/bbs/icon/'.I('post.old_icon'));  //删除旧文件   
        }else{
			$data['icon'] = I('post.old_icon');
		}
       
        $modelForum = M('bbs_forum');
        if ($modelForum->create($data)){
            $rt = $modelForum->save();
			if($rt){
				$this->success('版块编辑成功', U('Forum/forum', array('fid'=>$fid)));exit;
			}
        }
		$this->error('版块编辑失败', U('Forum/forum', array('fid'=>$fid)));exit;
    }

 	// 主题管理
 	public function thread(){
		
		$fid = I('get.fid');
		$category = I('get.category');
		$subject = I('get.subject');
		$displayorder = I('get.displayorder');
		$status = I('get.status');
		
	  	$modelThread = M('bbs_thread');
		$modelForum = M('bbs_forum');
		$modelUsers = M('users');
		
		$map = array('displayorder'=>array('neq', '-1'));
        if(!empty($_GET)){
            foreach ($_GET as $key=>$val){
				if ($val == '' || $val == '主题版块' || $val == '主题类型' || $val == '显示顺序' || $val == '主题状态') {
                    continue;
                }
				if($key == 'subject'){
					$map[$key] = array('LIKE', '%' . $val . '%');
				}else{
					$map[$key] = array('eq', $val);
				}
            }
        }
		
        // 查询总记录数
        $getPageCounts = $modelThread->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize);
        $threadRes = $modelThread->where($map)->order('dateline DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($threadRes as $key=>$vo){
			$forum = $modelForum->where('id='.$vo['fid'])->find();
			$threadRes[$key]['forum_name'] = $forum['name'];
			$user = $modelUsers->where('id='.$vo['authorid'])->find();
			$threadRes[$key]['user_name'] = $user['uname'];
		}
        $pageShow = $page->show();
		$this->assign('category', $category);
		$this->assign('subject', $subject);
		$this->assign('displayorder', $displayorder);
		$this->assign('status', $status);
        $this->assign('page', $pageShow);
        $this->assign('threadRes', $threadRes);
		
		// 版块
		$modelForum = M('bbs_forum');
		$forumRes = $modelForum->select();
		$tree = new \Think\Tree($forumRes);
		$str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
		$forumString = $tree->getTree(0, $str, $fid);
		$this->assign('forumString', $forumString);
		
        $this->display();
 	}
	
	// 查看主题
	public function threadview(){
		
		$id = I('get.id');
		
		$modelThread = M('bbs_thread');
		$modelSchedule = M('bbs_thread_schedule');
		
		$thread = $modelThread->where('id='.$id)->find();
		if($thread['quote'] != '0'){
			$quote = $modelThread->field('id, subject')->where('id='.$thread['quote'])->find();
			$thread['quotesubject'] = $quote['subject'];
		}
		
		// 工具
		$thread['tools'] = $modelSchedule->where('category=1 AND tid='.$id)->order('displayorder ASC')->select();
		// 方法
		$steps = $modelSchedule->where('category=2 AND tid='.$id)->order('displayorder ASC')->select();
		foreach($steps as $key=>$vo){
			$quote = $modelThread->field('id, subject')->where('id='.$vo['quote'])->find();
			$steps[$key]['quotesubject'] = $quote['subject'];
		}
		$thread['steps'] = $steps;
		// 注意事项
		$thread['matters'] = $modelSchedule->where('category=3 AND tid='.$id)->order('displayorder ASC')->select();
		
		// 版块
		$modelForum = M('bbs_forum');
		$forumRes = $modelForum->select();
		$tree = new \Think\Tree($forumRes);
		$str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
		$forumString = $tree->getTree(0, $str, $thread['fid']);
		$this->assign('forumString', $forumString);
		
		$this->assign('thread' ,$thread);
		$this->display('thread_view');
	}
	
	// 编辑主题
	public function doEditThread(){
		
		$id = I('post.id');
		$modelThread = M('bbs_thread');
		$modelSchedule = M('bbs_thread_schedule');
		
		$thread = $modelThread->field('tags')->where('id='.$id)->find();
		
		$data['id']            = $id;
		$data['category']           = I('post.category');
        $data['fid']           = I('post.fid');
        $data['subject']       = I('post.subject');
		$data['description']   = I('post.description');
        $data['content']       = I('post.content');
				
		$tags = I('post.tags');
		$tags = str_replace(" ", ",", $tags);    //空格替换成英文逗号
		$tags = str_replace("，", ",", $tags);   //中文逗号替换成英文逗号
		$tags = str_replace("|", ",", $tags);    //竖线替换成英文逗号
        $data['tags']          = $tags;
		
        $data['displayorder']  = I('post.displayorder');
        $data['status']        = I('post.status');
		$data['original']      = I('post.original');
		
		$rt = $modelThread->data($data)->save();
		
		// 保存工具
		$tools = I('post.tools');
		if(!empty($tools)){
			$modelSchedule->where('category=1 AND tid='.$id)->delete();
			foreach($tools as $key=>$tool){
				$trt = $modelSchedule->data(array('tid'=>$id, 'category'=>1, 'content'=>$tool, 'displayorder'=>($key+1)))->add();
			}
		}
		
		// 保存方法
		$steps = I('post.step');
		$attachments = I('post.attachments');
		$quotes = I('post.quotes');
		if(!empty($steps)){
			$modelSchedule->where('category=2 AND tid='.$id)->delete();
			foreach($steps as $key=>$step){
				$srt = $modelSchedule->data(array('tid'=>$id, 'category'=>2, 'content'=>$step, 'attachment'=>$attachments[$key], 'quote'=>$quotes[$key], 'displayorder'=>($key+1)))->add();
			}
		}
		
		//保存注意事项
		$matters = I('post.matters');
		if(!empty($matters)){
			$modelSchedule->where('category=3 AND tid='.$id)->delete();
			foreach($matters as $key=>$matter){
				$mrt = $modelSchedule->data(array('tid'=>$id, 'category'=>3, 'content'=>$matter, 'displayorder'=>($key+1)))->add();
			}
		}
		
		if($rt || $trt || $srt || $mrt){
			// 添加标签表
			$this->_addtags($data['tags'], $thread['tags']);
			$this->success('主题编辑成功', U('Forum/thread', array('fid'=>$fid)));exit;
		}
		$this->error('主题编辑失败', U('Forum/thread', array('fid'=>$fid)));exit;
	}
	
	// 批量操作主题
    public function batchThread(){
        
		$option = I('post.batch_option');
		if(!isset($option)){
            $this->error('非法操作');
            exit;
        }

        if($option == -1){
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
		if(empty($getIds)){
            $this->error('非法操作');
            exit;
        }
        $where = "id in (" . implode(', ', $getIds) . ') ';
        $modelThread = M('bbs_thread');
		
        switch($option){
            case 1:
				foreach($getIds as $id){
					$thread = $modelThread->where('id='.$id)->find();
					if($thread['category'] == '0'){
						$this->_actactivity($thread['authorid'],$id,'-1',0,20);     // 经验-20
					}else{
						$this->_actactivity($thread['authorid'],$id,'-1',0,40,30);  // 经验-40  虚拟币-30
					}
					$this->_notice('0',$thread['authorid'],$id,'8');           // 审核不通过 通知消息
				}
                $affectedRows = $modelThread->where($where)->data(array('displayorder' => '-1'))->save();   //批量加入回收站
                break;
            case 2:
				foreach($getIds as $id){
					$thread = $modelThread->where('id='.$id)->find();
					if($thread['lastdate'] == '0'){
						$this->_notice('0',$thread['authorid'],$id,'7');               // 审核通过 通知消息
						if($thread['category'] == '0'){
							$this->_actactivity($thread['authorid'],$id,'1',1,20);     // 经验+20
						}else{
							$this->_actactivity($thread['authorid'],$id,'1',1,40,30);  // 经验+40  虚拟币+30
						}
						CBWBaiduPush(array(C('BBS_URL')."/view/".$id.".html"), 'bbs');    // 百度SEO推送
					}
				}
				$affectedRows = $modelThread->where($where)->data(array('displayorder' => '0'))->save();   //批量审核主题
                break;
            case 3:
				$affectedRows = $modelThread->where($where)->data(array('status' => '1'))->save();   //批量屏蔽主题
                break;
            case 4:
				$affectedRows = $modelThread->where($where)->data(array('status' => '2'))->save();   //批量警告主题
                break;
        }

        if ($affectedRows){
            $this->success('执行成功', U('Forum/thread'));
        }else{
            $this->error('执行失败', U('Forum/thread'));
        }
    }
	
	// 主题快捷修改
	public function shortcutThread(){
		
		$id = I('post.id');
		$field = I('post.field');
		$val = I('post.val');

        if ($val == ''){
            echo "0";
            exit;
        }
		
		// 修改主题信息
        $modelThread = M('bbs_thread');
		$thread = $modelThread->where('id='.$id)->find();
		
		$data['id'] = $id;
		$data[$field] = $val;
		
        if($modelThread->create($data)) {
            $rt = $modelThread->save();
			if($rt){
				if($thread['lastdate'] == '0'){
					$this->_notice('0',$thread['authorid'],$id,'7');               // 审核通过 通知消息
					if($thread['category'] == '0'){
						$this->_actactivity($thread['authorid'],$id,'1',1,20);     // 经验+20
					}else{
						$this->_actactivity($thread['authorid'],$id,'1',1,40,30);  // 经验+40  虚拟币+30
					}
					CBWBaiduPush(array(C('BBS_URL')."/view/".$id.".html"), 'bbs');    // 百度SEO推送
				}
				echo "1";exit;
			}
        }
		echo "0";exit;
	}
	
	// 主题移动到回收站
	public function addThreadToRecycle(){
		
		$id = I('get.id');
        $modelThread = M('bbs_thread');
        $rt = $modelThread->where("id='{$id}'")->data(array('displayorder' => '-1'))->save();

        if ($rt) {
			// 审核不通过 通知消息
			$thread = $modelThread->where('id='.$id)->find();
			if($thread['category'] == '0'){
				$this->_actactivity($thread['authorid'],$id,'-1',0,20);     // 经验-20
			}else{
				$this->_actactivity($thread['authorid'],$id,'-1',0,40,30);  // 经验-40  虚拟币-30
			}
			$this->_notice('0',$thread['authorid'],$id,'8');
			
			// 相关帖子帖子审核状态:-1主题帖在回收站中
			$modelPost = M('bbs_post');
			$modelPost->where('tid='.$id)->save(array('invisible'=>'-1'));
			
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('主题（ID:' . $id . '）已成功加入回收站');
            $this->success('主题已加入回收站');
        } else {
            $this->error('操作失败');
        }
	}
	
	// 帖子列表
	public function post(){
		
		$tid = I('get.tid');
		$invisible = I('get.invisible');
		$message = I('get.message');
		$status = I('get.status');
		
	  	$modelPost = M('bbs_post');
		$modelThread = M('bbs_thread');
		$modelUsers = M('users');
		
		$map = array();
        if(!empty($_GET)){
            foreach ($_GET as $key=>$val){
				if ($val == '' || $val == '帖子审核状态' || $val == '帖子状态') {
                    continue;
                }
				if($key == 'message'){
					$map[$key] = array('LIKE', '%' . $val . '%');
				}else{
					$map[$key] = array('eq', $val);
				}
            }
        }
		
        // 查询总记录数
        $getPageCounts = $modelPost->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize);
        $postRes = $modelPost->where($map)->order('dateline DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($postRes as $key=>$vo){
			$thread = $modelThread->where('id='.$vo['tid'])->find();
			$postRes[$key]['subject'] = $thread['subject'];
			$user = $modelUsers->where('id='.$vo['authorid'])->find();
			$postRes[$key]['user_name'] = $user['uname'];
		}
        $pageShow = $page->show();
		$this->assign('tid', $tid);
		$this->assign('invisible', $invisible);
		$this->assign('message', $message);
		$this->assign('status', $status);
        $this->assign('page', $pageShow);
        $this->assign('postRes', $postRes);
		
        $this->display();
	}
	
	// 查看帖子
	public function postview(){
		
		$id = I('get.id');
		
		$modelThread = M('bbs_thread');
		$modelPost = M('bbs_post');
		
		$post = $modelPost->where('id='.$id)->find();
		$thread = $modelThread->where('id='.$post['tid'])->find();
		if($post['quote'] != '0'){
			$quote = $modelThread->field('id, subject')->where('id='.$post['quote'])->find();
			$post['quotesubject'] = $quote['subject'];
		}
		$this->assign('post' ,$post);
		$this->assign('thread' ,$thread);
		$this->display('post_view');
	}
	
	// 编辑帖子
	public function doEditPost(){
		
		$id = I('post.id');
		$tid = I('post.tid');
		$satisfy = I('post.satisfy');
		
		$modelPost = M('bbs_post');
		$post = $modelPost->where('id='.$id)->find();
		if($satisfy != $post['satisfy']){
			if($satisfy == '1'){
				//当前帖子被采纳 经验+40 虚拟币+40
				$this->_actactivity($post['authorid'],$tid,'4',1,40,40);
				
				//获取之前被采纳  经验-40 虚拟币-40
				$satisfypost = $modelPost->where('satisfy=1 AND tid='.$tid)->find();
				if(!empty($satisfypost)){
					$this->_actactivity($satisfypost['authorid'],$tid,'-4',0,40,40);
				}
			}else{
				//经验-40 虚拟币-40
				$this->_actactivity($post['authorid'],$tid,'-4',0,40,40);
			}
			$modelPost->where('tid='.$tid)->save(array('satisfy'=>'0'));
		}
			
		$data['id']            = $id;
        $data['message']       = I('post.message');
        $data['invisible']     = I('post.invisible');
		$data['satisfy']       = $satisfy;
        $data['status']        = I('post.status');
       
        if ($modelPost->create($data)){
            $rt = $modelPost->save();
			if($rt){
				$this->success('帖子编辑成功', U('Forum/post', array('tid'=>$tid)));exit;
			}
        }
		$this->error('帖子编辑失败', U('Forum/post', array('tid'=>$tid)));exit;
	}
	
	// 帖子删除
	public function removePost(){
		
		$id = I('get.id');
		$modelPost = M('bbs_post');
		$modelComments = M('bbs_comments');
		
		$post = $modelPost->where('id='.$id)->find();
		
		$rt = $modelPost->where('id='.$id)->delete();       //删除帖子
        if ($rt) {
			
			// 删除图片
			if(!empty($post['attachment'])){
				unlink('Public/bbs/attachment/' . $post['attachment']);
			}
			
			$this->_notice('0',$post['authorid'],$post['tid'],'8',1);                    // 审核不通过 通知消息
			
			// 主题回复次数-1
			$modelThread = M('bbs_thread');
			$modelThread->where('id='.$post['tid'])->setDec('replies',1);
			
			// 删除评论表与帖子关联数据
			$modelComments->where("category=1 AND acceptid=".$id)->delete();
			
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('帖子（ID:' . $id . '）已删除');
            $this->success('删除帖子成功');
        } else {
            $this->error('操作失败');
        }
	}
	
	// 批量操作帖子
    public function batchPost(){
        
		$option = I('post.batch_option');
		if(!isset($option)){
            $this->error('非法操作');
            exit;
        }

        if($option == -1){
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
		if(empty($getIds)){
            $this->error('非法操作');
            exit;
        }
        $where = "id in (" . implode(', ', $getIds) . ') ';
        $modelPost = M('bbs_post');

        switch($option){
            case 1:
				$modelComments = M('bbs_comments');
				foreach($getIds as $id){
					$post = $modelPost->where('id='.$id)->find();
					
					// 删除图片
					if(!empty($post['attachment'])){
						unlink('Public/bbs/attachment/' . $post['attachment']);
					}
					
					$this->_notice('0',$post['authorid'],$post['tid'],'8',1);                    // 审核不通过 通知消息
					
					// 主题回复次数-1
					$modelThread = M('bbs_thread');
					$modelThread->where('id='.$post['tid'])->setDec('replies',1);
					
					// 删除评论表与帖子关联数据
					$modelComments->where("category=1 AND acceptid=".$id)->delete();
				}
				$affectedRows = $modelPost->where($where)->delete();                                //批量删除帖子
                break;
            case 2:
				foreach($getIds as $id){
					$post = $modelPost->where('id='.$id)->find();

					if($post['lastdate'] == '0'){
						$this->_actactivity($post['authorid'],$post['tid'],'2',1,10,5);           // 经验+10 虚拟币+5
					}
					$this->_notice('0',$post['authorid'],$post['id'],'7',1);                    // 审核通过 通知消息
					
					// 短信与邮件通知
					$modelThread = M('bbs_thread');
					$thread = $modelThread->field('id, subject, authorid')->where('id='.$post['tid'])->find();
					$notice = array(
						'tid'=>$thread['id'],
						'userid'=>$thread['authorid'],
						'subject'=>$thread['subject'],
						'message'=>$post['message']
					);
					$this->_smsnotice($notice);
				}
				$affectedRows = $modelPost->where($where)->data(array('invisible' => '0'))->save();   //批量审核帖子
                break;
            case 3:
				$affectedRows = $modelPost->where($where)->data(array('status' => '1'))->save();   //批量屏蔽帖子
                break;
            case 4:
				$affectedRows = $modelPost->where($where)->data(array('status' => '2'))->save();   //批量警告帖子
                break;
        }

        if ($affectedRows){
            $this->success('执行成功', U('Forum/post'));
        }else{
            $this->error('执行失败', U('Forum/post'));
        }
    }
	
	// 帖子快捷修改
	public function shortcutPost(){
		
		$id = I('post.id');
		$field = I('post.field');
		$val = I('post.val');

        if ($val == ''){
            echo "0";
            exit;
        }
		
		$data['id'] = $id;
		$data[$field] = $val;

        // 修改帖子信息
        $modelPost = M('bbs_post');
		$post = $modelPost->where('id='.$id)->find();
        if ($modelPost->create($data)) {
            $rt = $modelPost->save();
			if($rt){
				if($post['lastdate'] == '0'){
					$this->_actactivity($post['authorid'],$post['tid'],'2',1,10,5);           // 经验+10 虚拟币+5
				}
				$this->_notice('0',$post['authorid'],$post['id'],'7',1);                    // 审核通过 通知消息
				// 短信与邮件通知
				$modelThread = M('bbs_thread');
				$thread = $modelThread->field('id, subject, authorid')->where('id='.$post['tid'])->find();
				$notice = array(
					'tid'=>$thread['id'],
					'userid'=>$thread['authorid'],
					'subject'=>$thread['subject'],
					'message'=>$post['message']
				);
				$this->_smsnotice($notice);
			}
			echo "1";exit;
        }
		echo "0";exit;
	}
	
	// 评论列表
	public function comments(){
		
		$category = I('get.category');
		$acceptid = I('get.acceptid');
		$content = I('get.content');
		$status = I('get.status');
		
		$modelThread = M('bbs_thread');
	  	$modelPost = M('bbs_post');
		$modelComments = M('bbs_comments');
		$modelUsers = M('users');
		
		$map = array();
        if(!empty($_GET)){
            foreach ($_GET as $key=>$val){
				if ($val == '' || $val == '评论类型' || $val == '评论状态') {
                    continue;
                }
				if($key == 'content'){
					$map[$key] = array('LIKE', '%' . $val . '%');
				}else{
					$map[$key] = array('eq', $val);
				}
            }
        }
		
        // 查询总记录数
        $getPageCounts = $modelComments->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize);
        $commentRes = $modelComments->where($map)->order('dateline DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($commentRes as $key=>$vo){
			if($vo['category'] == '0'){
				$thread = $modelThread->where('id='.$vo['acceptid'])->find();
				$commentRes[$key]['subject'] = $thread['subject'];
			}else{
				$post = $modelPost->where('id='.$vo['acceptid'])->find();
				$thread = $modelThread->where('id='.$post['tid'])->find();
				$commentRes[$key]['subject'] = $thread['subject'];
			}
			
			$user = $modelUsers->where('id='.$vo['authorid'])->find();
			$commentRes[$key]['user_name'] = $user['uname'];
		}
        $pageShow = $page->show();
		$this->assign('category', $category);
		$this->assign('acceptid', $acceptid);
		$this->assign('content', $content);
		$this->assign('status', $status);
        $this->assign('page', $pageShow);
        $this->assign('commentRes', $commentRes);
		
        $this->display();
	}
	
	// 查看评论
	public function commentview(){
		
		$id = I('get.id');
		
		$modelComments = M('bbs_comments');
		$comment = $modelComments->where('id='.$id)->find();
		
		$this->assign('comment' ,$comment);
		$this->display('comment_view');
	}
	
	// 编辑评论
	public function doEditComments(){
		
		$id = I('post.id');
		$modelComments = M('bbs_comments');
		
		$data['id']            = $id;
        $data['content']       = I('post.content');
        $data['status']        = I('post.status');
       
        if ($modelComments->create($data)){
            $rt = $modelComments->save();
			if($rt){
				$this->success('评论编辑成功', U('Forum/comments'));exit;
			}
        }
		$this->error('评论编辑失败', U('Forum/comments'));exit;
	}
	
	// 评论删除
	public function removeComments(){
		
		$id = I('get.id');
		$modelComments = M('bbs_comments');
		$comment = $modelComments->where('id='.$id)->find();
		$rt = $modelComments->where('id='.$id)->delete();       //删除评论
        if ($rt) {
			
			if($comment['category'] == '1'){
				$modelPost = M('bbs_post');
				$post = $modelPost->where('id='.$comment['acceptid'])->find();
				$tid = $post['tid'];
			}else{
				$tid = $comment['acceptid'];
			}
			$this->_actactivity($comment['authorid'],$tid,'-3',0,5,5);  // 经验-5 虚拟币-5
			
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('评论（ID:' . $id . '）已删除');
            $this->success('删除评论成功');
        } else {
            $this->error('操作失败');
        }
	}
	
	// 批量操作评论
    public function batchComments(){
        
		$option = I('post.batch_option');
		if(!isset($option)){
            $this->error('非法操作');
            exit;
        }

        if($option == -1){
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
		if(empty($getIds)){
            $this->error('非法操作');
            exit;
        }
        $where = "id in (" . implode(', ', $getIds) . ') ';
		$modelPost = M('bbs_post');
        $modelComments = M('bbs_comments');

        switch($option){
            case 1:
				foreach($getIds as $id){
					$comment = $modelComments->where('id='.$id)->find();
					if($comment['category'] == '1'){
						$post = $modelPost->where('id='.$comment['acceptid'])->find();
						$tid = $post['tid'];
					}else{
						$tid = $comment['acceptid'];
					}
					$this->_actactivity($comment['authorid'],$tid,'-3',0,5,5);   // 经验-5 虚拟币-5
				}
				$affectedRows = $modelComments->where($where)->delete();                                //批量删除评论
                break;
        }

        if ($affectedRows){
            $this->success('执行成功', U('Forum/comments'));
        }else{
            $this->error('执行失败', U('Forum/comments'));
        }
    }

 	// 主题回收站
 	public function recycle(){
		
		$fid = I('get.fid');
		$category = I('get.category');
		$subject = I('get.subject');
		
	  	$modelThread = M('bbs_thread');
		$modelForum = M('bbs_forum');
		$modelUsers = M('users');
		
		$map = array('displayorder'=>array('eq', '-1'));
        if(!empty($_GET)){
            foreach ($_GET as $key=>$val){
				if ($val == '' || $val == '请选择版块' || $val == '请选择类型') {
                    continue;
                }
				if($key == 'subject'){
					$map[$key] = array('LIKE', '%' . $val . '%');
				}else{
					$map[$key] = array('eq', $val);
				}
            }
        }
		
        // 查询总记录数
        $getPageCounts = $modelThread->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);
        $threadRes = $modelThread->where($map)->order('dateline DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($threadRes as $key=>$vo){
			$forum = $modelForum->where('id='.$vo['fid'])->find();
			$threadRes[$key]['forum_name'] = $forum['name'];
			$user = $modelUsers->where('id='.$vo['authorid'])->find();
			$threadRes[$key]['user_name'] = $user['uname'];
		}
        $pageShow = $page->show();
		$this->assign('category', $category);
		$this->assign('subject', $subject);
        $this->assign('page', $pageShow);
        $this->assign('threadRes', $threadRes);
		
		// 版块
		$modelForum = M('bbs_forum');
		$forumRes = $modelForum->select();
		$tree = new \Think\Tree($forumRes);
		$str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
		$forumString = $tree->getTree(0, $str, $fid);
		$this->assign('forumString', $forumString);
		
        $this->display();
 	}

    // 将主题从回收站移出
    public function removeThreadToRecycle(){
		
		$id = I('get.id');
        $modelThread = M('bbs_thread');
        $rt = $modelThread->where("id='{$id}'")->data(array('displayorder' => '0'))->save();

        if ($rt) {
			// 相关帖子帖子审核状态:0已审核通过
			$modelPost = M('bbs_post');
			$modelPost->where('tid='.$id)->save(array('invisible'=>'0'));
			
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('主题（ID:' . $id . '）已从回收站移出');
            $this->success('主题移出回收站');
        } else {
            $this->error('操作失败');
        }
    }

    // 主题删除
    public function removeThread(){
		
		$id = I('get.id');
        $modelThread = M('bbs_thread');
		$modelSchedule = M('bbs_thread_schedule');
		$modelPost = M('bbs_post');
		$modelComments = M('bbs_comments');
		
		$thread = $modelThread->where('id='.$id)->find();
		
		$rt = $modelThread->where('id='.$id)->delete();       //删除主题
        if ($rt) {
			// 删除attachment图片
			if(!empty($thread['attachment'])){
				unlink('Public/bbs/attachment/'.$thread['attachment']);
			}
			
			//删除附表
			$schedules = $modelSchedule->where('tid='.$id)->select();
			foreach($schedules as $vo){
				if(!empty($vo['attachment'])){
					unlink('Public/bbs/attachment/'.$vo['attachment']);
				}
			}
			$modelSchedule->where('tid='.$id)->delete();
					
			// 删除标签
			$this->_deltags($thread['tags']);
			
			// 删除帖子表关联数据
			$postRes = $modelPost->where('tid='.$id)->select();
			if(!empty($postRes)){
				foreach($postRes as $vo){
					// 删除评论表与帖子关联数据
					$comments = $modelComments->where("category=1 AND acceptid=".$vo['id'])->count();
					if($comments > 0){
						$modelComments->where("category=1 AND acceptid=".$vo['id'])->delete();     
					}
				}
				$modelPost->where('tid='.$id)->delete();
			}
			
			// 删除评论表与主题关联数据
			$comments = $modelComments->where('category=0 AND acceptid='.$id)->count();
			if($comments > 0){
				$modelComments->where('category=0 AND acceptid='.$id)->delete();     
			}
			
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('主题（ID:' . $id . '）已删除');
            $this->success('删除主题成功');
        } else {
            $this->error('操作失败');
        }
    }
	
	// 批量操作回收站的主题
    public function batchRecycle(){
        
		$option = I('post.batch_option');
		if(!isset($option)){
            $this->error('非法操作');
            exit;
        }

        if($option == -1){
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
		if(empty($getIds)){
            $this->error('非法操作');
            exit;
        }
        $where = "id in (" . implode(', ', $getIds) . ') ';
		
        $modelThread = M('bbs_thread');
		$modelSchedule = M('bbs_thread_schedule');

        switch($option){
            case 1:
                $affectedRows = $modelThread->where($where)->data(array('displayorder' => '0'))->save();   //批量移出主题
                break;
            case 2:
				$modelPost = M('bbs_post');
				$modelComments = M('bbs_comments');
 
				foreach($getIds as $id){
					$thread = $modelThread->field('attachment,tags')->where('id='.$id)->find();
					
					// 删除attachment图片
					if(!empty($thread['attachment'])){
						unlink('Public/bbs/attachment/'.$thread['attachment']);
					}
					
					//删除附表
					$schedules = $modelSchedule->where('tid='.$id)->select();
					foreach($schedules as $vo){
						if(!empty($vo['attachment'])){
							unlink('Public/bbs/attachment/'.$vo['attachment']);
						}
					}
					$modelSchedule->where('tid='.$id)->delete();
			
					// 删除帖子表关联数据
					$postRes = $modelPost->where('tid='.$id)->select();
					if(!empty($postRes)){
						foreach($postRes as $vo){
							// 删除评论表与帖子关联数据
							$comments = $modelComments->where("category=1 AND acceptid=".$vo['id'])->count();
							if($comments > 0){
								$modelComments->where("category=1 AND acceptid=".$vo['id'])->delete();     
							}
						}
						$modelPost->where('tid='.$id)->delete();     
					}
					
					// 删除评论表与主题关联数据
					$comments = $modelComments->where('category=0 AND acceptid='.$id)->count();
					if($comments > 0){
						$modelComments->where('category=0 AND acceptid='.$id)->delete();     
					}
					
					// 删除标签
					$this->_deltags($thread['tags']);
				}
				$affectedRows = $modelThread->where($where)->delete();       //批量删除主题
                break;
        }

        if ($affectedRows){
            $this->success('执行成功', U('Forum/recycle'));
        }else{
            $this->error('执行失败', U('Forum/recycle'));
        }
    }
	
	/* 用户论坛操作,经验值与草包豆变更函数
	 * $userid 用户ID
	 * $threadid 主题ID
	 * $act 动作类别(-5=取消投票;-4=取消采纳;-3=删除评论; -2=删除回复;-1=删除发布;1=发布; 2=回复;3=写评论;4=采纳;5=投票;)
	 * $category 增减 1=增  0=减
	 * $exp 经验值  发表/分享=20/40 回答=10 评论=5 采纳=10 被采纳=40 投票=5
	 * $virtual 虚拟币  回答=5 评论=5 被采纳=40 分享技巧=30 被推荐=60
	 * return void
	 */
	private function _actactivity($userid, $threadid, $act=1, $category=1, $exp=5, $virtual=0){
		
		$modelUsers = M('user_info');
		$modelActivity = M('bbs_activity');
		$modelVirtual = M('user_virtual');
		
		$users = $modelUsers->field('virtual_money,exp')->where('user_id='.$userid)->find();   //获取用户目前的经验值与虚拟币

        $nowtime = strtotime(date('Y-').date('m-').date('d ').'00:00:00');  // 今天零时的时间
		$exp_add = $modelActivity->where('authorid='.$userid.' AND category=1 AND dateline>'.$nowtime)->sum('exp');          //今天增加的经验值
		$exp_del = $modelActivity->where('authorid='.$userid.' AND category=0 AND dateline>'.$nowtime)->sum('exp');          //今天扣除的经验值
		$virtual_add = $modelActivity->where('authorid='.$userid.' AND category=1 AND dateline>'.$nowtime)->sum('virtual');  //今天增加的虚拟币
		$virtual_del = $modelActivity->where('authorid='.$userid.' AND category=0 AND dateline>'.$nowtime)->sum('virtual');  //今天扣除的虚拟币
		
		if($category){
			$user_exp = $users['exp']+$exp;
			$sum_exp = ($exp_add - $exp_del)+$exp;
			$new_exp = $exp;
			if($sum_exp > 500){    //今天获取的经验值不能超过500
				$new_exp = $exp-($sum_exp-500);
				$user_exp = $users['exp']+$new_exp;
			}
			
			$user_virtual = $users['virtual_money']+$virtual;
			$sum_virtual = ($virtual_add - $virtual_del)+$virtual;
			$new_virtual = $virtual;
			if($sum_virtual > 500){    //今天获取的虚拟币不能超过500
				$new_virtual = $virtual-($sum_virtual-500);
				$user_virtual = $users['virtual_money']+$new_virtual;
			}
		}else{
			$user_exp = $users['exp']-$exp;
			$new_exp = $exp;
			
			$user_virtual = $users['virtual_money']-$virtual;
			$new_virtual = $virtual;
			if($user_virtual < 0){    //如果用户虚拟币小于0 
				$new_virtual = $virtual-($virtual-$users['virtual_money']);
				$user_virtual = $users['virtual_money']-$new_virtual;
			}
		}
		
		$data['authorid'] = $userid;
		$data['tid']      = $threadid;
		$data['act']      = $act;
		$data['category'] = $category;
		$data['exp']      = $new_exp;
		$data['virtual']  = $new_virtual;
		$data['dateline'] = time();
		
		if($modelActivity->create($data)){
			$rt = $modelActivity->add();
			if($rt){
				$modelUsers->where('user_id='.$userid)->save(array('exp'=>$user_exp, 'virtual_money'=>$user_virtual));  // 保存用户信息表经验值
				
				//添加虚拟币记录
				if($new_virtual > 0){
					$virtuals['user_id'] = $userid;
					$virtuals['type']    = $category;
					$virtuals['val']     = $new_virtual;
					$virtuals['surplus'] = $user_virtual;
					$virtuals['event']   = 7;
					$virtuals['sn']      = $act;
					$virtuals['create_time'] = time();
					$modelVirtual->create($virtuals);
					$modelVirtual->add();
				}
				return $rt;exit;
			}
		}
		return 0;exit;
	}
	
	/* 用户论坛通知操作
	 * $userid 发起用户ID
	 * $userid 接收用户ID
	 * $relationid 关联主题ID
	 * $act 动作类别(1=回复;2=评论;3=采纳;4=取消采纳;5=投票;6=取消投票;7=通过审核;8=不通过审核;)
	 * $category 类别(0=主题;1=帖子;2=评论;)
	 * return void
	 */
	private function _notice($userid, $receiveid, $relationid, $act=1, $category=0){
		
		$modelNotice = M('bbs_notice');
		
		$data['authorid']   = $userid;
		$data['receiveid']  = $receiveid;
		$data['category']   = $category;
		$data['relationid'] = $relationid;
		$data['act']        = $act;
		$data['dateline']   = time();
		
		if($modelNotice->create($data)){
			$rt = $modelNotice->add();
			if($rt){
				return $rt;exit;
			}
		}
		return 0;exit;
	}
	
	/* 短信与邮件通知
	 * $sms 参数数组
	 * return void
	 */
	private function _smsnotice($notice){
		
		$modelUsers = M('users');
		$user = $modelUsers->field('email, mobile')->where('id='.$notice['userid'])->find();
		
		// 发送短信
		if(!empty($user['mobile'])){
			//初始化
			$sms = new \Think\Sms();
			// 发送模板短信
			$sms->sendSMS($user['mobile'],'post',array('subject'=>$notice['subject'], 'id'=>$notice['tid']));
		}
		
		//发送邮件
		if(!empty($user['email'])){
			$code = '<p>问:'.$notice['subject'].'</p><p>答:'.$notice['message'].'</p>';
			$url = C('BBS_URL').'/view/'.$notice['tid'].'.html';
			sendEmail($user['email'], 'email_send_notice', $url, $code);
		}
	}
	
	/* 用户修改主题时,添加标签
	 * $tag 标签
	 * $old 以前的标签
	 * return void
	 */
	private function _addtags($tag, $old = ''){
		
		$modelTags = M('bbs_tags');
		
		if(!empty($old)){
			$olds = explode(",", $old);
			if(!empty($olds)){
				foreach($olds as $vo){
					$modelTags->where("name='".$vo."'")->setDec('inputs',1);  // 减少
				}
			}
		}
		
		$tags = explode(",", $tag);
		if(!empty($tags)){
			foreach($tags as $vo){
				$count = $modelTags->where("name='".$vo."'")->count();
				if($count > 0){
					$modelTags->where("name='".$vo."'")->setInc('inputs',1);
				}else{
					$data = array();
					$data['name'] = $vo;
					$data['inputs'] = 1;
					$modelTags->create($data);
					$rt = $modelTags->add();
				}
			}
			if($rt){
				return $rt;exit;
			}
		}
		return false;exit;
	}
	
	/* 用户删除主题时,删除标签
	 * $tag 标签
	 * return void
	 */
	private function _deltags($tag){
		
		$modelTags = M('bbs_tags');
		
		if(!empty($tag)){
			$tags = explode(",", $tag);
			if(!empty($tags)){
				foreach($tags as $vo){
					$modelTags->where("name='".$vo."'")->setDec('inputs',1);  // 减少
				}
			}
		}
		return true;exit;
	}

}