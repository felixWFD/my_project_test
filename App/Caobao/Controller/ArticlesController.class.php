<?php   
namespace Caobao\Controller;
use       Think\Controller;

    /**
     * 文章管理
     * 
     * @author: Sun
     * @date: 2015-01-12
     */

class ArticlesController extends MyController{
     public function __construct() {

        parent::__construct();
       
    }

    /**
     * 文章类别分类列表 Category article_cat and display article_cat
     * @date 2015-01-22
     * @return void 
     */
    public function categoryList(){
        
         $db = M("article_cat");
         $count = $db->count();
         $page = new \Think\Page($count,10); 
         $list = $db -> limit($page->firstRow.','.$page->listRows)->order("sort ASC")->select();
         $this->assign("list",$list);
         $show  = $page->show();
         $this->assign("page",$show);
         $this->display();
    }

    /**
     * 添加文章类别 display adding article category
     * @date 2015-01-22
     * @return void 
     */
    public function addArticlesCategory(){
        $this->display();
    }

    /**
     * 执行添加文章类别 display adding article category
     * @date 2015-01-22
     * @return void 
     */
    public function addCategory(){        
        $db = M("article_cat");
        if($db->create()){
            $res=$db->add();
            if($res){
                $this->success("添加成功");
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }
        
    }

    /**
     * 修改文章类别 editCategory as
     * @date 2015-01-22
     * @return void 
     */
    public function editCategory(){
        $id = $_GET['id'];
        $data = M("article_cat");
        $list = $data->where("id =".$id)->find();
        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行修改文章类别
     * @date 2015-01-22
     * @return void 
     */
    public function categoryUpdate(){
        $db = M("article_cat");
        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("Articles/categoryList"));
                exit;
            }
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除文章类别
     * @date 2015-01-22
     * @return void 
     */
    public function delCategory(){
        $id = $_GET['id'];
        $db = M("article_cat");
        $ls = $db->where("id =".$id)->delete();

        if($ls){
            $this->success("删除成功",U("Articles/categoryList"));
        }else{
            $this->error("删除失败");
        }
    }

    public function del(){
        $ids = array();
        $ids = $_GET['id'];
        $data = implode(",",$ids);
        $db = M("article_cat");
        $list= $db ->where("id in (".$data.")")->delete();
        if($list){
            echo 1;
        }else{
            echo 0;
        }
    }

    /**
     * 添加文章
     * @date 2015-01-22
     * @return void 
     */
    public function addArticles(){
		$db = M("article_cat");
        $list = $db ->select();
      
        $this->assign("list",$list);
        $this->display();
    }

    /**
     * 执行添加文章
     * @date 2015-01-22
     * @return void 
     */
    public function doAddArticles(){
        
        $_POST['create_time']= time();

        $db = M("article_detail");
        if($db->create()){
            $res=$db->add();
            if($res){
                $this->success("添加成功",U("Articles/articlesList"));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      
    }

    /**
     * 修改文章
     * @date 2015-01-22
     * @return void 
     */
    public function editArticles(){
        $id = $_GET['id'];
        $data = M("article_detail");
        $list = $data->where("id =".$id)->find();       

		$db = M("article_cat");
        $catlist = $db ->select();
	
        $this->assign("list",$list);
		$this->assign("catlist",$catlist);
        $this->display();

    }

    /**
     * 文章List
     * @date 2015-01-22
     * @return void 
     */
    public function articlesList(){
		
		//文章分类
		$modelCat = M('article_cat');
		$catList = $modelCat->where('is_show=1')->select();
		$this->assign("catList",$catList);
		
		$modelArticles = M('article_detail');
		
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部文章' && $key == 'cat_id') {
                    continue;
                }

                if ($val == '' && $key == 'title') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		
		$count = $modelArticles->where($map)->count();
        $page = new \Think\Page($count,15); 
		
		$list = $modelArticles->where($map)
                        ->limit($page->firstRow.','.$page->listRows)
                        ->order('create_time DESC')
                        ->select();
		foreach($list as $key=>$item){
			$cat = $modelCat->where("id=".$item['cat_id'])->find();
			$list[$key]['cat_name'] = $cat['cat_name'];
		}
        $this->assign("list",$list);
        $show  = $page->show();
        $this->assign("page",$show);
        $this->display();

    }

    /**
     * 执行修改文章
     * @date 2015-01-22
     * @return void 
     */
    public function updateArticles(){
	
		$db = M("article_detail");

        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("Articles/articlesList"));
                exit;
            }else{
				$this->error("提交表单无修改");
			}
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除文章
     * @date 2015-01-22
     * @return void 
     */
    public function delArticles(){
        $id = $_GET['id'];
        $db = M("article_detail");
        $ls = $db->where("id =".$id)->delete();
        if($ls){
            $this->success("删除成功",U("Articles/articlesList"));
        }else{
            $this->error("删除失败");
        }
    }


}
