<?php
namespace Caobao\Controller;
use         Think\Controller;

class RelationController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * relationList 维修关联列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		
		$product_id = I('get.product_id');
		
		$modelRelation = M("repair_relation");
		
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
				
                if ($val == '查找全部关联商品' && $key == 'product_id') {
                    continue;
                }
                $map[$key] = $val;
            }
        }
	
		$count = $modelRelation->where($map)->count();
		$page = new \Think\Page($count,10,$map); 
		$list = $modelRelation->where($map)->limit($page->firstRow.','.$page->listRows)->order("id ASC")->select();
		
		$modelRepairs = M("repairs");
		$modelFault = M("repair_fault");
		$modelPlan = M("repair_plan");
		foreach($list as $key=>$item){
			$repairs= $modelRepairs->field("pro_name")->where("id=".$item['product_id'])->find();
			$list[$key]['pro_name'] = $repairs['pro_name'];
			$fault= $modelFault->field("name")->where("id=".$item['fault_id'])->find();
			$list[$key]['name'] = $fault['name'];
			if(!empty($item['plan_ids'])){
				$plans = $modelPlan->where("id in(".$item['plan_ids'].")")->select();
				foreach($plans as $p){
					$list[$key]['plan_names'] .= "<span class='span3'>".$p['name']."</span>";
				}
			}
		}
		
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		
		//维修商品列表
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
		$this->assign('product_id', $product_id);
		
		$this->display('relation_list');
    }

    /**
     * relationAdd 添加维修关联
     * @date 2015-01-22
     * @return void 
     */

    public function relationAdd(){
		
		$modelRepairs = M('repairs');
		
		$modelRepairs = M('repairs');
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
    	$this->display('relation_add');
    }

    /**
     * doRelationAdd 执行添加维修关联
     * @date 2015-01-22
     * @return void 
     */

    public function doRelationAdd()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}

		$data['product_id'] = I('post.productid');
        $data['fault_id'] = I('post.faultid');
        $data['plan_ids'] = I('post.planids');
        
        $modelRelation = M("repair_relation");
        if($modelRelation->create($data)){
            $res=$modelRelation->add();
            if($res){
                $this->success('添加成功.' . $lastID, U('Relation/index/product_id/'.I('post.productid')));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * relationEdit 修改维修关联
     * @date 2015-01-22
     * @return void 
     */
    public function relationEdit(){
        
		$id = $_GET['id'];
		
		$modelFault = M('repair_fault');
		$modelRelation = M('repair_relation');
		$modelPlan = M('repair_plan');
		$modelRepairs = M('repairs');
		
		$relation = $modelRelation->where("id =".$id)->find();  
		
		//商品故障
		$faults = $modelFault->where("is_show=1 AND product_id=".$relation['product_id'])->order("sort desc")->select();
		foreach($faults as $key=>$item){
			$repair = $modelRepairs->where("id=".$item['product_id'])->find();
			$faults[$key]['name'] = $repair['pro_name']." - ".$item['name'];
		}
		$this->assign('faultList', $faults);

        
		
		//维修方案初始化
		$ids = explode(",",$relation['plan_ids']);
		$plans = $modelPlan->field("id,name")->where("product_id=".$relation['product_id'])->select();
		$i=0;
		foreach($plans as $item){
			$planList[$i]['id'] = $item['id'];
			$planList[$i]['name'] = $item['name'];
			if(in_array($item['id'],$ids)){
				$planList[$i]['selected'] = '1';
			}else{
				$planList[$i]['selected'] = '0';
			}
			$i++;
		}
		$this->assign('relation', $relation);
		$this->assign('planList', $planList);
		
		//获取商品列表
		$repairSelect = $modelRepairs->select();
		$this->assign('repairSelect', $repairSelect);
		
        $this->display('relation_edit');
    }

    
     /**
     * doRelationEdit 执行修改维修关联
     * @date 2015-01-22
     * @return void 
     */
    public function doRelationEdit()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
		$data['id'] = I('post.id');
        $data['product_id'] = I('post.productid');
        $data['fault_id'] = I('post.faultid');
        $data['plan_ids'] = I('post.planids');
		
        $modelRelation = M("repair_relation");
		if($modelRelation->save($data))
		{
			$this->success('修改成功.' . $lastID, U('Relation/index/product_id/'.I('post.productid')));
		}
		else
		{
			$this->error('修改失败！');
		}
    }

    /**
     * relationDelete 删除维修关联
     * @date 2015-01-22
     * @return void 
     */
    public function relationDelete(){
        $id = $_GET['id'];
        $modelRelation = M("repair_relation");
        $ls = $modelRelation->where("id =".$id)->delete();

        if($ls){
            $this->success("删除成功",U("Relation/index"));
        }else{
            $this->error("删除失败");
        }
    }

	

    /**
     * 执行商品change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changePlanSelect()
    {
        $productId = I('get.id');
		$modelRepairs = M('repairs');
        $modelFault = M('repair_fault');
		$modelPlan = M('repair_plan');

        $fault = $modelFault->field("id,name,product_id")->where("product_id = '{$productId}'")->order("sort desc")->select();
		foreach($fault as $key=>$item){
			$repair = $modelRepairs->where("id=".$item['product_id'])->find();
			$fault[$key]['name'] = $repair['pro_name']." - ".$item['name'];
		}
		$plan = $modelPlan->field("id,name")->where("product_id = '{$productId}'")->order("sort desc")->select();

		if(!is_array($fault) && !is_array($plan)){
			exit;
		}
		$data = json_encode(array('fault'=>$fault,'plan'=>$plan));
		echo $data;
		exit;
    }



}