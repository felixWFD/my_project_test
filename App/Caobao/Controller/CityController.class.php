<?php
namespace Caobao\Controller;
use       Think\Controller;

class CityController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * cityList 城市列表
     * @date 2015-01-22
     * @return void 
	 */

    public function index(){
		
		$one_id = I('get.one_id');
		$two_id = I('get.two_id');
		
		//获取省份信息
		$modelProvince = M('addr_province');
		$province = $modelProvince->select();
		$this->assign('province',$province);
		
		$modelCity = M("addr_city");
		
		if(!empty($one_id)){
			$city = $modelCity->where("one_id=".$one_id)->order('check_nm ASC')->select();
			$city_str = '';
			foreach($city as $key=>$item){
				if($item['id'] == $two_id){
					$city_str .= '<option value="'.$item['id'].'" selected>'.$item['addr'].'</option>';
				}else{
					$city_str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
				}
			}
			$this->assign('city_str',$city_str);
		}
		
		$modelArea = M("addr_area");
		// 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部' && $key == 'one_id') {
                    continue;
                }

                if ($val == '查找全部' && $key == 'two_id') {
                    continue;
                }

                $map[$key] = $val;
            }
        }
		$count = $modelArea->where($map)->count();
		$page = new \Think\Page($count,18,$map); 
		$list = $modelArea->where($map)->order('check_nm ASC')->limit($page->firstRow.','.$page->listRows)->select();
		foreach($list as $key=>$item){
			$addr_province = $modelProvince->field('check_nm,addr')->where("id=".$item['one_id'])->find();
			$list[$key]['pro_addr'] = $addr_province['addr'];
			$list[$key]['pro_nm'] = $addr_province['check_nm'];
			
			$addr_city = $modelCity->field('check_nm,addr')->where("id=".$item['two_id'])->find();
			$list[$key]['city_addr'] = $addr_city['addr'];
			$list[$key]['city_nm'] = $addr_city['check_nm'];
		}
		//dump($list);
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		
		$this->assign("one_id",I('get.one_id'));
		$this->assign("two_id",I('get.two_id'));
		$this->display('index');
    }

    /**
     * areaAdd 添加区域
     * @date 2015-01-22
     * @return void 
     */

    public function areaAdd(){
		
		//获取省份信息
		$modelProvince = M('addr_province');
		$province = $modelProvince->select();
		$this->assign('province',$province);
		
    	$this->display('area_add');
    }

    /**
     * doaddcity 执行area
     * @date 2015-01-22
     * @return void 
     */

    public function doAreaAdd(){
    	
		if (!IS_POST){
			exit('页面错误~');
		}
		$modelArea = M("addr_area");
		
		$data = array();
        $data['check_nm'] = I('post.check_nm');
        $data['addr'] = I('post.addr');
        $data['one_id'] = I('post.one_id');
        $data['two_id'] = I('post.two_id');
        $data['quan_pin'] = I('post.quan_pin');
        $data['jian_pin'] = I('post.jian_pin');
		
		if($modelArea->create($data)){
			$res=$modelArea->add();
			if($res > 0){
				$this->success("添加成功",U('City/index', array('one_id'=>$data['one_id'], 'two_id'=>$data['two_id'])));
			}else{
				$this->error("添加失败");
			}
		}else{
			 $this->error("添加失败");
		}
		
    }

    /**
     * areaEdit 修改区域
     * @date 2015-01-22
     * @return void 
     */

    public function areaEdit(){
		
		$id = I('get.id');
		
		//获取省份信息
		$modelProvince = M('addr_province');
		$province = $modelProvince->select();
		$this->assign('province',$province);
		
		//获取一条区域信息
		$modelArea = M("addr_area");
		$area = $modelArea->where("id=".$id)->find();
		$this->assign('one_id',$area['one_id']);
		$this->assign('area',$area);
		//定义城市下拉框内容
		$modelCity = M("addr_city");
		$city = $modelCity->where("one_id=".$area['one_id'])->order('check_nm ASC')->select();
		$city_str = '';
		foreach($city as $key=>$item){
			if($item['id'] == $area['two_id']){
				$city_str .= '<option value="'.$item['id'].'" selected>'.$item['addr'].'</option>';
			}else{
				$city_str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
			}
		}
		$this->assign('city_str',$city_str);
		
    	$this->display('area_edit');
    }

    /**
     * doAreaEdit 执行area修改
     * @date 2015-01-22
     * @return void 
     */

    public function doAreaEdit(){
    	
		if (!IS_POST){
			exit('页面错误~');
		}
		$id = I('post.id');
		$modelArea = M("addr_area");
		
		$data = array();
        $data['check_nm'] = I('post.check_nm');
        $data['addr'] = I('post.addr');
        $data['one_id'] = I('post.one_id');
        $data['two_id'] = I('post.two_id');
        $data['quan_pin'] = I('post.quan_pin');
        $data['jian_pin'] = I('post.jian_pin');
		
		$res=$modelArea->where("id=".$id)->save($data);
		if($res > 0){
			$this->success("修改成功",U('City/index', array('one_id'=>$data['one_id'], 'two_id'=>$data['two_id'])));
		}else{
			 $this->error("修改失败");
		}

    }
	
	//根据省份编号获取城市信息
	public function changeProvinceSelect(){
		$id = I('post.id');
		
		//获取城市信息
		$modelCity = M('addr_city');
		$city = $modelCity->where("one_id=".$id)->order('check_nm ASC')->select();
		$str = '';
		foreach($city as $key=>$item){
			$str .= '<option value="'.$item['id'].'">'.$item['addr'].'</option>';
		}
        echo $str;
		exit;
		
	}

}