<?php   
namespace Caobao\Controller;
use       Think\Controller;

class ExplainController extends MyController{
     public function __construct() {

        parent::__construct();
       
    }

    /**
     * 说明书List
     * @date 2015-01-22
     * @return void 
     */
    public function index(){
		
		// 获取说明书分类, 说明书分类套用商品类别
		$cat_id = I('get.cat_id');
        $modelCats = M('product_cat');
		
		$productCats = $modelCats->where(array('is_show'=>'1'))->select();
		$tree = new \Think\Tree($productCats);
		$str = "<option value=\$id \$selected>\$spacer\$cat_name</option>"; //生成的形式
		$strCats = $tree->getTree(34,$str, $cat_id);
		
        $modelExplain = M('explain');

        // 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
                if ($val == '查找全部说明书' && $key == 'cat_id') {
                    continue;
                }

                if ($val == '' && $key == 'keyword') {
                    continue;
                }

                $map[$key] = $val;
            }
        }

        // 查询总记录数
        $getPageCounts = $modelExplain->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $explainList = $modelExplain->where($map)->order('id DESC')->limit($page->firstRow, $page->listRows)->select();
		foreach($explainList as $key=>$item){
			$cat = $modelCats->where("id=".$item['cat_id'])->find();
			$explainList[$key]['cat_name'] = $cat['cat_name'];
		}
        $pageShow = $page->show();
        $this->assign('strCats', $strCats);
        $this->assign('page', $pageShow);
        $this->assign('explainList', $explainList);
        $this->display();

    }

    /**
     * 添加说明书
     * @date 2015-01-22
     * @return void 
     */
    public function addExplain(){
		$modelCat = M('product_cat');
        $cats = $modelCat->where('fid=34')->select();
     
        // 将商品分类变量分配到模板
        $this->assign('cats', $cats);
        $this->display();
    }

    /**
     * 执行添加说明书
     * @date 2015-01-22
     * @return void 
     */
    public function doAddExplain(){
        
        $_POST['create_time']= time();

        $db = M("explain");
        if($db->create()){
            $rt=$db->add();
            if($rt){
				CBWBaiduPush(array(C('SITE_URL')."/explain/".$rt.".html"));        // 百度SEO推送
				CBWBaiduPush(array(C('MOBILE_URL')."/explain/".$rt.".html"), 'm'); // 百度SEO推送
                $this->success("添加成功",U("Explain/index"));
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      
    }

    /**
     * 修改说明书
     * @date 2015-01-22
     * @return void 
     */
    public function editExplain(){
        $id = $_GET['id'];
        $data = M("explain");
        $list = $data->where("id =".$id)->find();       

		$modelCat = M('product_cat');
        $type_cats = $modelCat->where('fid=34')->select();
		$brand_cats = $modelCat->where('fid='.$list['type_id'])->select();
		$model_cats = $modelCat->where('fid='.$list['brand_id'])->select();
     
        // 将商品分类变量分配到模板
		$this->assign('type_cats', $type_cats);
		$this->assign('brand_cats', $brand_cats);
        $this->assign('model_cats', $model_cats);
	
        $this->assign("list",$list);
        $this->display();

    }

    /**
     * 执行修改说明书
     * @date 2015-01-22
     * @return void 
     */
    public function updateExplain(){
	
		$db = M("explain");

        if($db->create()){
            $rt=$db->save();
             if($rt){
                $this->success("修改成功",U("Explain/index"));
                exit;
            }else{
				$this->error("提交表单无修改");
			}
        }else{
            $this->error("修改失败");
        }

    }

    /**
     * 删除说明书
     * @date 2015-01-22
     * @return void 
     */
    public function delExplain(){
        $id = $_GET['id'];
        $db = M("explain");
        $ls = $db->where("id =".$id)->delete();
        if($ls){
            $this->success("删除成功",U("Explain/index"));
        }else{
            $this->error("删除失败");
        }
    }


}
