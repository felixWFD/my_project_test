<?php
namespace Caobao\Controller;
use       Think\Controller;

header('Content-Type:text/html; charset=utf-8');

class RepairController extends MyController
{

    /**
     * 构造函数，初始化
     * @date 2015-01-16
     */
    public function __construct()
    {		
        parent::__construct();
    }

    /**
     * 维修商品列表
     * @date 2015-01-15
     * @return void
     */
    public function repairList()
    {
        // 获取商品类别
		$cat_id = I('get.cat_id');
        $modelCats = M('product_cat');
		
		$repairCats = $modelCats->where(array('is_show'=>'1'))->select();
		$tree = new \Think\Tree($repairCats);
		$str = "<option value=\$id \$selected>\$spacer\$cat_name</option>"; //生成的形式
		$strCats = $tree->getTree(3,$str, $cat_id);
		
        $modelRepairs = M('repairs');

        // 分页处理，带关键字搜索
        if (isset($_GET)) {
            foreach ($_GET as $key => $val) {
				
                if ($val == '查找全部商品类别' && $key == 'brand_id') {
                    continue;
                }

                if ($val == '全部' && $key == 'is_on_sale') {
                    continue;
                }

                if ($val == '' && $key == 'keyword') {
                    continue;
                }

                $map[$key] = $val;
            }
        }

        // 查询总记录数
        $getPageCounts = $modelRepairs->where($map)->count();
        // 每页显示 $pageSize 条数据
        $pageSize = 15;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);

        $repairsList = $modelRepairs->where($map)->order('sort ASC')->limit($page->firstRow, $page->listRows)->select();
        $pageShow = $page->show();
        $this->assign('strCats', $strCats);
        $this->assign('page', $pageShow);
        $this->assign('repairsList', $repairsList);
        $this->display('repair_list');
    }

    /**
     * 获取添加商品页模板
     * @return void
     */
    public function repairAdd()
    {
        $modelCat = M('product_cat');
        $cats = $modelCat->where('fid=3')->select();
     
        // 将商品分类变量分配到模板
        $this->assign('cats', $cats);
        $this->display('repair_add');
    }

    /**
     * 执行产品添加操作
     * @date 2015-01-16
     * @return void
     */
    public function doAddRepair()
    {
        // 处理基本通用信息，包含fetcher
		$post = $_POST;

		$repair = array();
        $repair['type_id']       = $post['type_id'];
        $repair['brand_id']      = $post['brand_id'];
        $repair['pro_name']      = $post['pro_name'];
        $repair['min_price']     = $post['min_price'];
        $repair['max_price']     = $post['max_price'];
        $repair['keywords']      = $post['keywords'];
        $repair['description']   = $post['description'];
		$repair['is_on_sale']    = $post['is_on_sale'];
        $repair['list_image']    = $post['list_image'];
		$repair['feacher']       = $post['feacher'];
        $repair['add_time']      = time();
        $repair['update_time']   = time();

        // 当商品名为空时，拒绝添加
        if (empty($repair['pro_name'])) {
            $this->error('商品添加失败');
            exit;
        }

        $modelRepairs = M('repairs');
        if ($modelRepairs->create($repair)) {
            $rt = $modelRepairs->add();
            if ($rt) {
                // 写入日志
                $cLog = new \Caobao\Controller\LogController();
                $cLog->setLog('维修商品（ID:' . $getRepairId . '）添加成功');
                $this->success('维修商品添加成功', 'repairList');
                exit;
            } else {
                $this->error('维修商品添加失败');
            }

        } else {
            $this->error('上传出错。');
        }

    }
	
	/**
     * 获取修改商品页模板
     * @return void
     */
    public function repairEdit()
    {
		
        $getRepairId = I('get.id');
        // 获取产品详细信息
        $modelRepair = M('repairs');

		$repairRes = $modelRepair->where("id='{$getRepairId}'")->find();

		$modelCat = M('product_cat');
        $type_cats = $modelCat->where('fid=3')->select();
		$brand_cats = $modelCat->where('fid='.$repairRes['type_id'])->select();
     
        // 将商品分类变量分配到模板
		$this->assign('type_cats', $type_cats);
		$this->assign('brand_cats', $brand_cats);

		$this->assign('id', $getRepairId);
        $this->assign('albumRes', $albumRes);
		$this->assign('coorRes', $coorRes);
        $this->assign('pro', $repairRes);
        $this->display('repair_edit');
    } 

    /**
     * 执行商品修改操作
     * @return void
     */
    public function doRepairEdit()
    {
        $getRepairId = $_POST['id'];
		
		$modelRepair = M('repairs');

		$post = $_POST;
		$repair = array();
		$repair['id']            = $getRepairId;
        $repair['type_id']       = $post['type_id'];
        $repair['brand_id']      = $post['brand_id'];
        $repair['pro_name']      = $post['pro_name'];
        $repair['min_price']     = $post['min_price'];
        $repair['max_price']     = $post['max_price'];
        $repair['keywords']      = $post['keywords'];
        $repair['description']   = $post['description'];
		$repair['is_on_sale']    = $post['is_on_sale'];
        $repair['list_image']    = $post['list_image'];
		$repair['feacher']       = $post['feacher'];
        $repair['update_time']   = time();
		
		if (empty($repair['pro_name'])) {
            $this->error('商品更改无效');
            exit;
        }
		
		//判断是否有缩略图上传,有上传则删除旧图片
		$list_image = $post['list_image'];
		$org_list_image = $post['org_list_image'];
		if(!empty($list_image)){
			unlink("./Public/Repair/thumb/".$org_list_image);        //删除旧图片
			$repair['list_image'] = $list_image;
		}else{
			$repair['list_image'] = $org_list_image;
		}

        // 修改通用信息
        if ($modelRepair->create($repair)) {
            $rt = $modelRepair->save();
			if ($rt) {
				// 写入日志
				$cLog = new \Caobao\Controller\LogController();
				$cLog->setLog('维修商品（ID:' . $getRepairId . '）修改成功');
				$this->success('商品修改成功');
				exit;
			}
        }
		$this->error('商品添加失败');exit;
    }
	
	/**
     * POST执行商品修改操作
     * @return void
     */
    public function postRepairEdit()
    {
        $getId = $_POST['id'];
		$getType = $_POST['type'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		
		switch ($getType)
		{
			case 'name':
			  $data['pro_name'] = $getVal;
			  break;
			case 'is_on_sale':
			  $data['is_on_sale'] = $getVal;
			  break;
			case 'sort':
			  $data['sort'] = $getVal;
			  break;
		}

        // 修改商品信息
        $modelRepair = M('repairs');
        if ($modelRepair->create($data)) 
		{
            $modelRepair->save();
			echo "1";
			exit;
        }
		else
		{
			echo "0";
			exit;
		}

    }

    public function deleteRepair()
    {
        $getRepairId = I('get.id');

        $modelP = M('repairs');	
		$repairInfo = $modelP -> where("id = $getRepairId") -> find();

		// 删除商品同时删除缩略图与背景图
        $affectedRows = $modelP->where(array('id' => $getRepairId))->delete();       //删除商品表信息
		unlink("./Public/Repair/thumb/".$repairInfo['list_image']);                 //删除商品缩略图
		
        if ($affectedRows) {
			
			//删除关联表
			$modelColor = M('repair_color');
			$modelColor->where(array('repair_id' => $getRepairId))->delete(); 
			$modelFault = M('repair_fault');
			$modelFault->where(array('product_id' => $getRepairId))->delete(); 
			$modelPlan = M('repair_plan');
			$modelPlan->where(array('product_id' => $getRepairId))->delete(); 
			$modelRelaion = M('repair_relation');
			$modelRelaion->where(array('product_id' => $getRepairId))->delete(); 
			$modelShopModel = M('user_model');
			$modelShopModel->where(array('model_id' => $getRepairId))->delete(); 
            // 写入日志
            $cLog = new \Caobao\Controller\LogController();
            $cLog->setLog('维修商品（ID:' . $getProductId . '）删除成功');
            $this->success('维修商品删除成功');
        } else {
            $this->error('删除失败');
        }
    }
	
	//批量操作的商品
    public function batchDo ()
    {
        if ( ! isset($_POST['batch_option']))
        {
            $this->error('非法操作');
            exit;
        }

        if ( $_POST['batch_option'] == -1)
        {
            $this->error('请选择执行操作');
            exit;
        }

        $getIds = I('post.ids');
        $where = "id in (" . implode(', ', $getIds) . ') ';
        $modelP = M('repairs');

        switch ($_POST['batch_option'])
        {
            case 1:
				$modelColor   = M('repair_color');
				$modelFault   = M('repair_fault');
				$modelPlan    = M('repair_plan');
				$modelRelaion = M('repair_relation');
				$modelShopModel = M('user_model');
                foreach($getIds as $id)
				{
					//删除关联表
					$modelColor->where(array('repair_id' => $id))->delete(); 
					$modelFault->where(array('product_id' => $id))->delete(); 
					$modelPlan->where(array('product_id' => $id))->delete(); 
					$modelRelaion->where(array('product_id' => $id))->delete(); 
					$modelShopModel->where(array('model_id' => $id))->delete(); 
					
					//删除商品缩略图
					$repairInfo = $modelP -> where("id = $id") -> find();
					unlink("./Public/Repair/thumb/".$repairInfo['list_image']); 
					
				}
				
				$affectedRows = $modelP -> where($where) -> delete();       //批量删除商品
				
                $jumpURL='repairList';
                break;
            case 2:
				$data['is_on_sale'] = 0;
                $affectedRows = $modelP->where($where)->data($data)->save();
                $jumpURL = 'repairList';
                break;
			case 3:
				$data['is_on_sale'] = 1;
                $affectedRows = $modelP->where($where)->data($data)->save();
                $jumpURL = 'repairList';
                break;
        }

        if ($affectedRows)
        {
            $this->success('执行成功', $jumpURL);
        }
        else
        {
            $this->error('执行失败');
        }
    }

    /**
     * 执行产品类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeProductSelect()
    {
        $catId = I('get.id');
        $modelRepairCat = M('product_cat');

        $cats = $modelRepairCat->where("fid = '{$catId}'")->select();
		if(!is_array($cats)){
			exit;
		}
		$retString = '';
		foreach($cats as $key=>$item){
			$retString .= '<option value="'.$item['id'].'">'.$item['cat_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 上传图片，用于多文件上传插件
     * 此方法被商品添加页，上传缩略图所使用 
     * @date 2015-01-15
     * @return void
     */
    public function upload()
    {
        $upload = new \Think\Upload();  // 实例化上传类
		$rootPath = "./Public/Repair/thumb/"; //图片保存文件夹路径
        $upload->maxSize = 3145728;             // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                          // 设置附件上传目录    // 上传文件
        $info = $upload->upload();
		$imgUrl = $info['Filedata']['savepath'].$info['Filedata']['savename'];
		
        echo $imgUrl;
    }

    /**
     * 颜色管理 列表
     * @date 2015-01-16
     * @return void
     */
    public function colorList()
    {	
        $id = I('get.id');
		$modelColor = M('repair_color');
        $colorRes = $modelColor->where("repair_id=".$id)->select();
        $this->assign('colorRes', $colorRes);
		$this->assign('id', $id);
        $this->display('repair_color_list');
    }

    /**
     * 添加商品颜色，获取参数添加页面
     * @return [type] [description]
     */
    public function addColor()
    {
        $id = I('get.id');

		$modelColor = M('repair_color');
        $colorRes = $modelColor->where("repair_id=".$id)->select();
        $this->assign('colorRes', $colorRes);
        $this->display('repair_color_add');
    }

    /**
     * 执行参数添加操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function doColorAdd()
    {
        if (I('post.repair_id') == '' || I('post.name') == '') {
            $this->error('警告：非法操作');
            exit;
        }
		$data = array();
        $data['repair_id'] = I('post.repair_id');
        $data['name'] = I('post.name');

        $modelColor = M('repair_color');
        if ($modelColor->create($data)) {
			$getColorId = $modelColor->add();
            if ($getColorId) {
                $this->success('添加成功！',U('Repair/colorList/id/'.I('post.repair_id')));
            } else {
                $this->error('添加失败');
            }
        }
    }

    /**
     * 执行删除颜色操作
     * @date 2015-01-16
     * @return void
     */
    public function doDeleteColor()
    {
        $id = I('get.id');
		
        $modelColor = M('repair_color');
		$color = $modelColor->where("id=".$id)->find();
        $affectedRows = $modelColor->where("id=".$id)->delete();
		
        if ($affectedRows > 0) {
            $this->success('删除成功',U('Repair/colorList/id/'.$color['repair_id']));
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 执行颜色编辑操作
     * @date 2015-01-16
     * @return void
     */
    public function doEditColor()
    {
        $modelColor = M('repair_color');
		$id = I('post.id');
		$repair_id = I('post.repair_id');
        $color['name'] = I('post.name');
		$colorId = $modelColor->where("id=".$id)->data($color)->save();
		
        if ($colorId) {
			$this->success('修改成功',U('Repair/colorList/id/'.$repair_id));
			exit;
        }else{
			$this->error('修改失败');
			exit;
		}
       
    }

    /**
     * 编辑商品颜色，获取颜色编辑页面
     * @return void
     */
    public function editColor()
    {
        $id = I('get.id');
        $modelColor = M('repair_color');
        $colorRes = $modelColor->where("id=".$id)->find();
		
        $this->assign('colorRes', $colorRes);
        $this->display('repair_color_edit');
    }

    /**
     * 伪代码，没有此方法多文件上传插件出错
     * @return void
     */
    public function index()
    {

    }
	
}
