<?php
namespace Caobao\Controller;
use       Think\Controller;

/**
 * CommentController
 * 评价管理
 */
class CommentController extends MyController {
	
	/**
     * 构造方法
     */
	public function __construct() {
		parent::__construct();
	}


    //用户评价
    public function index()
    {
        $id = $_GET['id'];
		$cat_id = $_GET['cat_id'];
		$keyword = $_GET['keyword'];
        
		$model = M();
		$modelUsers = M('users');
		$modelOrders = M('orders');
		$modelReplies = M('comment_replies');
        $modelComment = M('comments');
		$modelProducts = M('products');
		
		// 分页处理，带关键字搜索
		if(!empty($id)){
			$map['pro_id'] = $id;
		}
		if(!empty($keyword)){
			if($cat_id != '查找全部'){
				if($cat_id == 'uname'){
					$map['uname'] = $keyword;
				}elseif($cat_id == 'content'){
					$map['content'] = array('like','%'.$keyword.'%');
				}
			}
		}
		
		$count = $modelComment->where($map)->count();

        $pageSize = 15;
        $page = new \Think\Page($count, $pageSize);
        $showPage = $page->show();
		$userComment = $modelComment->where($map)->limit($page->firstRow, $page->listRows)->order('id DESC')->select(); //用户对于商品的评价
        foreach($userComment as $key=>$vo) {
			if($vo['user_id'] > 0){
				$user = $modelUsers->where("id=".$vo['user_id'])->find();
				$userComment[$key]['uname']  = $user['uname'];
				$userComment[$key]['mobile'] = $user['mobile'];
			}
			
			if($vo['order_id'] > 0){
				$order = $modelOrders->where("id=".$vo['order_id'])->find();
				$userComment[$key]['order_sn']  = $order['order_sn'];
			}
			
			if($vo['pro_id'] > 0){
				$products = $modelProducts->field('pro_name')->where("id=".$vo['pro_id'])->find();
				$userComment[$key]['pro_name'] = $products['pro_name'];
			}else{
				$comment = $model->table('__ORDERS__ AS o')
									->field('p.pro_name')
									->join('__ORDER_PRODUCTS__ op ON op.order_id= o.id')
									->join('__PRODUCTS__ p ON p.id= op.pro_id')
									->where('o.id='.$vo['order_id'])
									->find();
				$userComment[$key]['pro_name'] = $comment['pro_name'];
			}
			
            $counts = $modelReplies->where("comment_id = ".$vo['id'])->count();
            if ($counts){
                $userComment[$key]['is_reply'] = true;
            }else{
                $userComment[$key]['is_reply'] = false;
            }
        }

        $this->assign('page', $showPage);
        $this->assign('data', $userComment);
        $this->display('comment');
    }
	
    /**
     * 添加评价
     * @return void
     */
    public function commentAdd()
    {
		$id = I('get.id');
		$modelProducts = M('products');
		$product = $modelProducts->field('pro_name')->where("id=".$id)->find();
		
		$this->assign("product",$product);
        $this->display('comment_add');
    }
	
    /**
     * 添加评价操作
     * @return void
     */
    public function doCommentAdd()
    {
		$comments = D('Comments');
		$users    = D('Users');
		$area     = D('Area');
		
		$data = array();
		$data['pro_id']       = I('post.pro_id');
		$rand  = $users->randomUserName();
		$uname = I('post.uname');
		$data['uname']        = empty($uname)?$rand:$uname;
		$data['city']         = $area->getRandCity();
		$data['content']      = I('post.content');
		$data['score']        = 1;
		$comment_time         = I('post.comment_time');
		$comment_time         = strtotime($comment_time);
		$data['comment_time'] = empty($comment_time)?time():$comment_time;
		
		$rt = $comments->insertComment($data);
		if($rt['status'] > 0){
			$this->success('添加成功');
		}else{
			$this->error('数据错误');
		}
        
    }

    /**
     * 批量删除评价
     * @return void
     */
    public function doBatchDelete()
    {
        $fids = I('post.fids');
        $whereString = implode(', ', $fids);

        $modelComment = M('comments');
        $modelReplies = M('comment_replies');
        // 删除所有回复内容
        $modelReplies->where("comment_id in ({$whereString})")->delete();
        // 删除所有评价内容
        $affectedRows = $modelComment->where("id in ($whereString)")->delete();

        if ($affectedRows) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
	
	/**
     * 执行评论修改操作
     * @return void
     */
    public function postCommentEdit()
    {
        $getId = $_POST['id'];
		$getType = $_POST['type'];
		$getVal = $_POST['val'];

        if ($getVal == '') {
            echo "0";
            exit;
        }
		
		$data['id'] = $getId;
		
		switch ($getType)
		{
			case 'status':
				$data['status'] = $getVal;
				break;
		}

        // 修改商品信息
        $model = M('comments');
        if ($model->create($data)) 
		{
            $model->save();
			echo "1";
			exit;
        }
		else
		{
			echo "0";
			exit;
		}

    }

    /**
     * 执行删除操作
     * @return void
     */
    public function doDeleteComment()
    {
        $getFId = I('get.id');
        // 删除评价
        // 同时删除回复
        $modelComment = M('comments');
        $modelReplies = M('comment_replies');

        $modelReplies->where("comment_id = '{$getFId}'")->delete();
        $affectedRows = $modelComment->where("id='{$getFId}'")->delete();

        if ($affectedRows) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 获取回复页面
     * @return void
     */
    public function commentView()
    {
        $id = $_GET['id'];
        $fid = $_GET['fid'];    //父级页面的id
        //1.将评价取出
        $modelComment = M('comments');
        $modelUsers = M('users');

        $userReplay = $modelComment->where("id = $id")->find(); //用户对于商品的评价
		
		if($userReplay['user_id'] > 0){
			$user = $modelUsers->where("id=".$userReplay['user_id'])->find();
			$userReplay['uname'] = $user['uname'];
		}else{
			$user = $modelUsers->alias('u')
								->field('u.uname')
								->join('__ORDERS__ AS o ON o.user_id= u.id')
								->join('__COMMENTS__ AS c ON c.order_id=o.id')
								->where('c.id='.$userReplay['id'])
								->find();
			$userReplay['uname'] = $user['uname'];
		}

        //2.将回复取出
        $modelReplies = M('comment_replies');
        $replay = $modelReplies -> where("comment_id = $id") -> find();
        $userReplay['replay_content'] = $replay['content'];
        $userReplay['replay_id'] = $replay['id'];
        $userReplay['replay_user_id'] = $replay['admin_user_id'];
        $userReplay['fid'] = $fid;


        $this->assign('data', $userReplay);
        $this->display('comment_view');
    }

    /**
     * 执行回复操作
     * @return void
     */
    public function doCommentRepliesAdd()
    {
		$id = $_POST['id'];
        $admin_id = $_SESSION['id'];
        $comment_id = $_POST['reply_id'];
		$content = $_POST['content'];
        $reply_content = $_POST['reply_content'];
        
		$modelComment = M('comments');
		$modelComment->where("id=".$id)->save(array('content'=>$content));

		$replies = array();
        $replies['content'] = $reply_content;
        $replies['reply_time'] = time();
        $replies['comment_id'] = $id;
        $replies['admin_user_id'] = $admin_id;

        $modelReplies = M('comment_replies');
		if(!empty($reply_content)){
			// 如果以前被回复过，则修改
			// 如果以前没有进行过回复 ，则插入
			if (!empty($comment_id)){
				$row = $modelReplies->where("comment_id='{$id}'")->data($replies)->save();
				if ($row) {
					$this->success('执行成功');exit;
				}
				$this->error('执行失败');exit;
			} else {
				if ($modelReplies->create($replies)) {
					if ($modelReplies->add()) {
						$this->success('执行成功');exit;
					}
				}
				$this->error('上传失败');exit;
			}
		}
		$this->success('执行成功');exit;
    }
}
