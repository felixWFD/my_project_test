<?php  
namespace Caobao\Controller;
use       Think\Controller;

    /**
     * SEO
     * 
     * @author: Sun
     * @date: 2015-01-22
     */

 class SeoController extends MyController{
     
    /**
     * 构造函数，初始化
     * @date 2015-01-22
     */

    public function __construct() {
        parent::__construct();
       
    }

    /**
     * seoList SEO列表
     * @date 2015-01-22
     * @return void 
     */

    public function index(){
		$db = M("seo");
		$count = $db->count();
		$page = new \Think\Page($count,10); 
		$list = $db -> limit($page->firstRow.','.$page->listRows)->select();
		//dump($list);
		$this->assign("list",$list);
		$show  = $page->show();
		$this->assign("page",$show);
		$this->display('seoList');
    }

    /**
     * addseo 添加seo
     * @date 2015-01-22
     * @return void 
     */

    public function addseo(){

    	$this->display();
    }

    /**
     * doaddseo 执行添加seo
     * @date 2015-01-22
     * @return void 
     */

    public function doaddSeo()
	{
    	if (!IS_POST) {
			exit('页面错误~');
		}
        $data['seoname'] = I('post.seoname');
        $data['ident'] = I('post.ident');
        $data['title'] = I('post.title');
        $data['keywords'] = I('post.keywords');
        $data['description'] = I('post.description');
        //$data['affiliation'] = I('post.affiliation');
        $data['time'] = time();
        $modelSeo = M("seo");
        if($modelSeo->create($data)){
            $res=$modelSeo->add();
            if($res){
                $this->success('添加成功.' . $lastID, 'index');
            }else{
                $this->error("添加失败");
            }
        }else{
             $this->error("添加失败");
        }      

    }

    /**
     * editSeo 修改seo
     * @date 2015-01-22
     * @return void 
     */
    public function editSeo(){
        $id = $_GET['id'];
        $data = M("seo");
        $list = $data->where("id =".$id)->find();       
        $this->assign("list",$list);
        $this->display();
    }

    
     /**
     * doeditSeo 执行修改Seo
     * @date 2015-01-22
     * @return void 
     */
    public function doeditSeo()
	{
		if (!IS_POST) {
			exit('页面错误~');
		}
        $data['id'] = I('post.id');
        $data['seoname'] = I('post.seoname');
        $data['ident'] = I('post.ident');
        $data['title'] = I('post.title');
        $data['keywords'] = I('post.keywords');
        $data['description'] = I('post.description');
        //$data['affiliation'] = I('post.affiliation');
        $modelSeo = M("seo");
		if($modelSeo->save($data))
		{
			$this->success('修改成功.' . $lastID, 'index');
		}
		else
		{
			$this->error('修改失败！');
		}
    }

    
    /**
     * delSeo 删除Seo
     * @date 2015-01-22
     * @return void 
     */
    public function delSeo(){
        $id = $_GET['id'];
        $modelSeo = M("seo");
        $ls = $modelSeo->where("id =".$id)->delete();

        if($ls){
            $this->success("删除成功",U("Seo/index"));
        }else{
            $this->error("删除失败");
        }
    }





}