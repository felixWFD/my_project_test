<?php
/**
 * 管理员和用户组关联模型
 */
namespace Caobao\Model;
use Think\Model\RelationModel;

Class UserRelationModel extends RelationModel{
	protected $tableName='admin_users';
	protected $_link=array(
			'AuthGroup'=>array(
				'mapping_type'=>self::MANY_TO_MANY,
				'class_name'=>'AuthGroup',
				'mapping_name'=>'gro',
				'foreign_key'=>'uid',
				'relation_foreign_key'=>'group_id',
				'relation_table'=>'__AUTH_GROUP_ACCESS__',
				'mapping_fields'=>'id,title'
			),
			'AdminInfo'=>array(
				'mapping_type'=>self::HAS_ONE,
				'class_name'=>'AdminInfo',
				'mapping_name'=>'info',
				'foreign_key'=>'uid',
				'mapping_fields'=>'true_name,thumbnail,email,phone,qq'
			),
		);
}
?>