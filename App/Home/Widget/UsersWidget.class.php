<?php
namespace Home\Widget;
use       Think\Controller;

class UsersWidget extends Controller {
	
	// 用户中心菜单
    public function menu() {
		$users = D('Users');
		$ua    = D('UserAgent');
		
		$userId = session('user.userid');
		$user = $users->getUserAndInfoById($userId);
		
		if($user['is_agent']){
			$agent = $ua->getAgentByUserId($userId);
			$user['agent'] = $agent;
		}
		
		$this->assign("user", $user);
        $this->display('Users:menu');
    }

}