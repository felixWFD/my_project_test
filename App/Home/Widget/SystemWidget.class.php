<?php
namespace Home\Widget;
use       Think\Controller;

class SystemWidget extends Controller {

	// 网站侧栏
	public function sidebar(){
		$this->display('Public:sidebar');
	}
	
	// 网站页脚
    public function footer() {
		// 加载页脚文章
		$modelCat = M('product_cat');
		$modelExplain = M('fault_explain');
		$catRess = $modelCat->field('id,cat_name')->where(array('fid'=>'4','is_show'=>'1'))->order('sort ASC')->select();
		foreach($catRess as $key=>$vo){
			$brandRes = $modelCat->field('id,cat_name')->where(array('fid'=>$vo['id'],'is_show'=>'1'))->order('sort ASC')->select();
			foreach($brandRes as $k=>$v){
				$faults = $modelExplain->field('id,title')->where(array('fault_id'=>$v['id'],'is_show'=>'1'))->order('id ASC')->select();
				$brandRes[$k]['faults'] = $faults;
			}
			$catRess[$key]['brandRes'] = $brandRes;
		}
		$this->assign('catRess',$catRess);	
		
		// 友情链接
		$system = D('System');
		$linksRes = $system->loadLinks();
		$this->assign("linksRes",$linksRes);
		$this->assign('controller', CONTROLLER_NAME);
		$this->assign('action', ACTION_NAME);
        $this->display('Public:footer');
    }
	
	// 用户登录信息
	public function user($userId, $shopId){
		$users   = D('Users');
		$orders  = D('Orders');
		$repairs = D('Repairs');
 
		$user       = $users->getUserAndInfoById($userId);
		$offerCount = 0;
		
		if(!empty($user)){
			if($user['group_id'] > 0){
				$offerCount = $orders->getOrderOfferIngCountByUserId(array('userId'=>$userId, 'status'=>0));
			}else{
				$offerCount = $orders->getOrderOfferCountByUserId($userId);
			}
		}
		
		// 
		if($userId > 0){
			$offer = $orders->getOrderOfferByUserId($userId, 0, 1);
			if(!empty($offer)){
				$order = $orders->getOrderById($offer[0]['order_id']);
				if($order['order_type'] == 1){
					$server = $orders->getOrderServerByOrderId($order['id']);
					if($server['relation_id'] > 0){
						$relation = $repairs->getRepairByRelationId($server['relation_id']);
						$order['pro_name'] = $relation['pro_name'].'维修订单';
					}else{
						$order['pro_name'] = '维修订单';
					}
				}elseif($order['order_type'] == 2){
					$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
					if($recovery['reco_id'] > 0){
						$repair = $repairs->getRepairById($recovery['reco_id']);
						$order['pro_name'] = $repair['pro_name'].'回收订单';
					}else{
						$order['pro_name'] = '回收订单';
					}
				}
			}
		}
		
		$offerShopCount = 0;
		if($shopId > 0){
			// 未读可以报价数量
			$offerShopCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));
		}
		
		$this->assign('user', $user);
		$this->assign('offerCount', $offerCount);
		$this->assign('order', $order);
		$this->assign('offerShopCount', $offerShopCount);
        $this->display('Public:user');
	}
	
	// 店铺系统菜单
    public function menu($shopId) {
		$us     = D('UserShop');
		$orders = D('Orders');
		
		$shop       = $us->getShopByShopId($shopId);
		$notices    = $us->getShopNoticeCountByObject(array('shopId'=>$shopId, 'status'=>'0'));
		$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0));
		

		$this->assign('shop', $shop);                   // 店铺信息
		$this->assign('notices', $notices);             // 店铺未读通知数量
		$this->assign('offerCount', $offerCount);       // 未报价订单数量
		$this->assign('action', ACTION_NAME);           // 当前行为动作
        $this->display('System:menu');
    }

}