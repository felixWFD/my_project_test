<?php
/**
 * 函数库
 */
	
	/**
	 * 维修师系统,统计未处理维修订单数,有带编号统计维修师个人,否则统计内部维修师订单数
	 * @param  [string] $repair_id    [维修师编号]
	 * @return [int]	              [统计数量]
	 */
	function countRepair($repair_id){
	
		$modelOrders = M('orders');
		
		$where = "o.id = s.order_id AND o.order_type=1 AND o.order_status=1";
		if(!empty($repair_id)){
			$where .= " AND s.server_status=1 AND s.repair_id=".$repair_id;
		}else{
			$where .= " AND s.server_status=0 AND s.server_way=0 AND s.server_shipping_way<>0 AND s.server_shipping_num<>0";
		}
        $count = $modelOrders->table('__ORDERS__ AS o, __ORDER_SERVER__ AS s')->where($where)->count();
		return $count;
		
	}
	
	/**
     * 日志写入类，为其它操作器写入的接口
     * @date 2015-01-14
     * @param string $userid 操作人编号
     * @param string $username 操作人
     * @param string $content 需要记录的日志信息
     */
    function setLog($userid, $username, $content = ''){
        // 如果设置的内容为空，则返回
        if ($content == ''){
            return ;
        }
        $getRemoteIp        = I('server.REMOTE_ADDR');

        $data['user_id']    = $userid;
        $data['username']   = $username;
        $data['content']    = $content;
        $data['ctime']      = time();
        $data['cip']        = ip2long($getRemoteIp);

        $modelLog = M('admin_logs');
        if ($modelLog->create($data))
        {
            $modelLog->add();
        }
    }
	
	/**
	 * 获取当前页面完整URL地址
	 */
	function getUrl() {
		$sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
		$php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
		$path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
		$relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self.(isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : $path_info);
		return $sys_protocal.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '').$relate_url;
	}
	
?>