<?php
namespace   Home\Controller;

class IndexController extends BaseController {
	/*public function test(){
		$products = D('Products');
		$sql = "SELECT * FROM __PREFIX__product_attr WHERE pro_id>0 AND attr_id<6 ORDER BY pro_id ASC";
		$data = $products->query($sql);
		foreach($data as $vo){
			$attr  = $products->queryRow("SELECT * FROM __PREFIX__attribute WHERE id=".$vo['attr_id']);
			$attr2 = $products->queryRow("SELECT * FROM __PREFIX__attribute WHERE pro_id=".$vo['pro_id']." AND attr_name='".$attr['attr_name']."'");
			if(empty($attr2)){
				$attrArray = array(
					'pro_id'     => $vo['pro_id'],
					'attr_name'  => $attr['attr_name'],
					'attr_ident' => '', 
					'sort'       => 0
				);
				$rt = $products->insertAttribute($attrArray);
				$attrId = $rt['status'];
			}else{
				$attrId = $attr2['id'];
			}
			$products->updateProductAttr($vo['id'], array('attr_id'=>$attrId));
		}
	}*/
	
	// 首页控制器
    public function index(){
//		$model = M();
		
		// 当前保存seeion城市
		$area = D('Area');
		$cityName = session('location.city');
		$city = $area->getCityByName($cityName);
        if(empty($city)){
            $city = $area->getCityByName('深圳市');
        }
		$this->assign("city",$city);

		// Banner 幻灯片 1919 x 430
		$ads = D('Advs');
		$catAds = $ads->getAdvsByCat(1, 1);
		$this->assign('catAds',$catAds);
		
		$products = D('Products');
		$cat      = D('ProductCat');
		$repairs  = D('Repairs');
		$us       = D('UserShop');
		
		// 快捷栏目
		$cats = $cat->getCatListById(3);
		foreach($cats as &$vo){
			$brands = $cat->getCatListById($vo['id']);
			foreach($brands as &$v){
				$v['models'] = $repairs->getRepairByBrandId($v['id']);
			}
			unset($v);
			$vo['brands'] = $brands;
		}
		unset($vo);
		$this->assign("cats",$cats);
		
		// 配件商店		
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
        $productRes=[];
		//$cat_name=['Componment','External','tool'];// Componment(内配) External（外配） tool（工具）
        //新品
        $product_new= $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'new'=>'is_new', 'groupby'=>1, 'm'=>0, 'n'=>4));
        array_push($productRes,$product_new);
        //热销
        $product_hot= $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'hot'=>'is_hot', 'groupby'=>1, 'm'=>0, 'n'=>4));
        array_push($productRes,$product_hot);
        //内配 外配 工具
        foreach ([5,6,9] as $k=>$cat_id){
            //'groupId'=>$groupId, 'vip'=>$vip,
            $product_ca= $products->getProductRelationByObject(array( 'classId'=>$cat_id, 'groupby'=>1,'sort'=>'default', 'm'=>0, 'n'=>4));
            array_push($productRes,$product_ca);
            $t=M()->getLastSql();
        }

		$this->assign("productRes",$productRes);
		
		// 获取所在城市的地区
		$areas = $area->getAreaByCityId($city['id']);
		foreach($areas as &$vo){
			$shops = $us->getShopsByObject(array('area'=>$vo['id'], 'm'=>0, 'n'=>6));
			if(!empty($shops)){
				foreach($shops as $k=>$v){
					$level = CBWShopLevel($v['exp']); // 等级换算
					$shops[$k]['level_image_name'] = $level['name'];
					$shops[$k]['level_image_num']  = $level['num'];
					$shops[$k]['level_level']      = $level['level'];
					
					$sbrand = $us->getUserBrandByUserId($v['user_id']);
					$bname = array();
					foreach($sbrand as $b){
						$bname[] = $b['cat_name'];
					}
					$shops[$k]['brands'] = implode(" ", array_unique($bname));
				}
			}
			$vo['shopc'] = $us->getShopsCountByObject(array('area'=>$vo['id']));
			$vo['shops'] = $shops;
		}
		unset($vo);
		$areas = CBWArraySort($areas,'shopc',SORT_DESC);
		$this->assign("areas",$areas);
		
		// 维修视频
		$video = D('Video');
        $videoRes = $video->getVideoList(array('m'=>0, 'n'=>4));
		$this->assign("videoRes",$videoRes);
		
		// 维修技巧
		$forum = D('Forum');
		$skillRes = $forum->getThreadByObject(array('category'=>array(1,2), 'sort'=>'helpful', 'm'=>0, 'n'=>5));
		foreach($skillRes as &$vo){
			$schedules = $forum->getScheduleByThreadId($vo['id'], 2);
			// 图片处理
			if(empty($vo['attachment'])){
				foreach($schedules as $sche){
					if(!empty($sche['attachment'])){
						$vo['attachment'] = $sche['attachment'];
						break;
					}
				}
			}
			$vo['schedules'] = $schedules;
		}
		unset($vo);
		$this->assign("skillRes",$skillRes);

		//资讯中心
        $modelNews = M('news_contents');
        $modelCat = M('news_cat');

        //新闻分类列表
        $newsRes = $modelCat->where("is_show=1")->order('id ASC')->select();
        $this->assign("newsRes",$newsRes);

        $newsList_all=[];
        foreach($newsRes as $key=>$va){
            $where_news='is_show=1 and cat_id='.$va['id'];
            $newsList = $modelNews->where($where_news)->order("cat_id ASC,create_time DESC")->limit(0,3)->select();
            foreach($newsList as $key=>$vo) {
                if (empty($vo['thumbnail'])) {
                    $thumbnail = CBWGrabImages(htmlspecialchars_decode($vo['content']));  //抓取内容第一张图片 作为列表图
                    $thumbnail = '<img src="' . $thumbnail . '" title="' . $vo['title'] . '" alt="' . $vo['title'] . '" />';
                } else {
                    $thumbnail = '<img src="' . C('SITE_URL') . '/Public/News/' . $vo['thumbnail'] . '" title="' . $vo['title'] . '" alt="' . $vo['title'] . '" />';
                }
                $newsList[$key]['thumbnail'] = $thumbnail;
            }
            $newsList_all= array_merge($newsList_all,[$va['id']=>$newsList]);
        }
        $this->assign("newsList_all",$newsList_all);


		// SEO
        $ident ="index";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->assign("ident",$ident);
        $this->display();
    }
	
	// 加载县区维修店
	public function loadshops(){
		
		$area_id = I('post.area_id');
		
		$model = M();
		
		$shopRes = $model->table('__USER_SHOP__ AS s')
					->field('s.*,p.addr as province,c.addr as city,a.addr as area')
					->join('__ADDR_AREA__ AS a ON a.id=s.area_id')
					->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
					->join('__ADDR_PROVINCE__ AS p ON p.id=a.one_id')
					->where("s.type_id<>2 AND s.server_status=1 AND a.id=".$area_id)
					->order('s.score DESC,s.id ASC')
					->limit(0,6)
					->select();
		if(!empty($shopRes)){
			foreach($shopRes as $key=>$vo){
				$shopcatRes = $model->table('__USER_SHOP_CAT__ AS sc')
						->field('pc.cat_name')
						->join('__PRODUCT_CAT__ AS pc ON pc.id=sc.brand_id')
						->where('sc.user_id='.$vo['id'])
						->limit(3)
						->select();
				$shopcat = array();
				if(!empty($shopcatRes)){
					foreach($shopcatRes as $s){
						$shopcat[] = $s['cat_name'];
					}
					$shopRes[$key]['shopcat'] = implode(',', $shopcat);
				}
			}
			echo json_encode($shopRes);exit;
		}
		echo '0';exit;
	}
	
	//维修招募专题页面
	public function special(){
		
		//SEO
        $ident ="recruit";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	//维修招募页面
	public function joins(){

		//SEO
        $ident ="recruit";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	//招募页面-店铺信息
	public function recruit(){
		
		$user_id = session('user.userid');
		$groupId = session('user.groupid');

		if(!empty($user_id)){
			$modelUsers = M('users');
			$user = $modelUsers->field('mobile,mobile_state')->where('id='.$user_id)->find();
			if(empty($user['mobile']) || $user['mobile_state'] == '0'){
				$this->error('请完善个人信息后再进行下一步操作(绑定手机)', U('Users/security'));
				exit;
			}

			$modelRecruit = M('user_recruit');
			$count = $modelRecruit->where('user_id='.$user_id)->count();
			if($count > 0){
				$this->error('您已经有招募申请在处理,请不要重复申请');
			}
		}
		if($groupId){
            $this->error('您的店铺已开通,请不要重复申请', U('Users/index'));
			exit;
        }
		
		$type = I('get.type');
		$this->assign('type',$type);
		
		// 三级联动
		$areaId = session('recruit.areaid');
		if($areaId > 0){
			$modelArea = M('addr_area');
			$info = $modelArea->where('id='.$areaId)->find();
			$cityArray = array('province'=>$info['one_id'], 'city'=>$info['two_id'], 'area'=>$info['id']);
			$city = \Think\Area::city($cityArray);
		}else{
			$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
			$city = \Think\Area::city($cityArray);
		}
        $this->assign('city', $city);
		
		// 招募简介
		$modelArticle = M('article_detail');
		$article = $modelArticle->where("id=52")->find();
		$this->assign('article', $article);
		
		//SEO
        $ident ="recruit";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	//检查短信验证码是否正确
	public function check_sms(){
		
		$mobile = I('post.mobile');
		$smscode = I('post.smscode');
		
		$modelUserCode = M('user_code');
		
		//获取验证码
		$code = $modelUserCode->where("account='".$mobile."'")->find();
		
		// 判断验证码时间和数字
		if(strcmp($smscode, $code['code']) != 0){
			echo '0';   //验证码错误
			exit;
		}
		$nowTime = time();
		$time_num = $nowTime - $code['add_time'];
		if($time_num > 28800)	{
			echo '2';   //验证码已过期 
			exit;
		}
		echo '1';
		exit;
	}
	
	/**
	 * 获取城市区号
	 */
    public function getCityNumber(){
		$city   = I('post.city');
		$modelCity = M('addr_city');
		$data = $modelCity->where("addr like '%".$city."%'")->find();
		echo $data['area_no'];
    }
	
	//添加维修招募操作
	public function add_recruit(){
		
		if (!IS_POST) {
			$this->error('非法请求！');
		}
		
		$userId    = session('user.userid');
		$mobile    = I('post.mobile');
		$shopName  = I('post.shop_name');
		$userName  = I('post.user_name');
		$isStore   = I('post.is_store');
		$isDoor    = I('post.is_door');
		$main      = I('post.main');	
		$areaId    = I('post.area');
		$address   = I('post.address');
		
		// 保存Session
		session(array('name'=>'shopRecruit', 'prefix'=>'recruit'));
		session('mobile',  $mobile);
		session('sname',   $shopName);
		session('uname',   $userName);
		session('store',   $isStore);
		session('door',    $isDoor);
		session('main',    $main);
		session('areaid',  $areaId);
		session('address', $address);
		
		if(empty($userId)){   //未登陆
			$modelUsers = M('users');
			$users = $modelUsers->where("mobile='".$mobile."'")->find();
			if(!empty($users)){   //未注册
				$userId = $users['id'];
				if($users['group_id'] > 0){
					$this->error('您的店铺已开通,请不要重复申请');
				}
			}else{
				$modelUserCode = M('user_code');
				$code = $modelUserCode->where("account='".$mobile."'")->find();
				
				// 判断验证码时间和数字
				$smscode = I('post.smscode');
				if(strcmp($smscode, $code['code']) != 0){
					$this->error('短信验证码错误');
				}
				$nowTime = time();
				$time_num = $nowTime - $code['add_time'];
				if($time_num > 28800)	{
					$this->error('短信验证码已过期');
				}
			
				// 插入用户登录表
				$password = substr($mobile, -6);
				$user['account']		 = $this->randomAccount();
				$user['uname']			 = $this->randomUserName();
				$user['password']  		 = sha1($password);
				$user['mobile']          = $mobile;
				$user['mobile_state']    = 1;
				$user['create_time']     = time();
				$user['last_login_time'] = time();
				$user['last_login_ip']   = ip2long(I('server.REMOTE_ADDR'));

				if($modelUsers->create($user)){
					$userId = $modelUsers->add();
					if($userId){
						$modelUserInfo = M('user_info');
						$modelUserInfo->data(array('user_id'=>$userId))->add();   	// 插入用户详细表
						
						//发送密码
						sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
					}else{
						$this->error('注册失败');
					}
				}else{
					$this->error('未知错误');
				}
			}
		}
		
		$modelRecruit = M('user_recruit');
		$count = $modelRecruit->where('user_id='.$userId)->count();
		if($count > 0){
			$this->error('您已经有招募申请在处理,请不要重复申请');
		}
		$data['user_id']     = $userId;
		$data['type']        = I('post.type');
		$data['shop_name']   = $shopName;
		$data['user_name']   = $userName;
		$data['is_store']    = $isStore;
		$data['is_door']     = $isDoor;
		$data['main']        = $main;	
		$data['area_id']     = $areaId;
		$data['address']     = $address;
		$data['address_deg'] = I('post.lng').','.I('post.lat');
		
		// 判断有没有文件上传 - 正面
		$positive = I('post.identity_positive');
        if (!empty($positive)){
			$imgPositive = CBWBase64Upload($positive, './Public/Repair/data/');
			if($imgPositive){
				$data['identity_positive'] = $imgPositive;
			}
        }
		
		// 判断有没有文件上传 - 反面
		$back = I('post.identity_back');
        if (!empty($back)){
			$imgBack = CBWBase64Upload($back, './Public/Repair/data/');
			if($imgBack){
				$data['identity_back'] = $imgBack;
			}
        }
		
		// 判断有没有文件上传 - 半身
		$body = I('post.identity_body');
        if (!empty($body)){
			$imgBody = CBWBase64Upload($body, './Public/Repair/data/');
			if($imgBody){
				$data['identity_body'] = $imgBody;
			}
        }
		
		// 判断有没有文件上传 - 营业执照
		$business = I('post.business');
        if (!empty($business)){
			$imgBusiness = CBWBase64Upload($business, './Public/Repair/data/');
			if($imgBusiness){
				$data['business'] = $imgBusiness;
			}
        }
		
		// 判断有没有文件上传 - 店铺图片
		$external = I('post.external');
        if (!empty($external)){
			$imgExternal = CBWBase64Upload($external, './Public/Repair/data/');
			if($imgExternal){
				$data['external'] = $imgExternal;
			}
        }

		if($modelRecruit->create($data)){
			$rt = $modelRecruit->add();
			if($rt > 0){
				//添加成功,删除user_code表相关记录
				$modelUserCode = M('user_code');
				$modelUserCode->where("account='".$mobile."'")->delete();
				
				$this->redirect('Index/success',array('mobile'=>$mobile, 'password'=>$password));
			}else{
				// 提交失败,删除上传图片
				unlink("./Public/Repair/data/".$data['identity_positive']); // 身份证明正面
				unlink("./Public/Repair/data/".$data['identity_back']);     // 身份证明反面
				unlink("./Public/Repair/data/".$data['identity_body']);     // 身份证明半身
				unlink("./Public/Repair/data/".$data['business']);          // 营业执照
				unlink("./Public/Repair/data/".$data['external']);          // 店铺图片
				$this->error("提交失败.");
			}
		}else{
			$this->error("提交失败.");
		}
	}
	
	//维修招募提交完成
	public function success(){
		
		$mobile = I('get.mobile');
		$password = I('get.password');
		$this->assign("mobile",$mobile);
		$this->assign("password",$password);
		
		//SEO
        $ident ="recruit";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	//城市切换
	public function citys(){
		
		//Banner 1238 x 206
        $modelAdv = M('advs');
        $banner = $modelAdv->field('id, adv_name, thumbnail, link_url')->where("id=5")->find();
		$this->assign("banner",$banner);
		
		//按方位归类
		$position = array(
			'0'=>array('name'=>'华东','province'=>array('山东','江苏','浙江','安徽')),
			'1'=>array('name'=>'华南','province'=>array('广东','福建','广西','海南')),
			'2'=>array('name'=>'中南','province'=>array('河南','湖北','湖南','江西')),
			'3'=>array('name'=>'东北','province'=>array('辽宁','黑龙江','吉林')),
			'4'=>array('name'=>'西南','province'=>array('四川','云南','贵州','西藏')),
			'5'=>array('name'=>'华北','province'=>array('河北','山西','内蒙古')),
			'6'=>array('name'=>'西北','province'=>array('陕西','新疆','甘肃','宁夏','青海')),
			'7'=>array('name'=>'其他','province'=>array('香港','澳门','台湾','钓鱼岛')),
		);
		
		//需要去掉的字符
		$strs = array('省','市','自治区','回族','壮族','维吾尔','特别行政区');
		
		//城市信息
		$modelProvince = M('addr_province');
		$modelCity = M('addr_city');
		$provinces = $modelProvince->field('id,addr')->select();
		
		$data = array();
		
		foreach($position as $key=>$item){
			$newProvince = array();
			foreach($provinces as $k=>$v){
				$addr = $v['addr'];
				//去除字符
				foreach($strs as $s){
					$addr = str_replace($s, "", $addr);
				}
				if(in_array($addr,$item['province'])){
					$data[$key]['name'] = $item['name'];
					$citys = $modelCity->field('id,addr,quan_pin,jian_pin')->where('one_id='.$v['id'])->select();
					foreach($citys as $ky=>$c){
						$citys[$ky]['addr'] = str_replace("市", "", $c['addr']);
					}
					$newProvince[$k]['id'] = $province['id'];
					$newProvince[$k]['addr'] = $addr;
					$newProvince[$k]['citys'] = $citys;
					$data[$key]['province'] = $newProvince;
				}
			}
		}
		$this->assign("datas",$data);
		
		
		//SEO
        $ident ="citys";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	/**
     * 切换城市 更改session操作
     * @date 2015-01-18
     * @return void 
     */
    public function edit_city()
    {
        $id = I('post.id');

        // 实例化数据模型
		$modelCity = M('addr_city');
        //$modelProvince = M('addr_province');
		
		$data = $modelCity->alias("c")
				->field("c.addr AS city,p.addr AS province")
				->join("__ADDR_PROVINCE__ AS p ON p.id=c.one_id")
				->where("c.id=".$id)
				->find();
		
		//保存到cookie
		cookie('province', $data['province'], array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
		cookie('city',     $data['city'],     array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
		
		//保存到session
		session('location',$data);   
		
		echo '1';exit;
    }

}