<?php
namespace   Home\Controller;

class SsystemController extends BaseController {
	
	public function __construct(){
		
		parent::__construct();
		
		$server_id = session('server.serverid');
		if($server_id == ""){
			$this->error("未登陆,请先登陆!",U('Login/server'));
		}
    }
	
	public function index(){
		
		$server_id = session('server.serverid');
		if($server_id != ""){
			$this->redirect('Ssystem/shopping');
		}
		
		$this->display();
	}
	
	// 购物订单
	public function shopping(){
		$keyword = I('get.keyword');
		$start   = I('get.start');
		$end     = I('get.end');
		$status  = I('get.status');
		
		$orders    = D('Orders');
		$products  = D('Products');
		
		if(isset($status) && $status !== ''){
			if($status == 0){
				$ostatus = 0;
			}elseif($status == 1){
				$ostatus = 1;
				$pay     = 0;
			}elseif($status == 2){
				$sstatus = 1;
				$pay     = 1;
			}elseif($status == 3){
				$sstatus = 2;
			}elseif($status == 4){
				$ostatus = 2;
			}
		}

		$count    = $orders->getOrdersCountByShopId(array("shopId"=>0, "sstatus"=>$sstatus, 'keyword'=>$keyword, 'pay'=>$pay, 'start'=>$start, 'end'=>$end, 'ostatus'=>$ostatus));
        $page     = new \Think\Page($count, 10);
		$pageShow = $page->show();
		$data     = $orders->getOrdersByShopId(array("shopId"=>0, "sstatus"=>$sstatus, 'keyword'=>$keyword, 'pay'=>$pay, 'start'=>$start, 'end'=>$end, 'ostatus'=>$ostatus, "m"=>$page->firstRow, "n"=>$page->listRows));
		foreach($data as &$vo){
			$vo['schedule'] = $orders->getOrderScheduleByOrderIdAndShopId($vo['id'], 0);
			$product  = $orders->getOrderProductsByOrderId($vo['id']);
			foreach($product as &$v){
				if(!empty($v['pro_attr'])){
					$v['attr_name'] = $products->getAttrNameByAttrId($v['pro_attr']);
				}
				$v['pro_money'] = $v['pro_price']*$v['pro_number'];
			}
			unset($v);
			$vo['product'] = $product;
		}
		unset($vo);
		
		// 查询记录总计金额
		$order  = $orders->getOrdersByShopId(array("shopId"=>0, "sstatus"=>$sstatus, 'keyword'=>$keyword, 'pay'=>$pay, 'start'=>$start, 'end'=>$end, 'ostatus'=>$ostatus));
		$money = 0;
		foreach($order as &$vo){
			$money += $vo['deail_price'];
		}

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->assign('status', $status);
		$this->assign('count', $count);
		$this->assign('money', $money);
		$this->display();
	}
	
	// 订单免运费
	public function exemptFee(){
		$orderId = I('get.id');
		
		$orderSn = CBWCreateOrderNumber();
		
		$orders = D('Orders');
		
		$order    = $orders->getOrderById($orderId);
		$schedule = $orders->getOrderScheduleByOrderIdAndShopId($orderId, 0);
		$rt = $orders->updateOrderSchedule($orderId, 0, array('shop_money'=>$schedule['shop_money']-$schedule['shipping_fee'], 'shipping_fee'=>0));
		if($rt['status'] > 0){
			$rt = $orders->updateOrders($orderId, array('order_sn'=>$orderSn, 'deail_price'=>$order['deail_price']-$schedule['shipping_fee']));
			$this->success("操作成功", U("Ssystem/shopping"));exit;
		}
		$this->error("操作失败");exit;
	}
	
	// 订单改价
	public function changePrice(){
		$orderId = I('post.id');
		$money   = I('post.money');
		$orderSn = CBWCreateOrderNumber();

		$orders = D('Orders');

		$rt = $orders->updateOrderSchedule($orderId, 0, array('shop_money'=>$money, 'shipping_fee'=>0));
		if($rt['status'] > 0){
			$schedule = $orders->getOrderScheduleByOrderId($orderId);
			$deail = 0;
			foreach($schedule as $vo){
				$deail += $vo['shop_money'];
			}
			$rt = $orders->updateOrders($orderId, array('order_sn'=>$orderSn, 'deail_price'=>$deail));
			$this->success("操作成功", U("Ssystem/shopping"));exit;
		}
		$this->error("操作失败");exit;
	}
	
	// 维修订单
	public function repair(){
		$keyword = I('get.keyword');
		$provinceId  = I('get.province');
		$cityId  = I('get.city');
		$areaId  = I('get.area');
		$start   = I('get.start');
		$end     = I('get.end');
		$way     = I('get.way');
		$status  = I('get.status');
		$sstatus = I('get.sstatus');

		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$area      = D('Area');

        $count = $orders->getRepairCountByObject(array("way"=>$way, 'keyword'=>$keyword, 'provinceId'=>$provinceId, 'cityId'=>$cityId, 'areaId'=>$areaId, 'start'=>$start, 'end'=>$end, 'status'=>$status, 'sstatus'=>$sstatus));
        $page = new \Think\Page($count, 8);
		$data = $orders->getRepairByObject(array("way"=>$way, 'keyword'=>$keyword, 'provinceId'=>$provinceId, 'cityId'=>$cityId, 'areaId'=>$areaId, 'start'=>$start, 'end'=>$end, 'status'=>$status, 'sstatus'=>$sstatus, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			if($vo['relation_id'] > 0 && $vo['color_id'] > 0){
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$color    = $repairs->getColorById($vo['color_id']);
				$vo['show_name']  = $relation['pro_name']." ".$color['name']."<br />".$relation['plan_name'];
				$vo['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
				
				//重新评估的维修信息
				if(!empty($vo['relation_ids'])){
					$ids = explode(",",$vo['relation_ids']);
					$plans = array();
					for($i=0;$i<count($ids);$i++){
						$relation = $repairs->getRepairByRelationId($ids[$i]);
						$plans[$i] = $relation['plan_name'];
					}
					$vo['show_name'] = $relation['pro_name']." ".$color['name']."<br />".implode(",",$plans);
				}
			}else{
				$vo['show_name']  = $vo['remarks'];
				$vo['show_img']   = '/Public/Home/images/2016/logo_03.png';
			}
		}
		
		// 查询记录总计金额
		$orders = $orders->getRepairByObject(array("way"=>$way, 'keyword'=>$keyword, 'provinceId'=>$provinceId, 'cityId'=>$cityId, 'areaId'=>$areaId, 'start'=>$start, 'end'=>$end, 'status'=>$status));
		$money = 0;
		foreach($orders as &$vo){
			if($vo['new_price'] > 0){
				$money += $vo['new_price'];
			}else{
				$money += $vo['deail_price'];
			}
		}
		
		//加载城市信息
		if($provinceId > 0){
			$provinceInfo = $area->getProvinceById($provinceId);
			$oneId = $provinceInfo['id'];
		}
		if($cityId > 0){
			$cityInfo = $area->getCityById($cityId);
			$twoId = $cityInfo['id'];
		}
		if($areaId > 0){
			$areaInfo = $area->getAreaById($areaId);
			$oneId = $areaInfo['one_id'];
			$twoId = $areaInfo['two_id'];
			$threeId = $areaInfo['id'];
		}
		$p = empty($oneId)?'选择省':$oneId;
		$c = empty($twoId)?'选择市':$twoId;
		$a = empty($threeId)?'选择县/区':$threeId;
        $city = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$c = \Think\Area::city($city);

		$this->assign('count', $count);
		$this->assign('money', $money);
        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		$this->assign("keyword",$keyword);
		$this->assign("start",$start);
		$this->assign("end",$end);
		$this->assign("way",$way);
		$this->assign("status",$status);
		$this->assign("sstatus",$sstatus);
		$this->assign("city", $c);
		$this->display();
	}
	
	// 回收订单
	public function recovery(){
		
		// 获取查询订单的类型
		$keyword   = I('get.keyword');
		$provinceId  = I('get.province');
		$cityId    = I('get.city');
		$areaId    = I('get.area');
		$start     = I('get.start');
		$end       = I('get.end');
		$way       = I('get.way');
		$status    = I('get.status');
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$area      = D('Area');
		
		
		$count = $orders->getRecoveryCountByObject(array("way"=>$way, 'keyword'=>$keyword, 'provinceId'=>$provinceId, 'cityId'=>$cityId, 'areaId'=>$areaId, 'start'=>$start, 'end'=>$end, 'status'=>$status));
        $page = new \Think\Page($count, 8);
		$data = $orders->getRecoveryByObject(array("way"=>$way, 'keyword'=>$keyword, 'provinceId'=>$provinceId, 'cityId'=>$cityId, 'areaId'=>$areaId, 'start'=>$start, 'end'=>$end, 'status'=>$status, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$repair = array();
			if($vo['reco_id'] > 0){
				$repair = $repairs->getRepairById($vo['reco_id']);
				$vo['pro_attr'] = implode('|', $repairs->getAttribute($vo['reco_attr']));
			}
			$vo['product'] = $repair;	
		}
		
		
		// 查询记录总计金额
		$orders = $orders->getRecoveryByObject(array("way"=>$way, 'keyword'=>$keyword, 'provinceId'=>$provinceId, 'cityId'=>$cityId, 'areaId'=>$areaId, 'start'=>$start, 'end'=>$end, 'status'=>$status));
		$money = 0;
		foreach($orders as &$vo){
			if($vo['new_price'] > 0){
				$money += $vo['new_price'];
			}else{
				$money += $vo['deail_price'];
			}
		}
		
		//加载城市信息
		if($provinceId > 0){
			$provinceInfo = $area->getProvinceById($provinceId);
			$oneId = $provinceInfo['id'];
		}
		if($cityId > 0){
			$cityInfo = $area->getCityById($cityId);
			$twoId = $cityInfo['id'];
		}
		if($areaId > 0){
			$areaInfo = $area->getAreaById($areaId);
			$oneId = $areaInfo['one_id'];
			$twoId = $areaInfo['two_id'];
			$threeId = $areaInfo['id'];
		}
		$p = empty($oneId)?'选择省':$oneId;
		$c = empty($twoId)?'选择市':$twoId;
		$a = empty($threeId)?'选择县/区':$threeId;
        $city = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$c = \Think\Area::city($city);

		$this->assign('count', $count);
		$this->assign('money', $money);
        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		$this->assign("keyword",$keyword);
		$this->assign("start",$start);
		$this->assign("end",$end);
		$this->assign("way",$way);
		$this->assign("status",$status);
		$this->assign("city", $c);
		$this->display();
	}
	
	//导出订单
	public function export(){
		$typeId = I('get.type');
		$keyword   = I('get.keyword');
		$start     = I('get.start');
		$end       = I('get.end');
		$status    = I('get.status');
		
		$orders = D('Orders');
		$users  = D('Users');
		
		$data = $orders->getOrderList(array('type'=>$typeId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status));
		$i = 0;
		foreach($data as $vo){
			$user = $users->getUserById($vo['user_id']);
			$xlsData[$i]['id']              = $vo['id'];
			$xlsData[$i]['shop_id']         = $vo['shop_id'];
			$type = '购物订单';
			if($typeId == 1){
				$type = '维修订单';
			}elseif($typeId == 2){
				$type = '回收订单';
			}
			$xlsData[$i]['order_sn']        = $vo['order_sn'];
			$xlsData[$i]['order_type']      = $type;
			$xlsData[$i]['mobile']          = $user['mobile'];
			if($user['group_id'] == 1){
				$group = "店铺会员";
			}else{
				$group = "普通会员";
			}
			$xlsData[$i]['user_group']      = $group;
			$xlsData[$i]['deail_price']     = $vo['deail_price'];
			$xlsData[$i]['reciver_user']    = $vo['reciver_user'];
			$xlsData[$i]['reciver_phone']   = $vo['reciver_phone'];
			$xlsData[$i]['reciver_address'] = $vo['pro_addr'].$vo['city_addr'].$vo['addr'].$vo['address'];
			if($vo['order_status'] == 0){
				$status = '已取消';
			}elseif($vo['order_status'] == 2){
				$status = '等待付款';
			}elseif($vo['order_status'] == 2){
				$status = '等待发货';
			}elseif($vo['order_status'] == 3){
				$status = '已发货';
			}elseif($vo['order_status'] == 4){
				$status = '已收货';
			}elseif($vo['order_status'] == 5){
				$status = '已完成';
			}elseif($vo['order_status'] == 6){
				$status = '申请退款';
			}
			$xlsData[$i]['status']          = $status;
			$xlsData[$i]['order_date']      = date('Y-m-d H:i:s', $vo['order_date']);
			$i++;
		}
		
		$xlsName  = "orders";
        $xlsCell  = array(
			array('id','编号'),
			array('shop_id','店铺编号'),
			array('order_sn','订单号'),
			array('order_type','订单类型'),
			array('mobile','会员账号'),
			array('user_group','会员组'),
			array('deail_price','订单金额'),
			array('reciver_user','收件人'),
			array('reciver_phone','收件电话'),
			array('reciver_address','收件地址'),
			array('status','订单状态'),
			array('order_date','下单时间')
        );
		
        CBWExportExcel($xlsName,$xlsCell,$xlsData);
	}
	
	//修改订单价格
	public function edit_price(){
		
		$id = I('post.orderid');
		$server_id = session('server.serverid');
		$user_name = session('server.username');
		$price = I('post.price');
		
		$order_sn = CBWCreateOrderNumber();
		
		$modelOrders = M('orders');
		$info = $modelOrders->field("order_sn,deail_price")->where("id=".$id)->find();
		$id = $modelOrders->where("id=".$id)->save(array('order_sn'=>$order_sn, 'deail_price'=>$price));
		if($id > 0){
			// 写入日志
			$content = '客服(ID:' . $server_id . ')修改了订单价格,原订单号:'.$info['order_sn'].',原价格:'.$info['deail_price'];
			setLog($server_id, $user_name, $content);
		
			echo $id;exit;
		}else{
			echo "0";exit;
		}
		
	}
	
	//回收订单付款操作,改变订单状态
	public function recovery_payment(){
		
		$id = I('post.id');
		$type = I('post.type');

		$modelOrders = M('orders');
		$data = array();
		if($type == "recovery"){
			$data['ok_date'] = time();
			$data['order_status']=5;
		}else{
			$data['order_status']=2;
			$data['pay_status']=1;
		}
		
		$id = $modelOrders->where("id=".$id)->save($data);
		if($id > 0){
			echo $id;exit;
		}else{
			echo "0";exit;
		}
		
	}
	
	//回收订单退回发货操作
	public function recovery_return(){
		
		$orderId = I('post.id');      //订单ID
		$server_id = session('server.serverid');
		$user_name = session('server.username');
		
		$modelOrders = M('orders');
		$data['shipping_way'] = I('post.shipping_way');
		$data['shipping_num'] = trim(I('post.shipping_num')); 
		$data['cancel_date'] = time();
		$data['order_status'] = 0;
		$affectedOrderInfo = $modelOrders->where("id=".$orderId)->save($data);
		if($affectedOrderInfo >０){
			
			//快递100 订阅请求
			$modelCompany = M('shipping_company');
			$shipping = $modelCompany->where('id='.$data['shipping_way'])->find();
			kd100($shipping['com'], $data['shipping_num']);
			
			// 写入日志
			$content = '客服(ID:' . $server_id . ')对回收订单id为'.$orderId.'的订单增加了'.$data['shipping_way'].'物流'.$data['shipping_num'];
			setLog($server_id, $user_name, $content);
			
			echo "1";exit;
			exit;
		}else{
			echo "0";exit;
			exit;
		}	
	}
	
	//购物订单发货列表
	public function productsend(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$orders    = D('Orders');
		$products  = D('Products');
		$shippings = D('Shippings');

		$count    = $orders->getOrdersCountByShopId(array("shopId"=>0, "sstatus"=>1, 'keyword'=>$keyword, 'pay'=>1, 'start'=>$start, 'end'=>$end, 'ostatus'=>1));
        $page     = new \Think\Page($count, 10);
		$pageShow = $page->show();
		$data     = $orders->getOrdersByShopId(array("shopId"=>0, "sstatus"=>1, 'keyword'=>$keyword, 'pay'=>1, 'start'=>$start, 'end'=>$end, 'ostatus'=>1, "m"=>$page->firstRow, "n"=>$page->listRows));
		foreach($data as &$vo){
			$vo['schedule'] = $orders->getOrderScheduleByOrderIdAndShopId($vo['id'], 0);
			$product  = $orders->getOrderProductsByOrderId($vo['id']);
			foreach($product as &$v){
				$v['attr_name'] = $products->getAttrNameByAttrId($v['pro_attr']);
				$v['pro_money'] = $v['pro_price']*$v['pro_number'];
			}
			unset($v);
			$vo['product'] = $product;
		}
		unset($vo);
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('keyword', $keyword);
		$this->assign('end', $end);
		$this->assign("shipping", $shipping);
		$this->display();
	}
	
	//购物订单已发货列表
	public function productsended(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$orders    = D('Orders');
		$products  = D('Products');
		$shippings = D('Shippings');

		$count    = $orders->getOrdersCountByShopId(array("shopId"=>0, "sstatus"=>2, 'keyword'=>$keyword, 'pay'=>1, 'start'=>$start, 'end'=>$end, 'ostatus'=>1));
        $page     = new \Think\Page($count, 10);
		$pageShow = $page->show();
		$data     = $orders->getOrdersByShopId(array("shopId"=>0, "sstatus"=>2, 'keyword'=>$keyword, 'pay'=>1, 'start'=>$start, 'end'=>$end, 'ostatus'=>1, "m"=>$page->firstRow, "n"=>$page->listRows));
		foreach($data as &$vo){
			$vo['schedule'] = $orders->getOrderScheduleByOrderIdAndShopId($vo['id'], 0);
			$product  = $orders->getOrderProductsByOrderId($vo['id']);
			foreach($product as &$v){
				$v['attr_name'] = $products->getAttrNameByAttrId($v['pro_attr']);
				$v['pro_money'] = $v['pro_price']*$v['pro_number'];
			}
			unset($v);
			$vo['product'] = $product;
		}
		unset($vo);
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('keyword', $keyword);
		$this->assign('end', $end);
		$this->assign('status', $status);
		$this->assign("shipping", $shipping);
		$this->display();
	}
	
	//发货清单
	public function printlist(){
		$orderId  = I('get.id');      //订单ID
	
		$orders    = D('Orders');
		$products  = D('Products');
		$service   = D('Service');
		$shippings = D('Shippings');
		
		$order    = $orders->getOrderById($orderId);
		$schedule = $orders->getOrderScheduleByOrderIdAndShopId($order['id'], 0);
		$product  = $orders->getOrderProductsByOrderId($order['id']);
		$money    = 0;
		foreach($product as &$vo){
			$vo['attr_name'] = $products->getAttrNameByAttrId($vo['pro_attr']);
			$vo['service']   = $service->getServiceByOrderId($order['id'], $vo['pro_id']);
			if($vo['shop_id'] == 0){
				$schedule['pro_money'] += $vo['pro_price']*$vo['pro_number'];
			}
		}
		unset($vo);
		
		$shipping = $shippings->getShippingCompanyById($schedule['shipping_way']);
		$schedule['cname']  = $shipping['cname'];

		$this->assign("order", $order);
		$this->assign("schedule", $schedule);
		$this->assign("product", $product);
		$this->display();
	}
	
	//修改收货地址页面
	public function editaddress(){
		$orderId = I('get.id');      //订单ID
		
		$orders = D('Orders');
		$area   = D('Area');
		
		$order = $orders->getOrderById($orderId);
		
		// 三级联动
		$info = $area->getAreaById($order['area_id']);
		$p = empty($info['one_id'])?'选择省':$info['one_id'];
		$c = empty($info['two_id'])?'选择市':$info['two_id'];
		$a = empty($info['id'])?'选择县/区':$info['id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city",$city);
		$this->assign("order",$order);
		$this->display();
	}
	
	//修改收货地址操作
	public function doEditAddress(){
		$orderId = I('post.id');
		
		$orders = D('Orders');

		$data = array(
			'reciver_user'  => trim(I('post.reciver_user')),
			'reciver_phone' => trim(I('post.reciver_phone')),
			'area_id'       => I('post.area'),
			'address'       => I('post.address')
		);	
		$rt = $orders->updateOrders($orderId, $data);
		if($rt['status'] > 0){
			$this->success("更新成功", U("Ssystem/productsend"));exit;
		}
		$this->error("更新失败");exit;
	}
	
	//发货操作
	public function delivery(){
		$orderId = I('post.id');
		$way     = I('post.way');
		$num     = I('post.num');
		
		$orders    = D('Orders');
		$shippings = D('Shippings');
		
		$order    = $orders->getOrderById($orderId);
		$schedule = $orders->getOrderScheduleByOrderIdAndShopId($orderId, 0);
		$shipping = $shippings->getShippingCompanyById($way);

		$rt = $orders->updateOrderSchedule($orderId, 0, array('send_date' => time(), 'shipping_way' => $way, 'shipping_num' => $num, 'shop_status' => 2));
		if($rt['status'] > 0){			
			// 快递100 订阅请求
			kd100($shipping['com'], $num);
			if($schedule['shop_status'] == 1){
				// 给客户发货通知短信
				sendSmsByAliyun($order['reciver_phone'], array(), 'SMS_116566045'); // 发货成功
			}
			echo $rt['status'];exit;
		}else{
			echo 0;exit;
		}
	}
	
	//售后申请
	public function apply(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$type    = I('get.type');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>$type, 'status'=>1));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>$type, 'status'=>1, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			if(!empty($product['pro_attr'])){
				$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			}
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();
		
		$this->assign('shipping', $shipping);
		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->assign('type', $type);
		$this->display();
	}
	
	//同意申请
	public function agree(){
		$id = I('post.id');
		$user_name = session('server.username');
		
		$model = M();
		$modelSaleService = M('sale_service');
		
		//获取订单状态
		$order = $model->table('__ORDERS__ AS o')
					->field('os.shop_status')
					->join('__ORDER_SCHEDULE__ AS os ON os.order_id=o.id')
					->join('__SALE_SERVICE__ AS s ON s.order_id=o.id')
					->where("os.shop_id=0 AND s.id=".$id)
					->find();
		if($order['shop_status'] == '1'){ //如果是等待发货
			$data['service_status'] = 5;
		}else{
			$data['service_status'] = 3;
		}
		$rt = $modelSaleService->where("id=".$id)->save($data);
		if($rt){
			$modelServiceDetails = M("service_details");
			$da = array();
			$da['service_id'] = $id;
			$da['time'] = time();
			$da['operator'] = $user_name;
			$da['message'] = $user_name."通过服务单申请";
			$modelServiceDetails->add($da);
		}
		echo $rt;
		exit;
	}
	
	//不同意申请
	public function disagree(){
		$id = I('post.id');
		$remark = I('post.remark');
		$user_name = session('server.username');
		
		$modelSaleService = M('sale_service');
		$rt = $modelSaleService->where("id=".$id)->save(array('service_status'=>0));
		if($rt){
			$modelServiceDetails = M("service_details");
			$da = array();
			$da['service_id'] = $id;
			$da['time'] = time();
			$da['operator'] = $user_name;
			$da['message'] = $user_name."不通过服务单申请,原因:".$remark;
			$modelServiceDetails->add($da);
		}
		echo $rt;
		exit;
	}
	
	//售后签收
	public function serversign(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$type    = I('get.type');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>$type, 'status'=>3));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>$type, 'status'=>3, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			if(!empty($product['pro_attr'])){
				$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			}
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();
		
		$this->assign('shipping', $shipping);
		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->assign('type', $type);
		$this->display();
	}
	
	//售后快递签收
	public function sign(){
		
		$id = I('post.id');
		$user_name = session('server.username');
		
		$modelSaleService = M('sale_service');
		$ser = $modelSaleService->where("id=".$id)->find();
		if($ser['service_type'] == 1){
			$status = 5;
		}else{
			$status = 4;
		}
		$rt = $modelSaleService->where("id=".$id)->save(array('service_status'=>$status));
		if($rt){
			$modelServiceDetails = M("service_details");
			$da = array();
			$da['service_id'] = $id;
			$da['time'] = time();
			$da['operator'] = $user_name;
			$da['message'] = $user_name."签收服务单";
			$modelServiceDetails->add($da);
			
			echo '1';exit;
		}else{
			echo '0';exit;
		}

	}
	
	//售后发货
	public function serversend(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>2, 'status'=>4));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>2, 'status'=>4, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();
		
		$this->assign('shipping', $shipping);
		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->display();
	}
	
	// 售后发货操作
	public function service_send(){
		
		$id = I('post.id');      //订单ID
		$user_name = session('server.username');
		
		$modelSaleService = M('sale_service');
		$data['return_way'] = I('post.return_way');
		$data['return_num'] = trim(I('post.return_num'));
		$data['service_status'] = 6;
		$rt = $modelSaleService->where("id=".$id)->save($data);
		if($rt >０){
			
			//快递100 订阅请求
			$modelCompany = M('shipping_company');
			$shipping = $modelCompany->where('id='.$data['return_way'])->find();
			kd100($shipping['com'], $data['return_num']);
			
			$modelServiceDetails = M("service_details");
			$da = array();
			$da['service_id'] = $id;
			$da['time'] = time();
			$da['operator'] = $user_name;
			$da['message'] = $user_name."服务单发货.";
			$modelServiceDetails->add($da);
			echo "1";
			exit;
		}else{
			echo "0";
			exit;
		}	
	}
	
	//售后关闭
	public function serveroff(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>0));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>0, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			if(!empty($product['pro_attr'])){
				$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			}
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}

		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->display();
	}
	
	//申诉列表
	public function appeal(){
		$keyword = I('get.keyword');
		$reason  = I('get.reason');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$orders = D('Orders');
		
		$count = $orders->getOrderAppealCountByObject(array('keyword'=>$keyword, 'reason'=>$reason, 'start'=>$start, 'end'=>$end));
        $page = new \Think\Page($count, 10);
		$data = $orders->getOrderAppealByObject(array('keyword'=>$keyword, 'reason'=>$reason, 'start'=>$start, 'end'=>$end, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
	
		$this->assign('page', $pageShow);
		$this->assign('appeal', $data);
		$this->assign('keyword', $keyword);
		$this->assign('reason', $reason);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->display();
	}
	
	// 申诉操作
	public function doAppeal(){
		$type     = I('post.type');
		$appealId = I('post.id');
		
		$orders = D('Orders');
		$users  = D('Users');
		$us     = D('UserShop');
		
		$appeal = $orders->getOrderAppealByAppealId($appealId);
		$order  = $orders->getOrderById($appeal['order_id']);
		if($order['order_status'] == 1){
			if($type == 'agree'){
				if($order['order_type'] == 1){
					$server = $orders->getOrderServerByOrderId($order['id']);
					$shopId = $server['shop_id'];
				}elseif($order['order_type'] == 2){
					$recovery = $orders->getOrderRecoveryByOrderId($order['id']);
					$shopId = $recovery['shop_id'];
				}
				if($shopId > 0){
					$offer = $orders->getOrderOfferByOrderIdAndShopId($order['id'], $shopId);
					if($offer['virtual_money'] > 0){
						$shop = $us->getShopByShopId($shopId);
						$users->giveVirtual($shop['user_id'], $offer['virtual_money'], 6, $order['order_sn']);      // 退还草包豆
					}
				}
				$rt = $orders->updateOrderAppeal($appealId, array('appeal_status'=>2));
			}else{
				$rt = $orders->updateOrderAppeal($appealId, array('appeal_status'=>0));
			}
		}else{
			$rt = $orders->updateOrderAppeal($appealId, array('appeal_status'=>0));
		}
		if($rt['status'] > 0){
			$orders->updateOrders($order['id'], array('order_status'=>0, 'is_offer'=>0));
			echo 1;exit;
		}
		echo 0;exit;
	}
	
	//举报列表
	public function report(){
		
		$model = M();
		
		// 获取查询订单的类型
		$keyword = I('get.keyWord');
		$start = I('get.startDate');
		$end = I('get.endDate');
		
		$where = "r.id>0";
		
		if(!empty($keyword)){
			$where .= " AND (s.shop_name like '%".$keyword."%')";
			$map['keyWord'] = $keyword;
		}
		
		if(!empty($start) && !empty($end)){
			$timeStamp_01 = strtotime($start);
			$timeStamp_02 = strtotime($end);
			$where .= " AND r.create_time < '" . $timeStamp_02 . "' AND r.create_time > '" . $timeStamp_01 . "'";
			$map['startDate'] = $start;
			$map['endDate'] = $end;
		}
		
        $order = "r.create_time DESC";
		
		// 查询总记录数
        $getPageCounts = $model->table('__USER_REPORT__ AS r')
					->field('r.*,u.email,u.mobile,s.shop_name')
					->join('__USERS__ AS u ON u.id=r.user_id')
					->join('__USER_SHOP__ AS s ON s.id=r.report_id')
					->where($where)
					->count();
								
        // 每页显示 $pageSize 条数据
        $pageSize = 10;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);
		
		$reportRes = $model->table('__USER_REPORT__ AS r')
					->field('r.*,u.email,u.mobile,s.shop_name')
					->join('__USERS__ AS u ON u.id=r.user_id')
					->join('__USER_SHOP__ AS s ON s.id=r.report_id')
					->where($where)
					->limit($page->firstRow, $page->listRows)
					->order($order)
					->select();
		$pageShow = $page->show();
		
        $this->assign('page', $pageShow);
		$this->assign("reportRes",$reportRes);
		$this->assign("keyWord",$keyword);
		$this->assign("startDate",$start);
		$this->assign("endDate",$end);
		
		$this->display();
	}
	
	//举报状态修改
	public function doReportStatus(){
		
		$id = I('post.id');       //举报ID
		$res = I('post.result');  //举报结果
		$server_id = session('server.serverid');
		
		$modelReport = M('user_report');
		$report = $modelReport->where("id=".$id)->find();
		$rt = $modelReport->where("id=".$id)->save(array('res'=>$res, 'oper_id'=>$server_id, 'status'=>1));
		if($rt){
			$modelUserShop = M('user_shop');
			$rt = $modelUserShop->where("id=".$report['report_id'])->save(array('shop_status'=>0));
			echo 1;
			exit;
		}else{
			echo 0;
			exit;
		}	
	}
	
	//举报信息
	public function report_info(){
		
		$sn = I('post.sn');      //举报ID

		$model = M();
		$reportRes = $model->table('__USER_REPORT__ AS r')
					->field('r.*,u.uname,u.email,u.mobile,s.shop_name')
					->join('__USERS__ AS u ON u.id=r.user_id')
					->join('__USER_SHOP__ AS s ON s.id=r.report_id')
					->where("report_sn='".$sn."'")
					->find();
		echo json_encode($reportRes);
		exit;
	}
	
	//订单信息
	public function order_info(){
		
		$id = I('post.id');
		$modelOrders = M('orders');
		$order = $modelOrders->field('id,user_id,order_sn,order_type,deail_price,pay_mode,area_id,reciver_user,reciver_phone,address,shipping_way,remarks')->where("id=".$id)->find();
		
		$data = array();		
		//获取地址信息
		$area = M('addr_area');
		$adderss = $area->alias('a')
				->field('a.addr,p.addr as pro_addr,c.addr as city_addr')
				->join('__ADDR_PROVINCE__ AS p ON p.id = a.one_id')
				->join('__ADDR_CITY__ AS c ON c.id = a.two_id')
				->where("a.id = ".$order['area_id'])
				->find();
		$data[] = array('联系人',$order['reciver_user']);
		$data[] = array('联系电话',$order['reciver_phone']);
		$data[] = array('收货地址',$adderss['pro_addr'].$adderss['city_addr'].$adderss['addr'].$order['address']);
		
		
		if($order['order_type'] == '0'){    //购物订单
	
			$modelOrderProducts = M('order_products');
			$modelAttr = M('product_attr');
			$products = $modelOrderProducts->alias('op')
						->field("op.pro_attr,p.pro_name")
						->join('__PRODUCTS__ AS p ON p.id=op.pro_id')
						->where("op.order_id=".$order['id'])
						->select();
			foreach($products as $key=>$item){
				$data[] = array('商品',$item['pro_name']);
				//商品属性名称
				$attr_name = array();
				if(!empty($item['pro_attr'])){
					$attrs = explode("|",$item['pro_attr']);
					foreach($attrs as $attr){
						$attrinfo = $modelAttr->where("id=".$attr)->find();
						$attr_name[] = $attrinfo['attr_value'];
					}
					$data[] = array('属性',implode("|",$attr_name));
				}
			}
		}elseif($order['order_type'] == '1'){   //维修订单
			$modelOrderServer = M('order_server');
			$server = $modelOrderServer->where("order_id=".$order['id'])->find();
			$modelRelation = M('repair_relation');
			$info = $modelRelation->alias("r")
						->field("c.cat_name,p.pro_name,f.name as fault_name,p2.name as plan_name")
						->join('__REPAIRS__ p ON r.product_id = p.id')
						->join('__PRODUCT_CAT__ c ON p.brand_id = c.id')
						->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
						->join('__REPAIR_PLAN__ p2 ON r.plan_ids = p2.id')
						->where("r.id=".$server['relation_id']) 
						->find();
			$data[] = array('商品',$info['cat_name'].$info['pro_name']);
			$data[] = array('维修方案',$info['plan_name']);
			
			//重新评估的维修信息
			if(!empty($server['relation_ids'])){
				$ids = explode(",",$server['relation_ids']);
				for($i=0;$i<count($ids);$i++){
					$relation = $modelRelation->alias("r")
						->field("p.name as plan_name")
						->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
						->join('__REPAIR_PLAN__ p ON r.plan_ids = p.id')
						->where("r.id=".$ids[$i])
						->find();
					$newInfo[$i] = $relation['plan_name'];
				}
				$data[] = array('新维修方案',implode(",",$newInfo));
			}
			
			//维修师信息
			if($server['shop_id'] > 0){
				$modelUserShop = M('user_shop');
				$shop = $modelUserShop->field("shop_name,user_name,user_phone")->where("id=".$server['shop_id'])->find();
				$data[] = array('维修师',$shop['shop_name']." - ".$shop['user_name']." - ".$shop['user_phone']);
			}
			
		}
		
		$data[] = array('价格',$order['deail_price']);
		$data[] = array('备注',empty($order['remarks'])?"无":$order['remarks']);
		
		if($order['shipping_way'] != '0'){
			$modelCompany = M('shipping_company');
			$company = $modelCompany->where("id=".$order['shipping_way'])->find();
			$data[] = array('快递',$company['cname']);
		}

		echo json_encode($data);
		exit;
	}
	
	//售后订单信息
	public function service_info(){
		
		$id = I('post.id');
		$model = M();
		$sale_service = $model->table("__SALE_SERVICE__ AS s")
							->field('s.*,u.mobile,o.order_sn')
							->join("__USERS__ AS u ON u.id=s.user_id")
							->join("__ORDERS__ AS o ON o.id=s.order_id")
							->where("s.id=".$id)
							->find();				
		$data = array();	
		
		$data[] = array('售后单号',$sale_service['service_sn']);
		$data[] = array('订单号',$sale_service['order_sn']);
		$data[] = array('会员账号',$sale_service['mobile']);
		if($sale_service['shipping_way'] != '0'){
			$modelCompany = M('shipping_company');
			$company = $modelCompany->where("id=".$sale_service['shipping_way'])->find();
			$data[] = array('快递',$company['cname']);
			$data[] = array('快递单号',$sale_service['shipping_num']);
		}
		$data[] = array('备注',empty($sale_service['description'])?"无":$sale_service['description']);

		echo json_encode($data);
		exit;
	}
	
	//物流信息
	public function shipping(){
		
		$way = I('post.way');      //物流公司编号
		$no  = I('post.no');
		
		//物流信息
		$modelCompany = M("shipping_company");
		$company = $modelCompany->field("cname")->where("id = ".$way)->find();
		
		//物流信息
		$modelExpress = M('express');
		$express = $modelExpress->where("number='".$no."'")->find();
		$con = CBWOjectArray(json_decode($express['content']));
		
		if(!empty($con)){
			foreach($con as $key=>$vo){
				$expInfo[$key]['time'] = $vo['time']; 
				$expInfo[$key]['context'] = $vo['context'];
			}
		}
		
		echo json_encode(array($company['cname'],$expInfo));
		exit;
	}
	
	// 提现
	public function withdraw(){
	
		$model = M();
		
		$type_id = I('get.type_id');
		$where = "w.id>0";
		
		if(!empty($type_id)){
			$where .= " AND w.type_id=".$type_id;
			$map['type_id'] = $type_id;
		}
		
        $order = "w.create_time DESC";
		
		// 查询总记录数
        $getPageCounts = $model->table('__USER_WITHDRAW__ AS w')
					->join('__USERS__ AS u ON u.id=w.user_id')
					->where($where)
					->count();
								
        // 每页显示 $pageSize 条数据
        $pageSize = 10;
        // 实例化分页类
        $page = new \Think\Page($getPageCounts, $pageSize, $map);
		
		$withdrawRes = $model->table('__USER_WITHDRAW__ AS w')
					->field('w.*,u.mobile')
					->join('__USERS__ AS u ON u.id=w.user_id')
					->where($where)
					->limit($page->firstRow, $page->listRows)
					->order($order)
					->select();
		foreach($withdrawRes as $key=>$vo){
			$withdrawRes[$key]['infos'] = unserialize($vo['infos']);
		}
		$pageShow = $page->show();
		
        $this->assign('page', $pageShow);
		$this->assign("withdrawRes",$withdrawRes);
		$this->assign("type_id",$type_id);
		
		$this->display();
	}
	
	// 提现申请 批量修改状态
	public function batch_withdraw(){
		
		$id = I('post.id');
		$ids = I('post.ids');
		
		$modelWithdraw = M('user_withdraw');
		$modelUserInfo = M('user_info');
		
		if(!empty($id)){
			$rt = $modelWithdraw->where('id='.$id)->save(array('status'=>1));
			if($rt>0){
				// 去除冻结金额
				$withdraw = $modelWithdraw->where('id='.$id)->find();
				$user = $modelUserInfo->where('user_id='.$withdraw['user_id'])->find();
				$modelUserInfo->where("user_id=".$withdraw['user_id'])->save(array('drawing_money'=>$user['drawing_money']-$withdraw['money']));

				echo "1";exit;
			}
		}elseif(!empty($ids)){
			foreach($ids as $d){
				$rt = $modelWithdraw->where('id='.$d)->save(array('status'=>1));
				if($rt>0){
					// 去除冻结金额
					$withdraw = $modelWithdraw->where('id='.$d)->find();
					$user = $modelUserInfo->where('user_id='.$withdraw['user_id'])->find();
					$modelUserInfo->where("user_id=".$withdraw['user_id'])->save(array('drawing_money'=>$user['drawing_money']-$withdraw['money']));
				}
			}
			echo "1";exit; 
		}
		echo "0";exit;
	}
	
	//导出excel
	public function load_excel(){

		$model = M();
		
		$type_id = I('get.type_id');
		$where = "w.status=0";
		$order = "w.create_time DESC";
		
		if(!empty($type_id) && $type_id !='undefined'){
			$where .= " AND w.type_id=".$type_id;
		}
		
		$withdrawRes = $model->table('__USER_WITHDRAW__ AS w')
					->field('w.*,u.mobile')
					->join('__USERS__ AS u ON u.id=w.user_id')
					->where($where)
					->order($order)
					->select();
		foreach($withdrawRes as $key=>$vo){
			$xlsData[$key]['id'] = $vo['id'];
			$xlsData[$key]['userinfo'] = $vo['user_id']."/".$vo['mobile'];
			
			$infos = unserialize($vo['infos']);
			if($vo['type_id'] == '1'){
				$xlsData[$key]['name']   = $infos['name'];
				$xlsData[$key]['alipay'] = $infos['alipay'];
			}else{
				$xlsData[$key]['bank_name'] = $infos['bank_name'];
				$xlsData[$key]['name']      = $infos['name'];
				$xlsData[$key]['card_no']   = $infos['card_no']." ";
			}
			$xlsData[$key]['create_time'] = date("Y-m-d H:i:s",$vo['create_time']); 
			$xlsData[$key]['money'] = $vo['money']; 
		}
		
		$xlsName  = "Withdraw";
        $xlsCell  = array(
			array('id','序号'),
			array('userinfo','用户编号/手机号码'),
			array('name','姓名'),
			array('alipay','支付宝账号'),
			array('bank_name','开户银行'),
			array('card_no','收款账号'),
			array('create_time','申请时间'),
			array('money','金额')
        );
        CBWExportExcel($xlsName,$xlsCell,$xlsData);
	}
	
	//购物退款
	public function refund(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>1, 'status'=>5));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>1, 'status'=>5, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();
		
		$this->assign('shipping', $shipping);
		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->display();
	}
	
	//退款记录
	public function refundlist(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>1, 'status'=>6));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'type'=>1, 'status'=>6, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			if(!empty($product['pro_attr'])){
				$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			}
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();
		
		$this->assign('shipping', $shipping);
		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->display();
	}
	
	//售后服务单退款操作
	public function service_refund(){
		$id = I('post.id');
		$user_name = session('server.username');
		
		$orders  = D('Orders');
		$users   = D('Users');
		$service = D('Service');
		
		$rt = $service->updateService($id, array('service_status'=>6));
		if($rt['status'] > 0){
			// 订单信息
			$order = $service->getServiceByServinceId($id);
			$orders->updateOrderProduct($order['order_id'], $order['pro_id'], array('status'=>0));
			$products = $orders->getOrderProductsByOrderId($order['order_id']);
			$i = 1;
			foreach($products as $vo){
				if($vo['status']){
					$i = 0;
				}
			}
			if($i == 1){
				$orders->closeOrders($order['order_id']);
			}
			
			// 退款成功,如果该订单有冻结金额记录,则扣冻结收益金额与用户账户金额的对应商品金额
			$ops = $orders->getOrderProductByOrderId($order['order_id'], $order['pro_id']);
			$frozens = $users->getFrozenMoneyByObject(array('orderId'=>$order['order_id'], 'status'=>0));
			if(!empty($frozens)){
				foreach($frozens as $vo){
					$user = $users->getUserAndInfoById($vo['user_id']);
					if($user['is_agent'] == 1){
						$userMoney = ($ops['two_price'] - $ops['three_price']) * $ops['pro_number'];
					}elseif($user['group_id'] == 1){
						$userMoney = ($ops['pro_price'] - $ops['two_price']) * $ops['pro_number'];
					}
					if($userMoney > 0){
						$users->decreaseMoney($user['id'], $order['id'], $userMoney, 6, '订单退货,扣除收益');                 // 扣除用户账户金额
						$users->updateUser(array('user_id'=>$user['id'], 'frozen_money'=>$user['frozen_money']-$userMoney));  // 减去用户账户冻结金额
						$money = $vo['money'] - $userMoney;
						if($money > 0){
							$users->updateFrozenMoney($vo['id'], array('money'=>$money));
						}else{
							if($money == 0){
								$users->deleteFrozenMoney($vo['id']);
							}
						}
					}
				}
			}
			
			$data = array(
				'service_id' => $id,
				'time'       => time(),
				'operator'   => $user_name,
				'message'    => $user_name."为服务单退款."
			);
			$service->addServiceDetails($data);
			
			echo '1';exit;
		}else{
			echo '0';exit;
		}

	}
	
	//帮助说明
	public function explain(){
		
		$this->display();
	}

}