<?php
namespace   Home\Controller;

class RepairController extends BaseController {
    
	public function __construct(){
		parent::__construct();
    }
	
	// 第一步 选择类型
	public function select(){
		//清除SESSION
		session('server', null);
		
		// SEO
        $ident ="repair";
        $idents =$this->seo($ident);
        $this->assign("title",empty($title)?$idents['title']:$title);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	// 第二部 选择品牌
	public function two(){
		//清除SESSION
		session('server', null);
		
		$typeId = I('get.type');
		
		$modelProductCat = M('product_cat');
		
		// 步骤 面包屑导航
		$step = $this->_steps($typeId);
		$this->assign('step',$step);
		
		// 品牌
		$brandRes = $modelProductCat->where('fid='.$typeId.' AND is_show=1')->order('sort ASC')->select();
		foreach($brandRes as $vo){
			$keyword[] = $vo['cat_name'].$step['type']."维修";
		}
		$this->assign('brandRes',$brandRes);
		
		// 保存SESSION
		session('server.typeId', $typeId);
		
		//SEO
        $this->assign("title","专业修".$step['type']."、草包网".$step['type']."维修平台");
        $this->assign("keywords",implode(",", $keyword));
        $this->assign("description","草包网支持国内外主流".$step['type']."快修服务，维修方式为：上门服务，到店服务，邮寄维修服务，自助维修服务.");
		
		$this->display();
	}
	
	// 第三步 选择型号
	public function three(){
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		
		$modelRepairs = M('repairs');
		
		// 步骤 面包屑导航
		$step = $this->_steps($typeId, $brandId);
		$this->assign('step',$step);
		
		// 型号
		$modelRes = $modelRepairs->field('id,pro_name,list_image')->where('is_on_sale=1 AND type_id='.$typeId.' AND brand_id='.$brandId)->order('sort DESC')->select();
		$this->assign('modelRes',$modelRes);
		
		// 保存SESSION
		session('server.typeId', $typeId);
		session('server.brandId', $brandId);
		
		//SEO
        $this->assign("title",$step['brand'].$step['type']."维修报价_".$step['brand'].$step['type']."维修价格表");
        $this->assign("keywords",$step['brand'].$step['type']."维修报价,".$step['brand'].$step['type']."维修价格表");
        $this->assign("description","提供最专业的".$step['brand'].$step['type']."维修服务,".$step['brand'].$step['type']."维修报价,".$step['brand'].$step['type']."维修价格表。支持全国上门、邮寄到店三种服务方式。维修".$step['brand'].$step['type']."上草包网，统一真实报价、安全可靠、原厂配件。");

		$this->display();
	}
	
	// 第四步 选择颜色
	public function four(){
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$shopId  = I('get.sid');

		$modelRepairColor = M('repair_color');
		$modelUserShop    = M('user_shop');
		
		// 指定店铺维修
		if($shopId > 0){
			$shop = $modelUserShop->field("id,shop_name")->where("id=$shopId")->find();
			if(!empty($shop)){
				session('server.shopId', $shopId);
			}
		}
		
		// 步骤 面包屑导航
		$step = $this->_steps($typeId, $brandId, $modelId);
		$this->assign('step',$step);
		
		// 颜色
		$colorRes = $modelRepairColor->field('id,name')->where('repair_id='.$modelId)->order('id ASC')->select();
		$this->assign('colorRes',$colorRes);
   
		// 保存SESSION
		session('server.typeId',  $typeId);
		session('server.brandId', $brandId);
		session('server.modelId', $modelId);
		
		//SEO
        $this->assign("title",$step['model']."维修价格_".$step['model']."维修多少钱");
        $this->assign("keywords",$step['model']."维修价格,".$step['model']."维修多少钱");
        $this->assign("description","提供最专业的".$step['model'].$step['type']."维修服务,查询".$step['model'].$step['type']."维修报价,".$step['model'].$step['type']."维修多少钱。支持全国上门、邮寄到店三种服务方式，维修".$step['model'].$step['type']."上草包网。");

		$this->display();
	}
	
	// 第五步 选择故障分类
	public function five(){
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$colorId = I('get.color');

		$m = M();
		
		// 步骤 面包屑导航
		$step = $this->_steps($typeId, $brandId, $modelId, $colorId);
		$this->assign('step',$step);
		
		// 故障分类
		$catRes = $m->table('__REPAIR_FAULT__ AS f')
						->field('c.id,c.name,f.id as fault_id')
						->join('__REPAIR_FAULT_CAT__ AS c ON c.id = f.cat_id')
						->where("c.is_show=1 AND c.cat_id=$typeId AND f.product_id=$modelId")
						->group('f.cat_id')
						->order('c.sort ASC')
						->select();
		$this->assign('catRes',$catRes);

		// 保存SESSION
		session('server.colorId', $colorId);
		
		//SEO
        $this->assign("title",$step['color'].'色'.$step['model']."维修价格_".$step['color'].'色'.$step['model']."维修多少钱");
        $this->assign("keywords",$step['color'].'色'.$step['model']."维修价格,".$step['color'].'色'.$step['model']."维修多少钱");
        $this->assign("description","提供最专业的".$step['color'].'色'.$step['model'].$step['type']."维修服务,查询".$step['color'].'色'.$step['model'].$step['type']."维修报价,".$step['color'].'色'.$step['model'].$step['type']."维修多少钱。支持全国上门、邮寄到店三种服务方式，维修".$step['color'].'色'.$step['model'].$step['type']."上草包网。");
		
		$this->display();
	}
	
	// 第六步 选择故障
	public function six(){
		$typeId  = I('get.type');
		$brandId = I('get.brand');
		$modelId = I('get.model');
		$colorId = I('get.color');
		$catId   = I('get.cat');
		
		$m = M();
		
		// 步骤 面包屑导航
		$step = $this->_steps($typeId, $brandId, $modelId, $colorId, $catId);
		$this->assign('step',$step);
		
		// 相关故障
		$faultRes = $m->table('__REPAIR_RELATION__ as r')
						->field('r.id,r.plan_ids,f.id as fault_id,f.name as fault_name,f.description,c.name as cat_name,p.id as plan_id,p.name as plan_name,p.money')
						->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
						->join('__REPAIR_FAULT_CAT__ c ON c.id = f.cat_id')
						->join('__REPAIR_PLAN__ p ON r.plan_ids = p.id')
						->where('r.product_id='.$modelId.' AND f.cat_id='.$catId)
						->order('f.sort DESC')
						->select();
		foreach($faultRes as $key=>$vo){
			$faults[] = $vo['fault_name'];
			$moneys = json_decode($vo['money']);
			foreach($moneys as $k=>$value){
				if($colorId == $k){
					if($value > 0){
						$faultRes[$key]['money'] = number_format($value, 2, '.', '');
					}else{
						$faultRes[$key]['money'] = '依据检测结果';
					}
				}
			}
		}
		$this->assign('faultRes',$faultRes);
		
		// 保存SESSION
		session('server.catId', $catId);
		
		//SEO
        $this->assign("title",$step['color'].'色'.$step['model'].implode("_" , $faults)."维修价格_多少钱");
        $this->assign("keywords",$step['color'].'色'.$step['model'].implode("_" , $faults)."维修价格,".$step['color'].'色'.$step['model'].implode("_" , $faults)."维修多少钱");
        $this->assign("description","提供最专业的".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."维修服务,查询".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."维修报价,".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."维修多少钱。支持全国上门、邮寄到店三种服务方式，维修".$step['color'].'色'.$step['model'].implode("_" , $faults).$step['type']."上草包网。");

		$this->display();
	}
	
	// 第七步 填写备注
	public function serven(){
		$faultId = I('get.fault');
		
		// 保存SESSION
		if($faultId > 0){
			session('server.faultId', $faultId);
		}

		$repairs = D('Repairs');
		$ua      = D('UserAddress');
		
		$server = session('server');
        $city=session('location.city');
        $city=explode('市',$city)[0];
        if(in_array($city,['北京','上海','广州','深圳'])){
            $is_show_tip=1;
        }else{
            $is_show_tip=0;
        }

		// 步骤 面包屑导航
		$step = $repairs->steps($server['typeId'], $server['brandId'], $server['modelId'], $server['colorId'], $server['catId']);
		$this->assign('step',$step);
 		
		// SEO
        $ident ="repair";
        $idents =$this->seo($ident);
        $this->assign('is_show_tip',$is_show_tip);
        $this->assign("title",empty($title)?$idents['title']:$title);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	// 私有方法 获取面包屑导航
	private function _steps($typeId, $brandId = '', $modelId = '', $colorId = '', $catId = '', $fault = ''){
		$m = M();
		$modelProductCat = M('product_cat');
		$modelRepairs = M('repairs');
		$modelRepairColor = M('repair_color');
		$modelRepairFaultCat = M('repair_fault_cat');
		
		// 类型 
		$types = $modelProductCat->field('cat_name')->where('id='.$typeId)->find();
		$step['type'] = $types['cat_name'];
		
		// 品牌
		if(!empty($brandId)){
			$brands = $modelProductCat->field('cat_name')->where('id='.$brandId)->find();
			$step['brand'] = $brands['cat_name'];
		}
		
		// 型号
		if(!empty($modelId)){
			$models = $modelRepairs->field('pro_name')->where('id='.$modelId)->find();
			$step['model'] = $models['pro_name'];
		}
		
		// 颜色
		if(!empty($colorId)){
			$colors = $modelRepairColor->field('name')->where('id='.$colorId)->find();
			$step['color'] = $colors['name'];
		}
		// 故障分类
		if(!empty($catId)){
			$cats = $modelRepairFaultCat->field('name')->where('id='.$catId)->find();
			$step['cat'] = $cats['name'];
		}
		
		// 相关故障
		if(!empty($fault)){
			$faults = $m->table('__REPAIR_RELATION__ as r')
						->field('f.name')
						->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
						->where("r.product_id=$modelId AND r.fault_id=$fault")
						->find();
			$step['fault'] = $faults['name'];
		}

		return $step;
	}
	
	// 生成订单
	public function create(){
		$brand      = I('post.brand');
		$model      = I('post.model');
		$color      = I('post.color');
		$remarks    = I('post.remarks');
		$username   = I('post.username');
		$mobile     = I('post.mobile');
		$smscode    = I('post.smscode');
		$city       = I('post.city');
		$address    = I('post.address');
		$supplement = I('post.supplement');
		$district   = I('post.district');
		$lng        = I('post.lng');
		$lat        = I('post.lat');
		$way        = I('post.way');
		$server     = session('server');
		$userId     = session('user.userid');

		$repairs = D('Repairs');
		$orders  = D('Orders');
		$uc      = D('UserCode');
		$users   = D('Users');
		$area    = D('Area');

		if(empty($server)){
			$this->error("请勿重复提交订单!", U('Orders/repair'));exit;
		}
		
		// 城市信息
		$citys = $area->getCityByName($city);
		$areas = $area->getAreaByAreaName($district, $citys['id']);

		// 用户信息
		if(empty($userId)){
			$u = $uc->check($mobile, $smscode);   // 验证短信验证码
			if($u == 1){
				$user = $users->getUserByMobile($mobile);
				if(empty($user)){
					$u = $uc->check($mobile, $smscode);   // 验证短信验证码
					if($u == 1){
						$password  = substr($mobile, -6);
						$rs = $users->regist($mobile, $password);
						if($rs['userId']>0){                       // 注册成功
							$user = $users->getUserByMobile($mobile);
							
							// 发送密码
							sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
	
							// 加载用户信息				
							$userId = $rs['userId'];
						}else{
							$this->error('注册失败');exit;
						}
					}
				}else{
					$userId = $user['id'];
				}
			}else{
				$this->error("短信验证码错误!", U('Orders/repair'));exit;
			}
			$users->saveLogin($user);  // 保存会员登录信息
			$uc->del($mobile);         // 删除user_code表相关记录
		}
		if($userId <= 0){
			$this->error("用户信息错误!", U('Orders/repair'));exit;
		}

		// 维修信息
		if($server['modelId'] > 0 && $server['faultId'] > 0){
			$relation = $repairs->getFaultAllByModelIdAndFaultId($server['modelId'], $server['faultId']);
		}
		
		// 生成订单
		if(empty($server['orderId'])){
			$isFree = 0;
			if($server['shopId'] > 0){
				$isFree = 1;
			}
			$data = array(
				'user_id'       => $userId,
				'agent_id'      => 0,
				'order_sn'      => CBWCreateOrderNumber(),
				'order_type'    => 1,
				'order_date'    => time(),
				'deail_price'   => 0,
				'order_status'  => 1,
				'pay_status'    => 0,
				'area_id'       => $areas['id'],
				'address'       => $address.$supplement,
				'reciver_user'  => $username,
				'reciver_phone' => $mobile,
				'is_free'       => $isFree,
				'remarks'       => $brand.' '.$model.' '.$color.' '.$remarks
			);
			$orderId = $orders->insertOrders($data);
			if($orderId){
				session('server.orderId', $orderId);
				$da = array();
				$da['order_id']      = $orderId;
				$da['shop_id']       = 0;
				$da['relation_id']   = $relation['id'];
				$da['color_id']      = $server['colorId'];
				$da['server_way']    = $way;
				$da['server_status'] = 1;
				$da['lng']           = $lng;
				$da['lat']           = $lat;
				$orders->insertOrderServer($da);           // 添加维修订单表
			}
		}else{
			$orderId = session('server.orderId');
		}
		
		// 推送消息
		if($server['message'] == ''){
			$this->message($orderId, $lng, $lat, $way, $server['shopId']);
		}
		
		$this->redirect('Repair/server', array('id'=>$orderId));exit;
	}
	
	// 选择服务
    public function server(){
		$orderId = I('get.id');

		$repairs = D('Repairs');
		$orders  = D('Orders');
		
		// 订单信息
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($orderId);
		
		// 设置订单报价已读
		$offers = $orders->getOrderOfferByOrderId($orderId, 0, 5);
		foreach($offers as $vo){
			$orders->updateOrderOffer($vo['id'], array('status'=>1));
		}
		
		// 维修信息
		$money = 0;
		if($server['relation_id'] > 0 && $server['color_id'] > 0){
			$relation = $repairs->getRepairByRelationId($server['relation_id']);
			$money = $relation['money'];
			if(!empty($relation['color_money'])){
				$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
				$money = number_format($colormoneys[$server['color_id']], 2, '.', '');
			}
		}
		$relation['money'] = $money;
		
		// 可报价数量
		$offerCount = $orders->getOrderOfferIngCountByOrderId($orderId);
		
		$this->assign('order',$order);
		$this->assign('server',$server);
		$this->assign('relation',$relation);
		$this->assign('offerCount',$offerCount);
 		
		// 清空SESSION
		session('server', NULL);
		
		// SEO
        $ident ="repair";
        $idents =$this->seo($ident);
        $this->assign("title",empty($title)?$idents['title']:$title);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
        $this->display();
    }
	
	// 加载报价
	public function loadOffer(){
		$id  = I('post.id');
		$lng = I('post.lng');
		$lat = I('post.lat');
		$len = I('post.len');
		$len = empty($len)?0:$len;
		
		$orders = D('Orders');
		$us     = D('UserShop');
		
		// 当前报价列表
		$data = $orders->getOrderOfferByOrderId($id, $len, 5);
		foreach($data as &$vo){
			$shop = $us->getShopByShopId($vo['shop_id']);
			$distance = CBWGetDistance($shop['lng'], $shop['lat'], $lng, $lat);
			$vo['distance']   = number_format($distance/1000, 2, '.', '');
			$vo['type_id']    = $shop['type_id'];
			$vo['shop_name']  = $shop['shop_name'];
			$vo['user_name']  = $shop['user_name'];
			$vo['user_phone'] = $shop['user_phone'];
			$vo['address']    = $shop['city_addr'].$shop['addr'].$shop['address'];
			$vo['lng']        = $shop['lng'];
			$vo['lat']        = $shop['lat'];
			$vo['logo_image'] = $shop['logo_image'];
			$vo['exp']        = $shop['exp'];
			$level = CBWShopLevel($shop['exp']); // 等级换算
			$vo['level_image_name'] = $level['name'];
			$vo['level_image_num']  = $level['num'];
			$vo['level_level']      = $level['level'];
		}
		unset($vo);
		echo json_encode($data);
	}
	
	// 确认店铺服务
	public function confirmShopServer(){
		$orderId = I('post.oid');
		$shopId  = I('post.sid');

		$orders = D('Orders');
		$us     = D('UserShop');
		
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($order['id']);
		$shop = $us->getShopByShopId($shopId);
		$offer  = $orders->getOrderOfferByOrderIdAndShopId($order['id'], $shopId);
		
		if($server['server_status'] == 2){
			echo $order['order_sn'];exit;
		}
		$way = 1;
		if($offer['is_door'] == 1){
			$way = 2;
		}
		$rt = $orders->updateOrderServer($order['id'], array('shop_id'=>$shopId, 'server_status'=>2, 'server_way'=>$way, 'warranty'=>$offer['warranty']));
		if($rt['status'] > 0){
			$this->pushOrderMessage($order['id'], $shopId);  // 推送通知
			$orders->updateOrders($order['id'], array('deail_price' =>$offer['offer'], 'is_offer' =>0, 'is_offer_selected' =>1));
			$orders->updateOrderOffer($offer['id'], array('selected'=>1));
			if($order['is_offer'] == 0){
				$users->decreaseVirtual($shop['user_id'], 1000, 4, $order['order_sn']);  // 扣除虚拟币
			}
			
			// 设置订单报价已读
			$offers = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
			foreach($offers as $vo){
				$orders->updateOrderOffer($vo['id'], array('status'=>1));
			}
			
			// 推送订单
			/*if($shop['user_id'] == 14762){
				Vendor('ExternalApi.jikexiuApi');
				$jkx = new \jikexiuApi();
				$jkx->push($order);
			}*/
			echo $order['order_sn'];exit;
		}
		echo 0;exit;
	}
	
	// 确认邮寄官方服务
	public function confirmOfficialServer(){
		$orderId  = I('post.oid');
		$warranty = I('post.warranty');

		$orders  = D('Orders');
		$users   = D('Users');
		$us      = D('UserShop');
		$repairs = D('Repairs');
		
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($order['id']);
		if($server['server_status'] == 2){
			echo $order['order_sn'];exit;
		}
		
		// 维修信息
		$money = 0;
		if($server['relation_id'] > 0){
			$relation = $repairs->getRepairByRelationId($server['relation_id']);
			$money = $relation['money'];
			if(!empty($relation['color_money'])){
				$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
				$money = number_format($colormoneys[$server['color_id']], 2, '.', '');
			}
		}
		
		$rt = $orders->updateOrderServer($order['id'], array('shop_id'=>29, 'server_status'=>2, 'server_way'=>3, 'warranty'=>$warranty));
		if($rt['status'] > 0){
			$orders->updateOrders($order['id'], array('deail_price' =>$money));
			
			// 检查订单是否有报价,有报价则退还
			$offer = $orders->getOrderOfferByOrderId($order['id'], 0, 5);
			if(!empty($offer)){
				foreach($offer as $vo){
					if($vo['is_free'] == 0 && $vo['virtual_money'] > 0){
						$shop = $us->getShopByShopId($vo['shop_id']);
						$users->giveVirtual($shop['user_id'], $vo['virtual_money'], 6, $order['order_sn']);      // 增加草包豆
					}
				}
			}
			
			echo $order['order_sn'];exit;
		}
		echo 0;exit;
	}
	
	// 保存当前位置
	public function saveLocation(){
		$lng  = I('post.lng');
		$lat  = I('post.lat');
		$addr = I('post.addr');
		
		// 保存登录COOKIE
		$location = $lng.'|'.$lat.'|'.$addr;
		cookie('location', $location, array('expire' => C('AUTO_TIME_LOGIN')));

		echo true;exit;
	}
	
	/**
	 * 获取城市区号
	 */
    public function getCityNumber(){
		$city   = I('post.city');
		$modelCity = M('addr_city');
		$data = $modelCity->where("addr like '%".$city."%'")->find();
		echo $data['area_no'];
    }
}