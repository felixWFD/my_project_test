<?php
namespace   Home\Controller;
/**
 * 银联在线 支付模块
 * @version 2015-01-20
 */
class UpacpayController extends BaseController {
	
	//在类初始化方法中，引入相关类库    
	public function __construct() {
		
	}

	/*doUpacpay*/
	public function doUpacpay(){
		
		Vendor('Upacp.common');
		Vendor('Upacp.payLog');
		
		$sdk_log_file_path = './Public/Upacpay/logs/';  //日志 目录 
		$sdk_log_level = 'INFO';                        //日志级别
		$log = new \PhpLog ($sdk_log_file_path , "PRC", $sdk_log_level );
		$log->LogInfo ( "============处理前台请求开始===============" );
		
		$trade_no = I('post.trade_no');
		
		$comm = new \UpacpayCommon ();
		// 初始化日志
		$params = array(
				'version' => '5.0.0',				          //版本号
				'encoding' => 'utf-8',				          //编码方式
				'certId' => $comm->getSignCertId (),		  //证书ID
				'txnType' => '01',				              //交易类型	
				'txnSubType' => '01',				          //交易子类
				'bizType' => '000201',				          //业务类型
				'frontUrl' => 'https://www.caobao.com/Upacpay/FrontReceive.html',  		//前台通知地址
				'backUrl' => 'https://www.caobao.com/Upacpay/BackReceive.html',		    //后台通知地址
				'signMethod' => '01',		                  //签名方法
				'channelType' => '07',		                  //渠道类型，07-PC，08-手机
				'accessType' => '0',		                  //接入类型
				'merId' => '898440348120601',		          //商户代码，请改自己的测试商户号
				'orderId' => $trade_no,	                      //商户订单号
				'txnTime' => date('YmdHis'),	              //订单发送时间
				'txnAmt' => $this->getOrderMoney($trade_no),  //交易金额，单位分
				'currencyCode' => '156',	                  //交易币种
				'defaultPayType' => '0001',	                  //默认支付方式	
				//'orderDesc' => '订单描述',                  //订单描述，网关支付和wap支付暂时不起作用
				'reqReserved' =>' 透传信息',                  //请求方保留域，透传字段，查询、通知、对账文件中均会原样出现
				);

		// 签名	
		$comm->sign ( $params );


		// 前台请求地址
		$front_uri = 'https://gateway.95516.com/gateway/api/frontTransReq.do';
		$log->LogInfo ( "前台请求地址为>" . $front_uri );
		// 构造 自动提交的表单
		$html_form = $comm->create_html ( $params, $front_uri );

		$log->LogInfo ( "-------前台交易自动提交表单>--begin----" );
		$log->LogInfo ( $html_form );
		$log->LogInfo ( "-------前台交易自动提交表单>--end-------" );
		$log->LogInfo ( "============处理前台请求 结束===========" );
		echo $html_form;
	}
	
	function FrontReceive(){
		$signature = I('post.signature');
		$orderId   = I('post.orderId');
		
		$modelOrders = M('orders');
		$order = $modelOrders->field("id,order_type")->where("order_sn='".$orderId."'")->find();
		if (isset ( $signature )) {
			//echo verify ( $_POST ) ? '验签成功' : '验签失败';
			
			$data['id'] = $order['id'];
			$data['order_status'] =2;
			
			if($order['order_type'] == '1'){
				$modelOrderServer = M('order_server');
				$server = $modelOrderServer->field('shop_id,server_way')->where('order_id='.$order['id'])->find();
				if($server['server_way'] > 0){
					$data['order_status'] = 5;
				}
				$modelOrderServer->where('order_id='.$order['id'])->save(array('server_status'=>1));
				
				// 店铺经验,维修计数
				$modelUserShop = M('user_shop');
				$shop = $modelUserShop->where("id=".$server['shop_id'])->find();
				$modelUserShop->where("id=".$server['shop_id'])->save(array('exp'=>$shop['exp']+10, 'repair_count'=>$shop['repair_count']+1));
				
				// 给店铺发送已付款通知短信
				sendSmsByAliyun($shop['user_phone'], array('order'=>$orderId), 'SMS_116581138'); // 付款成功
			}
			
			$data['pay_status'] =1;
			$data['pay_mode'] =1;
			$data['pay_date'] =time();
			
			$modelOrders->save($data);
			$this->redirect('Orders/index');

		} else {
			//echo '签名为空';
			
			$this->redirect('Orders/index');
		}
	}
	
	function BackReceive(){
		
		$signature = I('post.signature');
		$orderId   = I('post.orderId');
		
		$modelOrders = M('orders');
		$order = $modelOrders->field("id,order_type")->where("order_sn='".$orderId."'")->find();
		
		if (isset ( $signature )) {
			//echo verify ( $_POST ) ? '验签成功' : '验签失败';

			$data['id'] = $order['id'];
			$data['order_status'] =2;
			
			if($order['order_type'] == '1'){
				$modelOrderServer = M('order_server');
				$server = $modelOrderServer->field('shop_id,server_way')->where('order_id='.$order['id'])->find();
				if($server['server_way'] == 1 || $server['server_way'] == 2){
					$data['order_status'] = 5;
				}
				$modelOrderServer->where('order_id='.$order['id'])->save(array('server_status'=>1));
			
				// 店铺经验,维修计数
				$modelUserShop = M('user_shop');
				$shop = $modelUserShop->where("id=".$server['shop_id'])->find();
				$modelUserShop->where("id=".$server['shop_id'])->save(array('exp'=>$shop['exp']+10, 'repair_count'=>$shop['repair_count']+1));
				
				// 给店铺发送已付款通知短信
				sendSmsByAliyun($shop['user_phone'], array('order'=>$orderId), 'SMS_116581138'); // 付款成功
			}
			
			$data['pay_status'] =1;
			$data['pay_mode'] =1;
			
			$modelOrders->save($data);
			$this->redirect('Orders/index');

		} else {
			//echo '签名为空';
			
			$this->redirect('Orders/index');
		}
	}
	
	//获取支付金额函数
	//函数功能:跟进页面提交的订单编号获取支付金额
	//返回值:数值
	function getOrderMoney($sn){
		$modelOrders = M('orders');
		$order = $modelOrders->field("id,deail_price,order_type")->where("order_sn='".$sn."'")->find();
		$money = 0;
		if($order['order_type'] == '0'){
			$money = $order['deail_price'];
		}elseif($order['order_type'] == '1'){
			$modelOrderServer = M("order_server");
			$server = $modelOrderServer->where("order_id=".$order['id'])->find();
			if($server['new_price'] !='0.00'){
				$money = $server['new_price'];
			}else{
				$money = $order['deail_price'];
			}
		}
		return $money*100;
	}

}