<?php
namespace Home\Controller;

class VideoController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    //视频列表
    public function index(){
               
		$modelVideo = M('video');
		$modelCat = M('product_cat');
		
		//视频分类列表,此处引用的是商品分类
		$typeRes = $modelCat->where('fid=5 AND is_show=1')->order('sort ASC')->select();
		
        // 获取查询产品的类型
		$type = I('get.type');
		$brand = I('get.brand');
		$cat = I('get.cat');


		$where = "id>0";
		$parameter = 'index';
		if(!empty($type)){
			$where .= " AND type_id=".$type;
			//商品品牌列表
			$brandRes = $modelCat->where("fid=".$type." AND is_show=1")->select();
			$typeInfo = $modelCat->where("id=".$type." AND is_show=1")->find();
			$title = $typeInfo['cat_name']."维修视频教程";
			$parameter = $type;
		}		
		if(!empty($brand)){
			$where .= " AND brand_id=".$brand;
			//商品分类列表
			$catRes = $modelCat->where("fid=".$brand." AND is_show=1")->order("sort ASC")->select();
			$brandInfo = $modelCat->where("id=".$brand." AND is_show=1")->find();
			$title = $brandInfo['cat_name']."维修视频教程";
			$parameter .= '_'.$brand;
		}
		
		if(!empty($cat)){
			$where .= " AND cat_id=".$cat;
			$catInfo = $modelCat->where("id=".$cat." AND is_show=1")->find();
			$title = $catInfo['cat_name']."维修视频教程";
			$parameter .= '_'.$cat;
		}

        $order = "id DESC";

        $getPageCount = $modelVideo->where($where)->count();

        // 分页处理
        $page = new \Think\MyPage($getPageCount, 8);
		$page->url = 'video_list/'.$parameter.'/';
		$videoRes = $modelVideo->where($where)->order($order)->limit($page->firstRow, $page->listRows)->select();		

        $pageShow = $page->show();

		//SEO
        $ident ="video";
        $idents =$this->seo($ident);
        $this->assign("title",empty($title)?$idents['title']:$title);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);

		$this->assign('type', $type);
        $this->assign('brand', $brand);
		$this->assign('cat', $cat);
		$this->assign('type_name', $typeInfo['cat_name']);
		$this->assign('brand_name', $brandInfo['cat_name']);
		$this->assign('cat_name', $catInfo['cat_name']);
        $this->assign('page', $pageShow);
        $this->assign('videoRes', $videoRes);
		$this->assign('typeRes', $typeRes);
		$this->assign('brandRes', $brandRes);
		$this->assign('catRes', $catRes);
		
        $this->display();
    }
	
    //视频详细
    public function detail(){
       
		$modelVideo = M('video');
		$modelCat = M('product_cat');
		
		$id = I('get.id');
		
		//增加点击数
		$modelVideo->where('id = '.$id)->setInc('clicktimes',1);
		//视频信息
		$video = $modelVideo->where('id='.$id)->find();
		if(!is_array($video)){
			$this->redirect('Empty/index');
		}
		$this->assign('video',$video);
		
		//招募 维修师/店
		$modelAdv = M('advs');
		$recruit = $modelAdv->field('id, adv_name, thumbnail, link_url')->where("id=6")->find();
		$this->assign("recruit",$recruit);
		
		//相关视频
		$videoRes = $modelVideo->where('id<>'.$video['id'].' AND cat_id='.$video['cat_id'])->order('clicktimes DESC')->select();
		if(empty($videoRes)){
			$videoRes = $modelVideo->where("type_id=".$video['type_id'])->order('clicktimes DESC')->limit(0,8)->select();
		}
		$this->assign('videoRes',$videoRes);
	
		//相关产品
		$products = D('Products');
		
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$productRes = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'catId'=>$video['cat_id'], 'groupby'=>1, 'm'=>0, 'n'=>6));
		if(empty($productRes)){
			$productRes = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'typeId'=>$video['type_id'], 'groupby'=>1, 'm'=>0, 'n'=>6));
		}
		$this->assign("productRes",$productRes);

		//SEO
		if($video['keywords'] == '' && $video['description']==''){
			$ident ="video";
			$idents =$this->seo($ident);
		}else{
			$idents['title'] = $video['title'];
			$idents['keywords'] = $video['keywords'];
			$idents['description'] = $video['description'];
		}
		$this->assign("title",$idents['title']."_维修视频");
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);
		
        $this->display();
    }

}
