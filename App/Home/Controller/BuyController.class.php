<?php
namespace Home\Controller;

class BuyController extends BaseController {
	
	public function __construct(){
		parent::__construct();
    }
	
    //购物车
    public function cart(){
		$gws     = session('gws');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');

		$products = D('Products');

        foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				$product = $products->getProductPriceByRelationId($v['id'], $v['num'], $groupId, $vip);
				$v['pro_price']  = $product['price'];
				$v['num']        = $product['num'];
				$v['pro_amount'] = number_format($product['price']*$product['num'], 2, '.', '');
				$v['attr_name']  = $products->getAttrNameByAttrId($v['attr_id']);
			}
			unset($v);
        }
		unset($vo);
		$_SESSION['gws'] = $gws;
        $this->assign("gws",$gws);
		
		//SEO
		$ident = "cart";
		$idents =$this->seo($ident);
		$this->assign("title",$idents['title']);
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);

        $this->display();
    }
	
	//AJAX 计算总金额
	public function totalAmount(){
		$ids      = I('post.ids');
		$money    = 0;
		$gws    = session('gws');
		$groupId  = session('user.groupid');
		$vip      = session('user.vip');
		
		$products = D('Products');
		
		foreach($gws as $vo){
			foreach($vo['cart'] as $v){
				foreach($ids as $id){
					if($v['id'] == $id){
						$relation = $products->getProductPriceByRelationId($v['id'], $v['num'], $groupId, $vip);
						$money += $relation['price']*$relation['num'];
					}
				}
			}
		}
		
		echo $money;
		exit; 
	}
	
    //删除单个购物车的商品
    public function deleteCat(){
		$id  = I('post.id');
		$gws = session('gws');
		if(empty($gws)){
			exit;
		}

		foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				if($v['id'] == $id){
					unset($vo['cart'][$id]);
					$_SESSION['gws_count']--;
				}
			}
		}
		foreach($gws as $key=>&$vo){
			if(empty($vo['cart'])){
				unset($gws[$key]);
			}
		}
		$_SESSION['gws'] = $gws;
        echo 1;
        exit;
    }
	
    //批量删除购物车的商品
    public function deleteBatchCat(){
		$ids = I('post.ids');
		$gws = session('gws');
		if(empty($gws)){
			exit;
		}

		foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				foreach($ids as $id){
					if($v['id'] == $id){
						unset($vo['cart'][$id]);
						$_SESSION['gws_count']--;
					}
				}
			}
		}
		foreach($gws as $key=>&$vo){
			if(empty($vo['cart'])){
				unset($gws[$key]);
			}
		}
		$_SESSION['gws'] = $gws;
        echo 1;
        exit;
    }
	
    //修改购物车的商品的数量
    public function changeNumber(){
		$id      = I('post.id');
		$num     = I('post.num');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$gws     = session('gws');

		$products = D('Products');
		
		$data = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		foreach($gws as &$vo){
			foreach($vo['cart'] as &$v){
				if($v['id'] == $id){
					$v['num'] = $data['num'];
					$v['pro_price'] = $data['price'];
					$v['pro_amount'] = number_format($data['price']*$data['num'], 2, '.', '');
				}
			}
		}
		$_SESSION['gws'] = $gws;
        echo json_encode($data);
        exit;
    }
	
    //订单确认
    public function collect(){
		$ids     = I('post.ids');
		$ids     = empty($ids)?I('get.ids'):$ids;
        $userId  = session('user.userid');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$gws     = session('gws');
		
		if(!empty($ids)){
			session('buynow', $ids);
		}
		$buynow  = explode(",", session('buynow'));

		empty($userId) && $this->redirect('Users/login', array('controller'=>'Buy', 'action'=>'cart'));
		empty($gws) && $this->error("购物车为空",U('Buy/cart'));
		empty($buynow) && $this->error("未选择需要购买的商品",U('Buy/cart'));
		
		$users     = D('Users');
		$ua        = D('UserAddress');
		$shippings = D('Shippings');
		$products  = D('Products');
		$us        = D('UserShop');
		
		$user     = $users->getUserAndInfoById($userId);      // 会员信息
		$address  = $ua->getAddressByUserId($userId);         // 收货地址
		foreach($address as $vo){
			if($vo['is_default'] == 1){
				$provinceId = $vo['one_id'];
				break;
			}
		}
		$shipping = $shippings->getShippings();                // 物流信息
		foreach($shipping as $vo){
			if($vo['is_default'] == 1){
				$shippingId = $vo['id'];
				break;
			}
		}

		//商品清单
		$money = 0;              // 商品总价
		$goods = array();
		foreach($gws as $shopId=>$vo){
			if($shopId > 0){
				$shop = $us->getShopByShopId($shopId);
				if($shop['user_id'] == $userId){
					$this->error("不允许购买自己店铺商品",U('Buy/cart'));
				}
			}
			$cmoney     = 0;  // 卡类商品总价
			$smoney     = 0;  // 店铺商品总价
			$virtual    = 0;  // 赠送草包豆总数量
			$isshipping = 0;  // 是否包邮
			$i          = 0;
			foreach($vo['cart'] as $k=>$v){
				if(in_array($v['id'],$buynow)){
					if($v['num'] <= 0){
						$this->error("商品缺库存",U('Buy/cart'));
					}
					
					$smoney  += $v['pro_price']*$v['num'];
					$money   += $v['pro_price']*$v['num'];
					$virtual += $v['virtual']*$v['num'];
					
					// 店铺与代理没有草包豆奖励
					if($groupId || $user['is_agent']){
						$virtual = 0;
					}
					// 是否卡商品
					if($v['is_card'] == 1){
						$cmoney += $v['pro_price']*$v['num'];
					}
					// 商品包邮 或 卡类商品 或 满99元 包邮
					if($v['is_shipping'] == 1 || $smoney == $cmoney || ($smoney-$cmoney) >= 99){
						$isshipping = 1;
					}
					
					// 计算运费
					$freight = $shippings->getShippingsByProvinceId($provinceId, $shippingId, $shopId, $isshipping);
					if($i == 0){
						$money += $freight;
					}
					
					$goods[$shopId]['cart'][$k] = $v;
					$goods[$shopId]['shop']     = $vo['shop'];
					$goods[$shopId]['other']    = array('smoney'=>$smoney, 'virtual'=>$virtual, 'freight'=>$freight);
					$i++;
				}
			}
        }

		$this->assign("user", $user);
		$this->assign("address", $address);
		$this->assign("shipping",$shipping);	
		$this->assign("money",number_format($money, 2, '.', ''));
		$this->assign("goods",$goods);
		$this->assign("ids",$ids);
		
		//三级联动
		$city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$c = \Think\Area::city($city);
		$this->assign("city", $c);
		
		//SEO
		$ident = "collect";
		$idents =$this->seo($ident);
		$this->assign("title",$idents['title']);
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);
	
		$this->display();
    }
	
	//重新加载区域快递运费
	public function loadShipping(){
		$id         = I('post.adds');
		$shippingId = I('post.shipping_id');
		$buynow     = explode(",", session('buynow'));

		$ua        = D('UserAddress');
		$shippings = D('Shippings');
		
		$address = $ua->getAddressById($id);
		$gws     = session('gws');
		$data    = array();
		foreach($gws as $key=>$vo){
			$cmoney     = 0;  // 卡类商品总价
			$smoney     = 0;  // 店铺商品总价
			$isshipping = 0;  // 是否包邮
			foreach($vo['cart'] as $v){
				if(in_array($v['id'],$buynow)){
					// 判断是否包邮
					if($v['is_shipping'] == 1){
						$isshipping = 1;
					}
					// 是否卡商品
					if($v['is_card'] == 1){
						$cmoney += $v['pro_price']*$v['num'];
					}
					$smoney += $v['pro_price']*$v['num'];
				}
			}
			// 卡类商品 或 满99元 包邮
			if($smoney == $cmoney || ($smoney-$cmoney) >= 99){
				$isshipping = 1;
			}
			// 计算运费
			$data[] = array(
				'id'      => $key,
				'freight' => $shippings->getShippingsByProvinceId($address['one_id'], $shippingId, $key, $isshipping)
			);
        }
		echo json_encode($data);
        exit;
	}
	
	//提交订单
	public function submitOrder(){
		$userId     = session('user.userid');
		$groupId    = session('user.groupid');
		$vip        = session('user.vip');
		$ids        = I('post.ids');
		$buynow     = explode(",", session('buynow'));
		$virtual    = I('post.virtual');
		$addressId  = I('post.adds');
		$shippingId = I('post.shipping_id');
		$remarks    = I('post.remarks');
		$gws        = session('gws');
		$agentId    = 0;

		empty($userId) && $this->redirect('Users/login', array('controller'=>'Buy', 'action'=>'collect'));
		empty($gws) && $this->error("购物车为空",U('Buy/cart'));
		empty($addressId) && $this->error("请填写完整地址",U('Buy/cart'));
		
		$users     = D('Users');
		$ua        = D('UserAddress');
		$uw        = D('UserWechat');
		$shippings = D('Shippings');
		$orders    = D('Orders');
		
		$user     = $users->getUserAndInfoById($userId);              // 会员信息
		$address  = $ua->getAddressById($addressId);                  // 收货地址
		$shipping = $shippings->getShippingByShippingId($shippingId); // 物流信息
		$wechat   = $uw->getWechatByUserId($user['id']);              // 微信信息
		if(!empty($wechat) && $wechat['fid'] > 0){
			$agentId   = $wechat['fid'];
		}
		
		$orderSn = CBWCreateOrderNumber();
		$data    = array(
			'user_id'       => $userId,
			'agent_id'      => $agentId,
			'order_sn'      => $orderSn,
			'order_date'    => time(),
			'order_status'  => 1,
			'pay_status'    => 0,
			'area_id'       => $address['area_id'],
			'address'       => $address['street'],
			'reciver_user'  => $address['reciver_user'],
			'reciver_phone' => $address['reciver_phone'],
			'send_time'     => 0,
			'is_invoice'    => 0
		);
			
		// 组织订单数组
		$i = 0;
		foreach($gws as $shopId=>$vo){
			$smoney     = 0;  // 店铺商品总价
			$cmoney     = 0;  // 卡类商品总价
			$isshipping = 0;  // 是否包邮
			
			foreach($vo['cart'] as $v){
				if(in_array($v['id'],$buynow)){
					$smoney += $v['pro_price']*$v['num'];
					// 是否卡商品
					if($v['is_card'] == 1){
						$cmoney += $v['pro_price']*$v['num'];
					}
					// 商品包邮 或 卡类商品 或 满99元 包邮
					if($v['is_shipping'] == 1 || $smoney == $cmoney || ($smoney-$cmoney) >= 99){
						$isshipping = 1;
					}
					
					$goods[$i]['pro_id']        = $v['id'];
					$goods[$i]['pro_price']     = $v['pro_price'];
					$goods[$i]['two_price']     = $v['two_price'];
					$goods[$i]['three_price']   = $v['three_price'];
					$goods[$i]['pro_number']    = $v['num'];
					$goods[$i]['pro_attr']      = $v['attr_id'];
					$goods[$i]['virtual_money'] = $v['virtual'];
					$i++;
				}
			}
			
			$schedule[$shopId]['shop_id']      = $shopId;
			$schedule[$shopId]['shipping_way'] = $shipping['sid'];
			$schedule[$shopId]['is_shipping']  = $isshipping;
			$schedule[$shopId]['remarks']      = $remarks[$shopId];
			
			//计算运费
			$fee = $shippings->getShippingsByProvinceId($address['one_id'], $shipping['id'], $shopId, $isshipping);
			$schedule[$shopId]['shipping_fee'] = $fee;

			// 是否使用虚拟币
			$vmoney = 0;
			if($virtual > 100){
				if($user['virtual_money'] < $virtual){
					$this->error("您输入草包豆的数量超过您的库存,请重新下单!",U('Buy/cart'));
					exit;
				}elseif($virtual > ($smoney*100/2)){
					$this->error("使用草包豆不能超过订单总额的50%,请重新下单!",U('Buy/cart'));
					exit;
				}
				$discount = $virtual/100;
				$vmoney   = $virtual;
			}
			$schedule[$shopId]['virtual_money'] = $vmoney;
			$schedule[$shopId]['shop_money'] = $smoney+$fee-$discount;
			$data['deail_price'] += $smoney+$fee-$discount;
        }
		
		if($i > 0){
			$orderId = $orders->insertOrders($data);
			if($orderId){
				// 添加订单附表
				$orders->insertOrderSchedule($schedule, $orderId);
				// 添加购物订单表
				$orders->insertOrderProducts($goods, $orderId);
				// 扣除虚拟币
				if($virtual > 100){
					$users->decreaseVirtual($userId, $virtual, '1', $orderSn);
				}
				
				// 清除SESSION
				session('buynow', NULL);
				foreach($gws as &$vo){
					foreach($vo['cart'] as &$v){
						foreach($buynow as $id){
							if($v['id'] == $id){
								unset($vo['cart'][$id]);
								$_SESSION['gws_count']--;
							}
						}
					}
				}
				foreach($gws as $key=>&$vo){
					if(empty($vo['cart'])){
						unset($gws[$key]);
					}
				}
				$_SESSION['gws'] = $gws;
				
				$this->redirect('Buy/pay', array('id'=>$orderSn));
				exit;
			}else{
				$this->error('订单生成失败,请联系管理员',U('Buy/cart'));
				exit;
			}
		}
		$this->error('订单生成失败,请联系管理员',U('Buy/cart'));
		exit;
	}
	
    //订单去支付
    public function pay(){
		$this->isUserLogin();
        //$this->error('支付接口维护中,暂时停用', U('Orders/index'));
        //exit;
        $sn     = I('get.id');
		$userId = session('user.userid');
		
		$banks  = D('Banks');
		$users  = D('Users');
		$orders = D('Orders');
		
		$order = $orders->getOrderBySn($sn);
		if(empty($order)){
			$this->error('订单错误(价格变更?/订单号错误?)', U('Orders/index'));
		}
		$money    = $orders->getOrderMoneyById($order['id']);
		$user     = $users->getUserAndInfoById($userId);
		$cards    = $users->getCardByUserId($userId);
		$dcard    = $users->getDefaultCardByUserId($userId);
		$deposits = $banks->getBank();
		$credits  = $banks->getBankByCredit();
		
		$this->assign("credits", $credits);
		$this->assign("deposits", $deposits);
		$this->assign("dcard",$dcard);
		$this->assign("cards",$cards);
		$this->assign("user",$user);
		$this->assign("money",$money);
		$this->assign("order",$order);
		$this->assign("year", date("Y"));
		
		//SEO
		$ident = "collect";
		$idents =$this->seo($ident);
		$this->assign("title",$idents['title']);
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);

		$this -> display();
    }
	
	/**
     * 检查订单支付状态
     */
	public function orderQuery(){
		$sn = I('post.trade_no');
		$orders = D('Orders');
		$order = $orders->getOrderBySn($sn);
		echo json_encode($order);
        exit;
	}
	
	//余额支付
	public function pay_surplus(){
		$userId = session('user.userid');
		if($userId ==""){
			$this->redirect('Users/login');
			exit;
        }
		
		$sn          = I('post.trade_no');
		$payPassword = I('post.pay_password');
		
		$orders = D('Orders');
		$users  = D('Users');
		
		$order = $orders->getOrderBySn($sn);
		$money = $orders->getOrderMoneyById($order['id']);
		$user  = $users->getUserAndInfoById($order['user_id']);
		if($user['pay_password'] == ''){
			$this->error("请先设置支付密码", U("Users/security"));exit;
		}
		$password = sha1($payPassword);
		if($user['pay_password'] != $password){
			$this->error("支付密码错误,请重新输入", U("Buy/pay", array('id'=>$order_sn)));exit;
		}
		if($user['money'] < $money){
			$this->error("支付失败,您的账户余额不足", U("Buy/pay", array('id'=>$order_sn)));exit;
		}
		if($order['pay_status'] == 0 && $money > 0){
			$rt = $orders->updateOrders($order['id'], array('pay_status'=>1, 'pay_date'=>time(), 'pay_mode'=>4));
			if($rt['status'] > 0){
				// 余额扣除
				$users->decreaseMoney($order['user_id'], $order['id'], $money, 7, '');
				
				// 支付完成后,订单操作
				$orders->successPayOrderOperation($order['id']);
				
				// 页面跳转
				if(!empty($order['pay_link'])){
					$this->redirect($order['pay_link']);exit;
				}else{
					if($order['order_type'] == 0){
						$this->success("支付成功", U("orders/index"));exit;
					}elseif($order['order_type'] == 1){
						$this->success("支付成功", U("orders/repair"));exit;
					}elseif($order['order_type'] == 2){
						$this->success("支付成功", U("orders/recovery"));exit;
					}
				}
			}
		}else{
			$this->error("重复支付或支付金额错误", U("orders/index"));exit;
		}
		$this->error("支付失败", U("Buy/pay", array('id'=>$order_sn)));exit;
	}

    //修改默认地址
    public function edit_default()
    {
        $modelAddress = M('user_address');

        $user_id = session('user.userid');
        $modelAddress->where("user_id = $user_id")->save(array('is_default'=>0));  //将本用户所有地址改为非默认
		
		$id = I('post.id');
		$rt = $modelAddress -> where("id = $id") -> save(array('is_default'=>1));
        echo $rt;

    }

    //添加地址
    public function add_address()
    {
        $modelAddress = M('user_address');

        $user_id = session('user.userid');
		$count = $modelAddress->where("is_show=1 AND user_id=".$user_id)->count();
		if($count >=3){
			echo "0";
			exit;
		}
		$is_default = I('post.is_default');
		
		$address = array();
		$address['user_id'] = $user_id;
		$address['reciver_user'] = I('post.reciver_user');
		$address['reciver_phone'] = I('post.reciver_phone');
		$address['area_id'] = I('post.area_id');
		$address['street'] = I('post.street');
		$address['is_default'] = $is_default;

        if ($is_default == 1) {
            $data['is_default'] = 0;
            $modelAddress->where("user_id = $user_id")->save($data);
        }
		if($modelAddress->create($address)){
			$rt = $modelAddress->add();
			echo $rt;exit;
		}
        echo 0;exit;
    }
	
	//post获取需修改的地址信息
	public function address_info(){
	
		$id = I('post.id');
		$model = M();
		$modelProvince = M('addr_province');
		$modelCity = M('addr_city');
		$modelArea = M('addr_area');
		
		$data = $model->table('__USER_ADDRESS__ AS ua')
							->field('ua.*,a.id as area_id,a.one_id,a.two_id')
							->join('__ADDR_AREA__ AS a ON a.id=ua.area_id')
							->where('ua.id='.$id)
							->find();
		
		$citys='<select id="province" name="province" class="span2">';
			$citys.='<option>选择省</option>';
			$province = $modelProvince->select();
			foreach($province as $p){
				$citys.='<option value="'.$p['id'].'"';
				if($p['id'] == $data['one_id']){
					$citys.=' selected';
				}
				$citys.='>'.$p['addr'].'</option>';
			}
		$citys.='</select>';
		
		$citys.='<select id="city" name="city" class="span2">';
			$city = $modelCity->where('one_id='.$data['one_id'])->select();
			foreach($city as $c){
				$citys.='<option value="'.$c['id'].'"';
				if($c['id'] == $data['two_id']){
					$citys.=' selected';
				}
				$citys.='>'.$c['addr'].'</option>';
			}
		$citys.='</select>';
		
		$citys.='<select id="area" name="area" class="span2">';
			$area = $modelArea->where('two_id='.$data['two_id'])->select();
			foreach($area as $a){
				$citys.='<option value="'.$a['id'].'"';
				if($a['id'] == $data['area_id']){
					$citys.=' selected';
				}
				$citys.='>'.$a['addr'].'</option>';
			}
		$citys.='</select>';
		$data['citys'] = $citys;	
		
		echo json_encode($data);
        exit;
	}

    //修改地址
    public function edit_address()
    {
        $is_default = I('post.is_default');
		$user_id = session('user.userid');
        $modelAddress = M('user_address');
		
		$address = array();
		$address['user_id'] = $user_id;
		$address['id'] = I('post.id');
		$address['reciver_user'] = I('post.reciver_user');
		$address['reciver_phone'] = I('post.reciver_phone');
		$address['area_id'] = I('post.area_id');
		$address['street'] = I('post.street');
		$address['is_default'] = $is_default;

		//重置默认
		if ($is_default == 1) {
			$data['is_default'] = 0;
			$modelAddress->where("user_id=".$user_id)->save($data);
		}
		$rt = $modelAddress->where("id=".$address['id'])->save($address);
		echo $rt;exit;
    }

}
