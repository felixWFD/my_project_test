<?php
namespace   Home\Controller;
/**
 * 微信支付 支付模块
 * @version 2015-01-20
 */
class WxpayController extends BaseController {
	
	//在类初始化方法中，引入相关类库    
	public function __construct() {
		
		parent::__construct();
		
		Vendor('WxPayPubHelper.WxPayException');
		Vendor('WxPayPubHelper.WxPayConfig');
		Vendor('WxPayPubHelper.WxPayData');
		Vendor('WxPayPubHelper.WxPayApi');
		Vendor('WxPayPubHelper.WxPayNotify');
		Vendor('WxPayPubHelper.NativePay');
		Vendor('phpqrcode.phpqrcode');
	}
	
	//微信支付 扫码支付
	public function native(){
        //$this->error('支付接口维护中,暂时停用', U('Orders/index'));
        //exit;
		//页面传递数据
		$trade_no     = I('trade_no');
		$out_trade_no = $trade_no."_".rand(1000,9999);
		if(empty($trade_no)){
			$this->error('参数错误');exit;
		}
		
		$orders   = D('Orders');
		$products = D('Products');
		$repairs  = D('Repairs');
		
		$body  = $orders->getOrderBody($trade_no);     // 订单描述
		$body  = msubstr($body, 0, 45,'utf-8');
		$money = $orders->getOrderMoney($trade_no);    // 支付金额
		$order = $orders->getOrderBySn($trade_no);     // 订单信息
		if($order['order_type'] == '0'){       // 购物订单
			$orderProducts = $orders->getOrderProductsByOrderId($order['id']);
			$list = array();
			foreach($orderProducts as $k=>$v){
				$list[$k]['show_name'] = $v['pro_name'];
				$list[$k]['show_desc'] = $products->getAttrNameByAttrId($v['pro_attr']);
			}
			$order['list'] = $list;
		}elseif($order['order_type'] == '1'){  // 维修订单
			$server = $orders->getOrderServerByOrderId($order['id']);
			$relation = $repairs->getRepairByRelationId($server['relation_id']);
			$list = array();
			$list['show_name'] = $relation['pro_name']." ".$server['name'];
			$list['show_desc'] = $relation['plan_name'];
			$order['list'][0] = $list;
		}
		$this->assign("money",$money);
		$this->assign("order",$order);
		
		$notify = new \NativePay();
		//使用统一支付接口
		$input = new \WxPayUnifiedOrder();
		
		$input->SetBody($body);                                           // 描述
		$input->SetAttach($trade_no);                                     // 附加数据 选填
		$input->SetOut_trade_no($out_trade_no);                           // 订单号码
		$input->SetTotal_fee($money*100);                                 // 订单金额
		$input->SetTime_start(date("YmdHis"));                            // 交易起始时间 选填 
		$input->SetTime_expire(date("YmdHis", time() + 600));             // 交易结束时间 选填 
		$input->SetGoods_tag("product name");                             // 商品标记 选填 
		$input->SetNotify_url(\WxPayConfig::HOME_NOTIFY_URL);             // 通知地址 
		$input->SetTrade_type("NATIVE");                                  // 交易类型
		$input->SetProduct_id("product info");                            // 商品ID 选填
		$result = $notify->GetPayUrl($input);

		$code_url = urlencode(str_replace("/", "|", str_replace("?", ",", $result['code_url'])));
		$this->assign("code_url",$code_url);
		$this->assign("err_code",$result['err_code']);
		
		//SEO
        $ident ="native";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}
	
	//微信支付 站内查询订单支付状态
	public function orderQuery(){
		
		$trade_no = I('post.trade_no');
		
		$orders = D('Orders');
		
		$order = $orders->getOrderBySn($trade_no);
		echo json_encode($order);
	}
	
	//微信支付 微信查询订单支付状态
	public function wxOrderQuery(){
		$trade_no = I('post.trade_no');
		
		$input = new \WxPayOrderQuery();
		$input->SetOut_trade_no($trade_no);
		$r=\WxPayApi::orderQuery($input);
		
		//商户根据实际情况设置相应的处理流程,此处仅作举例
		if ($r["trade_state"] == "SUCCESS") {
			echo "1";   //支付成功
		}else{
			echo "0";
		}
	}
	
	//生成二维码
	public function qrcode(){
		$data = I('get.data');
		$url = urldecode($data);
		$url = str_replace("|", "/", str_replace(",", "?", $url));
		\QRcode::png($url,false,'QR_ECLEVEL_L', 8);
	}
	
	//异步通知
	public function notify(){
		$notify = new \WxPayNotify();
		$notify->Handle(true);
	}
	
	// 打印log
	function log_result($file,$word) {
	    $fp = fopen($file,"a");
	    flock($fp, LOCK_EX) ;
	    fwrite($fp,"执行日期：".strftime("%Y-%m-%d-%H：%M：%S",time())."\n".$word."\n\n");
	    flock($fp, LOCK_UN);
	    fclose($fp);
	}
	
}