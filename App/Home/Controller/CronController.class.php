<?php
namespace Home\Controller;
/**
 * 定时任务
 * User: YJG
 * Date: 2015/1/6
 * Time: 10:16
 */
class CronController extends BaseController {
    
	public function __construct(){
        parent::__construct();
    }
	
	// 每五分钟定时任务	
	public function aminute(){
		$orders = D('Orders');
		$users  = D('Users');
		$us     = D('UserShop');
		
		$now  = time();
		$time = $now-600;

		$data = $orders->getOrderList(array("type"=>array(1,2), 'status'=>1));
		
		foreach($data as $vo){
			if($vo['order_date'] < $time){
				$type   = 1;
				$shopId = 0;
				if($vo['order_type'] == 1){
					$server = $orders->getOrderServerByOrderId($vo['id']);
					$shopId = $server['shop_id'];
					if($server['server_way'] == 3){
						$type = 0;
						$orders->updateOrders($vo['id'], array('is_offer'=>0));
					}
				}elseif($vo['order_type'] == 2){
					$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
					$shopId = $recovery['shop_id'];
					if($recovery['reco_way'] == 3){
						$type = 0;
						$orders->updateOrders($vo['id'], array('is_offer'=>0));
					}
				}
				if($type == 1 && $shopId == 0){
					$offer = $orders->getOrderOfferByOrderId($vo['id'], 0, 5);
					if(!empty($offer)){
						// 下单后,10分钟自动选择第一个报价店铺服务
						foreach($offer as $k=>$v){
							if($k == 0){
								$way = 1;
								if($v['is_door'] == 1){
									$way = 2;
								}
								$orders->updateOrderOffer($v['id'], array('selected'=>1));
								if($vo['order_type'] == 1){
									$orders->updateOrderServer($vo['id'], array('shop_id'=>$v['shop_id'], 'server_status'=>2, 'server_way'=>$way, 'warranty'=>$v['warranty']));
								}elseif($vo['order_type'] == 2){
									$orders->updateOrderRecovery($vo['id'], array('shop_id'=>$v['shop_id'], 'reco_status'=>2, 'reco_way'=>$way));
								}
								$this->pushOrderMessage($vo['id'], $v['shop_id']);  // 推送通知
								$orders->updateOrders($vo['id'], array('deail_price'=>$v['offer'], 'is_offer'=>0));
								
								// 推送订单
								/*if($shop['user_id'] == 14762){
									Vendor('ExternalApi.jikexiuApi');
									$jkx = new \jikexiuApi();
									$jkx->push($vo);
								}*/
							}else{
								if($v['is_free'] == 0 && $v['virtual_money'] > 0){
									$shop = $us->getShopByShopId($v['shop_id']);
									$users->giveVirtual($shop['user_id'], $v['virtual_money'], 6, $vo['order_sn']);      // 退还草包豆
								}
							}
							$orders->updateOrderOffer($v['id'], array('status'=>1));
						}
					}else{
						// 下单后,白天2小时/凌晨8小时 没有报价的关闭订单
						/*$hour = date('H', $now);
						if($hour >= 0 && $hour < 10){
							$time2 = $now-3600*8;
						}else{
							$time2 = $now-3600*2;
						}*/
						$time2 = $now-3600*24;
						if($vo['order_date'] < $time2){
							$orders->updateOrders($vo['id'], array('order_status'=>0, 'is_offer'=>0));
						}
					}
				}
			}
		}
	}
	
	// 站点地图 6:10定时任务	
	public function sitemap(){
		
		// 权重值 0，1，2，3，4，5 分别对应以下权重
		/*$arr = array(1, 0.9, 0.8, 0.7, 0.6, 0.5);*/
		
		$sitemap = new \Think\Sitemap();
		$sitemap->AddItem('https://www.caobao.com/product.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/repair.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/repairer.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/video.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/wenda.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/explain.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/news.xml', '1', 'daily');
		$sitemap->AddItem('https://www.caobao.com/thread.xml', '1', 'daily');
		$sitemap->SaveToFile('sitemap.xml');
		
		//草包商城
		$modelProducts = M('products');
		$productRes = $modelProducts->where('classify_id in(5,6,9) AND is_on_sale=1 AND recycle=0')->order('add_time DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($productRes as $vo){
			$sitemap->AddItem('https://www.caobao.com/detail/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['add_time']));
		}
		$sitemap->SaveToFile('product.xml');
		
		// 我要维修
		$modelProductCat = M('product_cat');
		$modelRepairs = M('repairs');
		$modelColors = M('repair_color');
		$modelCat = M("repair_fault_cat");
		
		$sitemap = new \Think\Sitemap();
		$typeRes = $modelProductCat->field('id')->where('fid=3 AND is_show=1')->order('sort ASC')->select();
		foreach($typeRes as $type){
			$brandRes = $modelProductCat->field('id')->where('fid='.$type['id'].' AND is_show=1')->order('sort ASC')->select();
			$catRes = $modelCat->field('id')->where('cat_id='.$type['id'])->select();
			foreach($brandRes as $brand){
				$repairRes = $modelRepairs->field('id')->where('brand_id='.$brand['id'].' AND is_on_sale=1')->order('sort ASC,id DESC')->select();
				foreach($repairRes as $repair){
					$colorRes = $modelColors->field('id')->where("repair_id=".$repair['id'])->order("id ASC")->select();
					foreach($colorRes as $color){
						foreach($catRes as $cat){
							$sitemap->AddItem('https://www.caobao.com/repair/'.$type['id'].'-'.$brand['id'].'-'.$repair['id'].'-'.$color['id'].'-'.$cat['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', time()));
						}
					}
				}
			}
		}
		$sitemap->SaveToFile('repair.xml');
		
		// 维修店铺
		$modelShop = M('user_shop');
		$shopRes = $modelShop->field('id,create_shop')->order('id DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($shopRes as $vo){
			$sitemap->AddItem('https://www.caobao.com/repairer/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['create_shop']));
		}
		$sitemap->SaveToFile('repairer.xml');
		
		// 草包课堂
		$modelExplain = M('explain');
		$explainRes = $modelExplain->field('id,create_time')->where('is_show<>0')->order('id DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($explainRes as $vo){
			$sitemap->AddItem('https://www.caobao.com/explain/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['create_time']));
		}
		$sitemap->SaveToFile('explain.xml');
		
		//草包资讯
		$modelNews = M('news_contents');
		$newsRes = $modelNews->field('id,create_time')->where("is_show<>0")->order('create_time DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($newsRes as $vo){
			$sitemap->AddItem('https://www.caobao.com/news/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['create_time']));
		}
		$sitemap->SaveToFile('news.xml');
		
		// 草包视频
		$modelVideo = M('video');
		$videoRes = $modelVideo->field('id,create_time')->where("status<>0")->order('create_time DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($videoRes as $vo){
			$sitemap->AddItem('https://www.caobao.com/video/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['create_time']));
		}
		$sitemap->SaveToFile('video.xml');
		
		//草包问答
		$modelWenda = M('wenda_contents');
		$wendaRes = $modelWenda->field('id,create_time')->where("is_show<>0")->order('create_time DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($wendaRes as $vo){
			$sitemap->AddItem('https://www.caobao.com/wenda/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['create_time']));
		}
		$sitemap->SaveToFile('wenda.xml');
		
		//论坛主题
		$modelThread = M('bbs_thread');
		$threadRes = $modelThread->field('id,dateline')->where("displayorder=0")->order('dateline DESC')->select();
		$sitemap = new \Think\Sitemap();
		foreach($threadRes as $vo){
			$sitemap->AddItem('https://bbs.caobao.com/view/'.$vo['id'].'.html', '2', 'daily', date('Y-m-d H:i:s', $vo['dateline']));
		}
		$sitemap->SaveToFile('thread.xml');

	}
	
	// 重置站点 6:20定时任务	
	public function resetsite(){
		
		$model = M();
		
		// 清空手机验证码相关表记录
		$model->execute("truncate table __USER_CODE__");
		$model->execute("truncate table __SMS_IP__");
		
		// 商品今天点击数重置为0
		$modelProducts = M('products');
		$modelProducts->where('id>0')->save(array('click_date'=>0));
	} 
	
	// 6:30定时任务	
	public function cron630(){
		$this->removeFrozen();       // 解除冻结账户金额
		$this->completeShopping();   // 自动完成交易已发货超9天的购物订单
		$this->closeOrders();        // 关闭下单时间过期的交易
		$this->deleteOrders();       // 删除关闭交易的订单
		$this->closeComments();      // 交易完成超过30天,关闭评价
	}
	
	// 解除冻结账户金额
	private function removeFrozen(){
		$users  = D('Users');
		$us     = D('UserShop');
		$orders = D('Orders');
        $products = D('Products');
		$frozens = $users->getFrozenMoneyByObject(array('status'=>0, 'time'=>time()));
		foreach($frozens as $vo){
			// 解除冻结账户金额
			$users->removeFrozenMoney($vo['id']);
			
			// 更改购物订单状态
			$order = $orders->getOrderById($vo['order_id']);
			$shop  = $us->getShopByUserId($vo['user_id']);
			if($order['order_type'] == 0 && $shop['id'] > 0){
				$rt = $orders->updateOrderSchedule($order['id'], $shop['id'], array('shop_status'=>3));
				if($rt['status'] > 0){
					$schedule = $orders->getOrderScheduleByOrderId($order['id']);
					$product  = $orders->getOrderProductsByOrderId($order['id']);
					$i = $j = $virtual = 0;
					foreach($schedule as $v){
						if($v['shop_status'] == 3){
							$i++;
						}
						if($v['shop_id'] == $shop['id']){
							foreach($product as $pro){
								if($pro['shop_id'] == $shop['shop_id']){
									if($pro['virtual_money'] > 0){
										$virtual += $pro['virtual_money'];
									}
									$products->statistics($pro['pro_id'], 'sale');
								}
							}
						}
						$j++;
					}
					if($i == $j){
						$orders->updateOrders($order['id'], array('ok_date'=>time(), 'order_status'=>2));
					}
					if($virtual > 0){
						$users->giveVirtual($vo['user_id'], $virtual, 1, $order['order_sn']);
					}
				}
			}
		}
	}
	
    // 自动完成交易已发货超9天的购物订单
    private function completeShopping(){
		$orders   = D('Orders');
		$users    = D('Users');
		$us       = D('UserShop');
		$products = D('Products');

		$time = time()-3600*24*9;
		$data = $orders->getOrderScheduleByStatus(2);
		foreach($data as $vo){
			if($vo['send_date'] <= $time){
				$rt = $orders->updateOrderSchedule($vo['order_id'], $vo['shop_id'], array('shop_status'=>3));
				if($rt['status'] > 0){
					// 解除该订单店铺冻结金额
					if($vo['shop_id'] > 0){
						$shop = $us->getShopByShopId($vo['shop_id']);
						$users->removeFrozenMoney(0, $shop['user_id'], $vo['order_id']);
					}
					
					$order    = $orders->getOrderById($vo['order_id']);
					$schedule = $orders->getOrderScheduleByOrderId($order['id']);
					$product  = $orders->getOrderProductsByOrderId($order['id']);
					$i = $j = $virtual = 0;
					foreach($schedule as $v){
						if($v['shop_status'] == 3){
							$i++;
						}
						if($v['shop_id'] == $vo['shop_id']){
							foreach($product as $pro){
								if($pro['shop_id'] == $vo['shop_id']){
									if($pro['virtual_money'] > 0){
										$virtual += $pro['virtual_money'];
									}
									$products->statistics($pro['pro_id'], 'sale');
								}
							}
						}
						$j++;
					}
					if($i == $j){
						$orders->updateOrders($order['id'], array('ok_date'=>time(), 'order_status'=>2));
					}
					if($virtual > 0){
						$users->giveVirtual($order['user_id'], $virtual, 1, $order['order_sn']);
					}
				}
			}
		}
    }
	
    // 关闭下单时间过期的交易
    private function closeOrders(){
		$modelOrders = M('orders');
		$modelOrderProducts = M('order_products');
		$modelRelation = M('product_relation');
		$modelProducts = M('products');
		
		// 购物订单
		$time = time()-3600*24*3;
		$orders = $modelOrders->field("id,order_date")->where("order_type=0 AND pay_status=0 AND order_date<=".$time)->select();
		foreach($orders as $key=>$vo){
			$modelOrders->where("id=".$vo['id'])->save(array('order_status'=>0));
		}
    }
	
	// 删除关闭交易的订单
	private function deleteOrders(){
		$modelOrders = M('orders');
		$modelProducts = M('order_products');
		$modelSchedule = M('order_schedule');
		$modelServer = M('order_server');
		$modelRecovery = M('order_recovery');
		$modelComments = M('comments');
		$modelReplies = M('comment_replies');
		$modelSaleService = M('sale_service');
		$modelServiceDetails = M('service_details');
		
		$time = time()-3600*24*30;
		$orders = $modelOrders->field("id,order_type,order_date")->where("order_type in(0,1,2) AND order_status=0 AND order_date<=".$time)->select();
		foreach($orders as $vo){
			$rt = $modelOrders->where("id=".$vo['id'])->delete();
			if($rt){
				if($orders['order_type'] == '0'){
					// 删除订单相关
					$modelProducts->where("order_id=".$vo['id'])->delete();
					$modelSchedule->where("order_id=".$vo['id'])->delete();
					
					// 删除评价与回复
					$comments = $modelComments->where("order_id=".$vo['id'])->select();
					foreach($comments as $v){
						$modelComments->where("id=".$v['id'])->delete();
						$modelReplies->where("comment_id=".$v['id'])->delete();
					}
					
					// 删除订单相关售后
					$sales = $modelSaleService->where("order_id=".$vo['id'])->select();
					foreach($sales as $v){
						$modelSaleService->where("id=".$v['id'])->delete();
						$modelServiceDetails->where("service_id=".$v['id'])->delete();
					}
				}elseif($orders['order_type'] == '1'){
					// 删除订单相关
					$modelServer->where("order_id=".$vo['id'])->delete();
				}elseif($orders['order_type'] == '2'){
					// 删除订单相关
					$modelRecovery->where("order_id=".$vo['id'])->delete();
				}
			}
		}
	}
	
	// 交易完成超过30天,关闭评价
	private function closeComments(){
		$orders = D('Orders');
		$us     = D('UserShop');
		
		$time = time()-3600*24*30;
		$data = $orders->getOrderList(array("status"=>2, "comment"=>0));
		foreach($data as $vo){
			if($vo['order_date'] <= $time){
				$orders->updateOrders($vo['id'], array('is_comment'=>1));
				if($vo['order_type'] == 1 || $vo['order_type'] == 2){
					if($vo['order_type'] == 1){
						$server = $orders->getOrderServerByOrderId($vo['id']);
						$shopId = $server['shop_id'];
					}elseif($vo['order_type'] == 2){
						$server = $orders->getOrderRecoveryByOrderId($vo['id']);
						$shopId = $server['server_id'];
					}
					$us->updateShopScore($shopId, 1, 3);
				}
			}
		}
	}
}
