<?php
namespace   Home\Controller;
/**
 * 支付宝 支付模块
 * @version 2015-01-20
 */
header("Content-type:text/html;charset=utf-8");
class AlipayController extends BaseController {
	
	//在类初始化方法中，引入相关类库    
	public function __construct() {
		Vendor('Alipay.coreFunction');
		Vendor('Alipay.md5Function');
		Vendor('Alipay.payNotify');
		Vendor('Alipay.paySubmit');
	}

	//alipay方法  即时交易 支付宝支付
	/*该方法其实就是将接口文件包下alipayapi.php的内容复制过来然后进行相关处理*/
	public function alipay(){
		
		$orders = D('Orders');
		
		//通过C函数来读取配置项
		$alipay_config = C('alipay_config');
		$alipay = C('alipay');
		
		//页面传递数据
		$trade_no = I('post.trade_no');
		
		/**************************请求参数**************************/
		$payment_type      = "1";                               // 支付类型 //必填，不能修改
		$notify_url        = $alipay['ali_notify_url'];         // 服务器异步通知页面路径
		$return_url        = $alipay['ali_return_url'];         // 页面跳转同步通知页面路径
		$seller_email      = $alipay_config['seller_email'];    // 卖家支付宝帐户 //必填
		$out_trade_no      = $trade_no;                         // 商户订单号 通过支付页面的表单进行传递，注意要唯一！
		$subject           = "草包网订单";                      // 订单名称 //必填 通过支付页面的表单进行传递
		$total_fee         = $orders->getOrderMoney($trade_no); // 付款金额 //必填 通过支付页面的表单进行传递
		$body              = $orders->getOrderBody($trade_no);  // 订单描述 通过支付页面的表单进行传递
		$show_url          = CBWDomain();                       // 商品展示地址 通过支付页面的表单进行传递
		$anti_phishing_key = "";                                // 防钓鱼时间戳 //若要使用请调用类文件submit中的query_timestamp函数
		$exter_invoke_ip   = get_client_ip();                   // 客户端的IP地址 
		/************************************************************/
		
		//构造要请求的参数数组，无需改动
		$parameter = array(
			"service"        => "create_direct_pay_by_user",
			"partner"        => trim($alipay_config['partner']),
			"payment_type"   => $payment_type,
			"notify_url"     => $notify_url,
			"return_url"     => $return_url,
			"seller_email"   => $seller_email,
			"out_trade_no"   => $out_trade_no,
			"subject"        => $subject,
			"total_fee"      => $total_fee,
			"body"           => $body,
			"show_url"       => $show_url,
			"anti_phishing_key"  => $anti_phishing_key,
			"exter_invoke_ip"    => $exter_invoke_ip,
			"_input_charset"     => trim($alipay_config['input_charset'])
		);

		//建立请求
		$alipaySubmit = new \AlipaySubmit($alipay_config);
		$html_text = $alipaySubmit->buildRequestForm($parameter,"post", "确认");
		echo $html_text;
	}

	//alipay方法  扫码即时到账支付 
	/*异步通知中接收business_scene值，并且当business_scene=qrpay （目前只有qrpay一种回传值）时将支付方式修改为“支付宝-扫码支付”；*/
	public function qrcode(){
		
		$orders = D('Orders');
		
		//通过C函数来读取配置项
		$alipay_config = C('alipay_config');
		$alipay = C('alipay');
		
		//页面传递数据
		$trade_no = I('get.trade_no');
		
		/**************************请求参数**************************/
		$payment_type      = "1";                               // 支付类型 //必填，不能修改
		$notify_url        = $alipay['ali_notify_url'];         // 服务器异步通知页面路径
		$return_url        = $alipay['ali_return_url'];         // 页面跳转同步通知页面路径
		$seller_email      = $alipay_config['seller_email'];    // 卖家支付宝帐户必填
		$out_trade_no      = $trade_no;                         // 商户订单号 通过支付页面的表单进行传递，注意要唯一！
		$subject           = "草包网订单";                      // 订单名称 //必填 通过支付页面的表单进行传递
		$total_fee         = $orders->getOrderMoney($trade_no); // 付款金额  //必填 通过支付页面的表单进行传递
		$body              = $orders->getOrderBody($trade_no);  // 订单描述 通过支付页面的表单进行传递
		$show_url          = CBWDomain();                       // 商品展示地址 通过支付页面的表单进行传递
		$anti_phishing_key = "";                                // 防钓鱼时间戳 //若要使用请调用类文件submit中的query_timestamp函数
		$exter_invoke_ip   = get_client_ip();                   // 客户端的IP地址 
		$qr_pay_mode       = 2;                                 // 扫码支付方式 0简约前置模式, 1前置模式, 2页面跳转模式,直接进入到支付宝收银台
		/************************************************************/
		
		//构造要请求的参数数组，无需改动
		$parameter = array(
			"service"        => "create_direct_pay_by_user",
			"partner"        => trim($alipay_config['partner']),
			"payment_type"   => $payment_type,
			"notify_url"     => $notify_url,
			"return_url"     => $return_url,
			"seller_email"   => $seller_email,
			"out_trade_no"   => $out_trade_no,
			"subject"        => $subject,
			"total_fee"      => $total_fee,
			"body"           => $body,
			"show_url"       => $show_url,
			"qr_pay_mode"    => $qr_pay_mode,
			"anti_phishing_key"  => $anti_phishing_key,
			"exter_invoke_ip"    => $exter_invoke_ip,
			"_input_charset"     => trim($alipay_config['input_charset'])
		);

		//建立请求
		$alipaySubmit = new \AlipaySubmit($alipay_config);
		$html_text = $alipaySubmit->buildRequestForm($parameter,"post", "确认");
		echo $html_text;
	}

	/******************************
	服务器异步通知页面方法
	其实这里就是将notify_url.php文件中的代码复制过来进行处理

	*******************************/
	function notifyurl(){
		//通过C函数来读取配置项
		$alipay_config = C('alipay_config');
		//计算得出通知验证结果
		$alipayNotify = new \AlipayNotify($alipay_config);
		$verify_result = $alipayNotify->verifyNotify();
		if($verify_result) {
			//验证成功
			//获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
			$out_trade_no   = I('post.out_trade_no');      //商户订单号
			$trade_no       = I('post.trade_no');          //支付宝交易号
			$trade_status   = I('post.trade_status');      //交易状态
			$total_fee      = I('post.total_fee');         //交易金额
			$notify_id      = I('post.notify_id');         //通知校验ID。
			$notify_time    = I('post.notify_time');       //通知的发送时间。格式为yyyy-MM-dd HH:mm:ss。
			$buyer_email    = I('post.buyer_email');       //买家支付宝帐号；
			
			$parameter = array(
				"out_trade_no"  => $out_trade_no, //商户订单编号；
				"trade_no"      => $trade_no,     //支付宝交易号；
				"total_fee"     => $total_fee,    //交易金额；
				"trade_status"  => $trade_status, //交易状态
				"notify_id"     => $notify_id,    //通知校验ID。
				"notify_time"   => $notify_time,  //通知的发送时间。
				"buyer_email"   => $buyer_email,  //买家支付宝帐号；
			);
			if($trade_status == 'TRADE_FINISHED') {
				//
			}else if ($trade_status == 'TRADE_SUCCESS') {
				if(!$this->checkOrderStatus($out_trade_no)){
					$order = $this->orderHandle($out_trade_no);  //进行订单处理，并传送从支付宝返回的参数；
					$this->addTrading($parameter);               //将从支付宝返回的参数保存；
					print_r($order);exit;
					if(!empty($order['pay_link'])){
						$this->redirect($order['pay_link']);exit;
					}
				}
			}else if($_POST['trade_status'] == 'WAIT_SELLER_SEND_GOODS') {
				//该判断表示买家已在支付宝交易管理中产生了交易记录且付款成功，但卖家没有发货
		
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
				echo "success";		//请不要修改或删除
				if(!$this->checkOrderStatus($out_trade_no)){
					$order = $this->orderHandle($out_trade_no);  //进行订单处理，并传送从支付宝返回的参数；
					$this->addTrading($parameter);               //将从支付宝返回的参数保存；
					if(!empty($order['pay_link'])){
						$this->redirect($order['pay_link']);exit;
					}
				}
				$this->redirect(C('alipay.pay_successpage'));   //跳转到配置项中配置的支付成功页面；
				
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			}
			echo "success";        //请不要修改或删除
		}else {
			//验证失败
			echo "fail";
		}    
	}

	/*
	页面跳转处理方法；
	这里其实就是将return_url.php这个文件中的代码复制过来，进行处理； 
	*/
	function returnurl(){
		
		//通过C函数来读取配置项
		$alipay_config = C('alipay_config');
		
		$alipayNotify = new \AlipayNotify($alipay_config);//计算得出通知验证结果
		$verify_result = $alipayNotify->verifyReturn();
		if($verify_result) {
			//验证成功
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
			$out_trade_no   = I('get.out_trade_no');     //商户订单号
			$trade_no       = I('get.trade_no');         //支付宝交易号
			$trade_status   = I('get.trade_status');     //交易状态
			$total_fee      = I('get.total_fee');        //交易金额
			$notify_id      = I('get.notify_id');        //通知校验ID。
			$notify_time    = I('get.notify_time');      //通知的发送时间。
			$buyer_email    = I('get.buyer_email');      //买家支付宝帐号；

			$parameter = array(
				"out_trade_no" => $out_trade_no,      //商户订单编号；
				"trade_no"     => $trade_no,          //支付宝交易号；
				"total_fee"    => $total_fee,         //交易金额；
				"trade_status" => $trade_status,      //交易状态
				"notify_id"    => $notify_id,         //通知校验ID。
				"notify_time"  => $notify_time,       //通知的发送时间。
				"buyer_email"  => $buyer_email,       //买家支付宝帐号
			);

			if($trade_status == 'TRADE_FINISHED' || $trade_status == 'TRADE_SUCCESS') {
				if(!$this->checkOrderStatus($out_trade_no)){
					$order = $this->orderHandle($out_trade_no);  //进行订单处理，并传送从支付宝返回的参数；
					$this->addTrading($parameter);               //将从支付宝返回的参数保存；
					print_r($order);exit;
					if(!empty($order['pay_link'])){
						$this->redirect($order['pay_link']);exit;
					}
				}
				$this->redirect(C('alipay.pay_successpage'));   //跳转到配置项中配置的支付成功页面；
			}else if($_POST['trade_status'] == 'WAIT_SELLER_SEND_GOODS') {
				//该判断表示买家已在支付宝交易管理中产生了交易记录且付款成功，但卖家没有发货
		
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
				echo "success";		//请不要修改或删除
				if(!$this->checkOrderStatus($out_trade_no)){
					$order = $this->orderHandle($out_trade_no);  //进行订单处理，并传送从支付宝返回的参数；
					$this->addTrading($parameter);               //将从支付宝返回的参数保存；
					if(!empty($order['pay_link'])){
						$this->redirect($order['pay_link']);exit;
					}
				}
				$this->redirect(C('alipay.pay_successpage'));   //跳转到配置项中配置的支付成功页面；
				
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			}else {
				echo "trade_status=".$trade_status;
				$this->redirect(C('alipay.pay_errorpage'));     //跳转到配置项中配置的支付失败页面；
			}
		}else {
			//验证失败
			//如要调试，请看alipay_notify.php页面的verifyReturn函数
			echo "支付失败！";
		}
	}
	
	//在线交易订单支付处理函数
	//函数功能：根据支付接口传回的数据判断该订单是否已经支付成功；
	//返回值：如果订单已经成功支付，返回true，否则返回false；
	function checkOrderStatus($sn){
		$orders = D('Orders');
		$order = $orders->getOrderBySn($sn);
		if($order['pay_status']==1){
			return true;
		}else{
			return false;    
		}
	}

	//处理订单函数
	//更新订单状态，写入订单支付后返回的数据
	function orderHandle($sn){
		
		$orders = D('Orders');

		$order = $orders->getOrderBySn($sn);
		
		if($order['pay_status'] == 0){
			// 修改支付状态
			$orders->updateOrders($order['id'], array('pay_status'=>1, 'pay_date'=>time(), 'pay_mode'=>0));
			
			// 支付完成后,订单操作
			$orders->successPayOrderOperation($order['id']);
		}
		return $order;
	}

	//保存交易信息
	//写入订单支付后支付宝返回的数据
	function addTrading($parameter){
		
		$data = array();
		$data['order_no']     = $parameter['out_trade_no'];
		$data['trade_no']     = $parameter['trade_no'];
		$data['total_fee']    = $parameter['total_fee'];
		$data['trade_status'] = $parameter['trade_status'];
		$data['notify_id']    = $parameter['notify_id'];
		$data['notify_time']  = $parameter['notify_time'];
		$data['buyer_email']  = $parameter['buyer_email'];
		
		$orders = D('Orders');
		$orders->addTrading($data);
	} 
	
}