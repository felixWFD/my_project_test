<?php
namespace   Home\Controller;

class SearchController extends BaseController {

    public function index () {
		
		$keyword = str_replace('+', ' ', I('get.keyword'));
		$type = I('get.type');
		$type = empty($type)?'p':$type; 
		
		if(preg_match("/[\',:;*?~`!@#$%^&+=)(<>{}]|\]|\[|\/|\\\|\"|\|/",$keyword)){
			$this->error("搜索词 不允许带有特殊字符");
		} 
		
		$searchRes = $this->_key_list($keyword);
		foreach($searchRes as &$s){
			$ss = explode(" ",strtolower($s));
			if(strpos($ss[0], $ss[1]) !== false){
				$s = $ss[1];
			}
			if(strpos($ss[1], $ss[0]) !== false){
				$s = $ss[0];
			}
		}
		$searchRes = array_values(array_unique($searchRes));
		$newkeys = array();
		for($i=0;$i<count($searchRes);$i++){
			$z = 1;
			for($j=0;$j<count($searchRes);$j++){
				if($i != $j){
					$inf = strripos($searchRes[$j], $searchRes[$i]);
					if($inf !== false){
						$z = 0;
						break;
					}
				}
			}
			if($z){
				$newkeys[] = $searchRes[$i];
			}
		}
		$tables = array(
			'v'=>array('tab'=>'video', 'title'=>'视频'),
			'w'=>array('tab'=>'wenda_contents', 'title'=>'问答'),
			'e'=>array('tab'=>'explain', 'title'=>'课堂'),
			'n'=>array('tab'=>'news_contents', 'title'=>'资讯'),
		);
		$searchCount = 0;
		$listRes = array();
		foreach($tables as $key=>$tb){
			$where = array();
			$model = M($tb['tab']);
			if(!empty($newkeys)){
				foreach($newkeys as $search){
					if($key == 'v'){
						$where[$key] = "title LIKE '%{$search}%' AND status=1";
					}elseif($key == 'n' || $key == 'w' || $key == 'e'){
						$where[$key] = "title LIKE '%{$search}%' AND is_show=1";
					}
					
					$count = $model->where($where[$key])->count();
					$list = $model->where($where[$key])->order('id DESC')->limit(0,49)->select();
					foreach($list as $vo){
						if(empty($vo['thumbnail'])){
							$vo['thumbnail'] = CBWGrabImages(htmlspecialchars_decode($vo['content']));  //抓取内容第一张图片 作为列表图
						}else{
							if($key=='n'){
								$vo['thumbnail'] = C('SITE_URL').'/Public/News/'.$vo['thumbnail'];
							}elseif($key=='w'){
								$vo['thumbnail'] = C('SITE_URL').'/Public/Wenda/'.$vo['thumbnail'];
							}
						}

						if($key=='n' || $key=='w'){
							$vo['new_title'] = $this->_highlight($search, $vo['title']);
							$vo['new_desc'] = $this->_highlight($search, $vo['description']);
						}elseif($key == 'v'){
							$vo['new_title'] = $this->_highlight($search, $vo['title']);
						}elseif($key == 'e'){
							$vo['new_title'] = $this->_highlight($search, $vo['title']);
						}
						$listRes[$key][$vo['id']] = $vo;
					}
					$count = count($listRes[$key]);
					$tables[$key]['count'] = $count;
				}
				$searchCount += $count;				
			}
		}

		//SEO
        $ident ="search";
        $idents =$this->seo($ident);
        $this->assign("title",empty($keyword)?$idents['title']:$keyword);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->assign('keyword', $keyword);
		$this->assign('type', $type);
		$this->assign('searchCount', $searchCount);
		$this->assign('tables', $tables);
		$this->assign('typename', $typename);
		$this->assign('getPageCount', count($listRes[$type]));
		$this->assign('listRes', $listRes[$type]);
        $this->display('index');
    }
	
	//字符串 加亮
	private function _highlight($search, $str){
		$searchRes = explode(" ",$search);
		foreach($searchRes as $val){
			if($val != 'i' && $val != 'class' && $val != 'keyWords'){
				$str = str_ireplace($val, '<i class="keyWords">'.$val.'</i>', $str);
			}
		}
        return $str;
    }
	
	//分词,并把词保存到数据库
    private function _key_list($keyword){
		
		if(empty($keyword)){
			return '';
		}
		
		$modelSearch = M('search');
		
		//空格拆分,组合多词
		$keys = $this->_key_group($keyword);
		
		//中文分词
		Vendor('phpanalysis.phpanalysis');
		$pa = new \PhpAnalysis();
		$pa->SetSource($keyword);
		$pa->resultType=2;
		$pa->differMax=true;
		$pa->StartAnalysis();
		$arr = $pa->GetFinallyIndex();
		
		foreach($arr as $key=>$count){
			if(strlen($key) > 3 && strlen($key) <=12){
				$keys[] = $key;
				if(!is_numeric($key)){
					$keywordsRes = $modelSearch->field("id, times")->where("keyword = '{$key}'")->find();
					if ($keywordsRes){
						$times = $keywordsRes['times'] + 1;
						$modelSearch->where("id = '{$keywordsRes['id']}'")->data(array('times'=>$times))->save();
					}else{
						if(!empty($key) && strlen($key)>1){
							$modelSearch->create(array('keyword'=>$key));
							$modelSearch->add();
						}
					}
				}
			}
		}
		
        return array_unique($keys);
    }
	
	//拆分,组合多个词
	private function _key_group($keyword){
		
		$arrs = explode(" ",$keyword);
		if(count($arrs)>1){
			$keys = $arrs;
		}
		
		$modelSearch = M('search');
		//读取数据表,查找匹配词
		$searchRes = $modelSearch->field("keyword")->select();
		foreach($searchRes as $vo){
			$find = stristr($keyword,$vo['keyword']);
			if($find){
				$keys[] = $vo['keyword'];
			}
		}
		$count = count($keys);
		$res = array();
		$res[] = $keyword;
		if($count > 1){
			for($i=0; $i<$count-1; $i++){
				for($j=$i+1; $j<$count; $j++){
					$res[] = $keys[$i]." ".$keys[$j];
				}
				$res[] = $keys[$i];
			}
		}
		return $res;
	}

}