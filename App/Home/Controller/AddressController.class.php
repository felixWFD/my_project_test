<?php
namespace Home\Controller;
/**
 * 地址控制器
 */
class AddressController extends BaseController {
	
    /**
	 * 地址列表
	 */
    public function index(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$userAddress = D('UserAddress');
		$address = $userAddress->getAddressByUserId($userId);
		
		// 三级联动
		$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$c = \Think\Area::city($cityArray);
		
		$this->assign("city", $c);
		$this->assign("address", $address);
		$this->assign("title", "地址管理");
		$this->display();
    }
	
	/**
	 * POST加载地址内容
	 */
	public function organize(){
		$this->isUserLogin();
		
		$id = I('post.id');
		
		$userAddress = D('UserAddress');
		$area        = D('Area');
		
		$address = $userAddress->getAddressById($id);
		$provinces = $area->getProvince();
		$htmlProvince = '<option value="0">选择省</option>';
		foreach($provinces as $vo){
			if($address['one_id'] == $vo['id']){
				$htmlProvince .= '<option value="'.$vo['id'].'" selected>'.$vo['addr'].'</option>';
			}else{
				$htmlProvince .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
			}
		}
		$citys = $area->getCityByProvinceId($address['one_id']);
		$htmlCity = '<option value="0">选择市</option>';
		foreach($citys as $vo){
			if($address['two_id'] == $vo['id']){
				$htmlCity .= '<option value="'.$vo['id'].'" selected>'.$vo['addr'].'</option>';
			}else{
				$htmlCity .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
			}
		}
		$areas = $area->getAreaByCityId($address['two_id']);
		$htmlArea = '<option value="0">选择县/区</option>';
		foreach($areas as $vo){
			if($address['area_id'] == $vo['id']){
				$htmlArea .= '<option value="'.$vo['id'].'" selected>'.$vo['addr'].'</option>';
			}else{
				$htmlArea .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
			}
		}
		
		echo json_encode(array('address'=>$address,'provinces'=>$htmlProvince,'citys'=>$htmlCity,'areas'=>$htmlArea));
	}
	
	/**
	 * 更新地址
	 */
	public function update(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$id     = I('post.id');
		
		$userAddress = D('UserAddress');

		$data['user_id']       = $userId;
		$data['reciver_user']  = I('post.reciver_user');
		$data['reciver_phone'] = I('post.reciver_phone');
		$data['area_id']       = I('post.area');
		$data['street']        = I('post.street');
		if(empty($id)){
			$userAddress->insertAddress($data);
		}else{
			$userAddress->updateAdderss($id, $data);
		}
		$this->redirect('Address/index');
	}
	
	/**
	 * 删除地址
	 */
    public function delete(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$id    = I('post.id');
		
		$userAddress = D('UserAddress');
		$default = $userAddress->getDefaultAddressByUserId($userId);
		if($id == $default['id']){
			echo 0;exit;
		}
		$userAddress->deleteAdderss($id);
		echo 1;exit;
    }
}