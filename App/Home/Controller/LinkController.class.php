<?php
namespace   Home\Controller;

class LinkController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    public function links(){
		
		$modelLinks = M('links');
		$linkRes = $modelLinks->select();
		$this->assign("linkRes",$linkRes);
		
		//文章分类列表
		$modelArticle = M('article_detail');
		$modelArticleCat = M('article_cat');
		$articleRes = $modelArticleCat->where("is_show<>0")->order('sort ASC,id ASC')->select();
		foreach($articleRes as $key=>$item){
			$articleRes[$key]['list'] = $modelArticle->field("id,title")->where("cat_id=".$item['id'])->order("id ASC")->select();	
		}
		$this->assign("articleRes",$articleRes);
		
		//SEO
        $ident ="link";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);

        $this->display();
    }

}