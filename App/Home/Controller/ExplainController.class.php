<?php
namespace Home\Controller;

class ExplainController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    //说明书列表
    public function index(){
        
		$cat = I('get.cat');
		
		$modelExplain = M('explain');
		
		//说明书分类列表
		$modelCat = M('product_cat');
		$catRes = $modelCat->field('id,cat_name')->where(array('fid'=>'34','is_show'=>'1'))->order('sort ASC')->select();
		foreach($catRes as $key=>$vo){
			$brandRes = $modelCat->field('id,cat_name')->where(array('fid'=>$vo['id'],'is_show'=>'1'))->order('sort ASC')->select();
			foreach($brandRes as $k=>$v){
				$modelRes = $modelCat->field('id,cat_name')->where(array('fid'=>$v['id'],'is_show'=>'1'))->order('sort ASC')->select();
				foreach($modelRes as $m){
					if($m['id'] == $cat){
						$catRes[$key]['is_on'] = 1;
						$brandRes[$k]['is_on'] = 1;
					}
				}
				
				$brandRes[$k]['modelRes'] = $modelRes;
			}
			$catRes[$key]['brandRes'] = $brandRes;
		}
		
        // 获取查询产品的类型
		$parameter = 'index';
		if(!empty($cat)){
			$where = "cat_id=".$cat;
			$parameter = $cat;
		}else{
			$where = "brand_id in(37,38)";
		}
	
        $getPageCount = $modelExplain->where($where)->count();

        // 分页处理
        $page = new \Think\MyPage($getPageCount, 8);
		$page->url = 'explain_list/'.$parameter.'/';
		$explainRes = $modelExplain->where($where)->order("create_time DESC")->limit($page->firstRow, $page->listRows)->select();	
        $pageShow = $page->show();
		
		$this->assign('cat', $cat);
        $this->assign('page', $pageShow);
        $this->assign('catRes', $catRes);
		$this->assign('explainRes', $explainRes);
		
		//SEO
        $ident ="explain";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
        $this->display();
		
    }
	
    //说明书详细
    public function detail(){
       
		$modelExplain = M('explain');
		$modelCat = M('product_cat');
		
		$id = I('get.id');
		
		//增加点击数
		$modelExplain->where('id = '.$id)->setInc('clicktimes',1);
				
		$explain = $modelExplain->where('id='.$id)->find();
		if(!is_array($explain)){
			$this->redirect('Empty/index');
		}
		$explain['content'] = CBWKeywordAddLink(htmlspecialchars_decode($explain['content'])); // 关键词加链接
		$explain['content'] = CBWImageUpdateAttribute($explain['title'], $explain['content']); // 更新图片属性 
		$this->assign('explain',$explain);
		
		//说明书分类列表
		$modelCat = M('product_cat');
		$catRes = $modelCat->field('id,cat_name')->where(array('fid'=>'34','is_show'=>'1'))->order('sort ASC')->select();
		foreach($catRes as $key=>$vo){
			$brandRes = $modelCat->field('id,cat_name')->where(array('fid'=>$vo['id'],'is_show'=>'1'))->order('sort ASC')->select();
			foreach($brandRes as $k=>$v){
				$modelRes = $modelCat->field('id,cat_name')->where(array('fid'=>$v['id'],'is_show'=>'1'))->order('sort ASC')->select();
				foreach($modelRes as $m){
					if($m['id'] == $explain['cat_id']){
						$catRes[$key]['is_on'] = 1;
						$brandRes[$k]['is_on'] = 1;
					}
				}
				
				$brandRes[$k]['modelRes'] = $modelRes;
			}
			$catRes[$key]['brandRes'] = $brandRes;
		}
		$this->assign('catRes',$catRes);
		
		//上一篇
		$previous = $modelExplain->where('cat_id='.$explain['cat_id'].' AND id<'.$explain['id'])->order('id DESC')->find();
		$this->assign('previous',$previous);
		//下一篇
		$next = $modelExplain->where('cat_id='.$explain['cat_id'].' AND id>'.$explain['id'])->order('id ASC')->find();
		$this->assign('next',$next);
		
		//SEO
		if($explain['keywords'] == '' && $explain['description']==''){
			$ident ="explain";
			$idents =$this->seo($ident);
		}else{
			$idents['title'] = $explain['title'];
			$idents['keywords'] = $explain['keywords'];
			$idents['description'] = $explain['description'];
		}
		$this->assign("title",$idents['title']."_草包课堂");
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);
		
        $this->display();
		
    }

}
