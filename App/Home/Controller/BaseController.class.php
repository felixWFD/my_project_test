<?php
namespace Home\Controller;
/**
 * 基础控制器
 */
use Think\Controller;
class BaseController extends Controller {
	 
	public function __construct(){
		parent::__construct();
		
		if(CBWIsMobile()){   //手机访问,页面跳转手机端
			$url = getUrl();
			$url = str_replace("www.", "m.", $url);
			header('Location:'.$url);
		}
		
		// 如果COOKIE['cbw_auto']存在，并且用户不在登录状态
		$cbw = cookie('cbw_auto');
        if(isset($cbw) && !session('user.userid')){
            $cbw = explode('|',CBWCheckAutoLogin($cbw));
			$users = D('Users');
			$user = $users->getUserAndInfoById($cbw[0]);
			if($user['status'] > 0){
				//$ip  = ip2long(I('server.REMOTE_ADDR'));
				//if($cbw[3] == $ip){
					// 保存登录SESSION
					session(array('name'=>'userLogin', 'prefix'=>'user'));
					session('userid',   $user['id']);
					session('groupid',  $user['group_id']);
					session('vip',      $user['vip']);
					session('thumb',    $user['thumb']);
					session('username', cookie('cbw_uname'));
				//}
			}
        }
		
		// 如果COOKIE['system_auto']存在，并且店铺不在登录状态
		$system = cookie('system_auto');
        if(isset($system) && !session('shop.id')){
            $system = explode('|',CBWCheckAutoLogin($system));
            //$ip  = ip2long(I('server.REMOTE_ADDR'));
            //if($system[1] == $ip){
				// 保存登录SESSION
				session(array('name'=>'shopLogin', 'prefix'=>'shop'));
				session('id',   $system[0]);
				session('name', cookie('system_sname'));
            //}
        }
		
		$province = cookie('location_province');
		$city     = cookie('location_city');
        if(isset($city)){
			if(preg_match('/[a-zA-Z]/',$city)){
				cookie(null,'location_');
				// 获取城市信息
				$this->getCityByIp();
			}else{
				$data = array('province'=>$province, 'city'=>$city);
				session('location',$data);
			}
        }else{
            // 获取城市信息
			$this->getCityByIp();
		}
		
		// 系统配置
		$system = D('System');
		$GLOBALS['CONFIG'] = $system->loadConfigs();
		$this->assign('CONF',$GLOBALS['CONFIG']);
		// 网址域名
		$this->assign("CBW_URL",CBWDomain());
		$this->assign("CBW_CITY",session('location.city'));
	}

    // ip地址获取所在城市(新),通过淘宝的ip库查询
    private function getCityByIp(){
        $ip=get_client_ip(0,true);
        if(filter_var($ip, FILTER_VALIDATE_IP)){//验证ip格式
            $location_data=getCityByTaoBaoApi($ip);
            if($location_data!=false){
                $data = array('province'=>$location_data['region'], 'city'=>$location_data['city']);
                //保存到cookie
                cookie('province', $data['province'], array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
                cookie('city',     $data['city'],     array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
                //保存到session
                session('location',$data);
            }
        }
        //设置默认值
        $city=cookie('location_city');
        if(is_null($city)||strripos($city,'内网IP')!==false){
            $data=array('province'=>'广东省', 'city'=>'深圳');
            cookie('province', $data['province'], array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
            cookie('city',     $data['city'],     array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
            //保存到session
            session('location',$data);
        }
    }

	
	// ip地址获取所在城市   2018/8/6 改用淘宝的ip库
//	private function old_getCityByIp(){
//		Vendor('ORG.Net.IpLocation');                               // 导入IpLocation类
//		$Ip = new \Org\Net\IpLocation('UTFWry.dat');                // 实例化类
//		$data = $Ip->getlocation();                                 // 获取某个IP地址所在的位置
//		$data['country']  = @iconv('gbk','utf-8',$data['country']); // 转换格式，防止乱码
//		$data['area']     = @iconv('gbk','utf-8',$data['area']);    // 转换格式，防止乱码
//		$data['province'] = '';                               // 默认省份
//		$data['city']     = '深圳市';                               // 默认城市 防止city为空值
//
//		$especially = array("北京市","上海市","天津市","重庆市","香港","澳门");    // 特殊地区
//		$provinces  = array("黑龙江省","辽宁省","吉林省","河北省","河南省","湖北省","湖南省","山东省","山西省","陕西省", "安徽省","浙江省","江苏省","福建省","广东省","海南省","四川省","云南省","贵州省","青海省","甘肃省","江西省","台湾省","内蒙古","宁夏","新疆","西藏","广西","北京市","上海市","天津市","重庆市","香港","澳门");
//		foreach($provinces as $province) {
//			if(strpos($data['country'],$province) === 0) {
//				$data['province'] = $province;
//				if(in_array($province,$especially)){
//					$data['city'] = $province;
//                    $data['province'] = '';//如果是特殊地区则将它的省名称置空
//				}else{
//					$data['city'] = str_replace($province,'',$data['country']);
//
//				}
//				break;
//			}
//		}
//
//		//保存到cookie
//		cookie('province', $data['province'], array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
//		cookie('city',     $data['city'],     array('prefix' => 'location_', 'expire' => C('AUTO_TIME_LOGIN')));
//
//		//保存到session
//		session('location',$data);
//	}
	
	/**
	* 推送消息到店铺
	* 包含(微信,手机短信)
	* @param $orderId 订单编号
	* @param $lng     经度
	* @param $lat     纬度
	* @param $shopId  店铺编号
	* @param $way     服务方式
	* @return
	*/
    public function message($orderId, $lng, $lat, $way, $shopId=0){
		$us      = D('UserShop');
		$uw      = D('UserWechat');
		$cat     = D('ProductCat');
		$repairs = D('Repairs');
		$orders  = D('Orders');
		
		$offer = 0;
		$warranty = 0;
		
		if($orderId > 0){
			$order = $orders->getOrderById($orderId);
            //order_type  0：购物订单  1：维修订单  2：回收订单
			if($order['order_type'] == 1){//维修订单
				$first = "我需要手机维修服务，请亲及时报出你能维修的真实价格并联系我，谢谢！";
				$server = $orders->getOrderServerByOrderId($orderId);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$offer    = $relation['money'];
					if(!empty($relation['color_money'])){
						$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
						$offer = number_format($colormoneys[$server['color_id']], 2, '.', '');
					}
					$warranty = $relation['warranty'];
					$modelId  = $relation['product_id'];
					$type     = $cat->getCat($relation['type_id']);
					$brand    = $cat->getCat($relation['brand_id']);
					$keyword3 = $type['cat_name']." ".$brand['cat_name']." ".$relation['pro_name']." ".$server['name']." ".$relation['plan_name'];
				}else{
					$keyword3 = $order['remarks'];
				}
			}elseif($order['order_type'] == 2){//回收订单
				$first = "我需要手机回收服务，请亲及时报出你能维修的真实价格并联系我，谢谢！";
				$recovery = $orders->getOrderRecoveryByOrderId($orderId);
				if($recovery['reco_id'] > 0){
					$repair = $repairs->getRepairById($recovery['reco_id']);
					$modelId  = $repair['id'];
					$type     = $cat->getCat($repair['type_id']);
					$brand    = $cat->getCat($repair['brand_id']);
					$keyword3 = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
					$offer    = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
				}else{
					$keyword3 = $order['remarks'];
				}
			}
			
			// 推送消息(微信/通知/手机短信)
			$wechatObj = new \Think\WechatApi();
			
			if($shopId > 0){
				$shops[0] = $us->getShopByShopId($shopId);
			}else{
				$shops = $us->getShopsByLngLat($lng, $lat, 0, 99, $way);
			}
			$i = 0;
			$ids = array();
			foreach($shops as $key=>$vo){
				if($vo['server_range'] > 0){
					$distance = CBWGetDistance($vo['lng'], $vo['lat'], $lng, $lat);
					$s = round($distance/1000);
					if($s > $vo['server_range']){
						continue;
					}
				}
				
				// 直营店直接报价
				if($vo['type_id'] == 2){
					$data = array(
						'order_id'      => $orderId,
						'shop_id'       => $vo['id'],
						'offer'         => $offer,
						'warranty'      => $warranty,
						'virtual_money' => 0,
						'offer_date'    => time(),
						'is_free'       => 1,
						'remarks'       => ''
					);
					$offerId = $orders->insertOrderOffer($data);
					if($offerId > 0){
						$orders->updateOrderOfferIng($orderId, $vo['id'], array('is_offer'=>1));
						if($i >= 5){
							$orders->updateOrders($orderId, array('is_offer'=>0));
							break;
						}
					}
				}
				
				// 店铺信息
				$isSwitch = 1;
				$umodel = $us->getUserModelByUserId($vo['user_id']);
				if($modelId > 0 && count($umodel) > 0){
					$model = $us->getUserModelByModelId($vo['user_id'], $modelId);
					if(empty($model)){
						$isSwitch = 0;
					}
				}
				
				if($isSwitch == 1){
					// 微信
					$wechat = $uw->getWechatByUserId($vo['user_id']);
					if(!empty($wechat['fakeid'])){
						$data = array(
							"touser"      => $wechat['fakeid'],
							"template_id" => "x-XCV1YsEyEjmY38EK20z3lJj7i_8mgOKscF0CvWgt0",
							"url"         => CBWDomain()."/offer/".$order["id"]."-".$vo['id'].".html",
							"topcolor"    => "#FF0000",
							"data"        => array(
								"first" => array(
									"value" => $first,
									"color" => "#173177"
								),
								"keyword1" => array(
									"value" => $order["order_sn"],
									"color" => "#173177"
								),
								"keyword2" => array(
									"value" => date("Y-m-d H:i:s"),
									"color" => "#173177"
								),
								"keyword3" => array(
									"value" => $keyword3,
									"color" => "#173177"
								),
								"keyword4" => array(
									"value" => "请点此链接跳转至订单页面，报价并联系客户，快速报价有助于提高首页排名。",
									"color" => "#173177"
								),
								"remark" => array(
									"value" => "点击详情报价",
									"color" => "#173177"
								)
							)
						);
						$wechatObj->sendTemplateMessage($data);
					}
					
					// 通知
					$content = '<p>我需要手机维修服务，请亲及时报出你能维修的真实价格并联系我，谢谢！</p>';
					$content .= '<p>业务：'.$order["order_sn"].'</p>';
					$content .= '<p>时间：'.date("Y-m-d H:i:s").'</p>';
					$content .= '<p>提醒内容：'.$keyword3.'</p>';
					$content .= '<p>处理建议：请点此链接跳转至订单页面，报价并联系客户，快速报价有助于提高首页排名。 <a href="'.CBWDomain().'/offer/'.$order["id"]."-".$vo['id'].'.html">点击报价</a></p>';
					$notice  = array('shop_id'=>$vo['id'], 'title'=>'业务信息通知，订单号：'.$order["order_sn"], 'content'=>$content, 'create_time'=>time());
					//$us->insertShopNotice($notice);
					
					// 手机短信
					//sendSmsByAliyun($vo['user_phone'], array(), 'SMS_116561090');  // 报价通知
					
					$i++;
					$ids[$key]['id']      = $vo['id'];
					$ids[$key]['user_id'] = $vo['user_id'];
				}
			}
			
			// 更新订单信息
			if(!empty($ids)){
				foreach($ids as $vo){
					$orders->insertOrderOfferIng(array('shop_id'=>$vo['id'], 'user_id'=>$vo['user_id'], 'order_id'=>$orderId));
				}
			}
			
			if($i > 0){
				session('server.message', 1);
			}
		}
    }
	
	/**
	* 推送消息到店铺(接单通知)
	* 包含(微信,手机短信)
	* @param $orderId 订单编号
	* @param $shopId  店铺编号
	* @return
	*/
    public function pushOrderMessage($orderId, $shopId){
		$us      = D('UserShop');
		$uw      = D('UserWechat');
		$orders  = D('Orders');
		$repairs = D('Repairs');
		
		$offer = 0;
		$warranty = 0;
		
		if($orderId > 0){
			$order = $orders->getOrderById($orderId);
			if($order['order_type'] == 1){
				$server = $orders->getOrderServerByOrderId($orderId);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$offer    = $relation['money'];
					if(!empty($relation['color_money'])){
						$colormoneys = CBWOjectArray(json_decode($relation['color_money']));
						$offer = number_format($colormoneys[$server['color_id']], 2, '.', '');
					}
					$warranty = $relation['warranty'];
				}
				$first = "您有新的维修订单，请及时登录系统与顾客联系并处理。";
				$url   = CBWDomain()."/system/repair.html";
				$type  = "维修订单";
				if($server['server_way'] == 1){
					$type .= "-到站维修";
				}elseif($server['server_way'] == 2){
					$type .= "-上门维修";
				}
			}elseif($order['order_type'] == 2){
				$recovery = $orders->getOrderRecoveryByOrderId($orderId);
				if($recovery['reco_id'] > 0){
					$offer = $repairs->computeMoney($recovery['reco_id'], $recovery['reco_attr']);
				}
				$first = "您有新的回收订单，请及时登录系统与顾客联系并处理。";
				$url   = CBWDomain()."/system/recovery.html";
				$type  = "回收订单";
				if($recovery['reco_way'] == 1){
					$type .= "-到站回收";
				}elseif($recovery['server_way'] == 2){
					$type .= "-上门回收";
				}
			}
			
			$shop = $us->getShopByShopId($shopId);
			
			// 直营店直接报价
			if($shop['type_id'] == 2){
				$data = array(
					'order_id'      => $orderId,
					'shop_id'       => $shop['id'],
					'offer'         => $offer,
					'warranty'      => $warranty,
					'virtual_money' => 0,
					'offer_date'    => time(),
					'is_free'       => 1,
					'remarks'       => ''
				);
				$offerId = $orders->insertOrderOffer($data);
				if($offerId > 0){
					$orders->updateOrderOfferIng($orderId, $shop['id'], array('is_offer'=>1));
					$orders->updateOrders($orderId, array('is_offer'=>0));
				}
			}
			
			// 微信通知
			if($shop['is_wechat'] == 1){
				$wechat = $uw->getWechatByUserId($shop['user_id']);
				if(!empty($wechat['fakeid'])){
					$data = array(
						"touser"      => $wechat['fakeid'],
						"template_id" => "CjyDOCYBnCBKxs8jhMG5EwZYp6nnfSWCvPr5t5OgpQY",
						"url"         => $url,
						"topcolor"    => "#FF0000",
						"data"        => array(
							"first" => array(
								"value" => $first,
								"color" => "#173177"
							),
							"keyword1" => array(
								"value" => $order["order_sn"],
								"color" => "#173177"
							),
							"keyword2" => array(
								"value" => $type,
								"color" => "#173177"
							),
							"keyword3" => array(
								"value" => $order['reciver_user']." ".$order['reciver_phone']." ".$order['pro_addr'].$order['city_addr'].$order['addr'].$order['address'],
								"color" => "#173177"
							),
							"remark" => array(
								"value" => "如果问题请联系客服：400-6233-866",
								"color" => "#173177"
							)
						)
					);
					
					$wechatObj = new \Think\WechatApi();
					$wechatObj->sendTemplateMessage($data);
				}
			}
			
			// 手机短信通知
			if($shop['is_sms'] == 1){
				sendSmsByAliyun($shop['user_phone'], array('name'=>$order['reciver_user'],'tel'=>$order['reciver_phone']), 'SMS_116591038');  // 新订单
			}
		}
    }
	
	/**
	 * 空操作处理
	 */
    public function _empty(){
        header("HTTP/1.0 404 Not Found"); // 使HTTP返回404状态码 
        $this->display("Public:404");
    }
	
	/**
     * 发送短信验证码
     */
	public function sendSms(){
		$mobile     = I('post.mobile');         // 手机号码
		$imgcode    = I('post.imgcode');        // 图形验证码
		$tempid     = I('post.tempid');         // 短信模板编号
		$testid     = I('post.testid');         // 测试编号
		$ischeck    = I('post.ischeck');        // 是否检查手机号码
		$ip         = I('server.REMOTE_ADDR');  // 获取IP地址
		$code       = mt_rand(100000, 999999);  // 生成验证码
		$nowtime    = time();                   // 当前时间
		$code_count = 1;
		
		//记录测试信息
		if(!empty($testid)){
			$modelTest = M("test");
			$data=array('mobile'=>$mobile,'val'=>$testid);
			if($modelTest->create($data)){
				$modelTest->add();
			}
		}else{
			exit;
			$modelTest = M("test");
			$data=array('mobile'=>$mobile,'val'=>$tempid.'-'.$testid);
			if($modelTest->create($data)){
				$modelTest->add();
			}
		}
		
		if($ischeck == 1){
			$users    = D('Users');
			$user = $users->getUserByMobile($mobile);
			if(empty($user)){
				echo json_encode(array('status'=>-1, 'msg'=>'手机号码不存在'));exit;
			}
		}
		
		if(empty($tempid)){
			echo json_encode(array('status'=>-2, 'msg'=>'短信模板为空'));exit;
		}
		
		if(!empty($imgcode)){
			// 检查图形验证码
			$verify = new \Think\Verify();
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-3, 'msg'=>'图形验证码错误！请重试'));exit;
			}
		}
		
		$smsip    = D('SmsIp');
		$usercode = D('UserCode');
		
		$sms = $smsip->get($ip);
		if(is_array($sms)){
			if($sms['sms_count'] >= 6){
				echo json_encode(array('status'=>-4, 'msg'=>'您当日IP累计获取验证码已达上限，请您次日再试!'));exit;
			}
			$smsip->edit(array('id'=>$sms['id'], 'sms_count'=>$sms['sms_count']+1));       // 更新IP操作次数
		}else{
			$sms_count = 1;
			$smsip->insert(array('ip'=>$ip, 'add_time'=>$nowtime, 'sms_count'=>$sms_count)); // 插入IP操作次数
		}
		
		$codeinfo = $usercode->get($mobile);
		if(is_array($codeinfo)){
			if($codeinfo['code_count'] >= 6){
				echo json_encode(array('status'=>-5, 'msg'=>'当日手机号码累计获取验证码已达上限，请您次日再试!'));exit;
			}
			if ($nowtime - $codeinfo['add_time'] < 90){
				echo json_encode(array('status'=>-6, 'msg'=>'操作过于频繁，请您稍后再试'));exit;
			}
			$code = $codeinfo['code'];
			$code_count = $codeinfo['code_count']+1;
		}
		
		// 发送模板短信
		$result = sendSmsByAliyun($mobile, array('code'=>$code), $tempid); // 验证码
		if($result == NULL ) {
			echo json_encode(array('status'=>-7, 'msg'=>'验证失败'));exit;
		}
		if($result->Message != 'OK'){
			echo json_encode(array('status'=>-8, 'msg'=>'发送失败'));exit; //  $datas=$result->statusCode."<br>";//echo "error msg:".$result->statusMsg;
		}else{
			if($code_count == 1){
				$usercode->insert(array('account'=>$mobile, 'code'=>$code, 'add_time'=>$nowtime, 'code_count'=>$code_count)); // 插入发送次数
			}else{
				$usercode->edit(array('id'=>$codeinfo['id'], 'code_count'=>$code_count));                                     // 更新发送次数
			}
			echo json_encode(array('status'=>$code_count, 'msg'=>'发送成功'));exit;
		}
		echo json_encode(array('status'=>0, 'msg'=>'未知错误'));exit;
	}
	
    /**
	 * 验证模块的短信验证码
	 */
	public function checkVerifySms(){
		$code   = I('post.code');
		$mobile = I('post.mobile');
		
		$uc = D('UserCode');

		// 检查验证码
		$rt = $uc->check($mobile, $code);
		if($rt > 0){
			echo true;exit;
		}
		echo false;exit;
	}
	
	/**
     * 验证/保存用户银行卡
     */
	public function saveCard(){
		$this->isUserLogin();

		$cardid     = I('post.cardid');
		$name		= I('post.name');
		$idcardno	= I('post.idcardno');
		$bankcardno	= I('post.bankcardno');
		$bankid	    = I('post.bankid');
		$cardtype	= I('post.cardtype');
		$year	    = I('post.year');
		$month	    = I('post.month');
		$cvv2	    = I('post.cvv2');
		$mobile		= I('post.mobile');
		$imgcode	= I('post.imgcode');
		$userId     = session('user.userid');
		$default    = 0;

		if(!empty($imgcode)){
			// 检查图形验证码
			$verify = new \Think\Verify();
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-1));exit;
			}
		}
		
		$users = D('Users');
		$banks = D('Banks');
		
		$check = $users->checkCardByUserIdCardNo($userId, $bankcardno);
		if($check['status'] == -1){                // 检查会员银行卡是否存在
			echo json_encode(array('status'=>-2));exit;
		}
		
		$user = $users->getUserAndInfoById($userId);
		$bank = $banks->getBankByBankId($bankid);
		if(empty($name) && empty($idcardno)){     // 添加/修改银行卡
			$name	  = $user['true_name'];
			$idcardno = $user['identity'];
			if(!empty($cardid)){
				$card  = $users->getCardById($cardid);
				$bankcardno = $card['card_no'];
			}
		}else{                                    // 实名认证
			$default = 1;
			if(!empty($user['true_name']) && !empty($user['identity'])){
				echo json_encode(array('status'=>-3));exit;
			}
		}

		$bankcard = new \Think\BankCard();
		$auth = $bankcard->verifyCard(array('name'=>$name, 'idcardno'=>$idcardno, 'bankcardno'=>$bankcardno, 'tel'=>$mobile));
		if($auth['isok'] == 1){
			if($auth['code'] == 1){
				if(!empty($cardid)){
					$data = array(
						'card_mobile'  => $mobile,
						'card_exp'     => substr($year, -2).$month,
						'card_cvv2'    => $cvv2
					);
					$users->updateCard($cardid, $data);
				}else{
					$users->updateUserInfo(array('user_id'=>$userId, 'true_name'=>$name, 'identity'=>$idcardno));
					$data = array(
						'user_id'      => $userId,
						'bank_id'      => $bankid,
						'card_type'    => $cardtype,
						'card_no'      => $bankcardno,
						'card_mobile'  => $mobile,
						'card_exp'     => substr($year, -2).$month,
						'card_cvv2'    => $cvv2,
						'is_default'   => $default
					);
					$rt = $users->insertCard($data);
					if($rt['status'] > 0){
						$cardid = $rt['status'];
					}
				}
			}
		}
		echo json_encode(
			array(
				'status'  => $auth['code'],
				'cardid'  => $cardid,
				'type'    => $cardtype,
				'cardno'  => substr($bankcardno, -4),
				'picture' => $bank['picture'],
				'name'    => $bank['name'],
				'mobile'  => hidestr($mobile, 'tel')
			)
		);
		exit;
	}
	
	/**
     * 检查银行卡归属哪家银行
     */
	public function checkCard(){
		$banks = D('Banks');

		$cardNo = I('post.card_no');

		$card8 = substr($cardNo, 0, 8); 
		$card  = $banks->getBankCardByNumber($card8);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		$card6 = substr($cardNo, 0, 6); 
		$card  = $banks->getBankCardByNumber($card6);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		$card5 = substr($cardNo, 0, 5); 
		$card  = $banks->getBankCardByNumber($card5);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		$card4 = substr($cardNo, 0, 4); 
		$card  = $banks->getBankCardByNumber($card4);
		if(!empty($card)) { 
			echo json_encode($card);exit;
		}
		echo false;
	}

	/**
	 * 产生中文验证码图片
	 * @return [source]
	 */
	public function getChinaVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		$verify = new \Think\Verify($config);
		$verify->useZh = true;
        $verify->fontttf = 'msyh.ttf';
		$verify->entry();
	}
	
	/**
	 * 产生数字验证码图片
	 * 
	 */
	public function getVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		// 导入Image类库
    	$verify = new \Think\Verify($config);
    	$verify->entry();
    }
	
	/**
	 * 产生数学验证码图片
	 * 
	 */
	public function getArithVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		// 导入Image类库
    	$verify = new \Think\Verify($config);
		$verify->arith = true;
		$verify->fontttf = 'msyh.ttf';
    	$verify->entry();
    }
   
    /**
	 * AJAX验证模块的码校验
	 */
	public function checkAjaxVerify(){
		
		$code = I('post.code');
		$verify = new \Think\Verify();

		// 检查验证码
		if ($verify->check($code)){
			echo true;exit;
		}
		echo false;exit;
	}
	
    /**
	 * 验证模块的码校验
	 */
	public function checkVerify(){
		
		$code = I('post.code');
		$verify = new \Think\Verify();

		// 检查验证码
		if ($verify->check($code)){
			return true;
		}
		return false;
	}
	
    /**
     * 核对单独的验证码
	 * $re = false 的时候不是ajax返回
	 * @param  boolean $re [description]
	 * @return [type]      [description]
	 */
	public function checkCodeVerify($re = true){
		$code = I('code');
		$verify = new \Think\Verify(array('reset'=>false));    
		$rs =  $verify->check($code);		
		if ($re == false) return $rs;
		else $this->ajaxReturn(array('status'=>(int)$rs));
	}
	
	/**
     * ajax程序验证,只要不是会员都返回-999
     */
    public function isUserLogin() {
    	$userId = session('user.userid');
		if (empty($userId)){
			if(IS_AJAX){
				$this->ajaxReturn(array('status'=>-999,'url'=>'Users/login'));
			}else{
				$this->redirect("Users/login");
			}
		}
	}
	
	/**
	 * 获取用户可报价信息
	 */
	public function loadUserOffer(){
		$userId = session('user.userid');
		$type   = I('post.type');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$users   = D('Users');
		$us      = D('UserShop');
		$cat     = D('ProductCat');
		
		if($type == 'repair'){//加载附近正在用户报价的订单信息，以维修门店身份
			$data = $orders->getOrderOfferIngByUserId(array('userId'=>$userId, 'status'=>0, 'm'=>0, 'n'=>4));
			foreach($data as &$vo){
				$order = $orders->getOrderById($vo['order_id']);
				$user  = $users->getUserAndInfoById($order['user_id']);
				$vo['order_date'] = $order['order_date'];
				$vo['remarks']    = $order['remarks'];
				$vo['uname']      = $user['uname'];
				$vo['thumb']      = $user['thumb'];
				if($order['order_type'] == 1){//维修订单
					$server = $orders->getOrderServerByOrderId($vo['order_id']);
					if($server['relation_id'] > 0){
						$relation = $repairs->getRepairByRelationId($server['relation_id']);
						$vo['details'] = $relation['pro_name']." ".$relation['plan_name'];
					}else{
						$vo['details'] = $vo['remarks'];
					}
				}elseif($order['order_type'] == 2){//回收订单
					$recovery = $orders->getOrderRecoveryByOrderId($vo['order_id']);
					if($recovery['reco_id'] > 0){
						$repair = $repairs->getRepairById($recovery['reco_id']);
						$type     = $cat->getCat($repair['type_id']);
						$brand    = $cat->getCat($repair['brand_id']);
						$vo['details'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
					}else{
						$vo['details'] = $vo['remarks'];
					}
				}
				$vo['show_time'] = CBWTimeFormat($vo['order_date'], 'm.d');
			}
			unset($vo);
		}else{//加载自己报修或回收的订单报价通知信息，以维个人身份
			$data = $orders->getOrderOfferByUserId($userId, 0, 4);
			foreach($data as &$vo){
				$order = $orders->getOrderById($vo['order_id']);
				$vo['order_type'] = $order['order_type'];
				if($order['order_type'] == 1){//维修订单
					$server = $orders->getOrderServerByOrderId($vo['order_id']);
					if($server['relation_id'] > 0){
						$relation = $repairs->getRepairByRelationId($server['relation_id']);
						$vo['details'] = $relation['pro_name']." ".$relation['plan_name'];
					}else{
						$vo['details'] = $vo['remarks'];
					}
					$shop = $us->getShopByShopId($server['shop_id']);
				}elseif($order['order_type'] == 2){//回收订单
					$recovery = $orders->getOrderRecoveryByOrderId($vo['order_id']);
					if($recovery['reco_id'] > 0){
						$repair = $repairs->getRepairById($recovery['reco_id']);
						$type     = $cat->getCat($repair['type_id']);
						$brand    = $cat->getCat($repair['brand_id']);
						$vo['details'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
					}else{
						$vo['details'] = $vo['remarks'];
					}
					$shop = $us->getShopByShopId($recovery['shop_id']);
				}
				$vo['shop_name']  = $shop['shop_name'];
				$vo['logo_image'] = $shop['logo_image'];
				$vo['show_time']  = CBWTimeFormat($vo['offer_date'], 'm.d');
			}
			unset($vo);
		}
		echo json_encode($data);exit;
    }
	
	/*
	 * SEO
	 * 如果SEO表没有数据,则读取网站配置
	 *
	 */
	public function seo($ident){
		
		$seo = M("seo");
		$map['ident'] = array("eq",$ident);
		$seolist = $seo->where($map)->find();
		if(!is_array($seolist)){
			$modelConfig = M('sys_config');
			$syss = $modelConfig ->where("cname in ('sy_webname','sy_webkeyword','sy_webmeta')") -> select();
			foreach($syss as $sys){
				if($sys['cname'] == 'sy_webname'){
					$seolist['title'] = $sys['cvalue'];
				}else if($sys['cname'] == 'sy_webkeyword'){
					$seolist['keywords'] = $sys['cvalue'];
				}else if($sys['cname'] == 'sy_webmeta'){
					$seolist['description'] = $sys['cvalue'];
				}
				
			}
			
		}
		return $seolist;
	}

    /**
     * 执行城市三级联动下拉框获取操作
     */
    public function changeSelect(){
		
        $id   = I('post.id');
		$type = I('post.type');

		$area = D('Area');

        if($type == 'province'){
			$list = $area->getCityByProvinceId($id);
		}else{
			$list = $area->getAreaByCityId($id);
		}
		$str = '';
		foreach($list as $vo){
			$str .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
		}
        echo $str;
		exit;
    }
	
	/**
	 * 随机生成一个用户账号
	 */
	public function randomAccount(){
		
		for($i=0;$i<99;$i++){
			$account = CBWGetRandom();
			$crs = $this->checkAccount($account);
			if($crs['status']==1)return $account;
		}
		return '';
	}
	
	/**
	  * 查询用户账号是否存在
	  */
	public function checkAccount($account){
	 	$rd = array('status'=>-1);
	 	if($account=='')return $rd;
	 	$sql = " account='".$account."' ";
	 	$modelUsers = M('users');
	 	$rs = $modelUsers->where($sql)->count();
	    if($rs==0)$rd['status'] = 1;
	    return $rd;
	}
	
	/**
	 * 随机生成一个用户昵称
	 */
	public function randomUserName(){
		
		for($i=0;$i<99;$i++){
			$uname = 'u'.substr(md5(time()), 0, 6);
			$crs = $this->checkUserNickName($uname);
			if($crs['status']==1)return $uname;
		}
		return '';
	}
	
	/**
	  * 查询用户昵称是否存在
	  */
	public function checkUserNickName($userNick,$userId = 0,$isCheckKeys = true){
	 	$rd = array('status'=>-1);
	 	if($userNick=='')return $rd;
	 	if($isCheckKeys){
		 	if(!CBWCheckFilterWords($userNick,$GLOBALS['CONFIG']['sy_fkeyword'])){
		 		$rd['status'] = -2;
		 		return $rd;
		 	}
	 	}
	 	$sql = " uname='".$userNick."' ";
	    if($userId>0){
	 		$sql.=" AND id<>".$userId;
	 	}
	 	$modelUsers = M('users');
	 	$rs = $modelUsers->where($sql)->count();
	    if($rs==0)$rd['status'] = 1;
	    return $rd;
	}

}

 