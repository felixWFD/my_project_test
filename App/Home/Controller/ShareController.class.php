<?php
namespace Home\Controller;

class ShareController extends BaseController {
	
    public function share(){
		$shopId  = I('get.id');
		($shopId == 0 || $shopId == '') && $this->error('参数错误');
		$dir = date("Ym");
		$this->assign("dir",$dir);
		$this->assign("shopId",$shopId);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);

		$this->display();
    }
}
