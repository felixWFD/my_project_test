<?php
namespace Home\Controller;

class VipController extends BaseController {
	
    // 购买VIP服务
    public function purchase(){
		$this->isUserLogin();
		
		$vip    = session('user.vip');
		$userId = session('user.userid');
		
		($vip == 0) && $this->error('该用户组无法购买VIP服务');
		
		$users = D('Users');
		$us    = D('UserShop');
		
		$user = $users->getUserAndInfoById($userId);
		$this->assign("user",$user);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Vip/purchase');
    }
	
	//提交订单
	public function order(){
		$this->isUserLogin();
		
		$orders = D('Orders');
		$users  = D('Users');
		
		$userId = session('user.userid');
		$user = $users->getUserById($userId);
		$times  = I('get.times');
		!in_array($times, array(1,3,6,12)) && $this->error('参数错误');
		$viptime = strtotime("+$times month", $user['vip_time']);
		switch($times){
			case '1':
				$money = 1000;
				break;
			case '3':
				$money = 3000;
				break;
			case '6':
				$money = 6000;
				break;
			case '12':
				$money = 12000;
				break;
		}
		
		$orderSn = CBWCreateOrderNumber();
		
        $data['user_id']       = $userId;
        $data['order_sn']      = $orderSn;
        $data['order_type']    = 3;
        $data['order_date']    = time();
        $data['order_status']  = 1;
        $data['pay_status']    = 0;
		$data['deail_price']   = $money;
		$data['remarks']       = $times;
		$orderId = $orders->insertOrders($data);
        if($orderId){
			$this->redirect('Buy/pay', array('id'=>$orderSn));
        }
		$this->error('订单生成失败,请联系管理员.');
		exit;
	}

}
