<?php
namespace Home\Controller;

class NewsController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    //新闻列表
    public function index(){
               
		$modelNews = M('news_contents');
		$modelCat = M('news_cat');
		
		//新闻分类列表
		$catRes = $modelCat->where("is_show=1")->order('id ASC')->select();
		
		//新闻页广告
        $modelAdv = M('advs');
        $photoRes = $modelAdv->field('id, adv_name, thumbnail')
                              ->where(array('position_id'=>5, 'is_show'=>1))
                              ->order('id DESC')
                              ->limit(5)
                              ->select();
		$this->assign("photoRes",$photoRes);
		
		//最新资讯
		$new_news = $modelNews->order('create_time DESC')->limit(10)->select();
		$this->assign('new_news', $new_news);
		
        // 获取查询产品的类型
		$cat = I('get.cat');
		$where = "is_show=1";
		$parameter = "index";
		if(!empty($cat)){
			$where .= " AND cat_id=".$cat;
			$parameter = $cat;
			$nav = $modelCat->field('id,cat_name')->where('id='.$cat)->find();
			$this->assign('nav', $nav);
		}
	
        $getPageCount = $modelNews->where($where)->count();

        // 分页处理
        $page = new \Think\MyPage($getPageCount, 10);
		$page->url = 'news_list/'.$parameter.'/';
		$newsRes = $modelNews->where($where)->order("create_time DESC")->limit($page->firstRow, $page->listRows)->select();	
		foreach($newsRes as $key=>$vo){
			if(empty($vo['thumbnail'])){
				$thumbnail = CBWGrabImages(htmlspecialchars_decode($vo['content']));  //抓取内容第一张图片 作为列表图
				$thumbnail = '<img src="'.$thumbnail.'" title="'.$vo['title'].'" alt="'.$vo['title'].'" />';
			}else{
				$thumbnail = '<img src="'.C('SITE_URL').'/Public/News/'.$vo['thumbnail'].'" title="'.$vo['title'].'" alt="'.$vo['title'].'" />';
			}
			$newsRes[$key]['thumbnail'] = $thumbnail;
		}		
        $pageShow = $page->show();
		
		$this->assign('cat', $cat);
        $this->assign('page', $pageShow);
        $this->assign('catRes', $catRes);
		$this->assign('newsRes', $newsRes);
		
		//SEO
        $ident ="news";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);

        $this->display();
    }
	
    //新闻详细
    public function detail(){
       
		$modelNews = M('news_contents');
		$modelCat = M('news_cat');
		
		$id = I('get.id');
		
		//增加点击数
		$modelNews->where('id = '.$id)->setInc('clicktimes',1);
		
		//新闻分类列表
		$catRes = $modelCat->where("is_show=1")->order('id ASC')->select();
		$this->assign('catRes',$catRes);
		
		//新闻页广告
        $modelAdv = M('advs');
        $topRes = $modelAdv->field('id, adv_name, thumbnail, link_url')
                              ->where(array('position_id'=>7, 'is_show'=>1))
                              ->order('id DESC')
                              ->limit(5)
                              ->select();
		$this->assign("topRes",$topRes);
		$leftRes = $modelAdv->field('id, adv_name, thumbnail, link_url')->where("id=5")->find();
		$this->assign("leftRes",$leftRes);
				
		$news = $modelNews->where('id='.$id)->find();
		if(!is_array($news)){
			$this->redirect('Empty/index');
		}
		$news['content'] = CBWKeywordAddLink(htmlspecialchars_decode($news['content'])); // 关键词加链接
		$news['content'] = CBWImageUpdateAttribute($news['title'], $news['content']);    // 更新图片属性
		
		// 把标识cbwurl替换为"预约维修"按钮
		if(!empty($news['link'])){
			$botton = '<span class="bespoke"><a href="'.$news['link'].'" target="_blank">预约维修</a></span>';
			$news['content'] = preg_replace('/{cbwurl}/sui', $botton , $news['content'], 3); 
		}
		
		$this->assign('news',$news);
		
		//分类信息
		$nav = $modelCat->field('id,cat_name')->where('id='.$news['cat_id'])->find();
		$this->assign('nav', $nav);
		
		//相关资讯
		$keys = explode(",",$news['keywords']);
		$where = "id !=".$news['id']." AND (";
		foreach($keys as $key=>$item){
			if($key == 0){
				$where .= "title like '%".$item."%'";
			}else{
				$where .= " OR title like '%".$item."%'";
			}
		}
		$where .= ")";
		$relevant_news = $modelNews->where($where)->order('create_time DESC')->limit(10)->select();
		$this->assign('relevant_news', $relevant_news);
		
		//上一篇
		$previous = $modelNews->where('cat_id='.$news['cat_id'].' AND id<'.$news['id'])->order('id DESC')->find();
		$this->assign('previous',$previous);
		//下一篇
		$next = $modelNews->where('cat_id='.$news['cat_id'].' AND id>'.$news['id'])->order('id ASC')->find();
		$this->assign('next',$next);
		
		//SEO
		$this->assign("title",$news['title']);
		$this->assign("keywords",$news['title']);
		$this->assign("description",$news['description']);
		
        $this->display();
		
    }

}
