<?php
namespace Home\Controller;

class ArticleController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    //文章列表
    public function article(){
        
		$modelArticle = M('article_detail');
		$modelArticleCat = M('article_cat');
		
		//文章分类列表
		$articleRes = $modelArticleCat->where("is_show<>0")->order('sort ASC,id ASC')->select();
		foreach($articleRes as $key=>$item){
			$articleRes[$key]['list'] = $modelArticle->field("id,title")->where("cat_id=".$item['id'])->order("id ASC")->select();	
		}
        // 获取查询产品的类型
		$id = I('get.id');
		$id = empty($id)?18:$id;
	
		$article = $modelArticle->where("id=".$id)->find();		
		if(!is_array($article)){
			$this->redirect('Empty/index');
		}
		
		//点击次数加一
		$modelArticle ->where('id = '.$id)->setInc('clicktimes',1);
		
		//SEO
		if($article['keywords'] == '' && $article['description']==''){
			$ident ="article";
			$idents =$this->seo($ident);
		}else{
			$idents['title'] = $article['title'];
			$idents['keywords'] = $article['keywords'];
			$idents['description'] = $article['description'];
		}
		$this->assign("title",$idents['title']);
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);
		
		$this->assign('id', $id);
		$this->assign('article', $article);
		$this->assign('articleRes', $articleRes);
        $this->display();
    }

}
