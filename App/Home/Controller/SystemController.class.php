<?php
namespace Home\Controller;

class SystemController extends BaseController {	
	
	public function __construct(){   
		parent::__construct();
		
		$this->isUserLogin();
    }
	
	// 系统首页
	public function index(){
		$shopId = session('shop.id');
		
		if(empty($shopId)){
			$this->redirect("Users/logout");    //页面跳转
		}
		
		// 维修师系统公告
		$modelConfig = M('sys_config');
		$syss = $modelConfig->where("cname='sy_repair_notice'")->find();
		$this->assign("notice",$syss['cvalue']);
		
		// 个人信息
		$model = M();
		$shop = $model->table('__USER_SHOP__ AS s')
					->field('s.*,u.account,p.addr as province,c.addr as city,a.addr as area')
					->join('__USERS__ AS u ON u.id=s.user_id')
					->join('__ADDR_AREA__ AS a ON a.id=s.area_id')
					->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
					->join('__ADDR_PROVINCE__ AS p ON p.id=a.one_id')
					->where("s.id = $shopId")
					->find();
					
		$level = CBWShopLevel($shop['exp']);         // 等级换算
		$shop['level_image_name'] = $level['name'];
		$shop['level_image_num']  = $level['num'];
		$shop['level_level']      = $level['level'];
		
		$shop['integrity'] = $this->integrity();     // 店铺完善度
		$this->assign("shop",$shop);
		
		// 专属客服
		$service = $model->table('__ADMIN_USERS__ AS u')
							->field('u.*,i.*')
							->join('__ADMIN_INFO__ AS i ON i.uid=u.id')
							->where('u.id='.$shop['service_id'])
							->find();
		$this->assign("service",$service);
		
		// 我的收支明细
		$modelMoney = M('user_money');
		$moneyRes = $modelMoney->where("user_id=$shopId")->order('create_time DESC')->limit(0, 5)->select();		
        $this->assign('moneyRes', $moneyRes);
		
		$this->display();
	}
	
	// 店铺完善度
	private function integrity(){
		$shopId = session('shop.id');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		if($shop['is_main'] < 1){
			$shop = $us->getMainShopByUserId($shop['user_id']);
		}
		$inte = 10;
		if(!empty($shop['logo_image'])){
			$inte +=10;
		}
		if(!empty($shop['description'])){
			$inte +=10;
		}
		if(!empty($shop['content'])){
			$inte +=10;
		}
		if(!empty($shop['qq'])){
			$inte +=10;
		}
		if(!empty($shop['open_hours'])){
			$inte +=10;
		}
		if(!empty($shop['server_area'])){
			$inte +=10;
		}
		$model = $us->getUserModelByUserId($shop['user_id']);
		if(count($model) > 0){
			$inte +=30;
		}
		return $inte;
	}
	
	// 订单搜索
	public function search(){
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');

		$orders    = D('Orders');
		$service   = D('Service');
		$repairs   = D('Repairs');
		$shippings = D('Shippings');

        $count = $orders->getRepairCountByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'way'=>3));
        $page = new \Think\Page($count, 8);
		$data = $orders->getRepairByObject(array('shopId'=>0, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'way'=>3, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			if($vo['relation_id'] > 0 && $vo['color_id'] > 0){
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$color    = $repairs->getColorById($vo['color_id']);
				$vo['show_name']  = $relation['pro_name']." ".$color['name']."<br />".$relation['plan_name'];
				$vo['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
			}else{
				$vo['show_name']  = $vo['remarks'];
				$vo['show_img']   = '/Public/Home/images/2016/logo_03.png';
			}
		}
		
		// 物流库
		$shipping = $shippings->getCommonShippingCompany();

        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		$this->assign("keyword",$keyword);
		$this->assign("startdate",$start);
		$this->assign("enddate",$end);
		$this->assign("shipping",$shipping);
		
		$this->display();
	}
	
	// 加载物流信息
	public function loadShipping(){
		$way = I('post.way');   // 物流公司编号
		$num = I('post.num');   // 物流单号
		
		$modelCompany = M("shipping_company");
		$company = $modelCompany->field("cname")->where("id = ".$way)->find();
		
		$modelExpress = M('express');
		if(!empty($num)){
			$express = $modelExpress->where("number='".$num."'")->find();
			$content = CBWOjectArray(json_decode($express['content']));
			foreach($content as $key=>$vo){
				$expInfo[$key]['time'] = $vo['time']; 
				$expInfo[$key]['context'] = $vo['context'];
			}
		}
		echo json_encode(array($company['cname'],$expInfo));
	}
	
	// 订单信息
	public function order(){
		$orderId = I('post.orderid');
		
		$model = M();
		
		//订单信息
		$orders = $model->table('__ORDERS__ AS o')
							->field("o.order_sn,o.order_date,o.address,o.deail_price,o.reciver_user,o.reciver_phone,o.remarks,s.relation_id,s.relation_ids,s.server_shipping_way,s.server_shipping_num,rc.name,p.addr as province,c.addr as city,a.addr as area")
							->join('__ORDER_SERVER__ s ON s.order_id = o.id')
							->join('__REPAIR_COLOR__ AS rc ON rc.id = s.color_id')
							->join('__ADDR_AREA__ AS a ON o.area_id = a.id')
							->join('__ADDR_CITY__ AS c ON a.two_id = c.id')
							->join('__ADDR_PROVINCE__ AS p ON a.one_id = p.id')
							->where("o.id=".$orderId)
							->find();
		//故障信息
		$info = $model->table('__REPAIR_RELATION__ AS r')
				->field("p.type_id,p.brand_id,p.pro_name,f.description,f.name as fault_name,p2.name")
				->join('__REPAIRS__ p ON r.product_id = p.id')
				->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
				->join('__REPAIR_PLAN__ p2 ON r.plan_ids = p2.id')
				->where("r.id=".$orders['relation_id'])
				->find();
		//重新评估的维修信息
		if(!empty($orders['relation_ids'])){
			$ids = explode(",",$orders['relation_ids']);
			$newInfo = array();
			for($i=0;$i<count($ids);$i++){
				$relation = $model->table('__REPAIR_RELATION__ AS r')
					->field("f.name as fault_name,p2.name as plan_name")
					->join('__REPAIR_FAULT__ f ON r.fault_id = f.id')
					->join('__REPAIR_PLAN__ p2 ON r.plan_ids = p2.id')
					->where("r.id=".$ids[$i])
					->find();
				$newfault[$i] = $relation['fault_name'];
				$newplan[$i] = $relation['plan_name'];
			}
			$info['description'] = implode(",",$newfault);
			$info['name'] = implode(",",$newplan);
		}
		
		//物流公司
		$modelShipping = M('shipping_company');
		$shipping = $modelShipping->where("id=".$orders['server_shipping_way'])->find();
		
		$data['order_sn']     = $orders['order_sn'];
		$data['order_date']   = date('Y-m-d H:i:s',$orders['order_date']);
		$data['user']         = $orders['reciver_user'];
		$data['phone']        = $orders['reciver_phone'];
		$data['shipping_way'] = $shipping['cname'];
		$data['shipping_num'] = $orders['server_shipping_num'];
		$data['adds']         = $orders['province'].$orders['city'].$orders['area'].$orders['position'].$orders['address'];
		$modelRepairCat       = M('product_cat');
		$type = $modelRepairCat->field("cat_name")->where("id=".$info['type_id'])->find();  //类型
		$data['type']         = $type['cat_name'];
		$brand = $modelRepairCat->field("cat_name")->where("id=".$info['brand_id'])->find();//品牌
		$data['brand']        = $brand['cat_name'];
		$data['model']        = $info['pro_name'];
		$data['color']        = $orders['name'];
		$data['fault']        = $info['description'];
		$data['plan']         = $info['name'];
		$data['money']        = $orders['deail_price'];
		$data['remarks']      = $orders['remarks'];
		
		echo json_encode($data);
		exit;
	}
	
	// 签收订单
	public function sign(){
		$shopId  = session('shop.id');
		$orderId = I('post.orderid');
		$shipWay = I('post.shipping_way');
		$shipNum = I('post.shipping_num');
		
		$orders = D('Orders');
		
		$server = $orders->getOrderServerByOrderId($orderId);
		if(empty($server['server_shipping_num'])){
			$rt = $orders->updateOrderServer($orderId, array('shop_id'=>$shopId, 'shipping_way'=>$shipWay, 'shipping_num'=>$shipNum));
		}else{
			if($server['shipping_way'] != $shipWay || $server['shipping_num'] != $shipNum){
				$this->error("物流单号错误！",U('System/search'));exit;
			}
			$rt = $orders->updateOrderServer($orderId, array('shop_id'=>$shopId));
		}
		if ($rt['status'] > 0){
			$this->redirect('System/repair');exit;
		}
        $this->error("未知错误！",U('System/search'));exit;
	}
	
	// 维修订单
	public function repair(){
		$shopId  = session('shop.id');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');

		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$shippings = D('Shippings');

        $count = $orders->getRepairCountByObject(array("shopId"=>$shopId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status));
        $page = new \Think\Page($count, 8);
		$data = $orders->getRepairByObject(array("shopId"=>$shopId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			if($vo['relation_id'] > 0 && $vo['color_id'] > 0){
				$relation = $repairs->getRepairByRelationId($vo['relation_id']);
				$color    = $repairs->getColorById($vo['color_id']);
				$vo['show_name']  = $relation['pro_name']." ".$color['name']."<br />".$relation['plan_name'];
				$vo['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
			}else{
				$vo['show_name']  = $vo['remarks'];
				$vo['show_img']   = '/Public/Home/images/2016/logo_03.png';
			}
			// 订单是否已申诉
			$appeal = $orders->getOrderAppealByOrderId($vo['id']);
			if(empty($appeal)){
				$vo['appeal'] = 1;
			}
		}
		
		// 物流库
		$shipping = $shippings->getCommonShippingCompany();

        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		$this->assign("keyword",$keyword);
		$this->assign("startdate",$start);
		$this->assign("enddate",$end);
		$this->assign("status",$status);
		$this->assign("shipping",$shipping);
		
		$this->display();
	}
	
	// 正在抢单
	public function offerIng(){
		$shopId  = session('shop.id');

		$orders  = D('Orders');
		$repairs = D('Repairs');
		
        $count = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0));
        $page = new \Think\Page($count, 8);
		$data = $orders->getOrderOfferIngByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['pro_name']  = $relation['pro_name'];
				$vo['color_name'] = $color['name'];
				$vo['plan_name'] = $relation['plan_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['id']);
		}
		unset($vo);

		// 未读可以报价数量
		$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));

		$this->assign("offerCount",$offerCount);
        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		
		$this->display('offer_ing');
	}
	
	// 已抢订单
	public function offer(){
		$shopId  = session('shop.id');

		$orders  = D('Orders');
		$repairs = D('Repairs');

        $count = $orders->getOrderOfferCountByShopId($shopId);
        $page = new \Think\Page($count, 8);
		$data = $orders->getOrderOfferByShopId($shopId, $page->firstRow, $page->listRows);
		$pageShow = $page->show();
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['pro_name']  = $relation['pro_name'];
				$vo['pro_name']  = $relation['pro_name'];
				$vo['color_name'] = $color['name'];
				$vo['plan_name'] = $relation['plan_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['id']);
		}
		unset($vo);
		
		// 未读可以报价数量
		$offerCount = $orders->getOrderOfferIngCountByShopId(array('shopId'=>$shopId, 'isOffer'=>0, 'status'=>0));

		$this->assign("offerCount",$offerCount);
        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		
		$this->display('offer');
	}
	
	/**
	 * 创建维修订单
	 */
    public function create(){
		$shopId  = session('shop.id');
		
		$us   = D('UserShop');
		$cat  = D('ProductCat');
		$area = D('Area');
		
		$shop = $us->getShopByShopId($shopId);
		if($shop['type_id'] != 2){
			$this->error("店铺权限不足", U('System/repair'));exit;
		}
		
		$brands = $cat->getCatListById(10);
		
		//加载城市信息
		$info = $area->getAreaById($shop['area_id']);
		$p = empty($info['one_id'])?'选择省':$info['one_id'];
		$c = empty($info['two_id'])?'选择市':$info['two_id'];
		$a = empty($info['id'])?'选择县/区':$info['id'];
        $city = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$c = \Think\Area::city($city);

		$this->assign("brands", $brands);
		$this->assign("city", $c);
		$this->display();
    }

    /**
     * 执行品牌类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeBrandSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByBrandId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择型号</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['pro_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行型号类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeModelSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getColorByRepairId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择颜色</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行颜色类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeColorSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByModelId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择故障与方案</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['fault_name'].' - '.$vo['plan_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行故障关联change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeRelationSelect(){
        $id      = I('get.id');
		$colorId = I('get.color');
		$money   = 0;
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByRelationId($id);
		if(!empty($data)){
			$money = $data['money'];
			if(!empty($data['color_money'])){
				$moneys = json_decode($data['color_money']);
				$money = $moneys[$colorId];
			}
		}
        echo $money;
		exit;
    }
	
	/**
	 * 创建维修订单操作
	 */
    public function doCreateOrder(){
		$shopId     = session('shop.id');
		$way        = I('post.way');
		$money      = I('post.money');
		$uname      = I('post.uname');
		$mobile     = I('post.mobile');
		$password   = substr($mobile, -6);
		$areaId     = I('post.area');
		$address    = I('post.address');
		$colorId    = I('post.color');
		$relationId = I('post.relation');
		$remarks    = I('post.remarks');
		$orderSn    = CBWCreateOrderNumber();
		
		$users  = D('Users');
		$us     = D('UserShop');
		$orders = D('Orders');

		// 店铺信息
		$shop = $us->getShopByShopId($shopId);
		if($areaId == 0){
			$areaId = $shop['area_id'];
		}
		if($shop['type_id'] != 2){
			$this->error("店铺权限不足", U('System/repair'));exit;
		}

		// 检查手机号码是否注册,否则先注册
		$user = $users->getUserByMobile($mobile);
		if(empty($user)){
			$rs = $users->regist($mobile, $password);
			if($rs['userId']>0){                       // 注册成功
				// 发送密码
				sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
				
				// 加载用户信息				
				$user = $users->getUserById($rs['userId']);
			}
		}
		
		if($user['id'] == $shop['user_id']){
			$this->error("不允许下单到自己的店铺", U('System/create'));
		}
		
		$data['user_id']       = $user['id'];
		$data['agent_id']      = $shop['user_id'];
        $data['order_sn']      = $orderSn;
        $data['order_date']    = time();
		$data['order_type']    = 1;
        $data['order_status']  = 1;
        $data['pay_status']    = 0;
		$data['area_id']       = $areaId;
		$data['address']       = $address;
		$data['reciver_user']  = $uname;
		$data['reciver_phone'] = $mobile;
		$data['deail_price']   = $money;
		$data['is_line']       = 1;
		$data['remarks']       = $remarks;
		$orderId = $orders->insertOrders($data);
		if($orderId){
			$data = array();
			$data['order_id']      = $orderId;
			$data['shop_id']       = $shop['id'];
			$data['color_id']      = $colorId;
			$data['relation_id']   = $relationId;
			$data['server_way']    = $way;
			$data['server_status'] = 0;
			$serverId = $orders->insertOrderServer($data);        // 添加维修订单附表
			if($serverId){
				$this->success('创建订单成功',U("Orders/rdetail", array('sn'=>$orderSn)));exit;
			}
			$orders->deleteOrders($orderId);   // 下单失败,删除订单表数据
		}
		$this->error("创建订单失败.");exit;
    }
	
	/**
	 * 关闭订单
	 */
    public function close(){
		$orderId = I('get.id');
		
		$orders = D('Orders');
		
		$order = $orders->getOrderById($orderId);
		$rt = $orders->closeOrders($orderId);
		if($rt['status'] > 0){
			// 取消维修订单或回收订单,给店铺发送一条短信
			sendSmsByAliyun($order['reciver_phone'], array('order'=>$order['order_sn']), 'SMS_116591037'); // 取消维修订单
			
			if($order['order_type'] == '1'){
				$this->redirect('System/repair');
			}else{
				$this->redirect('System/recovery');
			}		
		}
		$this->error("关闭订单失败.");
    }
	
	/**
	 * 完成保障卡维修订单页面
	 */
    public function security(){
		$orderId = I('get.id');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
	
		// 订单信息
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($orderId);

		// 故障信息
		$relation = $repairs->getRepairByRelationId($server['relation_id']);

		$type  = $cat->getCat($relation['type_id']);   // 类型
		$brand = $cat->getCat($relation['brand_id']);  // 品牌
		$order['type']     = $type['cat_name'];
		$order['brand']    = $brand['cat_name'];
		$order['color_id'] = $server['color_id'];
		$order['model']    = $relation['pro_name'];
		$order['fault']    = $relation['description'];
		$order['plan']     = $relation['plan_name'];
		$order['pro_id']   = $relation['pro_id'];

		$this->assign('order',$order);
		
		$this->display();
    }
	
	/**
	 * 完成保障卡维修订单操作
	 */
    public function doSecurity(){
		$orderId = I('post.id');
		
		// 判断有没有文件上传
		$upload1 = I('post.upload1');
		$upload2 = I('post.upload2');
        if(empty($upload1) || empty($upload2)){
			$this->error("请上传照片.");
        }
		
		$orders = D('Orders');
		$users  = D('Users');
		$us     = D('UserShop');
		
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($order['id']);
		if($order['order_status'] == 1 && $server['security'] == 1){
			$rt = $orders->updateOrders($order['id'], array('order_status'=>2, 'pay_status'=>1, 'pay_date'=>time(), 'pay_mode'=>4));
			if($rt['status'] > 0){
				// 保存图片
				$img1 = CBWBase64Upload($upload1, './Public/Order/security/');
				$img2 = CBWBase64Upload($upload2, './Public/Order/security/');
				$orders->updateOrderServer($order['id'], array('security_img'=>$img1.','.$img2));
				
				$server = $orders->getOrderServerByOrderId($order['id']);
				$shop   = $us->getShopByShopId($server['shop_id']);
				$users->appendMoney($shop['user_id'], $order['id'], $order['deail_price'], 2, '');        // 增加店铺用户账户金额
				$users->insertFrozenMoney($shop['user_id'], $order['id'], $order['deail_price'], 2, '');  // 增加店铺用户冻结金额
				
				$this->redirect('System/repair');	
			}
		}
		$this->error("完成订单失败.");
    }
	
	// 发货操作
	public function deliver(){
		$shopId       = session('shop.id');
		$orderId      = I('post.orderid');             // 订单ID
		$shipping_way = I('post.shipping_way');        // 物理公司
		$shipping_num = trim(I('post.shipping_num'));  // 物流单号
		
		$orders = D('Orders');
		
		$order = $orders->getOrderById($orderId);
		$data  = array(
			'return_way' => $shipping_way,
			'return_num' => $shipping_num
		);
		if($order['order_type'] == 1){
			$rt = $orders->updateOrderServer($order['id'], $data);
		}elseif($order['order_type'] == 2){
			$rt = $orders->updateOrderRecovery($order['id'], $data);
		}
		if($rt['status'] > 0){
			if($order['order_status'] == 1 && $order['pay_status'] == 1){
				$orders->updateOrders($order['id'], array('ok_date'=>time(),'order_status'=>2));
			}
			
			// 快递100 订阅请求
			$modelCompany = M('shipping_company');
			$shipping = $modelCompany->where('id='.$shipping_way)->find();
			kd100($shipping['com'], $shipping_num);
			$this->redirect('System/repair');
		}
		$this->error("未知错误",U('System/repair'));
		exit;
	}
	
	// 维修检测改价
	public function editprice(){
		$orderId = I('get.id');
		$shopId  = session('shop.id');
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$cat       = D('ProductCat');
		$shippings = D('Shippings');
	
		// 订单信息
		$order  = $orders->getOrderById($orderId);
		$server = $orders->getOrderServerByOrderId($orderId);
		
		if($server['shop_id'] != $shopId){
			$this->error("该订单已调拨",U('System/repair'));
		}
		
		// 故障信息
		$relation = $repairs->getRepairByRelationId($server['relation_id']);
		
		if($relation['type_id'] > 0){
			$type  = $cat->getCat($relation['type_id']);           // 类型
		}
		if($relation['brand_id'] > 0){
			$brand = $cat->getCat($relation['brand_id']);          // 品牌
		}
		if($server['color_id'] > 0){
			$color = $repairs->getColorById($server['color_id']);  // 颜色
		}
		$order['type']         = $type['cat_name'];
		$order['brand']        = $brand['cat_name'];
		$order['color_id']     = $server['color_id'];
		$order['color_name']   = $color['name'];
		$order['warranty']     = $server['warranty'];
		$order['model']        = $relation['pro_name'];
		$order['fault']        = $relation['description'];
		$order['plan']         = $relation['plan_name'];
		$order['pro_id']       = $relation['pro_id'];
		$order['relation_ids'] = $server['relation_ids'];
		$order['new_price']    = $server['new_price'];
		$order['shop_remarks'] = $server['shop_remarks'];
		$order['server_way']   = $server['server_way'];

		
		if($relation['pro_id'] > 0){
			// 重新评估select
			$relationRes = $repairs->getRepairByModelId($relation['pro_id']);
			// 重新评估故障信息
			if(!empty($server['relation_ids'])){
				$again = $repairs->getRepairByRelationId($server['relation_ids']);
				$order['again_plan_name'] = $again['plan_name'];
			}
		}
		
		if($server['server_way'] == 3){
			// 寄回物流公司信息
			$shipping = $shippings->getShippingCompanyById($server['return_way']);
			$order['return_name'] = $shipping['cname'];
			$order['return_num']  = $server['return_num'];
			// 寄回物流信息
			$data = array();
			if(!empty($order['return_num'])){
				$expresss = $shippings->getExpressByNumber($server['return_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['return'] = $data;
			
			// 寄出物流公司信息
			$shipping = $shippings->getShippingCompanyById($server['shipping_way']);
			$order['shipping_name'] = $shipping['cname'];
			$order['shipping_num']  = $server['shipping_num'];
			// 寄出物流信息
			$data = array();
			if(!empty($server['shipping_num'])){
				$expresss = $shippings->getExpressByNumber($server['shipping_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['shipping'] = $data;
		}
		
		$this->assign('relationRes',$relationRes);
		$this->assign('order',$order);
		
		$this->display();
	}
	
	/**
	 * 更新维修金额
	 */
	public function updateServerMoney(){
		$fault   = I('post.fault');
		$orderId = I('post.oid');
		$colorId = I('post.cid');
		$shopId  = session('shop.id');
		
		$orders = D('Orders');
		$repair = D('Repairs');
		$us     = D('UserShop');
		
		$server = $orders->getOrderServerByOrderId($orderId);
		$shop   = $us->getShopByShopId($shopId);

		$money = 0;
		$relation = $repair->getRepairByRelationId($fault);
		$plan     = $repair->getPlanByPlanId($relation['plan_ids']);
		if(!empty($plan['color_money'])){
			$moneys = json_decode($plan['color_money']);
			$money += $moneys[$colorId];
		}else{
			$money += $plan['money'];
		}
		echo number_format($money, 2, '.', '');
	}
	
	/**
	 * 重新评估
	 */
	public function updateServer(){
		$orderId = I('post.oid');
		$fault   = I('post.fault');
		$money   = I('post.money');
		$remarks = I('post.remarks');

		$orders = D('Orders');
		
		$rt = $orders->updateOrderServer($orderId, array('relation_ids'=>$fault, 'shop_remarks'=>$remarks, 'new_price'=>$money));
		if($rt['status'] > 0){
			echo 1;exit; 
		}else{
			echo 0;exit; 
		}
	}
	
	// 提交申诉 
	public function doAppeal(){
		$orderId = I('post.id');
		$reason  = I('post.reason');
		
		$orders = D('Orders');
		
		$appeal = array(
			'order_id'      => $orderId,
			'reason'        => $reason,
			'appeal_date'   => time(),
			'appeal_status' => 1
		);
		$rt = $orders->insertOrderAppeal($appeal);
		if($rt['status'] > 0){
			$this->success('提交申诉成功');
		}else{
			$this->error('提交申诉失败');
		}
	}
	
	// 回收订单
	public function recovery(){
		$shopId  = session('shop.id');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');

		$orders    = D('Orders');;
		$repairs   = D('Repairs');
		$shippings = D('Shippings');
		$cat       = D('ProductCat');

        $count = $orders->getRecoveryCountByObject(array("shopId"=>$shopId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status));
        $page = new \Think\Page($count, 8);
		$data = $orders->getRecoveryByObject(array("shopId"=>$shopId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
			if($recovery['new_price'] !='0.00'){
				$money = $recovery['new_price'];
			}else{
				$money = $vo['deail_price'];
			}
			if($recovery['reco_id'] > 0){
				$repair = $repairs->getRepairById($recovery['reco_id']);
				$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				$type     = $cat->getCat($repair['type_id']);
				$brand    = $cat->getCat($repair['brand_id']);
				$vo['show_name'] = $type['cat_name']." ".$brand['cat_name']." ".$repair['pro_name'];
				$vo['show_img']  = '/Public/Repair/thumb/'.$repair['list_image'];
				$vo['show_desc'] = $attrs;
			}else{
				$vo['show_name'] = $vo['remarks'];
				$vo['show_img']  = '/Public/Home/images/2016/logo_03.png';
			}
			
			// 订单是否已申诉
			$appeal = $orders->getOrderAppealByOrderId($vo['id']);
			if(empty($appeal)){
				$vo['appeal'] = 1;
			}
			
			$vo['show_price']   = $money;
			$vo['shop_id']      = $recovery['shop_id'];
			$vo['reco_status']  = $recovery['reco_status'];
			$vo['reco_way']     = $recovery['reco_way'];
		}
		unset($vo);
		
		// 物流库
		$shipping = $shippings->getCommonShippingCompany();
	
        $this->assign('page', $pageShow);
		$this->assign("orders",$data);
		$this->assign("keyword",$keyword);
		$this->assign("startdate",$start);
		$this->assign("enddate",$end);
		$this->assign("status",$status);
		$this->assign("shipping",$shipping);
		
		$this->display();
	}
	
	// 回收商品检测评估
	public function assess(){
		$orderId = I('get.id');		
		
		$orders    = D('Orders');
		$repairs   = D('Repairs');
		$cat       = D('ProductCat');
		$shippings = D('Shippings');

		$order    = $orders->getOrderById($orderId);
		$recovery = $orders->getOrderRecoveryByOrderId($orderId);
		$repair   = $repairs->getRepairById($recovery['reco_id']);
		$type     = $cat->getCat($repair['type_id']);
		$brand    = $cat->getCat($repair['brand_id']);
		$recovery['type']       = $type['cat_name'];
		$recovery['brand']      = $brand['cat_name'];
		$recovery['model']      = $repair['pro_name'];
		$recovery['attrs']      = implode('|', $repairs->getAttribute($recovery['reco_attr']));
		$recovery['again_attr'] = implode('|', $repairs->getAttribute($recovery['new_attr']));
		
		if($recovery['reco_id'] > 0){			
			// 基本情况
			$basic    = $repairs->getBasicByModelId($recovery['reco_id']);

			// 功能情况
			$function = $repairs->getFunctionByModelId($recovery['reco_id']);

			// 其他情况
			$other    = $repairs->getOtherByModelId($recovery['reco_id']);
		}
		
		if($recovery['reco_way'] == 3){
			// 寄回物流公司信息
			$shipping = $shippings->getShippingCompanyById($recovery['return_way']);
			$order['return_name'] = $shipping['cname'];
			$order['return_num']  = $recovery['return_num'];
			// 寄回物流信息
			$data = array();
			if(!empty($order['return_num'])){
				$expresss = $shippings->getExpressByNumber($recovery['return_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['return'] = $data;
			
			// 寄出物流公司信息
			$shipping = $shippings->getShippingCompanyById($recovery['shipping_way']);
			$order['shipping_name'] = $shipping['cname'];
			$order['shipping_num']  = $recovery['shipping_num'];
			// 寄出物流信息
			$data = array();
			if(!empty($recovery['shipping_num'])){
				$expresss = $shippings->getExpressByNumber($recovery['shipping_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$order['shipping'] = $data;
		}
		$this->assign('order',$order);
		$this->assign('other',$other);
		$this->assign('function',$function);
		$this->assign('basic',$basic);
		$this->assign('recovery',$recovery);
		
		$this->display();
	}
	
	// 提交检测评估
	public function updateRecovery(){
		$orderId  = I('post.id');
		$money    = I('post.money');
		$ids      = I('post.ids');

		$orders = D('Orders');
		
		$rt = $orders->updateOrderRecovery($orderId, array('new_price'=>$money, 'new_attr'=>$ids));
		if($rt['status'] > 0){
			echo 1;exit;
		}else{
			echo 0;exit;
		}
	}
	
	//计算评估价格
	public function compute(){
		$modelId = I('post.model');
		$ids     = I('post.ids');
		$ids     = implode(",", $ids);

		$repairs = D('Repairs');
		
		$money = $repairs->computeMoney($modelId, $ids);
		
		echo json_encode(array('statue'=>1,'money'=>$money));exit;
	}
	
	/**
	 * 完成回收订单
	 */
    public function complete(){
		$shopId  = session('shop.id');
		$orderId = I('get.id');
		
		$orders = D('Orders');
		$us     = D('UserShop');
		
		$rt = $orders->updateOrders($orderId, array('pay_date'=>time(), 'ok_date'=>time(), 'pay_status'=>1, 'order_status'=>2));
		if($rt['status'] > 0){
			$shop = $us->getShopByShopId($shopId);
			$us->updateUserShop($shopId, array('exp'=>$shop['exp']+10, 'recovery_count'=>$shop['recovery_count']+1));
			$this->redirect('System/recovery');exit;
		}
		$this->error("操作失败.");	
    }
	
	// 店铺设置
	public function shopset(){
		$us   = D('UserShop');
		
		$shop = $us->getShopByShopId();
		$shop['open_hours'] = explode(',', $shop['open_hours']);
		
		// 三级联动
		$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("shop", $shop);
		$this->display();
	}
	
	// 保存店铺设置
	public function doShopset(){
		$shopId = session('shop.id');
		
		$area_id       = I('post.area');
		$address       = I('post.address');
		$lng           = I('post.lng');
		$lat           = I('post.lat');
		$hours_start   = I('post.hours_start');
		$hours_start   = $hours_start<=0?9:$hours_start;
		$hours_close   = I('post.hours_close');
		$hours_close   = $hours_close<=9?18:$hours_close;
		$server_area   = I('post.server_area');
		$server_area   = str_replace("|", ",", str_replace("，", ",", str_replace(" ", ",", $server_area)));    //空格/中文逗号/竖线替换成英文逗号
		
		$us = D('UserShop');
		$shop = $us->getShopByShopId($shopId);
		
		$data = array();
		$data['user_name']     = I('post.user_name');
		$data['user_phone']    = I('post.user_phone');
		$data['description']   = I('post.description');
		$data['content']       = I('post.content');
		$data['notice']        = I('post.notice');
		$data['qq']            = I('post.qq');
		$data['open_hours']    = $hours_start.','.$hours_close;
		$data['is_week']       = I('post.is_week');
		$data['server_area']   = $server_area;
		$data['server_range']  = I('post.server_range');
		$data['is_store']      = I('post.is_store');
		$data['is_door']       = I('post.is_door');
		$data['is_wechat']     = I('post.is_wechat');
		$data['is_sms']        = I('post.is_sms');
		$data['server_status'] = I('post.server_status');
		
		if($area_id > 0 && !empty($address) && !empty($lng) && !empty($lat)){
			$mod = array('area_id'=>$area_id, 'address'=>$address, 'lng'=>$lng, 'lat'=>$lat);
			$data['mod_address'] = json_encode($mod);
		}

		$rd = $us->updateUserShop($shopId, $data);
		if($rd['status'] == '1'){
			if($shop['is_main'] == 1){
				$shops = $us->getShopsByObject(array('userId'=>$shop['user_id'], 'main'=>0));
				$main = array(
					'description'   => I('post.description'),
					'content'       => I('post.content'),
					'notice'        => I('post.notice'),
					'open_hours'    => $hours_start.','.$hours_close,
					'is_week'       => I('post.is_week'),
					'server_area'   => $server_area,
					'server_range'  => I('post.server_range'),
					'is_store'      => I('post.is_store'),
					'is_door'       => I('post.is_door'),
					'is_wechat'     => I('post.is_wechat'),
					'is_sms'        => I('post.is_sms'),
					'server_status' => I('post.server_status')
				);
				foreach($shops as $vo){
					$us->updateUserShop($vo['id'], $main);
				}
			}
			$this->success('修改成功');
		}else{
			$this->error('更新失败');
		}
	}
	
    // 修改店铺形象
	public function image(){
		$shopId = session('shop.id');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		if(!empty($shop['mod_image'])){
			$this->error('正在审核,请不要重复提交!');
		}
		$this->display();
	}
	
	// 裁剪图片
	public function crop(){
		$crop = new \Think\CropAvatar(
		  './Public/Temp/',
		  './Public/User/',
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
		);
		$result  = $crop -> getResult();
		$message = $crop -> getMsg();
		if(!empty($result)){
			$us = D('UserShop');
			$shopId = session('shop.id');
			$shop = $us->getShopByShopId($shopId);
			$rt = $us->updateUserShop($shopId, array('mod_image'=>$result));
			if($rt['status'] > 0){
				unlink("./Public/User/".$shop['mod_image']);                   // 删除旧修改头像
			}
		}

		echo json_encode(array('state'  => 200, 'message' => $message, 'result' => $result));
	}
	
	// 维修设置
	public function repairset(){
		$shopId  = session('shop.id');
		$typeId  = I('get.type');
		$typeId  = empty($typeId)?10:$typeId;

		$us      = D('UserShop');
		$cat     = D('ProductCat');
		$repairs = D('Repairs');

		$shop = $us->getShopByShopId($shopId);                              // 店铺信息

		$types  = $cat->getCatListById(3);                                // 类型
		$brands = $cat->getCatListById($typeId);                          // 品牌
		foreach($brands as &$vo){
			$umodels = $us->getUserModelByBrandId($shop['user_id'], $vo['id']);
			$models  = $repairs->getRepairByBrandId($vo['id']);
			foreach($models as &$vm){
				foreach($umodels as $v){
					if($vm['id'] == $v['model_id']){
						$vm['selected'] = 1;
					}
				}
			}
			unset($vm);
			$vo['models'] = $models;
		}
		unset($vo);

		$this->assign('types', $types);
		$this->assign('brands', $brands);
		$this->assign('type', $typeId);
		$this->display();
	}
	
	// 保存维修设备
	public function updateModel(){
		$shopId   = session('shop.id');
		$brandIds = I('post.brand');
		$modelIds = I('post.model');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId); // 店铺信息

		foreach($brandIds as $brandId){
			$us->deleteShopModel($shop['user_id'], $brandId);
		}
		foreach($modelIds as $modelId){
			$data = explode("-", $modelId);
			$us->insertUserModel(array('user_id'=>$shop['user_id'], 'brand_id'=>$data[0], 'model_id'=>$data[1]));
		}
		echo 1;exit;
	}
	
	// 我的商品
	public function products(){
		$groupId  = session('user.groupid');
		$shopId   = session('shop.id');
		$classId  = I('get.classify_id');
		$typeId   = I('get.type_id');
		$brandId  = I('get.brand_id');
		$catId    = I('get.cat_id');
		$isOnSale = I('get.is_on_sale');
		$keyword  = I('get.keyword');
		
		$cat      = D('ProductCat');
		$products = D('Products');
		$us       = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		
		$classifys = $cat->getCatListById(1);
		if(!empty($classId)){
			$types = $cat->getCatListById($classId);    // 分类下的类型
		}
		if(!empty($typeId)){
			$brands = $cat->getCatListById($typeId);    // 类型下的品牌
		}
		if(!empty($brandId)){
			$cats = $cat->getCatListById($brandId);     // 品牌下的型号
		}
		
		$length = $products->getProductsCountByObject(array('shopId'=>$shopId, 'groupId'=>$groupId, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$catId, 'sale'=>$isOnSale, 'keyword'=>$keyword));
		$page   = new \Think\MyPage($length, 10);  // 分页处理
		$data = $products->getProductsByObject(array('shopId'=>$shopId, 'groupId'=>$groupId, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$catId, 'sale'=>$isOnSale, 'keyword'=>$keyword, 'sort'=>'new', 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$relation = $products->getRelationByProductId($vo['id']);
			$vo['rid'] = $relation[0]['id'];
		}

		$this->assign('classify', $classId);
		$this->assign('type', $typeId);
		$this->assign('brand', $brandId);
		$this->assign('cat', $catId);
		$this->assign('isonsale', $isOnSale);
		$this->assign('keyword', $keyword);
		$this->assign('classifys', $classifys);
		$this->assign('types', $types);
		$this->assign('brands', $brands);
		$this->assign('cats', $cats);
		$this->assign('data', $data);
		$this->assign('page', $pageShow);
		$this->display();
	}

	// 批量商品操作
	public function doBatchProducts(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$ids   = I('post.ids');
		$batch = I('post.batch');
		$eids  = array();

        $products = D('Products');
		
		if(!empty($ids)){
			foreach($ids as $id){
				if($batch == 'off'){
					$products->updateProduct($id, array('is_on_sale'=>0));
				}elseif($batch == 'on'){
					$attribute = $products->getAttributeByProductId($id);
					if(count($attribute) > 0){
						$products->updateProduct($id, array('is_on_sale'=>1));
					}else{
						$eids[] = $id;
					}
					
				}elseif($batch == 'del'){
					$data  = $products->getProductById($id);
					$rtPro = $products->deleteProduct($id);
					if($rtPro['status'] > 0){
						// 删除列表图
						if(!empty($data['list_image'])){
							unlink("./Public/Product/thumb/".$data['list_image']);
						}
						
						// 删除feacher图片
						if(!empty($data['feacher'])){
							preg_match_all("/(src)=([\"|']?)([^ \"'>]+\.('gif|jpg|jpeg|bmp|png'))\\2/i", $data['feacher'], $matches);
							foreach($matches[3] as $val){
								$val = str_replace("&quot;", ".", $val);
								@unlink($val);
							}
						}
						
						// 删除关联
						$products->deleteProductRelationByProductId($id);
						// 删除参数
						$products->deleteProductParamByProductId($id);
						// 删除属性
						$attribute = $products->getAttributeByProductId($id);
						foreach($attribute as $at){
							$items = $products->getAttrsByAttrId($at['id']);
							foreach($items as $vo){
								$ablums = $products->getAlbumByAttrId($vo['id']);
								foreach($ablums as $v){
									$products->deleteProductAlbum($v['id']);
									unlink("./Public/Product/images/source_img/".$v['images']);       //删除相册图片
									unlink("./Public/Product/images/goods_img/".$v['images']);        //删除相册图片
									unlink("./Public/Product/images/thumb_img/".$v['images']);        //删除相册图片
								}
								
								// 删除规格项
								$products->deleteProductAttr($vo['id']);
							}
										
							// 删除规格
							$products->deleteAttribute($at['id']);
						}
					}
				}
			}
			if(!empty($eids)){
				$str_ids = implode(",", $eids);
				$this->error('商品ID:'.$str_ids.'库存/规格为空,不允许上架');exit;
			}
			$this->success('执行成功');exit;
		}
		$this->error('执行失败');exit;
	}

    /**
     * 执行下级分类change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeNextSelect(){
        $id = I('get.id');
		
        $cat = D('ProductCat');

        $data = $cat->getCatListById($id);
		if(!is_array($data) || $id <= 0){
			exit;
		}
		$retString = '<option value="0">请选择</option>';
		foreach($data as $vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['cat_name'].'</option>';
		}
        echo $retString;
		exit;
    }

	// 添加商品
	public function productAdd(){
		$cat      = D('ProductCat');
		$products = D('Products');
		
		$classifys = $cat->getCatListById(1);
		$brands    = $products->getBrands();

		$this->assign('classifys', $classifys);
		$this->assign('brands', $brands);
		$this->display('product_add');
	}

	// 添加商品操作
	public function doProductAdd(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$products = D('Products');
		
		$data = array();
		$data['shop_id']     = session('shop.id');
		$data['classify_id'] = I('post.classify_id');
        $data['type_id']     = I('post.type_id');
        $data['brand_id']    = I('post.brand_id');
        $data['cat_id']      = I('post.cat_id');
		$data['pro_brand']   = I('post.pro_brand');
        $data['pro_name']    = I('post.pro_name');
        $data['pro_price']   = I('post.pro_price');
		$data['pro_sn']      = I('post.pro_sn');
        $data['keywords']    = I('post.keywords');
        $data['description'] = I('post.description');
		$data['is_shipping'] = I('post.is_shipping');
		$data['is_quality']  = I('post.is_quality');
		$data['is_return']   = I('post.is_return');
		$data['warranty']    = I('post.warranty');
		$data['is_on_sale']  = 0;
        $data['feacher']     = I('post.feacher');
        $data['add_time']    = time();
        $data['update_time'] = time();
		$data['verify']      = 0;
		$rt = $products->insertProduct($data);
		if($rt['status'] > 0){
			
			// 列表图
			$listImage = I('post.list_image');
			if (!empty($listImage)){
				$img = CBWBase64Upload($listImage, './Public/Product/thumb/');
				if($img){
					$products->updateProduct($rt['status'], array('list_image'=>$img));
				}
			}
			
			// 参数
			$paramTitle   = I('post.param_title');
			$paramContent = I('post.param_content');
			if(!empty($paramTitle)){
				for($i=0;$i<count($paramTitle);$i++){
					$products->insertProductParam(array('pro_id'=>$rt['status'], 'param_title'=>$paramTitle[$i], 'param_content'=>$paramContent[$i]));
				}
			}
			
			// 属性
			$image     = new \Think\Image();
			$specId    = I('post.spec_id');
			$specTitle = I('post.spec_title');
			if(!empty($specId)){
				for($i=0;$i<count($specId);$i++){
					if(!empty($specId[$i])){
						$insertAttribute = $products->insertAttribute(array('pro_id'=>$rt['status'], 'attr_name'=>$specTitle[$specId[$i]], 'attr_ident'=>$specId[$i], 'sort'=>$i+1));

						$itemId    = I('post.spec_item_id_'.$specId[$i]);
						$itemTitle = I('post.spec_item_title_'.$specId[$i]);
						for($j=0;$j<count($itemId);$j++){
							$insertAttr = $products->insertProductAttr(array('attr_id'=>$insertAttribute['status'], 'attr_value'=>$itemTitle[$j], 'attr_ident'=>$itemId[$j], 'sort'=>$j+1));
							
							$thumbImgs = I('post.spec_item_thumb_'.$itemId[$j]);
							if(!empty($thumbImgs)){
								for($k=0;$k<count($thumbImgs);$k++){
									$img = CBWBase64Upload($thumbImgs[$k], './Public/Temp/');
									if($img){
										// 裁剪图片
										list($width, $height) = getimagesize('./Public/Temp/'.$img);  // 获取图片宽度和高度
										if($width != $height){
											if($width > $height){
												$image->open('./Public/Temp/'.$img)->thumb($height, $height, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
											}else{
												$image->open('./Public/Temp/'.$img)->thumb($width, $width, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
											}
										}
										$image->open('./Public/Temp/'.$img)->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/goods_img/'.$img);
										$image->open('./Public/Temp/'.$img)->thumb(150, 150, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/thumb_img/'.$img);
										
										$products->insertProductAlbum(array('attr_id'=>$insertAttr['status'], 'images'=>$img, 'sort'=>$k+1));
									}
								}
							}
						}
					}
				}
			}
			
			// 关联
			$options = I('post.options');
			$options = json_decode(str_replace("&quot;","\"",$options), true);
			$stock   = 0;
			if(!empty($options['ids'])){
				for($i=0;$i<count($options['ids']);$i++){
					$stock += $options['stock'][$i];
					$ids = $options['ids'][$i];
					$idsArr = explode("_", $ids);
					$idss = array();
					for($j=0;$j<count($idsArr);$j++){
						$attr = $products->getAttrByAttrIdent($idsArr[$j]);
						$idss[$j] = $attr['id'];
					}
					
					$relation = array(
						'pro_id'      => $rt['status'],
						'pro_attr'    => implode("|", $idss),
						'pro_stock'   => $options['stock'][$i],
						'one_price'   => $options['oneprice'][$i],
						'two_price'   => $options['twoprice'][$i],
						'three_price' => $options['threeprice'][$i],
						'four_price'  => $options['fourprice'][$i],
						'goods_sn'    => $options['goodssn'][$i],
						'pro_sn'      => $options['productsn'][$i],
						'four_price'  => $options['fourprice'][$i],
						'pro_weight'  => $options['weight'][$i]
					);
					$products->insertProductRelation($relation);
				}
				$products->updateProduct($rt['status'], array('stock_num'=>$stock));
			}
			
			$this->success('商品添加成功',U('System/products'));
			exit;
		}else{
			$this->error('商品添加失败');
		}
	}

	// 编辑商品
	public function productEdit(){
		$id     = I('get.id');
		$shopId = session('shop.id');
		
		$products = D('Products');
		$cat      = D('ProductCat');

		$bbrands   = $products->getBrands();
		$data      = $products->getProductById($id);
        $params    = $products->getProductParamById($id);
		$classifys = $cat->getCatListById(1);
		if($data['shop_id'] != $shopId){
			$this->error('非法操作');
		}
		if(!empty($data['classify_id'])){
			$types = $cat->getCatListById($data['classify_id']);    // 分类下的类型
		}
		if(!empty($data['type_id'])){
			$brands = $cat->getCatListById($data['type_id']);       // 类型下的品牌
		}
		if(!empty($data['brand_id'])){
			$cats = $cat->getCatListById($data['brand_id']);        // 品牌下的型号
		}
		
		// 规格
		$specs = $products->getAttributeByProductId($data['id']);
		if(!empty($specs)){
			foreach($specs as &$vo){
				$item = $products->getProductAttrByAttributeId($vo['id']);
				foreach($item as &$v){
					$v['ablum'] = $products->getAlbumByAttrId($v['id']);
				}
				unset($v);
				$vo['item'] = $item;
			}
			unset($vo);
		}

		$this->assign('bbrands', $bbrands);
		$this->assign('data', $data);
		$this->assign('params', $params);
		$this->assign('classifys', $classifys);
		$this->assign('types', $types);
		$this->assign('brands', $brands);
		$this->assign('cats', $cats);
		$this->assign('specs', $specs);
		
		$this->display('product_edit');
	}
	
	// 获取商品规格项目数据
	public function loadProductRelations(){
		$id = I('post.id');
		
		$products = D('Products');
		$data = $products->getRelationByProductId($id);
		foreach($data as &$vo){
			$ids = explode("|", $vo['pro_attr']);
			$item = array();
			for($i=0; $i<count($ids); $i++){
				$attr = $products->getAttrByAttrId($ids[$i]);
				$item[$i] = $attr['attr_ident'];
			}
			$vo['item'] = $item;
		}
		echo json_encode($data);exit;
	}

	// 编辑商品操作
	public function doProductEdit(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$id = I('post.pro_id');
		
		$data = array();
		$data['classify_id'] = I('post.classify_id');
        $data['type_id']     = I('post.type_id');
        $data['brand_id']    = I('post.brand_id');
        $data['cat_id']      = I('post.cat_id');
		$data['pro_brand']   = I('post.pro_brand');
        $data['pro_name']    = I('post.pro_name');
        $data['pro_price']   = I('post.pro_price');
		$data['pro_sn']      = I('post.pro_sn');
        $data['keywords']    = I('post.keywords');
        $data['description'] = I('post.description');
		$data['is_shipping'] = I('post.is_shipping');
		$data['is_quality']  = I('post.is_quality');
		$data['is_return']   = I('post.is_return');
		$data['warranty']    = I('post.warranty');
		$data['is_on_sale']  = I('post.is_on_sale');
        $data['feacher']     = I('post.feacher');
        $data['update_time'] = time();
		$data['verify']      = 0;

        $products = D('Products');

		$rt = $products->updateProduct($id, $data);
		if($rt['status'] > 0){
			// 列表图
			$listImage = I('post.list_image');
			$oldImage = I('post.old_list_image');
			if(!empty($listImage)){
				$img = CBWBase64Upload($listImage, './Public/Product/thumb/');
				if($img){
					$products->updateProduct($id, array('list_image'=>$img));
					unlink("./Public/Product/thumb/".$oldImage);  // 删除旧图片
				}
			}
			
			// 规格
			$image     = new \Think\Image();
			$specId    = I('post.spec_id');
			$specTitle = I('post.spec_title');
			
			// 检查规格
			$attribute = $products->getAttributeByProductId($id);
			foreach($attribute as $at){
				if(!in_array($at['attr_ident'], $specId)){
					$items = $products->getAttrsByAttrId($at['id']);
					foreach($items as $vo){
						$ablums = $products->getAlbumByAttrId($vo['id']);
						foreach($ablums as $v){
							$products->deleteProductAlbum($v['id']);
							unlink("./Public/Product/images/source_img/".$v['images']);       //删除相册图片
							unlink("./Public/Product/images/goods_img/".$v['images']);        //删除相册图片
							unlink("./Public/Product/images/thumb_img/".$v['images']);        //删除相册图片
						}
						
						// 删除规格项需删除商品关联
						$relations = $products->getRelationByProductId($id);
						foreach($relations as $v){
							$attrArr = explode("|", $v['pro_attr']);
							if(in_array($vo['id'], $attrArr)){
								$products->deleteProductRelation($v['id']);
							}
						}
						
						// 删除规格项
						$products->deleteProductAttr($vo['id']);
					}
					
					// 删除规格需删除商品关联
					$products->deleteProductRelationByProductId($id);
								
					// 删除规格
					$products->deleteAttribute($at['id']);
				}
			}
			
			if(!empty($specId)){
				for($i=0;$i<count($specId);$i++){
					if(!empty($specId[$i])){
						$specs = $products->getAttributeByAttrIdent($specId[$i]);
						if(empty($specs)){
							// 添加规格
							$insertAttribute = $products->insertAttribute(array('pro_id'=>$id, 'attr_name'=>$specTitle[$specId[$i]], 'attr_ident'=>$specId[$i], 'sort'=>$i+1));
							if($insertAttribute['status'] > 0){
								// 新增规格需删除商品关联
								$products->deleteProductRelationByProductId($id);
								
								$itemId    = I('post.spec_item_id_'.$specId[$i]);
								$itemTitle = I('post.spec_item_title_'.$specId[$i]);
								for($j=0;$j<count($itemId);$j++){
									$insertAttr = $products->insertProductAttr(array('attr_id'=>$insertAttribute['status'], 'attr_value'=>$itemTitle[$j], 'attr_ident'=>$itemId[$j], 'sort'=>$j+1));
									if($insertAttr['status'] > 0){
										$thumbImgs = I('post.spec_item_thumb_'.$itemId[$j]);
										if(!empty($thumbImgs)){
											for($k=0;$k<count($thumbImgs);$k++){
												$img = CBWBase64Upload($thumbImgs[$k], './Public/Temp/');
												if($img){
													// 裁剪图片
													list($width, $height) = getimagesize('./Public/Temp/'.$img);  // 获取图片宽度和高度
													if($width != $height){
														if($width > $height){
															$image->open('./Public/Temp/'.$img)->thumb($height, $height, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
														}else{
															$image->open('./Public/Temp/'.$img)->thumb($width, $width, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
														}
													}
													$image->open('./Public/Temp/'.$img)->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/goods_img/'.$img);
													$image->open('./Public/Temp/'.$img)->thumb(150, 150, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/thumb_img/'.$img);
													
													$products->insertProductAlbum(array('attr_id'=>$insertAttr['status'], 'images'=>$img, 'sort'=>$k+1));
												}
											}
										}
									}
								}
							}
						}else{
							// 修改规格
							$products->updateAttribute($specs['id'], array('attr_name'=>$specTitle[$specId[$i]], 'attr_ident'=>$specId[$i]));

							$itemId    = I('post.spec_item_id_'.$specId[$i]);
							$itemTitle = I('post.spec_item_title_'.$specId[$i]);

							// 检查规格项
							$attr = $products->getProductAttrByAttributeId($specs['id']);
							foreach($attr as $vo){
								if(!in_array($vo['attr_ident'], $itemId)){
									$ablums = $products->getAlbumByAttrId($vo['id']);
									foreach($ablums as $v){
										$products->deleteProductAlbum($v['id']);
										unlink("./Public/Product/images/source_img/".$v['images']);       //删除相册图片
										unlink("./Public/Product/images/goods_img/".$v['images']);        //删除相册图片
										unlink("./Public/Product/images/thumb_img/".$v['images']);        //删除相册图片
									}
						
									// 删除规格项需删除商品关联
									$relations = $products->getRelationByProductId($id);
									foreach($relations as $v){
										$attrArr = explode("|", $v['pro_attr']);
										if(in_array($vo['id'], $attrArr)){
											$products->deleteProductRelation($v['id']);
										}
									}
									
									// 删除规格项
									$products->deleteProductAttr($vo['id']);
								}
							}
							
							for($j=0;$j<count($itemId);$j++){
								if(!empty($itemId[$j])){
									$items = $products->getAttrByAttrIdent($itemId[$j]);
									if(empty($items)){
										// 添加规格项
										$insertAttr = $products->insertProductAttr(array('attr_id'=>$specs['id'], 'attr_value'=>$itemTitle[$j], 'attr_ident'=>$itemId[$j], 'sort'=>$j+1));
									
										$thumbImgs = I('post.spec_item_thumb_'.$itemId[$j]);
										if(!empty($thumbImgs)){
											for($k=0;$k<count($thumbImgs);$k++){
												$img = CBWBase64Upload($thumbImgs[$k], './Public/Temp/');
												if($img){
													// 裁剪图片
													list($width, $height) = getimagesize('./Public/Temp/'.$img);  // 获取图片宽度和高度
													if($width != $height){
														if($width > $height){
															$image->open('./Public/Temp/'.$img)->thumb($height, $height, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
														}else{
															$image->open('./Public/Temp/'.$img)->thumb($width, $width, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
														}
													}
													$image->open('./Public/Temp/'.$img)->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/goods_img/'.$img);
													$image->open('./Public/Temp/'.$img)->thumb(150, 150, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/thumb_img/'.$img);
													
													$products->insertProductAlbum(array('attr_id'=>$insertAttr['status'], 'images'=>$img, 'sort'=>$k+1));
												}
											}
										}
									}else{
										// 修改规格项
										$products->updateProductAttr($items['id'], array('attr_value'=>$itemTitle[$j], 'attr_ident'=>$itemId[$j]));
										
										$thumbImgs = I('post.spec_item_thumb_'.$itemId[$j]);
										$thumbsort = I('post.spec_item_thumbsort_'.$itemId[$j]);
										if(!empty($thumbImgs)){
											for($k=0;$k<count($thumbImgs);$k++){
												$img = CBWBase64Upload($thumbImgs[$k], './Public/Temp/');
												if($img){
													// 裁剪图片
													list($width, $height) = getimagesize('./Public/Temp/'.$img);  // 获取图片宽度和高度
													if($width != $height){
														if($width > $height){
															$image->open('./Public/Temp/'.$img)->thumb($height, $height, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
														}else{
															$image->open('./Public/Temp/'.$img)->thumb($width, $width, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/source_img/'.$img);
														}
													}
													$image->open('./Public/Temp/'.$img)->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/goods_img/'.$img);
													$image->open('./Public/Temp/'.$img)->thumb(150, 150, \Think\Image::IMAGE_THUMB_CENTER)->save('./Public/Product/images/thumb_img/'.$img);
													
													$products->insertProductAlbum(array('attr_id'=>$items['id'], 'images'=>$img, 'sort'=>$thumbsort[$k]));
												}else{
													$images = $products->getAlbumByImages($thumbImgs[$k]);
													if(!empty($images)){
														$products->updateProductAlbum($images['id'], array('sort'=>$thumbsort[$k]));
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			// 关联
			$options = I('post.options');
			$options = json_decode(str_replace("&quot;","\"",$options), true);
			$stock   = 0;
			if(!empty($options['ids'])){
				for($i=0;$i<count($options['ids']);$i++){
					$stock += $options['stock'][$i];
					$idsArr = explode("_", $options['ids'][$i]);
					$idss = array();
					for($j=0;$j<count($idsArr);$j++){
						$attr = $products->getAttrByAttrIdent($idsArr[$j]);
						$idss[$j] = $attr['id'];
					}
					$relation = array(
						'pro_id'      => $id,
						'pro_attr'    => implode("|", $idss),
						'pro_stock'   => $options['stock'][$i],
						'one_price'   => $options['oneprice'][$i],
						//'two_price'   => $options['twoprice'][$i],
						'three_price' => $options['threeprice'][$i],
						//'four_price'  => $options['fourprice'][$i],
						'goods_sn'    => $options['goodssn'][$i],
						'pro_sn'      => $options['productsn'][$i],
						'four_price'  => $options['fourprice'][$i],
						'pro_weight'  => $options['weight'][$i]
					);
					if(empty($options['id'][$i])){
						$products->insertProductRelation($relation);
					}else{
						$products->updateProductRelation($options['id'][$i], $relation);
					}
					
				}
				$products->updateProduct($id, array('stock_num'=>$stock));
			}
			
			$this->success('商品编辑成功',U('System/productEdit',array('id'=>$id)));
			exit;
		}else{
			$this->error('商品编辑失败');
		}
	}

	// 删除商品操作
	public function doProductDelete(){		
		$id     = I('get.id');
		$shopId = session('shop.id');
		
        $products = D('Products');
		
		$data = $products->getProductById($id);
		if($data['shop_id'] != $shopId){
			$this->error('非法操作');
		}
		
		$rtPro = $products->deleteProduct($id);
		if($rtPro['status'] > 0){
			// 删除列表图
			if(!empty($data['list_image'])){
				unlink("./Public/Product/thumb/".$data['list_image']);
			}
			
			// 删除feacher图片
			if(!empty($data['feacher'])){
				preg_match_all("/(src)=([\"|']?)([^ \"'>]+\.('gif|jpg|jpeg|bmp|png'))\\2/i", $data['feacher'], $matches);
				foreach($matches[3] as $val){
					$val = str_replace("&quot;", ".", $val);
					@unlink($val);
				}
			}
			
			// 删除关联
			$products->deleteProductRelationByProductId($id);
			// 删除参数
			$products->deleteProductParamByProductId($id);
			// 删除属性
			$attribute = $products->getAttributeByProductId($id);
			foreach($attribute as $at){
				$items = $products->getAttrsByAttrId($at['id']);
				foreach($items as $vo){
					$ablums = $products->getAlbumByAttrId($vo['id']);
					foreach($ablums as $v){
						$products->deleteProductAlbum($v['id']);
						unlink("./Public/Product/images/source_img/".$v['images']);       //删除相册图片
						unlink("./Public/Product/images/goods_img/".$v['images']);        //删除相册图片
						unlink("./Public/Product/images/thumb_img/".$v['images']);        //删除相册图片
					}
					
					// 删除规格项
					$products->deleteProductAttr($vo['id']);
				}
							
				// 删除规格
				$products->deleteAttribute($at['id']);
			}
			
			$this->success('删除成功！');exit;
		}
		$this->error('删除失败');exit;
	}

	// 添加商品参数操作
	public function doParamAdd(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$id      = I('post.id');
		$title   = I('post.title');
		$content = I('post.content');
		
        $products = D('Products');

		$rt = $products->insertProductParam(array('pro_id'=>$id, 'param_title'=>$title, 'param_content'=>$content));
		if($rt['status'] > 0){
			echo $rt['status'];
			exit;
		}
		echo 0;exit;
	}

	// 编辑商品参数操作
	public function doParamEdit(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$pid     = I('post.pid');
		$title   = I('post.title');
		$content = I('post.content');
		
        $products = D('Products');

		$rt = $products->updateProductParam($pid, array('param_title'=>$title, 'param_content'=>$content));
		if($rt['status'] > 0){
			echo $rt['status'];
			exit;
		}
		echo 0;exit;
	}

	// 删除商品参数操作
	public function doParamDelete(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$id = I('post.id');
		
        $products = D('Products');

		$rt = $products->deleteProductParam($id);
		if($rt['status'] > 0){
			echo $rt['status'];
			exit;
		}
		echo 0;exit;
	}

	// 删除属性图片操作
	public function doAlbumDelete(){		
		$id = I('post.id');
		
        $products = D('Products');
		
		$album = $products->getAlbumByAlbumId($id);

		$rt = $products->deleteProductAlbum($id);
		if($rt['status'] > 0){
			unlink("./Public/Product/images/source_img/".$album['images']);       //删除相册图片
			unlink("./Public/Product/images/goods_img/".$album['images']);        //删除相册图片
			unlink("./Public/Product/images/thumb_img/".$album['images']);        //删除相册图片
			echo 1;exit;
		}
		echo 0;exit;
	}
	
	// 出库
	public function deliveryAdd(){
		$this->display('delivery_add');
	}
	
	// 加载商品
	public function loadSelectProducts(){
		$keyword = I('post.keyword');
		$shopId  = session('shop.id');
		
		$products = D('Products');
		
		$data = $products->getProductRelationByObject(array('shopId'=>$shopId, 'keyword'=>$keyword, 'm'=>0, 'n'=>25));
		foreach($data as &$vo){
			$vo['attr_name'] = $products->getAttrNameByAttrId($vo['pro_attr']);
		}
		unset($vo);
		echo json_encode($data);exit;
	}
	
	// 出库操作
	public function doDeliveryAdd(){
		$userName = I('post.username');
		$mobile   = I('post.mobile');
		$ids      = I('post.id');
		$prices   = I('post.price');
		$nums     = I('post.number');
		$shopId   = session('shop.id');

		$orders   = D('Orders');
		$users    = D('Users');
		$us       = D('UserShop');
		$products = D('Products');
		
		$shop = $us->getShopByShopId($shopId);  // 店铺信息
		
		if(!empty($mobile)){
			$user = $users->getUserByMobile($mobile);
			if(empty($user)){
				$password  = substr($mobile, -6);
				$rs = $users->regist($mobile, $password);
				if($rs['userId']>0){
					// 发送密码
					sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功

					// 加载用户信息				
					$userId = $rs['userId'];
				}
			}else{
				$userId = $user['id'];
			}
		}else{
			$userId = $shop['user_id'];
		}

		$money = 0;
		$goods = array();
		for($i=0; $i<count($ids); $i++){
			$relation = $products->getRelationByRelationId($ids[$i]);
			$goods[$i]['pro_id']     = $ids[$i];
			$goods[$i]['pro_price']  = $prices[$i];
			$goods[$i]['pro_number'] = $nums[$i];
			$goods[$i]['pro_attr']   = $relation['pro_attr'];
			$money += $prices[$i]*$nums[$i];
        }

		$data = array(
			'user_id'       => $userId,
			'agent_id'      => 0,
			'order_sn'      => CBWCreateOrderNumber(),
			'order_date'    => time(),
			'deail_price'   => $money,
			'order_status'  => 2,
			'pay_status'    => 1,
			'pay_type'      => 1,
			'area_id'       => $shop['area_id'],
			'address'       => $shop['address'],
			'reciver_user'  => empty($userName)?$shop['user_name']:$userName,
			'reciver_phone' => empty($mobile)?$shop['user_phone']:$mobile,
			'send_time'     => 0,
			'is_invoice'    => 0,
			'is_line'       => 1
		);
		$orderId = $orders->insertOrders($data);
		if($orderId > 0){
			// 添加订单附表
			$schedule[$shopId] = array('shop_id'=>$shopId, 'shop_money'=>$money, 'shop_status'=>3);
			$orders->insertOrderSchedule($schedule, $orderId);
			// 添加购物订单表
			$orders->insertOrderProducts($goods, $orderId);
			// 减库存
			for($i=0; $i<count($ids); $i++){
				$relation = $products->getRelationByRelationId($ids[$i]);
				$products->decrease($relation['id'], $nums[$i], $relation['pro_attr']);
			}
			
			$this->redirect('System/orders');
			exit;
		}
		$this->error('出库失败',U('System/deliveryAdd'));
		exit;
	}

    /**
     * 上传图片
     * 此方法被商品添加/修改页，上传缩略图所使用
     * @date 2015-01-15
     * @return void
     */
    public function upload(){
        $upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/Temp/";                         // 图片保存文件夹路径
        $upload->maxSize = 3145728;                           // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置附件上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = '';                               // 设置附件上传目录    // 上传文件
        $img    = $upload->upload();
		$imgUrl = $img['Filedata']['savepath'].$img['Filedata']['savename'];
		
		// 生成的缩略图
		$image = new \Think\Image();
		$image->open($rootPath.$imgUrl)->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save($rootPath.$imgUrl); //生产缩略图
		
        echo $imgUrl;
    }

    /**
     * 图片上传操作，此方法用于多文件上传插件，返回Ajax字符串
     * 此方法被商品属性添加页所调用
     * @date 2015-01-15
     * @return void
     */
    public function multiUpload(){
        $upload = new \Think\Upload();                        // 实例化上传类
		$rootPath = "./Public/Temp/product/";                 // 图片保存文件夹路径
        $upload->maxSize = 3145728;                           // 设置图片上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');   // 设置图片上传类型
        $upload->rootPath = $rootPath;
        $upload->savePath = 'source_img/';                    // 设置图片上传目录
        $img    = $upload->upload();
		$imgUrl = $img['Filedata']['savepath'].$img['Filedata']['savename'];
		$imgStr = substr($imgUrl,11);                         //除去source_img/
		
		// 生成的缩略图
		$image = new \Think\Image();
		list($width, $height) = getimagesize($rootPath.$imgUrl);  // 获取图片宽度和高度
		if($width != $height){
			if($width > $height){
				$image->open($rootPath.$imgUrl)->thumb($height, $height, \Think\Image::IMAGE_THUMB_CENTER)->save($rootPath.'source_img/'.$imgStr);
			}else{
				$image->open($rootPath.$imgUrl)->thumb($width, $width, \Think\Image::IMAGE_THUMB_CENTER)->save($rootPath.'source_img/'.$imgStr);
			}
		}
		$image->open($rootPath.$imgUrl)->thumb(500, 500, \Think\Image::IMAGE_THUMB_CENTER)->save($rootPath.'goods_img/'.$imgStr);
		$image->open($rootPath.$imgUrl)->thumb(150, 150, \Think\Image::IMAGE_THUMB_CENTER)->save($rootPath.'thumb_img/'.$imgStr);

        echo $imgStr;
    }

	// 我的订单
	public function orders(){
		$shopId  = session('shop.id');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');
		$line    = I('get.line');
		$line    = empty($line)?0:$line;
		
		$orders    = D('Orders');
		$products  = D('Products');
		$shippings = D('Shippings');
		$us        = D('UserShop');
	
		$shop = $us->getShopByShopId($shopId);
		
		if(isset($status) && $status !== ''){
			if($status == 0){
				$ostatus = 0;
			}elseif($status == 1){
				$ostatus = 1;
				$pay     = 0;
			}elseif($status == 2){
				$sstatus = 1;
				$pay     = 1;
			}elseif($status == 3){
				$sstatus = 2;
			}elseif($status == 4){
				$ostatus = 2;
			}
		}

		$count    = $orders->getOrdersCountByShopId(array("shopId"=>$shopId, "sstatus"=>$sstatus, 'keyword'=>$keyword, 'pay'=>$pay, 'start'=>$start, 'end'=>$end, 'ostatus'=>$ostatus, 'line'=>$line));
        $page     = new \Think\Page($count, 10);
		$pageShow = $page->show();
		$data     = $orders->getOrdersByShopId(array("shopId"=>$shopId, "sstatus"=>$sstatus, 'keyword'=>$keyword, 'pay'=>$pay, 'start'=>$start, 'end'=>$end, 'ostatus'=>$ostatus, 'line'=>$line, "m"=>$page->firstRow, "n"=>$page->listRows));
		foreach($data as &$vo){
			$vo['schedule'] = $orders->getOrderScheduleByOrderIdAndShopId($vo['id'], $shopId);
			$product  = $orders->getOrderProductsByOrderId($vo['id']);
			foreach($product as &$v){
				$v['attr_name'] = $products->getAttrNameByAttrId($v['pro_attr']);
				$v['pro_money'] = $v['pro_price']*$v['pro_number'];
			}
			unset($v);
			$vo['product'] = $product;
		}
		unset($vo);

		// 物流库
        $shipping = $shippings->getCommonShippingCompany();

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('shopId', $shopId);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->assign('status', $status);
		$this->assign('line', $line);
		$this->assign("shipping", $shipping);
		$this->display();
	}
	
	// 更改收货地址
	public function editAddress(){
		$orderId = I('get.id');
		
		$orders = D('Orders');
		$area   = D('Area');
		
		$order = $orders->getOrderById($orderId);
		
		// 三级联动
		$info = $area->getAreaById($order['area_id']);
		$p = empty($info['one_id'])?'选择省':$info['one_id'];
		$c = empty($info['two_id'])?'选择市':$info['two_id'];
		$a = empty($info['id'])?'选择县/区':$info['id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city",$city);
		$this->assign("order",$order);
		$this->display();
	}
	
	//修改收货地址操作
	public function doEditAddress(){
		$orderId = I('post.id');
		
		$orders = D('Orders');

		$data = array(
			'reciver_user'  => trim(I('post.reciver_user')),
			'reciver_phone' => trim(I('post.reciver_phone')),
			'area_id'       => I('post.area'),
			'address'       => I('post.address')
		);	
		$rt = $orders->updateOrders($orderId, $data);
		if($rt['status'] > 0){
			$this->success("更新成功", U("System/orders"));exit;
		}
		$this->error("更新失败");exit;
	}
	
	// 打印清单
	public function printlist(){
		$orderId = I('get.id');
		$shopId  = session('shop.id');
	
		$orders    = D('Orders');
		$products  = D('Products');
		$service   = D('Service');
		$shippings = D('Shippings');
		$us        = D('UserShop');
		
		$order    = $orders->getOrderById($orderId);
		$shop     = $us->getShopByShopId($shopId);
		$schedule = $orders->getOrderScheduleByOrderIdAndShopId($order['id'], $shopId);
		$product  = $orders->getOrderProductsByOrderId($order['id']);
		$money    = 0;
		foreach($product as &$vo){
			$vo['attr_name'] = $products->getAttrNameByAttrId($vo['pro_attr']);
			$vo['service']   = $service->getServiceByOrderId($order['id'], $vo['pro_id']);
			if($vo['shop_id'] == $shopId){
				$schedule['pro_money'] += $vo['pro_price']*$vo['pro_number'];
			}
		}
		unset($vo);
		
		$shipping = $shippings->getShippingCompanyById($schedule['shipping_way']);
		$schedule['cname']  = $shipping['cname'];
		
		// 生成公众号关注二维码
		$wechatObj = new \Think\WechatApi();
		$qrcode = $wechatObj->qrcodeCreate($shopId, 2);
		
		$this->assign("order", $order);
		$this->assign("shop", $shop);
		$this->assign("schedule", $schedule);
		$this->assign("product", $product);
		$this->assign("shopId", $shopId);
		$this->assign("qrcode",$qrcode);
		$this->display();
	}
	
	// 订单免运费
	public function exemptFee(){
		$orderId = I('get.id');
		$shopId  = session('shop.id');
		
		$orderSn = CBWCreateOrderNumber();
		
		$orders = D('Orders');
		
		$order    = $orders->getOrderById($orderId);
		$schedule = $orders->getOrderScheduleByOrderIdAndShopId($orderId, $shopId);
		$rt = $orders->updateOrderSchedule($orderId, $shopId, array('shop_money'=>$schedule['shop_money']-$schedule['shipping_fee'], 'shipping_fee'=>0));
		if($rt['status'] > 0){
			$rt = $orders->updateOrders($orderId, array('order_sn'=>$orderSn, 'deail_price'=>$order['deail_price']-$schedule['shipping_fee']));
			$this->success("操作成功", U("System/orders"));exit;
		}
		$this->error("操作失败");exit;
	}
	
	// 订单改价
	public function changePrice(){
		$orderId = I('post.id');
		$money   = I('post.money');
		$shopId  = session('shop.id');
		$orderSn = CBWCreateOrderNumber();

		$orders = D('Orders');

		$rt = $orders->updateOrderSchedule($orderId, $shopId, array('shop_money'=>$money, 'shipping_fee'=>0));
		if($rt['status'] > 0){
			$schedule = $orders->getOrderScheduleByOrderId($orderId);
			$deail = 0;
			foreach($schedule as $vo){
				$deail += $vo['shop_money'];
			}
			$rt = $orders->updateOrders($orderId, array('order_sn'=>$orderSn, 'deail_price'=>$deail));
			$this->success("操作成功", U("System/orders"));exit;
		}
		$this->error("操作失败");exit;
	}
	
	// 发货
	public function delivery(){
		$orderId = I('post.id');
		$way     = I('post.way');
		$num     = I('post.num');
		$shopId  = session('shop.id');
		
		$orders    = D('Orders');
		$shippings = D('Shippings');
		
		$order    = $orders->getOrderById($orderId);
		$schedule = $orders->getOrderScheduleByOrderIdAndShopId($orderId, $shopId);
		$shipping = $shippings->getShippingCompanyById($way);

		$rt = $orders->updateOrderSchedule($orderId, $shopId, array('send_date' => time(), 'shipping_way' => $way, 'shipping_num' => $num, 'shop_status' => 2));
		if($rt['status'] > 0){			
			// 快递100 订阅请求
			kd100($shipping['com'], $num);
			if($schedule['shop_status'] == 1){
				// 给客户发货通知短信
				sendSmsByAliyun($order['reciver_phone'], array(), 'SMS_116566045'); // 发货成功
			}
			echo $rt['status'];exit;
		}else{
			echo 0;exit;
		}
	}

	// 我的售后
	public function service(){
		$shopId  = session('shop.id');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');
		$type    = I('get.type');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$products  = D('Products');
		
		$count = $service->getServicesCountByObject(array('shopId'=>$shopId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status));
        $page = new \Think\Page($count, 8);
		$data = $service->getServicesByObject(array('shopId'=>$shopId, 'keyword'=>$keyword, 'start'=>$start, 'end'=>$end, 'status'=>$status, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$vo['order'] = $orders->getOrderById($vo['order_id']);
			$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			if(!empty($product['pro_attr'])){
				$product['attr_name'] = $products->getAttrNameByAttrId($product['pro_attr']);
			}
			$vo['product'] = $product;
			$vo['pro_money'] = $product['pro_price']*$vo['applynum'];
		}
		
		// 物流库
        $shipping = $shippings->getCommonShippingCompany();
		
		$this->assign('shipping', $shipping);
		$this->assign('service', $data);
		$this->assign('keyword', $keyword);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->assign('status', $status);
		$this->assign('type', $type);
		$this->display();
	}
	
	// 同意/反对申请
	public function audit(){
		$id     = I('get.id');
		$type   = I('get.type');
		$shopId = session('shop.id');
		
		$us      = D('UserShop');
		$orders  = D('Orders');
		$service = D('Service');
		
		$shop  = $us->getShopByShopId($shopId);
		$ser   = $service->getServiceByServinceId($id);
		$order = $orders->getOrderScheduleByOrderIdAndShopId($ser['order_id'], $shopId);
		if($type == 1){
			if($order['shop_status'] == 1){ //如果是等待发货
				$data['service_status'] = 5;
			}else{
				$data['service_status'] = 2;
			}
			$message = $shop['shop_name']."通过服务单申请";
		}else{
			$data['service_status'] = 0;
			$message = $shop['shop_name']."不通过服务单申请";
		}

		$rt = $service->updateService($id, $data);
		if($rt['status'] > 0){
			$da = array(
				'service_id' => $id,
				'time'       => time(),
				'operator'   => $shop['shop_name'],
				'message'    => $message
			);
			$service->addServiceDetails($da);
			$this->success('操作成功',U('System/service'));exit;
		}
		$this->error('操作失败');exit;
	}
	
	// 售后签收
	public function doSignService(){
		$id     = I('get.id');
		$shopId = session('shop.id');
		
		$us      = D('UserShop');
		$service = D('Service');
		
		$shop = $us->getShopByShopId($shopId);
		$ser  = $service->getServiceByServinceId($id);
		if($ser['service_type'] == 1){
			$status = 5;
		}else{
			$status = 4;
		}
		$rt = $service->updateService($id, array('service_status'=>$status));
		if($rt['status'] > 0){
			$da = array(
				'service_id' => $id,
				'time'       => time(),
				'operator'   => $shop['shop_name'],
				'message'    => $shop['shop_name']."签收服务单"
			);
			$service->addServiceDetails($da);
			$this->success('操作成功',U('System/service'));exit;
		}
		$this->error('操作失败');exit;
	}
	
	// 售后发货
	public function doDeliveryService(){
		$id     = I('post.id');
		$way    = I('post.way');
		$num    = I('post.num');
		$shopId = session('shop.id');
		
		$us        = D('UserShop');
		$service   = D('Service');
		$shippings = D('Shippings');
		
		$shop     = $us->getShopByShopId($shopId);
		$shipping = $shippings->getShippingCompanyById($way);

		$rt = $service->updateService($id, array('return_way' => $way, 'return_num' => $num, 'service_status'=>6));
		if($rt['status'] > 0){
			//快递100 订阅请求
			kd100($shipping['com'], $num);
			
			$da = array(
				'service_id' => $id,
				'time'       => time(),
				'operator'   => $shop['shop_name'],
				'message'    => $shop['shop_name']."服务单发货"
			);
			$service->addServiceDetails($da);
			
			echo $rt['status'];exit;
		}else{
			echo "0";exit;
		}
	}
	
	// 取消售后单
	public function doCloseService(){
		$id     = I('get.id');
		$shopId = session('shop.id');
		
		$us      = D('UserShop');
		$service = D('Service');
		
		$shop = $us->getShopByShopId($shopId);

		$rt = $service->updateService($id, array('service_status'=>0));
		if($rt['status'] > 0){
			$da = array(
				'service_id' => $id,
				'time'       => time(),
				'operator'   => $shop['shop_name'],
				'message'    => $shop['shop_name']."关闭服务单"
			);
			$service->addServiceDetails($da);
			$this->success('操作成功',U('System/service'));exit;
		}
		$this->error('操作失败');exit;
	}
	
	// 售后退款
	public function doRefundService(){
		$id     = I('post.id');
		$money  = I('post.money');
		$shopId = session('shop.id');
		
		$users   = D('Users');
		$us      = D('UserShop');
		$service = D('Service');
		$orders  = D('Orders');
		
		// 店铺用户信息
		$shop = $us->getShopByShopId($shopId);
		$user = $users->getUserAndInfoById($shop['user_id']);
		if(($user['money']-$user['frozen_money']) < $money){
			echo -1;exit;    // 账户余额不足
		}
		
		// 订单信息
		$order = $service->getServiceByServinceId($id);
		if($order['service_status'] == 6){
			echo -2;exit;    // 重复退款
		}
		
		$rt = $service->updateService($id, array('service_status'=>6));
		if($rt['status'] > 0){
			// 订单信息
			$orders->updateOrderProduct($order['order_id'], $order['pro_id'], array('status'=>0));
			$products = $orders->getOrderProductsByOrderId($order['order_id']);
			$i = 1;
			foreach($products as $vo){
				if($vo['status']){
					$i = 0;
				}
			}
			if($i == 1){
				$orders->closeOrders($order['order_id']);
			}
			
			$users->decreaseMoney($user['id'], $order['id'], $money, 1, '');     // 扣除用户账户金额
			$users->appendMoney($order['user_id'], $order['id'], $money, 1, ''); // 增加顾客账户金额
			
			$da = array(
				'service_id' => $id,
				'time'       => time(),
				'operator'   => $shop['shop_name'],
				'message'    => $shop['shop_name']."为服务单退款"
			);
			$service->addServiceDetails($da);
			
			// 退佣金
			$product = $orders->getOrderProductByOrderId($order['order_id'], $order['pro_id']);
			$commisionMoney = $product['pro_price']*$product['pro_number']*0.05;
			$commision = $orders->getOrderCommisionByOrderIdAndUserId($order['order_id'], $user['id']);
			if($commision['commision'] >= $commisionMoney){
				$orders->updateOrderCommision($commision['id'], array('commision'=>$commision['commision']-$commisionMoney)); // 更新佣金记录
			}
			$users->appendMoney($user['id'], $order['order_id'], $commisionMoney, 1, '佣金返还'); // 增加用户账户金额

			echo $rt['status'];exit;
		}else{
			echo "0";exit;
		}
	}

	// 物流设置
	public function shipping(){
		$shippings = D('Shippings');
		$data = $shippings->getShippings();
		
		$this->assign('shipping', $data);
		$this->display();
	}

	// 运费模板
	public function template(){
		$id     = I('get.id');
		$shopId = session('shop.id');
		
		$area      = D('Area');
		$shippings = D('Shippings');
		$data = $shippings->getShippingTemplateByShopId($shopId, $id);
		foreach($data as &$vo){
			$areas = explode(",",$vo['area']);
			$name = array();
			foreach($areas as $v){
				$province = $area->getProvinceById($v);
				$name[] = $province['addr'];
			}
			$vo['area'] = implode(",",$name);
		}
		
		$this->assign('id', $id);
		$this->assign('template', $data);
		$this->display();
	}

	// 添加运费模板
	public function templateAdd(){
		$id = I('get.id');
		
		$area = D('Area');
		$position = $area->getPositionProvince();

		$this->assign('id', $id);
		$this->assign('position', $position);
		$this->display('template_add');
	}

	// 添加运费模板操作
	public function doTemplateAdd(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$shopId = session('shop.id');
		$area   = I('post.area');
		$data   = array(
			'shop_id'     => $shopId,
			'shipping_id' => I('post.id'),
			'area'        => implode(',',$area),
			'fee'         => I('post.fee'),
			'fill'        => I('post.fill')
		);
		
		if(empty($area)){
			$this->error('物流区域必须勾选一个或多个');
		}
		
		$shippings = D('Shippings');
		
		$rt = $shippings->insertShippingTemplate($data);
        if($rt['status'] > 0) {
			$this->success('添加成功！',U('System/template/id/'.$data['shipping_id']));exit;
        }
		$this->error('添加失败');
	}

	// 编辑运费模板
	public function templateEdit(){
		$id = I('get.id');
		
		$area      = D('Area');
		$shippings = D('Shippings');
		
		$data     = $shippings->getTemplateByTemplateId($id);
		$sets     = explode(",", $data['area']);
		$position = $area->getPositionProvince();
		foreach($position as &$vo){
			foreach($vo['area'] as &$v){
				if(in_array($v['id'],$sets)){
					$v['checked'] = 1;
				}
			}
		}

		$this->assign('data', $data);
		$this->assign('position', $position);

		$this->display('template_edit');
	}

	// 编辑运费模板操作
	public function doTemplateEdit(){
		if (!IS_POST) {
			exit('页面错误~');
		}
		
		$id   = I('post.id');
		$sid  = I('post.sid');
		$area = I('post.area');
		$data = array(
			'area' => implode(',', $area),
			'fee'  => I('post.fee'),
			'fill' => I('post.fill')
		);
		
		if(empty($area)){
			$this->error('物流区域必须勾选一个或多个');
		}
		
		$shippings = D('Shippings');
		
		$rt = $shippings->updateShippingTemplate($id, $data);
        if($rt['status'] > 0) {
			$this->success('编辑成功！',U('System/template/id/'.$sid));exit;
        }
		$this->error('编辑失败');
	}

	// 删除运费模板
	public function doTemplateDelete(){
		$id = I('get.id');
		
        $shippings = D('Shippings');

		$rt = $shippings->deleteShippingTemplate($id);
		if($rt['status'] > 0){
			$this->success('删除成功！');exit;
		}
		$this->error('删除失败');exit;
	}
	
	// 我的评价
	public function comment(){
		$shopId = session('shop.id');
		
		$comments = D('Comments');
		
		// 获取查询订单的类型
		$start = I('get.startdate');
		$end   = I('get.enddate');

		$count = $comments->getCommentsCountByObject(array('shopId'=>$shopId, 'start'=>$start, 'end'=>$end));
        $page = new \Think\Page($count, 8);
		$data = $comments->getCommentsByObject(array('shopId'=>$shopId, 'start'=>$start, 'end'=>$end, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		
		foreach($data as &$vo){
			// 获取回复内容
			$replies = $comments->getRepliesByCommentId($vo['id']);
			$vo['replies'] = $replies['content'];
			$vo['reply_time'] = $replies['reply_time'];
		}
	
        $this->assign('page', $pageShow);
		$this->assign("comments",$data);
		$this->assign("startdate",$start);
		$this->assign("enddate",$end);
		
		$this->display();
	}
	
	// 回复提交
	public function reply(){
		$shopId = session('shop.id');
		
		$modelReplies = M('comment_replies');

		$data['comment_id']    = I('post.commentid');
		$data['admin_user_id'] = $shopId;
		$data['reply_time']    = time();
		$data['content']       = I('post.reply');
		if($modelReplies->create($data)){
			$rt = $modelReplies->add();
			if($rt){
				echo 1;exit;
			}
		}
		echo 0;exit;
	}
	
	// 下载海报
	public function poster(){
		$poster = D('Poster');
		$data = $poster->getPosterByObject();
		$this->assign("data", $data);
		$this->display();
	}
	
	// 生成海报
	public function createPoster(){
		$us = D('UserShop');
		$pr = D('Poster');
		
		$dir        = date("Ymd");
		$shopId     = session('shop.id');
		$posterId   = I('post.id');
		$shop       = $us->getShopByShopId($shopId);
		$poster     = $pr->getPosterById($posterId);
		$saveFile   = "./Public/Temp/".$dir."/poster_".$shop['id']."_".$posterId.".jpg"; // 海报文件
		$tempFile   = "./Public/Poster/".$poster['image'];                                    // 模板文件
		$qrcodeFile = "./Public/Temp/".$dir."/qrcode_".$shop['id'].".jpg";               // 二维码文件

		if(!file_exists($saveFile)){
			// 生成公众号关注二维码
			$wechatObj = new \Think\WechatApi();
			for($i=0; $i<100; $i++){
				$qrcode = $wechatObj->qrcodeCreate($shop['id'], 2);
				if(!empty($qrcode)){
					break;
				}
			}

			$image = new \Think\Image();  //图片处理类
			// 添加二维码
			$image->open($tempFile)->water($qrcodeFile, array(235,5063))->save($saveFile);
		}
		echo $dir."/poster_".$shop['id']."_".$posterId.".jpg";exit;
	}
	
	// 设置说明
	public function introduce(){
		$article = D('Article');
		$introduce = $article->getArticleList(12);
		$this->assign("introduce", $introduce);
		$this->display();
	}
	
	// 我的通知
	public function notice(){
		$shopId = session('shop.id');
		$status = I('get.status');
		
		$us = D('UserShop');
		
		$count = $us->getShopNoticeCountByObject(array('shopId'=>$shopId, 'status'=>$status));
        $page = new \Think\Page($count, 8);
		$pageShow = $page->show();
		$notices = $us->getShopNoticeByObject(array('shopId'=>$shopId, 'status'=>$status, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		
		$this->assign('status', $status);
		$this->assign('page', $pageShow);
		$this->assign("notices", $notices);
		$this->display();
	}
	
	// 通知 全部设置为已读
	public function doReadNotice(){
		$shopId = session('shop.id');
		
		$us = D('UserShop');
		
		$us->updateShopNotice("shop_id=$shopId", array('status'=>1));
		$this->redirect("System/notice"); 
	}
	
	// 通知详情
	public function noticedetail(){
		$noticeId = I('get.id');
		
		$us = D('UserShop');

		$data = $us->getShopNoticeByNoticeId($noticeId);
		if($data['status'] == 0){
			$us->updateShopNotice("id=$noticeId", array('status'=>1));
		}
		$this->assign("notice", $data);
		$this->display();
	}
	
}