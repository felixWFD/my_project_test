<?php
namespace Home\Controller;
/**
 * 售后控制器
 */
class ServiceController extends BaseController{
	
    /**
	 * 售后列表
	 * 
	 */
    public function index(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$shippings = D('Shippings');
		$orders    = D('Orders');
		$service   = D('Service');
		$repairs   = D('Repairs');
		$products  = D('Products');
		
		$shipping = $shippings->getCommonShippingCompany();  // 物流库
		
		$count = $service->getServicesCountByObject(array('userId'=>$userId));
        $page = new \Think\Page($count, 8);
		$serviceRes = $service->getServicesByObject(array('userId'=>$userId, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($serviceRes as $key=>$vo){
			if($vo['pro_id'] == 0){
				$order    = $orders->getOrderById($vo['order_id']);
				$server   = $orders->getOrderServerByOrderId($vo['order_id']);
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$list['show_id']    = 0;
				$list['show_name']  = $relation['pro_name']." ".$relation['plan_name'];
				$list['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
				$list['show_desc']  = '';
				$list['show_price'] = $server['new_price']>0?$server['new_price']:$order['deail_price'];
				$list['show_num']   = 1;
			}else{
				$product = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
				$list['show_id']    = $product['pro_id'];
				$list['show_name']  = $product['pro_name'];
				$list['show_img']   = '/Public/Product/thumb/'.$product['list_image'];
				$list['show_desc']  = $products->getAttrNameByAttrId($product['pro_attr']);
				$list['show_price'] = $product['pro_price'];
				$list['show_num']   = $vo['applynum'];
			}
			$serviceRes[$key]['list'] = $list;
		}
		$this->assign("serviceRes", $serviceRes);
		$this->assign("page", $pageShow);
		$this->assign("shipping",$shipping);
		$this->assign("title","我的售后");
		$this->display();
    }
	
	/**
	 * 申请
	 * 
	 */
	public function apply(){
		$this->isUserLogin();
		
		$userId    = session('user.userid');
		$orderId   = I('get.oid');
		$productId = I('get.pid');
		
		$orders   = D('Orders');
		$products = D('Products');
		$cards    = D('Cards');

		$order   = $orders->getOrderById($orderId);
		$product = $orders->getOrderProductByOrderId($orderId, $productId);
		if($product['is_card'] == '1'){
			$cnumber = $cards->getCardsCountByObject(array('order_id'=>$orderId));
			if($cnumber > 0){
				$this->error("非法操作,已提取卡密");
			}
		}
		$product['show_desc'] = $products->getAttrNameByAttrId($product['pro_attr']);

		$order_date = empty($order['ok_date'])?$order['order_date']:$order['ok_date'];
		$dvalue = ($order_date+(30*24*3600))-time();
		if($dvalue < 0 ){
			$this->error("已过退换货期限");
		}
		$this->assign("product",$product);
		$this->assign("order",$order);
		$this->assign("title","售后申请");
		$this->display();
	}
	
	/**
	 * 添加申请
	 * 
	 */
    public function submitApply(){
		$this->isUserLogin();
		
		$userId    = session('user.userid');
		$userName  = session('user.username');
		$orderId   = I('post.order_id');
		$productId = I('post.pro_id');
		$num       = I('post.num');
		
		$orders   = D('Orders');
		$service  = D('Service');
		$check = $service->checkServiceByOrderId($orderId, $productId);
		if($check){
			$this->error('该商品已存在服务单');
		}
		$order   = $orders->getOrderById($orderId);
		$product = $orders->getOrderProductByOrderId($orderId, $productId);
		if($order['user_id'] == $userId && $order['order_status'] > 0){
			if($num > $product['pro_number']){
				$this->error('输入的申请数量大于订单中的数量');
			}
			$data['service_sn']     = CBWCreateOrderNumber();
			$data['user_id']        = $userId;
			$data['shop_id']        = $order['shop_id'];
			$data['order_id']       = $orderId;
			$data['pro_id']         = $productId;
			$data['service_type']   = I('post.type');
			$data['reason']         = I('post.reason');
			$data['applynum']       = $num;
			$data['description']    = I('post.description');
			$data['apply_time']     = time();
			$data['service_status'] = 1;
			$rt = $service->addService($data);
			if($rt['status'] > 0){
				$details['service_id'] = $rt['status'];
				$details['time']       = time();
				$details['operator']   = $userName;
				$details['message']    = $userName."申请服务单";
				$service->addServiceDetails($details);
				$this->success('申请成功.', U('Service/index'));
			}else{
				$this->error('申请失败');
			}
		}else{
			$this->error('申请失败(非法账户)');
		}
    }
	
	/**
	 * 申请退款(远程维修订单)
	 */
	public function longrange(){
		$this->isUserLogin();
		
		$userId    = session('user.userid');
		$userName  = session('user.username');
		$orderId   = I('get.id');
		
		$orders   = D('Orders');
		$service  = D('Service');
		
		$check = $service->checkServiceByOrderId($orderId);
		if($check){
			$this->error('已存在服务单');
		}
		$order = $orders->getOrderById($orderId);
		if($order['user_id'] == $userId){
			$data = array(
				'service_sn'     => CBWCreateOrderNumber(),
				'user_id'        => $userId,
				'order_id'       => $orderId,
				'pro_id'         => 0,
				'service_type'   => 1,
				'reason'         => 9,
				'applynum'       => 1,
				'description'    => '远程维修订单',
				'apply_time'     => time(),
				'service_status' => 5
			);
			$rt = $service->addService($data);
			if($rt['status'] > 0){
				$details['service_id'] = $rt['status'];
				$details['time']       = time();
				$details['operator']   = $userName;
				$details['message']    = $userName."申请服务单";
				$service->addServiceDetails($details);
				$this->success('申请成功.', U('Service/index'));
			}else{
				$this->error('申请失败');
			}
		}else{
			$this->error('申请失败(非法账户)');
		}
	}
	
	/**
	 * 关闭售后单
	 */
	public function close(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$servinceId = I('get.id');
		
		$service = D('Service');
		$order = $service->getServiceByServinceId($servinceId);
		if($order['user_id'] == $userId){
			$rt = $service->updateService($servinceId, array('service_status'=>0));
			if($rt['status'] > 0){
				$this->redirect('Service/index');
			}else{
				$this->error('关闭失败');
			}
		}else{
			$this->error('关闭失败(非法账户)');
		}
	}
	
	/**
	 * 发货
	 */
	public function send(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$serviceSn = I('post.sn');
		$way       = I('post.shipping_way');
		$number    = trim(I('post.shipping_num'));
		
		$service = D('Service');
		$data = $service->getServiceBySn($serviceSn);
		if($data['user_id'] == $userId){
			$rt = $service->updateService($data['id'], array('shipping_way'=>$way, 'shipping_num'=>$number, 'service_status'=>3));
			if($rt['status'] > 0){
				//快递100 订阅请求
				$shippings = D('Shippings');
				$shipping = $shippings->getShippingCompanyById($way);
				kd100($shipping['com'], $number);
				
				$this->redirect('Service/index');
			}else{
				$this->error('发货失败');
			}
		}else{
			$this->error('发货失败(非法账户)');
		}
	}
	
	/**
	 * 售后单详细
	 * 
	 */
	public function detail(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$serviceSn = I('get.sn');
		
		$orders    = D('Orders');
		$shippings = D('Shippings');
		$service   = D('Service');
		$products  = D('Products');
		
		$sale = $service->getServiceAllBySn($serviceSn);
		
		// 寄出物流公司信息
		if($sale['shipping_way']){
			$shipping = $shippings->getShippingCompanyById($sale['shipping_way']);
			$sale['shipping_name'] = $shipping['cname'];
			$data = array();
			if(!empty($sale['shipping_num'])){
				$expresss = $shippings->getExpressByNumber($sale['shipping_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$sale['express'] = $data;
		}
		
		
		// 寄回物流公司信息
		if($sale['return_way']){
			$shipping = $shippings->getShippingCompanyById($sale['return_way']);
			$sale['return_name'] = $shipping['cname'];
			$data = array();
			if(!empty($sale['return_num'])){
				$expresss = $shippings->getExpressByNumber($sale['return_num']);
				$content = CBWOjectArray(json_decode($expresss['content']));
				foreach($content as $key=>$vo){
					$data[$key]['time']    = $vo['time']; 
					$data[$key]['context'] = $vo['context'];
				}
			}
			$sale['return'] = $data;
		}
		
		$sale['details'] = $service->getDetailsByServiceId($sale['id']);
		
		if($sale['pro_id'] > 0){
			$product = $orders->getOrderProductByOrderId($sale['order_id'], $sale['pro_id']);
			$product['attr'] = $products->getAttrNameByAttrId($product['pro_attr']);
		}
		
		$this->assign("sale",$sale);
		$this->assign("product",$product);
		$this->assign("title","售后详情");
		$this->display();
	}

}