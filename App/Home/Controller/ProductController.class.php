<?php
namespace Home\Controller;

class ProductController extends BaseController{
    
	public function __construct(){   
		parent::__construct();
    }
	
    // 首页
    public function index(){
		$cat      = D('ProductCat');
		$products = D('Products');
		$ads      = D('Advs');
		
		// 搜索条件
		$groupId = session('user.groupid');
		$vip     = session('user.vip');

		// Banner
		$banners = $ads->getAdvsByCat(8, 1);
		
		// category
		$category = $cat->getAllCategory(1);
		
		// 分类窗口广告图
		$cateadvs = $ads->getAdvsByCat(13, 1);
		
		// 新品推荐
		$news = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'new'=>'is_new', 'groupby'=>1, 'sort'=>'new', 'm'=>0, 'n'=>6));
		$hots = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'hot'=>'is_hot', 'groupby'=>1, 'sort'=>'new', 'm'=>0, 'n'=>6));		
		
		// 横条广告
		$baradv = $ads->getAdvsById(39);

		// 内配
		$insideadv1  = $ads->getAdvsById(31);
		$insideadv2  = $ads->getAdvsById(38);
		$insidecat   = $cat->getCatListById(2);
		$insides1    = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>5, 'groupby'=>1, 'sort'=>'position', 'position'=>'position'));
		$insides2    = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>5, 'groupby'=>1, 'm'=>0, 'n'=>9 - count($insides1)));
		$insides     = array_merge($insides1, $insides2);
		
		// 外配
		$outsideadv1 = $ads->getAdvsById(29);
		$outsideadv2 = $ads->getAdvsById(30);
		$outsidecat  = $cat->getCatListById(6);
		$outsides1   = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>6, 'groupby'=>1, 'sort'=>'position', 'position'=>'position'));
		$outsides2   = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>6, 'groupby'=>1, 'm'=>0, 'n'=>9 - count($outsides1)));
		$outsides    = array_merge($outsides1, $outsides2);

		// 工具
		$tooladv     = $ads->getAdvsById(32);
		$toolcat     = $cat->getCatListById(9);
		$tools1      = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>9, 'groupby'=>1, 'sort'=>'position', 'position'=>'position'));
		$tools2      = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'classId'=>9, 'groupby'=>1, 'm'=>0, 'n'=>9 - count($tools1)));
		$tools       = array_merge($tools1, $tools2);

		$this->assign("banners",$banners);
		$this->assign("category",$category);
		$this->assign("cateadvs",$cateadvs);
		$this->assign('news', $news);
		$this->assign('hots', $hots);
		$this->assign('baradv', $baradv);
		
		$this->assign('insideadv1', $insideadv1);
		$this->assign('insideadv2', $insideadv2);
		$this->assign('insidecat', $insidecat);
		$this->assign('insides', $insides);
		
		$this->assign('outsideadv1', $outsideadv1);
		$this->assign('outsideadv2', $outsideadv2);
		$this->assign('outsidecat', $outsidecat);
		$this->assign('outsides', $outsides);
		
		$this->assign('tooladv', $tooladv);
		$this->assign('toolcat', $toolcat);
		$this->assign('tools', $tools);

		//SEO
        $ident ="product";
        $idents =$this->seo($ident);
        $this->assign("title",empty($title)?$idents['title']:$title);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);

        $this -> display();
    }
	
    // 列表页
    public function lists(){
        $cat      = D('ProductCat');
		$products = D('Products');
		
		// 搜索条件
		$groupId  = session('user.groupid');
		$vip      = session('user.vip');
		$classId  = I('get.class');
		$typeId   = I('get.type');
		$brandId  = I('get.brand');
		$modelId  = I('get.model');
		$sort     = I('get.sort');
		$sort     = empty($sort)?'default':$sort;
		$min	  = I('get.min');
		$max	  = I('get.max');
		$keyword  = I('get.keyword');

		$outsides = $cat->getAllCategory(6);    // 外配
		$mobiles  = $cat->getAllCategory(2);    // 手机配件
		$ipads    = $cat->getAllCategory(41);   // 平板配件
		$tools    = $cat->getAllCategory(9);    // 工具
		
		if(!empty($classId)){
			$class = $cat->getCat($classId);            // 选中分类信息
			$types = $cat->getCatListById($classId);    // 分类下的类型
			$parameter = $classId;
		}
		if(!empty($typeId)){
			$type = $cat->getCat($typeId);              // 选中类型信息
			$brands = $cat->getCatListById($typeId);    // 类型下的品牌
			$parameter .= '_'.$typeId;
		}
		if(!empty($brandId)){
			$brand = $cat->getCat($brandId);            // 选中品牌信息
			$models = $cat->getCatListById($brandId);   // 品牌下的型号
			$parameter .= '_'.$brandId;
		}
		if(!empty($modelId)){
			$model = $cat->getCat($modelId);            // 选中型号信息
			$parameter .= '_'.$modelId;
		}
		if(empty($classId)){
			$data = $products->getProductsByObject(array('groupId'=>$groupId, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$modelId, 'sale'=>1, 'min'=>$min, 'max'=>$max, 'sort'=>$sort, 'keyword'=>$keyword, 'm'=>0, 'n'=>40));
		}else{
			$length = $products->getProductsCountByObject(array('groupId'=>$groupId, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$modelId, 'sale'=>1, 'min'=>$min, 'max'=>$max, 'keyword'=>$keyword));
			$page   = new \Think\MyPage($length, 20);  // 分页处理
			$page->url = 'product/'.$parameter.'/';
			$other = $this->other($_GET);
			$page->other = empty($other)?'':'?'.$other;
			$data = $products->getProductsByObject(array('groupId'=>$groupId, 'classId'=>$classId, 'typeId'=>$typeId, 'brandId'=>$brandId, 'catId'=>$modelId, 'sale'=>1, 'min'=>$min, 'max'=>$max, 'sort'=>$sort, 'keyword'=>$keyword, 'm'=>$page->firstRow, 'n'=>$page->listRows));
			$pageShow = $page->show();
		}
		foreach($data as &$vo){
			$relation = $products->getRelationByProductId($vo['id']);
			$attrs    = $products->getAttrsByProductId($vo['id']);
			$new      = array();
			$i        = 0;
			foreach($attrs as $k=>$v){
				$images = $products->getAlbumByAttrId($v['id']);
				if(!empty($images)){
					foreach($relation as $rel){
						if(in_array($v['id'], explode("|", $rel['pro_attr']))){
							$new[$k]['id']     = $rel['id'];
							$new[$k]['images'] = $images[0]['images'];
							$product = $products->getProductPriceByRelationId($rel['id'], 1, $groupId, $vip);
							$new[$k]['price']  = $product['price'];
							if($i == 0){
								$vo['rid']    = $rel['id'];
								$vo['images'] = $images[0]['images'];
								$vo['price']  = $product['price'];
							}
						}
					}
					$i++;
				}
			}
			$vo['attrs']  = $new;
		}
		unset($vo);

		$this->assign('class',  $classId);
		$this->assign('type',   $typeId);
        $this->assign('brand',  $brandId);
		$this->assign('model',  $modelId);
		$this->assign('sort',  $sort);
		$this->assign('min',  $min);
		$this->assign('max',  $max);
		$this->assign('keyword',  $keyword);
		$this->assign('models', $models);
		$this->assign('products',  $data);
		$this->assign('page',   $pageShow);
		$this->assign('cname',   $class['cat_name']);
		$this->assign('tname',   $type['cat_name']);
		$this->assign('bname',   $brand['cat_name']);
		$this->assign('mname',   $model['cat_name']);
		$this->assign('outsides',  $outsides);
		$this->assign('mobiles',  $mobiles);
		$this->assign('ipads',  $ipads);
		$this->assign('tools',  $tools);

		//SEO
		$title       = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
		$keywords    = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, '.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发商城';
		$description = '草包网商城专业提供'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'系列原厂品质配件。无论大小配件均由维修工程师测试合格方可寄出，请广大用户放心购买。';
		switch($classId){
			case 5:
				$title    = (empty($model['cat_name'])?$brand['cat_name']:$brand['cat_name'].$model['cat_name']).$type['cat_name'].'配件批发网';
				$keywords = (empty($model['cat_name'])?$brand['cat_name']:$brand['cat_name'].$model['cat_name']).$type['cat_name'].'配件网, '.(empty($model['cat_name'])?$brand['cat_name']:$model['cat_name']).$type['cat_name'].'配件批发平台';
				$strings  = empty($type['cat_name'])?$class['cat_name']:$type['cat_name'];
				if(!empty($brands)){
					$names = array();
					foreach($brands as $key=>$vo){
						$names[$key] = $vo['cat_name'];
					}
					$strings = implode('、', $names);
				}
				if(!empty($models)){
					$names = array();
					foreach($models as $key=>$vo){
						$names[$key] = $vo['cat_name'];
					}
					$strings = implode('、', $names);
				}
				$description = '草包网商城专业提供'.$strings.'系列原厂品质配件。无论大小配件均由维修工程师测试合格方可寄出，请广大用户放心购买。';
				break;  
			case 6:
				$title    = '手机'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
				$keywords = '手机'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, 手机'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发平台';
				$strings  = empty($type['cat_name'])?$class['cat_name']:$type['cat_name'];
				if(!empty($types)){
					$names = array();
					foreach($types as $key=>$vo){
						$names[$key] = $vo['cat_name'];
					}
					$strings = implode('、', $names);
				}
				$description = '草包网商城专业提供'.$strings.'系列原厂品质配件。无论大小配件均由维修工程师测试合格方可寄出，请广大用户放心购买。';
				break;  
			case 8:
				$title    = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
				$keywords = (empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, '.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发平台';
				if(!empty($brand)){
					$description = '草包网您提供'.$brand['cat_name'].'手机导购，'.$brand['cat_name'].'手机推荐，'.$brand['cat_name'].'手机评价，'.$brand['cat_name'].'手机图片，'.$brand['cat_name'].'手机好不好，怎么样等产品信息，了解更多'.$brand['cat_name'].'手机导购信息，就上草包网，正品行货，全国联保！';
				}else{
					$description = '草包网商城（www.caobao.com)提供正品行货手机。新品手机、手机推荐、手机最新报价、促销、测评、图片、价格行情、评价咨询等购物信息，更多更全手机品牌尽在草包网！';
				}
				break;  
			case 9:
				$title       = '手机数码维修'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网';
				$keywords    = '手机数码维修'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发网, 手机数码维修'.(empty($type['cat_name'])?$class['cat_name']:$type['cat_name']).'批发平台';
				$description = '草包网商城专业提供手机数码维修工具，拆机工具，各类螺丝刀.品质可靠，物美价廉，所售产品均属正品，受广大客户一致好评。';
				break;
		}
        $this->assign("title", $title);
        $this->assign("keywords",$keywords);
        $this->assign("description",$description);

        $this -> display();
    }
	
	private function other($data){
		$other = array('sort','min','max','keyword');
		$param = array();
		foreach($data as $key=>$val){
			if(in_array($key, $other)){
				if(!empty($val)){
					$param[] = $key."=".$val;
				}
			}			
		}
		return implode("&", $param);
	}
	
	public function detail(){
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$id      = I('get.id');
		$shareId = I('get.u');
		
		// 分享链接带用户参数,保存分享用户编号
		if(!empty($shareId)){
			session('shareid', $shareId);
		}

		$cat      = D('ProductCat');
		$products = D('Products');
		$comments = D('Comments');
		$orders   = D('Orders');
		$users    = D('Users');
		$us       = D('UserShop');
		$video    = D('Video');
		
		// 商品信息
		$data = $products->getRelationByRelationId($id);
		!isset($data) && $this->redirect('Empty/index');
		$data['price'] = $data['one_price'];
		if($groupId == 1){
			if($data['two_price'] > 0){
				$data['price'] = $data['two_price'];
			}
			if($num >= 5 && $data['four_price'] > 0){
				$data['price'] = $data['four_price'];
			}
		}
		if($vip == 1){
			if($data['three_price'] > 0){
				$data['price'] = $data['three_price'];
			}elseif($data['two_price'] > 0){
				$data['price'] = $data['two_price'];
			}
		}
		
		$attrs = explode("|", $data['pro_attr']);
		$attribute = $products->getAttributeByProductId($data['id']);
		$attr = $products->getProductAttrByAttributeId($attribute[0]['id']);
		$attrId = $attr[0]['id'];
		foreach($attr as $vo){
			if(in_array($vo['id'], $attrs)){
				$attrId = $vo['id'];
			}
		}
		if($attrId > 0){
			$data['album'] = $products->getAlbumByAttrId($attrId);
		}
		$data['attrs'] = $products->getProductAttrByProductId($data['id']);
		
		//商品分类
		if(!empty($data['type_id'])){
			$catRes = $cat->getCat($data['type_id']);
			$data['type_name'] = $catRes['cat_name'];
		}
		if(!empty($data['brand_id'])){
			$catRes = $cat->getCat($data['brand_id']);
			$data['brand_name'] = $catRes['cat_name'];
		}
		if(!empty($data['cat_id'])){
			$catRes = $cat->getCat($data['cat_id']);
			$data['cat_name'] = $catRes['cat_name'];
		}
		
		// 店铺信息
		if($data['shop_id'] > 0){
			$shop = $us->getShopByShopId($data['shop_id']);
		}
		
		//增加点击数
		$products->statistics($data['rid'], 'click'); 
		
		//相关视频
		$videoRes = $video->getVideoByProductId($data['id']);
		
		//评价总数
		$commentLength = $comments->getCommentCountByProductId($data['id']);
		
		//商品评价
		$commentsRes = $comments->getCommentByProductId($data['id'], 0, 8);
		foreach($commentsRes as &$vo){
			$replies = $comments->getRepliesByCommentId($vo['id']);
			$op      = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$number = 1;
			$attr   = '默认';
			if(!empty($op['pro_attr'])){
				$number = $op['pro_number'];
				$attr   = $products->getAttrNameByAttrId($op['pro_attr']);
			}
			$vo['number']  = $number;
			$vo['attr']    = $attr;
			$vo['replies'] = $replies['content'];
			
			if($vo['user_id'] > 0){
				$user = $users->getUserAndInfoById($vo['user_id']);
				$vo['city']  = $user['city_addr'];
				$vo['uname'] = $user['uname'];
				$vo['thumb'] = $user['thumb'];
			}
		}
		unset($vo);
		
		//商品参数
		$paramRes = $products->getProductParamById($data['id']);
		
		//关联工具
		$links = $products->getProductLinkByProductId($data['id'], $groupId, $vip, 3);
	
		//还购买了
		$buyRes = $products->getProductRelationByObject(array('groupId'=>$groupId, 'vip'=>$vip, 'typeId'=>$data['type_id'], 'groupby'=>1, 'sort'=>'new', 'm'=>0, 'n'=>4));
		
		//SEO
		$this->assign("title",$data['pro_name']."价格_图片_草包商城");
		$this->assign("keywords",$data['keywords']);
		$this->assign("description",$data['description']);
		
		$this->assign('data',$data);
		$this->assign('shop',$shop);
		$this->assign('attrs',$attrs);
		$this->assign("video",$videoRes);
		$this->assign("commentLength",$commentLength);
		$this->assign("pageCount",ceil($commentLength/8));
		$this->assign('commentsRes',$commentsRes);
		$this->assign('paramRes',$paramRes);
		$this->assign("links",$links);
		$this->assign('buyRes',$buyRes);
		$this->display();
	}
	
	//获取商品关联编号
	public function getId(){
		$proId = I('post.pro_id');
		$attrs = I('post.attrs');
		
		$products = D('Products');
		
		$relation = $products->getRelationByProductIdAndAttrId($proId, $attrs);
		if($relation['id'] > 0){
			echo $relation['id'];exit; 
		}
		echo 0;exit;
	}
	
	//获取更多评论
	public function loadComments(){
		$id   = I('post.id');
		$page = I('post.page');
		$page = empty($page)?1:$page;
		
		$comments = D('Comments');
		$orders   = D('Orders');
		$users    = D('Users');
		$products = D('Products');
		
		$commentsRes = $comments->getCommentByProductId($id, ($page-1)*8,8);
		foreach($commentsRes as &$vo){
			$replies = $comments->getRepliesByCommentId($vo['id']);
			$op      = $orders->getOrderProductByOrderId($vo['order_id'], $vo['pro_id']);
			$number = 1;
			$attr   = '默认';
			if(!empty($op['pro_attr'])){
				$number = $op['pro_number'];
				$attr   = $products->getAttrNameByAttrId($op['pro_attr']);
			}
			$vo['number']       = $number;
			$vo['attr']         = $attr;
			$vo['replies']      = $replies['content'];
			$vo['comment_time'] = date('m.d', $vo['comment_time']);
			
			if($vo['user_id'] > 0){
				$user = $users->getUserAndInfoById($vo['user_id']);
				$vo['city']  = $user['city_addr'];
				$vo['uname'] = $user['uname'];
				$vo['thumb'] = $user['thumb'];
			}
		}
		unset($vo);
		echo json_encode($commentsRes);exit;
	}

	//购物车添加
	public function addCart(){
		$id      = I('post.id');
		$num     = intval(I('post.num'));
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		
		$products = D('Products');
		$us       = D('UserShop');

		$relation = $products->getRelationByRelationId($id);
		$product  = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		$shopId   = $relation['shop_id'];
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			$_SESSION['gws'][$shopId]['shop'] = $shop;
		}
		
		//判断session中是否存在相同的数据,存在则购物车数量不相加
		if(!array_key_exists($id, $_SESSION['gws'][$shopId]['cart'])){
			$_SESSION['gws_count']++;
		}
		
		$_SESSION['gws'][$shopId]['cart'][$id] = array(
			'id'          => $id,
			'num'         => $product['num'],
			'pro_price'   => $product['price'],
			'two_price'   => $relation['two_price'],
			'three_price' => $relation['three_price'],
			'pro_amount'  => number_format($product['price']*$product['num'], 2, '.', ''),
			'attr_id'     => $relation['pro_attr'],
			'attr_name'   => $products->getAttrNameByAttrId($relation['pro_attr']),
			'list_image'  => $relation['list_image'],
			'pro_name'    => $relation['pro_name'],
			'is_card'     => $relation['is_card'],
			'virtual'     => $relation['virtual_money'],
			'is_shipping' => $relation['is_shipping']
		);
		
		echo $_SESSION['gws_count'];
		exit;
	}

	//立即购买 购物车添加
	public function buyCart(){
		$id      = I('post.id');
		$num     = intval(I('post.num'));
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		
		$products = D('Products');
		$us       = D('UserShop');

		$relation = $products->getRelationByRelationId($id);
		$product  = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		$shopId   = $relation['shop_id'];
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			$_SESSION['gws'][$shopId]['shop'] = $shop;
		}
		
		//判断session中是否存在相同的数据,存在则购物车数量不相加
		if(!array_key_exists($id, $_SESSION['gws'][$shopId]['cart'])){
			$_SESSION['gws_count']++;
		}

		$_SESSION['gws'][$shopId]['cart'][$id] = array(
			'id'          => $id,
			'num'         => $product['num'],
			'pro_price'   => $product['price'],
			'two_price'   => $relation['two_price'],
			'three_price' => $relation['three_price'],
			'pro_amount'  => number_format($product['price']*$product['num'], 2, '.', ''),
			'attr_id'     => $relation['pro_attr'],
			'attr_name'   => $products->getAttrNameByAttrId($relation['pro_attr']),
			'list_image'  => $relation['list_image'],
			'pro_name'    => $relation['pro_name'],
			'is_card'     => $relation['is_card'],
			'virtual'     => $relation['virtual_money'],
			'is_shipping' => $relation['is_shipping']
		);
		
		echo $id;
		exit;
	}

	//需要工具 购物车添加
	public function needCart(){
		$id      = I('post.id');
		$num     = 1;
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		
		$products = D('Products');
		$us       = D('UserShop');
		
		//组织商品属性
		$relation = $products->getRelationByRelationId($id);
		$product  = $products->getProductPriceByRelationId($id, $num, $groupId, $vip);
		$shopId   = $relation['shop_id'];
		if($shopId > 0){
			$shop = $us->getShopByShopId($shopId);
			$_SESSION['gws'][$shopId]['shop'] = $shop;
		}
		$attrs = $products->getProductAttrByProductId($relation['id']);
		foreach($attrs as $vo){
			$list = $products->getAttrsByProductIdAndAttrId($relation['id'], $vo['attr_id']);
			foreach($list as $v){
				$attr[] = $v['id'];
			}
		}
		$proAttr = implode("|", $attr);
		
		//判断session中是否存在相同的数据,存在则购物车数量不相加
		if(!array_key_exists($id, $_SESSION['gws'][$shopId]['cart'])){
			$_SESSION['gws_count']++;
		}
		
		$_SESSION['gws'][$shopId]['cart'][$id] = array(
			'id'          => $id,
			'num'         => $product['num'],
			'pro_price'   => $product['price'],
			'two_price'   => $relation['two_price'],
			'three_price' => $relation['three_price'],
			'pro_amount'  => number_format($product['price']*$product['num'], 2, '.', ''),
			'attr_id'     => $proAttr,
			'attr_name'   => $products->getAttrNameByAttrId($proAttr),
			'list_image'  => $relation['list_image'],
			'pro_name'    => $relation['pro_name'],
			'is_card'     => $relation['is_card'],
			'virtual'     => $relation['virtual_money'],
			'is_shipping' => $relation['is_shipping']
		);

		echo $_SESSION['gws_count'];
		exit;
	}

}
