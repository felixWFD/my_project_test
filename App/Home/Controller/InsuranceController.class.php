<?php
namespace   Home\Controller;

class InsuranceController extends BaseController {
    
	public function __construct(){
		parent::__construct();
    }
	
	// 保障卡维修订单
	public function security(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		empty($userId) || ($userId != 1229) && $this->error('权限不足');
		
		
		$cat = D('ProductCat');
		
		$brands = $cat->getCatListById(10);
		
		//加载城市信息
        $city = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$c = \Think\Area::city($city);
		
		$this->assign("city", $c);
		$this->assign("brands", $brands);
		
		// SEO
        $ident ="repair";
        $idents =$this->seo($ident);
        $this->assign("title",empty($title)?$idents['title']:$title);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);
		
		$this->display();
	}

    /**
     * 执行维修商品类别change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeRepairSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getRepairByBrandId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择型号</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['pro_name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行维修商品颜色change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeColorSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getColorByRepairId($id);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择颜色</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['id'].'">'.$vo['name'].'</option>';
		}
        echo $retString;
		exit;
    }

    /**
     * 执行维修故障change操作
     * 但Ajax返回HTML实体没有解决
     * @return void
     */
    public function changeFaultSelect(){
        $id = I('get.id');
		
        $repairs = D('Repairs');

        $data = $repairs->getFaultByModelIdAndCatId($id, 1);
		if(!is_array($data)){
			exit;
		}
		$retString = '<option value="0">选择故障</option>';
		foreach($data as $key=>$vo){
			$retString .= '<option value="'.$vo['fault_id'].'">'.$vo['fault_name'].'</option>';
		}
        echo $retString;
		exit;
    }
	
	// 获取维修店列表
	public function loadShops(){
		$lng = I('post.lng');
		$lat = I('post.lat');
		
		$us = D('UserShop');
		
		$shops = $us->getShopsByLngLat($lng, $lat, 0, 8);
		echo json_encode($shops);
	}
	
	/**
	 * 提交订单
	 */
	public function submitOrder(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		empty($userId) || ($userId != 1229) && $this->error('权限不足');

		$shopId  = I('post.shopid');
		$model   = I('post.model');
		$color   = I('post.color');
		$fault   = I('post.fault');
		$way     = I('post.way');
		$uname   = I('post.uname');
		$area    = I('post.area');
		$address = I('post.address');
		$mobile  = I('post.mobile');
		$price   = I('post.price');
		
		$users   = D('Users');
		$us      = D('UserShop');
		$repairs = D('Repairs');
		$orders  = D('Orders');
		
		$user = $users->getUserByMobile($mobile);
		if(empty($user['id'])){   // 未注册
			$password  = substr($mobile, -6);
			$rd = $users->regist($mobile, $password);
			if($rd['userId'] > 0){   // 注册成功
				$user = $users->getUserById($rd['userId']);
				// 发送密码
				sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
			}else{
				echo -1;exit;
			}
		}
		
		$orderSn = CBWCreateOrderNumber();
		
        $data['user_id']       = $user['id'];
		$data['agent_id']      = 0;
        $data['order_sn']      = $orderSn;
        $data['order_date']    = time();
		$data['order_type']    = 1;
        $data['order_status']  = 1;
        $data['pay_status']    = 0;
		$data['deail_price']   = $price;
		$data['area_id']       = $area;
		$data['address']       = $address;
		$data['reciver_user']  = $uname;
		$data['reciver_phone'] = $mobile;
		$data['remarks']       = '保障卡维修订单';
		
		$orderId = $orders->insertOrders($data);
		if($orderId){
			$relation = $repairs->getFaultAllByModelIdAndFaultId($model, $fault);
			$server = array();
			$server['order_id']      = $orderId;
			$server['shop_id']       = $shopId;
			$server['relation_id']   = $relation['id'];
			$server['color_id']      = $color;
			$server['server_way']    = $way;
			$server['security']      = 1;
			$server['server_status'] = 0;
			$serverid = $orders->insertOrderServer($server);  // 添加维修订单附表
			if($serverid){
				$shop = $us->getShopByShopId($shopId);     // 店铺信息
				sendSmsByAliyun($shop['user_phone'], array('name'=>$mobile,'password'=>$password), 'SMS_116591038'); // 给店铺发送下单通知短信
				//sendSmsByAliyun($mobile, array(), 'SMS_116561091'); // 给顾客发送下单通知短信
				echo 1;exit;
			}
			$orders->deleteOrders($orderId);   // 下单失败,删除订单表数据
		}
		echo 0;exit;
	}
	
}