<?php
namespace Home\Controller;

class FaultsController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    // 故障说明
    public function detail(){
		
		$model = M();
		$modelCity = M('addr_city');
		$modelExplain = M('fault_explain');
		
		$id = I('get.id');
		
		$faults = $modelExplain->where("id=".$id)->find();		
		if(!is_array($faults)){
			$this->redirect('Empty/index');
		}
		
		//故障说明分类列表
		$modelCat = M('product_cat');
		$catRes = $modelCat->field('id,cat_name')->where(array('fid'=>'4','is_show'=>'1'))->order('sort ASC')->select();
		foreach($catRes as $key=>$vo){
			$brandRes = $modelCat->field('id,cat_name')->where(array('fid'=>$vo['id'],'is_show'=>'1'))->order('sort ASC')->select();
			foreach($brandRes as $k=>$v){
				$faultRes = $modelExplain->field('id,title')->where(array('fault_id'=>$v['id'],'is_show'=>'1'))->order('id ASC')->select();
				foreach($faultRes as $m){
					if($m['id'] == $id){
						$catRes[$key]['is_on'] = 1;
						$brandRes[$k]['is_on'] = 1;
					}
				}
				$brandRes[$k]['faultRes'] = $faultRes;
			}
			$catRes[$key]['brandRes'] = $brandRes;
		}
		$this->assign('catRes',$catRes);
		
		//点击次数加一
		$modelExplain ->where('id = '.$id)->setInc('clicktimes',1);
		
		//附近店铺推荐
		$city  = session('location.city');
		$citys = $modelCity->where("addr='".$city."'")->find();
		if(empty($citys)){
			$citys = $modelCity->where("addr='深圳市'")->find();
		}
		$datas = $model->table('__USER_SHOP__ AS s')
					->field('s.id,s.user_id,s.shop_name,s.exp,s.logo_image,a.addr')
					->join('__USER_MODEL__ AS m ON m.user_id=s.user_id')
					->join('__ADDR_AREA__ AS a ON a.id=s.area_id')
					->join('__ADDR_CITY__ AS c ON c.id=a.two_id')
					->where("s.type_id<>2 AND s.server_status=1 AND c.id=".$citys['id'])
					->group('s.id')
					->order('s.id DESC')
					->select();
		$rand = array_rand($datas,5);
		$nearbys = array();
		$i = 0;
		foreach($datas as $key=>$vo){
			if(in_array($key,$rand)){
				$nearbys[$i]['id'] = $vo['id'];
				$nearbys[$i]['shop_name'] = $vo['shop_name'];
				$nearbys[$i]['logo_image'] = $vo['logo_image'];
				$nearbys[$i]['addr'] = $vo['addr'];
				
				$level = CBWShopLevel($vo['exp']); // 等级换算
				$nearbys[$i]['level_image_name'] = $level['name'];
				$nearbys[$i]['level_image_num']  = $level['num'];
				$nearbys[$i]['level_level']      = $level['level'];
				
				$shopcats = $model->table('__USER_MODEL__ AS m')
						->field('c.cat_name')
						->join('__PRODUCT_CAT__ AS c ON c.id=m.brand_id')
						->where('m.shop_id='.$vo['id'])
						->group('m.brand_id')
						->limit(6)
						->select();
				$shopcat = array();
				if(!empty($shopcats)){
					foreach($shopcats as $s){
						$shopcat[] = $s['cat_name'];
					}
					$nearbys[$i]['shopcat'] = implode(',', array_unique($shopcat));
				}
				
				$i++;
			}
		}
		
		//SEO
		if($faults['keywords'] == '' && $faults['description']==''){
			$ident ="faultexplain";
			$idents =$this->seo($ident);
		}else{
			$idents['title'] = $faults['title'];
			$idents['keywords'] = $faults['keywords'];
			$idents['description'] = $faults['description'];
		}
		$this->assign("title",$idents['title']);
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);
		
		$this->assign('id', $id);
		$this->assign('faults', $faults);
		$this->assign('nearbys', $nearbys);
		$this->assign('catRes', $catRes);
        $this->display();
    }

}
