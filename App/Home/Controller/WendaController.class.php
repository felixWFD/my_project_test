<?php
namespace Home\Controller;

class WendaController extends BaseController {
    public function __construct(){
        parent::__construct();
    }
	
    //问答列表
    public function index(){
               
		$modelWenda = M('wenda_contents');
		$modelCat = M('wenda_cat');
		
		//问答分类列表
		$catRes = $modelCat->where("is_show=1")->order('id ASC')->select();
		
		//问答页广告
        $modelAdv = M('advs');
        $photoRes = $modelAdv->field('id, adv_name, thumbnail')
                              ->where(array('position_id'=>5, 'is_show'=>1))
                              ->order('id DESC')
                              ->limit(5)
                              ->select();
		$this->assign("photoRes",$photoRes);
		
		//最新问答
		$new_wenda = $modelWenda->order('create_time DESC')->limit(10)->select();
		$this->assign('new_wenda', $new_wenda);
		
        // 获取查询产品的类型
		$cat = I('get.cat');
		$where = "is_show=1";
		$parameter = "index";
		if(!empty($cat)){
			$where .= " AND cat_id=".$cat;
			$parameter = $cat;
			$nav = $modelCat->field('id,cat_name')->where('id='.$cat)->find();
			$this->assign('nav', $nav);
		}
	
        $getPageCount = $modelWenda->where($where)->count();

        // 分页处理
        $page = new \Think\MyPage($getPageCount, 10);
		$page->url = 'wenda_list/'.$parameter.'/';
		$wendaRes = $modelWenda->where($where)->order("create_time DESC")->limit($page->firstRow, $page->listRows)->select();
		foreach($wendaRes as $key=>$vo){
			if(empty($vo['thumbnail'])){
				$thumbnail = CBWGrabImages(htmlspecialchars_decode($vo['content']));  //抓取内容第一张图片 作为列表图
				$thumbnail = '<img src="'.$thumbnail.'" title="'.$vo['title'].'" alt="'.$vo['title'].'" />';
			}else{
				$thumbnail = '<img src="'.C('SITE_URL').'/Public/Wenda/'.$vo['thumbnail'].'" title="'.$vo['title'].'" alt="'.$vo['title'].'" />';
			}
			$wendaRes[$key]['thumbnail'] = $thumbnail;
		}
        $pageShow = $page->show();
		
		$this->assign('cat', $cat);
        $this->assign('page', $pageShow);
        $this->assign('catRes', $catRes);
		$this->assign('wendaRes', $wendaRes);
		
		//SEO
        $ident ="wenda";
        $idents =$this->seo($ident);
        $this->assign("title",$idents['title']);
        $this->assign("keywords",$idents['keywords']);
        $this->assign("description",$idents['description']);

        $this->display();
    }
	
    //问答详细
    public function detail(){
       
		$modelWenda = M('wenda_contents');
		$modelCat = M('wenda_cat');
		
		$id = I('get.id');
		
		//增加点击数
		$modelWenda->where('id = '.$id)->setInc('clicktimes',1);
		
		//问答分类列表
		$catRes = $modelCat->where("is_show=1")->order('id ASC')->select();
		$this->assign('catRes',$catRes);
		
		//问答页广告
        $modelAdv = M('advs');
        $photoRes = $modelAdv->field('id, adv_name, thumbnail')
                              ->where(array('position_id'=>5, 'is_show'=>1))
                              ->order('id DESC')
                              ->limit(5)
                              ->select();
		$this->assign("photoRes",$photoRes);
				
		$wenda = $modelWenda->where('id='.$id)->find();
		if(!is_array($wenda)){
			$this->redirect('Empty/index');
		}
		$wenda['content'] = CBWKeywordAddLink(htmlspecialchars_decode($wenda['content'])); // 关键词加链接
		$wenda['content'] = CBWImageUpdateAttribute($wenda['title'], $wenda['content']);   // 更新图片属性 
		$this->assign('wenda',$wenda);
		
		//分类信息
		$nav = $modelCat->field('id,cat_name')->where('id='.$wenda['cat_id'])->find();
		$this->assign('nav', $nav);
		
		//相关资讯
		$keys = explode(",",$wenda['keywords']);
		$where = "id !=".$wenda['id']." AND (";
		foreach($keys as $key=>$item){
			if($key == 0){
				$where .= "title like '%".$item."%'";
			}else{
				$where .= " OR title like '%".$item."%'";
			}
		}
		$where .= ")";
		$relevant_wenda = $modelWenda->where($where)->order('create_time DESC')->limit(10)->select();
		$this->assign('relevant_wenda', $relevant_wenda);
		
		//上一篇
		$previous = $modelWenda->where('cat_id='.$wenda['cat_id'].' AND id<'.$wenda['id'])->order('id DESC')->find();
		$this->assign('previous',$previous);
		//下一篇
		$next = $modelWenda->where('cat_id='.$wenda['cat_id'].' AND id>'.$wenda['id'])->order('id ASC')->find();
		$this->assign('next',$next);
		
		//SEO
		if($wenda['keywords'] == '' && $wenda['description']==''){
			$ident ="wenda";
			$idents =$this->seo($ident);
		}else{
			$idents['title'] = $wenda['title'];
			$idents['keywords'] = $wenda['keywords'];
			$idents['description'] = $wenda['description'];
		}
		$this->assign("title",$idents['title']."_草包问答");
		$this->assign("keywords",$idents['keywords']);
		$this->assign("description",$idents['description']);
		
        $this->display();
		
    }

}
