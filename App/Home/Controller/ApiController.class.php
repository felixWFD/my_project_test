<?php
namespace   Home\Controller;

header('Content-type:text');
class ApiController extends BaseController {
	
	// 外部平台订单回调
	public function external(){
		$push = $_GET;
		if($push['name'] == 'jikexiu'){
			Vendor('ExternalApi.jikexiuApi');
			$response = \jikexiuApi::receive($push);
			return $response;
		}
	}
	
	// 微信公众平台消息接口开发
	public function wechat(){
		$echostr = I('get.echostr');
		$wechatObj = new \Think\WechatCallbackApiTest();
		if(empty($echostr)) {
			$wechatObj->responseMsg();
		}else{
			$wechatObj->valid();
		}	
	}

	// 百度知道,提问推送处理
    public function zhidao_questions(){

		//百度知道传递过来的所有参数都在$push这个数组里
		$push = $_POST;

		$data['question_id']  = $push['question_id'];                       //问题编号,不超过32个字符
		$data['title']        = $push['title'];                             //问题标题
		$data['content']      = $push['content'];                           //问题补充描述
		$data['pictures']     = $push['pictures'];                          //问题补充中包含的图片url,多个url用逗号分割
		$data['cid']          = $push['cid'];                               //问题分类id
		$data['cname']        = iconv('GB2312', 'UTF-8', $push['cname']);   //问题分类名
		$data['anonymous']    = $push['anonymous'];                         //是否匿名提问,1匿名 0非匿名
		$data['hit_keywords'] = $push['hit_keywords'];                      //匹配到的定制关键词列表,用逗号分隔
		$data['hit_cids']     = $push['hit_cids'];                          //匹配到的定制分类id列表,用逗号分隔
		$data['lbs_province'] = $push['lbs_province'];                      //问题的lbs属性:省份
		$data['lbs_city']     = $push['lbs_city'];                          //问题的lbs属性:城市
		$data['lbs_district'] = $push['lbs_district'];                      //问题的lbs属性:区域
		$data['lbs_street']   = $push['lbs_street'];                        //问题的lbs属性:街道
		$data['create_time']  = $push['create_time'];                       //问题产生的时间
		$data['appkey']       = $push['appkey'];                            //问题产生的来源应用编号,默认为1
		$data['appname']      = $push['appname'];                           //问题产生的来源应用名字,默认为百度知道
		$data['uid']          = $push['uid'];                               //提问者账号id,默认为空字符串
		$data['uname']        = $push['uname'];                             //提问者账号name,默认为空字符串

		$erron = 1;
		$modelQuestions = M('zhidao_questions');
		if($modelQuestions->create($data)){
			$rt = $modelQuestions->add();
			if($rt > 0){
				$erron = 0;
			}
		}
		//...........

		//最终您必须给百度知道返回以下格式的内容(json格式且必须包含errno字段),errno=0表示您已经接收成功,否则您可以设定errno为非0的任意整数值
		$res = array('errno' => $erron, 'errmsg' => 'success');
		echo json_encode($res);
    }
	
	// 百度知道,追问推送处理
    public function zhidao_zhuiwen(){

		//百度知道传递过来的所有参数都在$push这个数组里
		$push = $_POST;

		$data['question_id']  = $push['question_id'];   //问题编号,不超过32个字符
		$data['reply_id']     = $push['reply_id'];      //回答编号
		$data['reask_id']     = $push['reask_id'];      //追问编号
		$data['content']      = $push['content'];       //追问内容
		$data['pictures']     = $push['pictures'];      //追问内容里的图片url,多个用逗号分隔
		$data['create_time']  = $push['create_time'];   //追问产生的时间
		$data['appkey']       = $push['appkey'];        //追问产生的来源应用编号,默认为1
		$data['appname']      = $push['appname'];       //追问产生的来源应用名字,默认为百度知道
		$data['uid']          = $push['uid'];           //提问者账号id,默认为空字符串
		$data['uname']        = $push['uname'];         //提问者账号name,默认为空字符串

		$erron = 1;
		$modelZhuiwen = M('zhidao_zhuiwen');
		if($modelZhuiwen->create($data)){
			$rt = $modelZhuiwen->add();
			if($rt > 0){
				$erron = 0;
			}
		}
		//...........

		//最终您必须给百度知道返回以下格式的内容(json格式且必须包含errno字段),errno=0表示您已经接收成功,否则您可以设定errno为非0的任意整数值
		$res = array('errno' => $erron, 'errmsg' => 'success');
		echo json_encode($res);
    }
	
	// 百度知道,剔除推送处理
    public function zhidao_del(){

		//百度知道传递过来的所有参数都在$push这个数组里
		$push = $_POST;

		$data['question_id']  = $push['question_id'];   //问题编号,不超过32个字符
		$data['reason']       = $push['reason'];        //回答编号
		$data['create_time']  = $push['create_time'];   //追问产生的时间

		$erron = 0;
		
		// 删除提问表
		$modelQuestions = M('zhidao_questions');
		$modelQuestions->where("question_id='".$data['question_id']."'")->delete();
		
		// 删除回答表
		$modelAnswer = M('zhidao_answer');
		$modelAnswer->where("question_id='".$data['question_id']."'")->delete();
		
		// 删除追问表
		$modelZhuiwen = M('zhidao_zhuiwen');
		$modelZhuiwen->where("question_id='".$data['question_id']."'")->delete();
		
		// 删除追答表
		$modelZhuida = M('zhidao_zhuida');
		$modelZhuida->where("question_id='".$data['question_id']."'")->delete();
		//...........

		//最终您必须给百度知道返回以下格式的内容(json格式且必须包含errno字段),errno=0表示您已经接收成功,否则您可以设定errno为非0的任意整数值
		$res = array('errno' => $erron, 'errmsg' => 'success');
		echo json_encode($res);
    }

	// 快递100 推送处理
	public function kuaidi(){
		
		$param = $_POST['param'];
		$json = CBWOjectArray(json_decode($param));
		try{
			//$param包含了文档指定的信息，...这里保存您的快递信息,$param的格式与订阅时指定的格式一致
			$modelExpress = M('express');
			$data['message'] = $json['message'];
			$data['state']   = $json['lastResult']['state'];
			$data['ischeck'] = $json['lastResult']['ischeck'];
			$data['content'] = json_encode($json['lastResult']['data']);
			$modelExpress->where("number='".$json['lastResult']['nu']."'")->save($data);
			echo  '{"result":"true",	"returnCode":"200","message":"成功"}';
			//要返回成功（格式与订阅时指定的格式一致），不返回成功就代表失败，没有这个30分钟以后会重推
		} catch(Exception $e){
			echo  '{"result":"false",	"returnCode":"500","message":"失败"}';
			//保存失败，返回失败信息，30分钟以后会重推
		}
	}

}