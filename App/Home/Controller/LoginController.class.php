<?php
namespace 	Home\Controller;

class LoginController extends BaseController {
	
	//客服系统登录页面
    public function server(){
		
		//判断是否已经登录,如果登录则跳转
		$server_id = session('server.serverid');
		if($server_id != ""){
			$this->redirect('Ssystem/index');
		}
        $this->display();
    }
	
	//客服系统登录操作
	public function server_login(){
		
		if (!IS_POST) {
			$this->error('非法请求！');
		}
		
		$username = I('post.username');
		$password = I('post.password');
		
		if(empty($username)){
			$this->error("用户名不能为空");
			exit;
		}
		if (empty($password)){
            $this->error('密码不能为空');
            exit;
        }
		// 验证用户是否存在
		if ( ! $userInfo = $this->_checkServerLogin($username, $password)){
			$this->error('用户名或密码错误');
			exit;
		}
		// 保存SESSION
		$this->_saveServerSession($userInfo['id'], $userInfo['username'], $userInfo['last_login_time']);

		if (session('?serverid') && session('?username')){
			// 修改维修师此次登录的状态
			$modelUsers = M('admin_users');
			$upData['last_login_time'] = time();
			$upData['last_login_ip'] = ip2long(I('server.REMOTE_ADDR'));
			$modelUsers->where('id = ' . $userInfo['id'])->save($upData);
			
			$this->success('登录成功.' . $userInfo['id'], U('Ssystem/index'));
			exit;
		}else{
			$this->error('未知错误');
			exit;
		}

	}

	/**
	 * 退出客服登录
	 * @return void
	 */	
	public function server_logout(){
		
		session('server', NULL);
		$jumpURL = U('Login/server');
		$this->success('再见，下次再来', $jumpURL);
	}

	/**
	 * 验证客服是否存在
	 * @param  string $username 客服用户名
	 * @param  string $password 密码
	 * @return void           
	 */	
	protected function _checkServerLogin ($username, $password){
		
		$modelUsers = M('admin_users');
		$password = sha1($password);
		$userInfo = $modelUsers->where("username = '{$username}' AND password = '{$password}'")->find();

		return empty ($userInfo) ? NULL : $userInfo;
	}

	/**
	 * 登录客服保存SESSION信息
	 * @param  [int] 	$repairid   [用户ID]
	 * @param  [string] $loginid    [授权编号]
	 * @return [void]   
	 */
	protected function _saveServerSession($serverid, $username, $lasttime){
		
		// 加密干扰字符串
		$discrubleCode = md5($serverid . 'RIBBON');
		session(array('name'=>'server', 'prefix'=>'server'));
		session('serverid', $serverid);
		session('username', $username);
		session('lasttime', $lasttime);
		session('servermd5', $discrubleCode);
	}
	
}