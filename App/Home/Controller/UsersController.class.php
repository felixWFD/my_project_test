<?php
namespace Home\Controller;

class UsersController extends BaseController {
    
	/**
     * 登录界面
     */
	public function login($type = null){
		$userId = session('user.userid');

		if($userId != ""){
			$this->redirect("Users/index");
		}
		if(!empty($type)){
			$config = C("THINK_SDK_WEIXIN");
			$wechatObj = new \Think\WechatApi($config['APP_KEY'], $config['APP_SECRET']);
			redirect($wechatObj->qrconnectURL());
		}
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');

        $login_times=session('login_times');
        if(!empty($login_times)) {
            $this->assign('times', $login_times);
        }
        $tencent_config=C('TENCENT_IMG_CODE');
        $this->assign("appid",$tencent_config['appid']);
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Users/login');
	}

    /**
	 * 授权回调地址
	 * @type 类型 
	 * @code void 
	 * @return void 
	 */
    public function callback($code = null){
        (empty($code)) && $this->error('参数错误');

        $config = C("THINK_SDK_WEIXIN");
		$wechatObj = new \Think\WechatApi($config['APP_KEY'], $config['APP_SECRET']);
        $token = $wechatObj->accessTokenGet2($code);
        //获取当前登录用户信息
        if(is_array($token)){
			$wechat2 = new \Think\WechatApi($config['APP_KEY'], $config['APP_SECRET'], $token);
			$chat = $wechat2->userInfoQuery();
			if(is_array($chat)){
				$uw = D('UserWechat');
				$wechat = $uw->getWechatByUnionId($token['unionid']);
				if(!empty($wechat['user_id'])){
					$users = D('Users');
					$user = $users->getUserById($wechat['user_id']);
					$users->saveLogin($user);
					$this->redirect("Users/index");
				}else{
					$data = array(
						'openId'    =>$chat['openid'],
						'partnerId' =>$chat['unionid'],
						'nickName'  =>$chat['nickname'],
						'picURL'    =>base64_encode($chat['headimgurl']),
						'platform'  =>'wechat'
					);
					$this->redirect("Users/binding", $data);
				}
			}else{
				$this->error('获取微信用户信息失败');
			}
        }else{
			$this->error('微信返回数据有误');
		}
    }

	/**
	 * 微信扫码登录绑定手机界面
	 */
	public function binding(){		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Users/binding');
	}

	/**
	 * 检查手机号码是否有发送记录
	 */
	public function checkMobileSms(){		
		$mobile = I('post.mobile');
		$usercode = D('UserCode');
		$data = $usercode->get($mobile);
		echo json_encode($data);
	}

	/**
	 * 微信扫码登录绑定手机界面
	 */
	public function doBindingMobile(){
		$rd        = array('status'=>-1, 'msg'=>'绑定失败');
		$mobile    = I('post.mobile');
		$password  = substr($mobile, -6);
		$smscode   = I('post.smscode');
		$openId    = I('post.openId');
		$partnerId = I('post.partnerId');
		$nickName  = I('post.nickName');
		$picURL    = base64_decode(I('post.picURL'));
		$platform  = I('post.platform');
		
		$users    = D('Users');
		$usercode = D('UserCode');
		$uw       = D('UserWechat');
		
		if(empty($mobile) || empty($partnerId)){
			$rd = array('status'=>-2, 'msg'=>'参数错误');
		}else{
			$uc = $usercode->check($mobile, $smscode);   // 验证短信验证码
			if($uc == -1){
				$rd = array('status'=>-3, 'msg'=>'短信验证码超时');
			}else if($uc == -2){
				$rd = array('status'=>-4, 'msg'=>'短信验证码错误');
			}else{
				$user = $users->getUserByMobile($mobile);
				if(empty($user)){
					$rs = $users->regist($mobile, $password);
					if($rs['userId']>0){                       // 注册成功
						// 发送密码
						sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
						
						// 加载用户信息				
						$user = $users->getUserAndInfoById($rs['userId']);
						
						// 保存会员昵称
						$nickName = preg_replace("/[^\x{4e00}-\x{9fa5}]/iu",'',$nickName);  // 过滤特殊字符
						if(!empty($nickName)){
							$uname = strlen($nickName)>21?msubstr($nickName, 0, 7, 'utf-8', false):$nickName;
							if(!empty($uname)){
								$users->updateUser(array('user_id'=>$rs['userId'], 'uname'=>$uname));
							}
						}
						
						// 保存微信头像为会员头像
						/*if($picURL != '' && !empty($picURL)){
							$head = CBWDownloadFile($picURL, './Public/User/');
							if(!empty($head)){
								if(filesize('./Public/User/'.$head) > 0){
									$users->updateUserInfo(array('user_id'=>$rs['userId'], 'thumb'=>$head));
									@unlink('./Public/User/'.$user['thumb']);
								}else{
									@unlink('./Public/User/'.$head);
								}
							}
						}*/
					}
				}
				
				$wechat = $uw->getWechatByUnionId($partnerId);
				if(empty($wechat)){
					$data = array(
						'user_id'    => $user['id'], 
						'openid'     => $openId,
						'unionid'    => $partnerId,
						'nickname'   => $nickName,
						'headimgurl' => $picURL
					);
					$uw->insertWechat($data);
				}else{
					$fid = ($wechat['fid'] == $user['id'])?0:$wechat['fid'];
					if($wechat['user_id']){  // user_id不为0,则检查用户是否是店铺或代理用户(店铺用户或代理用户不能成为粉丝)
						$wuser = $users->getUserById($wechat['user_id']);
						if($wuser['group_id'] > 0 || $wuser['is_agent'] > 0){
							$fid = 0;
						}
					}
					$data = array(
						'fid'        => $fid,
						'user_id'    => $user['id'], 
						'nickname'   => $nickName,
						'headimgurl' => $picURL
					);
					$uw->updateWechat($wechat['id'], $data);
				}

				$users->saveLogin($user);  // 保存会员登录信息
				$usercode->del($mobile);   // 删除短信验证码相关记录
				$rd = array('status'=>$user['id']);
			}
		}
		echo json_encode($rd);
	}

	/*
	 * 生成验证码
	 */
    public function imgCode(){
        // 检查图形验证码
        $verify = new \Think\Verify();
        $verify->useImgBg = true;//开启验证码背景
        //$verify->useZh = true;//中文验证码
        $verify->entry(1);
    }

	/**
	 * 用户登录操作
	 * @return void 
	 */
	public function dologin(){
		$rd = array('status'=>-1);
		if (!IS_POST) {
			$rd["status"] = -2;  // 非法请求!
		}else{
			//验证码验证
            if(session('login_times')){
                // 检查图形验证码
                $configuration=C('TENCENT_IMG_CODE');
                $Ticket = I('ticket'); //票据
                $Randstr =I('randstr');//随机串
                $UserIP = get_client_ip(); //客户端ip

                $params = array(
                    "aid" => $configuration['appid'],//appid
                    "AppSecretKey" => $configuration['AppSecretKey'], //AppSecretKey
                    "Ticket" => $Ticket,
                    "Randstr" => $Randstr,
                    "UserIP" => $UserIP
                );
                $paramstring = http_build_query($params);
                $content = verifyTencentImgCode($paramstring,1,0);
                $result = json_decode($content,true);
                if($result){
                	if($result['response']!=1){
                        echo json_encode(['status'=>-5,'msg'=>$result['err_msg']]) ;
                        exit;
					}
                }else{
                    echo json_encode(['status'=>-5,'msg'=>'请求失败！']) ;
                    exit;
                }
            }

			$username   = I('post.username');
			$password   = I('post.password');
			$controller = session('jump.controller');
			$action     = session('jump.action');
			if(!empty($controller) && !empty($action)){
				$rd["refer"] = U($controller.'/'.$action);
			}else{
				$rd["refer"] = U('Users/index');
			}
			
			$users = D('Users');
			
			// 用户登录验证
			$user = $users->checkLogin($username, $password);
			if($user['status'] == 0){
				$rd["status"] = -3;  // 登陆失败，账号被锁
			}else if($user['status'] == -1){
				$rd["status"]  = 0;  // 登陆失败，账号或密码错误
				$rd["failed"] = $users->saveLoginFailed($username);  // 登陆失败次数
                session('login_times',$rd["failed"]);
			}else{
				$users->saveLogin($user);  // 保存会员登录信息
				if($user['login_failed'] > 0){
					$users->updateUser(array('user_id'=>$user['id'], 'login_failed'=>0));  // 重置登录失败次数
				}
				$rd["status"] = 1;
			}
		}
		echo json_encode($rd);
	}

	/**
	 * 退出登录
	 * @return void
	 */	
	public function logout(){
		cookie(null,'cbw_');
		session('user', NULL);
		session('jump', NULL);
		session('gws', NULL);
		session('gws_count', 0);
		
		// 店铺系统退出
		cookie(null,'system_');
		session('shop', NULL);
		
		$this->redirect("Users/login");    //页面跳转
	}
	
	/**
     * 注册界面
     * 
     */
	public function regist(){
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Users/regist');
	}
	
	/**
	 * 检查注册账号是否已经注册
	 * @return void 
	 */
	public function checkRegist() {
		$rd = array('status'=>-1);
		if (!IS_POST) {
			$rd['status'] = -2;  // 非法请求!
		}
		
		$type      = I('post.type');
		$loginName = I('post.loginName');
		
		$users = D('Users');
		
		if($type == 'mobile'){
			$rd = $users->checkUserMobile($loginName);
		}else{
			$rd = $users->checkUserEmail($loginName);
		}
		echo json_encode($rd);
	}

	/**
	 * 新用户注册
	 */
	public function doRegist(){
		$rs = array();
		$loginName  = I('post.loginName');
		$loginPwd   = I('post.loginPwd');
		$codeword   = I('post.codeword');
		$type       = I('post.type');
		$controller = session('jump.controller');
		$action     = session('jump.action');
		
		$usercode = D('UserCode');
		$uc = $usercode->check($loginName, $codeword);   // 验证短信验证码
		if($uc == -1){
			$rs["status"] = -3;
			$rs['msg'] = '短信验证码超时';
		}else if($uc == -2){
			$rs["status"] = -4;
			$rs['msg'] = '短信验证码错误';
		}else{
			$users = D('Users');
			$rs = $users->regist($loginName, $loginPwd, $type);
			if($rs['userId']>0){                       // 注册成功	
				// 删除短信验证码相关记录
				$usercode->del($loginName);
				// 加载用户信息				
				$user = $users->getUserById($rs['userId']);
				if(!empty($user)){
					$users->saveLogin($user);  // 保存会员登录信息
					if(!empty($controller) && !empty($action)){
						$rs["refer"] = U($controller.'/'.$action);
					}else{
						$rs["refer"] = U('Users/index');
					}
				}
			}	
		}
		echo json_encode($rs);
	}
	
	/**
	 * 忘记密码
	 */
    public function forget(){
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('index');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
    	$this->display();
    }
	
	/**
	 * 检查输入邮箱或手机号码是否存在
	 */
	public function doForget(){
		$loginName = I('post.acc');
		
		$users = D('Users');
		$user = $users->getUserByLoginName($loginName);
		if($user['id'] >0){
			$this->redirect('Users/validate', array('id'=>$user['id']));
		}else{
			$this->error('注册的邮箱地址或手机号码不存在');
		}
		
	}
	
	/**
	 * 发送 邮箱验证 or 手机验证
	 */
	public function validate(){
		$userId = I('get.id');
		
		$users = D('Users');
		$user = $users->getUserById($userId);
		$user['mobile_format'] = hidestr($user['mobile']);
		$this->assign("user",$user);
		$this->display();
	}
	
	/**
	 * 发送找回密码邮件
	 */
	public function sendForgetMail(){
		$email = I('post.email');
		
		$users  = D('Users');
		$system = D('System');
		
		$user = $users->getUserByEmail($email );

		$code = md5($user['email'] . md5($user['password']));
		
		$url = C('SITE_URL').'/Users/password/id/' . $user['id'] . '/active_code/' . $code;
		
		// 保存active code
		$users->updateUser(array('user_id'=>$user['id'], 'active_code'=>$code));
		
		// 获取邮件模板内容
		$temp = $system->loadEmailTemplate('email_get_pwd');
		
		$result = CBWSendMail($email, $temp['subject'], $temp['content'], 'template', $url);
		
		echo $result;
	}
	
	/**
	 * 找回密码  修改密码页面
	 */
	public function password(){
		
		$type   = I('get.type');
		$code   = I('get.mesgCode');
		$userId = I('get.id');
		$active = I('get.active_code');

		$users  = D('Users');
		$uc     = D('UserCode');
		
		$user = $users->getUserById($userId);
		
		if($type == 'mobile'){
			// 检查验证码
			$rs = $uc->check($user['mobile'], $code);
			if($rs == -1){
				$this->error('验证码过期',U('Users/forget'));
				exit;
			}else if($rs == -2){
				$this->error('验证码错误',U('Users/forget'));
				exit;
			}
			// 删除user_code表相关记录
			$uc->del($user['mobile']);
		}else{
			if($user['active_code'] != $active){
				$this->error('信息错误',U('Users/forget'));
				exit;
			}
		}
		$this->assign('user',$user);
		$this->display();
	}
	
	/**
	 * 找回密码  修改密码操作
	 */
	public function editPassword(){
		$userId     = I('post.id');
		$password   = I('post.password');
		$repassword = I('post.repassword');
		
		if($repassword == $password){
			$users = D('Users');
			$rs = $users->updatePass($userId, $password);
			if($rs['status'] > 0){
				$this->success('密码修改成功', U('Users/login'));
			}else{
				$this->error('密码修改失败', U('Users/forget'));
			}
		}
	}
	
	/**
	 * 发送邮件注册验证码
	 * @date 2015-01-14
	 * @return [void] 
	 */
	
	public function sendRegistMail(){
		
		$email      = I('post.email');          // 邮件地址
		$imgcode    = I('post.imgcode');        // 图形验证码
		$ip         = I('server.REMOTE_ADDR');  // 获取IP地址
		$code       = mt_rand(100000, 999999);  // 生成验证码
		$nowtime    = time();                   // 当前时间
		$count      = 1;
		
		if(!empty($imgcode)){
			// 检查图形验证码
			$verify = new \Think\Verify();
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-2, 'msg'=>'图形验证码错误！请重试'));exit;
			}
		}
		
		$smsip    = D('SmsIp');
		$usercode = D('UserCode');
		$system   = D('System');
		
		$sms = $smsip->get($ip);
		if(is_array($sms)){
			if($sms['sms_count'] >= 6){
				echo json_encode(array('status'=>-3, 'msg'=>'您当日IP累计获取验证码已达上限，请您次日再试'));exit;
			}
			$smsip->edit(array('id'=>$sms['id'], 'sms_count'=>$sms['sms_count']+1));       // 更新IP操作次数
		}else{
			$sms_count = 1;
			$smsip->insert(array('ip'=>$ip, 'add_time'=>$nowtime, 'sms_count'=>$sms_count)); // 插入IP操作次数
		}
		
		$codeinfo = $usercode->get($email);
		if(is_array($codeinfo)){
			if($codeinfo['sms_count'] >= 6){
				echo json_encode(array('status'=>-4, 'msg'=>'当日邮件累计获取验证码已达上限，请您次日再试'));exit;
			}
			if ($nowtime - $codeinfo['add_time'] < 90){
				echo json_encode(array('status'=>-5, 'msg'=>'操作过于频繁，请您稍后再试'));exit;
			}
			$code = $codeinfo['code'];
			$count = $codeinfo['code_count']+1;
		}
		
		// 获取邮件模板内容
		$temp = $system->loadEmailTemplate('email_send_verify');
		
		$result = CBWSendMail($email, $temp['subject'], $temp['content'], 'template', '', $code);
		if(!$result){
			echo json_encode(array('status'=>0, 'msg'=>'发送失败'));exit;
		}else{
			if($count == 1){
				$usercode->insert(array('account'=>$email, 'code'=>$code, 'add_time'=>$nowtime, 'code_count'=>$count)); // 插入发送次数
			}else{
				$usercode->edit(array('id'=>$codeinfo['id'], 'code_count'=>$count));                                    // 更新发送次数
			}
			echo json_encode(array('status'=>$count, 'msg'=>'发送成功'));exit;
		}
		echo json_encode(array('status'=>-1, 'msg'=>'未知错误'));exit;
	}
	
	/**
     * 用户中心页面
     */
	public function index(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
        session('login_times',null);//重置登录次数
		
		$users    = D('Users');
		$us       = D('UserShop');
		
		// 用户信息
		$user = $users->getUserAndInfoById();
		$user['level'] = CBWSecurityLevel($user);
		
		// 店铺信息
		if($user['group_id'] > 0){
			$shop = $us->getShopByUserId($user['id']);
		}
		
		// 草包豆收支明细
		$virtualc = $users->getVirtualCountByUserId($userId);
		$virtuals = $users->getVirtualByObj(array('userId'=>$userId, 'm'=>0, 'n'=>5));
		
		$this->assign("page", 1);
		$this->assign("pagecount",ceil($virtualc/5));
		$this->assign("virtuals", $virtuals);
		$this->assign("user", $user);
		$this->assign("shop", $shop);
		$this->assign("title", "用户中心");
		$this->display();
	}
	
    /**
     * 草包豆收支明细分页
     */
	public function loadPageVirtual(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$page   = I('post.page');
		
		$users = D('Users');
		$data = $users->getVirtualByObj(array('userId'=>$userId, 'm'=>$page*5, 'n'=>5));
		foreach($data as $key=>$vo){
			$data[$key]['create_time'] = date('Y-m-d H:i:s',$vo['create_time']);
		}
		echo json_encode($data);
	}
	
    /**
     * 跳到设置头像
     */
	public function avatar(){
		$this->isUserLogin();
		
		// 用户信息
		$users = D('Users');
		$user = $users->getUserAndInfoById();
		$this->assign("user", $user);
		$this->assign("title", "设置头像");
		$this->display();
	}
	
	/**
     * 裁剪图片保存头像
     */
	public function crop(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$crop = new \Think\CropAvatar(
		  './Public/Temp/',
		  './Public/User/',
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
		);
		$result  = $crop -> getResult();
		$message = $crop -> getMsg();
		if(!empty($result)){
			$users = D('Users');
			$user = $users->getUserAndInfoById($userId);
			$rt = $users->updateUserInfo(array('user_id'=>$userId, 'thumb'=>$result));
			if($rt['status'] > 0){
				unlink("./Public/User/".$user['thumb']);                   // 删除旧头像
			}
		}
		echo json_encode(array('state'  => 200, 'message' => $message, 'result' => $result));
	}
	
    /**
     * 提现
     */
	public function withdraw(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		
		$users = D('Users');
		$user = $users->getUserAndInfoById($userId);
		$user['alipay'] = unserialize($user['alipay']);
		$user['card']   = $users->getCardByUserId($userId);
		$user['allow']  = $user['money'] - $user['frozen_money'];

		$this->assign("user", $user);
		$this->assign("title", "提现");
		$this->display();
	}
	
	/**
     * 提现提交
     */
	public function saveWithdraw(){
		$this->isUserLogin();
		
		$userId   = session('user.userid');
		$typeId   = I('post.type');
		$cardId   = I('post.card');
		$money    = I('post.money');
		$password = I('post.password');
		
		$users = D('Users');
		$user = $users->getUserAndInfoById($userId);
		$allow = $user['money'] - $user['frozen_money'];
		
		if($user['pay_password'] != sha1($password)){
			$this->error("支付密码错误", U("Users/withdraw"));exit;
		}
		if($money > $allow){
			$this->error("账户余额不足", U("Users/withdraw"));exit;
		}
		$data = array();
		$data['user_id']     = $userId;
		$data['type_id']     = $typeId;

		if($typeId == '1'){
			$data['infos']   = $user['alipay'];
		}elseif($typeId == '2'){
			$card = $users->getCardById($cardId);
			$data['infos']   = serialize(array('name'=>$card['true_name'], 'card_id'=>$card['id'],'bank_name'=>$card['name'],'card_no'=>$card['card_no']));
		}
		$data['money']       = $money;
		$data['create_time'] = time();
		$data['status']      = 0;
		$rt = $users->insertWithdraw($data);
		if($rt['status'] > 0){
			$users->decreaseMoney($userId, 0, $money, 4, '提现'); // 扣除账户余额
			$this->success("提现成功,24小时内到达您的账户", U("Users/money"));exit;
		}
		$this->error("提现失败", U("Users/withdraw"));exit;
	}
	
    /**
     * 账户余额收支明细
     */
	public function money(){
		$this->isUserLogin();
		
		$userId = session('user.userid');

		$users  = D('Users');
		$orders = D('Orders');
		
		$count    = $users->getMoneyCount(array("userId"=>$userId));
        $page     = new \Think\Page($count, 9);
		$pageShow = $page->show();
		$data     = $users->getMoneyByObj(array("userId"=>$userId, "m"=>$page->firstRow, "n"=>$page->listRows));
		foreach($data as &$vo){
			if($vo['order_id'] > 0){
				$vo['order'] = $orders->getOrderById($vo['order_id']);
			}
		}
		unset($vo);
		
		$this->assign("moneys", $data);
		$this->assign('page', $pageShow);
		$this->assign("title", "余额收支明细");
		$this->display();
	}
	
    /**
     * 充值
     */
	public function recharge(){
		$this->isUserLogin();
		
		$userId = session('user.userid');

		$users    = D('Users');
		$products = D('Products');
		
		$user    = $users->getUserAndInfoById($userId);
		$product = $products->getProductAttrByProductId(722);
		$product = $product[0];
		foreach($product['list'] as $key=>&$vo){
			if($key == 0){
				$vo['virtual'] = $vo['attr_value']*100;
			}else{
				$vo['virtual']      = $vo['attr_value']*100;
				$vo['give_virtual'] = $key*10*$vo['attr_value'];
			}
		}
		unset($vo);
		
		$this->assign("user", $user);
		$this->assign("product", $product);
		$this->assign("title", "账户充值");
		$this->display();
	}
	
    /**
     * 充值操作
     */
	public function doRecharge(){
		$this->isUserLogin();
		
		$id      = I('post.id');
		$payLink = I('post.link');
		$userId  = session('user.userid');
		$groupId = session('user.groupid');
		$vip     = session('user.vip');
		$areaId  = 3008;

		$orders   = D('Orders');
		$products = D('Products');
		$users    = D('Users');
		$us       = D('UserShop');
		$ua       = D('UserAddress');
		
		$user = $users->getUserById($userId);
		if($user['group_id'] > 0){
			$shop = $us->getMainShopByUserId($user['id']);
			$areaId       = $shop['area_id'];
			$address      = $shop['address'];
			$reciverName  = $shop['user_name'];
			$reciverPhone = $shop['user_phone'];
		}else{
			// 默认地址
			$addr = $ua->getDefaultAddressByUserId($userId);
			if(!empty($addr)){
				$areaId       = $addr['area_id'];
				$address      = $addr['street'];
				$reciverName  = $addr['reciver_user'];
				$reciverPhone = $addr['reciver_phone'];
			}
		}

		// 商品信息
		$product = $products->getRelationByProductIdAndAttrId(722, $id);

		$orderSn = CBWCreateOrderNumber();
		$data    = array(
			'user_id'       => $userId,
			'agent_id'      => 0,
			'order_sn'      => $orderSn,
			'order_type'    => 0,
			'order_date'    => time(),
			'deail_price'   => $product['one_price'],
			'order_status'  => 1,
			'pay_status'    => 0,
			'area_id'       => $areaId,
			'address'       => $address,
			'reciver_user'  => $reciverName,
			'reciver_phone' => $reciverPhone,
			'send_time'     => 0,
			'is_invoice'    => 0,
			'pay_link'      => $payLink
		);
		$orderId = $orders->insertOrders($data);
		if($orderId){
			// 添加订单附表
			$schedule[0]['shop_id']       = 0;
			$schedule[0]['shipping_way']  = $shipping['sid'];
			$schedule[0]['is_shipping']   = 1;
			$schedule[0]['remarks']       = '';
			$schedule[0]['shipping_fee']  = 0;
			$schedule[0]['virtual_money'] = 0;
			$schedule[0]['shop_money']    = $product['one_price'];
			$orders->insertOrderSchedule($schedule, $orderId);
			
			// 添加购物订单表
			$goods[0]['pro_id']        = $product['id'];
			$goods[0]['pro_price']     = $product['one_price'];
			$goods[0]['two_price']     = $product['two_price'];
			$goods[0]['three_price']   = $product['three_price'];
			$goods[0]['pro_number']    = 1;
			$goods[0]['pro_attr']      = $product['pro_attr'];
			$goods[0]['virtual_money'] = 0;
			$orders->insertOrderProducts($goods, $orderId);

			$this->redirect('Buy/pay', array('id'=>$orderSn));
			exit;
		}else{
			$this->error('订单生成失败,请联系管理员',U('Buy/cart'));
			exit;
		}
	}
	
    /**
     * 个人信息
     */
	public function info(){
		$this->isUserLogin();
		
		$users = D('Users');
		$area  = D('Area');
		
		// 个人信息
		$user = $users->getUserAndInfoById();
		
		// 三级联动
		if(!$user || !$user['area_id']){
            $user['area_id']=1;
		}
		$info = $area->getAreaById($user['area_id']);
		$p = empty($info['one_id'])?'选择省':$info['one_id'];
		$c = empty($info['two_id'])?'选择市':$info['two_id'];
		$a = empty($info['id'])?'选择县/区':$info['id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("user", $user);
		$this->assign("title", "个人信息");
		$this->display();
	}
	
	/**
     * 更新个人信息
     */
	public function updateinfo(){
		$this->isUserLogin();
		
		$userId  = session('user.userid');
		$uname   = I('post.uname');
		$uname   = strlen($uname)>21?msubstr($uname, 0, 7, 'utf-8', false):$uname;
		$area    = I('post.area');
		$address = I('post.address');
		
		$users = D('Users');
		$u = $users->updateUser(array('user_id'=>$userId, 'uname'=>$uname));
		$i = $users->updateUserInfo(array('user_id'=>$userId, 'area_id'=>$area, 'address'=>$address));
		
		if($u['status'] == '1' || $i['status'] == '1'){
			$this->redirect("Users/info");
		}else{
			$this->error('更新失败');
		}
	}
	
    /**
     * 支付管理
     */
	public function pay(){
		$this->isUserLogin();
		
		$userId   = session('user.userid');
		
		$users = D('Users');
		$system = D('System');
		
		$user = $users->getUserAndInfoById();
		$user['alipay'] = unserialize($user['alipay']);
		$user['card']   = $users->getCardByUserId($userId);
		
		$banks = $system->loadBank();
		
		
		$this->assign("banks", $banks);
		$this->assign("user", $user);
		$this->assign("title", "支付管理");
		$this->display();
	}
	
	/**
     * 查询账号是否实名认证
     */
	public function checkRealNameAuth(){
		$this->isUserLogin();
	
		$users = D('Users');
		$rs = $users->checkUserRealNameAuth();
		if($rs['status'] == 1){
			echo 1;exit;     // 已实名认证
		}
		echo -1;exit;        // 未实名认证
	}
	
	/**
     * 添加快捷卡页面
     */
	public function addcard(){
		$this->isUserLogin();
		
		$userId   = session('user.userid');
		
		$users = D('Users');
		$banks = D('Banks');		
		
		$user     = $users->getUserAndInfoById();		
		$deposits = $banks->getBank();
		$credits  = $banks->getBankByCredit();
		
		$this->assign("credits", $credits);
		$this->assign("deposits", $deposits);
		$this->assign("user", $user);
		$this->assign("year", date("Y"));
		$this->assign("title", "添加快捷卡");
		$this->display();
	}
	
	/**
     * 修改快捷卡手机验证页面
     */
	public function editcard(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$id     = I('get.id');
		
		$users = D('Users');
		$card = $users->getCardById($id);
		$this->assign("card", $card);
		$this->assign("year", date("Y"));
		$this->assign("title", "修改快捷卡手机验证");
		$this->display();
	}
	
	/**
	 * 删除快捷卡
	 */
    public function delcard(){
		$this->isUserLogin();

		$users = D('Users');
		
		$id = I('get.id');
		$users->deleteCard($id);
		$this->redirect("Users/pay");
    }
	
	/**
     * 更新支付宝账号
     */
	public function updateAlipay(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$name   = I('post.name');
		$alipay = I('post.alipay');

		$users = D('Users');
		$rt = $users->updateUserInfo(array('user_id'=>$userId, 'alipay'=>serialize(array('name'=>$name, 'alipay'=>$alipay))));
		if($rt['status'] > 0){
			$this->success("更新成功", U("Users/pay"));exit;
		}
		$this->error("更新失败", U("Users/pay"));exit;
	}
	
	/**
     * 安全管理
     */
	public function security(){
		$this->isUserLogin();
		
		$users = D('Users');
		$user = $users->getUserAndInfoById();
		$user['level'] = CBWSecurityLevel($user);
		$this->assign("user", $user);
		$this->assign("title", "支付管理");
		$this->display();
	}
	
	/**
     * 发送绑定/解绑邮件
     */
	public function bindEmailSend(){
		$this->isUserLogin();
		
		$userId   = session('user.userid');
		$mail     = I('post.email');
		$template = I('post.template');
		$action   = I('post.action');
		
		$users  = D('Users'); 
		$user = $users->getUserAndInfoById();
		
		if($action == 'bind'){
			// 检查账号是否已绑定邮箱
			if($user['email_state'] == 1){
				echo "-1";exit;
			}
			// 检查邮箱是否已绑定账号
			$rd = $users->checkUserEmail($mail);
			if($rd['status'] == '-1'){
				echo "-2";exit;
			}
			$code = md5($mail.md5($user['password']));
			$users->updateUser(array('user_id'=>$userId, 'email'=>$mail, 'active_code'=>$code));
		}elseif($action == 'unbind'){
			// 检查是否未绑定
			if($user['email_state'] == 0){
				echo "-1";exit;
			}
			$code = $user['active_code'];
		}
		
		// 获取邮件模板内容
		$system = D('System');
		$data = $system->loadEmailTemplate($template);
		
		$url = CBWDomain().'/Users/bindmail/id/'.$userId.'/action/'.$action.'/code/'.$code;
		$send = CBWSendMail($mail, $data['subject'], $data['content'], 'template', $url);
		echo $send;
	}
	
	/**
     * 绑定/解绑邮箱操作
     */
	public function bindmail(){		
		$userId   = I('get.id');
		$code     = I('get.code');
		$action   = I('get.action');
		
		$users = D('Users');
		$user = $users->getUserAndInfoById($userId);
		
		if($action == 'bind'){
			if($user['active_code'] == $code){
				if($user['email_state'] == '1'){
					$this->error('邮箱已经绑定了,亲还要绑定多一次嘛?', U('Users/security'));
				}else{
					$users->updateUser(array('user_id'=>$userId, 'email_state'=>1));
					$this->success('邮箱绑定成功', U('Users/security'));
				}
			}else{
				$this->error('参数错误,请重新发送绑定邮件.', U('Users/security'));
			}
		}elseif($action == 'unbind'){
			if($user['active_code'] == $code){
				if($user['email_state'] == '0'){
					$this->error('邮箱已经解绑了,亲还要解绑多一次嘛?', U('Users/security'));
				}else{
					if($user['mobile_state'] == '1'){
						$users->updateUser(array('user_id'=>$userId, 'email'=>'', 'email_state'=>0, 'active_code'=>''));
						$this->success('邮箱解绑成功', U('Users/security'));
					}else{
						$this->error('解绑失败,解绑邮箱需要先绑定手机.', U('Users/security'));
					}
				}
			}else{
				$this->error('参数错误,请重新发送绑定邮件.', U('Users/security'));
			}
		}
	}
	
	/**
     * 绑定/解绑手机号码操作
     */
	public function bindmobile(){		
		$mobile   = I('post.mobile');
		$code     = I('post.code');
		$action   = I('post.action');
		
		$users = D('Users');
		$usercode = D('UserCode');
		
		$user   = $users->getUserAndInfoById();
		$mobile = empty($mobile)?$user['mobile']:$mobile;
		$uc = $usercode->check($mobile, $code);   // 验证短信验证码
		if($uc == -1){
			echo json_encode(array('status'=>-1, 'msg'=>'短信验证码超时'));exit;
		}else if($uc == -2){
			echo json_encode(array('status'=>-2, 'msg'=>'短信验证码错误'));exit;
		}
		if($action == 'bind'){
			if($user['mobile_state'] == 1){
				echo json_encode(array('status'=>-3, 'msg'=>'账号已绑定手机'));exit;
			}
			$rd = $users->checkUserMobile($mobile);
			if($rd['status'] == '-1'){
				echo json_encode(array('status'=>-4, 'msg'=>'手机已绑定账号'));exit;
			}
			$rs = $users->updateUser(array('user_id'=>$user['user_id'], 'mobile'=>$mobile, 'mobile_state'=>1));
			if($rs['status'] == 1){
				$usercode->del($mobile);     // 删除短信验证码相关记录
				echo json_encode(array('status'=>1, 'msg'=>'绑定成功'));exit;
			}
		}elseif($action == 'unbind'){
			if($user['email_state'] == 0){
				echo json_encode(array('status'=>-3, 'msg'=>'解绑手机需要先绑定邮箱'));exit;
			}
			$rs = $users->updateUser(array('user_id'=>$user['user_id'], 'mobile'=>'', 'mobile_state'=>0));
			if($rs['status'] == 1){
				$usercode->del($mobile);     // 删除短信验证码相关记录
				echo json_encode(array('status'=>1, 'msg'=>'解绑成功'));exit;
			}
		}
		echo json_encode(array('status'=>0, 'msg'=>'操作失败'));exit;
	}
	
	/**
     * 更新密码
     */
	public function updatepwd(){
		$this->isUserLogin();
		
		$password    = I('post.password');
		$newPassword = I('post.newPassword');
		
		$users = D('Users');
		
		$user   = $users->getUserAndInfoById();
		if($user['password'] == sha1($password)){
			$rs = $users->updatePass($user['id'], $newPassword);
			if($rs['status'] == 1){
				echo json_encode(array('status'=>1, 'msg'=>'修改成功'));exit;
			}else{
				echo json_encode(array('status'=>0, 'msg'=>'修改失败'));exit;
			}
		}else{
			echo json_encode(array('status'=>-1, 'msg'=>'旧密码错误'));exit;
		}
	}
	
	/**
     * 设置支付密码
     */
	public function payPassword(){
		$this->isUserLogin();
		
		$code     = I('post.code');
		$password = I('post.password');
		
		$users = D('Users');
		$usercode = D('UserCode');
		
		$user   = $users->getUserAndInfoById();
		$uc = $usercode->check($user['mobile'], $code);   // 验证短信验证码
		if($uc == -1){
			echo json_encode(array('status'=>-2, 'msg'=>'短信验证码超时'));exit;
		}else if($uc == -2){
			echo json_encode(array('status'=>-3, 'msg'=>'短信验证码错误'));exit;
		}
		$rs = $users->updateUser(array('user_id'=>$user['user_id'], 'pay_password'=>sha1($password)));
		if($rs['status'] == 1){
			$usercode->del($user['mobile']);     // 删除短信验证码相关记录
			echo json_encode(array('status'=>1, 'msg'=>'设置成功'));exit;
		}
		echo json_encode(array('status'=>0, 'msg'=>'设置失败'));exit;
	}
	
	/**
     * 实名认证
     */
	public function auth(){
		$this->isUserLogin();

		$banks = D('Banks');
		$users = D('Users');
		
		$user = $users->getUserAndInfoById($userId);
		if(!empty($user['true_name']) && !empty($user['identity'])){
			$this->error('账号已实名认证');
		}
		
		$deposits = $banks->getBank();
		$credits  = $banks->getBankByCredit();
		
		$this->assign("credits", $credits);
		$this->assign("deposits", $deposits);
		$this->assign("year", date("Y"));
		$this->assign("title", "实名认证");
		$this->display();
	}
	
	/**
     * 好友管理
     */
	public function friends(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$groupId = session('user.groupid');
		($groupId != 1) && $this->error('该用户组权限不足');

		$orders = D('Orders');
		$uw     = D('UserWechat');
		
		$count = $uw->getWechatsCountByUserId($userId);
        $page = new \Think\Page($count, 12);
		$friends = $uw->getWechatsByUserId($userId, $page->firstRow, $page->listRows);
		$pageShow = $page->show();
		foreach($friends as &$vo){
			$money   = 0;
			$harvest = 0;
			$wait    = 0;
			if(!empty($vo['user_id'])){
				$os = $orders->getOrderList(array('userId'=>$vo['user_id'], 'type'=>0, 'status'=>5, 'date'=>$vo['fans_time']));
				foreach($os as $v){
					$money += $v['deail_price']-$v['shipping_fee'];
					$harvest += ($v['deail_price']-$v['shipping_fee'])*0.02;
				}
			}
			$vo['nickname'] = empty($vo['nickname'])?'暂缺':$vo['nickname'];
			$vo['money']    = number_format($money, 2, '.', '');
			$vo['harvest']  = number_format($harvest, 2, '.', '');
		}
		unset($vo);
		$this->assign("count", $count);
		$this->assign("friends", $friends);
		$this->assign('page', $pageShow);
		$this->assign("title", "好友圈");
		$this->display();
	}
	
    /**
     * 分享
     */
	public function share(){
		$this->isUserLogin();
		$dir     = date("Ym");
		$dir2    = date("Ymd");
		$userId  = session('user.userid');
		$shopId  = session('shop.id');
		$imgFile = $dir."/share_".$shopId.".png";
		$groupId = session('user.groupid');
		
		($groupId != 1) && $this->error('该用户组权限不足');
		
		$users = D('Users');
		$us    = D('UserShop');

		$user = $users->getUserAndInfoById($userId);
		$this->assign("user",$user);

		if(!file_exists("./Public/Share/".$imgFile)){
			// 生成公众号关注二维码
			$wechatObj = new \Think\WechatApi();
			for($i=0; $i<100; $i++){
				$qrcode = $wechatObj->qrcodeCreate($shopId, 2);
				if(!empty($qrcode)){
					break;
				}
			}
		
			$image = new \Think\Image();  //图片处理类
			
			$tempFile      = "./Public/User/qrcode.png";                           // 模板文件
			$saveFile      = "./Public/Share/".$dir."/share_".$shopId.".png";      // 保存文件
			$qrcodeFile    = "./Public/Temp/".$dir2."/qrcode_".$shopId.".jpg";     // 二维码文件
			$qrcodeFile240 = "./Public/Temp/".$dir2."/qrcode_".$shopId."_240.jpg"; // 二维码文件240
			
			// 添加二维码
			$image->open($qrcodeFile)->thumb(240, 240, \Think\Image::IMAGE_THUMB_SCALE)->save($qrcodeFile240);
			$image->open($tempFile)->water($qrcodeFile240, array(158,243))->save($saveFile);
			
			// 添加头像
			if(!empty($user['thumb'])){
				$headImg = "./Public/User/".$user['thumb'];
				$image->open($headImg)->thumb(66, 66, \Think\Image::IMAGE_THUMB_SCALE)->save($headImg);
			}else{
				$headImg = "./Public/User/default100.jpg";
			}
			$image->open($saveFile)->water($headImg, array(80,62))->save($saveFile);
			
			//添加昵称
			$image->open($saveFile)->text($user['uname'], './Public/Mobile/font/PingFang Normal.ttf', 20, '#000000', array(175,80))->save($saveFile);
		}
		
		
		$this->assign("imgFile",$imgFile);
		$this->assign("shopId",$shopId);
		$this->assign("dir",$dir);
		$this->assign("title", "分享二维码");
		$this->display();
	}
    
    /**
     * 福利规则
     */
	public function explain(){
		$this->isUserLogin();
		$groupId = session('user.groupid');
		($groupId != 1) && $this->error('该用户组权限不足');
		
		$this->assign("title", "福利规则");
		$this->display();
	}
	
	/**
     * 我的消息
     */
	public function msg(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$type   = I('get.type');
		$type   = empty($type)?0:$type;
		
		$um = D('UserMessage');
		
		$count = $um->getMessageCount($userId, $type);
        $page = new \Think\Page($count, 8);
		$messages = $um->getMessageByUserId($userId, $type, $page->firstRow, $page->listRows);
		$pageShow = $page->show();
		
		$this->assign("messages", $messages);
		$this->assign("page", $pageShow);
		$this->assign("type", $type);
		$this->assign("title", "我的消息");
		$this->display();
	}
	
	/**
     * 删除消息
     */
	public function deleteMessage(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$ids    = I('post.ids');
		
		$um = D('UserMessage');
		$data = $um->deleteMessage($userId, $ids);
		if($data['status'] > 0){
			echo 1;exit;
		}
		echo 0;exit;
	}
	
	/**
     * 消息详情
     */
	public function msgview(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$id     = I('get.id');
		
		$um = D('UserMessage');
		
		$message = $um->getMessageByMsgId($id);
		if($message['is_read']=='0'){
			$um->updateMessage("id=".$message['id'], array('is_read'=>1));
		}
		
		$this->assign("message", $message);
		$this->assign("title", "消息详情");
		$this->display();
	}
}
?>
