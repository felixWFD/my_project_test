<?php
namespace Home\Controller;
/**
 * 代理控制器
 */
class AgentController extends BaseController {
	public function __construct(){
		parent::__construct();
		
		$this->isUserLogin();
	}
	
	/**
	 * 检查是否为代理用户
	 */
	private function checkUserAgent(){
		$userId = session('user.userid');
		
		$users = D('Users');
		
		$user = $users->getUserById($userId);
		if($user['is_agent'] == 0){
			$this->error('非代理,禁止操作', U('Agent/shopadd'));exit;
		}
	}
	
	/**
	 * 我的店铺
	 */
    public function myshop(){
		$userId   = session('user.userid');
		$province = I('get.province');
		$city     = I('get.city');
		$area     = I('get.area');
		$keyword  = I('get.keyword');

		$orders = D('Orders');
		$us     = D('UserShop');

		// 我的店铺
		$shops = $us->getShopsByObject(array('userId'=>$userId, 'province'=>$province, 'city'=>$city, 'area'=>$area, 'keyword'=>$keyword, 'orderby'=>'main'));
		foreach($shops as &$vo){
			$level = CBWShopLevel($vo['exp']); // 等级换算
			$vo['level_image_name'] = $level['name'];
			$vo['level_image_num']  = $level['num'];
			$vo['level_level']      = $level['level'];
			$repairCount      = $orders->getRepairCountByObject(array('shopId'=>$vo['id'], 'status'=>1));
			$recoveryCount    = $orders->getRecoveryCountByObject(array('shopId'=>$vo['id'], 'status'=>1));
			$vo['orderCount'] = $repairCount+$recoveryCount;
		}
		$shops = CBWArraySort($shops,'orderCount',SORT_DESC);
		
		// 三级联动
		$p = empty($province)?'选择省':$province;
		$c = empty($city)?'选择市':$city;
		$a = empty($area)?'选择县/区':$area;
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("shops", $shops);
		$this->assign("keyword", $keyword);
		$this->assign("title", "我的店铺");
		$this->display();
    }
	
	/**
	 * 跳转店铺系统
	 */
	public function jumpSystem(){
		$userId = session('user.userid');
		$shopId = I('get.id');
		
		$us = D('UserShop');
		
		$shop = $us->getShopByShopId($shopId);
		
		if($shop['user_id'] == $userId){
			$ip      = ip2long(I('server.REMOTE_ADDR'));
			$auto    = $shop['id'].'|'.$ip;
			$system  = CBWCheckAutoLogin($auto,1);
			cookie('auto',  $system,            array('prefix' => 'system_', 'expire' => C('AUTO_TIME_LOGIN')));
			cookie('sname', $shop['shop_name'], array('prefix' => 'system_', 'expire' => C('AUTO_TIME_LOGIN')));
			
			// 保存登录SESSION
			session(array('name'=>'shopLogin', 'prefix'=>'shop'));
			session('id',   $shop['id']);
			session('name', $shop['shop_name']);
			
			$this->redirect("System/index");exit;
		}	
		$this->error('跳转失败', U('Agent/myshop'));exit;
	}
	
	/**
	 * 添加店铺页面
	 */
	public function shopadd(){		
		// 三级联动
		$cityArray = array('province'=>'选择省', 'city'=>'选择市', 'area'=>'选择县/区');
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("title", "添加店铺");
		$this->display();
	}
	
	/**
	 * 添加店铺操作
	 */
	public function doAddShop(){
		$userId = session('user.userid');

		$us = D('UserShop');
		
		$data = array();
		
		$shop = $us->getShopByUserId($userId);

		$data['type_id']       = $shop['type_id'];
		$data['logo_image']    = $shop['logo_image'];
		$data['description']   = $shop['description'];
		$data['content']       = $shop['content'];
		$data['notice']        = $shop['notice'];
		$data['open_hours']    = $shop['open_hours'];
		$data['is_week']       = $shop['is_week'];
		$data['server_area']   = $shop['server_area'];
		$data['server_status'] = $shop['server_status'];
		$data['user_id']       = $userId;
		$data['shop_name']     = I('post.shop_name');
		$data['user_name']     = I('post.user_name');
		$data['user_phone']    = I('post.user_phone');
		$data['area_id']       = I('post.area');
		$data['address']       = I('post.address');
		$data['lng']           = I('post.lng');
		$data['lat']           = I('post.lat');
		$data['is_store']      = I('post.is_store');
		$data['is_door']       = I('post.is_door');
		$data['create_shop']   = time();
		$rd = $us->insertUserShop($data);
		if($rd['status'] > 0){
			$this->redirect("Agent/myshop");exit;
		}
		$this->error('添加失败', U('Agent/shopadd'));exit;
	}
	
	/**
	 * 编辑店铺页面
	 */
	public function shopedit(){
		$shopId = I('get.id');

		$us    = D('UserShop');
		$area  = D('Area');
		
		// 店铺信息
		$shop = $us->getShopByShopId($shopId);
		
		// 三级联动
		$data = $area->getAreaById($shop['area_id']);
		$p = empty($data['one_id'])?'选择省':$data['one_id'];
		$c = empty($data['two_id'])?'选择市':$data['two_id'];
		$a = empty($data['id'])?'选择县/区':$data['id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign("shop", $shop);
		$this->assign("title", "编辑店铺");
		$this->display();
	}
	
	/**
	 * 编辑店铺操作
	 */
	public function doEditShop(){
		$shopId = I('post.id');
		
		$us = D('UserShop');
		
		$data = array();
		$data['shop_name']  = I('post.shop_name');
		$data['user_name']  = I('post.user_name');
		$data['user_phone'] = I('post.user_phone');
		$data['area_id']    = I('post.area');
		$data['address']    = I('post.address');
		$data['lng']        = I('post.lng');
		$data['lat']        = I('post.lat');
		$data['is_store']   = I('post.is_store');
		$data['is_door']    = I('post.is_door');
		
		$rt = $us->updateUserShop($shopId, $data);
		if($rt['status'] > 0){
			$this->redirect("Agent/myshop");exit;
		}
		$this->error('修改失败');exit;
	}
	
	/**
	 * 报价通知
	 */
	public function offers(){
		$userId = session('user.userid');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$cat     = D('ProductCat');
		$users   = D('Users');
		$us      = D('UserShop');
		
		$user = $users->getUserById($userId);
		if($user['is_agent'] == 0){
			$this->redirect("System/offering");exit;
		}
		
		$count = $orders->getOrderOfferIngCountByUserId(array('userId'=>$userId));
        $page = new \Think\Page($count, 8);
		$data = $orders->getOrderOfferIngByUserId(array('userId'=>$userId, 'm'=>$page->firstRow, 'n'=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$order = $orders->getOrderById($vo['order_id']);
			$user  = $users->getUserAndInfoById($order['user_id']);
			$vo['order_type'] = $order['order_type'];
			$vo['order_date'] = $order['order_date'];
			$vo['remarks']    = $order['remarks'];
			$vo['uname']      = $user['uname'];
			$vo['thumb']      = $user['thumb'];
			if($order['order_type'] == 1){
				$server = $orders->getOrderServerByOrderId($vo['order_id']);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$color    = $repairs->getColorById($server['color_id']);
					$vo['pro_name']   = $relation['pro_name'];
					$vo['color_name'] = $color['name'];
					$vo['plan_name']  = $relation['plan_name'];
				}
				$shop = $us->getShopByShopId($vo['shop_id']);
				$vo['shop_name'] = $shop['shop_name'];
			}elseif($order['order_type'] == 2){
				$recovery = $orders->getOrderRecoveryByOrderId($vo['order_id']);
				if($recovery['reco_id'] > 0){
					$repair = $repairs->getRepairById($recovery['reco_id']);
					$type   = $cat->getCat($repair['type_id']);
					$brand  = $cat->getCat($repair['brand_id']);
					$vo['pro_name'] = $repair['pro_name'];
					$vo['attrs']    = implode('|', $repairs->getAttribute($recovery['reco_attr']));
				}
				$shop = $us->getShopByShopId($vo['shop_id']);
				$vo['shop_name'] = $shop['shop_name'];
			}
			$vo['offer_count'] = $orders->getOrderOfferCountByOrderId($vo['order_id']);
		}
		unset($vo);

		$this->assign("offers", $data);
		$this->assign('page', $pageShow);
		$this->assign("title", "报价通知");
		$this->display();
	}
	
	/**
	 * 我的订单-维修订单
	 */
	public function repair(){
		$userId  = session('user.userid');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$us      = D('UserShop');

		$count = $orders->getOrderCount(array("agentId"=>$userId, "type"=>1, "keyword"=>$keyword, "start"=>$start, "end"=>$end, "status"=>$status));
        $page = new \Think\Page($count, 8);
		$data = $orders->getOrderList(array("agentId"=>$userId, "type"=>1, "keyword"=>$keyword, "start"=>$start, "end"=>$end, "status"=>$status, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			$server = $orders->getOrderServerByOrderId($vo['id']);
			$shop   = $us->getShopByShopId($server['shop_id']);
			if($server['relation_id'] > 0){
				$relation = $repairs->getRepairByRelationId($server['relation_id']);
				$color    = $repairs->getColorById($server['color_id']);
				$vo['show_name']  = $relation['pro_name']." ".$color['name']."<br />".$relation['plan_name'];
				$vo['show_img']   = '/Public/Repair/thumb/'.$relation['list_image'];
				if(!empty($server['relation_ids'])){
					$ids = explode(",",$server['relation_ids']);
					$plans = array();
					for($i=0;$i<count($ids);$i++){
						$relation = $repairs->getRepairByRelationId($ids[$i]);
						$plans[$i] = $relation['plan_name'];
					}
					$vo['show_name'] = $relation['pro_name']." ".$color['name']."<br />".implode(",",$plans);
				}
			}else{
				$vo['show_name']  = $vo['remarks'];
				$vo['show_img']   = '/Public/Home/images/2016/logo_03.png';
			}
			$vo['server_way'] = $server['server_way'];
			$vo['shop_id']    = $shop['id'];
			$vo['shop_name']  = $shop['shop_name'];
			
			$money = $vo['deail_price'];
			if($server['new_price'] !='0.00'){
				$money = $server['new_price'];
			}
			$vo['show_price'] = $money;
		}
		unset($vo);

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('status', $status);
		$this->assign('end', $end);
		$this->assign('start', $start);
		$this->assign('keyword', $keyword);
		$this->assign("title", "维修订单");
		$this->display();
	}
	
	/**
	 * 我的订单-回收订单
	 */
    public function recovery(){
		$userId  = session('user.userid');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');
		
		$orders  = D('Orders');
		$repairs = D('Repairs');
		$us      = D('UserShop');

		$count = $orders->getOrderCount(array("agentId"=>$userId, "type"=>2, "keyword"=>$keyword, "start"=>$start, "end"=>$end, "status"=>$status));
        $page = new \Think\Page($count, 8);
		$data = $orders->getOrderList(array("agentId"=>$userId, "type"=>2, "keyword"=>$keyword, "start"=>$start, "end"=>$end, "status"=>$status, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		foreach($data as &$vo){
			
			$recovery = $orders->getOrderRecoveryByOrderId($vo['id']);
			$repair   = $repairs->getRepairById($recovery['reco_id']);
			$shop     = $us->getShopByShopId($recovery['server_id']);
			if($recovery['new_price'] !='0.00'){
				$money = $recovery['new_price'];
				$attrs = $repairs->getAttribute($recovery['new_attr']);
			}else{
				$money = $vo['deail_price'];
				$attrs = $repairs->getAttribute($recovery['reco_attr']);
			}
			
			$vo['reco_way']   = $recovery['reco_way'];
			$vo['show_name']  = $repair['pro_name'];
			$vo['show_img']   = '/Public/Repair/thumb/'.$repair['list_image'];
			$vo['shop_id']    = $shop['id'];
			$vo['shop_name']  = $shop['shop_name'];
			$vo['show_desc']  = implode("|",$attrs);
			$vo['show_price'] = $money;
			$vo['harvest']    = 0;
		}
		unset($vo);

		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('status', $status);
		$this->assign('end', $end);
		$this->assign('start', $start);
		$this->assign('keyword', $keyword);
		$this->assign("title", "回收订单");
		$this->display();
    }
	
	/**
	 * 我的订单-购物订单
	 */
    public function shopping(){
		$userId  = session('user.userid');
		$keyword = I('get.keyword');
		$start   = I('get.startdate');
		$end     = I('get.enddate');
		$status  = I('get.status');
		
		$orders   = D('Orders');
		$products = D('Products');
		$us       = D('UserShop');
		
		$count = $orders->getOrderCount(array("agentId"=>$userId, "type"=>0, "keyword"=>$keyword, "start"=>$start, "end"=>$end, "status"=>$status));
        $page = new \Think\Page($count, 8);
		$data = $orders->getOrderList(array("agentId"=>$userId, "type"=>0, "keyword"=>$keyword, "start"=>$start, "end"=>$end, "status"=>$status, "m"=>$page->firstRow, "n"=>$page->listRows));
		$pageShow = $page->show();
		
		foreach($data as &$vo){
			$ops = $orders->getOrderProductsByOrderId($vo['id']);
			$harvest = 0;
			foreach($ops as $k=>$v){
				$vip = $products->getProductAttrByPrice($v['pro_id'],$v['pro_attr'],$v['pro_number'],0,1);
				if($vip['price'] > 0){
					$harvest += $v['pro_price']*$v['pro_number']*0.02;
				}
				$ops[$k]['show_desc'] = $products->getAttrNameByAttrId($v['pro_attr']);
				$ops[$k]['money']     = $v['pro_price']*$v['pro_number'];
			}
			$vo['harvest'] = $harvest;
			$vo['list'] = $ops;
			
			$shop = $us->getShopByUserId($vo['user_id']);
			$vo['shop_name'] = $shop['shop_name'];
		}
		unset($vo);
		
		$this->assign("orders", $data);
		$this->assign('page', $pageShow);
		$this->assign('status', $status);
		$this->assign('end', $end);
		$this->assign('start', $start);
		$this->assign('keyword', $keyword);
		$this->assign("title", "购物订单");
		$this->display();
    }

}