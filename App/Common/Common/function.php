<?php
/**
 * 函数库
 */
	
	/**
	 * 隐藏字符串某些部分,用星号（*）代替
     * @param string  $str     字符串
     * @param string  $type    类型
	 * @return String          返回隐藏后的字符串
	 */
	function hidestr($str, $type='tel'){
		if($type == 'tel'){              // 电话号码
			$IsWhat = preg_match('/(0[0-9]{2,3}[-]?[2-9][0-9]{6,7}[-]?[0-9]?)/i',$str); // 固定电话
			if($IsWhat == 1){
				return preg_replace('/(0[0-9]{2,3}[-]?[2-9])[0-9]{3,4}([0-9]{3}[-]?[0-9]?)/i','$1****$2',$str);
			}else{
				return preg_replace('/(1[3578]{1}[0-9])[0-9]{4}([0-9]{4})/i','$1****$2',$str);
			}
		}elseif($type == 'card'){        // 银行卡号
			$top4  = substr($str, 0, 4);    // 前面4位数字
			$post4 = substr($str, -4);      // 前面4位数字
			return $top4.'********'.$post4; 
		}elseif($type == 'idcard'){      // 身份证号码
			return strlen($str)==15?substr_replace($str,"****",5,7):(strlen($str)==18?substr_replace($str,"****",7,7):"");
		}elseif($type == 'ip'){          // IP地址
			return preg_replace('/((?:\d+\.){3})\d+/', "\\1*", $str);
		}elseif($type == 'email'){       // 电子邮箱
			$emailArray = explode("@", $str); 
			$prevfix = (strlen($emailArray[0]) < 4) ? "" : substr($str, 0, 3); // 邮箱前缀 
			$count = 0; 
			$str = preg_replace('/([\d\w+_-]{0,100})@/', '***@', $str, -1, $count); 
			return $prevfix.$str; 
		}elseif($type == 'name'){        // 姓名
			$hyphenated = array('欧阳','太史','端木','上官','司马','东方',
							'独孤','南宫','万俟','闻人','夏侯','诸葛',
							'尉迟','公羊','赫连','澹台','皇甫','宗政',
							'濮阳','公冶','太叔','申屠','公孙','慕容',
							'仲孙','钟离','长孙','宇文','城池','司徒',
							'鲜于','司空','汝嫣','闾丘','子车','亓官',
							'司寇','巫马','公西','颛孙','壤驷','公良',
							'漆雕','乐正','宰父','谷梁','拓跋','夹谷',
							'轩辕','令狐','段干','百里','呼延','东郭',
							'南门','羊舌','微生','公户','公玉','公仪',
							'梁丘','公仲','公上','公门','公山','公坚',
							'左丘','公伯','西门','公祖','第五','公乘',
							'贯丘','公皙','南荣','东里','东宫','仲长',
							'子书','子桑','即墨','达奚','褚师'); 
			$strLen  = mb_strlen($str, 'utf-8');
			$surname = '';  // 姓氏 
			$name    = '';  // 名字
			if($strLen > 2){
				$preTwoWords = mb_substr($str, 0, 2, 'utf-8');    // 取命名的前两个字,看是否在复姓库中
				if(in_array($preTwoWords, $hyphenated)){
					$surname = "**";                             // $surname = $preTwoWords;
					$name = mb_substr($str, 2, 10, 'utf-8');
				}else{
					$surname = "*";                              // $surname = mb_substr($str, 0, 1, 'utf-8');
					$name = mb_substr($str, 1, 10, 'utf-8');
				}
			}else if($strLen == 2){                               // 全名只有两个字时,以前一个为姓,后一下为名
				$surname = "*";                                  // $surname = mb_substr($str, 0, 1, 'utf-8');
				$name = mb_substr($str, 1, 10, 'utf-8');
			}else{
				$surname = $str;
			}
			return $surname.$name;
		}
		return $str;
	}
	
	/**
      * 截取字符串
      * @param name string|array  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
      * @param string  str        要截取的字符串id
      * @param int start          开始位置，默认从0开始
      * @param int length         截取长度
      * @param string charset     字符编码，默认UTF-8
      * @param boolean suffix      是否在截取后的字符后面显示省略号，默认true显示，false为不显示
      * 模版使用：{$vo.title|msubstr=0,5,'utf-8',false}
      * @return String            返回截取后的字符串
     */
	function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true)  {
		if($suffix && (strlen($str) < $length*3)){
			$suffix=false;
		}

		if(function_exists("mb_substr")){  
			if($suffix){  
				return mb_substr($str, $start, $length, $charset)."...";  
			}else{
				return mb_substr($str, $start, $length, $charset);  
			}
		}elseif(function_exists('iconv_substr')){  
			if($suffix){  
				return iconv_substr($str,$start,$length,$charset)."...";  
			}else{
				return iconv_substr($str,$start,$length,$charset);  
			}
		}  
		
		$re['utf-8']   = "/[x01-x7f]|[xc2-xdf][x80-xbf]|[xe0-xef]
		[x80-xbf]{2}|[xf0-xff][x80-xbf]{3}/"; 
		$re['gb2312'] = "/[x01-x7f]|[xb0-xf7][xa0-xfe]/";  
		$re['gbk']    = "/[x01-x7f]|[x81-xfe][x40-xfe]/";  
		$re['big5']   = "/[x01-x7f]|[x81-xfe]([x40-x7e]|xa1-xfe])/";  
		preg_match_all($re[$charset], $str, $match);  
		$slice = join("",array_slice($match[0], $start, $length));  
		if($suffix) return $slice."…";
		
		return $slice;
	}

    /**
     * 发送短信（阿里云短信）
     * @param $mobile  手机号码
     * @param $code    验证码
     * @return bool    短信发送成功返回true失败返回false
     */
    function sendSmsByAliyun($mobile, $smsParam, $templateCode){

        $params = array ();

		// *** 需用户填写部分 ***

		// fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
		$accessKeyId = 'LTAIcU360wlOEKCx';
        $accessKeySecret = 'nM2TZxtKZD5PyyvnJ1WIagwBXwGwSN';

		// fixme 必填: 短信接收号码
		$params["PhoneNumbers"] = $mobile;

		// fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
		$params["SignName"] = '草包网';

		// fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
		$params["TemplateCode"] = $templateCode;

		// fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
		$params['TemplateParam'] = $smsParam;

		// fixme 可选: 设置发送短信流水号
		//$params['OutId'] = "12345";

		// fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
		//$params['SmsUpExtendCode'] = "1234567";


		// *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
		if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
			$params["TemplateParam"] = json_encode($params["TemplateParam"]);
		}

		// 初始化SignatureHelper实例用于设置参数，签名以及发送请求
		vendor("Aliyun.SignatureHelper");
		$helper = new \SignatureHelper();

		// 此处可能会抛出异常，注意catch
		$content = $helper->request(
			$accessKeyId,
			$accessKeySecret,
			"dysmsapi.aliyuncs.com",
			array_merge($params, array(
				"RegionId" => "cn-hangzhou",
				"Action" => "SendSms",
				"Version" => "2017-05-25",
			))
		);

		return $content;
    } 
	
	/**
	 * 邮件发送
	 * @param  [string] $email        [用户邮箱]
	 * @param  [string] $templates    [模板标识]
	 * @param  [string] $url          [连接地址]
	 * @param  [string] $code         [验证码]
	 * @return [bool]	          [描述]
	 */
	function sendEmail($email, $templates="", $url="", $code=""){
		
		$objEmailConfig = M('sys_config');
		$emailConfig 	= $objEmailConfig->where("cname LIKE 'email_%'")->select();
		$emailInfo 		= CBWConvertArray($emailConfig);

		$smtpserver = $emailInfo['email_serv_addr'];
		$port 		= $emailInfo['email_serv_port'];
		$smtpuser	= $emailInfo['email_serv_uname'];
		$smtppwd	= $emailInfo['email_serv_pwd'];
		$sender		= $emailInfo['email_sender_count'];

		// 开启了邮箱验证
		$objEmail 	= new \Think\Email($smtpserver,$port,true,$smtpuser,$smtppwd);

		// 邮件主题和内容
		$objTmp 	= M('email_templates');			// 查找模板
		$tmpInfo 	= $objTmp->where("ename = '".$templates."'")->find();

		$subject 	= $tmpInfo['subject'];			// 模板中的邮件主题
		$content 	= $tmpInfo['content'];			// 模板中的邮件内容
	
		// 获取邮件发送所需要替换的信息，网站名称和版权
		$sendInfo 	= $objEmailConfig->where("cname in ('sy_webname', 'sy_webcopyright')")->select();
		$sendInfo 	= CBWConvertArray($sendInfo);		// 将二维数组转化为一维
		
		// 自定义邮件发送内容
		$username 	= '您于' . date('Y年m月d分 H时i分s秒') . '操作邮箱账号<strong style="color:#00acff;">' . $email . '</strong>';
		$sy_webname = $sendInfo['sy_webname'];
		$sy_webcopyright = $sendInfo['sy_webcopyright'];
		
		// 邮件内容正则
		$pattern[] = '/{sy_webname}/';
		$pattern[] = '/{sy_webcopyright}/';
		$pattern[] = '/{username}/';
		$replacement[] = $sy_webname;
		$replacement[] = $sy_webcopyright;
		$replacement[] = $username;
		if(!empty($url)){
			$pattern[] = '/{url}/';
			$replacement[] = '<a href="' . $url .'">' . $url . '</a>';
		}
		if(!empty($code)){
			$pattern[] = '/{code}/';
			$replacement[] = $code;
		}

		$newSubject 	= preg_replace($pattern, $replacement, $subject);
		$newContent 	= preg_replace($pattern, $replacement, $content);
		$newContent 	= htmlspecialchars_decode($newContent);
		$send			= $objEmail->sendmail($email,$sender,$newSubject,$newContent,'HTML');
		return $send;
	}
	
	/**
     * 快递100 订阅请求前组织参数
     * @date 2015-12-28
     * @param string $company 快递公司代码
     * @param string $number  快递单号
     * @param string $url     回调地址URL
     * return boolean 
     */
	function kd100($company, $number){
		$param = array(
			'company'     => $company,
			'number'      => $number,
			'from'        => '',
			'to'          => '',
			'callbackurl' => 'https://www.caobao.com/api/kuaidi',
			'salt'        => '',
			'resultv2'    => '1',
		);
		$boolean = postorder($param);
		
		$modelExpress = M('express');
		
		$data['company']   = $company;
		$data['number']    = $number;
		$data['post_time'] = time();
		$data['result']    = $boolean;
		if($modelExpress->create($data)){
			$modelExpress->add();
		}
	}
	
	/**
     * 快递100 订阅请求
     * @date 2015-12-28
     * @param array $param   参数数组 包含 快递公司company、快递单号number、出发地城市from、目的地城市to、回调地址callbackurl、签名salt、是否开通行政区域解析功能resultv2
     * return boolean 
     */
    function postorder($param){
		foreach($param as $k=>$v){
			$$k = $v;
		}
		
		$data = array();
		$data["schema"] = 'json' ;
		$data["param"] = '{"company":"'.$company.'", "number":"'.$number.'","from":"'.$from.'", "to":"'.$to.'", "key":"aVsSWVAG5447", "parameters":{"callbackurl":"'.$callbackurl.'","salt":"'.$salt.'","resultv2":"'.$resultv2.'"}}';

		$url='http://www.kuaidi100.com/poll';

		$o=""; 
		foreach ($data as $k=>$v){
			$o.= "$k=".urlencode($v)."&";		//默认UTF-8编码格式
		}

		$data=substr($o,0,-1);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		return curl_exec($ch);		//返回提交结果，格式与指定的格式一致（true代表成功）
    }
	
/********分割线********分割线********分割线********分割线********分割线********分割线********分割线********分割线********/
/**
* 多个字符串组合
* @param String $pre
* @param array  $chars
* @param array  $arr
* @param int    $number
* @return Array
*/
function CBWCombinationString($pre, $chars, $arr, $number) {
	if(!empty($pre) && strlen($pre) === $number){ 
		$arr[] = $pre;
	}
	if(!empty($chars)) {
		foreach($chars as $char) {
			$tempChars = array();
			foreach($chars as $c) {
				if($c !== $char) { $tempChars[] = $c; }
			}
			if(empty($pre)){
				$tempPre = $char;
			}else{
				$tempPre = $pre.'|'.$char;
			}
			$arr = CBWCombinationString($tempPre, $tempChars, $arr, $number);
		}
	}
	return $arr;
}
/**
* 验证手机号是否正确
* @param number $mobile
* @return blean
*/
function CBWCheckMobile($mobile) {
	if (!is_numeric($mobile)) {
		return false;
	}
	return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
}
/**
  * 遍历某个目录下的所有文件和子文件夹
  * @return Array
 */
function CBWReadAllDir($dir){
	$result = array();
	$handle = opendir($dir);
	if($handle){
		while(($file=readdir($handle)) !== false){
			if($file != '.' && $file != '..'){
				$curPath = $dir. DIRECTORY_SEPARATOR .$file;
				if(is_dir($curPath)){
					$result['dir'][$curPath] = CBWReadAllDir($curPath);
				}else{
					$result['file'][] = $curPath;
				}
			}
		}
		closedir($handle);
	}
	return $result;
}

/**
  * 生成订单号码 
  * @return String
 */
function CBWCreateOrderNumber(){
	$d = time();                      // 获取当前时间的时间戳
	$u = uniqid();                    // 获取以微秒计的当前时间
	$n = substr($u, 7, 13);           // 截取字符串
	$n = str_split($n, 1);            // 把字符串分割到数组中
	$n = array_map('ord', $n);        // 运行ord函数获取获取字符串的ASCII值
	$n = implode(NULL, $n);           // 把数组元素组合为字符串
    $n = substr($n, 0, 8);            // 截取字符串
	return $d.$n;
}

/**
  * 显示某一个时间相当于当前时间在多少秒前，多少分钟前，多少小时前 
  * @param timeInt 时间戳 
  * @param format  时间显示格式
  * @return String
 */
function CBWTimeFormat($timeInt, $format='Y-m-d H:i:s'){ 
    if(empty($timeInt)||!is_numeric($timeInt)||!$timeInt){ 
        return ''; 
    } 
    $d=time()-$timeInt; 
    if($d<0){ 
        return ''; 
    }else{ 
        if($d<60){ 
            return $d.'秒前'; 
        }else{ 
            if($d<3600){ 
                return floor($d/60).'分钟前'; 
            }else{ 
                if($d<86400){ 
                    return floor($d/3600).'小时前'; 
                }else{ 
                    if($d<259200){//3天内 
                        return floor($d/86400).'天前'; 
                    }else{ 
                        return date($format,$timeInt); 
                    } 
                } 
            } 
        } 
    } 
} 

/**
  * 下载远程文件
  * @param string  dst  源文件
  * @param string  path 下载路径
  * @return String      返回文件名
 */	
function CBWDownloadFile($dst, $path) {	
	$d = date('Ym');
	$u = uniqid();

	$_dst = $path.$d.'/'.$u.'.jpg'; // 目标文件

	//判断目的文件夹是否存在? 如果不存在就生成
	CBWFileMake($path.$d);
	//判断目的文件是否存在? 存在不允许进行操作
	if(CBWIsFind($_dst) === TRUE){
		return '';
	}
	
	// 设置运行时间为无限制
    set_time_limit(0);
    
    $dst  = trim($dst);
    $curl = curl_init();
	
    // 设置你需要抓取的URL
    curl_setopt($curl, CURLOPT_URL, $dst);
    // 设置header
    curl_setopt($curl, CURLOPT_HEADER, 0);
    // 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // 运行cURL，请求网页
    $file = curl_exec($curl);
    // 关闭URL请求
    curl_close($curl);
    // 将文件写入获得的数据
    $write = @fopen($_dst, "w");
    if($write == false){
        return '';
    }
    if(fwrite ($write, $file) == false){
        return '';
    }
    if(fclose($write) == false){
        return '';
    }
	return $d.'/'.$u.'.jpg';
}

/**
  * 从姓名中截取姓氏
  * @param string  str  姓名
  * @return String      返回截取后的字符串
 */
function CBWSubSurname($str){  
	$hyphenated = array('欧阳','太史','端木','上官','司马','东方',
						'独孤','南宫','万俟','闻人','夏侯','诸葛',
						'尉迟','公羊','赫连','澹台','皇甫','宗政',
						'濮阳','公冶','太叔','申屠','公孙','慕容',
						'仲孙','钟离','长孙','宇文','城池','司徒',
						'鲜于','司空','汝嫣','闾丘','子车','亓官',
						'司寇','巫马','公西','颛孙','壤驷','公良',
						'漆雕','乐正','宰父','谷梁','拓跋','夹谷',
						'轩辕','令狐','段干','百里','呼延','东郭',
						'南门','羊舌','微生','公户','公玉','公仪',
						'梁丘','公仲','公上','公门','公山','公坚',
						'左丘','公伯','西门','公祖','第五','公乘',
						'贯丘','公皙','南荣','东里','东宫','仲长',
						'子书','子桑','即墨','达奚','褚师'); 
	
	$strLen  = mb_strlen($str, 'utf-8');
	if($strLen > 2){
		$preTwoWords = mb_substr($str, 0, 2, 'utf-8');    // 取命名的前两个字,看是否在复姓库中
		if(in_array($preTwoWords, $hyphenated)){
			$surname = $preTwoWords;
		}else{
			$surname = mb_substr($str, 0, 1, 'utf-8');
		}
	}else if($strLen == 2){                               // 全名只有两个字时,以前一个为姓,后一下为名
		$surname = mb_substr($str, 0, 1, 'utf-8');
	}else{
		$surname = $str;
	}
	return $surname;
}

/**
  * 百度URL提交-主动推送
  * @param urls array  需要提交的url数组  	$urls = array('http://www.example.com/1.html','http://www.example.com/2.html');
  * @return Array      返回推送结果 成功:{"remain":4999998,"success":2,"not_same_site":[],"not_valid":[]}  失败:{"error":401,"message":"token is not valid"}
 */
function CBWBaiduPush($urls, $type='www'){

	//old  $api = 'http://data.zz.baidu.com/urls?site=www.caobao.com&token=epUaXUzD5J8OOa5t&type=original';
	if($type == 'www'){
		$url = 'www.caobao.com';
	}elseif($type == 'm'){
		$url = 'm.caobao.com';
	}elseif($type == 'bbs'){
		$url = 'bbs.caobao.com';
	}
	$api = 'http://data.zz.baidu.com/urls?site='.$url.'&token=4br55vBaRoLyK1bZ';
	$ch = curl_init();
	$options =  array(
		CURLOPT_URL => $api,
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POSTFIELDS => implode("\n", $urls),
		CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
	);
	curl_setopt_array($ch, $options);
	$result = curl_exec($ch);
	//echo $result;
}
	
/**
 * 导出EXCEL
 * @param  [string] $expTitle     文件名称
 * @param  [Array]  $expCellName  栏目    $xlsCell  = array(array('id','账号序列'),array('name','姓名'));   
 * @param  [Array]  $expTableData 数据    
 * @print
 */
function CBWExportExcel($expTitle,$expCellName,$expTableData){
	$xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
	$fileName = $expTitle.date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
	$cellNum = count($expCellName);
	$dataNum = count($expTableData);

	vendor("PHPExcel.PHPExcel");
		
	$objPHPExcel = new \PHPExcel();
	$cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');

	$objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  Export time:'.date('Y-m-d H:i:s'));
	for($i=0;$i<$cellNum;$i++){
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i][1]);
	}
	// Miscellaneous glyphs, UTF-8
	for($i=0;$i<$dataNum;$i++){
		for($j=0;$j<$cellNum;$j++){
			$objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$expCellName[$j][0]]);
		}
	}

	header('pragma:public');
	header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
	header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
}

/**
 * 用户安全等级
 * @date 2015-11-19
 * @param string $data 数组
 * return int
 */
function CBWSecurityLevel($data){
	$level = 0;
	if(!empty($data['true_name']) && !empty($data['identity'])){
		$level++;
	}
	if($data['mobile_state'] == '1'){
		$level++;
	}
	if($data['email_state'] == '1'){
		$level++;
	}
	if($data['pwd_level'] > 3){
		$level++;
	}
	if(!empty($data['pay_password'])){
		$level++;
	}
	return $level;
}
	
/**
 * 关键词加链接
 * @date 2015-11-19
 * @param string $html 内容
 * return string
 */
function CBWKeywordAddLink($html, $type='www'){

	$keywords = D('Keywords');
	$keys = $keywords->getKeywords();
	$i = 0;
	foreach($keys as $key=>$vo){
		$pos = strpos($html, $vo['keyword']);
		if($pos > 0){
			$i++;
		}
		if($type == 'm'){
			$links = preg_replace('/www/sui', 'm' , $vo['links'], 1);
			$links = '<a target="_blank" href="'. $links.'" title="'.$vo['keyword'].'">'.$vo['keyword'].'</a>';
		}else{
			$links = '<a target="_blank" href="'. $vo['links'].'" title="'.$vo['keyword'].'">'.$vo['keyword'].'</a>';
		}
		$html = preg_replace('/'.$vo['keyword'].'/sui', $links , $html, 1); 
		if($i > 4){
			break;
		}
	}
	return $html;
}

/**
 * 更新图片属性  
 * @date 2015-11-19
 * @param string $title 标题
 * @param string $html  内容
 * return string
 */
function CBWImageUpdateAttribute($title , $html){		
	/*$content = preg_replace('/<img.+?src=\"(.+?)\".+?>/','<img src="\1">',$content);     //去除src以外所有属性*/
	$html = str_replace('alt=""', 'alt="'.$title.'" title="'.$title.'" ', $html);  //img追加alt 和 title 属性
	return $html;
}

/**
 * 生成任意长度随机数
 * @param int $length 长度
 * @return hash  随机数
 */
function CBWGetRandom($length = 6){
	$chars = '0123456789';
	$hash = '';
	$max = strlen($chars) - 1;
	for($i = 0; $i < $length; $i++) {
		$hash .= $chars[mt_rand(0, $max)];
	}
	$hash = preg_replace('/^0+/','',$hash);
	if($hash < 100000){
		$hash = str_pad($hash,$length,'0',STR_PAD_RIGHT);
	}
	return $hash;
}

/**
 * base64编码的图片上传到服务器
 * @param  string  $base64 图片流
 * @param  string  $url    上传路径
 * @return data
 */
function CBWBase64Upload($base64, $url) {
	$date = date('Ym')."/";
	$imgUrl = $url.$date;
	CBWFileMake($imgUrl);  // 检查路径文件夹是否存在,不存在则创建
	if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)){
		$type = $result[2];
		if(in_array($type,array('pjpeg','jpeg','jpg','gif','bmp','png'))){
			$name = uniqid().rand(100,999).'.'.$type;
			$image_file = $imgUrl.$name;
			//服务器文件存储路径
			if (file_put_contents($image_file, base64_decode(str_replace($result[1], '', $base64)))){
				return $date.$name;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}

/**
 * 移动文件
 * @param  array  源文件夹数组: 简单处理：采用文件名作为元素值
 * @param  string 目的文件夹
 * @return data
 */
function CBWMoveFile($src = array(), $dst = '') {
	if(!is_array($src)){
		$src = array($src);
	}
	//判断源文件是否存在?
	foreach($src as $val) {
		if(CBWIsFind($val) === FALSE){
			return false;
		}
	}
	//判断目的文件夹是否存在? 如果不存在就生成
	if(CBWIsFindDir($dst) === FALSE){
		CBWFileMake($dst);
	}
	// 移动
	foreach($src as $val){
		$_dst = $dst.'/'.basename($val);
		//判断目的文件是否存在? 存在不允许进行操作
		if(CBWIsFind($_dst) === TRUE){
			return false;
		}elseif(strpos($dst, $val) === 0){
			return false;
		}
		if(!rename($val, $_dst)){
			return false;
		} 
	}
	return _dst;
}

/**
 * 判断文件是否存在
 * @param  string  $filename 
 * @return data
 */
function CBWIsFind($filename){
	return @file_exists($filename);
}

/**
 * 判断文件夹是否存在
 * @param  string  $filename 
 * @return data
 */
function CBWIsFindDir($dir){
	$ls = scandir(dirname(__FILE__));
	foreach($ls as $val){
		if($val == $dir) return true;
	}
	return false;
}

/**
 * 检查路径文件夹是否存在,不存在则创建
 * @param  string  $folder   路径
 * @return data
 */
function CBWFileMake($folder){
	$reval = false;
    if(!file_exists($folder)){
        @umask(0);
        preg_match_all('/([^\/]*)\/?/i', $folder, $atmp);
        $base = ($atmp[0][0] == '/') ? '/' : '';
        foreach($atmp[1] AS $val){
            if('' != $val){
                $base .= $val;
                if ('..' == $val || '.' == $val){
                    $base .= '/';
                    continue;
                }
            }else{
                continue;
            }
            $base .= '/';
			
            if(!file_exists($base)){
                if(@mkdir(rtrim($base, '/'), 0777)){
                    @chmod($base, 0777);
                    $reval = true;
                }
            }
        }
    }else{
        $reval = is_dir($folder);
    }
    clearstatcache();
    return $reval;
}

/**
  * 重新组织金额个位数, 小于5清理, 否则进1
  * @param $money 金额
  * @return String
 */	
function CBWNewMoney($money){
	$money_array = explode(".",$money);
	$money_array = str_split($money_array[0]);
	$last_num = $money_array[count($arr)-1];
	$last_two = true;
	if($last_num < 5 && $last_num != 0){
		$money_array[count($money_array)-1] = 0;
		$last_two = false;
	}else{
		$money_array[count($money_array)-1] = 0;
	}
	if($last_two){
		for($i=0;$i<count($money_array);$i++){
			$new_money.=$money_array[$i];
		}
	}else{
		for($i=0;$i<count($money_array);$i++){
			$new_money.=$money_array[$i];
		}
		$new_money = $new_money+10;
	}
	return number_format($new_money,2,".","");
}

/**
  * 维修店等级 计算
  * @param $score  分数
  * @return String
 */
function CBWShopLevel($score){
	$data = array(
		0 =>  array('min'=>0,      'max'=>10,         'name'=>'rankicon1', 'num'=>1, 'level'=>1),   // 2
		1 =>  array('min'=>10,     'max'=>20,         'name'=>'rankicon1', 'num'=>2, 'level'=>2),
		2 =>  array('min'=>20,     'max'=>40,         'name'=>'rankicon1', 'num'=>3, 'level'=>3),
		3 =>  array('min'=>40,     'max'=>80,         'name'=>'rankicon1', 'num'=>4, 'level'=>4),
		4 =>  array('min'=>80,     'max'=>160,        'name'=>'rankicon1', 'num'=>5, 'level'=>5),
		
		5 =>  array('min'=>160,    'max'=>290,        'name'=>'rankicon2', 'num'=>1, 'level'=>6),   // 1.8
		6 =>  array('min'=>290,    'max'=>520,        'name'=>'rankicon2', 'num'=>2, 'level'=>7),
		7 =>  array('min'=>520,    'max'=>940,        'name'=>'rankicon2', 'num'=>3, 'level'=>8),
		8 =>  array('min'=>940,    'max'=>1700,       'name'=>'rankicon2', 'num'=>4, 'level'=>9),
		9 =>  array('min'=>1700,   'max'=>3060,       'name'=>'rankicon2', 'num'=>5, 'level'=>10),
		
		10 => array('min'=>3060,   'max'=>4900,       'name'=>'rankicon3', 'num'=>1, 'level'=>11),  // 1.6
		11 => array('min'=>4900,   'max'=>7840,       'name'=>'rankicon3', 'num'=>2, 'level'=>12),
		12 => array('min'=>7840,   'max'=>12540,      'name'=>'rankicon3', 'num'=>3, 'level'=>13),
		13 => array('min'=>12540,  'max'=>20060,      'name'=>'rankicon3', 'num'=>4, 'level'=>14),
		14 => array('min'=>20060,  'max'=>32100,      'name'=>'rankicon3', 'num'=>5, 'level'=>15),
		
		15 => array('min'=>32100,  'max'=>44940,      'name'=>'rankicon4', 'num'=>1, 'level'=>16),  // 1.4
		16 => array('min'=>44940,  'max'=>62920,      'name'=>'rankicon4', 'num'=>2, 'level'=>17),
		17 => array('min'=>62920,  'max'=>88090,      'name'=>'rankicon4', 'num'=>3, 'level'=>18),
		18 => array('min'=>88090,  'max'=>123330,     'name'=>'rankicon4', 'num'=>4, 'level'=>19),
		19 => array('min'=>123330, 'max'=>172660,     'name'=>'rankicon4', 'num'=>5, 'level'=>20),
		
		20 => array('min'=>172660, 'max'=>207190,     'name'=>'rankicon5', 'num'=>1, 'level'=>21),  // 1.2
		21 => array('min'=>207190, 'max'=>248630,     'name'=>'rankicon5', 'num'=>2, 'level'=>22),
		22 => array('min'=>248630, 'max'=>298360,     'name'=>'rankicon5', 'num'=>3, 'level'=>23),
		23 => array('min'=>298360, 'max'=>358030,     'name'=>'rankicon5', 'num'=>4, 'level'=>24),
		24 => array('min'=>358030, 'max'=>1000000000, 'name'=>'rankicon5', 'num'=>5, 'level'=>25)
	);
	foreach($data as $level){
		if($score >= $level['min'] && $score < $level['max']){
			return $level; 
		} 
	}
	return $data[0]; 
}

/**
 * 获取指定月份的第一天开始和最后一天结束的时间戳
 *
 * @param int $year 年份 $month 月份
 * @return array(本月开始时间，本月结束时间)
 */
function CBWMFristAndLast($month,$year=""){
	if($month=="") $month=date("m");
	if($year=="") $year=date("Y");
	
	$month=sprintf("%02d",intval($month));
	$year=str_pad(intval($year),4,"0",STR_PAD_RIGHT);

	$month>12||$month<1?$month=1:$month=$month;
	$firstday=strtotime($year.$month."01000000");
	$firstdaystr=date("Y-m-01",$firstday);
	$lastday = strtotime(date('Y-m-d 23:59:59', strtotime("$firstdaystr +1 month -1 day")));
	return array("firstday"=>$firstday,"lastday"=>$lastday);
}
	
/**
 * 二维数组排序
 * @date 2015-01-14
 * @param Array $arrays 数据
 * @param string $sortKey 需要排序的数组的Key
 * @param string $sortOrder 排序规则
 * @param string $sortType 字段类型
 */
function CBWArraySort($arrays,$sortKey,$sortOrder=SORT_DESC,$sortType=SORT_NUMERIC ){   
	if(is_array($arrays)){   
		foreach ($arrays as $array){   
			if(is_array($array)){   
				$keyArrays[] = $array[$sortKey];   
			}else{   
				return false;   
			}   
		}   
	}else{   
		return false;   
	}  
	array_multisort($keyArrays,$sortOrder,$sortType,$arrays);   
	return $arrays;   
}

/**
 * 将二维数组置换为一维
 * @param  [array] $arr [需要转换的二维数组]
 * @return [array]      [转换后的一维数组]
 */
function CBWConvertArray($arr){
	if ( ! is_array($arr)){
		return false;
	}
	$newArr = array();
	foreach ($arr as $key=>$val){
		$newKey = $val['cname'];
		$newArr[$newKey] = $val['cvalue'];
	}
	return $newArr;
}

/**
 * 将stdClass Object转array
 * @param  [array] $arr [需要转换的二维数组]
 * @return [array]      [转换后的一维数组]
 */
function CBWOjectArray($array) {
	if(is_object($array)) { 
		$array = (array)$array; 
	} 
	if(is_array($array)) { 
		foreach($array as $key=>$value) { 
			$array[$key] = CBWOjectArray($value); 
		} 
	} 
	return $array;
}
	
/**
 * 密码强度计算
 * @param  [string] $password    登录密码
 * @return int
 */
function CBWPasswordLevel($password){ 
	$score = 0; 
	if(preg_match("/[0-9]+/",$password)) { 
	  $score ++; 
	} 
	if(preg_match("/[0-9]{3,}/",$password)) { 
	  $score ++; 
	} 
	if(preg_match("/[a-z]+/",$password)) { 
	  $score ++; 
	} 
	if(preg_match("/[a-z]{3,}/",$password)) { 
	  $score ++; 
	} 
	if(preg_match("/[A-Z]+/",$password)) { 
	  $score ++; 
	} 
	if(preg_match("/[A-Z]{3,}/",$password)) { 
	  $score ++; 
	} 
	if(preg_match("/[_|\-|+|=|*|!|@|#|$|%|^|&|(|)]+/",$password)) { 
	  $score += 2; 
	} 
	if(preg_match("/[_|\-|+|=|*|!|@|#|$|%|^|&|(|)]{3,}/",$password)) { 
	  $score ++ ; 
	} 
	if(strlen($password) >= 6) { 
	  $score ++; 
	} 
	return $score; 
}
	
/**
 * 抓取内容第一张图片 作为列表图
 * @date 2015-11-18
 * @param string $html 内容
 * return string
 */
function CBWGrabImages($html){
	preg_match_all("/<img.*>/isU", $html, $ereg); // 正则表达式把图片的整个都获取出来了
	$img = $ereg[0][0];                         // 图片
	$p = '#src=("|")(.*)("|")#isU';             // 正则表达式
	preg_match_all($p, $img, $img1);
	$img_path = $img1[2][0];                    // 获取第一张图片路径
	if(!$img_path){
		$img_path = C('SITE_URL')."/Public/Home/images/coins/nopic.jpg";
	}//如果不存在图片，用默认的nopic.jpg替换 */
	return $img_path;
}
	
/**
 * base64_encode加密/解密
 * @return void
 */	
function CBWCheckAutoLogin($value, $type=0){
	$key= md5(C('AUTO_KEYWORDS'));
	if($type){
		return base64_encode($key^$value);
	}
	$value = base64_decode($value);
	return $key^$value;
}

/**
 * 计算某个经纬度的周围某段距离的正方形的四个点
 *
 * @param lng float 经度
 * @param lat float 纬度
 * @param distance float 该点所在圆的半径，该圆与此正方形内切，默认值为6千米
 * @return array 正方形的四个点的经纬度坐标
 * $squares = CBWSquarePoint($lng, $lat);
 * $info_sql = "select id,locateinfo,lat,lng from `lbs_info` where lat<>0 and lat>{$squares['right-bottom']['lat']} and lat<{$squares['left-top']['lat']} and lng>{$squares['left-top']['lng']} and lng<{$squares['right-bottom']['lng']} "; 
 *
 */
function CBWSquarePoint($lng, $lat, $distance = 10){
	$radius = 6378.137;   // 地球半径
	$dlng   = 2 * asin(sin($distance / (2 * $radius)) / cos(deg2rad($lat)));
	$dlng   = rad2deg($dlng);
	$dlat   = $distance/$radius;
	$dlat   = rad2deg($dlat);

	return array(
	'left-top'    =>array('lat'=>$lat + $dlat, 'lng'=>$lng - $dlng),
	'right-top'   =>array('lat'=>$lat + $dlat, 'lng'=>$lng + $dlng),
	'left-bottom' =>array('lat'=>$lat - $dlat, 'lng'=>$lng - $dlng),
	'right-bottom'=>array('lat'=>$lat - $dlat, 'lng'=>$lng + $dlng)
	);
}

/**
 *  计算两组经纬度坐标之间的距离
 *   params ： lng1 经度1；lat1 纬度1； lng2 经度2； lat2 纬度2
 *   return int
 */
function CBWGetDistance($lng1, $lat1, $lng2, $lat2){
	$radius  = 6378.137;              // 地球半径，假设地球是规则的球体
	$radLat1 = $lat1 * PI()/ 180.0;   // PI()圆周率
	$radLat2 = $lat2 * PI() / 180.0;
	$a = $radLat1 - $radLat2;
	$b = ($lng1 * PI() / 180.0) - ($lng2 * PI() / 180.0);
	$s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
	$s = $s * $radius;
	$s = round($s * 1000);
	return $s;
}

function CBWConvertNumber($number){
	if($number > 1000){
		return round($number/1000, 1).'公里';
	}
	return $number.'米';
}

/**
 * 判断是否微信端访问
 */	
function CBWUserAgent() {
	$agent   = strtolower($_SERVER['HTTP_USER_AGENT']);
	$weixin  = (strpos($agent, 'micromessenger')) ? true : false;
	$iphone  = (strpos($agent, 'iphone')) ? true : false;
	$ipad    = (strpos($agent, 'ipad')) ? true : false;
	$android = (strpos($agent, 'android')) ? true : false;
	$i       = 0;
	if($android){  
		$i = 3;
	}
	if($iphone || $ipad){  
		$i = 2;
	}
	if($weixin){
		$i = 1;
	}
	return $i;
}

/**
 * 判断是否手机访问
 */
function CBWIsMobile() {
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
	if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
		return true;
 
	//此条摘自TPM智能切换模板引擎，适合TPM开发
	if(isset ($_SERVER['HTTP_CLIENT']) &&'PhoneClient'==$_SERVER['HTTP_CLIENT'])
		return true;
	//如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
	if (isset ($_SERVER['HTTP_VIA']))
		//找不到为flase,否则为true
		return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
	//判断手机发送的客户端标志,兼容性有待提高
	if (isset ($_SERVER['HTTP_USER_AGENT'])) {
		$clientkeywords = array(
			'nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile'
		);
		//从HTTP_USER_AGENT中查找手机浏览器的关键字
		if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
			return true;
		}
	}
	//协议法，因为有可能不准确，放到最后判断
	if (isset ($_SERVER['HTTP_ACCEPT'])) {
		// 如果只支持wml并且不支持html那一定是移动设备
		// 如果支持wml和html但是wml在html之前则是移动设备
		if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
			return true;
		}
	}
	return false;
}

/**
 * 邮件发送函数
 * @param string to      要发送的邮箱地址
 * @param string subject 邮件标题
 * @param string content 邮件内容
 * @param string type    邮件类型   template=模板  text=文本
 * @param string url     连接地址
 * @param string code    验证码
 * @return [bool]	     描述
 */
function CBWSendMail($to, $subject, $content="", $type="", $url="", $code=""){
	
	// 邮件服务器配置
	$smtpserver = $GLOBALS['CONFIG']['email_serv_addr'];
	$port 		= $GLOBALS['CONFIG']['email_serv_port'];
	$smtpuser	= $GLOBALS['CONFIG']['email_serv_uname'];
	$smtppwd	= $GLOBALS['CONFIG']['email_serv_pwd'];
	$sender		= $GLOBALS['CONFIG']['email_sender_count'];

	// 开启了邮箱验证
	$mail = new \Think\Email($smtpserver,$port,true,$smtpuser,$smtppwd);
	
	if($type == 'template'){
		// 自定义邮件发送内容
		$username 	     = '您于' . date('Y年m月d分 H时i分s秒') . '操作邮箱账号<strong style="color:#00acff;">'.$to.'</strong>';
		$sy_webname      = $GLOBALS['CONFIG']['sy_webname'];
		$sy_webcopyright = $GLOBALS['CONFIG']['sy_webcopyright'];
		
		// 邮件内容正则
		$pattern[] = '/{sy_webname}/';
		$pattern[] = '/{sy_webcopyright}/';
		$pattern[] = '/{username}/';
		$replacement[] = $sy_webname;
		$replacement[] = $sy_webcopyright;
		$replacement[] = $username;
		if(!empty($url)){
			$pattern[] = '/{url}/';
			$replacement[] = '<a href="'.$url.'">'.$url.'</a>';
		}
		if(!empty($code)){
			$pattern[] = '/{code}/';
			$replacement[] = $code;
		}

		$subject = preg_replace($pattern, $replacement, $subject);
		$content = preg_replace($pattern, $replacement, $content);
		$content = htmlspecialchars_decode($content);
	}
	return $mail->sendmail($to,$sender,$subject,$content,'HTML');
}

/**
 * 字符串替换
 * @param string $str     要替换的字符串
 * @param string $repStr  即将被替换的字符串
 * @param int $start      要替换的起始位置,从0开始
 * @param string $splilt  遇到这个指定的字符串就停止替换
 */
function CBWStrReplace($str,$repStr,$start,$splilt = ''){
	$newStr = substr($str,0,$start);
	$breakNum = -1;
	for ($i=$start;$i<strlen($str);$i++){
		$char = substr($str,$i,1);
		if($char==$splilt){
			$breakNum = $i;
			break;
		}
		$newStr.=$repStr;
	}
	if($splilt!='' && $breakNum>-1){
		for ($i=$breakNum;$i<strlen($str);$i++){
			$char = substr($str,$i,1);
			$newStr.=$char;
		}
	}
	return $newStr;
}

/**
 * 循环删除指定目录下的文件及文件夹
 * @param string $dirpath 文件夹路径
 */
function CBWDelDir($dirpath){
	$dh=opendir($dirpath);
	while (($file=readdir($dh))!==false) {
		if($file!="." && $file!="..") {
		    $fullpath=$dirpath."/".$file;
		    if(!is_dir($fullpath)) {
		        unlink($fullpath);
		    } else {
		        CBWDelDir($fullpath);
		        rmdir($fullpath);
		    }
	    }
	}	 
	closedir($dh);
    $isEmpty = 1;
	$dh=opendir($dirpath);
	while (($file=readdir($dh))!== false) {
		if($file!="." && $file!="..") {
			$isEmpty = 0;
			break;
		}
	}
	return $isEmpty;
}

/**
 * 获取网站域名
 */
function CBWDomain(){
	$server = $_SERVER['HTTP_HOST'];
	$http = is_ssl()?'https://':'http://';
	return $http.$server.__ROOT__;
}

/**
 * 获取系统根目录
 */
function CBWRootPath(){
	return dirname(dirname(dirname(dirname(__File__))));
}

/**
 * 获取网站根域名
 */
function CBWRootDomain(){
	$server = $_SERVER['HTTP_HOST'];
	$http = is_ssl()?'https://':'http://';
	return $http.$server;
}

/**
 * 生成缓存文件
 */
function CBWDataFile($name, $path = '',$data=array()){
	$key = C('DATA_CACHE_KEY');
	$name = md5($key.$name);
	if(is_array($data) && !empty($data)){
	    $data   =   serialize($data);
        if( C('DATA_CACHE_COMPRESS') && function_exists('gzcompress')) {
            //数据压缩
            $data   =   gzcompress($data,3);
        }
        if(C('DATA_CACHE_CHECK')) {//开启数据校验
            $check  =  md5($data);
        }else {
            $check  =  '';
        }
        $data    = "<?php\n//".sprintf('%012d',$expire).$check.$data."\n?>";
        $result  =   file_put_contents(DATA_PATH.$path.$name.".php",$data);
	    clearstatcache();
	}else if(is_null($data)){
	    unlink(DATA_PATH.$path.$name.".php");
	}else{
		if(file_exists(DATA_PATH.$path.$name.'.php')){
		    $content    =   file_get_contents(DATA_PATH.$path.$name.'.php');
            if( false !== $content) {
	            $expire  =  (int)substr($content,8, 12);
	            if(C('DATA_CACHE_CHECK')) {//开启数据校验
	                $check  =  substr($content,20, 32);
	                $content   =  substr($content,52, -3);
	                if($check != md5($content)) {//校验错误
	                    return null;
	                }
	            }else {
	            	$content   =  substr($content,20, -3);
	            }
	            if(C('DATA_CACHE_COMPRESS') && function_exists('gzcompress')) {
	                //启用数据压缩
	                $content   =   gzuncompress($content);
	            }
	            $content    =   unserialize($content);
	            return $content;
	        }
		}
		return null;
	}
}

/**
 * 建立文件夹
 * @param string $aimUrl
 * @return viod
 */
function CBWCreateDir($aimUrl) {
	$aimUrl = str_replace('', '/', $aimUrl);
	$aimDir = '';
	$arr = explode('/', $aimUrl);
	$result = true;
	foreach ($arr as $str) {
		$aimDir .= $str . '/';
		if (!file_exists_case($aimDir)) {
			$result = mkdir($aimDir,0777);
		}
	}
	return $result;
}

/**
 * 建立文件
 * @param string $aimUrl
 * @param boolean $overWrite 该参数控制是否覆盖原文件
 * @return boolean
 */
function CBWCreateFile($aimUrl, $overWrite = false) {
	if (file_exists_case($aimUrl) && $overWrite == false) {
		return false;
	} elseif (file_exists_case($aimUrl) && $overWrite == true) {
		CBWUnlinkFile($aimUrl);
	}
	$aimDir = dirname($aimUrl);
	CBWCreateDir($aimDir);
	touch($aimUrl);
	return true;
}

/**
 * 删除文件
 * @param string $aimUrl
 * @return boolean
 */
function CBWUnlinkFile($aimUrl) {
	if (file_exists_case($aimUrl)) {
		unlink($aimUrl);
		return true;
	} else {
		return false;
	}
}

/**
 * 添加日志文件内容
 * @param string $aimUrl
 * @return boolean
 */
function  CBWLog($filepath,$word){
	if(!file_exists_case($filepath)){
		CBWCreateFile($filepath);
	}
	$fp = fopen($filepath,"a");
	flock($fp, LOCK_EX) ;
	fwrite($fp,$word);
	flock($fp, LOCK_UN);
	fclose($fp);
}

/**
 * 读取Excel文件
 * @param string $aimUrl
 * @return boolean
 */
function CBWReadExcel($file){
	Vendor("PHPExcel.PHPExcel");
	Vendor("PHPExcel.PHPExcel.IOFactory");
	return PHPExcel_IOFactory::load(CBWRootPath()."/Upload/".$file);
}

/**
 * 处理转义字符
 * @param $str 需要处理的字符串
 */
function CBWAddslashes($str){
	if (!get_magic_quotes_gpc()){
		if (!is_array($str)){
			$str = addslashes($str);
		}else{
			foreach ($str as $key => $val){
				$str[$key] = CBWAddslashes($val);
			}
		}
	}
	return $str;
}

/**
 * 检测字符串是否包含
 * @param $srcword 被检测的字符串
 * @param $filterWords 禁用使用的字符串列表
 * @return boolean true-检测到,false-未检测到
 */
function CBWCheckFilterWords($srcword,$filterWords){
	$flag = true;
	$filterWords = str_replace("，",",",$filterWords);
	$words = explode(",",$filterWords);
	for($i=0;$i<count($words);$i++){
		if(strpos($srcword,$words[$i]) !== false){
			$flag = false;
			break;
		}
	}
	return $flag;
}

/**
 * 比较两个日期相差的天数
 * @param $date1 开始日期  Y-m-d
 * @param $date2 结束日期  Y-m-d
 */
function CBWCompareDate($date1,$date2){
	$time1 = strtotime($date1);
	$time2 = strtotime($date2);
	return ceil(($time1-$time2)/86400);
}

/**
 * 截取字符串
 */
function CBWMSubstr($str, $start = 0, $length, $charset = "utf-8", $suffix = true) {
	$newStr = '';
	if (function_exists ( "mb_substr" )) {
		if ($suffix)
			$newStr = mb_substr ( $str, $start, $length, $charset );
		else
			$newStr = mb_substr ( $str, $start, $length, $charset );
	} elseif (function_exists ( 'iconv_substr' )) {
		if ($suffix)
			$newStr = iconv_substr ( $str, $start, $length, $charset );
		else
			$newStr = iconv_substr ( $str, $start, $length, $charset );
	}
	if($newStr==''){
	$re ['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
	$re ['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
	$re ['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
	$re ['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
	preg_match_all ( $re [$charset], $str, $match );
	$slice = join ( "", array_slice ( $match [0], $start, $length ) );
	if ($suffix)
		$newStr = $slice;
	}
	return (strlen($str)>strlen($newStr))?$newStr."...":$newStr;
}

/**
 * 获取当前毫秒数
 */
function CBWGetMillisecond(){
	$time  = explode(" ", microtime());
	$time  = $time[1].($time[0] * 1000);
	$time2 = explode(".", $time);
	$time  = $time2[0];
	return $time;
}

/**
 * 格式化查询语句中传入的in 参与，防止sql注入
 * @param unknown $split
 * @param unknown $str
 */
function CBWFormatIn($split,$str){
	$strdatas = explode($split,$str);
	$data = array();
	for($i=0;$i<count($strdatas);$i++){
		$data[] = (int)$strdatas[$i];
	}
	$data = array_unique($data);
	return implode($split,$data);
}

/**
 * 获取上一个月或者下一个月份 1:下一个月,其他值为上一个月
 * @param int $sign default 1
 */
function CBWMonth($sign=1,$month = ''){
	$tmp_year=date('Y');  
	$tmp_mon =date('m'); 
    $tmp_nextmonth=mktime(0,0,0,$tmp_mon+1,1,$tmp_year);  
    $tmp_forwardmonth=mktime(0,0,0,$tmp_mon-1,1,$tmp_year);  
    if($sign==1){  
        //得到当前月的下一个月   
        return $fm_next_month=date("Y-m",$tmp_nextmonth);          
    }else{  
        //得到当前月的上一个月   
        return $fm_forward_month=date("Y-m",$tmp_forwardmonth);           
    }  
} 


/**
 * 高精度数字相加
 * @param $num
 * @param number $i 保留小数位
 */
function CBWBCMoney($num1,$num2,$i=2){
	$num = bcadd($num1, $num2, $i);
	return (float)$num;
}

/**
 * 获取 IP  地理位置
 * 淘宝IP接口
 * @Return: array
 */
function getCityByTaoBaoApi($ip = '')
{
    if($ip == ''){
       return false;
    }else{
        $url="http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
        $ip=json_decode(file_get_contents($url));
        if((string)$ip->code=='1'){
            return false;
        }
        $data = (array)$ip->data;
    }
    return $data;
}
/**
 * 腾讯验证码验证
 * 请求接口返回内容
 * @param  string $url [请求的URL地址]
 * @param  string $params [请求的参数]
 * @param  int $ipost [是否采用POST形式] 0:get 其他数字： post（默认）
 * @param  int $byPass [是否绕过ssl验证] 1：绕过  其他数字： 不绕过（默认）
 * @return  string
 */
function verifyTencentImgCode($params=false,$isPost=1,$byPass=1){
    if(!$params) return false;
    $url = "https://ssl.captcha.qq.com/ticket/verify";
    $httpInfo = array();
    $ch = curl_init();

    $UserAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; .NET CLR 3.5.21022; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
    curl_setopt( $ch, CURLOPT_USERAGENT , $UserAgent );
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
    curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    //这里根据环境看是否需要绕过ssl验证
    if($byPass!==1){
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    }
    if($isPost!==0){
        curl_setopt( $ch , CURLOPT_POST , true );
        curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
        curl_setopt( $ch , CURLOPT_URL , $url );
    }else {
        if($params){
            curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
        }else{
            curl_setopt( $ch , CURLOPT_URL , $url);
        }
    }
    $response = curl_exec( $ch );
//    $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
//    $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
//    if ($response === FALSE) {
//        echo "cURL Error: " . curl_error($ch);
//        return false;
//    }
    curl_close( $ch );
    return $response;
}