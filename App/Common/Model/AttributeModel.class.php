<?php
namespace Common\Model;
/**
 * 属性服务类
 */
class AttributeModel extends BaseModel{

	/**
     * 获取属性
	 * @return $data
     */
	public function getAttribute(){
		$sql = "SELECT * FROM __PREFIX__attribute ORDER BY sort DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
     * 获取属性
	 * @param $productId 商品编号
	 * @return $data
     */
	public function getAttributeByAttrId($attrId){
		$sql = "SELECT * FROM __PREFIX__attribute where id=$attrId";
		$data = $this->queryRow($sql);
		return $data;
	}
}