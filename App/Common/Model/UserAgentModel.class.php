<?php
namespace Common\Model;
/**
 * 代理服务类
 */
class UserAgentModel extends BaseModel {
	protected $tableName = 'user_agent';
	/**
	 * 获取代理信息
	 * @param $agentId 代理编号
	 * @return $data
	 */
	public function getAgentByAgentId($agentId){
		$data = $this->where("id=$agentId")->find();
		return $data;
	}
	
	/**
	 * 获取用户代理信息
	 * @param $userId 用户编号
	 * @return $data
	 */
	public function getAgentByUserId($userId){
		$data = $this->where("user_id=$userId")->find();
		return $data;
	}
	
	/**
	* 获取店铺代理信息
	* @param $userId 用户编号
	* @return $data
	*/
	public function getShopAgentByUserId($userId){
		$sql = "SELECT a.* FROM __PREFIX__user_agent as a
				LEFT JOIN __PREFIX__user_shop AS s ON s.agent_id=a.id
				WHERE s.user_id=$userId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加代理
	 */
	public function insertUserAgent($data){
		$rd = array('status'=>-1);
	    if($this->create($data)){	
			$rs = $this->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改代理
	 */
	public function updateUserAgent($agentId, $data){
		$rd = array('status'=>-1);
		$agentId = (int)$agentId;
		$rs = $this->where("id=".$agentId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}

}