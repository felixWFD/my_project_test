<?php
namespace Common\Model;
/**
 * SEO服务类
 */
class SeoModel extends BaseModel{
	
	/**
	 * 获取SEO
	 */
	public function getSeo($ident){
		$sql = "SELECT title,keywords,description FROM __PREFIX__seo WHERE ident='$ident'";
		$data = $this->queryRow($sql);
		if(!is_array($data)){
			$data['title']       = $GLOBALS['CONFIG']['sy_webname'];
			$data['keywords']    = $GLOBALS['CONFIG']['sy_webkeyword'];
			$data['description'] = $GLOBALS['CONFIG']['sy_webmeta'];
		}
		return $data;
	}
}