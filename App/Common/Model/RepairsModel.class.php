<?php
namespace Common\Model;
/**
 * 维修服务类
 */
class RepairsModel extends BaseModel {
	/**
	* 获取设备
	* @param $id  编号
	* @return $data
	*/
	public function getRepairById($id){
		$sql = "SELECT * FROM __PREFIX__repairs WHERE id=$id";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取设备
	* @param $typeId  类型编号
	* @return $data
	*/
	public function getRepairByTypeId($typeId){
		$sql = "SELECT * FROM __PREFIX__repairs WHERE type_id=$typeId AND is_on_sale=1 ORDER BY sort DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取设备
	* @param $brandId 品牌编号
	* @return $data
	*/
	public function getRepairByBrandId($brandId){
		$sql = "SELECT * FROM __PREFIX__repairs WHERE brand_id=$brandId AND is_on_sale=1 ORDER BY sort DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 根据维修设备的颜色
	* @param $colorId 颜色编号
	* @return $data
	*/
	public function getColorById($colorId){
		$sql = "SELECT * FROM __PREFIX__repair_color WHERE id=$colorId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 根据设备编号获取该维修设备的颜色
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getColorByRepairId($modelId){
		$sql = "SELECT * FROM __PREFIX__repair_color WHERE repair_id=$modelId ORDER BY id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 根据维修方案获取维修设备的颜色
	* @param $planId 型号编号
	* @return $data
	*/
	public function getColorByPlanId($planId){
		$sql = "SELECT c.* FROM __PREFIX__repair_color AS c
				LEFT JOIN __PREFIX__repair_plan AS p ON p.product_id=c.repair_id
				WHERE p.id=$planId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加维修故障
	 */
	public function insertFault($data){
		$rd = array('status'=>-1);
		$model = M('repair_fault');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	* 获取维修故障
	* @param $faultId 故障编号
	* @return $data
	*/
	public function getFaultByFaultId($faultId){
		$sql = "SELECT * FROM __PREFIX__repair_fault WHERE id=$faultId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取维修故障
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getFaultByModelId($modelId){
		$sql = "SELECT * FROM __PREFIX__repair_fault WHERE product_id=$modelId ORDER BY sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 根据设备类型获取该维修设备的故障分类
	* @param $typeId  类型编号
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getCatByTypeId($typeId, $modelId){
		$sql = "SELECT c.id,c.name FROM __PREFIX__repair_fault AS f 
					LEFT JOIN __PREFIX__repair_fault_cat AS c ON c.id=f.cat_id
					WHERE c.is_show=1 AND c.cat_id=$typeId AND f.product_id=$modelId 
					GROUP BY c.id 
					ORDER BY c.sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加维修方案
	 */
	public function insertPlan($data){
		$rd = array('status'=>-1);
		$model = M('repair_plan');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	* 获取维修方案
	* @param $planId 方案编号
	* @return $data
	*/
	public function getPlanByPlanId($planId){
		$sql = "SELECT * FROM __PREFIX__repair_plan WHERE id=$planId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取维修方案
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getPlanByModelId($modelId){
		$sql = "SELECT * FROM __PREFIX__repair_plan WHERE product_id=$modelId ORDER BY sort ASC, id DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加维修关联
	 */
	public function insertRelation($data){
		$rd = array('status'=>-1);
		$model = M('repair_relation');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	* 获取该维修设备的全部故障信息
	* @param $modelId 型号编号
	* @param $catId   故障分类编号
	* @return $data
	*/
	public function getFaultByModelIdAndCatId($modelId, $catId){
		$sql = "SELECT r.id,r.plan_ids,f.id as fault_id,f.name as fault_name,f.description,c.name as cat_name,p.id as plan_id,p.name as plan_name,p.money,p.color_money,p.warranty,p.is_show
					FROM __PREFIX__repair_relation AS r 
					LEFT JOIN __PREFIX__repair_fault AS f ON f.id=r.fault_id
					LEFT JOIN __PREFIX__repair_fault_cat AS c ON c.id=f.cat_id
					LEFT JOIN __PREFIX__repair_plan AS p ON p.id=r.plan_ids
					WHERE r.product_id=$modelId AND f.cat_id=$catId 
					ORDER BY f.sort DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取单个故障全部信息
	* @param $modelId 型号编号
	* @param $faultId 故障编号
	* @return $data
	*/
	public function getFaultAllByModelIdAndFaultId($modelId, $faultId){
		$sql = "SELECT rr.id,rr.plan_ids,r.pro_name,f.id as fault_id,f.name as fault_name,f.description,p.id as plan_id,p.name as plan_name,p.money,p.color_money,p.warranty,p.is_show
					FROM __PREFIX__repair_relation AS rr 
					LEFT JOIN __PREFIX__repairs AS r ON r.id=rr.product_id
					LEFT JOIN __PREFIX__repair_fault AS f ON f.id=rr.fault_id
					LEFT JOIN __PREFIX__repair_plan AS p ON p.id=rr.plan_ids
					WHERE rr.product_id=$modelId AND rr.fault_id=$faultId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取维修设备全部故障信息/维修方案
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getRepairByModelId($modelId){
		$sql = "SELECT rr.id,r.pro_name,f.id as fault_id,f.name as fault_name,f.description,p.id as plan_id,p.name as plan_name,p.money,p.color_money,p.warranty,p.is_show
					FROM __PREFIX__repair_relation AS rr 
					LEFT JOIN __PREFIX__repairs AS r ON r.id=rr.product_id
					LEFT JOIN __PREFIX__repair_fault AS f ON f.id=rr.fault_id
					LEFT JOIN __PREFIX__repair_plan AS p ON p.id=rr.plan_ids
					WHERE rr.product_id=$modelId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取维修信息
	* @param $relationId 关联编号
	* @return $data
	*/
	public function getRepairByRelationId($relationId){
		$sql = "SELECT rr.*,r.id as pro_id,r.type_id,r.brand_id,r.pro_name,r.list_image,f.name as fault_name,f.description,p.name as plan_name,p.money,p.color_money,p.warranty,p.is_show
					FROM __PREFIX__repair_relation AS rr 
					LEFT JOIN __PREFIX__repairs AS r ON r.id=rr.product_id
					LEFT JOIN __PREFIX__repair_fault AS f ON f.id=rr.fault_id
					LEFT JOIN __PREFIX__repair_plan AS p ON p.id=rr.plan_ids
					WHERE rr.id=$relationId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 组织面包屑导航数据
	* @return $data
	*/
	public function steps($typeId, $brandId = 0, $modelId = 0, $colorId = 0, $catId = 0){
		// 类型
		$sql = "SELECT cat_name FROM __PREFIX__product_cat WHERE id=$typeId";
		$data = $this->queryRow($sql);
		$step['type'] = $data['cat_name'];
		
		// 品牌
		if(!empty($brandId)){
			$sql = "SELECT cat_name FROM __PREFIX__product_cat WHERE id=$brandId";
			$data = $this->queryRow($sql);
			$step['brand'] = $data['cat_name'];
		}
		
		// 型号
		if(!empty($modelId)){
			$sql = "SELECT pro_name FROM __PREFIX__repairs WHERE id=$modelId";
			$data = $this->queryRow($sql);
			$step['model'] = $data['pro_name'];
		}
		
		// 颜色
		if(!empty($colorId)){
			$sql = "SELECT name FROM __PREFIX__repair_color WHERE id=$colorId";
			$data = $this->queryRow($sql);
			$step['color'] = $data['name'];
		}
		
		// 故障分类
		if(!empty($catId)){
			$sql = "SELECT name FROM __PREFIX__repair_fault_cat WHERE id=$catId";
			$data = $this->queryRow($sql);
			$step['cat'] = $data['name'];
		}
		return $step;
	}
	
	/**
	* 获取设备基本情况信息
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getBasicByModelId($modelId){
		$sql = "SELECT * FROM __PREFIX__recovery_content WHERE types=1 AND pid=0 ORDER BY id ASC";
		$data = $this->query($sql);
		foreach($data as $key=>$vo){
			$sql = "SELECT c.* FROM __PREFIX__recovery_content AS c
				LEFT JOIN __PREFIX__recovery_relation AS r ON r.type_id=c.id
				WHERE c.pid=".$vo['id']." AND r.pro_id=$modelId AND r.is_show=1
				ORDER BY c.id ASC";
			$basic = $this->query($sql);
			$show = 1;
			if(empty($basic)){
				$show = 0;
			}
			$data[$key]['show'] = $show;
			$data[$key]['basic'] = $basic;
		}
		return $data;
	}
	
	/**
	* 获取设备功能情况信息
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getFunctionByModelId($modelId){
		$sql = "SELECT c.* FROM __PREFIX__recovery_content AS c
				LEFT JOIN __PREFIX__recovery_relation AS r ON r.type_id=c.id
				WHERE c.types=2 AND r.pro_id=$modelId AND r.is_show=1
				ORDER BY c.id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取设备其他情况信息
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getOtherByModelId($modelId){
		$sql = "SELECT * FROM __PREFIX__recovery_content WHERE types=3 AND pid=0 ORDER BY id ASC";
		$data = $this->query($sql);
		foreach($data as $key=>$vo){
			$sql = "SELECT c.* FROM __PREFIX__recovery_content AS c
				LEFT JOIN __PREFIX__recovery_relation AS r ON r.type_id=c.id
				WHERE c.pid=".$vo['id']." AND r.pro_id=$modelId AND r.is_show=1
				ORDER BY c.id ASC";
			$other = $this->query($sql);
			$show = 1;
			if(empty($other)){
				$show = 0;
			}
			$data[$key]['show'] = $show;
			$data[$key]['other'] = $other;
		}
		return $data;
	}
	
	/**
	* 获取设备回收关联信息
	* @param $modelId 型号编号
	* @param $typeId  类型编号
	* @return $data
	*/
	public function getRelationByModelId($modelId, $typeId){
		$sql = "SELECT * FROM __PREFIX__recovery_relation WHERE pro_id=$modelId AND type_id=$typeId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 计算设备回收属性
	* @param $ids      评估项目
	* @return $data
	*/
	public function getAttribute($ids){
		$ids = explode(",", $ids);
		foreach($ids as $id){
			if(!empty($id)){
				$sql = "SELECT * FROM __PREFIX__recovery_content WHERE id=$id";
				$d = $this->queryRow($sql);
				$data[] = $d['name'];
			}
		}
		return $data;
	}
	
	/**
	* 计算设备回收金额
	* @param $modelId 型号编号
	* @param $ids     评估项目
	* @return $data
	*/
	public function computeMoney($modelId, $ids){
		$repair = $this->getRepairById($modelId);
		$max = $repair['max_price'];
		$min = $repair['min_price'];
		$ids = explode(",", $ids);
		foreach($ids as $id){
			if(!empty($id)){
				$relation = $this->getRelationByModelId($modelId,$id);
				if($relation['project_id'] == '1'){       //比例
					$max = $max*$relation['money'];
				}elseif($relation['project_id'] == '2'){  //加
					$max = $max+$relation['money'];
				}elseif($relation['project_id'] == '3'){  //减
					$max = $max+$relation['money'];
				}
			}
		}
		if($max < $min){
			$max = $min;
		}
		return CBWNewMoney($max);
	}
}