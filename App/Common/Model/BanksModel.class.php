<?php
namespace Common\Model;
/**
 * 银行服务类
 */
class BanksModel extends BaseModel {
	protected $tableName = 'bank';
	/**
	* 获取银行信息
	* @return $data
	*/
	public function getBank(){
		$sql = "SELECT * FROM __PREFIX__bank WHERE is_show=1 ORDER BY id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取支持信用卡的银行信息
	* @param $credit 银行编号
	* @return $data
	*/
	public function getBankByCredit(){
		$sql = "SELECT * FROM __PREFIX__bank WHERE is_show=1 AND credit=1 ORDER BY id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取单个银行信息
	* @param $bankId 银行编号
	* @return $data
	*/
	public function getBankByBankId($bankId){
		$sql = "SELECT * FROM __PREFIX__bank WHERE id=$bankId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取单个银行信息
	* @param $number 卡号头数字串
	* @return $data
	*/
	public function getBankCardByNumber($number){
		$sql = "SELECT c.*, b.name, b.picture
				FROM __PREFIX__bank_card AS c
				LEFT JOIN __PREFIX__bank AS b ON b.id=c.bank_id
				WHERE c.card_number='$number'";
		$data = $this->queryRow($sql);
		return $data;
	}
}