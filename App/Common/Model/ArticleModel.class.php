<?php
namespace Common\Model;
/**
 * 文章服务类
 */
class ArticleModel extends BaseModel {
	protected $tableName = 'article_detail';
	
	/**
	* 获取文章分类信息
	* @return $data
	*/
	public function getArticleCat(){
		$sql = "SELECT * FROM __PREFIX__article_cat WHERE is_show=1 ORDER BY id DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取分类下文章列表
	* @param $catId 分类编号
	* @return $data
	*/
	public function getArticleList($catId){		
		$sql = "SELECT * FROM __PREFIX__article_detail WHERE cat_id=$catId ORDER BY id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取文章
	* @param $articleId 文章编号
	* @return $data
	*/
	public function getArticle($articleId){
		$sql = "SELECT * FROM __PREFIX__article_detail WHERE id=$articleId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 文章统计
	 * @param $articleId 文章编号
	 */
	public function statistics($articleId){		
		$this->execute("UPDATE __PREFIX__article_detail set clicktimes=clicktimes+1 WHERE id=".$articleId);
	}
}