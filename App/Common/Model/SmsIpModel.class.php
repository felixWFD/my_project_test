<?php
namespace Common\Model;
/**
 * 短信发送IP服务类
 */
class SmsIpModel extends BaseModel {
    /**
	 * 新增
	 */
	public function insert($data){
	 	$rd = -1;
	    $m = M('Sms_ip');
		if($m->create($data)){
			$rs = $m->add();
			if(false !== $rs){
				$rd = $rs;
			}
		}
		return $rd;
	} 
	 
    /**
	 * 修改
	 */
	public function edit($data){
	 	$rd = -1;
	    $m = M('Sms_ip');
		$rs = $m->save($data);
		if(false !== $rs){
			$rd = $rs;
		}
		return $rd;
	} 
	  
	/**
	 * 删除
	 */
	public function del($id){
	 	$rd = -1;
	    $m = M('Sms_ip');
	    $rs = $m->where("id=".$id)->delete();
		if(false !== $rs){
		   $rd = 1;
		}
		return $rd;
	}
	
	/**
	 * 获取今天指定对象
	 */
    public function get($ip){
	 	$m = M('Sms_ip'); 
		return $m->where("ip='".$ip."' AND add_time>".strtotime(date('Y-m-d')))->find();
	}
	
};
?>