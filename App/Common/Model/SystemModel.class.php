<?php
namespace Common\Model;
/**
 * 系统服务类
 */
class SystemModel extends BaseModel {
	protected $tableName = 'sys_config'; 
    /**
     * 获取网站配置文件
     */
	public function loadConfigs(){
		$configs = CBWDataFile('site_config');
		if(!$configs){
			$sql = "SELECT cname,cvalue FROM __PREFIX__sys_config order by cname asc";
			$rs = $this->query($sql);
			$configs = array();
			if(count($rs)>0){
				foreach ($rs as $key=>$v){
					$configs[$v['cname']] = $v['cvalue'];
				}
			}
			unset($rs);
			CBWDataFile('site_config','',$configs);
		}
		return $configs;
	}
	
	/**
     * 获取邮件发送模板
     */
	public function loadEmailTemplate($template){
		$sql = "SELECT * FROM __PREFIX__email_templates	WHERE ename='$template'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
     * 获取银行信息
     */
	public function loadBank(){
		$sql = "SELECT * FROM __PREFIX__bank WHERE is_show=1";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
     * 获取友情链接
     */
	public function loadLinks($category=1){
		$sql = "SELECT * FROM __PREFIX__links WHERE category=$category AND is_show=1 LIMIT 0,30";
		$data = $this->query($sql);
		return $data;
	}
}