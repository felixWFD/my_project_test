<?php
namespace Common\Model;
/**
 * 店铺服务类
 */
class UserShopModel extends BaseModel {
	protected $tableName = 'user_shop';
	/**
	* 获取店铺信息
	* @param $shopId 店铺ID
	* @return $data
	*/
	public function getShopByShopId($shopId){
		$shopId = intval($shopId?$shopId:session('shop.id'));
		$sql = "SELECT s.*,a.addr,a.one_id,a.two_id,c.addr as city_addr,p.addr as pro_addr
				FROM __PREFIX__user_shop as s
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE s.id=$shopId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取主店铺信息
	* @param $userId 用户ID
	* @return $data
	*/
	public function getMainShopByUserId($userId){
		$sql = "SELECT s.*,a.addr,a.one_id,a.two_id,c.addr as city_addr,p.addr as pro_addr
				FROM __PREFIX__user_shop as s
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE s.user_id=$userId AND is_main=1";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取店铺信息
	* @param $sql 条件语句
	* @return $data
	*/
	public function getShopBySql($sql){
		$data = $this->where($sql)->find();
		return $data;
	}
	
	/**
	* 获取用户店铺信息
	* @param $userId 用户ID
	* @return $data
	*/
	public function getShopByUserId($userId){
		$userId = intval($userId?$userId:session('user.userid'));
		$sql = "SELECT s.*,a.addr,a.one_id,a.two_id,c.addr as city_addr,p.addr as pro_addr
				FROM __PREFIX__user_shop as s
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE s.user_id=$userId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取用户全部下级店铺信息
	* @param $userId 用户ID
	* @return $data
	*/
	public function getAllShopByUserId($userId){
		$userId = intval($userId?$userId:session('user.userid'));
		$sql = "SELECT s.* FROM __PREFIX__user_shop AS s
				LEFT JOIN __PREFIX__users AS u ON u.id=s.user_id
				WHERE u.parent_id=$userId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取用户店铺数量
	* @param $obj 查询条件数组
	* @return $data
	*/
	public function getShopsCountByObject($obj){
		$userId     = $obj['userId'];
		$province   = $obj['province'];
		$city       = $obj['city'];
		$area       = $obj['area'];
		$main       = $obj['main'];
		$way        = $obj['way'];
		$lng        = $obj['lng'];
		$lat        = $obj['lat'];
		$status     = $obj['status'];
		$keyword    = $obj['keyword'];
		
		$where = "s.shop_status<>0";
		if(!empty($userId)){
			$where .= " AND s.user_id=$userId";		
		}
		if(!empty($province)){
			$where .= " AND p.id=$province";
		}
		if(!empty($city)){
			$where .= " AND c.id=$city";
		}
		if(!empty($area)){
			$where .= " AND a.id=$area";
		}
		if(isset($main)){
			$where .= " AND s.is_main=$main";
		}
		if($way == 1){
			$where .= " AND s.is_store=1";
		}elseif($way == 2){
			$where .= " AND s.is_door=1";
		}
		if(!empty($lng) && !empty($lat)){
			$squares  = CBWSquarePoint($lng, $lat);
			$where .= " AND s.lat<>0 AND s.lat>{$squares['right-bottom']['lat']} AND s.lat<{$squares['left-top']['lat']} AND s.lng>{$squares['left-top']['lng']} AND s.lng<{$squares['right-bottom']['lng']}";
		}
		if(isset($status)){
			$where .= " AND s.server_status=$status";
		}
		if(!empty($keyword)){
			$where .= " AND (s.shop_name like '%".$keyword."%' OR s.user_name like '%".$keyword."%')";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__user_shop AS s
				LEFT JOIN __PREFIX__users AS u ON u.id=s.user_id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where";
		
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取用户店铺列表
	* @param $obj 查询条件数组
	* @return $data
	*/
	public function getShopsByObject($obj){
		$userId     = $obj['userId'];
		$province   = $obj['province'];
		$city       = $obj['city'];
		$area       = $obj['area'];
		$main       = $obj['main'];
		$way        = $obj['way'];
		$lng        = $obj['lng'];
		$lat        = $obj['lat'];
		$status     = $obj['status'];
		$keyword    = $obj['keyword'];
		$orderby    = $obj['orderby'];
		$m          = $obj['m'];
		$n          = $obj['n'];
		
		$where = "s.shop_status<>0";
		if(!empty($userId)){
			$where .= " AND s.user_id=$userId";		
		}
		if(!empty($province)){
			$where .= " AND p.id=$province";
		}
		if(!empty($city)){
			$where .= " AND c.id=$city";
		}
		if(!empty($area)){
			$where .= " AND a.id=$area";
		}
		if(isset($main)){
			$where .= " AND s.is_main=$main";
		}
		if($way == 1){
			$where .= " AND s.is_store=1";
		}elseif($way == 2){
			$where .= " AND s.is_door=1";
		}
		if(!empty($lng) && !empty($lat)){
			$squares  = CBWSquarePoint($lng, $lat);
			$where .= " AND s.lat<>0 AND s.lat>{$squares['right-bottom']['lat']} AND s.lat<{$squares['left-top']['lat']} AND s.lng>{$squares['left-top']['lng']} AND s.lng<{$squares['right-bottom']['lng']}";
		}
		if(isset($status)){
			$where .= " AND s.server_status=$status";
		}
		if(!empty($keyword)){
			$where .= " AND (s.shop_name like '%".$keyword."%' OR s.user_name like '%".$keyword."%')";
		}
		if($orderby == 'num'){
			$order = 's.type_id DESC,s.repair_count DESC';
		}elseif($orderby == 'main'){
			$order = 's.type_id DESC,s.is_main DESC,s.id ASC';
		}else{
			$order = 's.type_id DESC,s.score DESC,s.id ASC';
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT s.*,u.uname,u.mobile,a.addr,c.addr as city_addr,p.addr as pro_addr
				FROM __PREFIX__user_shop AS s
				LEFT JOIN __PREFIX__users AS u ON u.id=s.user_id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where 
				ORDER BY $order $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	  * 根据查询条件获取店铺数量
	  */
    public function getRepairerCountByObj($obj){
		$area       = $obj['area'];
		$city       = $obj['city'];
		$type       = $obj['type'];
		$brand      = $obj['brand'];
		
		$where = "s.shop_status<>0 AND s.server_status=1";
		if(!empty($area)){
			$where .= " AND a.id=$area";
		}
		if(!empty($city)){
			$where .= " AND c.id=$city";
		}
		if(!empty($type)){
			$where .= " AND pc.fid=$type";
		}
		if(!empty($brand)){
			$where .= " AND m.brand_id=$brand";
		}
		$sql = "SELECT s.* FROM __PREFIX__user_shop AS s
				LEFT JOIN __PREFIX__user_model AS m ON m.user_id=s.user_id 
				LEFT JOIN __PREFIX__product_cat AS pc ON pc.id=m.brand_id 
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where
				GROUP BY s.id
				ORDER BY s.score DESC,s.id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	  * 根据查询条件获取店铺信息
	  */
    public function getRepairerByObj($obj){
		$area       = $obj['area'];
		$city       = $obj['city'];
		$type       = $obj['type'];
		$brand      = $obj['brand'];
		$orderby    = $obj['orderby'];
		$m          = $obj['m'];
		$n          = $obj['n'];
		
		$where = "s.shop_status<>0 AND s.server_status=1";
		if(!empty($area)){
			$where .= " AND a.id=$area";
		}
		if(!empty($city)){
			$where .= " AND c.id=$city";
		}
		if(!empty($type)){
			$where .= " AND pc.fid=$type";
		}
		if(!empty($brand)){
			$where .= " AND m.brand_id=$brand";
		}
		if($orderby == 'num'){
			$order = 's.repair_count DESC';
		}else{
			$order = 's.score DESC,s.id ASC';
		}
		$sql = "SELECT s.*,a.addr,c.addr as city_addr,p.addr as pro_addr 
				FROM __PREFIX__user_shop AS s
				LEFT JOIN __PREFIX__user_model AS m ON m.user_id=s.user_id 
				LEFT JOIN __PREFIX__product_cat AS pc ON pc.id=m.brand_id 
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where
				GROUP BY s.id
				ORDER BY $order
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	  * 根据查询条件获取维修店铺信息
	  */
    public function getShopsByLngLat($lng, $lat, $m, $n, $way=0){
		$where = "s.shop_status<>0 AND s.server_status=1";
		if($way == 1){
			$where .= " AND s.is_store=1";
		}elseif($way == 2){
			$where .= " AND s.is_door=1";
		}
		$squares  = CBWSquarePoint($lng, $lat);
		$where .= " AND s.lat<>0 AND s.lat>{$squares['right-bottom']['lat']} AND s.lat<{$squares['left-top']['lat']} AND s.lng>{$squares['left-top']['lng']} AND s.lng<{$squares['right-bottom']['lng']}";
		$sql = "SELECT s.*,a.addr,c.addr as city_addr,p.addr as pro_addr 
				FROM __PREFIX__user_shop AS s
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=s.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where
				GROUP BY s.id
				ORDER BY s.type_id DESC,s.score DESC
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加店铺
	 */
	public function insertUserShop($data){
		$rd = array('status'=>-1);
		$rs = $this->add($data);
	    if(false !== $rs){
			$rd['status'] = 1;
		}
		return $rd;
	}
	
	/**
	 * 修改店铺信息
	 */
	public function updateUserShop($shopId, $data){
		$rd = array('status'=>-1);
		$shopId = (int)$shopId;
		$rs = $this->where("id=".$shopId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 修改用户店铺信息
	 */
	public function updateUserShopByUserId($userId, $data){
		$rd = array('status'=>-1);
		$userId = (int)$userId;
		$rs = $this->where("user_id=".$userId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 获取店铺已添加的品牌
	* @param $userId 会员编号
	* @return $data
	*/
	public function getUserBrandByUserId($userId){
		$sql = "SELECT m.*,c.* FROM __PREFIX__user_model AS m
				LEFT JOIN __PREFIX__product_cat AS c ON c.id=m.brand_id
				WHERE m.user_id=$userId
				GROUP BY m.brand_id";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺已添加的品牌
	* @param $userId 会员编号
	* @param $typeId 类型编号
	* @return $data
	*/
	public function getUserBrandByTypeId($userId, $typeId){
		$sql = "SELECT m.*,c.* FROM __PREFIX__user_model AS m
				LEFT JOIN __PREFIX__product_cat AS c ON c.id=m.brand_id
				WHERE m.user_id=$userId AND c.fid=$typeId
				GROUP BY m.brand_id";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺已添加的型号
	* @param $userId 会员编号
	* @return $data
	*/
	public function getUserModelByUserId($userId){
		$sql = "SELECT m.*,r.* FROM __PREFIX__user_model AS m
				LEFT JOIN __PREFIX__repairs AS r ON r.id=m.model_id
				WHERE m.user_id=$userId
				ORDER BY m.model_id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺已添加的型号
	* @param $userId 会员编号
	* @param $brandId 品牌编号
	* @return $data
	*/
	public function getUserModelByBrandId($userId, $brandId){
		$sql = "SELECT m.*,r.* FROM __PREFIX__user_model AS m
				LEFT JOIN __PREFIX__repairs AS r ON r.id=m.model_id
				WHERE m.user_id=$userId AND m.brand_id=$brandId
				ORDER BY m.model_id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺已添加的型号
	* @param $userId  会员编号
	* @param $modelId 型号编号
	* @return $data
	*/
	public function getUserModelByModelId($userId, $modelId){
		$sql = "SELECT * FROM __PREFIX__user_model WHERE user_id=$userId AND model_id=$modelId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加店铺已添加型号记录
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertUserModel($data){
		$rd = array('status'=>-1);
		$m = M('user_model');
	    if($m->create($data)){	
			$rs = $m->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新店铺已添加型号记录
	 * @param $sql  条件语句
	 * @param $data 数据数组
	 * @return $rs
	 */
	public function updateUserModel($sql, $data){
		$rd = array('status'=>-1);
		$rs = M('user_model')->where($sql)->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 删除店铺已添加型号记录
	 * @param $userId  会员编号
	 * @param $brandId 品牌编号
	 * @param $modelId 型号编号
	 * @return $rs
	 */
	public function deleteShopModel($userId, $brandId = 0, $modelId = 0){
		$rd = array('status'=>-1);
		$where = "user_id=$userId";
		if($brandId > 0){
			$where .= " AND brand_id=$brandId";
		}
		if($modelId > 0){
			$where .= " AND model_id=$modelId";
		}
	    $rs = M('user_model')->where($where)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 获取通知信息
	* @param $noticeId 通知ID
	* @return $data
	*/
	public function getShopNoticeByNoticeId($noticeId){
		$sql = "SELECT * FROM __PREFIX__user_shop_notice WHERE id=$noticeId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	  * 根据查询条件获取通知数量
	  */
    public function getShopNoticeCountByObject($object){
		$shopId = $object['shopId'];
		$status = $object['status'];
		
		$where = "shop_id=$shopId";
		if(isset($status) && $status!=''){
			$where .= " AND status=$status";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__user_shop_notice WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	  * 根据查询条件获取店铺通知
	  */
    public function getShopNoticeByObject($object){
		$shopId = $object['shopId'];
		$status = $object['status'];
		$m      = $object['m'];
		$n      = $object['n'];
		
		$where = "shop_id=$shopId";
		if(isset($status) && $status!=''){
			$where .= " AND status=$status";
		}
		$sql = "SELECT * FROM __PREFIX__user_shop_notice WHERE $where ORDER BY create_time DESC LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加店铺通知
	 */
	public function insertShopNotice($data){
		$rd = array('status'=>-1);
		$m = M('user_shop_notice');
	    if($m->create($data)){	
			$rs = $m->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 编辑店铺通知
	 */
	public function updateShopNotice($sql, $data){
		$rd = array('status'=>-1);
		$m = M('user_shop_notice');
		$rs = $m->where($sql)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 获取评分信息
	* @param $scoreId 评分ID
	* @return $data
	*/
	public function getShopScoreByScoreId($scoreId){
		$sql = "SELECT * FROM __PREFIX__user_shop_score WHERE id=$scoreId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	  * 根据查询条件获取评分数量
	  */
    public function getShopScoreCountByShopId($shopId){
		$sql = "SELECT count(*) AS num FROM __PREFIX__user_shop_score WHERE shop_id=$shopId";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	  * 根据查询条件获取评分
	  */
    public function getShopScoreByShopId($shopId, $m, $n){
		$sql = "SELECT * FROM __PREFIX__user_shop_score WHERE shop_id=$shopId ORDER BY create_time DESC LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加店铺评分
	 */
	public function insertShopScore($data){
		$rd = array('status'=>-1);
		$m = M('user_shop_score');
	    if($m->create($data)){	
			$rs = $m->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改店铺评分
	 * @param $shopId 店铺编号
	 */
	public function updateShopScore($shopId, $type, $score, $remarks=''){
		$sql = "UPDATE __PREFIX__user_shop set score=score-$score WHERE id=$shopId";
		if($type){
			$sql = "UPDATE __PREFIX__user_shop set score=score+$score WHERE id=$shopId";
		}
		$this->execute($sql);
		$this->insertShopScore(array('shop_id'=>$shopId, 'type'=>$type, 'score'=>$score, 'create_time'=>time(), 'remarks'=>$remarks));
	}
	
	/**
	 * 店铺点击数统计
	 * @param $shopId 店铺编号
	 */
	public function statistics($shopId){
		$sql = "UPDATE __PREFIX__user_shop set clicktimes=clicktimes+1 WHERE id=$shopId";
		$this->execute($sql);
	}
	
};
?>