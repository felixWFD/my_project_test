<?php
namespace Common\Model;
/**
 * 广告服务类
 */
class AdvsModel extends BaseModel{
	
	/**
	 * 获取广告
	 */
	public function getAdvsById($adId){
		$sql = "SELECT id,adv_name,thumbnail,link_url FROM __PREFIX__advs WHERE id=$adId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取分类广告
	 */
	public function getAdvsByCat($catId, $show = 0){
		$data = S('CBW_CACHE_ADVS_CAT_'.$catId."_".$show);
		if(!$data){
			$where = "position_id=$catId";
			if($show == 1){
				$where .= " AND is_show=1";
			}
			$sql = "SELECT id,adv_name,thumbnail,link_url FROM __PREFIX__advs WHERE $where ORDER BY id DESC";
			$data = $this->query($sql);
			S('CBW_CACHE_ADVS_CAT_'.$catId."_".$show,$data,86400);
		}
		return $data;
	}
}