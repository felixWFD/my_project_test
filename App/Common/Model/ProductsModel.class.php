<?php
namespace Common\Model;
/**
 * 商品服务类
 */
class ProductsModel extends BaseModel {
	
	/**
	* 获取单个商品信息
	* @param $productId 商品编号
	* @return $data
	*/
	public function getProductById($productId){
		$data = $this->where("id=$productId")->find();
		return $data;
	}
	
	/**
	* 商品关联商品
	* @param $productId  商品编号
	* @param $groupId    用户组
	* @param $vip        是否会员
	* @param $limit      显示数量
	* @return $data
	*/
	public function getProductLinkByProductId($productId, $groupId=0, $vip=0, $limit='4'){	
		$sql = "SELECT pl.*,p.pro_name,p.list_image FROM __PREFIX__product_link AS pl 
				LEFT JOIN __PREFIX__products AS p ON p.id=pl.link_pro_id 
				WHERE pl.pro_id=$productId 
				LIMIT 0,$limit";
		$data = $this->query($sql);
		foreach($data as &$vo){
			$relations = $this->getRelationByProductId($vo['link_pro_id']);
			$vo['rid'] = $relations[0]['id'];
			$rel = $this->getProductPriceByRelationId($relations[0]['id'], 1, $groupId, $vip);
			$vo['price'] = $rel['price'];
		}
		unset($vo);
		return $data;
	}
	
	/**
	* 通过筛选条件获取商品列表
	* @param $object   条件数组
	* @return $data
	*/
	public function getProductsCountByObject($object){
		$shopId   = (int)$object['shopId'];
		$groupId  = (int)$object['groupId'];
		$classId  = (int)$object['classId'];
		$typeId   = (int)$object['typeId'];
		$brandId  = (int)$object['brandId'];
		$catId    = (int)$object['catId'];
		$sale     = $object['sale'];
		$keyword  = $object['keyword'];
		$min      = (int)$object['min'];
		$max      = (int)$object['max'];
		
		$where = "recycle<>1";
		if($shopId > 0){
			$where .= " AND shop_id=$shopId";
		}
		if($groupId == 0){
			$where .= ' AND is_shop=0';
		}
		if($classId > 0){
			$where .= " AND classify_id=".$classId;
		}
		if($typeId > 0){
			$where .= " AND type_id=".$typeId;
		}
		if($brandId > 0){
			$where .= " AND brand_id=".$brandId;
		}
		if($catId > 0){
			$where .= " AND cat_id=".$catId;
		}
		if(isset($sale) && $sale != '' && $sale != '是否上架'){
			$where .= " AND is_on_sale=$sale";
		}
		if(!empty($keyword)){
			$where .= " AND pro_name like '%".$keyword."%'";
		}
		if($min >= 0 && $max > 0){
			$where .= " AND pro_price BETWEEN $min AND $max";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__products WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 通过筛选条件获取商品列表
	* @param $object   条件数组
	* @return $data
	*/
	public function getProductsByObject($object){
		$shopId   = (int)$object['shopId'];
		$groupId  = (int)$object['groupId'];
		$classId  = (int)$object['classId'];
		$typeId   = (int)$object['typeId'];
		$brandId  = (int)$object['brandId'];
		$catId    = (int)$object['catId'];
		$sale     = $object['sale'];
		$keyword  = $object['keyword'];
		$min      = (int)$object['min'];
		$max      = (int)$object['max'];
		$sort     = $object['sort'];
		$m        = $object['m'];
		$n        = $object['n'];
		
		$where = "recycle<>1";
		if($shopId > 0){
			$where .= " AND shop_id=$shopId";
		}
		if($groupId == 0){
			$where .= ' AND is_shop=0';
		}
		if($classId > 0){
			$where .= " AND classify_id=".$classId;
		}
		if($typeId > 0){
			$where .= " AND type_id=".$typeId;
		}
		if($brandId > 0){
			$where .= " AND brand_id=".$brandId;
		}
		if($catId > 0){
			$where .= " AND cat_id=".$catId;
		}
		if(isset($sale) && $sale != '' && $sale != '是否上架'){
			$where .= " AND is_on_sale=$sale";
		}
		if(!empty($keyword)){
			$where .= " AND pro_name like '%".$keyword."%'";
		}
		if($min >= 0 && $max > 0){
			$where .= " AND pro_price BETWEEN $min AND $max";
		}
		if($sort == 'default'){
			$order = " sale_quantity DESC, click_date DESC";
		}elseif($sort == 'new'){
			$order = " add_time DESC";
		}elseif($sort == 'comment'){
			$order = " comment_quantity DESC";
		}elseif($sort == 'sale'){
			$order = " sale_quantity DESC";
		}elseif($sort == 'down'){
			$order = " pro_price DESC";
		}elseif($sort == 'up'){
			$order = " pro_price ASC";
		}elseif($sort == 'position'){
			$order = " adv_position ASC";
		}else{
			$order = " click_date DESC";
		}
		if(isset($m) && isset($n)){
			$limit = " LIMIT $m,$n";
		}
		$sql = "SELECT * FROM __PREFIX__products WHERE $where ORDER BY $order $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加商品信息
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertProduct($data){
		$rd = array('status'=>-1);
	    if($this->create($data)){	
			$rs = $this->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改商品信息
	 * @param $productId 商品编号
	 * @param $object       商品信息数组
	 * @return $data
	 */
	public function updateProduct($productId, $object){
		$rd = array('status'=>-1);
		$productId = (int)$productId;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = $this->where("id=".$productId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品
	 * @param $id    编号
	 * @return $rd
	 */
	public function deleteProduct($id){
		$rd = array('status'=>-1);
	    $rs = $this->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 商品减库存
	 * @param $productId 商品编号
	 * @param $num       数量
	 * @param $attrId    商品属性
	 */
	public function decrease($productId, $num, $attrId){		
		$this->execute("UPDATE __PREFIX__products set stock_num=stock_num-$num WHERE id=".$productId);
		if(!empty($attrId)){
			$attr = explode("|", $attrId);
			$combineArray = CBWCombinationString('', $attr, $combineArray, strlen(implode("", $attr))+count($attr)-1);
			$this->execute("UPDATE __PREFIX__product_relation set pro_stock=pro_stock-$num WHERE pro_id=$productId AND pro_attr in('".implode("','", $combineArray)."')");
		}
	}
	
	/**
	 * 商品加库存
	 * @param $productId 商品编号
	 * @param $num       数量
	 */
	public function increase($productId, $num){		
		$this->execute("UPDATE __PREFIX__products set stock_num=stock_num+$num WHERE id=".$productId);
	}
	
	/**
	 * 商品品牌
	 * @return $data
	 */
	public function getBrands(){
		$sql = "SELECT * FROM __PREFIX__product_brand where is_show=1 ORDER BY sort DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取商品参数
	* @param $productId 商品编号
	* @return $data
	*/
	public function getProductParamById($productId){
		$sql = "SELECT * FROM __PREFIX__product_param WHERE pro_id=$productId ORDER BY id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加商品参数
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertProductParam($data){
		$rd = array('status'=>-1);
		$model = M('product_param');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改商品参数
	 * @param $id     参数编号
	 * @param $object 参数内容数组
	 * @return $rd
	 */
	public function updateProductParam($id, $object){
		$rd = array('status'=>-1);
		$id = (int)$id;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = M('product_param')->where("id=".$id)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品参数
	 * @param $id     参数编号
	 * @return $rd
	 */
	public function deleteProductParam($id){
		$rd = array('status'=>-1);
	    $rs = M('product_param')->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品参数
	 * @param $productId  商品编号
	 * @return $rd
	 */
	public function deleteProductParamByProductId($productId){
		$rd = array('status'=>-1);
	    $rs = M('product_param')->where("pro_id=".$productId)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 获取属性
	* @param $attrId   属性编号
	* @return $data
	*/
	public function getAttributeByAttrId($attrId){
		$sql = "SELECT * FROM __PREFIX__attribute WHERE id=$attrId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取属性
	* @param $attrIdent   属性标识
	* @return $data
	*/
	public function getAttributeByAttrIdent($attrIdent){
		$sql = "SELECT * FROM __PREFIX__attribute WHERE attr_ident='$attrIdent'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取属性
	* @param $productId   商品编号
	* @return $data
	*/
	public function getAttributeByProductId($productId){
		$sql = "SELECT * FROM __PREFIX__attribute WHERE pro_id=$productId ORDER BY sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加商品属性
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertAttribute($data){
		$rd = array('status'=>-1);
		$model = M('attribute');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改商品属性
	 * @param $id     属性编号
	 * @param $object 属性内容数组
	 * @return $rd
	 */
	public function updateAttribute($id, $object){
		$rd = array('status'=>-1);
		$id = (int)$id;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = M('attribute')->where("id=".$id)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品属性
	 * @param $id     属性编号
	 * @return $rd
	 */
	public function deleteAttribute($id){
		$rd = array('status'=>-1);
	    $rs = M('attribute')->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 获取属性
	* @param $attributeId 属性编号
	* @return $data
	*/
	public function getProductAttrByAttributeId($attributeId){
		$sql = "SELECT * FROM __PREFIX__product_attr WHERE attr_id=$attributeId ORDER BY sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取商品属性
	* @param $productId 商品编号
	* @return $data
	*/
	public function getProductAttrByProductId($productId){
		$sql = "SELECT pa.*,a.attr_name FROM __PREFIX__product_attr pa 
				INNER JOIN __PREFIX__attribute AS a ON a.id=pa.attr_id 
				WHERE a.pro_id=$productId
				GROUP BY pa.attr_id 
				ORDER BY a.sort ASC";
		$data = $this->query($sql);
		foreach($data as &$vo){
			$vo['list'] = $this->getAttrsByProductIdAndAttrId($productId, $vo['attr_id']);
		}
		unset($vo);
		return $data;
	}
	
	/**
	* 根据属性组合获取属性名称
	* @param $attrId   属性编号
	* @return $data
	*/
	public function getAttrNameByAttrId($attrId){
		$attrs = explode("|",$attrId);
		foreach($attrs as $attr){
			$sql = "SELECT pa.attr_value FROM __PREFIX__product_attr AS pa 
					LEFT JOIN __PREFIX__attribute AS a ON a.id=pa.attr_id 
					WHERE pa.id=$attr";
			$attribute = $this->queryRow($sql);
			$data[] = $attribute['attr_value'];
		}
		return implode(" ",$data);
	}
	
	/**
	* 获取商品属性
	* @param $productId 商品编号
	* @return $data
	*/
	public function getAttrsByProductId($productId){
		$sql = "SELECT pa.*,a.attr_name,a.attr_ident FROM __PREFIX__product_attr pa 
				INNER JOIN __PREFIX__attribute AS a ON a.id=pa.attr_id 
				WHERE a.pro_id=$productId
				ORDER BY pa.sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取商品属性
	* @param $productId 商品编号
	* @param $attrId    属性编号
	* @return $data
	*/
	public function getAttrsByProductIdAndAttrId($productId, $attrId){
		$sql = "SELECT pa.*,a.attr_name FROM __PREFIX__product_attr pa 
				INNER JOIN __PREFIX__attribute AS a ON a.id=pa.attr_id 
				WHERE a.pro_id=$productId AND pa.attr_id=$attrId
				ORDER BY pa.sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取商品属性
	* @param $productId 商品编号
	* @param $attrId    属性编号
	* @return $data
	*/
	public function getAttrsByAttrId($attrId){
		$sql = "SELECT pa.*,a.attr_name FROM __PREFIX__product_attr pa 
				INNER JOIN __PREFIX__attribute AS a ON a.id=pa.attr_id 
				WHERE pa.attr_id=$attrId
				ORDER BY pa.sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取属性
	* @param $attrId   属性编号
	* @return $data
	*/
	public function getAttrByAttrId($attrId){
		$sql = "SELECT * FROM __PREFIX__product_attr WHERE id=$attrId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取属性
	* @param $ident   属性标识
	* @return $data
	*/
	public function getAttrByAttrIdent($ident){
		$sql = "SELECT * FROM __PREFIX__product_attr WHERE attr_ident='$ident'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加商品属性
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertProductAttr($data){
		$rd = array('status'=>-1);
		$model = M('product_attr');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改商品属性
	 * @param $id     属性编号
	 * @param $object 属性内容数组
	 * @return $rd
	 */
	public function updateProductAttr($id, $object){
		$rd = array('status'=>-1);
		$id = (int)$id;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = M('product_attr')->where("id=".$id)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品属性
	 * @param $id     属性编号
	 * @return $rd
	 */
	public function deleteProductAttr($id){
		$rd = array('status'=>-1);
	    $rs = M('product_attr')->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品属性
	 * @param $productId     商品编号
	 * @return $rd
	 */
	public function deleteProductAttrByProductId($productId){
		$rd = array('status'=>-1);
	    $rs = M('product_attr')->where("pro_id=".$productId)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 获取属性图片
	* @param $attrId   属性编号
	* @return $data
	*/
	public function getAlbumByAttrId($attrId){
		$sql = "SELECT * FROM __PREFIX__product_album WHERE attr_id=$attrId ORDER BY sort ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取属性图片
	* @param $albumId   图片编号
	* @return $data
	*/
	public function getAlbumByAlbumId($albumId){
		$sql = "SELECT * FROM __PREFIX__product_album WHERE id=$albumId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取属性图片
	* @param $images   图片路径
	* @return $data
	*/
	public function getAlbumByImages($images){
		$sql = "SELECT * FROM __PREFIX__product_album WHERE images='$images'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加商品属性图片
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertProductAlbum($data){
		$rd = array('status'=>-1);
		$model = M('product_album');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改商品属性图片
	 * @param $id     图片编号
	 * @param $object 图片数组
	 * @return $rd
	 */
	public function updateProductAlbum($id, $object){
		$rd = array('status'=>-1);
		$id = (int)$id;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = M('product_album')->where("id=".$id)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品属性图片
	 * @param $id     图片编号
	 * @return $rd
	 */
	public function deleteProductAlbum($id){
		$rd = array('status'=>-1);
	    $rs = M('product_album')->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 通过筛选条件获取商品列表
	* @param $object   条件数组
	* @return $data
	*/
	public function getProductRelationCountByObject($object){
		$shopId   = (int)$object['shopId'];
		$groupId  = (int)$object['groupId'];
		$classId  = (int)$object['classId'];
		$typeId   = (int)$object['typeId'];
		$brandId  = (int)$object['brandId'];
		$catId    = (int)$object['catId'];
		$new      = $object['new'];
		$hot      = $object['hot'];
		$position = $object['position'];
		$min      = (int)$object['min'];
		$max      = (int)$object['max'];
		$keyword  = $object['keyword'];
		
		$where = "p.is_on_sale<>0 AND p.recycle<>1";
		if($shopId > 0){
			$where .= " AND p.shop_id=$shopId";
		}
		if($groupId == 0){
			$where .= ' AND p.is_shop=0';
		}
		if($classId > 0){
			$where .= " AND p.classify_id=".$classId;
		}
		if($typeId > 0){
			$where .= " AND p.type_id=".$typeId;
		}
		if($brandId > 0){
			$where .= " AND p.brand_id=".$brandId;
		}
		if($catId > 0){
			$where .= " AND p.cat_id=".$catId;
		}
		if(!empty($new)){
			$where .= " AND p.is_new=1";
		}
		if(!empty($hot)){
			$where .= " AND p.is_hot=1";
		}
		if(!empty($position)){
			$where .= " AND p.adv_position<>0";
		}
		if($min >= 0 && $max > 0){
			$where .= " AND p.pro_price BETWEEN $min AND $max";
		}
		if(!empty($keyword)){
			$where .= " AND p.pro_name like '%".$keyword."%'";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__product_relation AS r
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 通过筛选条件获取商品列表
	* @param $object   条件数组
	* @return $data
	*/
	public function getProductRelationByObject($object){
		$groupId  = (int)$object['groupId'];
		$shopId   = (int)$object['shopId'];
		$vip      = (int)$object['vip'];
		$classId  = (int)$object['classId'];
		$typeId   = (int)$object['typeId'];
		$brandId  = (int)$object['brandId'];
		$catId    = (int)$object['catId'];
		$new      = $object['new'];
		$hot      = $object['hot'];
		$position = $object['position'];
		$min      = (int)$object['min'];
		$max      = (int)$object['max'];
		$groupby  = $object['groupby'];
		$sort     = $object['sort'];
		$keyword  = $object['keyword'];
		$m        = $object['m'];
		$n        = $object['n'];
		
		$where = "p.is_on_sale<>0 AND p.recycle<>1";
		if($groupId == 0){
			$where .= ' AND p.is_shop=0';
		}
		if($shopId > 0){
			$where .= " AND p.shop_id=$shopId";
		}
		if($classId > 0){
			$where .= " AND p.classify_id=".$classId;
		}
		if($typeId > 0){
			$where .= " AND p.type_id=".$typeId;
		}
		if($brandId > 0){
			$where .= " AND p.brand_id=".$brandId;
		}
		if($catId > 0){
			$where .= " AND p.cat_id=".$catId;
		}
		if(!empty($new)){
			$where .= " AND p.is_new=1";
		}
		if(!empty($hot)){
			$where .= " AND p.is_hot=1";
		}
		if(!empty($position)){
			$where .= " AND p.adv_position<>0";
		}
		if($min >= 0 && $max > 0){
			$where .= " AND p.pro_price BETWEEN $min AND $max";
		}
		if(!empty($keyword)){
			$where .= " AND p.pro_name like '%".$keyword."%'";
		}
		if($groupby > 0){
			$group = " GROUP BY p.id";
		}
		if($sort == 'default'){
			$order = " p.sale_quantity DESC,p.click_date DESC";
		}elseif($sort == 'new'){
			$order = " p.add_time DESC";
		}elseif($sort == 'comment'){
			$order = " p.comment_quantity DESC";
		}elseif($sort == 'sale'){
			$order = " p.sale_quantity DESC";
		}elseif($sort == 'down'){
			$order = " p.pro_price DESC";
		}elseif($sort == 'position'){
			$order = " p.adv_position ASC";
		}elseif($sort == 'up'){
			$order = " p.pro_price ASC";
		}else{
			$order = " p.click_date DESC";
		}
		if(isset($m) && isset($n)){
			$limit = " LIMIT $m,$n";
		}
		$sql = "SELECT r.id AS rid,r.pro_attr,r.one_price,r.two_price,r.three_price,r.four_price,r.pro_stock,p.* 
				FROM __PREFIX__product_relation AS r
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE $where $group
				ORDER BY $order $limit";
		$data = $this->query($sql);
		foreach($data as &$vo){
			$price = $vo['one_price'];
			if($groupId == 1){
				if($vo['two_price'] > 0){
					$price = $vo['two_price'];
				}
				if($num >= 5 && $vo['four_price'] > 0){
					$price = $vo['four_price'];
				}
			}
			if($vip == 1){
				if($vo['three_price'] > 0){
					$price = $vo['three_price'];
				}elseif($vo['two_price'] > 0){
					$price = $vo['two_price'];
				}
			}
			$vo['price'] = $price;
		}
		unset($vo);
		return $data;
	}
	
	/**
	* 根据商品属性获取价格
	* @param $relationId 关联编号
	* @param $attr       选择属性
	* @param $num        购买数量
	* @param $groupId    用户组
	* @param $vip        是否会员
	* @return $data
	*/
	public function getProductPriceByRelationId($relationId, $num=1, $groupId=0, $vip=0){
		$sql = "SELECT one_price,two_price,three_price,four_price,pro_stock FROM __PREFIX__product_relation WHERE id=$relationId";
		$relation = $this->queryRow($sql);
		
		$data['status'] = 1;
		$data['num']    = $num;
		if($relation['pro_stock'] < $num || $relation['pro_stock'] == 0){
			$data['status'] = 0;
			$data['num']    = $relation['pro_stock'];
		}
		
		$price = $relation['one_price'];
		if($groupId == 1){
			if($relation['two_price'] > 0){
				$price = $relation['two_price'];
			}
			if($data['num'] >= 5 && $relation['four_price'] > 0){
				$price = $relation['four_price'];
			}
		}
		if($vip == 1){
			if($relation['three_price'] > 0){
				$price = $relation['three_price'];
			}elseif($relation['two_price'] > 0){
				$price = $relation['two_price'];
			}
		}
		$data['price'] = $price;
		return $data;
	}
	
	/**
	* 根据商品属性获取价格
	* @param $productId 商品编号
	* @param $attr      选择属性
	* @param $num       购买数量
	* @param $groupId   用户组
	* @param $vip       是否会员
	* @return $data
	*/
	public function getProductAttrByPrice($productId, $attr, $num='1', $groupId=0, $vip=0){
		$sql = "SELECT one_price,two_price,three_price,four_price,pro_stock FROM __PREFIX__product_relation WHERE pro_id=$productId AND pro_attr='".$attr."'";
		$data = $this->queryRow($sql);
		
		$data['status'] = 1;
		$data['num']    = $num;
		if($data['pro_stock'] < $num || $data['pro_stock'] == 0){
			$data['status'] = 0;
			$data['num']    = $data['pro_stock'];
		}
		
		$price = $data['one_price'];
		if($groupId == 1){
			if($data['two_price'] > 0){
				$price = $data['two_price'];
			}
			if($data['num'] >= 5 && $data['four_price'] > 0){
				$price = $data['four_price'];
			}
		}
		if($vip == 1){
			if($data['three_price'] > 0){
				$price = $data['three_price'];
			}elseif($data['two_price'] > 0){
				$price = $data['two_price'];
			}
		}
		$data['price'] = $price;
		return $data;
	}

	/**
	* 获取商品属性关联
	* @param $productId 商品编号
	* @return $data
	*/
	public function getRelationByProductId($productId){
		$sql = "SELECT * FROM __PREFIX__product_relation WHERE pro_id=$productId";
		$data = $this->query($sql);
		return $data;
	}

	/**
	* 获取商品属性关联
	* @param $productId 商品编号
	* @param $attr      属性编号
	* @return $data
	*/
	public function getRelationByProductIdAndAttrId($productId, $attr){
		if(is_array($attr)){
			$combineArray = CBWCombinationString('', $attr, $combineArray, strlen(implode("", $attr))+count($attr)-1);
			$sql = "SELECT * FROM __PREFIX__product_relation WHERE pro_id=$productId AND pro_attr in('".implode("','", $combineArray)."')";
		}else{
			$sql = "SELECT * FROM __PREFIX__product_relation WHERE pro_id=$productId AND pro_attr=$attr";
		}
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取属性关联
	* @param $relationId   关联编号
	* @return $data
	*/
	public function getRelationByRelationId($relationId){
		$sql = "SELECT r.id AS rid,r.pro_attr,r.one_price,r.two_price,r.three_price,r.four_price,r.pro_stock,p.* 
				FROM __PREFIX__product_relation AS r
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE r.id=$relationId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加商品属性关联
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertProductRelation($data){
		$rd = array('status'=>-1);
		$model = M('product_relation');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改商品属性关联
	 * @param $id     关联编号
	 * @param $object 关联数组
	 * @return $rd
	 */
	public function updateProductRelation($id, $object){
		$rd = array('status'=>-1);
		$id = (int)$id;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = M('product_relation')->where("id=".$id)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品属性关联
	 * @param $id     关联编号
	 * @return $rd
	 */
	public function deleteProductRelation($id){
		$rd = array('status'=>-1);
	    $rs = M('product_relation')->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除商品属性关联
	 * @param $productId     商品编号
	 * @return $rd
	 */
	public function deleteProductRelationByProductId($productId){
		$rd = array('status'=>-1);
	    $rs = M('product_relation')->where("pro_id=".$productId)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 商品点击数统计
	 * @param $productId 商品编号
	 * @param $type      类型
	 */
	public function statistics($productId, $type){
		$sql = "SELECT pro_id FROM __PREFIX__product_relation WHERE id=$productId";
		$data = $this->queryRow($sql);

		if($type == 'click'){
			$sql = "UPDATE __PREFIX__products set click_times=click_times+1,click_date=click_times+1 WHERE id=".$data['pro_id'];
		}else if($type == 'sale'){
			$sql = "UPDATE __PREFIX__products set sale_quantity=sale_quantity+1 WHERE id=".$data['pro_id'];
		}else if($type == 'comment'){
			$sql = "UPDATE __PREFIX__products set comment_quantity=comment_quantity+1 WHERE id=".$data['pro_id'];
		}else{
			return false;
		}
		$this->execute($sql);
	}
}