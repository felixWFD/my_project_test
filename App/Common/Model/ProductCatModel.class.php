<?php
namespace Common\Model;
/**
 * 分类服务类
 */
class ProductCatModel extends BaseModel {
	
	/**
	* 获取单个分类信息
	* @param $catId 编号
	* @return $data
	*/
	public function getCat($catId){
		return $this->where("id=$catId")->find();
	}
	
	/**
	* 获取分类下的子分类
	* @param $parentId 父级编号
	* @return $data
	*/
	public function getCatListById($parentId){
		if(is_array($parentId)){
			$strParent = implode(",", $parentId);
			return $this->where("fid in ($strParent) AND is_show=1")->order("sort ASC")->select();
		}
		return $this->where("fid=$parentId AND is_show=1")->order("sort ASC")->select();
	}
	
	/**
	* 获取分类下所有子分类
	* @param $parentId 父级编号
	* @return $data
	*/
	public function getAllCategory($parentId){
		$sql = "SELECT * FROM __PREFIX__product_cat WHERE fid=$parentId AND is_show=1 ORDER BY sort ASC";
		$data = $this->query($sql);
		if($data){
			foreach($data as &$vo){
				$vo['category'] = $this->getAllCategory($vo['id']);
			}
		}
		unset($vo);
		return $data;
	}
}