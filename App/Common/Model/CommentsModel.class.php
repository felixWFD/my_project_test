<?php
namespace Common\Model;
/**
 * 评价服务类
 */
class CommentsModel extends BaseModel {
	/**
	* 商品评论数量
	* @param $productId  商品编号
	* @return $length
	*/
	public function getCommentCountByProductId($productId){	
		$sql = "SELECT COUNT(*) as count FROM __PREFIX__comments As c
				LEFT JOIN __PREFIX__product_relation AS r ON r.id=c.pro_id
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE c.status=1 AND p.id=$productId";
		$data = $this->query($sql);
		return $data[0]['count'];
	}
	
	/**
	* 商品评论
	* @param $productId  商品编号
	* @param $m          开始位置
	* @param $n          读取个数
	* @return $data
	*/
	public function getCommentByProductId($productId, $m=0, $n=10){	
		$sql = "SELECT c.* FROM __PREFIX__comments AS c
				LEFT JOIN __PREFIX__product_relation AS r ON r.id=c.pro_id
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE c.status=1 AND p.id=$productId 
				ORDER BY c.id DESC 
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺评价数量
	* @param $object 条件数组
	* @param $typeId 评价类型
	* @param $m      开始位置
	* @param $n      读取个数
	* @return $data
	*/
	public function getCommentsCountByObject($object){
		$shopId = $object['shopId'];
		$score  = $object['score'];
		$start  = $object['start'];
		$end    = $object['end'];

		$where = "c.id<>0";
		if(!empty($shopId)){
			$where .= " AND c.shop_id=".$shopId;
		}
		if(!empty($score)){
			$where .= " AND c.score=".$score;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (c.comment_time BETWEEN $startint AND $endint)";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__comments AS c
				LEFT JOIN __PREFIX__orders AS o ON o.id=c.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id 
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取店铺评价
	* @param $object 条件数组
	* @param $m      开始位置
	* @param $n      读取个数
	* @return $data
	*/
	public function getCommentsByObject($object){
		$shopId = $object['shopId'];
		$score  = $object['score'];
		$start  = $object['start'];
		$end    = $object['end'];
		$m      = $object['m'];
		$n      = $object['n'];
		
		$where = "c.id<>0";
		if(!empty($shopId)){
			$where .= " AND c.shop_id=".$shopId;
		}
		if(!empty($score)){
			$where .= " AND c.score=".$score;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (c.comment_time BETWEEN $startint AND $endint)";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT c.*,o.order_sn,u.uname,i.thumb FROM __PREFIX__comments AS c
				LEFT JOIN __PREFIX__orders AS o ON o.id=c.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id 
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				WHERE $where
				ORDER BY c.id DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取用户评价回复
	* @param $commentId 评价编号
	* @return $data
	*/
	public function getRepliesByCommentId($commentId){
		$sql = "SELECT * FROM __PREFIX__comment_replies WHERE comment_id=$commentId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 添加评价
	* @param $data 添加数据包
	* @return $rd
	*/
	public function insertComment($data){
		$id = 0;
        if($this->create($data)){
            $id = $this->add();
        }
		return $id;
	}
	
	/**
	* 添加回复
	* @param $data 添加数据包
	* @return $rd
	*/
	public function insertReply($data){
		$id = 0;
		$modelReplies = M('comment_replies');
        if($modelReplies->create($data)){
            $id = $modelReplies->add();
        }
		return $id;
	}
}