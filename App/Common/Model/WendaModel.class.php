<?php
namespace Common\Model;
/**
 * 问答服务类
 */
class WendaModel extends BaseModel {
	protected $tableName = 'wenda_contents';
	
	/**
	* 获取问答分类信息
	* @return $data
	*/
	public function getWendaCat(){
		$sql = "SELECT * FROM __PREFIX__wenda_cat WHERE is_show=1 ORDER BY id DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取分类下问答列表
	* @param $catId 分类编号
	* @return $data
	*/
	public function getWendaList($catId){		
		$sql = "SELECT * FROM __PREFIX__wenda_contents WHERE cat_id=$catId ORDER BY id ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取问答
	* @param $wendaId 问答编号
	* @return $data
	*/
	public function getWenda($wendaId){
		$sql = "SELECT * FROM __PREFIX__wenda_contents WHERE id=$wendaId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 问答统计
	 * @param $wendaId 问答编号
	 */
	public function statistics($wendaId){		
		$this->execute("UPDATE __PREFIX__wenda_contents set clicktimes=clicktimes+1 WHERE id=".$wendaId);
	}
}