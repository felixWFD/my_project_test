<?php
namespace Common\Model;
/**
 * 论坛服务类
 */
class ForumModel extends BaseModel {
	protected $tableName = 'bbs_forum';
	
	/**
	 * 获取版块信息
	 * @param $forumId  版块编号
	 * @return $data
	 */
    public function getForumById($forumId){
		$sql = "SELECT * FROM __PREFIX__bbs_forum WHERE id=$forumId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取版块信息
	 * @param $keyword  关键词
	 * @param $number   返回数量
	 * @return $data
	 */
    public function getForumByKeyword($keyword, $number=7){
		$sql = "SELECT * FROM __PREFIX__bbs_forum WHERE level=2 AND tags like '%".$keyword."%' ORDER BY id DESC LIMIT 0,$number";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取子版块信息
	 * @param $forumId  版块编号
	 * @return $data
	 */
    public function getForumsByForumId($forumId=0){
		$sql = "SELECT * FROM __PREFIX__bbs_forum WHERE fid=$forumId ORDER BY displayorder ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 版块统计
	 * @param $forumId  版块编号
	 * @param $category 主题类别
	 * @return $data
	 */
    public function getForumStatisticsByForumId($forumId, $category){
		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_thread WHERE displayorder in(0,1,2,3) AND fid=$forumId AND category=$category";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取主题信息
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getThreadById($threadId){
		$sql = "SELECT t.*, f.name, f.description as fdesc, f.icon, u.uname, i.exp, i.thumb, i.virtual_money 
			FROM __PREFIX__bbs_thread AS t
			LEFT JOIN __PREFIX__bbs_forum AS f ON f.id=t.fid
			LEFT JOIN __PREFIX__users AS u ON u.id=t.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE t.id=$threadId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 在某时间内发帖用户数
	 * @param $object  条件对象
	 * @return $data
	 */
    public function getUsersCountByObject($object){
		$fid      = $object['fid'];
		$category = $object['category'];
		$dateline = $object['dateline'];

		$where = "displayorder in(0,1,2,3)";
		if(!empty($fid)){
			if(is_array($fid)){
				$fids = implode(",", $fid);
				$where .= " AND fid in($fids)";
			}else{
				$where .= " AND fid=$fid";			
			}
		}
		if(!empty($category)){
			$where .= " AND category=$category";
		}
		if(!empty($dateline)){
			$where .= " AND dateline>=$dateline";
		}
		
		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_thread WHERE $where GROUP BY authorid";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取主题信息
	 * @param $object  条件对象
	 * @return $data
	 */
    public function getThreadCountByObject($object){
		$fid      = $object['fid'];
		$category = $object['category'];
		$sub      = $object['sub'];
		$authorId = $object['authorId'];
		$keyword  = $object['keyword'];
		$dateline = $object['dateline'];
		$replies  = $object['replies'];

		$where = "t.displayorder in(0,1,2,3)";
		if(!empty($fid)){
			if(is_array($fid)){
				$fids = implode(",", $fid);
				$where .= " AND t.fid in($fids)";
			}else{
				$where .= " AND t.fid=$fid";			
			}
		}
		if(!empty($category)){
			if(is_array($category)){
				$categorys = implode(",", $category);
				$where .= " AND t.category in($categorys)";
			}else{
				$where .= " AND t.category=$category";	
			}
		}
		if(!empty($sub)){
			$where .= " AND t.sub=$sub";
		}
		if(!empty($authorId)){
			$where .= " AND t.authorid=$authorId";			
		}
		if(!empty($keyword)){
			$where .= " AND (t.subject like '%".$keyword."%' OR t.description like '%".$keyword."%' OR t.content like '%".$keyword."%') ";
		}
		if(!empty($dateline)){
			$where .= " AND t.dateline>=$dateline";
		}
		if(!empty($replies)){
			$where .= " AND t.replies=0";
		}
		
		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_thread AS t
			LEFT JOIN __PREFIX__bbs_forum AS f ON f.id=t.fid
			WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取主题信息
	 * @param $object  条件对象
	 * @return $data
	 */
    public function getThreadByObject($object){
		$fid      = $object['fid'];
		$category = $object['category'];
		$sub      = $object['sub'];
		$authorId = $object['authorId'];
		$keyword  = $object['keyword'];
		$dateline = $object['dateline'];
		$replies  = $object['replies'];
		$sort     = $object['sort'];
		$m        = $object['m'];
		$n        = $object['n'];

		$where = "t.displayorder in(0,1,2,3)";
		if(!empty($fid)){
			if(is_array($fid)){
				$fids = implode(",", $fid);
				$where .= " AND t.fid in($fids)";
			}else{
				$where .= " AND t.fid=$fid";			
			}
		}
		if(!empty($category)){
			if(is_array($category)){
				$categorys = implode(",", $category);
				$where .= " AND t.category in($categorys)";
			}else{
				$where .= " AND t.category=$category";	
			}
		}
		if(!empty($sub)){
			$where .= " AND t.sub=$sub";
		}
		if(!empty($authorId)){
			$where .= " AND t.authorid=$authorId";			
		}
		if(!empty($keyword)){
			$where .= " AND (t.subject like '%".$keyword."%' OR t.description like '%".$keyword."%' OR t.content like '%".$keyword."%') ";
		}
		if(!empty($dateline)){
			$where .= " AND t.dateline>=$dateline";
		}
		if(!empty($replies)){
			$where .= " AND t.replies=0";
		}
		$order = "";
		if($sort == 'heats'){
			$order = "ORDER BY t.heats DESC";
		}elseif($sort == 'new'){
			$order = "ORDER BY t.dateline DESC";
		}elseif($sort == 'helpful'){
			$order = "ORDER BY t.votes DESC";
		}else{
			$order = "ORDER BY t.id DESC";
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT t.*,f.name,f.icon,u.uname,i.thumb
			FROM __PREFIX__bbs_thread AS t
			LEFT JOIN __PREFIX__bbs_forum AS f ON f.id=t.fid
			LEFT JOIN __PREFIX__users AS u ON u.id=t.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE $where $order $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取主题明细表信息
	 * @param $category  类型
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getScheduleByThreadId($threadId, $category=0){
		$where = "tid<>0";
		if(!empty($category)){
			$where .= " AND category=$category";
		}
		if(!empty($threadId)){
			$where .= " AND tid=$threadId";
		}
		$sql = "SELECT * FROM __PREFIX__bbs_thread_schedule WHERE $where ORDER BY displayorder ASC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加主题
	 */
	public function insertThread($data){
		$rd = array('status'=>-1);
		$model = M('bbs_thread');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新主题
	 * @param $threadId 主题编号
	 * @param $data   数据数组
	 * @return $rs
	 */
	public function updateThread($threadId, $data){
		$rd = array('status'=>-1);
		$model = M('bbs_thread');
		$rs = $model->where("id=$threadId")->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 更新主题
	 * @param $threadId 主题编号
	 * @param $field    数据字段
	 * @param $type     类型
	 * @return $rs
	 */
	public function updateThreadStatus($threadId, $field, $type){
		if($type){
			$sql = "UPDATE __PREFIX__bbs_thread set $field=$field+1 WHERE id=$threadId";
		}else{
			$sql = "UPDATE __PREFIX__bbs_thread set $field=$field-1 WHERE id=$threadId";
		}
		$this->execute($sql);
	}
	
	/**
	 * 添加主题清单
	 */
	public function insertThreadSchedule($data){
		$rd = array('status'=>-1);
		$model = M('bbs_thread_schedule');
	    if($model->create($data)){	
			$model->add();
		}
	}
	
	/**
	 * 删除主题清单
	 */
	public function deleteThreadSchedule($threadId, $category){
		$rd = array('status'=>-1);
		$model = M('bbs_thread_schedule');
		$rs = $model->where("category=$category AND tid=$threadId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 获取回帖信息
	 * @param $postId  回帖编号
	 * @return $data
	 */
    public function getPostById($postId){
		$sql = "SELECT * FROM __PREFIX__bbs_post WHERE id=$postId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取用户在主题中回帖
	 * @param $userId    用户编号
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getPostByUserId($userId, $threadId){
		$sql = "SELECT p.*,u.uname,i.thumb FROM __PREFIX__bbs_post AS p
			LEFT JOIN __PREFIX__users AS u ON u.id=p.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE p.authorid=$userId AND p.tid=$threadId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取用户满意回帖数量
	 * @param $userId    用户编号
	 * @return $data
	 */
    public function getSatisfyPostCountByUserId($userId){
		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_post WHERE satisfy=1 AND authorid=$userId";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取用户满意回帖
	 * @param $userId    用户编号
	 * @return $data
	 */
    public function getSatisfyPostByUserId($userId){
		$sql = "SELECT * AS number FROM __PREFIX__bbs_post WHERE satisfy=1 AND authorid=$userId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取回帖数量
	 * @param $object  条件对象
	 * @return $data
	 */
    public function getPostCountByObject($object){
		$fid      = $object['fid'];
		$threadId = $object['threadId'];
		$authorId = $object['authorId'];
		$satisfy  = $object['satisfy'];
		$dateline = $object['dateline'];

		$where = "p.invisible=0";
		if(!empty($fid)){
			$where .= " AND t.fid=$fid";			
		}
		if(!empty($threadId)){
			$where .= " AND p.tid=$threadId";			
		}
		if(!empty($authorId)){
			$where .= " AND p.authorid=$authorId";			
		}
		if(isset($satisfy)){
			$where .= " AND p.satisfy=$satisfy";			
		}
		if(!empty($dateline)){
			$where .= " AND p.dateline>=$dateline";
		}
		
		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_post AS p
			LEFT JOIN __PREFIX__bbs_thread AS t ON t.id=p.tid
			LEFT JOIN __PREFIX__bbs_forum AS f ON f.id=t.fid
			LEFT JOIN __PREFIX__users AS u ON u.id=p.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取回帖信息
	 * @param $object  条件对象
	 * @return $data
	 */
    public function getPostByObject($object){
		$fid      = $object['fid'];
		$threadId = $object['threadId'];
		$authorId = $object['authorId'];
		$satisfy  = $object['satisfy'];
		$dateline = $object['dateline'];
		$sort     = $object['sort'];
		$m        = $object['m'];
		$n        = $object['n'];

		$where = "p.invisible=0";
		if(!empty($fid)){
			$where .= " AND t.fid=$fid";			
		}
		if(!empty($threadId)){
			$where .= " AND p.tid=$threadId";			
		}
		if(!empty($authorId)){
			$where .= " AND p.authorid=$authorId";			
		}
		if(isset($satisfy)){
			$where .= " AND p.satisfy=$satisfy";			
		}
		if(!empty($dateline)){
			$where .= " AND p.dateline>=$dateline";
		}
		$order = "";
		if($sort == 'satisfy'){
			$order = "ORDER BY p.satisfy DESC";
		}else{
			$order = "ORDER BY p.id DESC";
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT p.*,t.subject,f.name,f.icon,u.uname,i.exp,i.thumb
			FROM __PREFIX__bbs_post AS p
			LEFT JOIN __PREFIX__bbs_thread AS t ON t.id=p.tid
			LEFT JOIN __PREFIX__bbs_forum AS f ON f.id=t.fid
			LEFT JOIN __PREFIX__users AS u ON u.id=p.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE $where $order $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取满意回帖
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getSatisfyPostByThreadId($threadId){
		$sql = "SELECT p.*,u.uname,i.exp,i.thumb FROM __PREFIX__bbs_post AS p
			LEFT JOIN __PREFIX__users AS u ON u.id=p.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE p.satisfy=1 AND p.tid=$threadId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取最后一条回帖
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getLastPostByThreadId($threadId){
		$sql = "SELECT p.*,u.uname,i.thumb FROM __PREFIX__bbs_post AS p
			LEFT JOIN __PREFIX__users AS u ON u.id=p.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE p.invisible=0 AND p.tid=$threadId
			ORDER BY p.dateline DESC";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取用户最后一条回帖
	 * @param $userId  用户编号
	 * @return $data
	 */
    public function getLastPostByUserId($userId){
		$sql = "SELECT p.*,u.uname,i.thumb FROM __PREFIX__bbs_post AS p
			LEFT JOIN __PREFIX__users AS u ON u.id=p.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE p.authorid=$userId
			ORDER BY p.dateline DESC";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加帖子
	 */
	public function insertPost($data){
		$rd = array('status'=>-1);
		$model = M('bbs_post');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新帖子
	 * @param $postId 主题编号
	 * @param $data   数据数组
	 * @return $rs
	 */
	public function updatePost($postId, $data){
		$rd = array('status'=>-1);
		$model = M('bbs_post');
		$rs = $model->where("id=$postId")->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 获取回帖排行
	 * @param $number  数量
	 * @return $data
	 */
    public function postRanking($number){
		$sql = "SELECT authorid,count(id) AS number FROM __PREFIX__bbs_post 
				WHERE satisfy=1
				GROUP BY authorid
				ORDER BY number DESC
				LIMIT 0,$number";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取分享排行
	 * @param $number  数量
	 * @return $data
	 */
    public function shareRanking($number){
		$sql = "SELECT authorid,count(id) AS number FROM __PREFIX__bbs_thread 
				WHERE category in(1,2) AND displayorder in(0,1,2,3)
				GROUP BY authorid
				ORDER BY number DESC
				LIMIT 0,$number";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取评论信息
	 * @param $commentId  评论编号
	 * @return $data
	 */
    public function getCommentById($commentId){
		$sql = "SELECT * FROM __PREFIX__bbs_comments WHERE id=$commentId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取评论数
	 * @param $acceptId  编号
	 * @return $data
	 */
    public function getCommentCountByObject($object){
		$category = $object['category'];
		$acceptId = $object['acceptId'];

		$where = "c.id<>0";
		if(isset($category)){
			$where .= " AND c.category=$category";			
		}
		if(!empty($acceptId)){
			$where .= " AND c.acceptid=$acceptId";			
		}

		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_comments AS c
			LEFT JOIN __PREFIX__users AS u ON u.id=c.authorid
			WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取评论信息
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getCommentsByObject($object){
		$category = $object['category'];
		$acceptId = $object['acceptId'];
		$m        = $object['m'];
		$n        = $object['n'];

		$where = "c.id<>0";
		if(isset($category)){
			$where .= " AND c.category=$category";			
		}
		if(!empty($acceptId)){
			$where .= " AND c.acceptid=$acceptId";			
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT c.*, u.uname,i.thumb FROM __PREFIX__bbs_comments AS c
			LEFT JOIN __PREFIX__users AS u ON u.id=c.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE $where ORDER BY c.dateline DESC, c.id DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取最后一条评论
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getLastCommentByThreadId($threadId){
		$sql = "SELECT c.*,u.uname,i.thumb FROM __PREFIX__bbs_comments AS c
			LEFT JOIN __PREFIX__users AS u ON u.id=c.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE c.status=0 AND c.category=0 AND c.acceptid=$threadId
			ORDER BY c.dateline DESC";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取用户最后一条评论
	 * @param $userId  用户编号
	 * @return $data
	 */
    public function getLastCommentByUserId($userId){
		$sql = "SELECT c.*,u.uname,i.thumb FROM __PREFIX__bbs_comments AS c
			LEFT JOIN __PREFIX__users AS u ON u.id=c.authorid
			LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
			WHERE c.authorid=$userId
			ORDER BY c.dateline DESC";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取评论
	 */
    public function insertComment($data){
		$rd = array('status'=>-1);
		$model = M('bbs_comments');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 获取收藏信息
	 * @param $userId    用户编号
	 * @param $threadId  主题编号
	 * @return $data
	 */
    public function getCollectByUserId($userId, $threadId){
		$sql = "SELECT * FROM __PREFIX__bbs_collect WHERE userid=$userId AND tid=$threadId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取用户全部收藏数
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getCollectCountByObject($object){
		$userId   = $object['userId'];
		$threadId = $object['threadId'];

		$where = "dateline <>0";
		if(!empty($userId)){
			$where .= " AND userid=$userId";			
		}
		if(!empty($threadId)){
			$where .= " AND tid=$threadId";			
		}

		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_collect WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取用户全部收藏信息
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getCollectByObject($object){
		$userId   = $object['userId'];
		$threadId = $object['threadId'];
		$m        = $object['m'];
		$n        = $object['n'];

		$where = "c.dateline <>0";
		if(!empty($userId)){
			$where .= " AND c.userid=$userId";			
		}
		if(!empty($threadId)){
			$where .= " AND t.id=$threadId";			
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT c.*, t.subject, t.replies, t.votes, t.favtimes FROM __PREFIX__bbs_collect AS c
			LEFT JOIN __PREFIX__bbs_thread AS t ON t.id=c.tid
			WHERE $where ORDER BY c.dateline DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加收藏信息
	 */
    public function insertCollect($data){
		$rd = array('status'=>-1);
		$model = M('bbs_collect');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 删除收藏信息
	 * @param $userId    用户编号
	 * @param $threadId  主题编号
	 * @return $rs
	 */
	public function deleteCollect($userId, $threadId){
		$rd = array('status'=>-1);
		$model = M('bbs_collect');
		$rs = $model->where("userid=$userId AND tid=$threadId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 获取投票信息
	 * @param $userId    用户编号
	 * @param $voteId    投票编号
	 * @param $category  投票类别
	 * @return $data
	 */
    public function getVoteByUserId($userId, $voteId, $category){
		$sql = "SELECT * FROM __PREFIX__bbs_vote WHERE category=$category AND authorid=$userId AND voteid=$voteId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加投票信息
	 */
    public function insertVote($data){
		$rd = array('status'=>-1);
		$model = M('bbs_vote');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新投票
	 * @param $id        主题或帖子编号
	 * @param $category  投票类别
	 * @param $type      类型
	 * @return $data
	 */
    public function updateVote($id, $category, $type){
		if($category){
			if($type == 'yes'){
				$sql = "UPDATE __PREFIX__bbs_post set votes=votes+1 WHERE id=$id";
			}else{
				$sql = "UPDATE __PREFIX__bbs_post set votes=votes-1 WHERE id=$id";
			}
			$this->execute($sql);
			$data = $this->getPostById($id);
		}else{
			if($type == 'yes'){
				$sql = "UPDATE __PREFIX__bbs_thread set votes=votes+1 WHERE id=$id";
			}else{
				$sql = "UPDATE __PREFIX__bbs_thread set votes=votes-1 WHERE id=$id";
			}
			$this->execute($sql);
			$data = $this->getThreadById($id);
		}
		return $data['votes'];
	}
	
	/**
	 * 删除投票
	 * @param $voteId   编号
	 * @return $rs
	 */
	public function deleteVote($voteId){
		$rd = array('status'=>-1);
		$model = M('bbs_vote');
		$rs = $model->where("id=$voteId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 获取标签
	 * @param $number  数量
	 * @return $data
	 */
    public function getTags($number){
		$sql = "SELECT * FROM __PREFIX__bbs_tags ORDER BY inputs DESC LIMIT 0,$number";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取标签
	 * @param $name  标签
	 * @return $data
	 */
    public function getTagsByName($name){
		$sql = "SELECT * FROM __PREFIX__bbs_tags WHERE name='$name'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加标签
	 */
	public function insertTags($data){
		$rd = array('status'=>-1);
		$model = M('bbs_tags');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新标签状态
	 * @param $tags  标签
	 * @param $type  类型
	 * @return $data
	 */
    public function updateTags($tags, $type){
		if($type){
			$sql = "UPDATE __PREFIX__bbs_tags set inputs=inputs+1 WHERE name='$tags'";
		}else{
			$sql = "UPDATE __PREFIX__bbs_tags set inputs=inputs-1 WHERE name='$tags'";
		}
		$this->execute($sql);
	}
	
	/**
	 * 获取用户通知
	 * @param $noticeId  消息编号
	 * @return $data
	 */
    public function getNoticeById($noticeId){
		$sql = "SELECT * FROM __PREFIX__bbs_notice WHERE id=$noticeId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取用户通知数量
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getNoticeCountByObject($object){
		$receiveId = intval($object['receiveId']);
		$status    = $object['status'];
		$m         = $object['m'];
		$n         = $object['n'];
		
		$where = "receiveid=$receiveId";
		if(isset($status)){
			$where .= " AND status=$status";
		}
		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_notice WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取用户通知
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getNoticeByObject($object){
		$receiveId = intval($object['receiveId']);
		$status    = $object['status'];
		$m         = $object['m'];
		$n         = $object['n'];
		
		$where = "receiveid=$receiveId";
		if(isset($status)){
			$where .= " AND status=$status";
		}
		$sql = "SELECT * FROM __PREFIX__bbs_notice WHERE $where ORDER BY dateline DESC LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加通知
	 */
	public function insertNotice($data){
		$rd = array('status'=>-1);
		$model = M('bbs_notice');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新通知
	 * @param $noticeId    通知编号
	 * @return $rs
	 */
	public function updateNotice($noticeId, $data){
		$rd = array('status'=>-1);
		$model = M('bbs_notice');
		$rs = $model->where("id=$noticeId")->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 删除通知信息
	 * @param $noticeId    通知编号
	 * @return $rs
	 */
	public function deleteNotice($noticeId){
		$rd = array('status'=>-1);
		$model = M('bbs_notice');
		$rs = $model->where("id=$noticeId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 获取动态活动数
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getActivityCountByObject($object){
		$authorId = $object['authorId'];
		$threadId = $object['threadId'];

		$where = "a.id<>0";
		if(!empty($authorId)){
			$where .= " AND a.authorId=$authorId";			
		}
		if(!empty($threadId)){
			$where .= " AND a.tid=$threadId";			
		}

		$sql = "SELECT count(*) AS number FROM __PREFIX__bbs_activity AS a
			LEFT JOIN __PREFIX__users AS u ON u.id=a.authorid
			LEFT JOIN __PREFIX__bbs_thread AS t ON t.id=a.tid
			WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 获取动态活动信息
	 * @param $object  条件数组
	 * @return $data
	 */
    public function getActivityByObject($object){
		$authorId = $object['authorId'];
		$threadId = $object['threadId'];
		$m        = $object['m'];
		$n        = $object['n'];

		$where = "a.id<>0";
		if(!empty($authorId)){
			$where .= " AND a.authorId=$authorId";			
		}
		if(!empty($threadId)){
			$where .= " AND a.tid=$threadId";			
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT a.*, u.uname, t.subject FROM __PREFIX__bbs_activity AS a
			LEFT JOIN __PREFIX__users AS u ON u.id=a.authorid
			LEFT JOIN __PREFIX__bbs_thread AS t ON t.id=a.tid
			WHERE $where ORDER BY a.dateline DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 添加经验值与草包豆
	 */
    public function insertActivity($data){
		$rd = array('status'=>-1);
		$model = M('bbs_activity');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 今天获取的草包豆或经验值
	 * @param $userId   用户编号
	 * @param $category 加减
	 * @param $type     类型
	 * @return $data
	 */
    public function getDayActivityByUserId($userId, $category, $type){
		$now = strtotime(date('Y-').date('m-').date('d ').'00:00:00');
		$sql = "SELECT sum($type) AS number FROM __PREFIX__bbs_activity WHERE authorid=$userId AND category=$category AND dateline>$now";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	 * 主题主题热度值统计
	 * @param $threadId 主题编号
	 */
	public function statistics($threadId){
		$sql = "UPDATE __PREFIX__bbs_thread set heats=heats+1 WHERE id=$threadId";
		$this->execute($sql);
	}
	
}