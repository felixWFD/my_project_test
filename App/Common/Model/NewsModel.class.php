<?php
namespace Common\Model;
/**
 * 新闻服务类
 */
class NewsModel extends BaseModel {
	protected $tableName = 'news_contents';
	/**
	* 获取新闻分类信息
	* @return $data
	*/
	public function getNewsCat(){
		$sql = "SELECT * FROM __PREFIX__news_cat WHERE is_show=1 ORDER BY id DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取新闻
	* @param $newsId 新闻编号
	* @return $data
	*/
	public function getNews($newsId){
		$sql = "SELECT * FROM __PREFIX__news_contents WHERE id=$newsId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 同分类下其他新闻
	* @param $newsId  新闻编号
	* @param $catId   分类编号
	* @return $data
	*/
	public function getNewsByCat($newsId, $catId){
		$sql = "SELECT * FROM __PREFIX__news_contents WHERE is_show<>0 AND cat_id=$catId AND id<>$newsId ORDER BY create_time DESC LIMIT 0,7";
		$data = $this->query($sql);
		foreach($data as $key=>$vo){
			if(empty($vo['thumbnail'])){
				$data[$key]['grabimages'] = CBWGrabImages(htmlspecialchars_decode($vo['content']));  //抓取内容第一张图片为列表图
			}
		}
		return $data;
	}
	
	/**
	* 通过筛选条件获取新闻列表
	* @param $obj 条件
	* @return $data
	*/
	public function getNewsList($obj){		
		$catId = (int)$obj['catId'];
		$m     = (int)$obj['m'];
		$n     = (int)$obj['n'];

		$where = "id>0";
		if(!empty($catId)){
			$where .= " AND cat_id=$catId";
		}
		$sql = "SELECT * FROM __PREFIX__news_contents WHERE ".$where." ORDER BY create_time DESC LIMIT $m,$n";
		$data = $this->query($sql);
		foreach($data as $key=>$vo){
			if(empty($vo['thumbnail'])){
				$data[$key]['grabimages'] = CBWGrabImages(htmlspecialchars_decode($vo['content']));  //抓取内容第一张图片为列表图
			}
		}
		return $data;
	}
	
	/**
	 * 新闻统计
	 * @param $newsId 新闻编号
	 */
	public function statistics($newsId){		
		$this->execute("UPDATE __PREFIX__news_contents set clicktimes=clicktimes+1 WHERE id=".$newsId);
	}
}