<?php
namespace Common\Model;
/**
 * 县区服务类
 */
class AreaModel extends BaseModel {
	protected $tableName = 'addr_area';
	/**
	 * 获取城市下的地区列表
	 * @param $cityId 城市编号
	 * @return $data
	 */
	public function getAreaByCityId($cityId){
		$data = S("CBW_CACHE_AREA_".$cityId);
 		if(!$data){
			$sql = "SELECT * FROM __PREFIX__addr_area WHERE two_id=$cityId ORDER BY id ASC";
			$data = $this->query($sql);
			S("CBW_CACHE_AREA_".$cityId,$data,2592000);
 		}
		return $data;
	}
 
	/**
	 * 获取省份下的城市列表
	 * @param $provinceId 省份编号
	 * @return $data
	 */
	public function getCityByProvinceId($provinceId){
		$data = S("CBW_CACHE_CITY_".$provinceId);
 		if(!$data){
			$sql = "SELECT * FROM __PREFIX__addr_city WHERE one_id=$provinceId ORDER BY id ASC";
			$data = $this->query($sql);
			S("CBW_CACHE_CITY_".$provinceId,$data,2592000);
 		}
		return $data;
	}
 
	/**
	 * 获取省份列表
	 * @return $data
	 */
	public function getProvince(){
		$data = S("CBW_CACHE_PROVINCE");
 		if(!$data){
			$sql = "SELECT * FROM __PREFIX__addr_province ORDER BY id ASC";
			$data = $this->query($sql);
			S("CBW_CACHE_PROVINCE",$data,2592000);
 		}
		return $data;
	}
 
	/**
	 * 获取划分方位省份列表
	 * @return $data
	 */
	public function getPositionProvince(){
		$position = array('0'=>'直辖市','1'=>'华南','2'=>'中南','3'=>'东北','4'=>'西南','5'=>'华北','6'=>'西北','7'=>'华东','8'=>'其他');
		$data = array();
		$i = 0;
		foreach($position as $key=>$val){
			$data[$i]['name'] = $val;
			$data[$i]['area'] = $this->query("SELECT id,addr FROM __PREFIX__addr_province where position=$key ORDER BY id ASC");
			$i++;
		}
		return $data;
	}
	
	/**
	 * 获取省份信息
	 * @param $provinceId 省份编号
	 * @return $data
	 */
	public function getProvinceById($provinceId){
		$sql = "SELECT * FROM __PREFIX__addr_province WHERE id=$provinceId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取省份信息
	 * @param $provinceName 省份名称
	 * @return $data
	 */
	public function getProvinceByName($provinceName){
		$sql = "SELECT * FROM __PREFIX__addr_province WHERE addr like '$provinceName'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取城市信息
	 * @param $cityId 城市编号
	 * @return $data
	 */
	public function getCityById($cityId){
		$sql = "SELECT * FROM __PREFIX__addr_city WHERE id=$cityId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取城市信息
	 * @param $cityName 城市名称
	 * @return $data
	 */
	public function getCityByName($cityName){
		$sql = "SELECT * FROM __PREFIX__addr_city WHERE addr like '%$cityName%'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 随机获取城市信息
	 * @return $data
	 */
	public function getRandCity(){
		$rd = 0;
		do{
			$cityId = rand(1,364);
			$sql = "SELECT * FROM __PREFIX__addr_city WHERE id=$cityId";
			$data = $this->queryRow($sql);
			if($data['id'] > 0){
				$rd = 1;
			}
		}while($rd < 1);
		return $data['addr'];
	}
	
	/**
	 * 获取县区信息
	 * @param $areaId 县区编号
	 * @return $data
	 */
	public function getAreaById($areaId){
		return $this->where('id='.$areaId)->find();
	}
	
	/**
	 * 获取县区信息
	 * @param $areaName 县区名称
	 * @param $cityId   城市编号
	 * @return $data
	 */
	public function getAreaByAreaName($areaName, $cityId = 0){
		if($cityId > 0){
			$sql ="SELECT id FROM __PREFIX__addr_area WHERE two_id=$cityId AND addr like '%".$areaName."%'";
			$data = $this->queryRow($sql);
			if(empty($data)){
				$areaName = msubstr($areaName, 0, 2, 'utf-8', false);
				$sql ="SELECT id FROM __PREFIX__addr_area WHERE two_id=$cityId AND addr like '%".$areaName."%'";
				$data = $this->queryRow($sql);
			}
		}else{
			$sql ="SELECT id FROM __PREFIX__addr_area WHERE addr like '%".$areaName."%'";
			$data = $this->queryRow($sql);
		}
		return $data;
	}
	  
	/**
	* 定位所在城市
	*/
	public function getDefaultCity(){
		$cityId = (int)I('city',0);
		if($cityId==0){
			$cityId = (int)session('cityId');
		}
		//检验城市有效性
		if($cityId>0){
			$sql ="SELECT id FROM __PREFIX__addr_city WHERE id=".$cityId;
			$rs = $this->query($sql);
			if($rs[0]['id']=='')$cityId = 0;
		}else{
			$cityId = (int)$_COOKIE['cityId'];
		}
		//定位城市
		if($cityId==0){
			//IP定位
			$iparea = CBWIPAddress();
			if(!empty($iparea)){
				$where = array();
				$where['addr'] = array('like', '%'.$iparea['city'].'%');
				$rs = $this->where($where)->getField('id');
				if(intval($rs)>0){
					$cityId = intval($rs);
				}else{
					$cityId = '212';  // 默认城市: 深圳市
				}
			}else{
				$cityId = '212';  // 默认城市: 深圳市
			}
		}
		session('cityId',$cityId);
		setcookie("cityId", $cityId, time()+3600*24*90);
		return $cityId;
	}
}