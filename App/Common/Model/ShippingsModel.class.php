<?php
namespace Common\Model;
/**
 * 物流服务类
 */
class ShippingsModel extends BaseModel {
	/**
	* 获取物流
	* @return $data
	*/
	public function getShippings(){
		$sql = "SELECT s.*, c.cname	FROM __PREFIX__shippings AS s
				LEFT JOIN __PREFIX__shipping_company AS c ON c.id=s.sid";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取物流
	* @param $shippingId 物流编号
	* @return $data
	*/
	public function getShippingByShippingId($shippingId){
		$sql = "SELECT * FROM __PREFIX__shippings WHERE id=$shippingId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取已设置物流信息
	* @return $data
	*/
	public function getShippingCompany(){
		$sql = "SELECT sid FROM __PREFIX__shippings";
		$shippings = $this->query($sql);
		// 查询结果为一个二维数组，需要将其组合成一维数组
        $arr = array();
        foreach ($shippings as $vo){
            $arr[] = $vo['sid'];
        }
		// 用逗号连接成字符串
        $sString = implode(', ', $arr);
		
		$sql = "SELECT * FROM __PREFIX__shipping_company WHERE id in($sString)";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取物流信息及收费
	* @param $provinceId 省份编号
	* @param $shippingId 物流编号
	* @param $shopId     店铺编号
	* @param $isshipping 是否包邮
	* @return $data
	*/
	public function getShippingsByProvinceId($provinceId, $shippingId, $shopId, $isshipping){
		$sql = "SELECT s.id,s.sid,s.is_default,c.cname,t.area,t.fee,t.fill
				FROM __PREFIX__shippings AS s
				LEFT JOIN __PREFIX__shipping_company AS c ON c.id=s.sid
				LEFT JOIN __PREFIX__shipping_template AS t ON t.shipping_id=s.id
				WHERE s.id=$shippingId AND t.shop_id=$shopId";
		$data = $this->query($sql);
		$money = 0;
		foreach($data as &$vo){
			$area = explode(",",$vo['area']);
			if(in_array($provinceId,$area)){
				if($isshipping > 0){
					$money = $vo['fill'];
				}else{
					$money = $vo['fee'];
				}
				
			}
		}
		return number_format($money, 2, '.', '');
	}
	
	/**
	* 获取常用物流公司信息
	* @param $id 公司编号
	* @return $data
	*/
	public function getCommonShippingCompany(){
		$sql = "SELECT * FROM __PREFIX__shipping_company WHERE status=1";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取物流公司信息
	* @param $id 公司编号
	* @return $data
	*/
	public function getShippingCompanyById($id){
		$sql = "SELECT * FROM __PREFIX__shipping_company WHERE id=$id";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取店铺运费模板信息
	* @param $shopId     店铺编号
	* @param $shippingId 物流编号
	* @return $data
	*/
	public function getShippingTemplateByShopId($shopId, $shippingId){
		$sql = "SELECT t.*, c.cname FROM __PREFIX__shipping_template AS t
				LEFT JOIN __PREFIX__shippings AS s ON s.id=t.shipping_id
				LEFT JOIN __PREFIX__shipping_company AS c ON c.id=s.sid
				WHERE t.shop_id=$shopId AND t.shipping_id=$shippingId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取运费模板
	* @param $templateId 模板编号
	* @return $data
	*/
	public function getTemplateByTemplateId($templateId){
		$sql = "SELECT * FROM __PREFIX__shipping_template WHERE id=$templateId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加运费模板
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertShippingTemplate($data){
		$rd = array('status'=>-1);
		$model = M('shipping_template');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 修改运费模板
	 * @param $id     编号
	 * @param $object 数组
	 * @return $rd
	 */
	public function updateShippingTemplate($id, $object){
		$rd = array('status'=>-1);
		$id = (int)$id;
        
		$data = array();
		foreach($object as $key=>$val){
			if(isset($object[$key])){
				$data[$key] = $val;
			}
		}
		$rs = M('shipping_template')->where("id=".$id)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除运费模板
	 * @param $id     编号
	 * @return $rd
	 */
	public function deleteShippingTemplate($id){
		$rd = array('status'=>-1);
	    $rs = M('shipping_template')->where("id=".$id)->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 计算普通快递运费
	* @param $provinceId 省份编号
	* @return $data
	*/
	public function getFee($provinceId, $shippingId){
		$fee = 0;
		$sql = "SELECT t.* FROM __PREFIX__shipping_template as t
				LEFT JOIN __PREFIX__shippings AS s ON s.id=t.shipping_id
				WHERE s.sid=".$shippingId;
		$temp = $this->query($sql);
		foreach($temp as $v){
			$area = explode(",",$v['area']);
			if(in_array($provinceId,$area)){
				$fee = $v['fee'];
			}
		}
		return $fee;
	}
	
	/**
	* 顺丰加急运费
	* @param $provinceId 省份编号
	* @return $data
	*/
	public function getUrgentFee($provinceId){
		$urgentFee = 0;
		$sql = "SELECT * FROM __PREFIX__shipping_template WHERE shipping_id=12";
		$data = $this->query($sql);
		foreach($data as $key=>$vo){
			$area = explode(",",$vo['area']);
			if(in_array($provinceId,$area)){
				$urgentFee = $vo['fill'];
			}
		}
		return $urgentFee;
	}
	
	/**
	* 获取快递物流信息
	* @param $number 快递单号
	* @return $data
	*/
	public function getExpressByNumber($number){
		$sql = "SELECT * FROM __PREFIX__express WHERE number='$number'";
		$data = $this->queryRow($sql);
		return $data;
	}
}