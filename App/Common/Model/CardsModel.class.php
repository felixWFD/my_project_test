<?php
namespace Common\Model;
/**
 * 保障卡服务类
 */
class CardsModel extends BaseModel {
	protected $tableName = 'cards';
	
	/**
	* 获取保障卡信息
	* @$cardId 编号
	* @return $data
	*/
	public function getCardsByCardId($cardId){
		$sql = "SELECT * FROM __PREFIX__cards WHERE id=$cardId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取保障卡信息
	* @$cardNo 卡密
	* @return $data
	*/
	public function getCardsByCardNo($cardNo){
		$sql = "SELECT * FROM __PREFIX__cards WHERE card_no='$cardNo'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取保障卡数量
	* @$object 数组对象
	* @return $data
	*/
	public function getCardsCountByObject($object){
		$userId  = $object['user_id'];
		$orderId = $object['order_id'];
		$where = "id<>0";
		if(isset($userId)){
			$where .= " AND user_id=$userId";
		}
		if(isset($orderId)){
			$where .= " AND order_id=$orderId";
		}
		$sql = "SELECT count(*) AS number FROM __PREFIX__cards WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	* 获取保障卡信息
	* @$object 数组对象
	* @return $data
	*/
	public function getCardsByObject($object){
		$userId  = $object['user_id'];
		$orderId = $object['order_id'];
		$m       = $object['m'];
		$n       = $object['n'];
		$where = "id<>0";
		if(isset($userId)){
			$where .= " AND user_id=$userId";
		}
		if(isset($orderId)){
			$where .= " AND order_id=$orderId";
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT * FROM __PREFIX__cards WHERE $where ORDER BY id ASC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 添加保障卡信息
	*/
	public function insertCards($data){
		$rd = array('status'=>-1);
		$model = M('cards');
	    if($model->create($data)){	
			$rs = $model->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新保障卡信息
	 * @param $cardId 编号
	 * @param $data   数据数组
	 * @return $rs
	 */
	public function updateCards($cardId, $data){
		$rd = array('status'=>-1);
		$model = M('cards');
		$rs = $model->where("id=$cardId")->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 删除保障卡信息
	 * @param $cardId 编号
	 * @return $rs
	 */
	public function deleteCards($cardId){
		$rd = array('status'=>-1);
		$model = M('cards');
		$rs = $model->where("id=$cardId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
}