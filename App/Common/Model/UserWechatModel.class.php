<?php
namespace Common\Model;
/**
 * 微信服务类
 */
class UserWechatModel extends BaseModel {
	/**
	* 获取微信粉丝信息
	* @param $wechatId 微信粉丝ID
	* @return $data
	*/
	public function getWechatByWechatId($wechatId){
		$sql = "SELECT * FROM __PREFIX__user_wechat WHERE id=$wechatId";
		$data = $this->queryRow($sql);
		return $data;
	}
	/**
	* 获取会员下微信粉丝数量
	* @param $userId 会员ID
	* @return $data
	*/
	public function getWechatsCountByUserId($userId){
		$sql = "SELECT count(*) AS num FROM __PREFIX__user_wechat WHERE user_id<>$userId AND fid=$userId";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取会员下的所有微信粉丝
	* @param $userId 会员ID
	* @return $data
	*/
	public function getWechatsByUserId($userId, $m='0', $n='8'){
		$sql = "SELECT * FROM __PREFIX__user_wechat
				WHERE user_id<>$userId AND fid=$userId
				ORDER BY id DESC
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取微信粉丝信息
	* @param $userId 会员ID
	* @return $data
	*/
	public function getWechatByUserId($userId){
		$sql = "SELECT * FROM __PREFIX__user_wechat WHERE user_id='$userId'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取微信粉丝信息
	* @param $unionId unionID
	* @return $data
	*/
	public function getWechatByUnionId($unionId){
		$sql = "SELECT * FROM __PREFIX__user_wechat WHERE unionid='$unionId'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取微信粉丝信息
	* @param $openId OpenID
	* @return $data
	*/
	public function getWechatByOpenId($openId){
		$sql = "SELECT * FROM __PREFIX__user_wechat WHERE openid='$openId'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加微信
	 */
	public function insertWechat($data){
		$rd = array('status'=>-1);
		$rs = $this->add($data);
	    if(false !== $rs){
			$rd['status'] = 1;
		}
		return $rd;
	}
	
	/**
	 * 修改微信
	 */
	public function updateWechat($wechatId, $data){
		$rd = array('status'=>-1);
		$wechatId = (int)$wechatId;
		$rs = $this->where("id=".$wechatId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
};
?>