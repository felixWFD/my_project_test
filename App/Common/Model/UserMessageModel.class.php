<?php
namespace Common\Model;
/**
 * 消息服务类
 */
class UserMessageModel extends BaseModel {
	/**
	 * 获取用户消息数量
	 * @param $userId 用户编号
	 * @param $type   消息类型
	 * @return $num
	 */
	public function getMessageCount($userId, $type){
		
		$where = "um.user_id=$userId";
		if($type == '1'){
			$where .= " AND um.is_read=0";
		}elseif($type == '2'){
			$where .= " AND um.is_read=1";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__user_message AS um
				LEFT JOIN __PREFIX__message AS m ON m.id=um.msg_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	 * 获取用户消息
	 * @param $userId 用户编号
	 * @param $type   消息类型
	 * @param $m      读取消息开始位置
	 * @param $n      读取消息数量
	 * @return $data
	 */
	public function getMessageByUserId($userId, $type, $m, $n){
		
		$where = "um.user_id=$userId";
		if($type == '1'){
			$where .= " AND um.is_read=0";
		}elseif($type == '2'){
			$where .= " AND um.is_read=1";
		}
		$sql = "SELECT um.*,m.title FROM __PREFIX__user_message AS um
				LEFT JOIN __PREFIX__message AS m ON m.id=um.msg_id
				WHERE $where
				ORDER BY send_date DESC
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取消息详情
	 * @param $msgId 消息编号
	 * @return $data
	 */
	public function getMessageByMsgId($msgId){
		$sql = "SELECT m.title,m.content,um.id,um.is_read,um.send_date FROM __PREFIX__message AS m
				LEFT JOIN __PREFIX__user_message AS um ON um.msg_id=m.id
				WHERE m.id=$msgId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 更新消息
	 * @param $sql  条件语句
	 * @param $data 数据数组
	 * @return $rs
	 */
	public function updateMessage($sql, $data){
		$rd = array('status'=>-1);
		$rs = $this->where($sql)->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 删除消息
	 * @param $userId  会员编号
	 * @param $ids     消息编号
	 * @return $rs
	 */
	public function deleteMessage($userId, $ids){
		$rd = array('status'=>-1);
	    $rs = $this->where("id in($ids)")->delete();
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
}