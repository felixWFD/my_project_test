<?php
namespace Common\Model;
/**
 * 售后服务类
 */
class ServiceModel extends BaseModel {
	protected $tableName = 'sale_service';
	
	/**
	* 通过筛选条件获取售后单数量
	* @param $object  条件数组
	* @return $count
	*/
	public function getServicesCountByObject($object){
		$userId  = $object['userId'];
		$shopId  = $object['shopId'];
		$type    = $object['type'];
		$keyword = $object['keyword'];
		$start   = $object['start'];
		$end     = $object['end'];
		$status  = $object['status'];
		
		$where = "ss.id<>0";
		if(!empty($userId)){
			$where .= " AND ss.user_id=".$userId;
		}
		if(!empty($shopId)){
			$where .= " AND ss.shop_id=$shopId";		
		}
		if(!empty($type)){
			$where .= " AND ss.service_type=$type";		
		}
		if(!empty($keyword)){
			$where .= " AND ss.service_sn like '%$keyword%'";
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (ss.apply_time BETWEEN $startint AND $endint)";
		}
		if(isset($status) && $status !== ''){
			$where .= " AND ss.service_status=".$status;
		}
		
		$sql = "SELECT count(*) AS num FROM __PREFIX__sale_service AS ss
				LEFT JOIN __PREFIX__product_relation AS r ON r.id=ss.pro_id
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 通过筛选条件获取售后单
	* @param $object  条件数组
	* @return $data
	*/
	public function getServicesByObject($object){
		$userId  = $object['userId'];
		$shopId  = $object['shopId'];
		$type    = $object['type'];
		$keyword = $object['keyword'];
		$start   = $object['start'];
		$end     = $object['end'];
		$status  = $object['status'];		
		$m       = $object['m'];		
		$n       = $object['n'];
		
		$where = "ss.id<>0";
		if(!empty($userId)){
			$where .= " AND ss.user_id=".$userId;
		}
		if(!empty($shopId)){
			$where .= " AND ss.shop_id=$shopId";		
		}
		if(!empty($type)){
			$where .= " AND ss.service_type=$type";		
		}
		if(!empty($keyword)){
			$where .= " AND ss.service_sn like '%$keyword%'";
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (ss.apply_time BETWEEN $startint AND $endint)";
		}
		if(isset($status) && $status !== ''){
			$where .= " AND ss.service_status=".$status;
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT ss.* FROM __PREFIX__sale_service AS ss
				LEFT JOIN __PREFIX__product_relation AS r ON r.id=ss.pro_id
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE $where 
				ORDER BY ss.apply_time DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取售后单
	* @param $sn  售后单序列号
	* @return $data
	*/
	public function getServiceBySn($sn){
		$data = $this->where("service_sn='$sn'")->find();
		return $data;
	}
	
	/**
	* 获取售后单
	* @param $sn  售后单序列号
	* @return $data
	*/
	public function getServiceAllBySn($sn){
		$sql = "SELECT s.*,o.order_sn,o.address,o.reciver_user,o.reciver_phone, a.addr, c.addr AS city_addr, ap.addr AS pro_addr 
				FROM __PREFIX__sale_service AS s
				LEFT JOIN __PREFIX__orders AS o ON o.id=s.order_id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS ap ON ap.id=a.one_id
				WHERE s.service_sn='$sn'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取售后单
	* @param $servinceId  售后单编号
	* @return $data
	*/
	public function getServiceByServinceId($servinceId){
		$sql = "SELECT s.*,o.order_sn,o.order_status,o.address,o.reciver_user,o.reciver_phone, a.addr, c.addr AS city_addr, ap.addr AS pro_addr 
				FROM __PREFIX__sale_service AS s
				LEFT JOIN __PREFIX__orders AS o ON o.id=s.order_id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS ap ON ap.id=a.one_id
				WHERE s.id=$servinceId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取订单商品售后信息
	* @param $orderId    订单编号
	* @param $productId  商品编号
	* @param $status     状态
	* @return $data
	*/
	public function getServiceByOrderId($orderId, $productId, $status){
		$where = "order_id=$orderId";
		if($productId > 0){
			$where .= " AND pro_id=$productId";
		}
		if(isset($status)){
			$where .= " AND service_status=$status";
		}else{
			$where .= " AND service_status in(1,2,3,4,5)";
		}
		$data = $this->where($where)->find();
		return $data;
	}
	
	/**
	* 获取售后跟踪信息
	* @param $serviceId  售后订单编号
	* @return $data
	*/
	public function getDetailsByServiceId($serviceId){
		$sql ="SELECT * FROM __PREFIX__service_details WHERE service_id=$serviceId ORDER BY id DESC";
		return $this->query($sql);
	}
	
	/**
	* 检查订单商品是否已经存在售后单
	* @return
	*/
	public function checkServiceByOrderId($orderId, $productId){
		$where = "service_status in(1,2,3,4,5)";
		if(!empty($orderId)){
			$where .= " AND order_id=$orderId";
		}
		if(!empty($productId)){
			$where .= " AND pro_id=$productId";
		}
		$data = $this->where($where)->count();
		if($data > 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	* 更新售后单
	* @param $serviceId 订单编号
	* @param $data    更新数据
	* @return $rt
	*/
	public function updateService($serviceId, $data){
		$rd = array('status'=>-1);
		$serviceId = (int)$serviceId;
		$rs = $this->where("id=$serviceId")->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
  
	/**
	* 添加售后单
	* @param $data 添加数据包
	* @return $serviceId
	*/
	public function addService($data){
		$rd = array('status'=>-1);
		$rs = $this->add($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
  
	/**
	* 添加售后跟踪消息
	* @param $data 添加数据包
	* @return $detailsId
	*/
	public function addServiceDetails($data){
		$detailsId = 0;
		$model = M("service_details");
        if($model->create($data)){
            $detailsId = $model->add();
        }
		return $detailsId;
	}
}