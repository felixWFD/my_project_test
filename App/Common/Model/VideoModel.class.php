<?php
namespace Common\Model;
/**
 * 视频服务类
 */
class VideoModel extends BaseModel {
	
	/**
	* 获取单个视频信息
	* @param $videoId 视频编号
	* @return $video
	*/
	public function getVideo($videoId){
		return $this->where('id='.$videoId)->find();
	}
	
	/**
	* 获取商品关联视频信息
	* @param $productId 商品编号
	* @return $video
	*/
	public function getVideoByProductId($productId){
		return $this->where('product_id='.$productId)->find();
	}
	
	/**
	* 同分类下其他视频
	* @param $videoId 视频编号
	* @param $catId   分类编号
	* @return $video
	*/
	public function getVideoByCat($videoId, $catId){
		return $this->field('id,title,simg,clicktimes')->where('status<>0 AND cat_id='.$catId.' AND id<>'.$videoId)->order("clicktimes DESC")->limit(4)->select();
	}
	
	/**
	* 通过筛选条件获取视频列表
	* @param $obj 条件
	* @return $videoList
	*/
	public function getVideoList($obj){

		$typeId  = (int)$obj['typeId'];
		$brandId = (int)$obj['brandId'];
		$modelId = (int)$obj['modelId'];
		$m       = (int)$obj['m'];
		$n       = (int)$obj['n'];

		$where = "status<>0";
		if(!empty($typeId)){
			$where .= " AND type_id=".$typeId;
		}
		if(!empty($brandId)){
			$where .= " AND brand_id=".$brandId;
		}
		if(!empty($modelId)){
			$where .= " AND cat_id=".$modelId;
		}
		return $this->field('id,title,simg,clicktimes')->where($where)->order("create_time DESC")->limit($m,$n)->select();
	}
	
	/**
	 * 视频统计
	 * @param $videoId 视频编号
	 */
	public function statistics($videoId){		
		$this->execute("UPDATE __PREFIX__video set clicktimes=clicktimes+1 WHERE id=".$videoId);
	}
}