<?php
namespace Common\Model;
/**
 * 短信验证码服务类
 */
class UserCodeModel extends BaseModel {
    /**
	 * 新增
	 */
	public function insert($data){
	 	$rd = -1;
		if($this->create($data)){
			$rs = $this->add();
			if(false !== $rs){
				$rd = $rs;
			}
		}
		return $rd;
	} 
	 
    /**
	 * 修改
	 */
	public function edit($data){
	 	$rd = -1;
		$rs = $this->save($data);
		if(false !== $rs){
			$rd = $rs;
		}
		return $rd;
	} 
	  
	/**
	 * 删除
	 */
	public function del($account){
	 	$rd = -1;
	    $rs = $this->where("account='".$account."'")->delete();
		if(false !== $rs){
		   $rd = 1;
		}
		return $rd;
	}
	
	/**
	 * 获取今天指定对象
	 */
    public function get($account){
		$date8 = time()-8*60*60;
		return $this->where("account='".$account."' AND add_time>".$date8)->find();
	}
	
	/**
	 * 验证短信验证码
	 */
    public function check($account, $code){
		$rd = 1;
		$usercode = $this->get($account);
		$nowTime = time();
		$time_num = $nowTime-$usercode['add_time'];
		if($time_num > 28800){
			$rd = -1;
		}
		if(strcmp($code, $usercode['code']) != 0){
			$rd = -2;
		}
		return $rd;
	}
	
};
?>