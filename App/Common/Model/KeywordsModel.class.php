<?php
namespace Common\Model;
/**
 * 关键词服务类
 */
class KeywordsModel extends BaseModel {
	
	/**
	* 获取全部关键词信息
	* @return $date
	*/
	public function getKeywords(){
		return $this->where('is_show=1')->order('sort ASC,id ASC')->select();
	}

}