<?php
namespace Common\Model;
/**
 * 订单服务类
 */
class OrdersModel extends BaseModel {
	/**
	* 通过筛选条件获取订单数量
	* @param  $object 条件
	* @return $count
	*/
	public function getOrdersCountByShopId($object){
		$shopId    = $object['shopId'];
		$sstatus   = $object['sstatus'];
		$type      = $object['type'];
		$keyword   = $object['keyword'];
		$ostatus   = $object['ostatus'];
		$pay       = $object['pay'];
		$start     = $object['start'];
		$end       = $object['end'];
		$orderDate = $object['date'];
		$okDate    = $object['okdate'];
		$payDate   = $object['paydate'];
		$line      = $object['line'];
		$comment   = $object['comment'];
		
		$where = "o.id<>0";
		if(isset($shopId) && $shopId !== ''){
			$where .= " AND s.shop_id=$shopId";		
		}
		if(isset($sstatus) && $sstatus !== ''){
			$where .= " AND s.shop_status=".$sstatus;
		}
		if(isset($type)){
			$where .= " AND o.order_type=".$type;
		}
		if(!empty($keyword)){
			$where .= " AND (o.order_sn like '%$keyword%' OR u.mobile like '%$keyword%')";
		}
		if(isset($ostatus) && $ostatus !== ''){
			$where .= " AND o.order_status=".$ostatus;
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($orderDate)){
			$where .= " AND o.order_date>=".$orderDate;
		}else{
			if(!empty($start) && !empty($end)){
				$startint = strtotime($start);
				$endint = strtotime($end.' 23:59:59');
				$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
			}
		}
		if(!empty($okDate)){
			$where .= " AND o.ok_date <= $okDate";
		}
		if(!empty($payDate)){
			$where .= " AND o.pay_date <= $payDate";
		}
		if(isset($line) && $line !== ''){
			$where .= " AND o.is_line=".$line;
		}
		if(isset($comment)){
			$where .= " AND o.is_comment=$comment";
		}
		
		$sql = "SELECT count(*) AS num FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__order_schedule AS s ON s.order_id=o.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 通过筛选条件获取订单列表
	* @param  $object 条件数组
	* @return $data
	*/
	public function getOrdersByShopId($object){
		$shopId    = $object['shopId'];
		$sstatus   = $object['sstatus'];
		$type      = $object['type'];
		$keyword   = $object['keyword'];
		$ostatus   = $object['ostatus'];
		$pay       = $object['pay'];
		$start     = $object['start'];
		$end       = $object['end'];
		$orderDate = $object['date'];
		$okDate    = $object['okdate'];
		$payDate   = $object['paydate'];
		$line      = $object['line'];
		$comment   = $object['comment'];
		$m   	   = $object['m'];
		$n   	   = $object['n'];

		$where = "o.id<>0";
		if(isset($shopId) && $shopId !== ''){
			$where .= " AND s.shop_id=$shopId";		
		}
		if(isset($sstatus) && $sstatus !== ''){
			$where .= " AND s.shop_status=".$sstatus;
		}
		if(isset($type)){
			$where .= " AND o.order_type=".$type;
		}
		if(!empty($keyword)){
			$where .= " AND (o.order_sn like '%$keyword%' OR u.mobile like '%$keyword%')";
		}
		if(isset($ostatus) && $ostatus !== ''){
			$where .= " AND o.order_status=".$ostatus;
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($orderDate)){
			$where .= " AND o.order_date>=".$orderDate;
		}else{
			if(!empty($start) && !empty($end)){
				$startint = strtotime($start);
				$endint   = strtotime($end.' 23:59:59');
				$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
			}
		}
		if(!empty($okDate)){
			$where .= " AND o.ok_date <= $okDate";
		}
		if(!empty($payDate)){
			$where .= " AND o.pay_date <= $payDate";
		}
		if(isset($line) && $line !== ''){
			$where .= " AND o.is_line=".$line;
		}
		if(isset($comment)){
			$where .= " AND o.is_comment=$comment";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT o.*, a.addr, c.addr AS city_addr, p.addr AS pro_addr 
				FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__order_schedule AS s ON s.order_id=o.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where
				ORDER BY o.order_date DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 通过筛选条件获取订单数量
	* @param $obj 条件
	* @return $count
	*/
	public function getOrderCount($object){
		$userId    = $object['userId'];
		$agentId   = $object['agentId'];
		$type      = $object['type'];
		$keyword   = $object['keyword'];
		$status    = $object['status'];
		$pay       = $object['pay'];
		$start     = $object['start'];
		$end       = $object['end'];
		$orderDate = $object['date'];
		$okDate    = $object['okdate'];
		$payDate   = $object['paydate'];
		$comment   = $object['comment'];
		
		$where = "o.id<>0";
		if(!empty($userId)){
			$where .= " AND o.user_id=".$userId;
		}
		if(!empty($agentId)){
			$where .= " AND o.agent_id=$agentId";		
		}
		if(isset($type)){
			$where .= " AND o.order_type=".$type;
		}
		if(!empty($keyword)){
			$where .= " AND o.order_sn like '%$keyword%'";
		}
		if(isset($status) && $status !== ''){
			if(is_array($status)){
				$where .= " AND o.order_status in(".implode(",",$status).")";
			}else{
				$where .= " AND o.order_status=".$status;
			}
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($orderDate)){
			$where .= " AND o.order_date>=".$orderDate;
		}else{
			if(!empty($start) && !empty($end)){
				$startint = strtotime($start);
				$endint = strtotime($end.' 23:59:59');
				$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
			}
		}
		if(!empty($okDate)){
			$where .= " AND o.ok_date <= $okDate";
		}
		if(!empty($payDate)){
			$where .= " AND o.pay_date <= $payDate";
		}
		if(isset($comment)){
			$where .= " AND o.is_comment=$comment";
		}
		
		$sql = "SELECT count(*) AS num FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 通过筛选条件获取订单列表
	* @param $object 条件
	* @return $data
	*/
	public function getOrderList($object){
		$userId    = $object['userId'];
		$agentId   = $object['agentId'];
		$type      = $object['type'];
		$keyword   = $object['keyword'];
		$status    = $object['status'];
		$pay       = $object['pay'];
		$start     = $object['start'];
		$end       = $object['end'];
		$orderDate = $object['date'];
		$okDate    = $object['okdate'];
		$payDate   = $object['paydate'];
		$comment   = $object['comment'];
		$m         = $object['m'];
		$n         = $object['n'];
		
		$where = "o.id<>0";
		if(!empty($userId)){
			$where .= " AND o.user_id=".$userId;
		}
		if(!empty($agentId)){
			if(is_array($agentId)){
				$agentStr = implode(",", $agentId);
				$where .= " AND o.agent_id in($agentStr)";
			}else{
				$where .= " AND o.agent_id=$agentId";
			}			
		}
		if(isset($type)){
			if(is_array($type)){
				$where .= " AND o.order_type in(".implode(",",$type).")";
			}else{
				$where .= " AND o.order_type=".$type;
			}
		}
		if(!empty($keyword)){
			$where .= " AND o.order_sn like '%$keyword%'";
		}
		if(isset($status) && $status !== ''){
			if(is_array($status)){
				$where .= " AND o.order_status in(".implode(",",$status).")";
			}else{
				$where .= " AND o.order_status=".$status;
			}
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($orderDate)){
			$where .= " AND o.order_date>=".$orderDate;
		}else{
			if(!empty($start) && !empty($end)){
				$startint = strtotime($start);
				$endint   = strtotime($end.' 23:59:59');
				$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
			}
		}
		if(!empty($okDate)){
			$where .= " AND o.ok_date <= $okDate";
		}
		if(!empty($payDate)){
			$where .= " AND o.pay_date <= $payDate";
		}
		if(isset($comment)){
			$where .= " AND o.is_comment=$comment";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT o.*, a.addr, c.addr AS city_addr, p.addr AS pro_addr 
				FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where
				ORDER BY o.order_date DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺维修订单数量
	* @param $object 条件
	* @return $data
	*/
	public function getRepairCountByObject($object){		
		$shopId  = intval($object['shopId']);
		$keyword = $object['keyword'];
		$provinceId  = $object['provinceId'];
		$cityId  = $object['cityId'];
		$areaId  = $object['areaId'];		
		$way     = $object['way'];
		$status  = $object['status'];
		$sstatus = $object['sstatus'];
		$start   = $object['start'];
		$end     = $object['end'];
		$m       = $object['m'];
		$n       = $object['n'];
		
		$where = "o.order_type=1";
		if($shopId > 0){
			$where .= " AND s.shop_id=$shopId";
		}
		if(!empty($keyword)){
			$where .= " AND (o.order_sn like '%$keyword%' OR u.mobile like '%$keyword%')";
		}
		if($provinceId > 0){
			$where .= " AND p.id=".$provinceId;
		}
		if($cityId > 0){
			$where .= " AND c.id=".$cityId;
		}
		if($areaId > 0){
			$where .= " AND a.id=".$areaId;
		}
		if(!empty($way)){
			$where .= " AND s.server_way=".$way;
		}
		if(isset($status) && $status !== ''){
			if(is_array($status)){
				$where .= " AND o.order_status in(".implode(",", $status).")";
			}else{
				$where .= " AND o.order_status=".$status;
			}
		}
		if(isset($sstatus) && $sstatus !== ''){
			$where .= " AND s.server_status=".$sstatus;
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql ="SELECT count(*) AS num FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__order_server AS s ON s.order_id=o.id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取店铺维修订单列表
	* @param $object 条件
	* @return $data
	*/
	public function getRepairByObject($object){		
		$shopId  = intval($object['shopId']);
		$keyword = $object['keyword'];
		$provinceId  = $object['provinceId'];
		$cityId  = $object['cityId'];
		$areaId  = $object['areaId'];
		$way     = $object['way'];
		$status  = $object['status'];
		$sstatus = $object['sstatus'];
		$start   = $object['start'];
		$end     = $object['end'];
		$m       = $object['m'];
		$n       = $object['n'];
		
		$where = "o.order_type=1";
		if($shopId > 0){
			$where .= " AND s.shop_id=$shopId";
		}
		if(!empty($keyword)){
			$where .= " AND (o.order_sn like '%$keyword%' OR u.mobile like '%$keyword%')";
		}
		if($provinceId > 0){
			$where .= " AND p.id=".$provinceId;
		}
		if($cityId > 0){
			$where .= " AND c.id=".$cityId;
		}
		if($areaId > 0){
			$where .= " AND a.id=".$areaId;
		}
		if(!empty($way)){
			$where .= " AND s.server_way=".$way;
		}
		if(isset($status) && $status !== ''){
			if(is_array($status)){
				$where .= " AND o.order_status in(".implode(",", $status).")";
			}else{
				$where .= " AND o.order_status=".$status;
			}
		}
		if(isset($sstatus) && $sstatus !== ''){
			$where .= " AND s.server_status=".$sstatus;
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql ="SELECT o.*,s.* FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__order_server AS s ON s.order_id=o.id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where 
				ORDER BY o.order_date DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺回收订单数量
	* @param $object 条件
	* @return $data
	*/
	public function getRecoveryCountByObject($object){		
		$shopId  = intval($object['shopId']);
		$keyword = $object['keyword'];
		$provinceId  = $object['provinceId'];
		$cityId  = $object['cityId'];
		$areaId  = $object['areaId'];
		$way     = $object['way'];
		$status  = $object['status'];
		$start   = $object['start'];
		$end     = $object['end'];
		$m       = $object['m'];
		$n       = $object['n'];
		
		$where = "o.order_type=2";
		if(isset($shopId)){
			$where .= " AND r.shop_id=$shopId";
		}
		if(!empty($keyword)){
			$where .= " AND o.order_sn like '%$keyword%'";
		}
		if($provinceId > 0){
			$where .= " AND p.id=".$provinceId;
		}
		if($cityId > 0){
			$where .= " AND c.id=".$cityId;
		}
		if($areaId > 0){
			$where .= " AND a.id=".$areaId;
		}
		if(isset($way) && $way !== ''){
			$where .= " AND r.reco_way=".$way;
		}
		if(isset($status) && $status !== ''){
			if(is_array($status)){
				$where .= " AND o.order_status in(".implode(",", $status).")";
			}else{
				$where .= " AND o.order_status=".$status;
			}
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql ="SELECT count(*) AS num FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__order_recovery AS r ON r.order_id=o.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取店铺回收订单列表
	* @param $object 条件
	* @return $data
	*/
	public function getRecoveryByObject($object){		
		$shopId  = intval($object['shopId']);
		$keyword = $object['keyword'];
		$provinceId  = $object['provinceId'];
		$cityId  = $object['cityId'];
		$areaId  = $object['areaId'];
		$way     = $object['way'];
		$status  = $object['status'];
		$start   = $object['start'];
		$end     = $object['end'];
		$m       = $object['m'];
		$n       = $object['n'];
		
		$where = "o.order_type=2";
		if(isset($shopId)){
			$where .= " AND r.shop_id=$shopId";
		}
		if(!empty($keyword)){
			$where .= " AND o.order_sn like '%$keyword%'";
		}
		if($provinceId > 0){
			$where .= " AND p.id=".$provinceId;
		}
		if($cityId > 0){
			$where .= " AND c.id=".$cityId;
		}
		if($areaId > 0){
			$where .= " AND a.id=".$areaId;
		}
		if(isset($way) && $way !== ''){
			$where .= " AND r.reco_way=".$way;
		}
		if(isset($status) && $status !== ''){
			if(is_array($status)){
				$where .= " AND o.order_status in(".implode(",", $status).")";
			}else{
				$where .= " AND o.order_status=".$status;
			}
		}
		if(isset($pay)){
			$where .= " AND o.pay_status=".$pay;
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint = strtotime($end.' 23:59:59');
			$where .= " AND (o.order_date BETWEEN $startint AND $endint)";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql ="SELECT o.*,r.* FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__order_recovery AS r ON r.order_id=o.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where 
				ORDER BY o.order_date DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺回收订单列表
	* @param $obj 条件
	* @return $data
	*/
	public function getMyRecoveryOrderList($obj){		
		$shopId = intval($obj['shopId']?$obj['shopId']:session('shop.id'));
		$m      = $obj['m'];
		$n      = $obj['n'];
		$sql ="SELECT o.*,r.* FROM __PREFIX__orders AS o,__PREFIX__order_recovery AS r
				WHERE o.id=r.order_id AND o.order_type=2 AND r.server_id=$shopId
				ORDER BY o.order_date DESC
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取订单
	* @param $orderId   订单编号
	* @return $data
	*/
	public function getOrderById($orderId){
		$sql = "SELECT o.*, a.addr, c.addr AS city_addr, p.addr AS pro_addr 
				FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE o.id=$orderId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取订单
	* @param $sn   订单序列号
	* @return $data
	*/
	public function getOrderBySn($sn){
		$sql = "SELECT o.*, a.addr, c.addr AS city_addr, p.addr AS pro_addr 
				FROM __PREFIX__orders AS o
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=o.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE o.order_sn='$sn'";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取订单附表
	* @param $status 状态
	* @return $data
	*/
	public function getOrderScheduleByStatus($status){
		$sql = "SELECT * FROM __PREFIX__order_schedule WHERE shop_status=$status";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取订单附表
	* @param $orderId 订单编号
	* @return $data
	*/
	public function getOrderScheduleByOrderId($orderId){
		$sql = "SELECT * FROM __PREFIX__order_schedule WHERE order_id=$orderId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取订单附表
	* @param $orderId 订单编号
	* @param $shopId  店铺编号
	* @return $data
	*/
	public function getOrderScheduleByOrderIdAndShopId($orderId, $shopId){
		$sql = "SELECT * FROM __PREFIX__order_schedule WHERE order_id=$orderId AND shop_id=$shopId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取购物订单表
	* @param $orderId 订单编号
	* @return $data
	*/
	public function getOrderProductsByOrderId($orderId){
		$sql = "SELECT op.*, p.id, p.shop_id, p.pro_name, p.is_card, p.list_image FROM __PREFIX__order_products AS op
				LEFT JOIN __PREFIX__product_relation AS r ON r.id=op.pro_id
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE op.order_id=$orderId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取购物订单表
	* @param $orderId   订单编号
	* @param $productId 商品编号
	* @return $data
	*/
	public function getOrderProductByOrderId($orderId, $productId){
		$sql = "SELECT op.*, p.id, p.shop_id, p.pro_name, p.is_card, p.list_image FROM __PREFIX__order_products AS op
				LEFT JOIN __PREFIX__product_relation AS r ON r.id=op.pro_id
				LEFT JOIN __PREFIX__products AS p ON p.id=r.pro_id
				WHERE op.order_id=$orderId AND op.pro_id=$productId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取维修订单表
	* @param $orderId   订单编号
	* @return $data
	*/
	public function getOrderServerByOrderId($orderId){
		$sql ="SELECT s.*,c.id as color_id, c.name FROM __PREFIX__order_server AS s
				LEFT JOIN __PREFIX__repair_color AS c ON c.id=s.color_id
				WHERE order_id=$orderId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取回收订单表
	* @param $orderId   订单编号
	* @return $data
	*/
	public function getOrderRecoveryByOrderId($orderId){
		$sql ="SELECT * FROM __PREFIX__order_recovery WHERE order_id=$orderId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取订单报价数量
	* @param $orderId   订单编号
	* @return $data
	*/
	public function getOrderOfferCountByOrderId($orderId){
		$sql ="SELECT count(*) AS num FROM __PREFIX__order_offer WHERE order_id=$orderId";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取用户所有等待报价订单的报价数量
	* @param $userId   用户编号
	* @return $data
	*/
	public function getOrderOfferCountByUserId($userId){
		$sql ="SELECT count(*) AS num FROM __PREFIX__order_offer AS oo
			LEFT JOIN __PREFIX__orders AS o ON o.id=oo.order_id
			WHERE o.user_id=$userId AND oo.status=0";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取用户所有等待报价订单的报价数量
	* @param $userId   用户编号
	* @return $data
	*/
	public function getOrderOfferByUserId($userId, $m, $n){
		$sql ="SELECT oo.* FROM __PREFIX__order_offer AS oo
			LEFT JOIN __PREFIX__orders AS o ON o.id=oo.order_id
			WHERE o.user_id=$userId AND oo.status=0
			ORDER BY oo.id DESC 
			LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取选中报价
	* @param $orderId   订单编号
	* @return $data
	*/
	public function getOrderOfferSelectedByOrderId($orderId){
		$sql ="SELECT * FROM __PREFIX__order_offer WHERE order_id=$orderId AND selected=1";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取订单报价表
	* @param $orderId 订单编号
	* @param $m       起始位置
	* @param $n       返回记录数量
	* @return $data
	*/
	public function getOrderOfferByOrderId($orderId, $m, $n){
		$sql ="SELECT * FROM __PREFIX__order_offer WHERE order_id=$orderId ORDER BY selected DESC,offer_date ASC LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取店铺已报价,顾客未选择数据数量
	* @param $shopId  店铺编号
	* @return $data
	*/
	public function getOrderOfferCountByShopId($shopId){
		$sql ="SELECT count(*) AS num FROM __PREFIX__order_offer AS oo
				LEFT JOIN __PREFIX__orders AS o ON o.id=oo.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				WHERE oo.shop_id=$shopId AND o.order_status=1 AND o.is_offer_selected=0";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取店铺已报价,顾客未选择数据
	* @param $shopId  订单编号
	* @param $m       起始位置
	* @param $n       返回记录数量
	* @return $data
	*/
	public function getOrderOfferByShopId($shopId, $m, $n){
		$sql ="SELECT o.*, u.uname, i.thumb FROM __PREFIX__order_offer AS oo
				LEFT JOIN __PREFIX__orders AS o ON o.id=oo.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				WHERE oo.shop_id=$shopId AND o.order_status=1 AND o.is_offer_selected=0 
				ORDER BY oo.id DESC 
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取订单报价表
	* @param $orderId  订单编号
	* @param $shopId   店铺编号
	* @return $data
	*/
	public function getOrderOfferByOrderIdAndShopId($orderId, $shopId){
		$sql ="SELECT * FROM __PREFIX__order_offer WHERE order_id=$orderId AND shop_id=$shopId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取可以报价店铺数量
	* @param $orderId  订单编号
	* @return $data
	*/
	public function getOrderOfferIngCountByOrderId($orderId){
		$sql ="SELECT count(*) AS num FROM __PREFIX__order_offer_ing WHERE order_id=$orderId";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取可以报价店铺表
	* @param $orderId  订单编号
	* @return $data
	*/
	public function getOrderOfferIngByOrderId($orderId){
		$sql ="SELECT * FROM __PREFIX__order_offer_ing WHERE order_id=$orderId";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取订单报价表
	* @param $orderId  订单编号
	* @param $shopId   店铺编号
	* @return $data
	*/
	public function getOrderOfferIngByOrderIdAndShopId($orderId, $shopId){
		$sql ="SELECT * FROM __PREFIX__order_offer_ing WHERE order_id=$orderId AND shop_id=$shopId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 统计某门店的报价总条数
	* @param $object  条件数组
	* @return $data
	*/
	public function getOrderOfferIngCountByShopId($object){
		$shopId  = intval($object['shopId']);
		$isOffer = $object['isOffer'];
		$status  = $object['status'];
		
		$where = "oi.shop_id=$shopId AND o.order_status=1";
		if(isset($isOffer)){
			$where .= " AND oi.is_offer=$isOffer";
		}
		if(isset($status)){
			$where .= " AND oi.status=$status";
		}
		$sql ="SELECT count(*) AS num FROM __PREFIX__order_offer_ing AS oi
				LEFT JOIN __PREFIX__orders AS o ON o.id=oi.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取可以报价店铺表
	* @param $object  条件数组
	* @return $data
	*/
	public function getOrderOfferIngByShopId($object){
		$shopId  = intval($object['shopId']);
		$isOffer = $object['isOffer'];
		$status  = $object['status'];
		$m       = $object['m'];
		$n       = $object['n'];
		
		$where = "oi.shop_id=$shopId AND o.order_status=1";
		if(isset($isOffer)){
			$where .= " AND oi.is_offer=$isOffer";
		}
		if(isset($status)){
			$where .= " AND oi.status=$status";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql ="SELECT o.*, oi.shop_id, oi.status, u.uname, i.thumb FROM __PREFIX__order_offer_ing AS oi
				LEFT JOIN __PREFIX__orders AS o ON o.id=oi.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				WHERE $where
				ORDER BY oi.order_id DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取可以报价数量
	* @param $object  条件数组
	* @return $data
	*/
	public function getOrderOfferIngCountByUserId($object){
		$userId  = intval($object['userId']);
		$status  = $object['status'];
		
		$where = "o.order_status=1 AND o.is_offer=1 AND oi.user_id=$userId";
		if(isset($status)){
			$where .= " AND oi.status=$status";
		}
		$sql ="SELECT count(*) AS num FROM __PREFIX__order_offer_ing AS oi
				LEFT JOIN __PREFIX__orders AS o ON o.id=oi.order_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 获取用户可以报价表
	* @param $object  条件数组
	* @return $data
	*/
	public function getOrderOfferIngByUserId($object){
		$userId  = intval($object['userId']);
		$status  = $object['status'];
		$m       = $object['m'];
		$n       = $object['n'];
		
		$where = "o.order_status=1 AND o.is_offer=1 AND oi.user_id=$userId";
		if(isset($status)){
			$where .= " AND oi.status=$status";
		}
		$sql ="SELECT * FROM __PREFIX__order_offer_ing AS oi 
				LEFT JOIN __PREFIX__orders AS o ON o.id=oi.order_id
				WHERE $where 
				ORDER BY oi.order_id DESC 
				LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 获取订单支付金额
	* @param $orderId   订单编号
	* @return $data
	*/
	public function getOrderMoneyById($orderId){
		$order = $this->getOrderById($orderId);
		$money = 0;
		if($order['order_type'] == '0'){
			$money = $order['deail_price'];
		}elseif($order['order_type'] == '1'){
			$server = $this->getOrderServerByOrderId($order['id']);
			if($server['new_price'] > 0){
				$money = $server['new_price'];
			}else{
				$money = $order['deail_price'];
			}
		}elseif($order['order_type'] == '2'){
			$server = $this->getOrderRecoveryByOrderId($order['id']);
			if($server['new_price'] > 0){
				$money = $server['new_price'];
			}else{
				$money = $order['deail_price'];
			}
		}else{
			$money = $order['deail_price'];
		}
		return number_format($money, 2, '.', '');
	}
	
	/**
	* 获取订单支付金额
	* @param  $sn   订单序列号
	* @return $data
	*/
	public function getOrderMoney($sn){
		$order = $this->getOrderBySn($sn);
		$money = 0;
		if($order['order_type'] == '0'){
			$money = $order['deail_price'];
		}elseif($order['order_type'] == '1'){
			$server = $this->getOrderServerByOrderId($order['id']);
			if($server['new_price'] > 0){
				$money = $server['new_price'];
			}else{
				$money = $order['deail_price'];
			}
		}elseif($order['order_type'] == '2'){
			$server = $this->getOrderRecoveryByOrderId($order['id']);
			if($server['new_price'] > 0){
				$money = $server['new_price'];
			}else{
				$money = $order['deail_price'];
			}
		}else{
			$money = $order['deail_price'];
		}
		return number_format($money, 2, '.', '');
	}
	
	/**
	* 获取订单商品信息
	* @param  $sn   订单序列号
	* @return $data
	*/
	public function getOrderBody($sn){
		$order = $this->getOrderBySn($sn);
		$data  = array();
		if($order['order_type'] == '0'){
			$orderProducts = $this->getOrderProductsByOrderId($order['id']);
			$products  = D('Products');
			foreach($orderProducts as $vo){
				$attr = $products->getAttrNameByAttrId($vo['pro_attr']);
				$data[] = $vo['pro_name']."[".$attr."]";
			}
			return msubstr(implode(",",$data), 0, 26, 'utf-8', true);
		}elseif($order['order_type'] == '1'){
			$server = $this->getOrderServerByOrderId($order['id']);
			$ids = array();
			if(!empty($server['relation_ids'])){
				$ids = explode(',', $server['relation_ids']);
			}else{
				$ids[] = $server['relation_id'];
			}
			$repairs = D('Repairs');
			for($i=0; $i<count($ids);$i++){
				$repair = $repairs->getRepairByRelationId($ids[$i]);
				$data[] = $repair['pro_name'].$repair['plan_name'];
			}
			return implode(",",$data);
		}
		return '草包网订单';
	}
	
	/**
	* 关闭订单
	* @param $orderId 订单编号
	* @return $rd
	*/
	public function closeOrders($orderId){
		$rd = array('status'=>-1);
	    $rs = $this->where("id=$orderId")->save(array('cancel_date'=>time(), 'order_status'=>0));
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 删除订单
	* @param $orderId 订单编号
	* @return $rd
	*/
	public function deleteOrders($orderId){
		$rd = array('status'=>-1);
	    $rs = $this->where("id=$orderId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 完成交易
	* @param $orderId 订单编号
	* @return $rd
	*/
	public function completeOrders($orderId){
		$rd = array('status'=>-1);
	    $rs = $this->where("id=$orderId")->save(array('order_status'=>5));
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单
	* @param $orderId 订单编号
	* @param $data    更新数据
	* @return $rd
	*/
	public function updateOrders($orderId, $data){
		$rd = array('status'=>-1);
	    $rs = $this->where("id=$orderId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单附表
	* @param $orderId 订单编号
	* @param $shopId  店铺编号
	* @param $data    更新数据
	* @return $rd
	*/
	public function updateOrderSchedule($orderId, $shopId, $data){
		$rd = array('status'=>-1);
		$model = M("order_schedule");
	    $rs = $model->where("order_id=$orderId AND shop_id=$shopId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单报价表
	* @param $offerId 报价编号
	* @param $data    更新数据
	* @return $rd
	*/
	public function updateOrderOffer($offerId, $data){
		$rd = array('status'=>-1);
		$model = M("order_offer");
	    $rs = $model->where("id=$offerId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单报价表
	* @param $orderId 订单编号
	* @param $shopId  店铺编号
	* @param $data    更新数据
	* @return $rd
	*/
	public function updateOrderOfferIng($orderId, $shopId, $data){
		$rd = array('status'=>-1);
		$model = M("order_offer_ing");
	    $rs = $model->where("order_id=$orderId AND shop_id=$shopId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单表(购物)
	* @param $orderId   订单编号
	* @param $productId 商品编号
	* @param $data      更新数据
	* @return $rd
	*/
	public function updateOrderProduct($orderId, $productId, $data){
		$rd = array('status'=>-1);
	    $model = M("order_products");
        $rs = $model->where("order_id=$orderId AND pro_id=$productId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单表(购物)
	* @param $id   编号
	* @param $data 更新数据
	* @return $rd
	*/
	public function updateOrderProduct2($id, $data){
		$rd = array('status'=>-1);
	    $model = M("order_products");
        $rs = $model->where("id=$id")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单表(维修)
	* @param $orderId 订单编号
	* @param $data    更新数据
	* @return $rd
	*/
	public function updateOrderServer($orderId, $data){
		$rd = array('status'=>-1);
	    $model = M("order_server");
        $rs = $model->where("order_id=$orderId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 删除订单表(维修)
	* @param $orderId 订单编号
	* @return $rd
	*/
	public function deleteOrderServer($orderId){
		$rd = array('status'=>-1);
		$model = M("order_server");
	    $rs = $model->where("order_id=$orderId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 更新订单表(回收)
	* @param $orderId 订单编号
	* @param $data    更新数据
	* @return $rd
	*/
	public function updateOrderRecovery($orderId, $data){
		$rd = array('status'=>-1);
	    $model = M("order_recovery");
        $rs = $model->where("order_id=$orderId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 添加订单
	* @param $data 添加数据包
	* @return $rd
	*/
	public function insertOrders($data){
		$orderId = 0;
        if($this->create($data)){
            $orderId = $this->add();
        }
		return $orderId;
	}
	
	/**
	* 添加订单附表
	* @param $schedule 订单数组
	* @param $orderId  订单编号
	* @return True
	*/
	public function insertOrderSchedule($schedule, $orderId){
		if(empty($schedule)){
			return false;
		}
		$model = M("order_schedule");
		foreach($schedule as $vo){
			$data = array();
			$data['order_id']      = $orderId;
			$data['shop_id']       = $vo['shop_id'];
			$data['send_date']     = $vo['send_date'];
			$data['shop_money']    = $vo['shop_money'];
			$data['virtual_money'] = $vo['virtual_money'];
			$data['shipping_way']  = $vo['shipping_way'];
			$data['shipping_num']  = $vo['shipping_num'];
			$data['is_shipping']   = $vo['is_shipping'];
			$data['shipping_fee']  = $vo['shipping_fee'];
			$data['remarks']       = $vo['remarks'];
			$data['shop_status']   = $vo['shop_status'];
			if($model->create($data)){
				$model->add();
			}
		}
		return true;
	}
	
	/**
	* 添加订单报价表
	* @param $data  数组
	* @return
	*/
	public function insertOrderOffer($data){
		if(empty($data)){
			return 0;
		}
		$model = M("order_offer");
        if($model->create($data)){
			$id = $model->add();
		}
		return $id;
	}
	
	/**
	* 添加店铺可报价表
	* @param $data  数组
	* @return
	*/
	public function insertOrderOfferIng($data){
		if(empty($data)){
			return 0;
		}
		$model = M("order_offer_ing");
        if($model->create($data)){
			$id = $model->add();
		}
		return $id;
	}
	
	/**
	* 添加购物订单表
	* @param $goods   商品数组
	* @param $orderId 订单编号
	* @return True
	*/
	public function insertOrderProducts($goods, $orderId){
		if(empty($goods)){
			return false;
		}
		$model = M("order_products");
		foreach($goods as $vo){
			$data = array();
			$data['order_id']      = $orderId;
			$data['pro_id']        = $vo['pro_id'];
			$data['pro_price']     = $vo['pro_price'];
			$data['two_price']     = $vo['two_price'];
			$data['three_price']   = $vo['three_price'];
			$data['pro_number']    = $vo['pro_number'];
			$data['pro_attr']      = $vo['pro_attr'];
			$data['virtual_money'] = $vo['virtual_money'];
			if($model->create($data)){
				$model->add();
			}
		}
		return true;
	}
	
	/**
	* 添加维修订单附表
	* @param $data   维修数组
	* @return
	*/
	public function insertOrderServer($data){
		if(empty($data)){
			return 0;
		}
		$model = M("order_server");
        if($model->create($data)){
			$id = $model->add();
		}
		return $id;
	}
	
	/**
	* 添加回收订单附表
	* @param $data   维修数组
	* @return
	*/
	public function insertOrderRecovery($data){
		if(empty($data)){
			return 0;
		}
		$model = M("order_recovery");
        if($model->create($data)){
			$id = $model->add();
		}
		return $id;
	}
	
	/**
	* 更新订单支付信息
	* @param $orderId  订单编号
	* @param $mode     支付方式
	* @return $data
	*/
	public function updateOrderPay($orderId, $status, $mode){
		$sql = "UPDATE __PREFIX__orders set order_status=$status, pay_status=1, pay_date='".time()."', pay_mode=$mode WHERE id = $orderId";		
		$rs = $this->execute($sql);	
		return $rs;
	}
	
	/**
	* 保存交易信息
	* @param $data  添加数据
	* @return $rs
	*/
	public function addTrading($data){
		$rd = array('status'=>0);
		$model = M('order_trading');
		if($model->create($data)){
			$rd['status'] = $model->add();
		}
		return $rd;
	}
	
	/**
	* 获取佣金记录
	* @param $orderId  订单编号
	* @param $userId   用户编号
	* @return $data
	*/
	public function getOrderCommisionByOrderIdAndUserId($orderId, $userId){
		$sql ="SELECT * FROM __PREFIX__order_commision WHERE order_id=$orderId AND user_id=$userId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 添加佣金记录
	* @param $data  添加数据
	* @return $rs
	*/
	public function insertOrderCommision($data){
		$rd = array('status'=>0);
		$model = M('order_commision');
		if($model->create($data)){
			$rd['status'] = $model->add();
		}
		return $rd;
	}
	
	/**
	* 更新佣金记录
	* @param $commisionId  佣金编号
	* @return $rs
	*/
	public function updateOrderCommision($commisionId, $data){
		$rd = array('status'=>-1);
	    $model = M("order_commision");
        $rs = $model->where("id=$commisionId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	* 添加订单申诉表
	* @param $data  数组
	* @return
	*/
	public function insertOrderAppeal($data){
		if(empty($data)){
			return 0;
		}
		$model = M("order_appeal");
        if($model->create($data)){
			$id = $model->add();
		}
		return $id;
	}
	
	/**
	* 更新申诉表
	* @param $appealId 订单编号
	* @param $data     更新数据
	* @return $rd
	*/
	public function updateOrderAppeal($appealId, $data){
		$rd = array('status'=>-1);
	    $model = M("order_appeal");
        $rs = $model->where("id=$appealId")->save($data);
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}	
	
	/**
	* 获取申诉信息
	* @param  $appealId 申诉编号
	* @return $data
	*/
	public function getOrderAppealByAppealId($appealId){
		$sql = "SELECT * FROM __PREFIX__order_appeal WHERE id=$appealId";
		$data = $this->queryRow($sql);
		return $data;
	}		
	
	/**
	* 获取申诉信息
	* @param  $orderId 订单编号
	* @return $data
	*/
	public function getOrderAppealByOrderId($orderId){
		$sql = "SELECT * FROM __PREFIX__order_appeal WHERE order_id=$orderId";
		$data = $this->queryRow($sql);
		return $data;
	}	
	
	/**
	* 通过筛选条件获取获取申诉数量
	* @param  $object 条件
	* @return $num
	*/
	public function getOrderAppealCountByObject($object){
		$keyword = $object['keyword'];
		$reason  = $object['reason'];
		$start   = $object['start'];
		$end     = $object['end'];
		
		$where = "a.id<>0";
		if(!empty($keyword)){
			$where .= " AND o.order_sn like '%$keyword%'";
		}
		if($reason > 0){
			$where .= " AND a.reason=$reason";
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint   = strtotime($end.' 23:59:59');
			$where .= " AND (a.appeal_date BETWEEN $startint AND $endint)";
		}
		$sql = "SELECT count(*) AS num FROM __PREFIX__order_appeal AS a
				LEFT JOIN __PREFIX__orders AS o ON o.id=a.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	* 通过筛选条件获取获取申诉列表
	* @param  $object 条件
	* @return $data
	*/
	public function getOrderAppealByObject($object){
		$keyword = $object['keyword'];
		$reason  = $object['reason'];
		$start   = $object['start'];
		$end     = $object['end'];
		$m   	 = $object['m'];
		$n   	 = $object['n'];
		
		$where = "a.id<>0";
		if(!empty($keyword)){
			$where .= " AND o.order_sn like '%$keyword%'";
		}
		if($reason > 0){
			$where .= " AND a.reason=$reason";
		}
		if(!empty($start) && !empty($end)){
			$startint = strtotime($start);
			$endint   = strtotime($end.' 23:59:59');
			$where .= " AND (a.appeal_date BETWEEN $startint AND $endint)";
		}
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT a.*,o.order_sn,o.order_type,u.uname FROM __PREFIX__order_appeal AS a
				LEFT JOIN __PREFIX__orders AS o ON o.id=a.order_id
				LEFT JOIN __PREFIX__users AS u ON u.id=o.user_id
				WHERE $where
				ORDER BY a.appeal_date DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 支付完成后,订单操作
	* 包含(朋友圈收益,店铺收益,代理收益,维修订单结算,VIP时间增加)
	* @param $orderId 订单编号
	* @return
	*/
	public function successPayOrderOperation($orderId){
		$users    = D('Users');
		$us       = D('UserShop');
		$uw       = D('UserWechat');
		$products = D("Products");
		
		$order = $this->getOrderById($orderId);
		if($order['order_type'] == '0'){           // 购物订单
			// 结算
			$schedule = $this->getOrderScheduleByOrderId($order['id']);
			foreach($schedule as $vo){
				if($vo['shop_id'] > 0 && $vo['shop_money'] > 0){
					$shop = $us->getShopByShopId($vo['shop_id']);
					$commision = $vo['shop_money']*0.05;
					$money = $vo['shop_money']-$commision;
					$users->appendMoney($shop['user_id'], $order['id'], $money, 2, '');                   // 增加账户账户金额
					$users->insertFrozenMoney($shop['user_id'], $order['id'], $money, 2, '', 15);         // 增加冻结金额
					
					$this->insertOrderCommision(array('order_id'=>$order['id'], 'user_id'=>$shop['user_id'], 'commision'=>$commision)); // 增加佣金记录
					$users->decreaseMoney($shop['user_id'], $order['id'], $commision, 9, '');             // 扣除账户账户金额
				}
			}
			
			// 减库存, 草包豆充值完成订单
			$product = $this->getOrderProductsByOrderId($order['id']);
			foreach($product as $vo){
				if($vo['id'] == 722){
					if($vo['pro_price'] == 10){
						$vNumber = $vo['pro_price']*100;
					}elseif($vo['pro_price'] == 50){
						$vNumber = ($vo['pro_price']*100)+(1*10*$vo['pro_price']);
					}elseif($vo['pro_price'] == 100){
						$vNumber = ($vo['pro_price']*100)+(2*10*$vo['pro_price']);
					}elseif($vo['pro_price'] == 500){
						$vNumber = ($vo['pro_price']*100)+(3*10*$vo['pro_price']);
					}
					$users->giveVirtual($order['user_id'], $vNumber, 8, $order['order_sn']);      // 增加草包豆
					$this->updateOrders($order['id'], array('ok_date'=>time(),'order_status'=>2));
				}
				$products->decrease($vo['id'], $vo['pro_number'], $vo['pro_attr']);
			}

			// 朋友圈
			$wechat = $uw->getWechatByUserId($order['user_id']);
			if(!empty($wechat) && $wechat['fid'] > 0){
				$shop    = $us->getShopByUserId($wechat['fid']);
				$harvest = 0;
				foreach($product as $vo){
					if($vo['pro_price'] > 0 && $vo['two_price'] > 0){
						$harvest += ($vo['pro_price']-$vo['two_price'])*$vo['pro_number'];
					}
				}
				if($harvest > 0){
					//$harvest = $harvest > 30?15:$harvest;
					$users->appendMoney($shop['user_id'], $order['id'], $harvest, 6, '');            // 增加账户金额(收益)
					$users->insertFrozenMoney($shop['user_id'], $order['id'], $harvest, 1, '', 15);  // 增加冻结金额(收益)
				}
			}
		}elseif($order['order_type'] == '1'){      // 维修订单
			$server = $this->getOrderServerByOrderId($order['id']);
			$shop   = $us->getShopByShopId($server['shop_id']);

			// 变更服务状态
			$this->updateOrderServer($order['id'], array('server_status'=>3));
				
			if($server['server_way'] != 3){
				// 店铺经验,维修计数
				$us->updateUserShop($server['shop_id'], array('exp'=>$shop['exp']+10, 'repair_count'=>$shop['repair_count']+1));
				
				$money = $server['new_price']>0?$server['new_price']:$order['deail_price'];
			
				// 结算
				if($money > 0){
					$users->appendMoney($shop['user_id'], $order['id'], $money, 2, '');           // 增加账户账户金额
					if($order['is_line'] == 0){
						$users->insertFrozenMoney($shop['user_id'], $order['id'], $money, 2, '', 2); // 增加账户冻结金额
					}
				}
				
				// 变更订单状态
				$this->updateOrders($order['id'], array('ok_date'=>time(),'order_status'=>2));
			}

			// 给店铺发送已付款通知短信
			$sms = new \Think\Sms();
			$sms->sendSMS($shop['user_phone'],'pay',array('order'=>$sn));
		}elseif($order['order_type'] == '2'){      // 回收订单
			$recovery = $this->getOrderRecoveryByOrderId($order['id']);
			$shop     = $us->getShopByShopId($recovery['shop_id']);

			// 变更服务状态
			$this->updateOrderRecovery($order['id'], array('reco_status'=>3));
				
			if($recovery['reco_way'] != 3){
				// 店铺经验,维修计数
				$us->updateUserShop($recovery['shop_id'], array('exp'=>$shop['exp']+10, 'recovery_count'=>$shop['recovery_count']+1));
				
				$money = $recovery['new_price']>0?$recovery['new_price']:$order['deail_price'];
			
				// 结算
				if($money > 0){
					$users->appendMoney($order['user_id'], $order['id'], $money, 2, '');           // 增加顾客账户账户金额
				}
	
				// 变更订单状态
				$this->updateOrders($order['id'], array('ok_date'=>time(),'order_status'=>2));
			}

			// 给顾客发送已付款通知短信
			if(!empty($order['reciver_phone'])){
				$sms = new \Think\Sms();
				$sms->sendSMS($order['reciver_phone'],'pay',array('order'=>$sn));
			}
		}
	}
}