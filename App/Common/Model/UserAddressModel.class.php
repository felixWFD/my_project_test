<?php
namespace Common\Model;
/**
 * 会员地址服务类
 */
class UserAddressModel extends BaseModel {
	/**
	 * 获取单个地址
	 * @param $addressId 地址编号
	 * @return $data
	 */
	public function getAddressById($addressId){
		$sql = "SELECT ua.*,a.one_id,a.two_id,a.addr,p.addr as pro_addr,c.addr as city_addr
				FROM __PREFIX__user_address AS ua
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=ua.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE ua.id=$addressId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 地址数量
	 * @param $userId 用户编号
	 * @return $count
	 */
	public function getAddressCountById($userId){
		return $this->where("user_id=$userId")->count();
	}
	
	/**
	 * 用户地址
	 * @param $userId 用户编号
	 * @return $data
	 */
	public function getAddressByUserId($userId){
		$userId = $userId>0?$userId:(int)session('user.userid');
		$sql = "SELECT u.*,a.one_id,a.two_id,a.addr,p.addr as pro_addr,c.addr as city_addr
				FROM __PREFIX__user_address AS u
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=u.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE u.is_show=1 AND u.user_id=$userId
				ORDER BY u.is_default DESC,u.id DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 用户默认地址
	 * @param $userId 用户编号
	 * @return $data
	 */
	public function getDefaultAddressByUserId($userId){
		$sql = "SELECT u.*,a.one_id,a.two_id,a.addr,p.addr as pro_addr,c.addr as city_addr
				FROM __PREFIX__user_address AS u
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=u.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE u.user_id=$userId
				ORDER BY u.is_default DESC,u.id DESC";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加地址
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertAddress($data){
		$rd = array('status'=>-1);
		$data["is_default"] = 1;
		$data["is_show"]    = 1;
	    if($this->create($data)){	
			$rs = $this->add();
			if(false !== $rs){
				$rd['status']= $rs;
				//修改所有的地址为非默认
				$this->where('user_id='.(int)$data['user_id']." and id!=".$rs)->save(array('is_default'=>0));
			}
		}
		return $rd;
	}
	
	/**
	 * 更新地址
	 * @param $id   编号
	 * @param $data 数据数组
	 * @return $rs
	 */
	public function updateAdderss($id, $data){
		$rd = array('status'=>-1);
		$data["is_default"] = 1;
		$rs = $this->where("id=".$id)->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
			//修改所有的地址为非默认
			$this->where('user_id='.(int)$data['user_id']." and id!=".$id)->save(array('is_default'=>0));
		}
		return $rd;
	}
	
	/**
	 * 删除地址
	 * @param $addressId   地址编号
	 * @return $rs
	 */
	public function deleteAdderss($addressId){
		$rd = array('status'=>-1);
	    $rs = $this->where("id=$addressId")->delete();
		if(false !== $rs){
		   $rd['status']= 1;
		}
		return $rd;
	}
	
}