<?php
namespace Common\Model;
/**
 * 搜索词服务类
 */
class SearchModel extends BaseModel {
	protected $tableName = 'search'; 
    /**
     * 获取搜索词
	 * @param $length 显示数量
	 * @return $data
     */
	public function getWords($length){
		$sql = "SELECT * FROM __PREFIX__search ORDER BY times DESC";
		if($length > 0){
			$sql .= " LIMIT 0,$length";
		}
		$data = $this->query($sql);
		return $data;
	}

	/**
     * 分词,并把词保存到数据库
	 * @param $keyword 搜索词
	 * @return $data
     */
    public function splitWords($keyword){
		// 过滤特殊符号
		$keyword = preg_replace("/\/|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\(|\)|\_|\+|\{|\}|\:|\<|\>|\?|\[|\]|\,|\.|\/|\;|\'|\`|\-|\=|\\\|\|/","",$keyword);
		
		if(empty($keyword)) return '';
		
		// 空格分词
		$words = explode(" ",$keyword);

		// 模糊搜索表中的匹配词
		$data = $this->where("keyword LIKE '%$keyword%'")->select();
		foreach($data as $vo){
			$words[] = $vo['keyword'];
		}
		
		// 中文字库分词
		Vendor('phpanalysis.phpanalysis');
		$pa = new \PhpAnalysis();
		$pa->SetSource($keyword);
		$pa->resultType=2;
		$pa->differMax=true;
		$pa->StartAnalysis();
		$paWords = $pa->GetFinallyIndex();
		
		foreach($paWords as $key=>$vo){
			if(strlen($key) > 3 && strlen($key) <=12){
				$words[] = $key;
				if(!is_numeric($key)){
					$word = $this->where("keyword = '$key'")->find();
					if ($word){
						$this->statistics($word['id']);
					}else{
						if(!empty($key) && strlen($key)>1){
							$this->insertWord(array('keyword'=>$key));
						}
					}
				}
			}
		}
        return array_unique($words);
    }
	
	/**
	 * 添加搜索词
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertWord($data){
		$rd = array('status'=>-1);
	    if($this->create($data)){	
			$rs = $this->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 搜索词搜索统计
	 * @param $wordId 搜索词编号
	 */
	public function statistics($wordId){
		$sql = "UPDATE __PREFIX__search set times=times+1 WHERE id=$wordId";
		$this->execute($sql);
	}
}