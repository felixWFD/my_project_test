<?php
namespace Common\Model;
/**
 * 会员服务类
 */
class UsersModel extends BaseModel {
	
	/**
	  * 获取用户信息
	  */
    public function getUserById($userId){
		$userId = intval($userId?$userId:session('user.userid'));
		$user = $this->where("id=".$userId)->find();
		return $user;
	}
	
	/**
	* 获取用户店铺数量
	* @param $obj 查询条件数组
	* @return $data
	*/
	public function getUsersCount($obj){
		$userId   = $obj['userId'];
		$parentId = $obj['parentId'];
		$province = $obj['province'];
		$city     = $obj['city'];
		$area     = $obj['area'];
		$keyword  = $obj['keyword'];
		$m        = $obj['m'];
		$n        = $obj['n'];
		
		$where = "u.id<>0";
		if(!empty($userId)){
			if(is_array($userId)){
				$str = implode(",", $userId);
				$where .= " AND u.id in($str)";
			}else{
				$where .= " AND u.id=$userId";
			}			
		}
		if(!empty($parentId)){
			if(is_array($parentId)){
				$str = implode(",", $parentId);
				$where .= " AND u.parent_id in($str)";
			}else{
				$where .= " AND u.parent_id=$parentId";
			}			
		}
		if(!empty($province)){
			$where .= " AND p.id=$province";
		}
		if(!empty($city)){
			$where .= " AND c.id=$city";
		}
		if(!empty($area)){
			$where .= " AND a.id=$area";
		}
		if(!empty($keyword)){
			$where .= " AND (u.uname like '%".$keyword."%' OR (u.mobile like '%".$keyword."%' AND u.mobile_state=1))";
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql   = "SELECT count(*) AS num FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=i.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where";
		
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	  * 获取用户信息
	  */
    public function getUserByObj($obj){
		$userId   = $obj['userId'];
		$parentId = $obj['parentId'];
		$province = $obj['province'];
		$city     = $obj['city'];
		$area     = $obj['area'];
		$keyword  = $obj['keyword'];
		$m        = $obj['m'];
		$n        = $obj['n'];
		
		$where = "u.id<>0";
		if(!empty($userId)){
			if(is_array($userId)){
				$str = implode(",", $userId);
				$where .= " AND u.id in($str)";
			}else{
				$where .= " AND u.id=$userId";
			}			
		}
		if(!empty($parentId)){
			if(is_array($parentId)){
				$str = implode(",", $parentId);
				$where .= " AND u.parent_id in($str)";
			}else{
				$where .= " AND u.parent_id=$parentId";
			}			
		}
		if(!empty($province)){
			$where .= " AND p.id=$province";
		}
		if(!empty($city)){
			$where .= " AND c.id=$city";
		}
		if(!empty($area)){
			$where .= " AND a.id=$area";
		}
		if(!empty($keyword)){
			$where .= " AND (u.uname like '%".$keyword."%' OR (u.mobile like '%".$keyword."%' AND u.mobile_state=1))";
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql   = "SELECT u.*,i.*,a.addr,c.addr as city_addr,p.addr as pro_addr 
				FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=i.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE $where
				ORDER BY u.id DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	  * 获取用户信息
	  */
    public function getUserBySql($sql){
		$user = $this->where($sql)->find();
		return $user;
	}
	
	/**
	  * 获取用户信息
	  */
    public function getUserByAccount($account){
		$user = $this->where("account='$account'")->find();
		return $user;
	}
	
	/**
	  * 获取用户信息
	  */
    public function getUserAndInfoById($userId=''){
		$userId = intval($userId?$userId:session('user.userid'));
		$sql = "SELECT u.*,i.*,a.addr,c.addr as city_addr,p.addr as pro_addr 
				FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=i.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE u.id=$userId";
		$user = $this->queryRow($sql);
		return $user;
	}
	 
	/**
	  * 根据手机号码获取用户信息
	  */
    public function getUserByMobile($mobile){
		$mobile = CBWAddslashes($mobile);
	 	$user = $this->where(" mobile ='".$mobile."' AND mobile_state=1 ")->find();
	    return $user;
	}
	
	/**
	  * 获取用户信息
	  */
    public function getUserAndInfoByMobile($mobile){
		$mobile = CBWAddslashes($mobile);
		$sql = "SELECT u.*,i.*,a.addr,c.addr as city_addr,p.addr as pro_addr 
				FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=i.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE u.mobile=$mobile AND u.mobile_state=1";
		$user = $this->queryRow($sql);
		return $user;
	}
	 
	/**
	  * 根据邮箱获取用户信息
	  */
    public function getUserByEmail($email){
		$email = CBWAddslashes($email);
	 	$user  = $this->where(" email ='".$email."' AND email_state=1 ")->find();
	    return $user;
	}
	
	/**
	  * 根据邮箱获取用户信息
	  */
    public function getUserAndInfoByEmail($email){
		$email = CBWAddslashes($email);
		$sql   = "SELECT u.*,i.*,a.addr,c.addr as city_addr,p.addr as pro_addr 
				FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				LEFT JOIN __PREFIX__addr_area AS a ON a.id=i.area_id
				LEFT JOIN __PREFIX__addr_city AS c ON c.id=a.two_id
				LEFT JOIN __PREFIX__addr_province AS p ON p.id=a.one_id
				WHERE u.email=$email AND u.email_state=1";
		$user = $this->queryRow($sql);
		return $user;
	}
	
	/**
	  * 根据账号/手机号码/邮箱获取用户信息
	  */
    public function getUserByLoginName($loginName){
		$loginName = CBWAddslashes($loginName);
		$user = $this->where("(account ='$loginName') OR (mobile ='$loginName' AND mobile_state=1) OR (email ='$loginName' AND email_state=1)")->find();
		return $user;
	}
	 
    /**
	 * 用户登录验证
	 */
	public function checkLogin($loginName, $loginPwd){
		$rd = array('status'=>-1);
		$loginName = CBWAddslashes($loginName);
		$loginPwd  = CBWAddslashes($loginPwd);
		$sql ="SELECT * FROM __PREFIX__users 
				WHERE (account='$loginName')
				OR (mobile='$loginName' AND mobile_state=1)
				OR (email='$loginName' AND email_state=1)";
		$rs = $this->queryRow($sql);
		if(!empty($rs)){
			if($rs['status'] == 0)return $rs['status'];
			if($rs['password'] != sha1($loginPwd))return $rd;
			
			// 记录此次登录的状态
			$data = array();
			$data['last_login_time'] = time();
			$data['last_login_ip']   = ip2long(I('server.REMOTE_ADDR'));
			$data['login_times']     = $rs['login_times'] + 1;
			$this->where('id='.$rs['id'])->save($data);
			
			$rd = $rs;
		}
		return $rd;
	}

	/**
	 * 保存会员登录信息
	 */
	public function saveLogin($user){
		// 分享链接入口
		$shareId = session('shareid');
		if(!empty($shareId) && $user['group_id'] == 0 && $user['is_agent'] == 0){
			$share = $this->getUserById($shareId);
			if($share['group_id'] == 1){
				$uw = D('UserWechat');
				$wechat = $uw->getWechatByUserId($user['id']);
				if(empty($wechat)){
					$uw->insertWechat(array('fid'=>$shareId, 'user_id'=>$user['id'], 'fans_time'=>time()));
				}else{
					if($wechat['fid'] == 0){
						$uw->updateWechat($wechat['id'], array('fid'=>$shareId, 'fans_time'=>time()));
					}
				}
			}
		}
		
		// 保存登录COOKIE
		$ip   = ip2long(I('server.REMOTE_ADDR'));
		$auto = $user['id'].'|'.$user['group_id'].'|'.$user['vip'].'|'.$ip;
		$cbw  = CBWCheckAutoLogin($auto,1);
		cookie('auto',  $cbw,           array('prefix' => 'cbw_', 'expire' => C('AUTO_TIME_LOGIN')));
		cookie('uname', $user['uname'], array('prefix' => 'cbw_', 'expire' => C('AUTO_TIME_LOGIN')));
		
		// 保存登录SESSION
		session(array('name'=>'userLogin', 'prefix'=>'user', 'domain'=>'caobao.com'));
		session('userid',   $user['id']);
		session('groupid',  $user['group_id']);
		session('vip',      $user['vip']);
		session('username', $user['uname']);
		
		// 店铺登录COOKIE
		if($user['group_id'] == 1){
			$us = D('UserShop');
			$shop = $us->getShopByUserId($user['id']);
			$auto    = $shop['id'].'|'.$ip;
			$system  = CBWCheckAutoLogin($auto,1);
			cookie('auto',  $system,            array('prefix' => 'system_', 'expire' => C('AUTO_TIME_LOGIN')));
			cookie('sname', $shop['shop_name'], array('prefix' => 'system_', 'expire' => C('AUTO_TIME_LOGIN')));
			
			// 保存登录SESSION
			session(array('name'=>'shopLogin', 'prefix'=>'shop'));
			session('id',   $shop['id']);
			session('name', $shop['shop_name']);
		}
	}

	/**
	 * 登录失败操作
	 */
	public function saveLoginFailed($loginName){
		$user = $this->getUserByLoginName($loginName);
		$loginFailed = $user['login_failed']+1;
		if($loginFailed > 50){//登录失败超过50次就锁定
			$this->updateUser(array('user_id'=>$user['id'], 'status'=>0));                   // 账号锁定	
		}else{
			$this->updateUser(array('user_id'=>$user['id'], 'login_failed'=>$loginFailed));  // 保存登录失败次数
		}
		return $loginFailed;
	}
	
	/**
	 * 会员注册
	 */
    public function regist($loginName, $loginPwd, $type='mobile'){
    	$rd = array('status'=>-1, 'msg'=>'注册失败');	   
    	
		$loginName  = CBWAddslashes($loginName);
		$loginPwd   = CBWAddslashes($loginPwd);
		$getIp      = I('server.REMOTE_ADDR');

        //检测账号是否存在
		if($type == 'email'){
			$crs = $this->checkUserEmail($loginName);
		}else{
			$crs = $this->checkUserMobile($loginName);
		}
        if($crs['status']!=1){
	    	$rd['status'] = -2;
	    	$rd['msg'] = "该账号已存在";
	    	return $rd;
	    }
		
	    $data = array();
		$data['account']		 = $this->randomAccount();
		$data['uname']			 = $this->randomUserName();
		if($type == 'email'){
			$data['email']       = $loginName;
			$data['email_state'] = 1;
		}else{
			$data['mobile']      = $loginName;
			$data['mobile_state']= 1;
		}
		$data['password']  		 = sha1($loginPwd);
		$data['create_time']     = time();
		$data['last_login_time'] = time();
		$data['last_login_ip']   = ip2long($getIp);
		$data['status']          = 1;
		
		$rs = $this->add($data);
		if(false !== $rs){
			$rd['status']= 1;
			$rd['userId']= $rs;
		}
	    if($rd['status']>0){
	    	// 添加新用户信息表
			$model = M('user_info');
		 	$data = array(
				'user_id' => $rd['userId'],
				'thumb'   => $this->randomUserHead()
			);
			$model->add($data);
	    }
		return $rd;
	}
	
	/**
	 * 随机生成一个用户账号
	 */
	public function randomAccount(){
		$rd = 0;
		do{
			$account = CBWGetRandom();
			$crs = $this->checkAccount($account);
			if($crs['status']==1){
				$rd = 1;
			}
		}while($rd < 1);
		return $account;
	}
	
	/**
	  * 查询用户账号是否存在
	  */
	public function checkAccount($account){
	 	$rd = array('status'=>-1);
	 	if($account=='')return $rd;
	 	$sql = " account='".$account."' ";
	 	$rs = $this->where($sql)->count();
	    if($rs==0)$rd['status'] = 1;
	    return $rd;
	}
	
	/**
	 * 随机生成一个用户昵称
	 */
	public function randomUserName(){
		$rd = 0;
		do{
			$uname = 'u'.substr(md5(time()), 0, 6);
			$crs = $this->checkUserNickName($uname);
			if($crs['status']==1){
				$rd = 1;
			}
		}while($rd < 1);
		return $uname;
	}
	
	/**
	 * 随机选择一个用户头像
	 */
	public function randomUserHead(){
		$r    = rand(1, 20);
		$d    = date('Ym');
		$u    = uniqid();
		
		$dst  = './Public/Head/'.$r.'.jpg';        // 源文件
		$_dst = './Public/User/'.$d.'/'.$u.'.jpg'; // 目标文件
		
		//判断源文件是否存在?
		if(CBWIsFind($dst) === FALSE){
			return '';
		}
		//判断目的文件夹是否存在? 如果不存在就生成
		CBWFileMake('./Public/User/'.$d);
		//判断目的文件是否存在? 存在不允许进行操作
		if(CBWIsFind($_dst) === TRUE){
			return '';
		}
		// 复制
		copy($dst, $_dst);
		return $d.'/'.$u.'.jpg';
	}
	
	/**
	  * 查询用户昵称是否存在
	  */
	public function checkUserNickName($userNick,$userId = 0,$isCheckKeys = true){
	 	$rd = array('status'=>-1);
	 	if($userNick=='')return $rd;
	 	if($isCheckKeys){
		 	if(!CBWCheckFilterWords($userNick,$GLOBALS['CONFIG']['sy_fkeyword'])){
		 		$rd['status'] = -2;
		 		return $rd;
		 	}
	 	}
	 	$sql = " uname='".$userNick."' ";
	    if($userId>0){
	 		$sql.=" AND id<>".$userId;
	 	}
	 	$rs = $this->where($sql)->count();
	    if($rs==0)$rd['status'] = 1;
	    return $rd;
	}
	
	/**
	 * 查询用户邮箱是否存在
	 */
    public function checkUserEmail($userEmail,$userId = 0){
    	$userId = $userId>0?$userId:(int)I("userId");
    	$rd = array('status'=>-1);
		if($userEmail=='')return $rd;
		$sql = " email_state=1 and email='".$userEmail."'";
		if($userId>0){
			$sql .= " AND id<>".$userId;
		}
		$rs = $this->where($sql)->count();
	    if($rs==0)$rd['status'] = 1;
	    return $rd;
	}
	
	/**
	 * 查询用户手机是否存在
	 */
    public function checkUserMobile($userPhone,$userId = 0){
    	$userId = $userId>0?$userId:(int)I("userId");
    	$rd = array('status'=>-1);
		if($userPhone=='')return $rd;
		$sql = " mobile_state=1 and mobile='".$userPhone."'";
		if($userId>0){
			$sql .= " AND id<>".$userId;
		}
		$rs = $this->where($sql)->count();
	    if($rs==0)$rd['status'] = 1;
	    return $rd;
	}
	
	/**
	 * 查询用户是否实名认证
	 */
    public function checkUserRealNameAuth($userId){
    	$userId = intval($userId?$userId:session('user.userid'));
    	$rd = array('status'=>-1);
		if($userId == '')return $rd;
		$sql = "SELECT * FROM __PREFIX__user_info where user_id=$userId";
		$data = $this->queryRow($sql);
		if(!empty($data['true_name']) && !empty($data['identity'])){
			$rd['status'] = 1;
		}
	    return $rd;
	}
	
	/**
	 * 修改用户密码
	 */
	public function updatePass($userId, $loginPwd){
    	$rd = array('status'=>-1, 'msg'=>'修改失败');	   

		$loginPwd   = CBWAddslashes($loginPwd);
		
		$data = array();
		$data['password']  = sha1($loginPwd);
		$data['pwd_level'] = CBWPasswordLevel($loginPwd);
		$data['status']    = 1;
		
		$user = $this->getUserById($userId);
		if(empty($user['email']) && $user['email_state'] == '1'){
			$data['active_code'] = md5($users['email'].md5(sha1($loginPwd)));
		}
		$rs = $this->where("id=".$user['id'])->save($data);
		if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 修改用户资料
	 */
	public function updateUser($obj){
		$rd = array('status'=>-1);
		$userId = (int)$obj["user_id"];

	    //检测昵称是否存在
		if(!empty($obj['uname'])){
			$crs = $this->checkUserNickName($obj['uname']);
			if($crs['status']!=1){
				$rd['status'] = -2;
				return $rd;
			}
		}
        
		$data = array();
		foreach($obj as $key=>$val){
			if(isset($obj[$key]) && $key != 'user_id'){
				$data[$key] = $val;
			}
		}
		$rs = $this->where(" id=".$userId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 修改用户详细资料
	 */
	public function updateUserInfo($obj){
		$rd = array('status'=>-1);
		$userId = (int)$obj["user_id"];
        
		$data = array();
		foreach($obj as $key=>$val){
			if(isset($obj[$key]) && $key != 'user_id'){
				$data[$key] = $val;
			}
		}
		$m = M('user_info');
		$rs = $m->where(" user_id=".$userId)->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 检查会员银行卡是否存在
	 * @param $userId 会员编号
	 * @param $cardNo 银行卡号
	 * @return $data
	 */
	public function checkCardByUserIdCardNo($userId, $cardNo){
		$rd = array('status'=>-1);
	 	if($userId=='' || $cardNo=='')return $rd;
	 	$sql = "SELECT count(*) as count FROM __PREFIX__user_card WHERE user_id=$userId AND card_no='$cardNo'";
		$data = $this->queryRow($sql);
	    if($data['count']==0)$rd['status'] = 1;
	    return $rd;
	}
	
	/**
	 * 获取会员所有银行卡
	 * @param $userId 会员编号
	 * @return $data
	 */
	public function getCardByUserId($userId){
		$sql = "SELECT uc.*,b.ident,b.name,b.picture,i.true_name,i.identity
				FROM __PREFIX__user_card AS uc 
				LEFT JOIN __PREFIX__bank AS b ON b.id=uc.bank_id 
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=uc.user_id 
				WHERE uc.user_id=$userId 
				ORDER BY is_default DESC";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取会员默认银行卡
	 * @param $userId 会员编号
	 * @return $data
	 */
	public function getDefaultCardByUserId($userId){
		$sql = "SELECT uc.*,b.ident,b.name,b.picture,i.true_name,i.identity
				FROM __PREFIX__user_card AS uc 
				LEFT JOIN __PREFIX__bank AS b ON b.id=uc.bank_id 
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=uc.user_id 
				WHERE uc.user_id=$userId 
				ORDER BY is_default DESC";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 获取银行卡信息
	 * @param $cardId 银行卡编号
	 * @return $data
	 */
	public function getCardById($cardId){
		$sql = "SELECT uc.*,b.ident,b.name,b.picture,i.true_name,i.identity
				FROM __PREFIX__user_card AS uc 
				LEFT JOIN __PREFIX__bank AS b ON b.id=uc.bank_id 
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=uc.user_id 
				WHERE uc.id=$cardId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加银行卡
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertCard($data){
		$rd = array('status'=>-1);
		$modelCard = M('user_card');
		$data["is_default"] = 1;
	    if($modelCard->create($data)){	
			$rs = $modelCard->add();
			if(false !== $rs){
				$rd['status']= $rs;
				//修改其他所有的银行卡为非默认
				$modelCard->where('user_id='.(int)$data['user_id']." and id!=".$rs)->save(array('is_default'=>0));
			}
		}
		return $rd;
	}
	
	/**
	 * 更新银行卡
	 * @param $cardId 编号
	 * @param $data   数据数组
	 * @return $rs
	 */
	public function updateCard($cardId, $data){
		$rd = array('status'=>-1);
		$modelCard = M('user_card');
		$data["is_default"] = 1;
		$rs = $modelCard->where("id=$cardId")->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
			//修改其他所有的银行卡为非默认
			$modelCard->where('user_id='.(int)$data['user_id']." and id!=".$cardId)->save(array('is_default'=>0));
		}
		return $rd;
	}
	
	/**
	 * 删除银行卡
	 * @param $cardId   编号
	 * @return $rs
	 */
	public function deleteCard($cardId){
		$rd = array('status'=>-1);
		$modelCard = M('user_card');
		$card = $modelCard->where("id=$cardId")->find();
		if($card['is_default']){
			//修改下一张银行卡为默认
			$ids = $modelCard->where("id<>".$cardId." AND user_id =".$card['user_id'])->getField('id');
			if(!empty($ids)){
				$modelCard->where("id = $ids")->save(array('is_default'=>1));
			} 
		}
		$rs = $modelCard->where("id=$cardId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 添加提现申请记录
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertWithdraw($data){
		$rd = array('status'=>-1);
		$modelWithdraw = M('user_withdraw');
	    if($modelWithdraw->create($data)){	
			$rs = $modelWithdraw->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 获取余额收支记录数
	 * @param $obj 条件数组
	 * @return $data
	 */
	public function getMoneyCount($obj){
		$userId = $obj['userId'];
		$month  = $obj['month'];
		
		$where = "user_id=$userId";
		if(isset($month)){
			$date = CBWMFristAndLast($month);
			$firstday = $date['firstday'];
			$lastday  = $date['lastday'];
			$where .= " AND create_time BETWEEN $firstday AND $lastday";
		}
		
		$sql = "SELECT count(*) AS num FROM __PREFIX__user_money WHERE $where";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	 * 获取余额收支记录
	 * @param $obj 条件数组
	 * @return $data
	 */
	public function getMoneyByObj($obj){
		$userId = $obj['userId'];
		$month  = $obj['month'];
		$m      = $obj['m'];
		$n      = $obj['n'];
		
		$where = "user_id=$userId";
		if(isset($month)){
			$date = CBWMFristAndLast($month);
			$firstday = $date['firstday'];
			$lastday  = $date['lastday'];
			$where .= " AND create_time BETWEEN $firstday AND $lastday";
		}
		
		$sql = "SELECT * FROM __PREFIX__user_money WHERE $where ORDER BY id DESC LIMIT $m,$n";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取用户草包豆收支记录个数
	 * @param $userId 会员编号
	 * @return $data
	 */
	public function getVirtualCountByUserId($userId){
		$sql = "SELECT count(*) as num FROM __PREFIX__user_virtual WHERE user_id=$userId";
		$data = $this->queryRow($sql);
		return $data['num'];
	}
	
	/**
	 * 获取用户草包豆收支记录
	 * @param $obj 条件数组
	 * @return $data
	 */
	public function getVirtualByObj($obj){
		$userId = $obj['userId'];
		$month  = $obj['month'];
		$m      = $obj['m'];
		$n      = $obj['n'];
		
		$where = "user_id=$userId";
		if(isset($month)){
			$date = CBWMFristAndLast($month);
			$firstday = $date['firstday'];
			$lastday  = $date['lastday'];
			$where .= " AND create_time BETWEEN $firstday AND $lastday";
		}
		if(isset($m)){
			$limit = "LIMIT $m,$n";
		}
		
		$sql = "SELECT * FROM __PREFIX__user_virtual WHERE $where ORDER BY create_time DESC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	 * 获取店铺加盟申请
	 * @param $userId 会员编号
	 * @return $data
	 */
	public function getRecruitByUserId($userId){
		$sql = "SELECT * FROM __PREFIX__user_recruit WHERE user_id=$userId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	 * 添加店铺加盟申请
	 * @param $data 数据数组
	 * @return $rd
	 */
	public function insertRecruit($data){
		$rd = array('status'=>-1);
		$modelRecruit = M('user_recruit');
	    if($modelRecruit->create($data)){	
			$rs = $modelRecruit->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}

	/**
	 * 论坛等级排名
	 * @param $number  数量
	 * @return $rs
	 */
	public function usersLevelRanking($number){
		$sql = "SELECT u.*,i.* FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				ORDER BY i.exp DESC LIMIT 0,$number";
		$data = $this->query($sql);
		return $data;
	}

	/**
	 * 草包豆排名
	 * @param $number  数量
	 * @return $rs
	 */
	public function usersVirtualRanking($number){
		$sql = "SELECT u.*,i.* FROM __PREFIX__users AS u
				LEFT JOIN __PREFIX__user_info AS i ON i.user_id=u.id
				ORDER BY i.virtual_money DESC LIMIT 0,$number";
		$data = $this->query($sql);
		return $data;
	}

	/**
	 * 赠送草包豆
	 * @param $userId  用户编号
	 * @param $num     数量 
	 * @param $event   活动类型 
	 * @param $sn      订单编号 
	 * @return $rs
	 */
	public function giveVirtual($userId, $num, $event, $sn){
		$user = $this->getUserAndInfoById($userId);
		// 增加
		$this->execute("UPDATE __PREFIX__user_info set virtual_money=virtual_money+$num WHERE user_id=$userId");
		
		//添加草包豆消费记录
		$modelVirtual = M('user_virtual');
		$data['user_id']     = $userId;
		$data['type']        = 1;
		$data['val']         = $num;
		$data['surplus']     = $user['virtual_money']+$num;
		$data['event']       = $event;
		$data['sn']          = $sn;
		$data['create_time'] = time();
		$modelVirtual->create($data);
		$modelVirtual->add();
	}

	/**
	 * 扣除草包豆
	 * @param $userId  用户编号
	 * @param $num     数量 
	 * @param $event   活动类型  操作事件 默认1 商城 2维修 3回收 4报价 5评价 6返还 7论坛 
	 * @param $sn      订单编号 
	 * @return $rs
	 */
	public function decreaseVirtual($userId, $num, $event, $sn){
		$user = $this->getUserAndInfoById($userId);
		// 扣除
		$this->execute("UPDATE __PREFIX__user_info set virtual_money=virtual_money-$num WHERE user_id=$userId");
		
		//添加草包豆消费记录
		$modelVirtual = M('user_virtual');
		$data['user_id']     = $userId;
		$data['type']        = 0;
		$data['val']         = $num;
		$data['surplus']     = $user['virtual_money']-$num;
		$data['event']       = $event;
		$data['sn']          = $sn;
		$data['create_time'] = time();
		$modelVirtual->create($data);
		$modelVirtual->add();
	}
	
	/**
	 * 增加账户余额
	 * @param $userId  用户编号
	 * @param $orderId 订单编号
	 * @param $money   金额
	 * @param $event   活动类型  操作事件 默认1 退款 2结算 3回收 4提现 5充值 6收益 7支付 8佣金
	 * @param $remarks 备注 
	 * @return $rs
	 */
	public function appendMoney($userId, $orderId, $money, $event, $remarks){
		$user = $this->getUserAndInfoById($userId);
		// 增加
		$this->execute("UPDATE __PREFIX__user_info set money=money+$money WHERE user_id=$userId");

		//添加余额消费记录
		$modelMoney = M('user_money');
		$data['user_id']     = $userId;
		$data['order_id']    = $orderId;
		$data['type']        = 1;
		$data['val']         = $money;
		$data['surplus']     = $user['money']+$money;
		$data['event']       = $event;
		$data['remarks']     = $remarks;
		$data['create_time'] = time();
		$modelMoney->create($data);
		$modelMoney->add();
	}
	
	/**
	 * 扣除账户余额
	 * @param $userId  用户编号
	 * @param $orderId 订单编号
	 * @param $money   金额
	 * @param $event   活动类型  操作事件 默认1 退款 2结算 3回收 4提现 5充值 6收益 7支付 8佣金
	 * @param $remarks 备注
	 * @return $rs
	 */
	public function decreaseMoney($userId, $orderId, $money, $event, $remarks){
		$user = $this->getUserAndInfoById($userId);
		// 扣除
		$this->execute("UPDATE __PREFIX__user_info set money=money-$money WHERE user_id=$userId");
		
		// 提现 增加提现金额
		if($event == '4'){
			$this->execute("UPDATE __PREFIX__user_info set drawing_money=drawing_money+$money WHERE user_id=$userId");
		}
		
		//添加余额消费记录
		$modelMoney = M('user_money');
		$data['user_id']     = $userId;
		$data['order_id']    = $orderId;
		$data['type']        = 0;
		$data['val']         = $money;
		$data['surplus']     = $user['money']-$money;
		$data['event']       = $event;
		$data['remarks']     = $remarks;
		$data['create_time'] = time();
		$modelMoney->create($data);
		$modelMoney->add();
	}
	
	/**
	 * 获取冻结金额记录
	 * @param $object  用户编号
	 * @return $rs
	 */
	public function getFrozenMoneyByObject($object){
		$userId  = $object['userId'];
		$orderId = $object['orderId'];
		$status  = $object['status'];
		$time    = $object['time'];
		
		$where = "id<>0";
		if(!empty($agentId)){
			$where .= " AND user_id=$userId";		
		}
		if(!empty($orderId)){
			$where .= " AND order_id=$orderId";		
		}
		if(isset($status) && $status !== ''){
			$where .= " AND status=$status";
		}
		if(!empty($time)){
			$where .= " AND frozen_time <= $time";
		}
		$sql ="SELECT * FROM __PREFIX__user_frozen WHERE $where ORDER BY id DESC";
		$data = $this->query($sql);
		return $data;
		
	}
	
	/**
	 * 添加冻结金额记录
	 * @param $userId  用户编号
	 * @param $orderId 订单编号
	 * @param $money   金额
	 * @param $typeId  类型  默认0  1收益
	 * @param $remarks 备注
	 * @param $day 冻结天数
	 * @return $rs
	 */
	public function insertFrozenMoney($userId, $orderId, $money, $typeId, $remarks, $day=5){
		$this->execute("UPDATE __PREFIX__user_info set frozen_money=frozen_money+$money WHERE user_id=$userId");

		//添加冻结金额记录
		$modelFrozen = M('user_frozen');
		$data['user_id']     = $userId;
		$data['order_id']    = $orderId;
		$data['type_id']     = $typeId;
		$data['money']       = $money;
		$data['create_time'] = time();
		$data['frozen_time'] = strtotime("+$day day");
		$data['remarks']     = $remarks;
		$data['status']      = 0;
		$modelFrozen->create($data);
		$modelFrozen->add();
	}
	
	/**
	 * 修改冻结金额记录
	 * @param $frozenId 记录编号
	 * @param $data     数据数组
	 * @return $rs
	 */
	public function updateFrozenMoney($frozenId, $data){
		$rd = array('status'=>-1);
		$frozenId = (int)$obj["frozenId"];
		$modelFrozen = M('user_frozen');
		$rs = $modelFrozen->where("id=$frozenId")->data($data)->save();
	    if(false !== $rs){
			$rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 解除冻结金额记录
	 * @param $frozenId 编号
	 * @param $userId   用户编号
	 * @param $orderId  订单编号
	 * @return $rs
	 */
	public function removeFrozenMoney($frozenId, $userId=0, $orderId=0){
		if($frozenId > 0){
			$where = "id=$frozenId";
		}elseif($userId > 0 && $orderId > 0){
			$where = "user_id=$userId AND order_id=$orderId";
		}else{
			return false;
		}
		$this->execute("UPDATE __PREFIX__user_frozen set status=1 WHERE $where");
		
		$sql ="SELECT * FROM __PREFIX__user_frozen WHERE $where";
		$data = $this->queryRow($sql);
		$money  = $data['money'];
		$userId = $data['user_id'];
		$this->execute("UPDATE __PREFIX__user_info set frozen_money=frozen_money-$money WHERE user_id=$userId");
	}
	
	/**
	 * 删除冻结金额记录
	 * @param $frozenId 记录编号
	 * @return $rs
	 */
	public function deleteFrozenMoney($frozenId){
		$rd = array('status'=>-1);
		$modelFrozen = M('user_frozen');
		$rs = $modelFrozen->where("id=$frozenId")->delete();
		if(false !== $rs){
		    $rd['status']= 1;
		}
		return $rd;
	}
	
	/**
	 * 删除用户User文件夹中垃圾文件
	 * @return int
	 */
	public function deleteThumb(){
		$dirs = CBWReadAllDir('./Public/User');
		$i = 0;
		foreach($dirs['dir'] as $key=>$dir){
			if($key == './Public/User/201702'){
				foreach($dir['file'] as $urls){
					$fileName = basename($urls);
					if($fileName != 'Thumbs.db'){
						$explode = explode('.', $fileName);
						$sql = "SELECT * FROM __PREFIX__user_info WHERE thumb like '%".$explode[0]."%' OR identity_positive like '%".$explode[0]."%' OR identity_back like '%".$explode[0]."%'";
						$user = $this->queryRow($sql);
						$sql = "SELECT * FROM __PREFIX__user_shop WHERE logo_image like '%".$explode[0]."%' OR mod_image like '%".$explode[0]."%' OR shop_image like '%".$explode[0]."%' OR business like '%".$explode[0]."%'";
						$shop = $this->queryRow($sql);
						if(empty($user) && empty($shop)){
							@unlink($urls);
							$i++;
						}
					}
				}
			}
		}
		return $i;
	}
	
}