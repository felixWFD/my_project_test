<?php
namespace Common\Model;
/**
 * 海报服务类
 */
class PosterModel extends BaseModel {
	protected $tableName = 'poster';
	
	/**
	* 获取海报信息
	* @$posterId 编号
	* @return $data
	*/
	public function getPosterById($posterId){
		$sql = "SELECT * FROM __PREFIX__poster WHERE id=$posterId";
		$data = $this->queryRow($sql);
		return $data;
	}
	
	/**
	* 获取海报数量
	* @$object 数组对象
	* @return $data
	*/
	public function getPosterCountByObject($object){
		$status = $object['status'];
		$where = "id<>0";
		if(isset($status)){
			$where .= " AND status=$status";
		}
		$sql = "SELECT count(*) AS number FROM __PREFIX__poster WHERE $where";
		$data = $this->queryRow($sql);
		return $data['number'];
	}
	
	/**
	* 获取海报信息
	* @$object 数组对象
	* @return $data
	*/
	public function getPosterByObject($object){
		$status = $object['status'];
		$m       = $object['m'];
		$n       = $object['n'];
		$where = "id<>0";
		if(isset($status)){
			$where .= " AND status=$status";
		}
		$limit = "";
		if(isset($m) && isset($n)){
			$limit = "LIMIT $m,$n";
		}
		$sql = "SELECT * FROM __PREFIX__poster WHERE $where ORDER BY id ASC $limit";
		$data = $this->query($sql);
		return $data;
	}
	
	/**
	* 添加海报信息
	*/
	public function insertPoster($data){
		$rd = array('status'=>-1);
	    if($this->create($data)){	
			$rs = $this->add();
			if(false !== $rs){
				$rd['status']= $rs;
			}
		}
		return $rd;
	}
	
	/**
	 * 更新海报信息
	 * @param $posterId 编号
	 * @param $data   数据数组
	 * @return $rs
	 */
	public function updatePoster($posterId, $data){
		$rd = array('status'=>-1);
		$rs = $this->where("id=$posterId")->save($data);
		if(false !== $rs){
			$rd['status']= $rs;
		}
		return $rd;
	}
	
	/**
	 * 删除海报信息
	 * @param $posterId 编号
	 * @return $rs
	 */
	public function deletePoster($posterId){
		$rd = array('status'=>-1);
		$poster = $this->getPosterById($posterId);
		$rs = $this->where("id=$posterId")->delete();
		if(false !== $rs){
			unlink("./Public/Poster/".$poster['image']);  //删除海报图
		    $rd['status']= 1;
		}
		return $rd;
	}
}