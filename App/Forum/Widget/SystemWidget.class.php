<?php
namespace Forum\Widget;
use       Think\Controller;

class SystemWidget extends Controller {

	// 用户登录信息-PC端
	public function login(){
		$users = D('Users');
		$forum = D('Forum');
		
		$userId = session('user.userid');
		$user   = $users->getUserAndInfoById($userId);
		$notice = 0;
		$notice = $forum->getNoticeCountByObject(array('receiveId'=>$userId, 'status'=>0));

		$this->assign('user', $user);
		$this->assign('notice', $notice);
        $this->display('Public:login');
	}
	
	// 用户信息-PC端
	public function user($getId){
		$users = D('Users');
		$forum = D('Forum');
		
		$id = $userId = session('user.userid');
		if($getId > 0){
			$id = $getId;
		}
		
		// 用户信息
		$user = $users->getUserAndInfoById($id);
		$user['adopt'] = $forum->getSatisfyPostCountByUserId($id);  // 采纳数量
		
		$this->assign('user', $user);
		$this->assign('userId', $userId);
        $this->display('Public:user');
	}
	
	// 网站页脚-PC端
    public function footer() {
		// 热门标签
		$forum = D('Forum');
		$tags = $forum->getTags(15);
		$this->assign('tags',$tags);	
		
		// 友情链接
		$system = D('System');
		$links = $system->loadLinks(2);
		$this->assign("links",$links);

        $this->display('Public:footer');
    }
	
	// 菜单-移动端
    public function menu() {
		$userId = session('user.userid');
		
		$this->assign('userId', $userId);
        $this->display('Public:menu');
    }
	
	// 搜索-移动端
    public function search() {
        $this->display('Public:search');
    }
	
	// 专题品牌分类-移动端
    public function brand($category) {
		$category = empty($category)?1:$category;
		
		$forum = D('Forum');
		
		$forums = $forum->getForumsByForumId();
		foreach($forums as &$vo){
			$vo['forums'] = $forum->getForumsByForumId($vo['id']);
		}
		unset($vo);
		
		$this->assign('category', $category);
		$this->assign('forums', $forums);
        $this->display('Public:brand');
    }
	
	// 专题型号分类-移动端
    public function model($category, $brand) {
		$forum = D('Forum');
		
		$forums = $forum->getForumsByForumId($brand);

		$this->assign('category', $category);
		$this->assign('forums', $forums);
        $this->display('Public:model');
    }
	
	// 大家都在搜-移动端
    public function people() {
		$forum = D('Forum');
		
		$tags = $forum->getTags(15);

		$this->assign('tags', $tags);
        $this->display('Public:people');
    }

}