<?php
return array(
	//'配置项'=>'配置值'
	'DEFAULT_THEME'         =>  'Default',	               // 默认模板主题名称
	'URL_CASE_INSENSITIVE'  =>  true,                      // 默认false 表示URL区分大小写 true则表示不区分大小写
	'URL_MODEL'             =>  2,                         // URL访问模式,可选参数0、1、2、3,代表以下四种模式：
    // 0 (普通模式); 1 (PATHINFO 模式); 2 (REWRITE  模式); 3 (兼容模式)  默认为PATHINFO 模式
	
	'URL_ROUTER_ON'   => true, //开启路由
	'URL_ROUTE_RULES' => array( //定义路由规则
	
		'/^wenda\/(\d+)$/'                => 'Thread/wenda?page=:1',
		'/^wenda$/'                       => 'Thread/wenda',

		'/^jiqiao\/(\d+)$/'               => 'Thread/jiqiao?page=:1',
		'/^jiqiao$/'                      => 'Thread/jiqiao',
		
		'/^add\/(\d+)$/'                  => 'Thread/add?category=:1',
		'/^add$/'                         => 'Thread/add',
		
		'/^view\/(\d+)$/'                 => 'Thread/view?id=:1',
		
		'/^introduce$/'                   => 'Index/introduce',
		
		'/^forum\/(\d+)$/'                => 'Forum/forum?category=:1',
		'/^forum$/'                       => 'Forum/forum',
	
		'/^brand\/(\d+)_(\d+)\/(\d+)$/'   => 'Forum/brand?category=:1&brand=:2&page=:3',
		'/^brand\/(\d+)_(\d+)$/'          => 'Forum/brand?category=:1&brand=:2',

		'/^model\/(\d+)_(\d+)\/(\d+)$/'   => 'Forum/model?category=:1&model=:2&page=:3',
		'/^model\/(\d+)_(\d+)$/'          => 'Forum/model?category=:1&model=:2',
		
		'/^user\/(\d+)$/'                 => 'Users/index?id=:1',
		'/^user$/'                        => 'Users/index',
    ),
);