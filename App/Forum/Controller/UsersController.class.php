<?php
namespace Forum\Controller;
/**
 * 会员控制器
 */
class UsersController extends BaseController {
	/**
     * 跳去登录界面
     */
	public function login($type = null){
		$userId = session('user.userid');
		if($userId != ""){
			$this->redirect("Users/index");
		}
		if(!empty($type)){
			if($type == 'weixin'){
				$config = C("THINK_SDK_WEIXIN");
				//移动设备登录
				if(CBWIsMobile()) {
					$wechatObj = new \Think\WechatApi($config['APPKEY'], $config['APPSECRET']);
					redirect($wechatObj->getOauth2URL('http://bbs.caobao.com/users/callback.html'));
				}else{
					$wechatObj = new \Think\WechatApi($config['APP_KEY'], $config['APP_SECRET']);
					redirect($wechatObj->qrconnectURL());
				}
			}
		}
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('forum');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Users/login');
	}

    /**
	 * 授权回调地址
	 * @type 类型 
	 * @code void 
	 * @return void 
	 */
    public function callback($code = null){
        (empty($code)) && $this->error('参数错误');

        $config = C("THINK_SDK_WEIXIN");
		$wechatObj = new \Think\WechatApi($config['APPKEY'], $config['APPSECRET']);
		$token = $wechatObj->accessTokenGet2($code);
        //获取当前登录用户信息
        if(is_array($token)){
			$chat = $wechatObj->infoQuery($token['openid']);
			if(is_array($chat)){
				$uw = D('UserWechat');
				$wechat = $uw->getWechatByUnionId($token['unionid']);
				if(!empty($wechat['user_id'])){
					$users = D('Users');
					$user = $users->getUserById($wechat['user_id']);
					$users->saveLogin($user);
					$this->redirect("Users/index");
				}else{
					$data = array(
						'openId'    =>$chat['openid'],
						'partnerId' =>$chat['unionid'],
						'nickName'  =>$chat['nickname'],
						'picURL'    =>base64_encode($chat['headimgurl']),
						'platform'  =>'wechat'
					);
					$this->redirect("Users/binding", $data);
				}
			}else{
				$this->error('获取微信用户信息失败');
			}
        }else{
			$this->error('微信返回数据有误');
		}
    }

	/**
	 * 微信扫码登录绑定手机界面
	 */
	public function binding(){
		$weId      = I('get.id');
		$openId    = I('get.openId');
		$partnerId = I('get.partnerId');
		$nickName  = I('get.nickName');
		$picURL    = base64_decode(I('get.picURL'));
		$platform  = I('get.platform');

		if(!empty($weId)){
			$uw = D('UserWechat');
			$wechat = $uw->getWechatByWechatId($weId);
			$openId    = $wechat['openid'];
			$partnerId = $wechat['unionid'];
			$nickName  = $wechat['nickname'];
			$picURL    = $wechat['headimgurl'];
			$platform  = 'wechat';
			
		}
	
		$this->assign('openId', $openId);
		$this->assign('partnerId', $partnerId);
		$this->assign('nickName', $nickName);
		$this->assign('picURL', $picURL);
		$this->assign('platform', $platform);
		
		// SEO
		$seo = D('Seo');
		$identSeo = $seo->getSeo('forum');
		$this->assign('title', $identSeo['title']);
		$this->assign('keywords', $identSeo['keywords']);
		$this->assign('description', $identSeo['description']);
		
		$this->display('Users/binding');
	}

	/**
	 * 检查手机号码是否有发送记录
	 */
	public function checkMobileSms(){		
		$mobile = I('post.mobile');
		$usercode = D('UserCode');
		$data = $usercode->get($mobile);
		echo json_encode($data);
	}

	/**
	 * 微信扫码登录绑定手机界面
	 */
	public function doBindingMobile(){
		$rd        = array('status'=>-1, 'msg'=>'绑定失败');
		$mobile    = I('post.mobile');
		$password  = substr($mobile, -6);
		$smscode   = I('post.smscode');
		$openId    = I('post.openId');
		$partnerId = I('post.partnerId');
		$nickName  = I('post.nickName');
		$picURL    = base64_decode(I('post.picURL'));
		$platform  = I('post.platform');
		
		$users    = D('Users');
		$usercode = D('UserCode');
		$uw       = D('UserWechat');
		
		if(empty($mobile) || empty($partnerId)){
			$rd = array('status'=>-2, 'msg'=>'参数错误');
		}else{
			$uc = $usercode->check($mobile, $smscode);   // 验证短信验证码
			if($uc == -1){
				$rd = array('status'=>-3, 'msg'=>'短信验证码超时');
			}else if($uc == -2){
				$rd = array('status'=>-4, 'msg'=>'短信验证码错误');
			}else{
				$user = $users->getUserByMobile($mobile);
				if(empty($user)){
					$rs = $users->regist($mobile, $password);
					if($rs['userId']>0){                       // 注册成功
						// 发送密码
						sendSmsByAliyun($mobile, array('acc'=>$mobile,'password'=>$password), 'SMS_116591289'); // 注册成功
						
						// 加载用户信息				
						$user = $users->getUserAndInfoById($rs['userId']);
						
						// 保存会员昵称
						$nickName = preg_replace("/[^\x{4e00}-\x{9fa5}]/iu",'',$nickName);  // 过滤特殊字符
						if(!empty($nickName)){
							$uname = strlen($nickName)>21?msubstr($nickName, 0, 7, 'utf-8', false):$nickName;
							if(!empty($uname)){
								$users->updateUser(array('user_id'=>$rs['userId'], 'uname'=>$uname));
							}
						}
						
						// 保存微信头像为会员头像
						/*if($picURL != '' && !empty($picURL)){
							$head = CBWDownloadFile($picURL, './Public/User/');
							if(!empty($head)){
								if(filesize('./Public/User/'.$head) > 0){
									$users->updateUserInfo(array('user_id'=>$rs['userId'], 'thumb'=>$head));
									@unlink('./Public/User/'.$user['thumb']);
								}else{
									@unlink('./Public/User/'.$head);
								}
							}
						}*/
					}
				}
				
				$wechat = $uw->getWechatByUnionId($partnerId);
				if(empty($wechat)){
					$data = array(
						'user_id'    => $user['id'], 
						'fakeid'     => $openId,
						'openid'     => $openId,
						'unionid'    => $partnerId,
						'nickname'   => $nickName,
						'headimgurl' => $picURL
					);
					$uw->insertWechat($data);
				}else{
					$fid = ($wechat['fid'] == $user['id'])?0:$wechat['fid'];
					if($wechat['user_id']){  // user_id不为0,则检查用户是否是店铺或代理用户(店铺用户或代理用户不能成为粉丝)
						$wuser = $users->getUserById($wechat['user_id']);
						if($wuser['group_id'] > 0 || $wuser['is_agent'] > 0){
							$fid = 0;
						}
					}
					$data = array(
						'fid'        => $fid,
						'user_id'    => $user['id'], 
						'fakeid'     => $openId,
						'openid'     => $openId,
						'nickname'   => $nickName,
						'headimgurl' => $picURL
					);
					$uw->updateWechat($wechat['id'], $data);
				}

				$users->saveLogin($user);  // 保存会员登录信息
				$usercode->del($mobile);   // 删除短信验证码相关记录
				$rd = array('status'=>$user['id']);
			}
		}
		echo json_encode($rd);
	}

	/**
	 * APP端微信授权登录操作
	 */
	public function AppWechatLogin(){
		$users = D('Users');
		$uw    = D('UserWechat');
		
		if(!empty($_POST)){
			$token     = $_POST['token'];
			$openid    = $_POST['openid'];
			$sign      = $_POST['sign'];
			$_userjson = json_decode($token, true);
			if(isset($_userjson['openid'])){           
			   $userjson = $_userjson;          
		    }else{
				$ch = curl_init();
				$str ='https://api.weixin.qq.com/sns/userinfo?access_token='.$token.'&openid='.$openid;
				curl_setopt($ch, CURLOPT_URL, $str);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
				$output = curl_exec($ch);          
				$output = json_decode($output, true);
				if(isset($output['openid'])){
					$userjson=$output;              
				}
			}
			$wechat = $uw->getWechatByUnionId($userjson['unionid']);
			$data = array(
				'openid'         => $openid,
				'sign'           => $sign,
				'unionid'        => $userjson['unionid'],
				'nickname'       => $userjson['nickname'],
				'sex'       	 => $userjson['sex'],
				'country'        => $userjson['country'],
				'province'       => $userjson['province'],
				'city'       	 => $userjson['city'],
				'headimgurl'     => $userjson['headimgurl'],
				'subscribe'      => $userjson['subscribe'],
				'subscribe_time' => $userjson['subscribe_time']
			);
			if(empty($wechat)){
				$uw->insertWechat($data);
			}else{
				if(!empty($wechat['user_id'])){
					$uw->updateWechat($wechat['id'], $data);
				}
			}
		}else{
			$openid = I('openid');
			$wechat = $uw->getWechatByOpenId($openid);
			if(!empty($wechat['user_id'])){
				$user   = $users->getUserById($wechat['user_id']);
				if(!empty($user)){
					$users->saveLogin($user);
				}			
				$this->redirect("Users/index");
			}else{
				$this->redirect("Users/binding", array('id'=>$wechat['id']));
			}
		}
	}

	/**
	 * 用户登录操作
	 * @return void 
	 */
	public function dologin(){
		$rd = array('status'=>-1);
		if (!IS_POST) {
			$rd["status"] = -2;  // 非法请求!
		}else{
			$type       = I('type');
			$username   = I('post.username');
			$password   = I('post.password');
			$code       = I('code');
			$controller = session('jump.controller');
			$action     = session('jump.action');
			if(!empty($controller) && !empty($action)){
				$rd["refer"] = U($controller.'/'.$action);
			}else{
				$rd["refer"] = U('Users/index');
			}
			
			$users = D('Users');
			$uc    = D('UserCode');
			
			if($type == 1){	// 用户账号密码登录验证
				$user = $users->checkLogin($username, $password);
				if($user['status'] == 0){
					$rd["status"] = -3;  // 登陆失败，账号被锁
				}else if($user['status'] == -1){
					$rd["status"] = -4;  // 登陆失败，账号或密码错误
					$rd["failed"] = $users->saveLoginFailed($username);  // 登陆失败次数
				}else{
					$users->saveLogin($user);  // 保存会员登录信息
					if($user['login_failed'] > 0){
						$users->updateUser(array('user_id'=>$user['id'], 'login_failed'=>0));  // 重置登录失败次数
					}
					$rd["status"] = 1;
				}
			}else{	// 用户短信验证码登录验证
				$ucheck = $uc->check($username, $code);
				if($ucheck == -1){
					$rd["status"] = -5;  // 登陆失败，验证码超时
				}else if($ucheck == -2){
					$rd["status"] = -6;  // 登陆失败，验证码错误
				}else{
					$uc->del($username);  // 删除user_code表相关记录
					$user = $users->getUserByMobile($username);
					$users->saveLogin($user);  // 保存会员登录信息
					if($user['login_failed'] > 0){
						$users->updateUser(array('user_id'=>$user['id'], 'login_failed'=>0));  // 重置登录失败次数
					}
					$rd["status"] = 1;
				}
			}
		}
		echo json_encode($rd);
	}

	/**
	 * 退出登录
	 * @return void
	 */	
	public function logout(){
		cookie(null,'cbw_');
		session('user', NULL);
		session('jump', NULL);
		session('gws', NULL);
		session('gws_count', 0);
		
		// 店铺系统退出
		cookie(null,'system_');
		session('shop', NULL);
		
		$this->redirect("Users/login");    //页面跳转
	}

	/**
	 * 用户中心
	 */
	public function index(){
		$userId = session('user.userid');
		$id     = I('get.id');
		$id     = empty($id)?$userId:$id;
		
		if(empty($id)){
			$this->error('非法请求！');
		}

		$forum = D('Forum');
		$users = D('Users');
		
		// 用户信息
		$user = $users->getUserAndInfoById($id);
		$user['adopt']    = $forum->getSatisfyPostCountByUserId($id);                                 // 采纳数量
		$user['question'] = $forum->getThreadCountByObject(array('category'=>1, 'authorId'=>$id));    // 提问数量
		$user['post']     = $forum->getPostCountByObject(array('authorId'=>$id));                     // 回答数量
		$user['share']    = $forum->getThreadCountByObject(array('category'=>2, 'authorId'=>$id));    // 分享数量
		$user['collect']  = $forum->getCollectCountByObject(array('userId'=>$id));                    // 收藏数量
		$user['activity'] = $forum->getActivityCountByObject(array('authorId'=>$id));                 // 动态数量
		$user['notice']   = $forum->getNoticeCountByObject(array('receiveId'=>$id));                  // 消息数量
		
		// 最新的发表
		$data   = $forum->getThreadByObject(array('authorId'=>$id, 'sort'=>'new', 'm'=>0, 'n'=>5));
		
		$this->assign('id', $id);
		$this->assign('userId', $userId);
		$this->assign('user', $user);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/index');
	}

	/**
	 * 提问
	 */
	public function question(){
		$userId = session('user.userid');
		$id     = I('get.id');
		$id     = empty($id)?$userId:$id;

		$forum = D('Forum');

		$page = I('get.page');
		$page = empty($page)?'1':$page;

		$number = $forum->getThreadCountByObject(array('category'=>1, 'authorId'=>$id));
		$pages  = ceil($number/8);
		$data   = $forum->getThreadByObject(array('category'=>1, 'authorId'=>$id, 'm'=>($page-1)*8, 'n'=>8));

		$this->assign('id', $id);
		$this->assign('userId', $userId);
		$this->assign('page', $page);
		$this->assign('number', $number);
		$this->assign('pages', $pages);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/question');
	}
	
	// 加载更多提问
	public function loadMoreQuestion(){
		$id      = I('post.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$forum = D('Forum');
		
		// 主题列表
		$data = $forum->getThreadByObject(array('category'=>1, 'authorId'=>$id, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 回答
	 */
	public function post(){
		$userId = session('user.userid');
		$id     = I('get.id');
		$id     = empty($id)?$userId:$id;

		$forum = D('Forum');

		$page = I('get.page');
		$page = empty($page)?'1':$page;

		$number = $forum->getPostCountByObject(array('authorId'=>$id));
		$pages  = ceil($number/8);
		$data   = $forum->getPostByObject(array('authorId'=>$id, 'm'=>($page-1)*8, 'n'=>8));
		foreach($data as &$vo){
			//帖子引用
			if($vo['quote'] != '0'){
				$quote = $forum->getThreadById($vo['quote']);
				$vo['quotesubject'] = $quote['subject'];
			}
			// 帖子评论
			$vo['cnumber'] = $forum->getCommentCountByObject(array('category'=>1, 'acceptId'=>$vo['id']));
			$vo['comments'] = $forum->getCommentsByObject(array('category'=>1, 'acceptId'=>$vo['id'], 'm'=>0, 'n'=>9));
		}
		unset($vo);
		
		$this->assign('id', $id);
		$this->assign('userId', $userId);
		$this->assign('page', $page);
		$this->assign('number', $number);
		$this->assign('pages', $pages);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/post');
	}
	
	// 加载更多回答
	public function loadMorePost(){
		$id      = I('post.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$forum = D('Forum');
		
		// 回答列表
		$data = $forum->getPostByObject(array('authorId'=>$id, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
			//帖子引用
			if($vo['quote'] != '0'){
				$quote = $forum->getThreadById($vo['quote']);
				$vo['quotesubject'] = $quote['subject'];
			}
			// 帖子评论
			$vo['cnumber'] = $forum->getCommentCountByObject(array('category'=>1, 'acceptId'=>$vo['id']));
			$vo['comments'] = $forum->getCommentsByObject(array('category'=>1, 'acceptId'=>$vo['id'], 'm'=>0, 'n'=>9));
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 分享
	 */
	public function share(){
		$userId = session('user.userid');
		$id     = I('get.id');
		$id     = empty($id)?$userId:$id;

		$forum = D('Forum');

		$page = I('get.page');
		$page = empty($page)?'1':$page;

		$number = $forum->getThreadCountByObject(array('category'=>2, 'authorId'=>$id));
		$pages  = ceil($number/8);
		$data   = $forum->getThreadByObject(array('category'=>2, 'authorId'=>$id, 'm'=>($page-1)*8, 'n'=>8));

		$this->assign('id', $id);
		$this->assign('userId', $userId);
		$this->assign('page', $page);
		$this->assign('number', $number);
		$this->assign('pages', $pages);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/share');
	}
	
	// 加载更多分享
	public function loadMoreShare(){
		$id      = I('post.id');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$forum = D('Forum');
		
		// 主题列表
		$data = $forum->getThreadByObject(array('category'=>2, 'authorId'=>$id, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 收藏
	 */
	public function collect(){
		$this->isUserLogin();

		$userId = session('user.userid');

		$forum = D('Forum');

		$page = I('get.page');
		$page = empty($page)?'1':$page;

		$number = $forum->getCollectCountByObject(array('userId'=>$userId));
		$pages  = ceil($number/8);
		$data   = $forum->getCollectByObject(array('userId'=>$userId, 'm'=>($page-1)*8, 'n'=>8));

		$this->assign('userId', $userId);
		$this->assign('page', $page);
		$this->assign('number', $number);
		$this->assign('pages', $pages);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/collect');
	}
	
	// 加载更多收藏
	public function loadMoreCollect(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$forum = D('Forum');
		
		// 收藏列表
		$data = $forum->getCollectByObject(array('userId'=>$userId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 动态
	 */
	public function activity(){
		$this->isUserLogin();
		
		$userId = session('user.userid');

		$forum = D('Forum');

		$page = I('get.page');
		$page = empty($page)?'1':$page;

		$number = $forum->getActivityCountByObject(array('authorId'=>$userId));
		$pages  = ceil($number/8);
		$data   = $forum->getActivityByObject(array('authorId'=>$userId, 'm'=>($page-1)*8, 'n'=>8));

		$this->assign('userId', $userId);
		$this->assign('page', $page);
		$this->assign('number', $number);
		$this->assign('pages', $pages);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/activity');
	}
	
	// 加载更多动态
	public function loadMoreActivity(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$forum = D('Forum');
		
		// 动态列表
		$data = $forum->getActivityByObject(array('userId'=>$userId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 消息
	 */
	public function notice(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$show   = I('get.show');
		$show   = empty($show)?'all':$show;
		$page   = I('get.page');
		$page   = empty($page)?'1':$page;

		$forum = D('Forum');
		$users = D('Users');
		
		if($show == 'unread'){
			$status = 0;
		}
		
		$number = $forum->getNoticeCountByObject(array('receiveId'=>$userId, 'status'=>$status));
		$pages  = ceil($number/8);
		$data   = $forum->getNoticeByObject(array('receiveId'=>$userId, 'status'=>$status, 'm'=>($page-1)*8, 'n'=>8));
		foreach($data as &$vo){
			if($vo['authorid']){
				$user = $users->getUserById($vo['authorid']);
				$vo['uname'] = $user['uname'];
			}else{
				$vo['uname'] = '管理员';
			}
			if($vo['category'] == '0'){
				$thread = $forum->getThreadById($vo['relationid']);
				$vo['tid'] = $thread['id'];
				$vo['content'] = msubstr($thread['subject'],0,20,'utf-8',false);
			}elseif($vo['category'] == '1'){
				if($vo['act'] == '8'){
					$thread = $forum->getThreadById($vo['relationid']);
					$vo['tid'] = $thread['id'];
					$vo['content'] = msubstr($thread['subject'],0,20,'utf-8',false);
				}else{
					$post = $forum->getPostById($vo['relationid']);
					$vo['tid'] = $post['tid'];
					$vo['content'] = msubstr($post['message'],0,20,'utf-8',false);
				}
			}elseif($vo['category'] == '2'){
				$comment = $forum->getCommentById($vo['relationid']);
				$vo['content'] = msubstr($comment['content'],0,20,'utf-8',false);
				if($comment['category'] == '0'){
					$vo['tid'] = $comment['acceptid'];
				}else{
					$post = $forum->getPostById($comment['acceptid']);
					$vo['tid'] = $post['tid'];
				}
			}
		}
		unset($vo);

		$this->assign('userId', $userId);
		$this->assign('show', $show);
		$this->assign('page', $page);
		$this->assign('number', $number);
		$this->assign('pages', $pages);
		$this->assign('data', $data);
		$this->assign('title', '用户中心');
		
		$this->display('Users/notice');
	}
	
	// 加载更多消息
	public function loadMoreNotice(){
		$this->isUserLogin();
		$userId  = session('user.userid');
		$show    = I('post.show');
		$show    = empty($show)?'all':$show;
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		$forum = D('Forum');
		$users = D('Users');
		
		if($show == 'unread'){
			$status = 0;
		}
		
		// 消息列表
		$data = $forum->getNoticeByObject(array('receiveId'=>$userId, 'status'=>$status, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
			if($vo['authorid']){
				$user = $users->getUserById($vo['authorid']);
				$vo['uname'] = $user['uname'];
			}else{
				$vo['uname'] = '管理员';
			}
			if($vo['category'] == '0'){
				$thread = $forum->getThreadById($vo['relationid']);
				$vo['tid'] = $thread['id'];
				$vo['content'] = msubstr($thread['subject'],0,20,'utf-8',false);
			}elseif($vo['category'] == '1'){
				if($vo['act'] == '8'){
					$thread = $forum->getThreadById($vo['relationid']);
					$vo['tid'] = $thread['id'];
					$vo['content'] = msubstr($thread['subject'],0,20,'utf-8',false);
				}else{
					$post = $forum->getPostById($vo['relationid']);
					$vo['tid'] = $post['tid'];
					$vo['content'] = msubstr($post['message'],0,20,'utf-8',false);
				}
			}elseif($vo['category'] == '2'){
				$comment = $forum->getCommentById($vo['relationid']);
				$vo['content'] = msubstr($comment['content'],0,20,'utf-8',false);
				if($comment['category'] == '0'){
					$vo['tid'] = $comment['acceptid'];
				}else{
					$post = $forum->getPostById($comment['acceptid']);
					$vo['tid'] = $post['tid'];
				}
			}
		}
		unset($vo);
		echo json_encode($data);
	}

	/**
	 * 删除消息
	 */
	public function delNotice(){
		$this->isUserLogin();
		
		$userId = session('user.userid');
		$id     = I('get.id');
		
		$forum = D('Forum');
		
		$notice = $forum->getNoticeById($id);
		if($notice['receiveid'] == $userId){
			$rt = $forum->deleteNotice($id);
			if($rt['status'] > 0){
				$this->redirect("Users/notice");
			}
		}
		$this->error('删除失败');
	}

	/**
	 * 批量删除消息
	 */
	public function batchDelNotice(){
		$this->isUserLogin();
		
		$userId  = session('user.userid');
		$notices = I('post.notices');
		
		$forum = D('Forum');
		
		foreach($notices as $id){
			$notice = $forum->getNoticeById($id);
			if($notice['receiveid'] == $userId){
				$forum->deleteNotice($id);
			}
		}
		echo true;
	}

	/**
	 * 编辑
	 */
	public function edit(){
		$this->isUserLogin();
		
		$userId = session('user.userid');

		$users = D('Users');
		$area  = D('Area');
		
		$user = $users->getUserAndInfoById($userId);
		
		// 常居地
		$addr = $area->getAreaById($user['area_id']);
		$p = empty($addr['one_id'])?'选择省':$addr['one_id'];
		$c = empty($addr['two_id'])?'选择市':$addr['two_id'];
		$a = empty($addr['id'])?'选择县/区':$addr['id'];
		$cityArray = array('province'=>$p, 'city'=>$c, 'area'=>$a);
		$city = \Think\Area::city($cityArray);
		
		$this->assign("city", $city);
		$this->assign('user', $user);
		$this->assign('title', '用户中心');
		
		$this->display('Users/edit');
	}
	
	/**
     * 更新个人信息
     */
	public function doEditUser(){
		$this->isUserLogin();
		
		$userId    = session('user.userid');
		$uname     = I('post.uname');
		$uname     = strlen($uname)>21?msubstr($uname, 0, 7, 'utf-8', false):$uname;
		$area      = I('post.area');
		$address   = I('post.address');
		$oldpwd    = I('post.oldpwd');
		$newpwd    = I('post.newpwd');
		$againpwd  = I('post.againpwd');
		$signature = I('post.signature');
		$signature = strlen($signature)>21?msubstr($signature, 0, 65, 'utf-8', false):$signature;
		
		$users = D('Users');
		
		$user = $users->getUserById($userId);
		
		$urt = $users->updateUser(array('user_id'=>$userId, 'uname'=>$uname));
		if(!empty($oldpwd) && !empty($newpwd) && !empty($againpwd)){
			if($user['password'] !== sha1($oldpwd)){
				$this->error('旧密码错误');
			}
			if($newpwd === $againpwd){
				$urt = $users->updatePass($userId, $newpwd);
			}
		}
		
		$irt = $users->updateUserInfo(array('user_id'=>$userId, 'area_id'=>$area, 'address'=>$address, 'signature'=>$signature));
		if($urt['status'] > 0 || $irt['status'] > 0){
			$this->success('编辑成功', U('Users/edit'));
		}else{
			$this->error('编辑失败');
		}
	}

	/**
	 * 头像
	 */
	public function avatar(){
		$this->isUserLogin();
		
		$userId = session('user.userid');

		$users = D('Users');
		
		$user = $users->getUserAndInfoById($id);

		$this->assign('user', $user);
		$this->assign('title', '用户中心');
		
		$this->display('Users/avatar');
	}
	
	// 裁剪图片
	public function cropSave(){
		$this->isUserLogin();
		$userId = session('user.userid');
		
		$crop = new \Think\CropAvatar(
		  './Public/Temp/',
		  './Public/User/',
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
		);
		$image   = $crop -> getResult();
		$message = $crop -> getMsg();
		if(!empty($image)){
			$users = D('Users');
			$user = $users->getUserAndInfoById($userId);
			$rt = $users->updateUserInfo(array('user_id'=>$userId, 'thumb'=>$image));
			if($rt['status'] > 0){
				unlink("./Public/User/".$user['thumb']);  // 删除旧头像
			}
		}
		echo json_encode(array('state'  => 200, 'message' => $message, 'result' => $image));
	}
	
}
?>
