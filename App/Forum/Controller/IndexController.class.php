<?php
namespace   Forum\Controller;

class IndexController extends BaseController {
	
    public function __construct(){
        parent::__construct();
    }
	
	// 首页控制器
    public function index(){
		
		$forum = D('Forum');
		$users = D('Users');
		
		// 最有帮助的问题
		$help_issue = $forum->getThreadByObject(array('category'=>1, 'sort'=>'helpful', 'm'=>0, 'n'=>6));
		// 最新问题
		$new_issue  = $forum->getThreadByObject(array('category'=>1, 'sort'=>'new', 'm'=>0, 'n'=>6));
		foreach($new_issue as &$vo){
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			$vo['cname']     = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		unset($vo);
		
		// 热门分享
		$hot_share  = $forum->getThreadByObject(array('category'=>2, 'sort'=>'heats', 'm'=>0, 'n'=>6));
		// 最新分享
		$new_share  = $forum->getThreadByObject(array('category'=>2, 'sort'=>'new', 'm'=>0, 'n'=>6));
		foreach($new_share as &$vo){
			// 图片处理
			if(empty($vo['attachment'])){
				$schedules = $forum->getScheduleByThreadId($vo['id']);
				foreach($schedules as $sche){
					if(!empty($sche['attachment'])){
						$threadRes[$key]['attachment'] = $sche['attachment'];
						break;
					}
				}
			}
			
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
		}
		unset($vo);
		
		// 品牌专题
		$ids = array(3,4,5,6);
		for($i=0; $i<4; $i++){
			$brands[$i] = $forum->getForumById($ids[$i]);
			$models = $forum->getForumsByForumId($ids[$i]);
			$fid        = array();
			$statistics = 0;
			foreach($models as $key=>$vo){
				$fid[$key] = $vo['id'];
				$statistics += $forum->getForumStatisticsByForumId($vo['id'], 1);
			}
			$brands[$i]['statistics'] = $statistics;
			$brands[$i]['thread'] = $forum->getThreadByObject(array('fid'=>$fid, 'category'=>1, 'sort'=>'new', 'm'=>0, 'n'=>6));
		}

		// 等级排名
		$levels = $users->usersLevelRanking(5);
		foreach($levels as &$vo){
			$vo['level']     = CBWExpLevel($vo['exp']);
			$vo['levelname'] = CBWExpLevel($vo['exp'],'name');
		}
		unset($vo);

		// 回答行家
		$posts = $forum->postRanking(5);
		foreach($posts as &$vo){
			$user = $users->getUserAndInfoById($vo['authorid']);
			$vo['uname'] = $user['uname'];
			$vo['thumb'] = $user['thumb'];
		}
		unset($vo);

		// 分享达人
		$shares = $forum->shareRanking(5);
		foreach($shares as &$vo){
			$user = $users->getUserAndInfoById($vo['authorid']);
			$vo['uname'] = $user['uname'];
			$vo['thumb'] = $user['thumb'];
		}
		unset($vo);
		
		// 财富排名
		$virtuals = $users->usersVirtualRanking(5);
		
		$this->assign('help_issue', $help_issue);
		$this->assign('new_issue', $new_issue);
		$this->assign('hot_share', $hot_share);
		$this->assign('new_share', $new_share);
		$this->assign('brands', $brands);
		$this->assign('levels', $levels);
		$this->assign('posts', $posts);
		$this->assign('shares', $shares);
		$this->assign('virtuals', $virtuals);
		
		$this->assign('title', '专业的手机维修论坛 ');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display();
    }
	
	// 论坛说明
	public function introduce(){
		$this->assign('title', '手机维修解答论坛');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		$this->display();
	}

}