<?php
namespace Forum\Controller;

class SearchController extends BaseController {
    
	/**
     * 搜索
     */
	public function index(){
		$keyword = I('get.keyword');
		$page    = I('get.page');
		$page    = empty($page)?'1':$page;
		
		if(preg_match("/[\',:;*?~`!@#$%^&+=)(<>{}]|\]|\[|\/|\\\|\"|\|/",$keyword)){
			$this->error("搜索词 不允许带有特殊字符");
		} 
		
		$forum = D('Forum');
		
		$number = $forum->getThreadCountByObject(array('keyword'=>$keyword));
		$pages  = ceil($number/15);
		$data   = $forum->getThreadByObject(array('keyword'=>$keyword, 'sort'=>'helpful', 'm'=>($page-1)*15, 'n'=>15));
		foreach($data as &$vo){
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
			}
		}
		unset($vo);
		
		// 最新回答
		$news = $forum->getPostByObject(array('m'=>0, 'n'=>15));

		$this->assign('keyword', $keyword);
		$this->assign('data', $data);
		$this->assign("page", $page);
		$this->assign("pages", $pages);
		$this->assign("number", $number);
		$this->assign("news", $news);
		
		$this->assign('title', '手机维修解答论坛');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');

        $this->display('index');
	}
	
	// 加载更多动态
	public function loadMoreSearch(){
		$keyword = I('post.keyword');
		$counter = I('post.counter');
		$counter = intval($counter?$counter:0);
		$num     = I('post.num');
		
		if(preg_match("/[\',:;*?~`!@#$%^&+=)(<>{}]|\]|\[|\/|\\\|\"|\|/",$keyword)){
			$this->error("搜索词 不允许带有特殊字符");
		} 
		
		$forum = D('Forum');

		$data   = $forum->getThreadByObject(array('keyword'=>$keyword, 'sort'=>'helpful', 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = CBWTimeFormat($vo['dateline'], 'm.d');
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = CBWTimeFormat($post['dateline'], 'm.d');
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = CBWTimeFormat($comment['dateline'], 'm.d');
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
			}
		}
		unset($vo);
		echo json_encode($data);
	}
	
}
?>
