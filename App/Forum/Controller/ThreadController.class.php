<?php
namespace   Forum\Controller;

class ThreadController extends BaseController {
	
    public function __construct(){
        parent::__construct();
    }
	
	// 问题与解答控制器
	public function wenda(){
		
		$sort     = I('get.sort');
		$page     = I('get.page');
		$page     = empty($page)?'1':$page;
		$category = 1;
		
		$forum = D('Forum');
		
		// 进度条
		$ago      = time()-24*60*60;
		$snumber  = $forum->getThreadCountByObject(array('category'=>$category, 'dateline'=>$ago));
		$unanswer = $forum->getThreadCountByObject(array('category'=>$category, 'dateline'=>$ago, 'replies'=>'replies'));
		$pace     = 100-ceil(($unanswer/$snumber)*100);

		// 问题列表
		$replies = '';
		if($sort == 'unanswer'){
			$replies = 'unanswer';
		}
		$number = $forum->getThreadCountByObject(array('category'=>$category, 'replies'=>$replies, 'sort'=>$sort));
		$pages  = ceil($number/15);
		$data   = $forum->getThreadByObject(array('category'=>$category, 'replies'=>$replies, 'sort'=>$sort, 'm'=>($page-1)*15, 'n'=>15));
		foreach($data as &$vo){
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			$vo['cname']     = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		
		// 最新回答
		$news = $forum->getPostByObject(array('m'=>0, 'n'=>15));

		$this->assign('sort', $sort);
		$this->assign('snumber', $snumber);
		$this->assign('unanswer', $unanswer);
		$this->assign('pace', $pace);
		$this->assign('data', $data);
		$this->assign("page", $page);
		$this->assign("pages", $pages);
		$this->assign("news", $news);
		$this->assign("category", $category);
		
		$title       = '手机维修问题_手机修理答疑_手机维修技术讨论_手机维修问答讨论';
		$keywords    = '手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术';
		$description = '草包网手机维修论坛问答栏目为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修过程中的各种问题都可在草包论坛提问并得到答案。';

		$this->assign('title', $title);
		$this->assign('keywords', $keywords);
		$this->assign('description', $description);
		
		$this->display();
	}
	
	// 技巧分享控制器
	public function jiqiao(){
		
		$sort     = I('get.sort');
		$page     = I('get.page');
		$page     = empty($page)?'1':$page;
		$category = 2;
		
		$forum = D('Forum');
		
		// 分享列表
		$sub = '';
		if($sort == 'use'){
			$sub = 1;
		}elseif($sort == 'repair'){
			$sub = 2;
		}
		$number = $forum->getThreadCountByObject(array('category'=>$category, 'sub'=>$sub));
		$pages  = ceil($number/15);
		$data   = $forum->getThreadByObject(array('category'=>$category, 'sub'=>$sub, 'm'=>($page-1)*15, 'n'=>15));
		foreach($data as &$vo){
			// 图片处理
			if(empty($vo['attachment'])){
				$schedules = $forum->getScheduleByThreadId($vo['id']);
				foreach($schedules as $key => $sche){
					if(!empty($sche['attachment'])){
						$threadRes[$key]['attachment'] = $sche['attachment'];
						break;
					}
				}
			}
			
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
		}
		
		// 最新回答
		$news = $forum->getPostByObject(array('m'=>0, 'n'=>15));
		
		$this->assign('sort', $sort);
		$this->assign('data', $data);
		$this->assign("page", $page);
		$this->assign("pages", $pages);
		$this->assign("news", $news);
		$this->assign("category", $category);
		
		$title       = '手机维修技巧_手机拆机步骤教程_手机修理技术学习_手机维修经验分享';
		$keywords    = '手机维修技巧,手机维修经验,手机拆机步骤';
		$description = '最新手机维修技巧、手机拆机步骤图文详解，让你学习到更多手机修理技术，积累更多手机维修经验。';

		$this->assign('title', $title);
		$this->assign('keywords', $keywords);
		$this->assign('description', $description);
		
		$this->display();
	}
	
	// 加载更多主题
	public function loadMoreThread(){
		
		$category = I('post.category');
		$counter  = I('post.counter');
		$counter  = intval($counter?$counter:0);
		$num      = I('post.num');
		
		$forum = D('Forum');
		
		// 主题列表
		$data = $forum->getThreadByObject(array('category'=>$category, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			// 图片处理
			if(empty($vo['attachment'])){
				$schedules = $forum->getScheduleByThreadId($vo['id']);
				foreach($schedules as $sche){
					if(!empty($sche['attachment'])){
						$threadRes[$key]['attachment'] = $sche['attachment'];
						break;
					}
				}
			}
			
			$vo['userid']      = $vo['authorid'];
			$vo['occurtime']   = CBWTimeFormat($vo['dateline'], 'm.d');
			$vo['content']     = msubstr($vo['content'], 0, 26, 'utf-8', true);
			$vo['description'] = msubstr($vo['description'], 0, 26, 'utf-8', true);
			$vo['cname']       = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = CBWTimeFormat($post['dateline'], 'm.d');
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = CBWTimeFormat($comment['dateline'], 'm.d');
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		echo json_encode($data);
	}
	
	// 主题详情页面
	public function view(){
		
		$id       = I('get.id');
		$noticeId = I('get.nid');
		$userId   = session('user.userid');
		
		$forum = D('Forum');
		
		// 是否带有通知id
		if(!empty($noticeId)){
			$forum->updateNotice($noticeId, array('status'=>1));
		}
		
		// 主题
		$data = $forum->getThreadById($id);
		!isset($data) && $this->redirect('Empty/index');
		
		// 主题热度值
		$forum->statistics($data['id']);
		
		// 用户投票,收藏主题
		if($userId){
			$data['vote']    = $forum->getVoteByUserId($userId, $data['id'], 0);
			$data['collect'] = $forum->getCollectByUserId($userId, $data['id']);
		}
		
		// 主题引用
		if($data['quote'] != '0'){
			$quote = $forum->getThreadById($data['quote']);
			$data['quotesubject'] = $quote['subject'];
		}
		
		// 主题评论
		$data['cnumber'] = $forum->getCommentCountByObject(array('category'=>0, 'acceptId'=>$data['id']));
		$data['comments'] = $forum->getCommentsByObject(array('category'=>0, 'acceptId'=>$data['id'], 'm'=>0, 'n'=>9));
		
		if($data['category'] == '1'){
			// 最有帮助的答案
			$satisfy = $forum->getSatisfyPostByThreadId($id);
			if(!empty($satisfy)){
				//帖子引用
				if($satisfy['quote'] != '0'){
					$quote = $forum->getThreadById($satisfy['quote']);
					$satisfy['quotesubject'] = $quote['subject'];
				}
				// 帖子评论
				$satisfy['cnumber']  = $forum->getCommentCountByObject(array('category'=>1, 'acceptId'=>$satisfy['id']));
				$satisfy['comments'] = $forum->getCommentsByObject(array('category'=>1, 'acceptId'=>$satisfy['id'], 'm'=>0, 'n'=>9));
				// 用户投票
				if($userId){
					$satisfy['vote']    = $forum->getVoteByUserId($userId, $satisfy['id'], 1);
				}
			}
		}else{
			// 工具
			$tools = $forum->getScheduleByThreadId($id, 1);
			// 方法
			$steps = $forum->getScheduleByThreadId($id, 2);
			foreach($steps as &$vo){
				$quote = $forum->getThreadById($vo['quote']);
				$vo['quotesubject'] = $quote['subject'];
			}
			unset($vo);
			// 注意事项
			$matters = $forum->getScheduleByThreadId($id, 3);
		}

		// 主题帖子
		$posts = $forum->getPostByObject(array('threadId'=>$id, 'satisfy'=>0, 'm'=>0, 'n'=>15));
		foreach($posts as &$vo){
			//帖子引用
			if($vo['quote'] != '0'){
				$quote = $forum->getThreadById($vo['quote']);
				$vo['quotesubject'] = $quote['subject'];
			}
			// 帖子评论
			$vo['cnumber'] = $forum->getCommentCountByObject(array('category'=>1, 'acceptId'=>$vo['id']));
			$vo['comments'] = $forum->getCommentsByObject(array('category'=>1, 'acceptId'=>$vo['id'], 'm'=>0, 'n'=>9));
			// 用户投票
			if($userId){
				$vo['vote'] = $forum->getVoteByUserId($userId, $vo['id'], 1);
			}
		}
		unset($vo);
		
		// 相关主题
		$relevants = $forum->getThreadByObject(array('fid'=>$data['fid'], 'category'=>$data['category'], 'm'=>0, 'n'=>8));
		
		// 推荐主题
		$recommends = $forum->getThreadByObject(array('fid'=>$data['fid'], 'category'=>$data['category'], 'sort'=>'helpful', 'm'=>0, 'n'=>8));
		
		$this->assign('by', $by);
		$this->assign('data', $data);
		$this->assign('satisfy', $satisfy);
		$this->assign('tools', $tools);
		$this->assign('steps', $steps);
		$this->assign('matters', $matters);
		$this->assign('posts', $posts);
		$this->assign('relevants', $relevants);
		$this->assign('recommends', $recommends);
		
		$this->assign('title', $data['name'].' '.$data['subject']);
		$this->assign('keywords', $data['tags']);
		$this->assign('description', msubstr(empty($data['content'])?$data['description']:$data['content'], 0, 50, 'utf-8', true));
		
		$this->display();
	}
	
	// 发布主题页面
	public function add(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		$category = I('get.category');
		$category = empty($category)?1:$category;
		
		$forum = D('Forum');
		$view = 'share';
		if($category == '1'){
			$view = 'question';
		}
		
		$ago      = time()-24*60*60;
		$snumber  = $unanswer = $suser = $sshare = 0;
		$snumber  = $forum->getThreadCountByObject(array('category'=>1, 'dateline'=>$ago));
		$unanswer = $forum->getThreadCountByObject(array('category'=>1, 'replies'=>0, 'dateline'=>$ago));
		$suser    = $forum->getUsersCountByObject(array('category'=>2, 'dateline'=>$ago));
		$sshare   = $forum->getThreadCountByObject(array('category'=>2, 'dateline'=>$ago));

		$this->assign('category', $category);
		$this->assign('snumber', $snumber);
		$this->assign('unanswer', $unanswer);
		$this->assign('suser', $suser);
		$this->assign('sshare', $sshare);
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display($view);
	}
	
	// POST搜索版块
	public function searchForum(){
		
		$select = I('post.select');

		$forum = D('Forum');

		$qian=array(" ","　","\t","\n","\r");
		$hou=array("","","","","");
		$select = str_replace($qian,$hou,$select);    
		
		$forums = $forum->getForumByKeyword($select);
		foreach($forums as &$vo){
			$vo['content'] = msubstr($vo['content'], 0, 105, true);
			$vo['select']  = $select;
		}
		unset($vo);
		echo json_encode($forums);exit;
	}
	
	// POST搜索主题
	public function searchThread(){
		
		$id = I('post.id');
		$forum = D('Forum');
		$data = $forum->getThreadById($id);
		echo json_encode($data);exit;
	}
	
	// 发布主题操作
	public function doNewThread(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			$this->error('非法请求！');
		}

		$userId = session('user.userid');
		
		$forum = D('Forum');
		$users = D('Users');

		// 是否操作频繁
		$thread = $forum->getThreadByObject(array('authorid'=>$userId, 'sort'=>'new', 'm'=>0, 'n'=>1));
		$value = time()-$thread['dateline'];
		if($value < 180){
			$this->error('频繁发布,请三分钟后再发布！');
		}
		
		//敏感词提示
		$content     = nl2br(I('post.description'));
		$description = nl2br(I('post.content'));
		$strpos = $this->_strposWords($content.$description);
		if($strpos){
			$this->error('存在敏感词！');
		}
		
		$data['fid']          = I('post.fid');
		$data['category']     = I('post.category');
		$data['sub']          = I('post.sub');
		$data['authorid']     = $userId;
		$data['subject']      = I('post.subject');
		$data['dateline']     = time();
		$data['description']  = $content;
		$data['content']      = $description;
		
		// 用户等级达到2级以上,提问免审核
		if($data['category'] == '2'){
			$user  = $users->getUserAndInfoById($userId);
			$level = CBWExpLevel($user['exp']);
		}else{
			$level = 3;
		}
		if($level >= 2){
			$data['displayorder'] = '0';
		}else{
			$data['displayorder'] = '-2';
		}

		$data['quote']        = I('post.quote');
		$data['original']     = I('post.original');
		$data['notice']       = I('post.notice');
		
		// 标签
		$tag = I('post.tags');
		$tag = str_replace(" ", ",", $tag);    //空格替换成英文逗号
		$tag = str_replace("，", ",", $tag);   //中文逗号替换成英文逗号
		$tag = str_replace("|", ",", $tag);    //竖线替换成英文逗号
		$tagdata = explode(",", $tag);
		foreach($tagdata as $vo){
			if(!empty($vo) && $vo != ''){
				$tags[] = $vo;
			}
		}
		$data['tags'] = implode("," ,$tags);

		$rt = $forum->insertThread($data);
		if($rt['status'] > 0){
			
			// 保存简介图片
			$attachment = I('post.attachment');
			if(!empty($attachment)){
				$image = CBWBase64Upload($attachment, './Public/bbs/attachment/');
				$forum->updateThread($rt['status'], array('attachment'=>$image));
			}
			
			// 保存工具
			$tools = I('post.tools');
			if(!empty($tools)){
				foreach($tools as $key=>$tool){
					$forum->insertThreadSchedule(array('tid'=>$rt['status'], 'category'=>1, 'content'=>$tool, 'displayorder'=>($key+1)));
				}
			}
			// 保存方法
			$steps       = I('post.step');
			$attachments = I('post.attachments');
			$quotes      = I('post.quotes');
			if(!empty($steps)){
				foreach($steps as $key=>$step){
					$image = '';
					if(!empty($attachments[$key])){
						$image = CBWBase64Upload($attachments[$key], './Public/bbs/attachment/');
					}
					$forum->insertThreadSchedule(array('tid'=>$rt['status'], 'category'=>2, 'content'=>$step, 'quote'=>$quotes[$key], 'attachment'=>$image, 'displayorder'=>($key+1)));
				}
			}
			
			//保存注意事项
			$matters = I('post.matters');
			if(!empty($matters)){
				foreach($matters as $key=>$matter){
					$forum->insertThreadSchedule(array('tid'=>$rt['status'], 'category'=>3, 'content'=>$matter, 'displayorder'=>($key+1)));
				}
			}
			
			// 添加标签表
			$this->_addtags($data['tags']);

			if($level >= 2){
				if($data['category'] == '0'){
					$this->_actactivity($userId,$rt['status'],1,1,20);      // 经验增加  经验值+20
				}else{
					$this->_actactivity($userId,$rt['status'],1,1,40,30);   // 经验增加  经验值+40  虚拟币+30
				}
				CBWBaiduPush(array(C('BBS_URL')."/view/".$rt['status'].".html"), 'bbs');  // 百度SEO推送
				//$this->success('添加成功', C('BBS_URL').'/view/'.$rt['status'].'.html');exit;
				$this->redirect(C('BBS_URL')."/view/".$rt['status']);exit;
			}else{
				$this->redirect('Thread/finish', array('category'=>$data['category']));
			}
		}
		$this->error('发布失败');exit;
	}
	
	// 发布主题成功
	public function finish(){
		
		$category = I('get.category');
		
		$this->assign('category', 'category');
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display();
	}
	
	// 编辑主题
	public function edit(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		
		$id = I('get.id');
		
		$forum = D('Forum');
		
		// 主题评价数量
		$number = $forum->getPostCountByObject(array('threadId'=>$id));
		$data   = $forum->getThreadById($id);
		if($number > 0 && $data['category'] == 1){
			$this->error('主题已有回复,不允许编辑！');exit;
		}

		if($data['quote'] != '0'){
			$quote = $forum->getThreadById($data['quote']);
			if(!empty($quote)){
				$data['quotesubject'] = $quote['subject'];
			}
		}
		$data['forum'] = $forum->getForumById($data['fid']);
		$data['tools']   = $forum->getScheduleByThreadId($data['id'], 1);
		$steps   = $forum->getScheduleByThreadId($data['id'], 2);
		foreach($steps as $key=>&$vo){
			$quote = $forum->getThreadById($vo['quote']);
			if(!empty($quote)){
				$vo['quotesubject'] = $quote['subject'];
			}
		}
		unset($vo);
		$data['steps'] = $steps;
		$data['matters'] = $forum->getScheduleByThreadId($data['id'], 3);
		
		$view = 'editQuestion';
		if($data['category'] == '2'){
			$view = 'editShare';
		}
		
		$this->assign('data', $data);
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display($view);
	}
	
	// 提交编辑主题
	public function doEditThread(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			$this->error('非法请求！');
		}
		
		$userId   = session('user.userid');
		$threadId = I('post.id');
		
		$forum = D('Forum');
		
		$thread = $forum->getThreadById($threadId);
		if($thread['authorid'] != $userId){
			$this->error('不允许编辑他人主题！');
		}
		
		//敏感词提示
		$content     = nl2br(I('post.description'));
		$description = nl2br(I('post.content'));
		$strpos = $this->_strposWords($content.$description);
		if($strpos){
			$this->error('存在敏感词！');
		}
		
		$data['fid']          = I('post.fid');
		$data['sub']          = I('post.sub');
		$data['subject']      = I('post.subject');
		$data['lastdate']     = time();
		$data['description']  = $content;
		$data['content']      = $description;
		$data['displayorder'] = '-2';
		$data['quote']        = I('post.quote');
		$data['original']     = I('post.original');
		$data['notice']       = I('post.notice');
		
		// 标签
		$tag = I('post.tags');
		$tag = str_replace(" ", ",", $tag);    //空格替换成英文逗号
		$tag = str_replace("，", ",", $tag);   //中文逗号替换成英文逗号
		$tag = str_replace("|", ",", $tag);    //竖线替换成英文逗号
		$tagdata = explode(",", $tag);
		foreach($tagdata as $vo){
			if(!empty($vo) && $vo != ''){
				$tags[] = $vo;
			}
		}
		$data['tags'] = implode("," ,$tags);
		
		$rt = $forum->updateThread($threadId, $data);
		if($rt['status'] > 0){
			
			// 保存简介图片
			$attachment = I('post.attachment');
			if(!empty($attachment)){
				$image = CBWBase64Upload($attachment, './Public/bbs/attachment/');
				$forum->updateThread($threadId, array('attachment'=>$image));
				
				if(!empty($thread['attachment'])){
					@unlink('./Public/bbs/attachment/'.$thread['attachment']);
				}
			}
			
			// 保存工具
			$tools = I('post.tools');
			if(!empty($tools)){
				$forum->deleteThreadSchedule($threadId, 1);
				foreach($tools as $key=>$tool){
					$forum->insertThreadSchedule(array('tid'=>$threadId, 'category'=>1, 'content'=>$tool, 'displayorder'=>($key+1)));
				}
			}
			// 保存方法
			$steps       = I('post.step');
			$attachments = I('post.attachments');
			$olds        = I('post.old_attachments');
			$quotes      = I('post.quotes');
			if(!empty($steps)){
				$schedules = $forum->getScheduleByThreadId($threadId, 2);
				// 旧图移动到临时文件夹
				foreach($schedules as $vo){
					if(!empty($vo['attachment'])){
						CBWMoveFile('./Public/bbs/attachment/'.$vo['attachment'], './Public/Temp/'.date('Ymd').'/');
					}
				}
				// 删除
				$forum->deleteThreadSchedule($threadId, 2);
				
				// 添加
				foreach($steps as $key=>$step){
					if(!empty($attachments[$key])){
						$image = CBWBase64Upload($attachments[$key], './Public/bbs/attachment/');
					}else{
						if(!empty($olds[$key])){
							// 将图片从临时文件夹移回
							$name = substr($olds[$key], strrpos($olds[$key], '/')+1);
							CBWMoveFile('./Public/Temp/'.date('Ymd').'/'.$name, './Public/bbs/attachment/'.date('Ym').'/');
							$image = date('Ym').'/'.$name;
						}else{
							$image = '';
						}
					}
					$forum->insertThreadSchedule(array('tid'=>$threadId, 'category'=>2, 'content'=>$step, 'quote'=>$quotes[$key], 'attachment'=>$image, 'displayorder'=>($key+1)));
				}
			}
			
			//保存注意事项
			$matters = I('post.matters');
			if(!empty($matters)){
				$forum->deleteThreadSchedule($threadId, 3);
				foreach($matters as $key=>$matter){
					$forum->insertThreadSchedule(array('tid'=>$threadId, 'category'=>3, 'content'=>$matter, 'displayorder'=>($key+1)));
				}
			}
			
			// 添加标签表
			if(!empty($data['tags'])){
				$this->_addtags($data['tags'], $thread['tags']);
			}

			$this->success('编辑成功', C('BBS_URL').'/view/'.$threadId.'.html');exit;
		}
		$this->error('编辑失败', C('BBS_URL').'/thread/edit/id/'.$threadId.'.html');exit;
	}
	
	// 添加回帖
	public function addpost(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		$threadId = I('get.id');

		$forum = D('Forum');
		
		// 主题信息
		$data = $forum->getThreadById($threadId);
		$this->assign('data', $data);
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display();
	}
	
	// 添加回帖操作
	public function doAddPost(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			$this->error('非法请求！');
		}
		
		$userId = session('user.userid');
		
		$users = D('Users');
		$forum = D('Forum');
		
		$message  = I('post.message');
		$threadId = I('post.tid');
		$quote    = I('post.quote');
		$insert   = I('post.insert');
		
		// 主题信息
		$thread = $forum->getThreadById($threadId);
		if($thread['authorid'] == $userId){
			$this->error('不允许回答/回复自己的主题！');
		}
		// 是否重复回贴
		$post = $forum->getPostByUserId($userId, $threadId);
		if(!empty($post)){
			$this->error('同一主题不能重复回答/回复！');
		}
		
		// 是否操作频繁
		$post = $forum->getLastPostByUserId($userId);
		$val  = time()-$post['dateline'];
		if($val < 30){
			$this->error('回答/回复频繁,请30秒后再操作');
		}

		// 敏感词提示
		$strpos = $this->_strposWords($message);
		if($strpos){
			$this->error('存在敏感词！');
		}
		
		$data['tid']        = $threadId;
		$data['authorid']   = $userId;
		$data['message']    = $message;
		$data['quote']      = $quote;
		$data['dateline']   = time();
		$data['useip']      = ip2long(I('server.REMOTE_ADDR'));
		
		// 用户等级达到2级以上,免审核
		$user  = $users->getUserAndInfoById($userId);
		$level = CBWExpLevel($user['exp']);
		if($level >= 2){
			$data['invisible'] = '0';
		}else{
			$data['invisible'] = '-2';
		}
		
		$rt = $forum->insertPost($data);
		if($rt['status'] > 0){
			// 保存图片
			if(!empty($insert)){
				$image = CBWBase64Upload($insert, './Public/bbs/attachment/');
				$forum->updatePost($rt['status'], array('attachment'=>$image));
			}
			
			//主题信息
			$thread = $forum->getThreadById($threadId);
			
			// 站内消息通知
			$this->_notice($userId,$thread['authorid'],$threadId,1,0);
			
			// 主题回复次数+1
			$forum->updateThreadStatus($threadId, 'replies', 1);
			
			if($level >= 2){
				// 经验增加  回贴+10  虚拟币+5
				$this->_actactivity($userId,$threadId,2,1,10,5);
				
				// 短信与邮件通知
				if($thread['notice'] == '1'){
					$notice = array(
						'tid'     => $threadId,
						'userid'  => $thread['authorid'],
						'subject' => $thread['subject'],
						'message' => $message
					);
					$this->_smsnotice($notice);
				}
				$this->redirect(C('BBS_URL')."/view/".$threadId);exit;
				//$this->success('添加成功', C('BBS_URL').'/view/'.$threadId.'.html');exit;
			}else{
				$this->redirect('Thread/finish', array('category'=>$data['category']));
			}
		}
		$this->error('未知错误!');
	}
	
	// 编辑帖子
	public function editpost(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		
		$id = I('get.id');
		
		$forum = D('Forum');
		
		$data  = $forum->getPostById($id);
		
		if($data['quote'] != '0'){
			$quote = $forum->getThreadById($data['quote']);
			if(!empty($quote)){
				$data['quotesubject'] = $quote['subject'];
			}
		}
		
		$this->assign('data', $data);
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display();
	}
	
	// 提交编辑帖子
	public function doEditPost(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			$this->error('非法请求！');
		}
		
		$userId = session('user.userid');
		$postId = I('post.id');
		
		$forum = D('Forum');
		$users = D('Users');
		
		$post = $forum->getPostById($postId);
		if($post['authorid'] != $userId){
			$this->error('不允许编辑他人主题！');
		}
		
		// 敏感词提示
		$message = I('post.message');
		$strpos = $this->_strposWords($message);
		if($strpos){
			$this->error('存在敏感词！');
		}

		$data['lastdate'] = time();
		$data['message']  = $message;
		$data['quote']    = I('post.quote');
		$data['useip']    = ip2long(I('server.REMOTE_ADDR'));
		
		// 用户等级达到2级以上,免审核
		$user  = $users->getUserAndInfoById($userId);
		$level = CBWExpLevel($user['exp']);
		if($level >= 2){
			$data['invisible'] = '0';
		}else{
			$data['invisible'] = '-2';
		}
		
		$rt = $forum->updatePost($postId, $data);
		if($rt['status'] > 0){
			// 保存图片
			$insert = I('post.insert');
			if(!empty($insert)){
				$image = CBWBase64Upload($insert, './Public/bbs/attachment/');
				$forum->updatePost($postId, array('attachment'=>$image));
				
				if(!empty($post['attachment'])){
					@unlink('./Public/bbs/attachment/'.$post['attachment']);
				}
			}
			
			$this->success('编辑成功', C('BBS_URL').'/view/'.$post['tid'].'.html');exit;
		}
		$this->error('编辑失败', C('BBS_URL').'/thread/editpost/id/'.$post['id'].'.html');exit;
	}
	
	// 评论列表
	public function comments(){
		$acceptId = I('get.id');
		$category = I('get.category');
		$category = empty($category)?0:$category;
		
		$forum = D('Forum');
		
		$threadId = $acceptId;
		if($category){
			$post = $forum->getPostById($acceptId);
			$threadId = $post['tid'];
		}
		
		// 评论
		$cnumber = $forum->getCommentCountByObject(array('category'=>$category, 'acceptId'=>$acceptId));
		$data    = $forum->getCommentsByObject(array('category'=>$category, 'acceptId'=>$acceptId, 'm'=>0, 'n'=>9));

		$this->assign('data', $data);
		$this->assign('cnumber', $cnumber);
		$this->assign('threadId', $threadId);
		$this->assign('category', $category);
		$this->assign('acceptId', $acceptId);
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display();
	}
	
	// 评论列表
	public function loadMoreComments(){
		$acceptId = I('post.id');
		$category = I('post.category');
		$category = empty($category)?0:$category;
		$counter  = I('post.counter');
		$counter  = intval($counter?$counter:0);
		$num      = I('post.num');
		
		$forum = D('Forum');
		
		$data = $forum->getCommentsByObject(array('category'=>$category, 'acceptId'=>$acceptId, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
	}
	
	// 添加评价
	public function addcomment(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		$acceptId = I('get.id');
		$category = I('get.category');
		$category = empty($category)?0:$category;
		
		$forum = D('Forum');
		
		$threadId = $acceptId;
		if($category){
			$post = $forum->getPostById($acceptId);
			$threadId = $post['tid'];
		}

		$this->assign('threadId', $threadId);
		$this->assign('category', $category);
		$this->assign('acceptId', $acceptId);
		
		$this->assign('title', '手机维修论坛_手机修理资料_手机维修技术学习_智能手机维修问答');
		$this->assign('keywords', '手机维修,手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术');
		$this->assign('description', '草包网手机维修论坛为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修拆机过程中的各种问题和经验都可在草包论坛提问或发布，我们有相应的实质奖励。');
		
		$this->display();
	}
	
	// 添加评论
	public function doAddComment(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			echo 'error';exit;
		}
		
		$userId = session('user.userid');
		
		$threadId = I('post.tid');
		$category = I('post.category');
		$acceptId = I('post.acceptid');
		
		// 敏感词提示
		$content = I('post.content');
		$strpos = $this->_strposWords($content);
		if($strpos){
			echo 'badwords';exit;
		} 
		
		$forum = D('Forum');
		
		// 是否操作频繁
		$comment = $forum->getLastCommentByUserId($userId);
		$value = time()-$comment['dateline'];
		if($value < 30){
			echo 'freely';exit;
		}
		
		$data = array();
		$data['category']  = $category;
		$data['acceptid']  = $acceptId;
		$data['authorid']  = $userId;
		$data['dateline']  = time();
		$data['content']   = $content;

		$rt = $forum->insertComment($data);
		if($rt['status'] > 0){
			// 经验增加  评论+5 虚拟币+5
			$this->_actactivity($userId,$threadId,3,1,5,5);
			
			// 通知
			if($category == '0'){
				$datas = $forum->getThreadById($acceptId);
			}else{
				$datas = $forum->getPostById($acceptId);
			}
			if($userId != $datas['authorid']){
				$this->_notice($userId,$datas['authorid'],$acceptId,2,$category);
			}
			echo $rt['status'];exit;
		}
		echo 'error';exit;
	}
	
	// 加载更多评论
	public function loadComments(){
		
		$category = I('post.category');
		$acceptId = I('post.acceptid');
		$length   = I('post.length');
		
		$forum = D('Forum');
		
		$data = $forum->getCommentsByObject(array('category'=>$category, 'acceptId'=>$acceptId, 'm'=>$length, 'n'=>9));
		foreach($data as &$vo){
			$vo['dateline'] = CBWTimeFormat($vo['dateline'], 'm.d');
		}
		unset($vo);
		echo json_encode($data);
		exit;
	}
	
	// 用户投票
	public function userVote(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			echo 'illegal';exit; // 非法请求
		}
		
		$userId = session('user.userid');
		
		$votes    = 0;
		$threadId = I('post.tid');
		$type     = I('post.type');
		$category = I('post.category');
		$voteId   = I('post.voteid');
		
		$forum = D('Forum');
		
		$vote = $forum->getVoteByUserId($userId, $voteId, $category);
		if($vote>0){
			echo 'repeat';exit; // 请勿重复投票
		}
		
		if($type == 'yes'){
			$votevalue = '1';
		}else{
			$votevalue = '-1';
		}
		
		if($category == '0'){
			$thread = $forum->getThreadById($voteId);
			$receiveId = $thread['authorid'];
		}else{
			$post = $forum->getPostById($voteId);
			$receiveId = $post['authorid'];
		}
		if($userId == $receiveId){
			echo 'authorvotes';exit; // 不能给自己投票
		}
		
		$data = array(
			'category'  => $category,
			'voteid'    => $voteId,
			'authorid'  => $userId,
			'votevalue' => $votevalue,
			'dateline'  => time()
		);

		$rt = $forum->insertVote($data);
		if($rt['status'] > 0){
			// 更新投票
			$votes = $forum->updateVote($voteId, $category, $type);

			// 经验增加  投票+5
			$this->_actactivity($userId,$threadId,5,1,5);

			// 通知
			$this->_notice($userId,$receiveId,$voteId,5,$category);
			
			echo $votes;exit;
		}
		echo $votes;exit;	
	}
	
	// 撤销投票
	public function revokeVote(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			echo 'illegal';exit; // 非法请求
		}
		
		$userId = session('user.userid');
		
		$forum = D('Forum');
		$users = D('Users');

		$user = $users->getUserAndInfoById($userId);   //获取用户信息
		if($user['exp'] < 100){
			echo 'exp';exit; // 经验不足100
		}
		
		$votes    = 0;
		$threadId = I('post.tid');
		$category = I('post.category');
		$voteId   = I('post.voteid');

		$vote = $forum->getVoteByUserId($userId, $voteId, $category);
		
		$rt = $forum->deleteVote($vote['id']);
		if($rt['status'] > 0){
			// 更新投票
			$type = '';
			if($vote['votevalue'] < 0){
				$type = 'yes';
			}
			$votes = $forum->updateVote($voteId, $category, $type);

			// 经验增加  撤销投票-100 + -5
			$this->_actactivity($userId,$threadId,'-5',0,105);
			
			// 通知
			if($category == '0'){
				$thread = $forum->getThreadById($voteId);
				$receiveId = $thread['authorid'];
			}else{
				$post = $forum->getPostById($voteId);
				$receiveId = $post['authorid'];
			}
			$this->_notice($userId,$receiveId,$voteId,6,$category);
			
			echo $votes;exit;
		}
		echo $votes;exit;	
	}
	
	// 选择/取消最有帮助的答案
	public function helpfulAnswer(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			echo 'illegal';exit; // 非法请求
		}
		
		$userId = session('user.userid');
		
		$forum = D('Forum');
		
		$type   = I('post.type');
		$postId = I('post.pid');
		
		if($type == 'select'){
			$data['satisfy'] = '1';
		}else{
			$data['satisfy'] = '0';
		}
		
		$rt = $forum->updatePost($postId, $data);
		if($rt['status'] > 0){
			
			// 采纳经验增加
			$post = $forum->getPostById($postId);
			if($type == 'select'){
				$this->_actactivity($userId,$post['tid'],4,1,10);                    //采纳   经验+10
				$this->_actactivity($post['authorid'],$post['tid'],4,1,40,40);       //被采纳 经验+40 虚拟币+40
			
				// 通知
				$this->_notice($userId,$post['authorid'],$post['id'],3,1);
			}else{
				$this->_actactivity($userId,$post['tid'],'-4',0,10);                    //取消采纳   经验-10
				$this->_actactivity($post['authorid'],$post['tid'],'-4',0,40,40);       //取消被采纳 经验-40 虚拟币-40
			
				// 通知
				$this->_notice($userId,$post['authorid'],$post['id'],4,1);
			}
			
			echo '1';exit;
		}
		echo '0';exit;
	}
	
	// 收藏主题
	public function collect(){
		$this->isUserLogin();
		$this->checkUserForumStatus();
		if(!IS_POST) {
			echo 'illegal';exit; // 非法请求
		}
		
		$userId = session('user.userid');
		
		$threadId = I('post.tid');
		
		$forum = D('Forum');
		
		$collect = $forum->getCollectByUserId($userId, $threadId);
		if($collect){
			$rt = $forum->deleteCollect($userId, $threadId);
			if($rt['status'] > 0){
				$forum->updateThreadStatus($threadId, 'favtimes', 0);
				echo 'cancel';exit;  // 取消收藏
			}
		}else{
			$rt = $forum->insertCollect(array('userid'=>$userId, 'tid'=>$threadId, 'dateline'=>time()));
			if($rt['status'] > 0){
				$forum->updateThreadStatus($threadId, 'favtimes', 1);
				echo 'collect';exit; // 收藏
			}
		}
		echo '0';exit;
	}
	
	/* 用户添加主题时,添加标签
	 * $tags 标签
	 * $olds 以前的标签,在修改时使用
	 * return void
	 */
	private function _addtags($tags, $olds = ''){
		
		$forum = D('Forum');
		
		if(!empty($olds)){
			$olds = explode(",", $olds);
			if(!empty($olds)){
				foreach($olds as $old){
					if(!empty($old)){
						$forum->updateTags($old, 0);  // 减少
					}
				}
			}
		}
		
		$tags = explode(",", $tags);
		if(!empty($tags)){
			foreach($tags as $tag){
				$tag = trim($tag);
				if(!empty($tag)){
					$t = $forum->getTagsByName($tag);
					if(!empty($t)){
						$forum->updateTags($tag, 1);  // 增加
					}else{
						$forum->insertTags(array('name'=>$tag, 'inputs'=>1));    // 添加
					}
				}
			}
		}
	}
	
	/* 用户论坛操作,经验值与草包豆变更函数
	 * $userId    用户ID
	 * $threadId  主题ID
	 * $act       动作类别(-5=取消投票;-4=取消采纳;-3=删除评论; -2=删除回复;-1=删除发布;1=发布; 2=回复;3=写评论;4=采纳;5=投票;)
	 * $category  增减    1=增  0=减
	 * $exp       经验值  发表/分享=20/40 回答=10 评论=5 采纳=10 被采纳=40 投票=5
	 * $virtual   虚拟币  回答=5 评论=5 被采纳=40 分享技巧=30 被推荐=60
	 * return void
	 */
	private function _actactivity($userId, $threadId, $act=1, $category=1, $exp=5, $virtual=0){
		
		$forum = D('Forum');
		$users = D('Users');
		
		$user = $users->getUserAndInfoById($userId);   //获取用户信息
		
		$dayExp     = $forum->getDayActivityByUserId($userId, 1, 'exp');      //今天增加的经验值
		$delExp     = $forum->getDayActivityByUserId($userId, 0, 'exp');      //今天扣除的经验值
		$dayVirtual = $forum->getDayActivityByUserId($userId, 1, 'virtual');  //今天增加的虚拟币
		$delVirtual = $forum->getDayActivityByUserId($userId, 0, 'virtual');  //今天扣除的虚拟币
		
		if($category){
			$userExp = $user['exp']+$exp;
			$sumExp = ($dayExp - $delExp)+$exp;
			$newExp = $exp;
			if($sumExp > 500){    //今天获取的经验值不能超过500
				$newExp = $exp-($sumExp-500);
				$userExp = $user['exp']+$newExp;
			}

			$sumVirtual  = ($dayVirtual - $delVirtual)+$virtual;
			$newVirtual  = $virtual;
			if($sumVirtual > 500){    //今天获取的虚拟币不能超过500
				$newVirtual  = $virtual-($sumVirtual-500);
			}
		}else{
			$userExp = $user['exp']-$exp;
			$newExp = $exp;
			
			$newVirtual  = $virtual;
			if($user_virtual < 0){    //如果用户虚拟币小于0 
				$newVirtual  = $virtual-($virtual-$user['virtual_money']);
			}
		}
	
		$data = array();
		$data['authorid'] = $userId;
		$data['tid']      = $threadId;
		$data['act']      = $act;
		$data['category'] = $category;
		$data['exp']      = $newExp;
		$data['virtual']  = $newVirtual;
		$data['dateline'] = time();
		
		$rt = $forum->insertActivity($data);
		if($rt['status'] > 0){
			$users->updateUserInfo(array('user_id'=>$userId, 'exp'=>$userExp));  // 保存用户信息表经验值
			
			//添加虚拟币记录
			if($newVirtual > 0){
				if($category){
					$users->giveVirtual($userId, $newVirtual, 7, $act);      // 增加草包豆
				}else{
					$users->decreaseVirtual($userId, $newVirtual, 7, $act);  // 扣除草包豆
				}
			}
		}
	}
	
	/* 用户论坛通知操作
	 * $userId     发起用户ID
	 * $receiveId  接收用户ID
	 * $relationId 关联主题ID
	 * $act 动作类别(1=回复;2=评论;3=采纳;4=取消采纳;5=投票;6=取消投票;7=通过审核;8=不通过审核;)
	 * $category 类别(0=主题;1=帖子;2=评论;)
	 * return void
	 */
	private function _notice($userId, $receiveId, $relationId, $act=1, $category=0){
		
		$forum = D('Forum');
		
		$data['authorid']   = $userId;
		$data['receiveid']  = $receiveId;
		$data['category']   = $category;
		$data['relationid'] = $relationId;
		$data['act']        = $act;
		$data['dateline']   = time();
		
		$rt = $forum->insertNotice($data);
		if($rt['status'] > 0){
			return $rt['status'];exit;
		}
		return 0;exit;
	}
	
	/* 短信与邮件通知
	 * $sms 参数数组
	 * return void
	 */
	private function _smsnotice($notice){
		
		$modelUsers = M('users');
		$user = $modelUsers->field('email, mobile')->where('id='.$notice['userid'])->find();
		
		// 发送短信
		/*if(!empty($user['mobile'])){
			sendSmsByAliyun($user['mobile'], array('subject'=>$notice['subject']), 'SMS_116566011'); // 提问回答
		}*/
		
		//发送邮件
		if(!empty($user['email'])){
			$code = '<p>问:'.$notice['subject'].'</p><p>答:'.$notice['message'].'</p>';
			$url = C('BBS_URL').'/view/'.$notice['tid'].'.html';
			sendEmail($user['email'], 'email_send_notice', $url, $code);
		}
	}
	
	/* 匹配字符串中是否存在敏感词
	 * $string 字符串
	 * return void
	 */
	public function _strposWords($string){
		
		if(!empty($string)){
			$modelWords = M('bbs_words');
			$words = $modelWords->select();
			foreach($words as $vo){
				if(strpos($string, $vo['words']) !== false){
					return true;
				} 
			}
		}
		return false;
	}

}