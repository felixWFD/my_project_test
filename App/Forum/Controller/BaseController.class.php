<?php
namespace Forum\Controller;
/**
 * 基础控制器
 */
use Think\Controller;
class BaseController extends Controller {
	public function __construct(){
		parent::__construct();
		
		//移动设备浏览，则切换模板
		if(CBWIsMobile()) {
			//设置默认默认主题为 Mobile
			C('DEFAULT_THEME','Mobile');
		}
		
		// 如果COOKIE['cbw_auto']存在，并且用户不在登录状态
		$cbw = cookie('cbw_auto');
        if(isset($cbw) && !session('user.userid')){
            $cbw = explode('|',CBWCheckAutoLogin($cbw));
            //$ip  = ip2long(I('server.REMOTE_ADDR'));
            //if($cbw[3] == $ip){
				// 保存登录SESSION
				session(array('name'=>'userLogin', 'prefix'=>'user'));
				session('userid',   $cbw[0]);
				session('groupid',  $cbw[1]);
				session('vip',      $cbw[2]);
				session('username', cookie('cbw_uname'));
            //}
        }
		
		// 系统配置
		$system = D('System');
		$GLOBALS['CONFIG'] = $system->loadConfigs();
		$this->assign('CONF',$GLOBALS['CONFIG']);
		
		// 网址域名
		$this->assign("CBW_URL",CBWDomain());
	}
	
	/**
	 * 空操作处理
	 */
    public function _empty(){
        header("HTTP/1.0 404 Not Found"); // 使HTTP返回404状态码 
        $this->display("Public:404");
    }
	
	/**
     * 发送短信验证码
     */
	public function sendSms(){
		$mobile     = I('post.mobile');         // 手机号码
		$imgcode    = I('post.imgcode');        // 图形验证码
		$tempid     = I('post.tempid');         // 短信模板编号
		$testid     = I('post.testid');         // 测试编号
		$ischeck    = I('post.ischeck');        // 是否检查手机号码
		$ip         = I('server.REMOTE_ADDR');  // 获取IP地址
		$code       = mt_rand(100000, 999999);  // 生成验证码
		$nowtime    = time();                   // 当前时间
		$code_count = 1;
		
		//记录测试信息
		/*if(!empty($testid)){
			$modelTest = M("test");
			$data=array('mobile'=>$mobile,'val'=>$testid);
			if($modelTest->create($data)){
				$modelTest->add();
			}
		}else{
			$modelTest = M("test");
			$data=array('mobile'=>$mobile,'val'=>$tempid.'-11');
			if($modelTest->create($data)){
				$modelTest->add();
			}
		}*/
		
		if($ischeck == 1){
			$users    = D('Users');
			$user = $users->getUserByMobile($mobile);
			if(empty($user)){
				echo json_encode(array('status'=>-1, 'msg'=>'手机号码不存在'));exit;
			}
		}
		
		if(empty($tempid)){
			echo json_encode(array('status'=>-2, 'msg'=>'短信模板为空'));exit;
		}
		
		if(!empty($imgcode)){
			// 检查图形验证码
			$verify = new \Think\Verify();
			if (!$verify->check($imgcode)){
				echo json_encode(array('status'=>-3, 'msg'=>'图形验证码错误！请重试'));exit;
			}
		}
		
		$smsip    = D('SmsIp');
		$usercode = D('UserCode');
		
		$sms = $smsip->get($ip);
		if(is_array($sms)){
			if($sms['sms_count'] >= 6){
				echo json_encode(array('status'=>-4, 'msg'=>'您当日IP累计获取验证码已达上限，请您次日再试'));exit;
			}
			$smsip->edit(array('id'=>$sms['id'], 'sms_count'=>$sms['sms_count']+1));         // 更新IP操作次数
		}else{
			$sms_count = 1;
			$smsip->insert(array('ip'=>$ip, 'add_time'=>$nowtime, 'sms_count'=>$sms_count)); // 插入IP操作次数
		}
		
		$codeinfo = $usercode->get($mobile);
		if(is_array($codeinfo)){
			if($codeinfo['code_count'] >= 6){
				echo json_encode(array('status'=>-5, 'msg'=>'当日手机号码累计获取验证码已达上限，请您次日再试'));exit;
			}
			if ($nowtime - $codeinfo['add_time'] < 90){
				echo json_encode(array('status'=>-6, 'msg'=>'操作过于频繁，请您稍后再试'));exit;
			}
			$code = $codeinfo['code'];
			$code_count = $codeinfo['code_count']+1;
		}
		
		// 发送验证码
		$result = sendSmsByAliyun($mobile, array('code'=>$code), $tempid); // 验证码
		if($result == NULL ) {
			echo json_encode(array('status'=>-7, 'msg'=>'验证失败'));exit;
		}
		if($result->Message != 'OK'){
			echo json_encode(array('status'=>-8, 'msg'=>'发送失败'));exit;
		}else{
			if($code_count == 1){
				$usercode->insert(array('account'=>$mobile, 'code'=>$code, 'add_time'=>$nowtime, 'code_count'=>$code_count)); // 插入手机发送次数
			}else{
				$usercode->edit(array('id'=>$codeinfo['id'], 'code_count'=>$code_count));                                      // 更新手机发送次数
			}
			echo json_encode(array('status'=>$code_count, 'msg'=>'发送成功'));exit;
		}
		echo json_encode(array('status'=>0, 'msg'=>'未知错误'));exit;
	}
   
    /**
	 * 验证用户账号的论坛状态
	 */
	public function checkUserForumStatus(){
		$userId = session('user.userid');
		
		$users = D('Users');
		
		$user  = $users->getUserAndInfoById($userId);
		if($user['bbs_status'] == 0){
			$this->error("账号被锁,禁止操作");
		}
	}

	/**
	 * 产生中文验证码图片
	 * @return [source]
	 */
	public function getChinaVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		$verify = new \Think\Verify($config);
		$verify->useZh = true;
        $verify->fontttf = 'msyh.ttf';
		$verify->entry();
	}
	
	/**
	 * 产生数字验证码图片
	 * 
	 */
	public function getVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		// 导入Image类库
    	$verify = new \Think\Verify($config);
    	$verify->entry();
    }
	
	/**
	 * 产生数学验证码图片
	 * 
	 */
	public function getArithVerify(){
		// 查询验证码配置信息
		$config = array(
			'imageW' => $GLOBALS['CONFIG']['code_width'],
			'imageH' => $GLOBALS['CONFIG']['code_height'],
			'length' => $GLOBALS['CONFIG']['code_strlength'],
			'fontSize' => 25,
		);
		// 导入Image类库
    	$verify = new \Think\Verify($config);
		$verify->arith = true;
		$verify->fontttf = 'msyh.ttf';
    	$verify->entry();
    }
   
    /**
	 * AJAX验证模块的码校验
	 */
	public function checkAjaxVerify(){
		
		$code = I('post.code');
		$verify = new \Think\Verify();

		// 检查验证码
		if ($verify->check($code)){
			echo true;exit;
		}
		echo false;exit;
	}
	
    /**
	 * 验证模块的码校验
	 */
	public function checkVerify(){
		
		$code = I('post.code');
		$verify = new \Think\Verify();

		// 检查验证码
		if ($verify->check($code)){
			return true;
		}
		return false;
	}
	
    /**
     * 核对单独的验证码
	 * $re = false 的时候不是ajax返回
	 * @param  boolean $re [description]
	 * @return [type]      [description]
	 */
	public function checkCodeVerify($re = true){
		$code = I('code');
		$verify = new \Think\Verify(array('reset'=>false));    
		$rs =  $verify->check($code);		
		if ($re == false) return $rs;
		else $this->ajaxReturn(array('status'=>(int)$rs));
	}
	
	/**
     * ajax程序验证,只要不是会员都返回-999
     */
    public function isUserLogin() {
    	$userId = session('user.userid');
		if (empty($userId)){
			if(IS_AJAX){
				$this->ajaxReturn(array('status'=>-999,'url'=>'Users/login'));
			}else{
				$this->redirect("Users/login");
			}
		}
	}

    /**
     * 执行城市三级联动下拉框获取操作
     */
    public function changeSelect(){
		
        $id   = I('post.id');
		$type = I('post.type');

		$area = D('Area');

        if($type == 'province'){
			$list = $area->getCityByProvinceId($id);
		}else{
			$list = $area->getAreaByCityId($id);
		}
		$str = '';
		foreach($list as $vo){
			$str .= '<option value="'.$vo['id'].'">'.$vo['addr'].'</option>';
		}
        echo $str;
		exit;
    }
	
}