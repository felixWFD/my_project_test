<?php
namespace   Forum\Controller;

class ForumController extends BaseController {
	
    public function __construct(){
        parent::__construct();
    }
	
	// 版块列表
	public function forum(){
		
		$category = I('get.category');
		$category = empty($category)?1:$category;
		
		$forum = D('Forum');
		
		$forums = $forum->getForumsByForumId();
		foreach($forums as &$vo){
			$vo['forums'] = $forum->getForumsByForumId($vo['id']);
		}
		unset($vo);
		
		$this->assign('category', $category);
		$this->assign('forums', $forums);
		
		if($category == 1){
			$title       = '手机维修问题_手机修理答疑_手机维修技术讨论_手机维修问答讨论';
			$keywords    = '手机维修论坛,智能手机维修,手机维修技术学习,手机维修技术';
			$description = '草包网手机维修论坛问答栏目为大家提供一个开放的手机维修问题回答、手机维修技术学习讨论的良好环境，关于手机使用、手机维修过程中的各种问题都可在草包论坛提问并得到答案。';
		}else{
			$title       = '手机维修技巧_手机拆机步骤教程_手机修理技术学习_手机维修经验分享';
			$keywords    = '手机维修技巧,手机维修经验,手机拆机步骤';
			$description = '最新手机维修技巧、手机拆机步骤图文详解，让你学习到更多手机修理技术，积累更多手机维修经验。';
		}

		$this->assign('title', $title);
		$this->assign('keywords', $keywords);
		$this->assign('description', $description);
		
		$this->display();
	}
	
	// 品牌版块列表
	public function brand(){
		
		$category = I('get.category');
		$brand    = I('get.brand');
		$page     = I('get.page');
		$page     = empty($page)?'1':$page;
		$sort     = I('get.sort');
		
		$forum = D('Forum');
		
		// 品牌信息
		$brands = $forum->getForumById($brand);
		$subs   = $forum->getForumsByForumId($brand);
		$fid    = array();
		$bname  = array();
		foreach($subs as &$vo){
			$fid[]   = $vo['id'];
			$bname[] = $vo['name'];
			$vo['number'] = $forum->getThreadCountByObject(array('fid'=>$vo['id'], 'category'=>$category));
		}
		unset($vo);

		// 主题列表
		$sub     = '';
		$replies = '';
		if($sort == 'use'){
			$sub = 1;
		}elseif($sort == 'repair'){
			$sub = 2;
		}elseif($sort == 'unanswer'){
			$replies = 'unanswer';
		}
		$number = $forum->getThreadCountByObject(array('fid'=>$fid, 'category'=>$category));
		$num    = $forum->getThreadCountByObject(array('fid'=>$fid, 'category'=>$category, 'replies'=>$replies, 'sort'=>$sort, 'sub'=>$sub));
		$pages  = ceil($num/15);
		$data   = $forum->getThreadByObject(array('fid'=>$fid, 'category'=>$category, 'replies'=>$replies, 'sort'=>$sort, 'sub'=>$sub, 'm'=>($page-1)*15, 'n'=>15));
		foreach($data as &$vo){
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			$vo['cname']     = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		unset($vo);

		$this->assign('category', $category);
		$this->assign('brand', $brand);
		$this->assign("page", $page);
		$this->assign("sort", $sort);
		$this->assign('brands', $brands);
		$this->assign('subs', $subs);
		$this->assign('snumber', count($subs));
		$this->assign('number', $number);
		$this->assign('data', $data);
		$this->assign("pages", $pages);
		
		if($category == 1){
			$title       = $brands['name'].'专区';
			$keywords    = $brands['name'].'问答专栏，'.$brands['name'].'讨论专题，'.$brands['name'].'问题解答专栏';
			$description = '本专栏专业讨论 '.implode('、', $bname).' 机型的常用试用问题和维修问题解答。';
		}else{
			$title       = '手机维修技巧_手机拆机步骤教程_手机修理技术学习_手机维修经验分享';
			$keywords    = '手机维修技巧,手机维修经验,手机拆机步骤';
			$description = '最新手机维修技巧、手机拆机步骤图文详解，让你学习到更多手机修理技术，积累更多手机维修经验。';
		}

		$this->assign('title', $title);
		$this->assign('keywords', $keywords);
		$this->assign('description', $description);
		
		$this->display();
	}
	
	// 加载更多品牌版块列表
	public function loadMoreBrand(){
		
		$category = I('post.category');
		$brand    = I('post.brand');
		$counter  = I('post.counter');
		$counter  = intval($counter?$counter:0);
		$num      = I('post.num');

		$forum = D('Forum');
		
		// 品牌信息
		$subs   = $forum->getForumsByForumId($brand);
		$fid    = array();
		foreach($subs as &$vo){
			$fid[] = $vo['id'];
		}
		unset($vo);

		// 主题列表
		$data = $forum->getThreadByObject(array('fid'=>$fid, 'category'=>$category, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['userid']      = $vo['authorid'];
			$vo['content']     = msubstr($vo['content'], 0, 26, 'utf-8', true);
			$vo['description'] = msubstr($vo['description'], 0, 26, 'utf-8', true);
			$vo['occurtime']   = CBWTimeFormat($vo['dateline'], 'm.d');
			$vo['cname']       = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = CBWTimeFormat($post['dateline'], 'm.d');
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = CBWTimeFormat($comment['dateline'], 'm.d');
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		unset($vo);

		echo json_encode($data);
	}
	
	// 型号版块列表
	public function model(){
		
		$category = I('get.category');
		$model    = I('get.model');
		$page     = I('get.page');
		$page     = empty($page)?'1':$page;
		$sort     = I('get.sort');
		
		$forum = D('Forum');
		
		// 型号信息
		$models = $forum->getForumById($model);
		$models['number'] = $forum->getThreadCountByObject(array('fid'=>$model, 'category'=>$category));
		
		// 最新回答
		$news = $forum->getPostByObject(array('fid'=>$model, 'm'=>0, 'n'=>12));

		// 主题列表
		$sub     = '';
		$replies = '';
		if($sort == 'use'){
			$sub = 1;
		}elseif($sort == 'repair'){
			$sub = 2;
		}elseif($sort == 'unanswer'){
			$replies = 'unanswer';
		}
		$number = $forum->getThreadCountByObject(array('fid'=>$model, 'category'=>$category));
		$num    = $forum->getThreadCountByObject(array('fid'=>$model, 'category'=>$category, 'replies'=>$replies, 'sort'=>$sort, 'sub'=>$sub));
		$pages  = ceil($num/15);
		$data   = $forum->getThreadByObject(array('fid'=>$model, 'category'=>$category, 'replies'=>$replies, 'sort'=>$sort, 'sub'=>$sub, 'm'=>($page-1)*15, 'n'=>15));
		foreach($data as &$vo){
			$vo['userid']    = $vo['authorid'];
			$vo['occurtime'] = $vo['dateline'];
			$vo['cname']     = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = $post['dateline'];
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = $comment['dateline'];
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		unset($vo);

		$this->assign('category', $category);
		$this->assign('model', $model);
		$this->assign("page", $page);
		$this->assign("sort", $sort);
		$this->assign('models', $models);
		$this->assign('news', $news);
		$this->assign('number', $number);
		$this->assign('data', $data);
		$this->assign("pages", $pages);
		
		if($category == 1){
			$title       = $models['name'].'专区';
			$keywords    = $models['name'].'问题解答专栏，'.$models['name'].'在线讨论，'.$models['name'].'在线问答，'.$models['name'].'在线疑问解答提问专栏';
			$description = $models['name'].'在线疑问解答提问专栏';
		}else{
			$title       = '手机维修技巧_手机拆机步骤教程_手机修理技术学习_手机维修经验分享';
			$keywords    = '手机维修技巧,手机维修经验,手机拆机步骤';
			$description = '最新手机维修技巧、手机拆机步骤图文详解，让你学习到更多手机修理技术，积累更多手机维修经验。';
		}

		$this->assign('title', $title);
		$this->assign('keywords', $keywords);
		$this->assign('description', $description);
		
		$this->display();
	}
	
	// 加载更多型号版块列表
	public function loadMoreModel(){
		
		$category = I('post.category');
		$model    = I('post.model');
		$counter  = I('post.counter');
		$counter  = intval($counter?$counter:0);
		$num      = I('post.num');
		
		$forum = D('Forum');
		
		// 主题列表
		$data = $forum->getThreadByObject(array('fid'=>$model, 'category'=>$category, 'm'=>$counter*$num, 'n'=>$num));
		foreach($data as &$vo){
			$vo['userid']      = $vo['authorid'];
			$vo['content']     = msubstr($vo['content'], 0, 26, 'utf-8', true);
			$vo['description'] = msubstr($vo['description'], 0, 26, 'utf-8', true);
			$vo['occurtime']   = CBWTimeFormat($vo['dateline'], 'm.d');
			$vo['cname']       = '';
			
			if($vo['replies'] > 0){
				$post = $forum->getLastPostByThreadId($vo['id']);          // 最后回答
				$vo['userid']    = $post['authorid'];
				$vo['occurtime'] = CBWTimeFormat($post['dateline'], 'm.d');
				$vo['uname']     = $post['uname'];
				$vo['thumb']     = $post['thumb'];
				$vo['cname']     = 'answer';
			}
			if($vo['comments'] > 0){
				$comment = $forum->getLastCommentByThreadId($vo['id']);     // 最后评论
				if($comment['dateline'] > $post['dateline']){
					$vo['userid']    = $comment['authorid'];
					$vo['occurtime'] = CBWTimeFormat($comment['dateline'], 'm.d');
					$vo['uname']     = $comment['uname'];
					$vo['thumb']     = $comment['thumb'];
				}
			}
			$satisfy = $forum->getSatisfyPostByThreadId($vo['id']);         // 满意答案
			if(!empty($satisfy)){
				$vo['satisfy'] = 1;
				$vo['cname']   = 'satisfy';
			}
		}
		unset($vo);

		echo json_encode($data);
	}

}