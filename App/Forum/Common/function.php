<?php
/**
 * 函数库
 */
	
	/**
	 * 经验值转换等级
	 * @param  [string] $exp     经验值
	 * @print
	 */
	function CBWExpLevel($exp, $type = 'level'){
		
		$level = array(
			1 =>  array(0,10,'新手菜鸟'),
			2 =>  array(10,200,'入门学徒'),
			3 =>  array(200,2400,'初级学员'),
			4 =>  array(2400,7200,'中级技工'),
			5 =>  array(7200,14400,'高级技师'),
			6 =>  array(14400,28800,'大师教授'),
			7 =>  array(28800,56700,'宗师主任'),
			8 =>  array(56700,115200,'神级院长'),
			9 =>  array(115200,230400,'传说天下')
		);
		foreach($level as $key=>$vo){
			if($exp>=$vo[0] && $exp<$vo[1]){
				if($type == 'level'){
					return $key;exit;
				}elseif($type == 'name'){
					return $vo[2];exit;
				}				
			} 
		}
		return 1;exit;
	}
	
?>