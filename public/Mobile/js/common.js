/* 
* @Author: Marte
* @Date:   2016-03-23 10:58:46
* @Last Modified by:   Marte
* @Last Modified time: 2016-05-31 09:51:50
*/

// 返回上一页
$("div.return").click(function(event) {
    history.back(-1);
});

// 打开筛选窗口
$(".select").click(function(event) {
    $(".filter").animate({"left":"0"}, 500);
});

// 关闭筛选窗口
$(".filter .header dt").click(function(event) {
    $(".filter").animate({"left":"100%"}, 500);
});

// 分享辅助
var jiathis_config={
    summary:"",
    shortUrl:false,
    hideMore:false
}

// 关闭弹窗
$('.posF').delegate('.off,.bg','click',function(){
	$(this).parents('.posF').slideUp(250);
});
// 有新的维修店报价
$(".posF").on("click",".box60",function(){
   if(e && e.stopPropagation){
		e.stopPropagation();
	}else{
		e.cancelBubble = true;
	}
});

$("div.share,span.share").click(function(event) {
	if(event && event.stopPropagation){
		event.stopPropagation();
	}else{
		event.cancelBubble = true;
	}
    $(".sharesocil").fadeIn().find(".fixBottom").animate({"bottom":0},200).css("display","block");
    stop();
});

$(".fcancel,.sharesocil").click(function(){
	move();
	$(".sharesocil").fadeOut().find(".fixBottom").animate({"bottom":-192},200);
});

$(".feature li").click(function(){
	move();
});

$('body').bind('touchmove', function(e) { 
	//event.preventDefault();//禁用默认滚动行为，需要自己实现滚动
	var Top = $(window).scrollTop();
	if(Top>0){
		$("header,.fixed_bug").css({"position":"fixed"});
	}else{
		$("header,.fixed_bug").css({"position":"absolute"});
	}
});

// JavaScript添加更改URL参数  
function changeParam(name,value,url){
	if(!url){
		url=window.location.href;
	}
	var newUrl="";
	var reg = new RegExp("(^|)"+ name +"=([^&]*)(|$)");
	var tmp = name + "=" + value;
	if(url.match(reg) != null){
		newUrl= url.replace(eval(reg),tmp);
	}else{
		if(url.match("[\?]")){
			newUrl= url + "&" + tmp;
		}else{
			newUrl= url + "?" + tmp;
		}
	}
	location.href=newUrl;
}
	
// js二维数组排序
function listSortBy(field){
	return function (object1, object2){
		var value1 = object1[field];
		var value2 = object2[field];
		if(value1 < value2){
			return -1;
		}else if(value1 > value2){
			return 1;
		}else{
			return 0;
		}
	}
}

// 距离格式化
function conversion(distance){
	if(distance < 1000)
		return distance+ '米';
	else if(distance > 1000)
		return (Math.round(distance/100)/10).toFixed(1)+ '公里';
}
 
// 启动滑动限制
var mo=function(e){e.preventDefault();};
function stop(){
	document.body.style.overflow='hidden';        
	document.addEventListener("touchmove",mo,false);//禁止页面滑动
}
// 取消滑动限制
function move(){
	document.body.style.overflow='';//出现滚动条
	document.removeEventListener("touchmove",mo,false);        
}
function kefu() {
    var ok = confirm('需要联系在线客服吗？');
    if (!ok) {
        return false;
    }
    window.open('http://p.qiao.baidu.com/im/index?siteid=8982367&ucid=10589340', '_blank', 'toolbar=no,scrollbars=no,menubar=no,status=no,resizable=yes,location=no');
}
/*
//替换成美洽的客服软件
//打开美恰聊天窗口
function kefu() {
    _MEIQIA('showPanel');
}
(function(m, ei, q, i, a, j, s) {
    m[i] = m[i] || function() {
        (m[i].a = m[i].a || []).push(arguments)
    };
    j = ei.createElement(q),
        s = ei.getElementsByTagName(q)[0];
    j.async = true;
    j.charset = 'UTF-8';
    j.src = '//static.meiqia.com/dist/meiqia.js';
    s.parentNode.insertBefore(j, s);
})(window, document, 'script', '_MEIQIA');
_MEIQIA('entId', 29716);
// 在这里开启手动模式（必须紧跟美洽的嵌入代码）
_MEIQIA('withoutBtn');
*/