/* 
* @Author: Marte
* @Date:   2016-09-21 15:05:04
* @Last Modified by:   Marte
* @Last Modified time: 2016-09-23 16:56:06
*/

$(document).ready(function(){
	var YDB = new YDBOBJ();
	$(document).ready(function(){
		auto_size();
		isapp();
		
		// 商品分享
		$("#shareapp").click(function () {
			YDB.Share("<{$product.pro_name}>", "<{$product.pro_subname}>", "<{:C('SITE_URL')}>/Public/Product/thumb/<{$product.list_image}>", "<{:C('SITE_URL')}>/detail/<{$product.id}>.html");
		});
	});
	
	function isapp() {
		var YundabaoUA = navigator.userAgent;    // 获取当前的useragent
		if (YundabaoUA.indexOf('CK 2.0') > -1){  // 判断当前的ua中是否包含"CK 2.0"，如果包含"CK 2.0"标识当前在CK 2.0的应用中
			$("#shareapp").show();
			$("#shareweb").hide();
		}else{
			$("#shareapp").hide();
			$("#shareweb").show();
		}
	}

	$(window).resize(function(){
		auto_size();
	});

	// 商品主图切换
	TouchSlide({ 
		slideCell:"#focus",
		titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
		mainCell:".bd .my-gallery", 
		effect:"leftLoop",
		autoPage:true //自动分页
	});                                    // 主图轮播
	initPhotoSwipeFromDOM('.my-gallery');  // 主图放大
	
	$(".con").delegate('.add_car_buy .fix_cell', 'click', function(event) {                        // 固定层加入购物车和购买
		$(".bottom .join,.bottom .buy").hide();
		$(".bottom .confirm").show();
		$(".dark").fadeTo(300,0.5);
		$(".con").css({'overflow':'hidden','height':$(window).height()-94+'px'});
		$(".clickbuy").slideDown(300);
		if($(this).hasClass("join")){
			$(".bottom .confirm").addClass("join").removeClass("buy");
		}
		if($(this).hasClass("buy")){
			$(".bottom .confirm").addClass("buy").removeClass("join");
		}
	}).delegate(".clickbuy .middle_two .color .right span","click",function(){                    // 点击商品颜色切换主图
		var id = $(this).attr('data-id');
		var W = $(window).width();
		$.post('__CONTROLLER__/loadImages', {'id':id}, function(d){
			var data = $.parseJSON(d);
			if(data){
				$(".clickbuy").find('img').attr('src', "__PUBLIC__/Product/images/goods_img/"+data[0]['images']);
				$('.my-gallery').empty(); 
				for(var a = 0;a < data.length;a++){
					var html =  '<figure style="margin:0; display:inline-block;">';
						html += '	<a href="__PUBLIC__/Product/images/source_img/'+ data[a]['images'] +'" data-size="2000x2000">';
						html += '		<img src="__PUBLIC__/Product/images/goods_img/'+ data[a]['images'] +'" style="width:'+W+'px;"/>';
						html += '	</a>';
						html += '</figure>';
					$(".my-gallery").append(html);
				}
			}
		});
	}).delegate('.add', 'click', function(event) {                                                 // + 按钮事件:增加数量
		var t=$(this).siblings('.text_box'); 
		t.val(parseInt(t.val())+1);
		if(parseInt(t.val()) > 99){ 
			t.val(99); 
		}
		update_price();
	}).delegate('.min', 'click', function(event) {                                                // - 按钮事件:减少数量
		var t=$(this).siblings('.text_box'); 
		t.val(parseInt(t.val())-1);
		if(parseInt(t.val()) < 1){ 
			t.val(1); 
		}
		update_price();
	}).delegate('input[name=num]', 'change', function(event) {                                    // 手动输入数量
		var _this=$(this); 
		if(parseInt(_this.val()) < 1){ 
			_this.val(1); 
		}
		if(parseInt(_this.val()) > 99){ 
			_this.val(99); 
		}
		update_price();
	}).delegate('.clickbuy .top .close', 'click', function(event) {                              // 关闭
		$(".dark").fadeOut(300);
		$(".con").css({'height':'auto'});
		$(".clickbuy").slideUp(300);
	}).delegate('.bottom .confirm', 'click', function(event) {                                   // 弹出层确定
		if(!$(this).hasClass("nostock")){
			check_slt(this);
		}else{
			alert("已售罄")
			return false;
		}
	}).delegate('.middle_two div.attribute span', 'click', function(event) {                     // 点击弹出层商品属性
		if(!$(this).hasClass('current')){
			$(this).addClass('current').siblings().removeClass('current')
		}else{
			$(this).toggleClass('current');
		}
		update_price();
	}).delegate(".slt_content","click",function(){                                               // 点击选择商品属性
		$(".dark").fadeTo(300,0.5);
		$(".con").css({'overflow':'hidden','height':$(window).height()-94+'px'});
		$(".clickbuy").slideDown(300);
		$(".bottom .join,.bottom .buy").show();
		$(".bottom .confirm").hide();
	});
	/*
	* 检查选择商品属性
	*/
	function check_slt(_this){
		var attrs = $('.middle_two .attribute');
		for(var a = 0;a < attrs.length;a++){
			if(!$(attrs[a]).find('.right span').hasClass("current")){
				$(".bottom").siblings(".clickbuy_tip").show().text("请选择商品属性");
				setTimeout(hide, 1000);
				return false;
			}
			if($('.right span.current').parents(".attribute").length==attrs.length){
				$(".dark").fadeOut(300);
				$(".con").css({'height':'auto'});
				$(".clickbuy").slideUp(300);
				if($(_this).hasClass("join")){
					$(".select_news .clickbuy_tip").hide().text("加入购物车成功！");
					setTimeout(show, 500);
					setTimeout(hide, 1300);
					insert_cart();
				}
				if($(_this).hasClass("buy")){
					insert_cart('buy');
				}
			}
		}
	};
	/*
	* banner图自适应
	*/
	function auto_size(){
		var autoHeight = Math.ceil($(window).width());
		if(autoHeight<320){autoHeight=320}
		if(autoHeight>640){autoHeight=640}
		$("#focus").height(autoHeight);
	};
	/*
	* 提示
	*/
	function hide(){
		$(".clickbuy_tip").hide();
	}
	function show(){
		$(".clickbuy_tip").show();
	}
	
	/*
	* 加入购物车
	*/
	function insert_cart(type){
		var id = $('.middle_two').attr('data-id');
		var attr = [];
		var param = $('.attribute');
		for(var a = 0;a < param.length;a++){
			if($(param[a]).find('.right span').length > 0){
				if($(param[a]).find('span.current').length > 0){
					attr.push($(param[a]).find('span.current').attr('data-id'));
				}else{
					attr.push($(param[a]).find('span:eq(0)').attr('data-id'));
				}
			}
		}
		var num = $('input[name="num"]').val();
		$.post('__CONTROLLER__/insertCart', {'id':id,'attr':attr,'num':num,'type':type}, function(d){
			if(type == 'buy'){
				window.location.href = "<{$CBW_URL}>/buy/collect";
			}else{
				$('.kefu_car i').html(d);
			}
		});
	}
	
	/*
	* 更新商品价格
	*/
	function update_price(){
		var id = $('.middle_two').attr('data-id');
		var attr = [];
		var param = $('.attribute');
		for(var a = 0;a < param.length;a++){
			if($(param[a]).find('.right span').length > 0){
				if($(param[a]).find('span.current').length > 0){
					attr.push($(param[a]).find('span.current').attr('data-id'));
				}else{
					attr.push($(param[a]).find('span:eq(0)').attr('data-id'));
				}
			}
		}
		var num = $('input[name="num"]').val();
		$.post('__CONTROLLER__/updatePrice', {'id':id,'attr':attr,'num':num}, function(d){
			var data = $.parseJSON(d);
			if(data){
				if(data['num'] > 0){
					$(".confirm").html('确定').removeClass("nostock");
				}else{
					$(".confirm").html('已售罄').addClass("nostock");
				}
			}
		});
	}
	
	wx.config({
		debug: false,                         // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
		appId:     '<{$package.appId}>',      // 必填，公众号的唯一标识
		timestamp: '<{$package.timestamp}>',  // 必填，生成签名的时间戳
		nonceStr:  '<{$package.nonceStr}>',   // 必填，生成签名的随机串
		signature: '<{$package.signature}>',  // 必填，签名
		jsApiList: [
			// 所有要调用的 API 都要加到这个列表中
			'checkJsApi',
			'openLocation',
			'getLocation',
			'onMenuShareTimeline',
			'onMenuShareAppMessage',
			'onMenuShareQQ',
			'onMenuShareWeibo'
		]
	});
	wx.ready(function () {
		wx.onMenuShareTimeline({
			title:  "<{$product.pro_name}>",                                           // 分享标题
			link:   "<{:C('SITE_URL')}>/detail/<{$product.id}>.html",                  // 分享链接
			imgUrl: "<{:C('SITE_URL')}>/Public/Product/thumb/<{$product.list_image}>", // 分享图标
			success: function () { 
				// 用户确认分享后执行的回调函数
			},
			cancel: function () { 
				// 用户取消分享后执行的回调函数
			}
		});

		wx.onMenuShareAppMessage({
			title:   "<{$product.pro_name}>",                                                // 分享标题
			desc:    "<{$product.pro_subname}>",                                             // 分享描述
			link:    "<{:C('SITE_URL')}>/detail/<{$product.id}>.html",                       // 分享链接
			imgUrl:  "<{:C('SITE_URL')}>/Public/Product/thumb/<{$product.list_image}>",      // 分享图标
			type:    '',                                                                     // 分享类型,music、video或link，不填默认为link
			dataUrl: '',                                                                     // 如果type是music或video，则要提供数据链接，默认为空
			success: function () { 
				// 用户确认分享后执行的回调函数
			},
			cancel: function () { 
				// 用户取消分享后执行的回调函数
			}
		});

		wx.onMenuShareQQ({
			title:  "<{$product.pro_name}>",                                             // 分享标题
			desc:   "<{$product.pro_subname}>",                                          // 分享描述
			link:   "<{:C('SITE_URL')}>/detail/<{$product.id}>.html",                    // 分享链接
			imgUrl: "<{:C('SITE_URL')}>/Public/Product/thumb/<{$product.list_image}>",   // 分享图标
			success: function () { 
			   // 用户确认分享后执行的回调函数
			},
			cancel: function () { 
			   // 用户取消分享后执行的回调函数
			}
		});
	});
	
	var jiathis_config = { 
		pic: "<{:C('SITE_URL')}>/Public/Product/thumb/<{$product.list_image}>",
		url: "<{:C('SITE_URL')}>/detail/<{$product.id}>.html",
		title: "<{$product.pro_name}>",
		summary:"<{$product.pro_subname}>"
	} 
});