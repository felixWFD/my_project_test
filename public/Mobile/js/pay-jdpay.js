/* 
* @Author: Marte
* @Date:   2016-09-21 15:05:04
* @Last Modified by:   Marte
* @Last Modified time: 2016-09-23 16:56:06
*/

$(document).ready(function(){
	$(document).on('click','.card-list li:not(:last)',function(){ 
		var cardid = $(this).attr("id");
		var mobile = $(this).attr("mobile");
		var myscr  = $(this).find("img").attr("src");
		var myalt  = $(this).find("img").attr("alt");
		var mynews = $(this).find("p").html();
		$("input[name=trade_card]").val(cardid);
		$(this).addClass('active').siblings().removeClass('active');
		$("#card_no_txt").text(mobile);
		$(".dialog-quickly-bank").fadeOut(500);
		$(".index .pay-info li .bank-icon").attr({"src":myscr,"alt":myalt});
		$(".index .pay-info li div:first p").html(mynews);
	});
	$(".arrow-box,.mask").click(function(event) {
		$(this).parents(".dialog").fadeOut(500);
	});
	$(".index .pay-info li:first").click(function(event) {
		$(".dialog-quickly-bank").fadeIn(500);
	});
	$("#rj_pay").click(function(event) {
		var trade_card = $("input[name=trade_card]").val();
		var trade_no   = $("input[name=trade_no]").val();
		$.post(_APP_+"/jdpay/sign", {'trade_card':trade_card, 'trade_no':trade_no}, function(d){
			var data = JSON.parse(d);
			if(data['code'] == '0000'){
				$("#span-red-pay").text('');
				$(".dialog-confirmPay").fadeIn(500);
				clearInterval(timer);
				timer = setInterval(FreshTimes,1000);
				return false;
			}else{
				$("#span-red-pay").text(data['desc']);
				return false;
			}
		});
	});
	$(".keyboard.num li div").click(function(event) {
		var Input = $(".dialog-confirmPay .abs-lmcustom-holder")
		Input.addClass('dn');
		if($(".custom-txt p").text() == ""){
			Input.removeClass('dn')
		}
	});
	//添加银行卡
	$("#new_car_pay,.card-list.type1 li.add").click(function(event){
		$("#addBnakCart").show().animate({"left":"0"},500);
		$(".dialog").fadeOut(500);
	});
	$("#addBnakCart i").click(function(event){
		$("#addBnakCart").animate({"left":"100%"},500,function(){
			$("#addBnakCart").hide();
		});
	});
	//确认付款
	$("#paymoney").click(function(event){
		var smscode  = $("input[name=smscode]").val();
		var Iplength = smscode.length
		if(Iplength == 6){
			var trade_card = $("input[name=trade_card]").val();
			var trade_no   = $("input[name=trade_no]").val();
			$.post(_APP_+"/jdpay/pay", {'trade_card':trade_card, 'trade_no':trade_no, 'smscode':smscode}, function(d){
				var data = JSON.parse(d);
				if(data['code'] == '0000'){
					if(data['pay_link'] != ''){
						window.location.href = data['pay_link'];                                 // 页面跳转
					}else{
						window.location.href = '<{:U("Orders/index")}>';                         // 页面跳转
					}
				}else{
					$(".dialog-confirmPay").fadeOut(300);
					$("#span-red-pay").text(data['desc']);
					return false;
				}
			});
		}else{
			
		}
	});

	var num = 0;
	var endtimes = 60;
	var timer;
	var autoFocus;
	// 重新获取验证码
	$("#re_acquisition_code").click(function(event) {
		$(this).addClass("disabled").html("<em class='j_qrCodeCountdown'>60</em>秒后重发");
		endtimes = 60;
		clearInterval(timer);
		timer=setInterval(FreshTimes,1000);
		$("#rj_pay").trigger("click");
		$(".j_modalAuthInput").val("");
		
	});
	// 模拟表单聚焦
	function autofocus(){
		$(".custom-txt span").toggleClass('v-h');
	}
	//验证码倒计时
	function FreshTimes(){
		endtimes--;
		$('.j_qrCodeCountdown').html(endtimes);
		if(endtimes<=0){
			clearInterval(timer);
			$('#re_acquisition_code').removeClass("disabled").html("重新获取");
		}
	};
	autoFocus = setInterval(autofocus,500)
});