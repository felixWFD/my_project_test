/* 
* @Author: Marte
* @Date:   2016-09-21 15:05:04
* @Last Modified by:   Marte
* @Last Modified time: 2016-09-23 16:56:06
*/

$(document).ready(function(){
	var dataCard={}; //创建dataCard对象
    $(".con").delegate('#username', 'input', function(event) {                         // 真实姓名
        var regName =/^[\u4e00-\u9fa5]{2,4}$/;
        var name = $(this).val();
        var names = $(this).attr("name");
        $(this).siblings('.rightCancel').show();
        if($.trim(name) == ""){
            $("#span-red-name").text("请输入您的真实姓名");
            $(this).siblings('.rightCancel').hide();
        }
        if(!regName.test(name)){
            $("#span-red-name").text("姓名格式不正确");
			$(this).parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
			dataCard[names] = name;
            $("#span-red-name").text("");
            $(this).parents(".ui-form-item").removeClass("false");
			autocheck();
        }
    }).delegate('#userid', 'input', function(event) {                                   // 身份证号码
        var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        var idNo = $("#userid").val();
        var names = $("#userid").attr("name");
        $(this).siblings('.rightCancel').show();
        if($.trim(idNo) == ""){
            $("#span-red-id").text("请输入您本人的身份证号码");
            $(this).siblings('.rightCancel').hide();
        }
        if(!regIdNo.test(idNo)){
            $("#span-red-id").text("身份证格式不正确");
            $(this).parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
            dataCard[names] = idNo;
            $("#span-red-id").text("");
            $(this).parents(".ui-form-item").removeClass("false");
			autocheck();
        }
    }).delegate('#card-num', 'input', function(event) {                                 // 银行卡号
        var cardNoRule =  /^(\d{16}|\d{19}|\d{20})$/;
		var _this = $(this);
        var cardNo = _this.val();
        var names = _this.attr("name");
        _this.siblings('.rightCancel').show();
        if($.trim(cardNo) == ""){
            $("#span-red-cartId").text("请输入本人银行卡号");
            _this.siblings('.rightCancel').hide();
        }
        if(!cardNoRule.test(cardNo)){
            $("#span-red-cartId").text("银行卡号不正确")
			_this.parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
			$.post(_CONTROLLER_+"/checkCard", {'card_no':cardNo}, function(d){
				if(d){
					var data = JSON.parse(d);
					if(data['card_type']=='C'){
						typebank  = '信用卡'+'/'+data['name'];
						$("#credit-info-data").addClass("false").show();
						$("#credit-info-cvv2").addClass("false").show();
					}else{
						var typebank  = '储蓄卡'+'/'+data['name'];
						$("#credit-info-data").removeClass("false").hide();
						$("#credit-info-cvv2").removeClass("false").hide();
					}
					dataCard[names]      = cardNo;
					dataCard['bankid']   = data['bank_id'];
					dataCard['cardtype'] = data['card_type'];
					$("#cardtypeh").text(typebank);
					$("#span-red-cartId").text("");
					_this.parents(".ui-form-item").removeClass("false");
					autocheck();
				}else{
					$("#span-red-cartId").text("银行卡号不正确");
					_this.parents(".ui-form-item").addClass("false");
				}
			});
        }
    }).delegate('#selectdata', 'input', function(event) {                               // 信用卡有效期
        var dataRule = /^(\d{2})-(\d{4})$/;
        var data = $(this).val();
        var names = $(this).attr("name");
        $(this).siblings('.rightCancel').show();
        if($.trim(data) == ""){
            $("#span-red-data").text("请输入日期，格式为“月-年”");
            $(this).siblings('.rightCancel').hide();
        }
        if(!dataRule.test(data)){
            $("#span-red-data").text("日期格式不正确");
            $(this).parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
            dataCard[names] = data;
            $("#span-red-data").text("");
            $(this).parents(".ui-form-item").removeClass("false");
			autocheck();
        }
    }).delegate('#cardcode', 'input', function(event) {                                 // 信用卡验证码
        var cvv2Rule =  /^\d{3}$/;
        var cvv2 = $(this).val();
        var names = $(this).attr("name");
        $(this).siblings('.rightCancel').show();
        if($.trim(cvv2) == ""){
            $("#span-red-cvv2").text("请输入卡验证码");
            $(this).siblings('.rightCancel').hide();
        }
        if(!cvv2Rule.test(cvv2)){
            $("#span-red-cvv2").text("验证码格式不正确");
            $(this).parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
            dataCard[names] = cvv2;
            $("#span-red-cvv2").text("");
            $(this).parents(".ui-form-item").removeClass("false");
			autocheck();
        }
    }).delegate('#phone-num', 'input', function(event) {                                // 手机号码
        var phoneRule =  /^(((1[3456789][0-9]{1})|(15[0-9]{1}))+\d{8})$/;
        var phone = $(this).val();
        var names = $(this).attr("name");
        $(this).siblings('.rightCancel').show();
        if($.trim(phone) == ""){
            $("#span-red-phone").text("请输入银行预留的手机号码");
            $(this).siblings('.rightCancel').hide();
        }
        if(!phoneRule.test(phone)){
            $("#span-red-phone").text("手机号码格式不正确")
            $(this).parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
            dataCard[names] = phone;
            $("#span-red-phone").text("");
            $(this).parents(".ui-form-item").removeClass("false");
			autocheck();
        }
    }).delegate('#pic-code', 'input', function(event) {                                 // 图形验证码
        var codeRule =  /^[0-9]*$/;
        var code = $(this).val();
        var names = $(this).attr("name");
        $(this).siblings('.rightCancel').show();
        if($.trim(code) == ""){
            $("#span-red-code").text("请输入验证码");
            $(this).siblings('.rightCancel').hide();
        }
        if(!codeRule.test(code)){
            $("#span-red-code").text("验证码格式不正确")
            $(this).parents(".ui-form-item").addClass("false");
			autocheck();
            return false;
        }else{
            dataCard[names] = code;
            $("#span-red-code").text("");
            $(this).parents(".ui-form-item").removeClass("false");
			autocheck();
        }
    }).delegate('.rightCancel', 'click', function(event) {                             // 清空内容
        $(this).hide().siblings(".anim-input").val("").end().parents("div.ui-form-item").addClass('false');
		autocheck();
    }).delegate('#confirmBtn', 'click', function(event) {                              // 表单提交
		var name = $(this).attr("name");
		var has  = $(this).hasClass("col-btn1Gray");
		var TXT  = $(".false:first").find("input.anim-input").attr("placeholder");
		
		if(!$(".ui-form-item").hasClass('false')){
            $("#confirmBtn").children("#span-red-all").text("");
        }else{
            $("#confirmBtn").children("#span-red-all").text(TXT);
        }

		if(has){
			if(name == 'auth'){                                                        // 实名认证第一步
				$("form#authnext").submit();
			}else if(name == 'auth-next'){                                             // 实名认证第二步
				dataCard['name'] = $('input[name=name]').val();
				dataCard['idcardno'] = $('input[name=idcardno]').val();
				$.post(_CONTROLLER_+"/submitAuth", dataCard, function(d){
					var data = JSON.parse(d);
					if(data['status'] == -3){
						$('#span-red-all').text("账号已实名认证");
					}else if(data['status'] == -2){
						$('#span-red-all').text("该银行卡已经添加为快捷卡");
					}else if(data['status'] == -1){
						$('#span-red-all').text("图形验证码错误");
					}else if(data['status'] == 0){
						$('#span-red-all').text("添加快捷卡失败");
					}else{
						window.location.href = _APP_+'/users/security.html';
					}
				});
			}else if(name == 'user-add-card' || name == 'user-edit-card'){             // users 添加/修改快捷卡
				dataCard['cardid'] = $(this).attr("data-id");
				$.post(_CONTROLLER_+"/updateCard", dataCard, function(d){
					var data = JSON.parse(d);
					if(data['status'] == -2){
						$('#span-red-all').text("该银行卡已经添加为快捷卡");
					}else if(data['status'] == -1){
						$('#span-red-all').text("图形验证码错误");
					}else if(data['status'] == 0){
						$('#span-red-all').text("添加快捷卡失败");
					}else{
						window.location.href = _APP_+'/users/pay.html';
					}
				});
			}else if(name == 'buy-add-card'){                                          // buy 添加快捷卡
				$.post(_CONTROLLER_+"/addCard", dataCard, function(d){
					var data = JSON.parse(d);
					if(data['status'] == -2){
						$('#span-red-all').text("该银行卡已经添加为快捷卡");
					}else if(data['status'] == -1){
						$('#span-red-all').text("图形验证码错误");
					}else if(data['status'] == 0){
						if(data['code'] == '2'){
							$('#span-red-all').text("银行预留身份信息有误,请您核对或联系发卡行确认");
						}else if(data['code'] == '11'){
							$('#span-red-all').text("系统未知错误,请联系在线客服");
						}
					}else{
						var html = "<span class='abs-lm'>快捷卡</span>"
					        html += "<div class='bank-box bt bb posR abs-rm'>";
						    html += "<img class='abs-lm bank-icon' alt="+data['name']+" src='/Public/Bank/"+data['picture']+"'/>";
						    html += "<p>"
							if(data['type'] == 'C'){
								html+=	'<em class="credit">信用卡</em>';
							}else{
								html+=	'<em>储蓄卡</em>';
							}
							html +=" ("+data['cardno']+")</p>";
							html +="<span class='abs-rm arrow-rightb'>&#xe60f;</span>";
							html +="</div>";
						$(".index .pay-info li:first").html(html);
						
						var	html1 ="<li class='active' id="+data['status']+">"
						    html1 += "<div class='container bb clearfix'>"
							html1 +=	" <img class='abs-lm bank-icon' alt="+data['name']+" src='/Public/Bank/"+data['picture']+"'/>";
							html1 +=	" <div class='card-info'>"
							html1 += "<p>"
							if(data['type'] == 'C'){
								html1+=	'<em class="credit">信用卡</em>';
							}else{
								html1+=	'<em>储蓄卡</em>';
							}
							html1 +="("+data['cardno']+")</p>";
							html1 +=	" <span class='red fs10 red ta-c lijian hidden'></span>"
							html1 +=" </div> "
							html1 +=	" <span class='abs-rm correct'>&#xe622;</span>"
							html1 +=" </div> "
							html1 +=" </li>"
						$(".dialog-quickly-bank .card-listBox ul").prepend(html1);
						if($(".card-list li:first").hasClass("active")){
							$(".card-list li:first").siblings().removeClass("active");
						}
						$("input[name=trade_card]").val(data['status']);
						$(".type-pay").children(".abs-rm").text(data['name']);
						$("#card_no_txt").text(data['mobile']);
						$("#dcard,#rj_pay").removeClass("dn");
						$("#new_car_pay").addClass("dn");
						$("#addBnakCart").animate({"left":"100%"},500,function(){
							$("#addBnakCart").hide();
						});
					}
				});
			}
		}
    });
    function autocheck(){                                                //检查表单是否填写完整
        if(!$(".ui-form-item").hasClass('false')){
            $("#confirmBtn").addClass('col-btn1Gray');
        }else{
            $("#confirmBtn").removeClass('col-btn1Gray');
			$("#span-red-all").text("");
        }
    }
});