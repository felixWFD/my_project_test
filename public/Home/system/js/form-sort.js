	function refreshOptions(){
		var html = '<table class="table table-bordered table-condensed"><thead><tr class="active">';
		var specs = [];
		if($('.spec_item').length <= 0){
			$("#options").html('');
			return;
		}
		$(".spec_item").each(function(i){
			var _this = $(this);

			var spec = {
				id: _this.find(".spec_id").val(),
				title: _this.find(".spec_title").val()
			};
		
			var items = [];
			_this.find(".spec_item_items").each(function(){
				var __this = $(this);
				var item = {
					id: __this.find(".spec_item_id").val(),
					title: __this.find(".spec_item_title").val()
				}
				items.push(item);
			});
			
			spec.items = items;
			specs.push(spec);
		});
		specs.sort(function(x,y){
			if (x.items.length > y.items.length){
				return 1;
			}
			if (x.items.length < y.items.length) {
				return -1;
			}
		});

		var len = specs.length;
		var newlen = 1; 
		var h = new Array(len); 
		var rowspans = new Array(len); 
		for(var i=0;i<len;i++){
			html+="<th>" + specs[i].title + "</th>";
			var itemlen = specs[i].items.length;
			if(itemlen<=0) { itemlen = 1 };
			newlen*=itemlen;

			h[i] = new Array(newlen);
			for(var j=0;j<newlen;j++){
				h[i][j] = new Array();
			}
			var l = specs[i].items.length;
			rowspans[i] = 1;
			for(j=i+1;j<len;j++){
				rowspans[i]*= specs[j].items.length;
			}
		}
		

		html += '<th><div class=""><div style="padding-bottom:10px;text-align:center;">库存</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,1)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		html += '<th><div class=""><div style="padding-bottom:10px;text-align:center;">零售价</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,2)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		//html += '<th><div class=""><div style="padding-bottom:10px;text-align:center;">店铺价</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,3)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		html +='<th><div class=""><div style="padding-bottom:10px;text-align:center;">成本价</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,4)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		//html +='<th><div class=""><div style="padding-bottom:10px;text-align:center;">批发价</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,5)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		html +='<th><div class=""><div style="padding-bottom:10px;text-align:center;">编码</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,6)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		html +='<th><div class=""><div style="padding-bottom:10px;text-align:center;">条码</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,7)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		html +='<th><div class=""><div style="padding-bottom:10px;text-align:center;">重量</div><div class="input-group"><input type="text" class="form-control input-sm option_stock_all" value=""/><span onclick="setCol(this,8)" class="input-group-addon"><a href="javascript:;" class="fa fa-angle-double-down" title="批量设置"></a></span></div></div></th>';
		
		html +='</tr></thead>';
		
		for(var m=0;m<len;m++){
			var k = 0,kid = 0,n=0;
			for(var j=0;j<newlen;j++){
				var rowspan = rowspans[m]; 
				if( j % rowspan==0){
					h[m][j]={title: specs[m].items[kid].title,html: "<td class='full' rowspan='" +rowspan + "'>"+ specs[m].items[kid].title+"</td>\r\n",id: specs[m].items[kid].id};
				}
				else{
					h[m][j]={title:specs[m].items[kid].title, html: "",id: specs[m].items[kid].id};	
				}
				n++;
				if(n==rowspan){
				kid++; if(kid>specs[m].items.length-1) { kid=0; }
				n=0;
				}
				
			}
		}
		for(var i = 0;i<$(".spec_item_items").find(".spec_item_title").length;i++){
			if($(".spec_item_items").eq(i).find(".spec_item_title").val() == ""){
				$(".spec_item_items").eq(i).find(".spec_item_title").css("border-color","#f00");
				$(".spec_item_items").eq(i).addClass("false");
			} else {
				$(".spec_item_items").eq(i).find(".spec_item_title").css("border-color","#ccc");
				$(".spec_item").eq(i).removeClass("false");
			}
		}
		for(var i = 0;i<$(".spec_item").find(".spec_title").length;i++){
			if($(".spec_item").eq(i).find(".spec_title").val() == ""){
				$(".spec_item").eq(i).find(".spec_title").css("border-color","#f00");
				$(".spec_item").eq(i).addClass("false");
			} else {
				$(".spec_item").eq(i).find(".spec_title").css("border-color","#ccc");
				$(".spec_item").eq(i).removeClass("false");
			}
		};
		if($(".spec_item_items.false").length > 0 || $(".spec_item.false").length > 0){
			return false;
		}
	 
		var hh = "";
		for(var i=0;i<newlen;i++){
			hh+="<tr>";
			var ids = [];
			var titles = [];    
			for(var j=0;j<len;j++){
				hh+=h[j][i].html; 
				ids.push( h[j][i].id);
				titles.push( h[j][i].title);
			}
			ids =ids.join('_');
			titles= titles.join('+');
		
			var val ={ id : "",title:titles, stock : "",oneprice : "",twoprice : "",threeprice : "",fourprice : "",weight:"",productsn:"",goodssn:"" };
			
			if( $(".option_id_" + ids).length>0){
				val ={
					id : $(".option_id_" + ids+":eq(0)").val(),
					title: titles,
					stock : $(".option_stock_" + ids+":eq(0)").val(),
					oneprice : $(".option_oneprice_" + ids+":eq(0)").val(),
					//twoprice : $(".option_twoprice_" + ids+":eq(0)").val(),
					threeprice : $(".option_threeprice_" + ids+":eq(0)").val(),
					//fourprice : $(".option_fourprice_" + ids +":eq(0)").val(),
					goodssn : $(".option_goodssn_" + ids +":eq(0)").val(),
					productsn : $(".option_productsn_" + ids +":eq(0)").val(),
					weight : $(".option_weight_" + ids+":eq(0)").val()
				}
			}

			hh += '<td>'
			hh += '<input data-name="option_stock_' + ids +'" type="text" class="form-control cosf1 option_stock option_stock_' + ids +'" value="' +(val.stock=='undefined'?'':val.stock )+'"/>';
			
			hh += '<input data-name="option_id_' + ids+'" type="hidden" class="form-control option_id option_id_' + ids +'" value="' +(val.id=='undefined'?'':val.id )+'"/>';
			hh += '<input data-name="option_ids" type="hidden" class="form-control option_ids option_ids_' + ids +'" value="' + ids +'"/>';
			hh += '<input data-name="option_title_' + ids +'" type="hidden" class="form-control option_title option_title_' + ids +'" value="' +(val.title=='undefined'?'':val.title )+'"/>';
			hh += '<input data-name="option_virtual_' + ids +'" type="hidden" class="form-control option_virtual"/>';
			
			hh += '</td>';
			
			hh += '<td><input data-name="option_oneprice_' + ids+'" type="text" class="form-control cosf2 option_oneprice option_oneprice_' + ids +'" value="' +(val.oneprice=='undefined'?'':val.oneprice )+'"/></td>';
			
			//hh += '<td><input data-name="option_twoprice_' +ids+'" type="text" class="form-control cosf3 option_twoprice option_twoprice_' + ids +'" " value="' +(val.twoprice=='undefined'?'':val.twoprice )+'"/></td>';
			
			hh += '<td><input data-name="option_threeprice_' + ids+'" type="text" class="form-control cosf4 option_threeprice option_threeprice_' + ids +'" " value="' +(val.threeprice=='undefined'?'':val.threeprice )+'"/></td>';
			
			//hh += '<td><input data-name="option_fourprice_' + ids+'" type="text" class="form-control cosf5 option_fourprice option_fourprice_' + ids +'" value="' +(val.fourprice=='undefined'?'':val.fourprice )+'"/></td>';
			
			hh += '<td><input data-name="option_goodssn_' +ids+'" type="text" class="form-control cosf6 option_goodssn option_goodssn_' + ids +'" " value="' +(val.goodssn=='undefined'?'':val.goodssn )+'"/></td>';
			
			hh += '<td><input data-name="option_productsn_' +ids+'" type="text" class="form-control cosf7 option_productsn option_productsn_' + ids +'" " value="' +(val.productsn=='undefined'?'':val.productsn )+'"/></td>';
			
			hh += '<td><input data-name="option_weight_' + ids +'" type="text" class="form-control cosf8 option_weight option_weight_' + ids +'" " value="' +(val.weight=='undefined'?'':val.weight )+'"/></td>';
			
			hh += "</tr>";
		}
		html+=hh;
		html+="</table>";
		$("#options").html(html);
	}
	$(document).on("blur",".spec_title",function(){
		var _this = $(this);
		if(_this.val != ""){
			_this.css("border-color","#ccc");
			_this.parents(".spec_item").removeClass("false");
		}
	}).on("blur",".spec_item_title",function(){
		var _this = $(this);
		if(_this.val != ""){
			_this.css("border-color","#ccc");
			_this.parents(".spec_item_items").removeClass("false");
		}
	});
	
	function optionArray(){

		var option_stock = new Array();
		$('.option_stock').each(function (index,item) {
			option_stock.push($(item).val());
		});

		var option_id = new Array();
		$('.option_id').each(function (index,item) {
			option_id.push($(item).val());
		});

		var option_ids = new Array();
		$('.option_ids').each(function (index,item) {
			option_ids.push($(item).val());
		});

		var option_title = new Array();
		$('.option_title').each(function (index,item) {
			option_title.push($(item).val());
		});

		var option_oneprice = new Array();
		$('.option_oneprice').each(function (index,item) {
			option_oneprice.push($(item).val());
		});

		//var option_twoprice = new Array();
		//$('.option_twoprice').each(function (index,item) {
		//	option_twoprice.push($(item).val());
		//});

		var option_threeprice = new Array();
		$('.option_threeprice').each(function (index,item) {
			option_threeprice.push($(item).val());
		});

		//var option_fourprice = new Array();
		//$('.option_fourprice').each(function (index,item) {
		//	option_fourprice.push($(item).val());
		//});

		var option_goodssn = new Array();
		$('.option_goodssn').each(function (index,item) {
			option_goodssn.push($(item).val());
		});

		var option_productsn = new Array();
		$('.option_productsn').each(function (index,item) {
			option_productsn.push($(item).val());
		});

		var option_weight = new Array();
		$('.option_weight').each(function (index,item) {
			option_weight.push($(item).val());
		});
		
		var options = {
			"id" : option_id,
			"ids" : option_ids,
			"title" : option_title,
			"stock" : option_stock,
			"oneprice" : option_oneprice,
			//"twoprice" : option_twoprice,
			"threeprice" : option_threeprice,
			//"fourprice" : option_fourprice,
			"goodssn" : option_goodssn,
			"productsn" : option_productsn,
			"weight" : option_weight
		};
        var res = JSON.stringify(options); 
		$("input[name='options']").val(res);
	}
		