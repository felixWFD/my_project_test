/* 
* @Author: Marte
* @Date:   2016-04-27 11:50:15
* @Last Modified by:   Marte
* @Last Modified time: 2016-04-27 15:09:40
*/
$(document).ready(function(){
	$(function(){
		// 返回顶部
		$(window).scroll(function(event) {
			var win_st = $(window).scrollTop();
			if(win_st>0){
				$('.return_top').show(300);
			}else{
				$('.return_top').hide(300);
			}
		});
		$('.return_top').click(function(event) {
			$('html,body').animate({scrollTop:0}, 800);
		});
		// 关闭弹出层
		$(".posF .off").click(function(){
			var _this = $(this);
			_this.parents(".posF").slideUp(300);
		});
	 })
});