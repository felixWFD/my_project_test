/* 
* @Author: Marte
* @Date:   2016-04-27 11:50:15
* @Last Modified by:   Marte
* @Last Modified time: 2016-04-27 15:09:40
*/

$(document).ready(function(){
   $(".con").delegate('.bd2 ul li.last', 'click', function(event) {
       $(".select-jump").slideDown(300)
       $(".select-dark").fadeTo(300,0.5)
   }).delegate('.jump-tit i,.select-bd p', 'click', function(event) {
       $(".select-jump").slideUp(300)
       $(".select-dark").fadeOut(300)
   }).delegate('.brand ul li', 'click', function(event) {
       $(this).addClass('current').siblings().removeClass('current')
   }).delegate('.model ul li', 'click', function(event) {
       $(this).toggleClass('current');
   }).delegate('.ifo .info-b p.info-b-1 span', 'click', function(event) {
       $(".select-color").slideDown(300)
   }).delegate('.select-color p', 'click', function(event) {
       $(".select-color").slideUp(300)
   }).delegate('.bd2 ul li,.bd3 ul li', 'click', function(event) {
       $(this).addClass('current').siblings().removeClass('current')
   }).delegate('.bd4 ul li', 'click', function(event) {
       $(this).addClass('current').siblings().removeClass('current').parents(".bd4").siblings('.bd5').slideDown(300)
   });
});