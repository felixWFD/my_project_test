/* 
* @Author: Marte
* @Date:   2016-03-04 16:41:17
* @Last Modified by:   Marte
* @Last Modified time: 2016-05-12 09:50:35
*/

$(".support-bank").mouseenter(function(event) {
	$(".support-bank-list").show();
}).mouseleave(function(event) {
	$(".support-bank-list").hide();
});
$(".none-message").mouseenter(function(event) {
	$(".none-reason").show();
}).mouseleave(function(event) {
	$(".none-reason").hide();
});
$(".section-tab-title li").click(function(event) {
	$(this).addClass('current').siblings().removeClass('current')
	var myindex = $(this).index();
	$(".suport-bank-tab:eq("+myindex+")").show().siblings('.suport-bank-tab').hide()
});
var Constants = {
	inputHolderName: "请输入姓名",
	holderNameFormatError: "姓名格式不正确",
	idcardno: "请输入您本人的身份证号",
	holderIdError: "身份证不正确",
	bankcardno: "请输入您本人名下的银行卡",
	cardNoLengthLess: "银行卡号长度位数不足",
	cardNoFormatError: "银行卡号格式不正确",
	inputCVV2: "请输入卡验证码",
	cvv2FormatError: "卡验证码格式不正确",
	selectMonth: "请完整选择您的信用卡有效期",
	month: "请选择您的信用卡有效期的月份",
	year: "请选择您的信用卡有效期的年份",
	cvv2: "请输入您的信用卡背面三位数验证码",
	mobile: "请输入该卡在银行预留的手机号码",
	nocode: "请输入图形验证码",
	flcode: "图形验证码错误",
	phoneFormatError: "手机号格式不正确 , 请重新输入",
	cardFlase: "请完整填写信息",
	cardNameAuth: "账号已实名认证",
	infoError: "银行预留身份信息有误,请您核对或联系发卡行确认",
	cardRepeat: "该银行卡已经添加为快捷卡",
	parameterError: "参数不正确",
	parameterError: "商户余额不足",
	parameterError: "appkey不存在",
	ipError: "IP被拒绝",
	bankError: "银联系统维护中",
	refuseError: "银行拒绝核验此卡",
	failError: "查询失败请重试",
	systemError: "系统未知错误,请联系在线客服"
};
var dataCard={}; //创建dataCard对象
var endtimes = 60; //定时器
var timer;
// 真实姓名验证 
$("#ui-input-holderName").focus(function(event) {
	$("#span-notify-holderName").show();
	$("#span-red-name").text("");
}).blur(function(event) {
	var regName =/^[\u4e00-\u9fa5]{2,4}$/;
	var name = $("#ui-input-holderName").val();
	var names = $("#ui-input-holderName").attr("name");
	if(name == ""){
		$("#span-red-name").text(Constants.inputHolderName);
		$("#span-notify-holderName").hide();
		return false;
	}
	if(!regName.test(name)){
		 $("#span-red-name").text(Constants.holderNameFormatError);
		 $("#span-notify-holderName").hide();
		 return false;
	}else{
		dataCard[names] = name;
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
});
// 身份证号码验证 
$("#ui-input-holderId").focus(function(event) {
	$("#span-red-IDcart").text("");
}).blur(function(event) {
	var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
	var idNo = $("#ui-input-holderId").val();
	var names = $("#ui-input-holderId").attr("name");
	if(idNo == ""){
		$("#span-red-IDcart").text(Constants.idcardno);
		return false;
	}
	if(!regIdNo.test(idNo)){
		 $("#span-red-IDcart").text(Constants.holderIdError);
		 return false;
	}else{
		dataCard[names] = idNo;
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
});
// 银行卡号验证 
$("#ui-input-cardNo").focus(function(event) {
	$("#span-red-cartNo").text("");
	$("#div-newcard").addClass('enty-number-focus');
	$("#magnifier").show();
	$("#cardspan-notify-cardNo").show();
}).blur(function(event) {
	var cardNoRule =  /^(\d{16}|\d{19}|\d{20})$/;;
	var cardNo = $("#ui-input-cardNo").val();
	var names = $("#ui-input-cardNo").attr("name");
	$("#cardspan-notify-cardNo").hide();
	$("#div-newcard").removeClass('enty-number-focus');
	$("#magnifier").hide();
	if(cardNo == ""){
		$("#span-red-cartNo").text(Constants.bankcardno);
		return false;
	}
	if(!cardNoRule.test(cardNo)){
		 $("#span-red-cartNo").text(Constants.cardNoFormatError);
		 return false;
	}else{
		dataCard[names] = cardNo;
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
}).keyup(function(event){
	var cardno = this.value;
	document.getElementById("magnifier").innerHTML = cardno.replace(/\s/g,'').replace(/(\d{4})(?=\d)/g,"$1 ");
	if(cardno.length == 0){
		$("#bank-abc,#newcard-type-span").hide();
	}else{
		$("#bank-abc,#newcard-type-span").show();
	}
	if(cardno.length >= 16 && cardno.length <= 19){
		$.post(_CONTROLLER_+"/checkCard", {'card_no':cardno}, function(d){
			if(d){
				var data = JSON.parse(d);
				if(data['card_type']=='C'){
					var type     = '信用卡';
					var cardtype = 'C';
					$("#creditDate").show();
					$("#credit").show();
					$("#creditDate,#credit").addClass("false");
				}else{
					var type     = '储蓄卡';
					var cardtype = 'D';
					$("#creditDate").hide();
					$("#credit").hide();
					$("#creditDate,#credit").removeClass("false");
				}
				dataCard['cardtype'] = cardtype;
				dataCard['bankid']   = data['bank_id'];
				$('#bank-abc').html('<img src="'+_PUBLIC_+'/Bank/'+data['picture']+'" alt="'+data['name']+'" title="'+data['name']+'" />');
				$('#newcard-type-span').html('<em id="newcard-type" class="saving">'+type+'</em>');
				$('#span-red-cartNo').text('');
			}else{
				$('#span-red-cartNo').text('银行卡号不正确');
				$("#cardspan-notify-cardNo").hide();
			}
		});
	}
});
// 手机号码验证 
$("#input_card_phone_no").focus(function(event) {
	$("#span-red-number").text("");
	$("#span-phone-notify").show();
}).blur(function(event) {
	var phoneRule =  /^(((1[3456789][0-9]{1})|(15[0-9]{1}))+\d{8})$/;
	var phone = $("#input_card_phone_no").val();
	var names = $("#input_card_phone_no").attr("name");
	$("#span-phone-notify").hide();
	if(phone == ""){
		$("#span-red-number").text(Constants.mobile);
		return false;
	}
	if(!phoneRule.test(phone)){
		 $("#span-red-number").text(Constants.phoneFormatError);
		 return false;
	}else{
		dataCard[names] = phone;
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
});
//卡验证码 
$("#ui-input-cvv2").focus(function(event) {
	$("#span-red-cvv2").text("");
	$("#cardspan-notify-cartcode").show();
}).blur(function(event) {
	var cvveRule =  /^\d{3}$/;
	var cvv2 = $("#ui-input-cvv2").val();
	var names = $("#ui-input-cvv2").attr("name");
	$("#cardspan-notify-cartcode").hide();
	if(cvv2 == ""){
		$("#span-red-cvv2").text(Constants.inputCVV2);
		return false;
	}
	if(!cvveRule.test(cvv2)){
		 $("#span-red-cvv2").text(Constants.cvv2FormatError);
		 return false;
	}else{
		dataCard[names] = cvv2;
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
});
//卡日期
$("#creditCardMonth").change(function(event) {
	var month = $(this).val();
	var names = $(this).attr("name");
	if($(this).val() == ""){
		$("#span-red-Date").text(Constants.month);
		$(this).parents("dl").addClass("false");
	}else{
		dataCard[names] = month;
	}
 });
$("#creditCardYear").change(function(event) {
	var year = $(this).val();
	var names = $(this).attr("name");
	if($(this).val() == ""){
		$("#span-red-Date").text(Constants.year);
		$(this).parents("dl").addClass("false");
	}else{
		dataCard[names] = year;
		$("#span-red-Date").text("");
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
});
//图形验证码 
$("#input-phone-code").focus(function(event) {
	$("#span-red-code").text("");
}).blur(function(event) {
	var codeRule =  /^[0-9]*$/;
	var code = $("#input-phone-code").val();
	var names = $("#input-phone-code").attr("name");
	if(code == ""){
		$("#span-red-code").text(Constants.nocode);
		return false;
	}
	if(!codeRule.test(code)){
		 $("#span-red-code").text(Constants.flcode);
		 return false;
	}else{
		dataCard[names] = code;
		$(this).parents("dl").removeClass("false").end().parents(".ui-form-line").removeClass("false");
	}
});
// 表单提交事件 
$(".ui-button").click(function(event) {
	var type = $(this).attr('id');
	if(type == 'paySubmit'){
		var submittype = $(".payWays li.clearfix .way i.selected").siblings("span").attr("class");
		if(!$(".payWays li.clearfix .way i").hasClass("selected")){
			$("#span-red-All").show().text("请选择支付方式");
			return false;
		}
		if(submittype == 'jd_paybg paybg'){
			var trade_card = $("input[name=trade_card]").val();
			var trade_no   = $("input[name=trade_no]").val();
			$.post(_APP_+"/jdpay/sign", {'trade_card':trade_card, 'trade_no':trade_no}, function(d){
				var data = JSON.parse(d);
				if(data['code'] == '0000'){
					$("#span-red-All").show().text('');
					$("#common_verificationModal").show();
					clearInterval(timer)
					timer=setInterval(FreshTimes,1000);
					$('#re_acquisition_code').addClass("disable").html("<em class='j_qrCodeCountdown'>60</em>秒后重新获取验证码");
				}else{
					$("#span-red-All").show().text(data['desc']);					
					return false;
				}
			});
		}else{
			$("form#payNow").submit();
			return false;
		}
	}else{
		var dllen  = $(".realName .false").length;
		if(dllen>0){
			$("#span-red-All").text(Constants.cardFlase);
			return false;
		}else{
			$("#span-red-All").text("");
		}
		var cardid = $("#ui-input-cardid").val();
		if(cardid > 0){
			dataCard['cardid'] = cardid;
		}
		$.post(_CONTROLLER_+"/saveCard", dataCard, function(d){
			var data = JSON.parse(d);
			if(data['status'] == -1){
				$('#span-red-All').text(Constants.flcode);
			}else if(data['status'] == -2){
				$('#span-red-All').text(Constants.cardRepeat);
			}else if(data['status'] == -3){
				$('#span-red-All').text(Constants.cardNameAuth);
			}else if(data['status'] == 1){
				if(type == 'authSubmit'){
					window.location.href = '/users/security.html';
				}else if(type == 'cardSubmit'){
					window.location.href = '/users/pay.html';
				}else if(type == 'authPaySubmit'){
					var html1 =	'<span id="bank-icbc" class="bank-logo">';
						html1 +=		'<img src="/Public/Bank/'+data['picture']+'" title="'+data['name']+'" alt="'+data['name']+'">';
						html1 +=	'</span>';
						if(data['type'] == 'C'){
							html1 +=	'<em class="ub-i-quick credit">信用卡</em>';
						}else{
							html1 +=	'<em class="ub-i-quick">储蓄卡</em>';
						}
						html1 +=	'<em>(尾号'+data['cardno']+')</em>';
						html1 +=	'<i class="selected"></i>';
					var html  = '<li data-card="4" class="ub-item  posR ">';
						html +=	 html1
						html +=	'</li>';
						$('.j_usedBankList ul').append(html);
						$('#ub-item-firstBank').html(html1);
					$(".bank-return").hide()
									 .siblings(".bank-new").show()
									 .siblings(".used-bank").show();
					$(".used-bank .ub-list li:last").addClass("selected").siblings().removeClass("selected")
					$(".ui-button-XL").removeClass("disabled").text("立即支付").attr('id', 'paySubmit')
					$("#bw-quick-cardType").slideUp(300);
					$("#span-red-All").text("").hide();
					$(".used-bank dt").removeClass("dn");
					$(".emptyCart ").addClass("dn");
					$("input[name=trade_card]").val(data['cardid']);
					$("#common_m15_openBindPhone").find("em").text(data['mobile']);
					return false;
				}
			}else if(data['status'] == 2){
				$('#span-red-All').text(Constants.infoError);
			}else if(data['status'] == 11){
				$('#span-red-All').text(Constants.parameterError);
			}else if(data['status'] == 12){
				$('#span-red-All').text(Constants.parameterError);
			}else if(data['status'] == 13){
				$('#span-red-All').text(Constants.parameterError);
			}else if(data['status'] == 14){
				$('#span-red-All').text(Constants.ipError);
			}else if(data['status'] == 20){
				$('#span-red-All').text(Constants.bankError);
			}else if(data['status'] == 21){
				$('#span-red-All').text(Constants.refuseError);
			}else if(data['status'] == 22){
				$('#span-red-All').text(Constants.failError);
			}else{
				$('#span-red-All').text(Constants.systemError);
			}	
		});	
	}
});
//验证码倒计时
function FreshTimes(){
	endtimes--;
	$('.j_qrCodeCountdown').html(endtimes);
	if(endtimes<=0){
		clearInterval(timer);
		$('#re_acquisition_code').removeClass("disable").html("重新获取验证码");
	}
};
// 重新获取验证码
$("#re_acquisition_code").click(function(event) {
	$(this).addClass("disable").html("<em class='j_qrCodeCountdown'>60</em>秒后重新获取验证码");
	endtimes = 60;
	clearInterval(timer);
	timer=setInterval(FreshTimes,1000);
	$(".ui-button-XL").trigger("click");
	$("#common_messageError").text("短信验证码已发送，请注意查收");
	$(".j_modalAuthInput").val("");
});