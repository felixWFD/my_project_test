/* 
* @Author: Marte
* @Date:   2016-03-04 16:41:17
* @Last Modified by:   Marte
* @Last Modified time: 2016-05-12 09:50:35
*/

$(document).ready(function(){
	var myZindex = 0;
	$(".thumbs li").eq(0).find("img").mouseover(); 
	
	/*
	* 选择商品属性,跳转页面
	*/
    $(".attrs li").click(function(event) {
		$(this).addClass("current").siblings().removeClass("current");
		var array = [];
		var attrs = $(".pro_attr").find(".attrs");
		for(var i = 0;i<attrs.length;i++){
			array.push($(attrs[i]).find(".current").attr('data-id'))
		}
		$.post(_CONTROLLER_+'/getId', {'pro_id':_PROID_, 'attrs':array}, function(d){
			if(d > 0){
				window.location.href = '/detail/'+d+'.html';
			}else{
				return false;
			}
		});
	});

	/*
	* 关联商品,加入购物车
	* 返回新的购物车列表
	*/
	$('.needcart').click(function(){
		var id = $(this).attr('data-id');
		$.post(_CONTROLLER_+'/needCart', {'id':id}, function(d){
			if(d > 0){
				$('.tabnav .tabbd .by,.cb_sidebar_tabs li.cart').find('span').text("("+d+")");
				$('.cb_sidebar_tabs li.cart').find('span').text(d);
				$("#tip").show().animate({width: '200px'}, 300).fadeOut(500);//成功加入购物车动画效果
			}
		});
	});
	/*
	* 加入购物车
	* 返回新的购物车列表
	*/
	$('.join').click(function(){
		var num = $('select[name="num"]').val();
		$.post(_CONTROLLER_+'/addCart', {'id':_RELID_,'num':num}, function(d){
			if(d > 0){
				$('.tabnav .tabbd .by,.cb_sidebar_tabs li.cart').find('span').text("("+d+")");
				$('.cb_sidebar_tabs li.cart').find('span').text(d);
				$("#tip").show().animate({width: '200px'}, 300).fadeOut(500);//成功加入购物车动画效果
			}
		});
	});
	/*
	* 立即购买
	*/
	$('.buyer').click(function(){
		var num = $('select[name="num"]').val();
		$.post(_CONTROLLER_+'/buyCart', {'id':_RELID_,'num':num}, function(d){
			if(d){
				window.location.href = '/Buy/collect/ids/'+d+'.html';
			}
			return false;
		});
	});
	/*
	* 主图左右分页
	*/
	var ulWidth = $(".down ul li").length * 82;
	$(".down ul").css("width",ulWidth)
	var num1 = 0;
	$(".down .rightarrow").click(function(event) {
		num1++;
		if(num1<=$(".down ul li").length-6){
			$(".down ul").animate({"left":-(num1*82)}, 150);
		}else{
			return num1 = $(".down ul li").length-6;
		}
	});
	$(".down .leftarrow").click(function(event) {
		num1--;
		if(num1>=0){
			$(".down ul").animate({"left":-(num1*82)}, 150);
		}else{
			return num1 = 0;
		}
	});
	/*
	* 点击小缩略图触发事件
	*/
	$(document).on("mouseenter",".down ul li",function(){
        $(this).addClass('current').siblings().removeClass("current");
    });
	
	$(".display img").click(function(event) {
		myZindex++;
		$(".displaybig a").eq($(this).index()).eq($(this).index()).css({'display':'none','z-index':myZindex}).show(300);
	})
	$(".displaybig a span").click(function(event) {
		$(this).parent("a").hide(500);
	})
	/*
	* 切换显示内容
	*/
	$(".snav h3").click(function(event) {
		var eva = $(".evaluate");
		var pro = $(".products");
		var ser = $(".server");
		$(this).addClass("current").siblings().removeClass("current");
		if($(".eva").hasClass("current")){
			eva.show();
			pro.hide();
			ser.hide();
		}else if($(".pro").hasClass("current")){
			pro.show();
			eva.hide();
			ser.hide();
		}else if($(".server_h3").hasClass("current")){
			ser.show();
			eva.hide();
			pro.hide();
		}
	});
});
/*
* 主图第一张加载商品图
*/
function change_img(imgsrc){
	$('.goods').html('<img src="'+_PUBLIC_+'/Product/images/goods_img/'+imgsrc+'" data-highres="'+_PUBLIC_+'/Product/images/source_img/'+imgsrc+'" onclick="$(this).mbZoomify_overlay({startLevel:0});" />');
}
/*
* 切换评价内容
*/
function page_comments(page){
	if(!page) return false;
	$.post(_CONTROLLER_+'/loadComments',{'id':_PROID_, 'page':page}, function(d){
		var data = $.parseJSON(d);
		if(data){
			$('.eva_list ul').find("li").remove(); 
			for(var i = 0;i < data.length;i++){
				var html = '';
				html +='<li class="clearfix">';
				html +='	<div class="scal_left">';
				html +='		<p>';
				if(data[i]['score'] == 1){
				html +='		<img src="'+ _PUBLIC_ +'/Home/images/2016/good.png" />';
				html +='		<span>好评</span>';
				}else if(data[i]['score'] == 2){
				html +='		<img src="'+ _PUBLIC_ +'/Home/images/2016/middle.png" />';
				html +='		<span>中评</span>';
				}else if(data[i]['score'] == 3){
				html +='		<img src="'+ _PUBLIC_ +'/Home/images/2016/low.png" />';
				html +='		<span>差评</span>';
				}
				html +='		</p>';
				html +='		<time>'+ data[i]['comment_time'] +'</time>';
				html +='	</div>';
				html +='	<div class="content_Box clearfix">';
				html +='		<div class="content_right">'+ data[i]['content'] +'</div>';
				html +='		<div class="pro_news"><span class="attr">属性：'+ data[i]['attr'] +'</span><span>数量：'+ data[i]['number'] +'</span></div>';
				
				if(data[i]['replies'] != '' && data[i]['replies'] != null){
				html +='		<div class="reply">';
				html +='			[回复]'+ data[i]['replies'];
				html +='		</div>';
				}
				
				html +='	</div>';
				html +='	<div class="user_Box clearfix">';
				html +='		<img src="'+ _PUBLIC_ +'/User/'+ data[i]['thumb'] +'" onerror="javascript:this.src=\''+_PUBLIC_+'/User/default.jpg\';" />';
				html +='		<div class="right">';
				html +='			<p class="username">'+ data[i]['uname'] +'</p>';
				
				if(data[i]['city'] != '' && data[i]['city'] != null){
					html +='		<p>'+ data[i]['city'] +'</p>';
				}
				
				html +='		</div>';
				html +='	</div>';
				html +='</li>';
				$('.eva_list ul').append(html);
			}
		}
	});
};