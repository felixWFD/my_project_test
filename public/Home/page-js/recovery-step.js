/* 
* @Author: Marte
* @Date:   2016-05-09 09:18:37
* @Last Modified by:   Marte
* @Last Modified time: 2016-05-19 14:29:54
*/
$(function(){
	var timer = null;
	mapInit();
	loadOffer();
	timer = setInterval(function(){
		var baseNum = $(".store-list .lst_num").length;
		loadOffer(baseNum);
		if(baseNum >= 4){
			clearInterval(timer);
		}
	},3000);
});

function loadOffer(len){
	var id  = $('input[name=id]').val();
	var lng = $('input[name=lng]').val();
	var lat = $('input[name=lat]').val();
	$.post(_CONTROLLER_+'/loadOffer', {'id':id, 'lng':lng, 'lat':lat, 'len':len}, function (d) {
		var data = JSON.parse(d);
		if(data && data != ''){
			var html = '';
			for(var a = 0;a < data.length;a++){
				html += '<div class="lst_num">';
				html += '	<div class="lst_top clearfix">';
				html += '      <div class="f_left">'
				if(data[a]['type_id'] == 2){
					html += '		<img src="'+_PUBLIC_+'/Mobile/images/cooperation.png" class="ordinary-store">';
				}else{
					html += '		<img src="'+_PUBLIC_+'/Mobile/images/store.png" class="ordinary-store">';
				}
				html += '		<em class="store"><a href="'+_APP_+'/repairer/'+data[a]['shop_id']+'.html">'+data[a]['shop_name']+'</a></em>';
				for(var b = 0;b < data[a]['level_image_num'];b++){
					html += '<b><img src="/Public/Home/images/level/'+data[a]['level_image_name']+'.png" width="16" title="'+data[a]['level_level']+'级" alt="'+data[a]['level_level']+'级" /></b>';
				}
				html += '		</div>';
				html += '		<b class="distance">'+data[a]['distance']+'公里</b>';
				html += '	</div>';
				html += '	<div class="lst_bd clearfix">';
				html += '		<div class="pic"><a href="'+_APP_+'/repairer/'+data[a]['shop_id']+'.html"><img src="'+_PUBLIC_+'/User/'+data[a]['logo_image']+'" alt="'+data[a]['shop_name']+'" onerror="javascript:this.src=\''+_PUBLIC_+'/Home/images/2016/picc.png\';" /></a></div>';
				html += '		<div class="art">';
				if(data[a]['type_id'] == 2){
					html += '		<span class="chain">连锁</span>';
				}
				html += '			<span class="Authentication">认证</span>';
				html += '			<p class="ar2">联系方式：<span class="atr-detial">'+data[a]['user_name']+'  '+data[a]['user_phone']+'</span></p>';
				html += '			<p class="ar2 address">店铺地址：<span class="atr-detial">'+data[a]['address']+'<span class="position_ab" data-deg="'+data[a]['lng']+','+data[a]['lat']+'"></span></span></p>';
				html += '			<p class="ar3">回收报价：<strong>￥</strong><em class="pri">'+data[a]['offer']+'</em>';
				if(data[a]['is_door'] == 1 && data[a]['warranty'] == 0){
					html += '			<span class="atr-detial">可以上门回收</span>';
				}else if(data[a]['is_door'] == 0 && data[a]['warranty'] > 0){
					html += '			<span class="atr-detial">保修'+data[a]['warranty']+'个月</span>';
				}else if(data[a]['is_door'] == 1 && data[a]['warranty'] > 0){
					html += '			<span class="atr-detial">可以上门回收，保修'+data[a]['warranty']+'个月</span>';
				}
				html += '			</p>';
				html += '		</div>';
				html += '		<div class="lst_btn"><a href="javascript:;" shop-id="'+data[a]['shop_id']+'" class="nowRepair">找他回收</a></div>';
				html += '	</div>';
				if(data[a]['remarks'] != ''){
					html += '	<div class="remarks">备注：<span class="art-detial">'+data[a]['remarks']+'</span></div>';
				}else{
					html += '	<div class="remarks">备注：<span class="art-detial">无</span></div>';
				}
				html += '</div>';
			}
			$('.store-list').append(html);
		}
	});
}

// 主体
$(".con").delegate('.position_ab', 'click', function(event) {         // 维修店地图
	var _this = $(this);
	var xy = _this.attr('data-deg').split(",");
	shopPosition(xy[0], xy[1]);
	$(".dark").fadeTo(500,0.35);
	$('.bigbox').slideDown(250);
	$(".PS").hide();
	$(".ensure").hide();
}).delegate(".nowRepair","click",function(){                         // 维修店回收
	var oid = $("input[name=id]").val();
	var sid = $(this).attr('shop-id');
	var txt = "确定选择这店回收吗";
	var option = {
		title: "维修店",
		btn: parseInt("0011",2),
		onOk: function(){
			$.post(_CONTROLLER_+'/confirmShopServer', {'oid':oid, 'sid':sid}, function(d){
				if(d){
					window.location.href = _APP_+'/orders/redetail/sn/'+d+'.html';
				}
			});
		}
	}
	window.wxc.xcConfirm(txt, "custom", option);
}).delegate("#officialRecovery","click",function(){               // 官方回收
	var oid      = $("input[name=id]").val();
	var warranty = $(this).attr('warranty');
	var txt = "确定选择邮寄官方回收吗";
	var option = {
		title: "官方回收",
		btn: parseInt("0011",2),
		onOk: function(){
			$.post(_CONTROLLER_+'/confirmOfficialServer', {'oid':oid, 'warranty':warranty}, function(d){
				if(d){
					window.location.href = _APP_+'/orders/redetail/sn/'+d+'.html';
				}
			});
		}
	}
	window.wxc.xcConfirm(txt, "custom", option);
});;




