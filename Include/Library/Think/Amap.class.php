<?php
// +----------------------------------------------------------------------
// | 高德地图API
// +----------------------------------------------------------------------

namespace Think;

class Amap{

	const REQ_GET = 1;
	const REQ_POST = 2;
	const ACCESS_KEY = 'a42aaaec0d751f19407a8ad65948c121';

	/**
	* 执行CURL请求
	* @param $url
	* @param array $params
	* @param bool $encode
	* @param int $method
	* @return mixed
	*/
	private function async($url, $params = array(), $encode = true, $method = self::REQ_GET){
		$ch = curl_init();
		if ($method == self::REQ_GET){
			$url = $url . '?' . http_build_query($params);
			$url = $encode ? $url : urldecode($url);
			curl_setopt($ch, CURLOPT_URL, $url);
		}else{
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		curl_setopt($ch, CURLOPT_REFERER, '高德地图referer');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$resp = curl_exec($ch);
		curl_close($ch);
		return $resp;
	}

	/**
	* ip定位
	* @param string $ip
	* @return array
	* @throws Exception
	*/
	public function locationByIP($ip){
		//检查是否合法IP
		if (!filter_var($ip, FILTER_VALIDATE_IP)){
			throw new Exception('ip地址不合法');
		}
		$params = array(
			'key' => self::ACCESS_KEY,
			'ip' => $ip
		);
		$api = 'http://restapi.amap.com/v3/ip';
		$resp = $this->async($api, $params);
		$data = json_decode($resp, true);
		//返回地址信息
		return array(
			'status' => $data['status'],
			'info' => $data['info'],
			'infocode' => $data['infocode'],
			'province' => $data['province'],
			'city' => $data['city'],
			'adcode' => $data['adcode'],
			'rectangle' => $data['rectangle']
		);
	}

	/**
	* 地理编码
	* @param string $address 详细地址   填写结构化地址信息:省+市+区+街道+门牌号 
	* @param string $city    城市       可选：城市中文、中文全拼、citycode、adcode 
	* @return array
	* @throws Exception
	*/
	public function geocode($address, $city){
		$params = array(
			'key' => self::ACCESS_KEY,
			'address' => $address,
			'city' => $city
		);
		$api = 'http://restapi.amap.com/v3/geocode/geo';
		$resp = $this->async($api, $params);
		$data = json_decode($resp, true);
		//返回地址信息
		return array(
			'status' => $data['status'],
			'info' => $data['info'],
			'location' => $data['geocodes'][0]['location']
		);
	}

}