<?php
namespace Think;
class Area{
	public static function city($pro){
		
		$modelProvince = M('addr_province');
		$modelCity     = M('addr_city');
		$modelArea     = M('addr_area');

		$str="";
		if(!defined('CALENDAR_INIT')){
			define('CALENDAR_INIT',1);
			$str.='<select id="province" name="province" class="span2">';
				$str.='<option value="0">选择省</option>';
				$province = $modelProvince->select();
				foreach($province as $p){
					$str.='<option value="'.$p['id'].'"';
					if($p['id'] == $pro['province']){
						$str.=' selected';
					}
					$str.='>'.$p['addr'].'</option>';
				}
			$str.='</select>';
			
			$str.='<select id="city" name="city" class="span2">';
				if($pro['province'] == '选择省'){
					$str.='<option value="0">选择市</option>';
				}else{
					$str.='<option value="0">选择市</option>';
					$city = $modelCity->where('one_id='.$pro['province'])->select();
					foreach($city as $c){
						$str.='<option value="'.$c['id'].'"';
						if($c['id'] == $pro['city']){
							$str.=' selected';
						}
						$str.='>'.$c['addr'].'</option>';
					}
				}
			$str.='</select>';
			
			$str.='<select id="area" name="area" class="span2">';
				if($pro['city'] == '选择市'){
					$str.='<option value="0">选择县/区</option>';
				}else{
					$str.='<option value="0">选择县/区</option>';
					$area = $modelArea->where('two_id='.$pro['city'])->select();
					foreach($area as $a){
						$str.='<option value="'.$a['id'].'"';
						if($a['id'] == $pro['area']){
							$str.=' selected';
						}
						$str.='>'.$a['addr'].'</option>';
					}
				}
			$str.='</select>';
			$script ='
					$("body").delegate("#province","change",function(){
						var proid = $(this).children("option:selected").val();
						if(proid){
							$.post("'.__CONTROLLER__.'/changeSelect", {"id": proid,"type": "province"}, function (d) {
								if(d){
									$("#city").empty();
									$("#area").empty();
									$("#city").append("<option></option>"+d);
								}else{
									$("#city").empty();
									$("#area").empty();
								}
							});
						}
					});
					$("body").delegate("#city","change",function(){
						var cityid = $(this).children("option:selected").val();
						if(cityid){
							$.post("'.__CONTROLLER__.'/changeSelect", {"id": cityid,"type": "city"}, function (d) {
								if(d){
									$("#area").empty();
									$("#area").append("<option></option>"+d);
								}else{
									$("#area").empty();
								}
							});
						}
					});';
		}
		return array('str'=>$str,'script'=>$script);
    }
	
	public static function city2($pro){
		$city = json_encode($pro);
	
		$str="";
		if(!defined('CALENDAR_INIT')){
			define('CALENDAR_INIT',1);
			$str.='<select id="province" name="province" class="span2">
                         <option></option>
                   </select>
                    <select id="city" name="city" class="span2">
                               <option></option>
                    </select>
                    <select id="area" name="area" class="span2">
                        <option></option>
                    </select>
					<script type="text/javascript"  src="'.__ROOT__.'/Public/Admin/js/area.js"></script>
					<script type="text/javascript">_init_area('.$city.');</script>';
		}
		return $str;
    }
}
?>
