<?php
/**
 * 微信高级接口
 */
namespace Think;

class WechatApi{
    private $appID;       // APP ID
    private $appSecret;   // KEY
    private $accessToken; // TOKEN
	private $openId;      // OPENID
    public function __CONSTRUCT($appID = '', $appSecret = '', $token = null){
		if(empty($appID) && empty($appSecret)){
			$config = C("THINK_SDK_WEIXIN");
			$appID     = $config['APPKEY'];
			$appSecret = $config['APPSECRET'];
		}
		$this->appID     = $appID;
		$this->appSecret = $appSecret;
		if(!empty($token)){
			$this->accessToken = $token; //设置获取到的TOKEN
		}else{
			$this->accessTokenGet();
		}
    }
	
    /* 创建二维码 @param - $qrcodeID传递的参数，$qrcodeType二维码类型 默认为临时二维码 @return - 返回二维码图片地址*/
    public function qrcodeCreate($qrcodeID, $qrcodeType = 0){
        if($qrcodeType == 0){
            $qrcodeType = 'QR_SCENE';    
        }else{
            $qrcodeType = 'QR_LIMIT_SCENE';
        }
        $tempJson = '{"expire_seconds": 1800, "action_name": "'.$qrcodeType.'", "action_info": {"scene": {"scene_id": '.$qrcodeID.'}}}';
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$this->accessToken['access_token'];
        $tempArr = json_decode($this->jsonPost($url, $tempJson), true);
        if(@array_key_exists('ticket', $tempArr)){
			$imgUrl = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$tempArr['ticket'];
            return $this->downloadQrcode($imgUrl, $qrcodeID, './Public/Temp/');
        }else{
            $this->logger('qrcode create falied.');
        }
    }
	
	/**
     * 扫码授权
     * return URL
     */
    public function qrconnectURL(){
		$url = 'https://open.weixin.qq.com/connect/qrconnect';
        $params = array(
			'appid'         => $this->appID,          
			'redirect_uri'  => 'https://www.caobao.com/users/callback.html',
			'response_type' => 'code',
			'scope'         => 'snsapi_login',
			'state'         => 'STATE'
        );
        return $url . '?' . http_build_query($params) . '#wechat_redirect';
    }
	
	/**
     * 授权登录
     * return URL
     */
	public function getOauth2URL(){
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize';
        $params = array(
			'appid'         => $this->appID,          
			'redirect_uri'  => 'https://m.caobao.com/users/callback.html',
			'response_type' => 'code',
			'scope'         => 'snsapi_base',
			'state'         => '123'
        );
        return $url . '?' . http_build_query($params) . '#wechat_redirect';
    }
	
    /* 用户分组查询 */
    public function groupsQuery(){
        $url = 'https://api.weixin.qq.com/cgi-bin/groups/get';
		$params = array(
            'access_token' => $this->accessToken['access_token']
        );
		$data = $this->http($url, $params, 'POST');
        $tempArr = json_decode($data, true);
        if(@array_key_exists('groups', $tempArr)){
            return $tempArr['groups']; //返回数组格式的分组信息
        }else{
            $this->logger('groups query falied.');
            $this->accessTokenGet();
            $this->groupsQuery();
        }
    }
	
    /* 用户基本信息查询 */
    public function infoQuery($openId){
		$url = 'https://api.weixin.qq.com/cgi-bin/user/info';
		$params = array(
            'access_token' => $this->accessToken['access_token'],
            'openid'       => $openId,
			'lang'         => 'zh_CN'
        );
        $data = $this->http($url, $params, 'POST');
        return json_decode($data, true);
    }
    
    /**
     * 拉取用户信息
     * @return date
     */
    public function userInfoQuery(){
		$url = 'https://api.weixin.qq.com/sns/userinfo';
        $params = array(
            'access_token' => $this->accessToken['access_token'],
            'openid'       => $this->openIdGet(),
			'lang'         => 'zh_CN'
        );
        $data = $this->http($url, $params, 'POST');
        return json_decode($data, true);
    }
	
	/* 签名算法实现 */
	public function getSignPackage() {
		$jsapiTicket = $this->ticketGet();
		// 注意 URL 一定要动态获取，不能hardcode.
		$protocol  = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$url       = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$timestamp = time();
		$nonceStr  = $this->createNonceStr();

		// 这里参数的顺序要按照 key 值 ASCII 码升序排序
		$string      = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
		$signature   = sha1($string);
		$signPackage = array(
			"appId"     => $this->appID,
			"nonceStr"  => $nonceStr,
			"timestamp" => $timestamp,
			"url"       => $url,
			"signature" => $signature,
			"rawString" => $string
		);
		return $signPackage; 
	}
    
    /* 发送模板消息 */
    public function sendTemplateMessage($data){
		$data = json_encode($data);
        $url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.$this->accessToken['access_token'];
        return json_decode($this->jsonPost($url, $data), true);
    }

    // 工具函数 //
    /* 使用curl来post一个json数据 */
    // CURLOPT_SSL_VERIFYPEER,CURLOPT_SSL_VERIFYHOST - 在做https中要用到
    // CURLOPT_RETURNTRANSFER - 不以文件流返回，带1
    private function jsonPost($url, $data){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		if (!empty($data)){
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        if (curl_errno($curl)) {
            $this->logger('curl falied. Error Info: '.curl_error($curl));
        }
        curl_close($curl);
        return $result;
    }
	
	/* 生成签名的随机串 */
	private function createNonceStr($length = 16) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$str = "";
		for($i = 0; $i < $length; $i++) {
			$str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
		}
		return $str;
	}
	
	/* 下载二维码到服务器 */
	private function downloadQrcode($url, $id, $filestring){
		if($url == ""){return false;}
		$dir = date("Ymd").'/';
		CBWFileMake($filestring.$dir);  // 检查路径文件夹是否存在,不存在则创建
		$fileName = 'qrcode_'.$id.'.jpg';
		ob_start();
		readfile($url);
		$img = ob_get_contents();
		ob_end_clean();
		$size = strlen($img);
		$fp = fopen($filestring.$dir.$fileName,"a");
		if(fwrite($fp,$img) === false){
			$this->ErrorLogger('dolwload image falied. Error Info: 无法写入图片');exit();
		}
		fclose($fp);
		return $dir.$fileName;
	}
    
    /* 从微信服务器获取access_token */
    private function accessTokenGet(){
        $url = 'https://api.weixin.qq.com/cgi-bin/token';
		$params = array(
            'grant_type' => 'client_credential',
            'appid'      => $this->appID,
            'secret'     => $this->appSecret
        );
		$data = $this->http($url, $params, 'POST');
        $data = json_decode($data, true);
        if(@array_key_exists('access_token', $data)){            
            $this->accessToken = $data;
        }else{
            $this->logger('access_token get falied.');
            exit();
        }
    }
	
	
    
    /* 从微信服务器获取access_token */
    public function accessTokenGet2($code){
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token';
		$params = array(
            'grant_type' => 'authorization_code',
            'appid'      => $this->appID,
            'secret'     => $this->appSecret,
			'code'       => $code,
        );
		$data = $this->http($url, $params, 'POST');
        $data = json_decode($data, true);
        if(@array_key_exists('access_token', $data)){            
            return $data;
        }else{
            $this->logger('access_token get falied.');
            exit();
        }
    }
    
    /* 获取当前授权应用的openid */
    public function openIdGet(){
        $data = $this->accessToken;
        if(!empty($data['openid'])){
            return $data['openid'];
        }else{
			$this->logger('openid get falied.');
            exit();
		}
    }
    
    /* 从微信服务器获取ticket */
    private function ticketGet(){
        $url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket';
		$params = array(
            'access_token' => $this->accessToken['access_token'],
            'type'         => 'jsapi'
        );
		$data = $this->http($url, $params, 'POST');
        $data = json_decode($data, true);
        if(@array_key_exists('ticket', $data)){            
            return $data['ticket'];
        }else{
            $this->logger('ticket get falied.');
            exit();
        }
    }

	/**
	 * 发送HTTP请求方法，目前只支持CURL发送请求
	 * @param  string $url    请求URL
	 * @param  array  $params 请求参数
	 * @param  string $method 请求方法GET/POST
	 * @return array  $data   响应数据
	 */
	private function http($url, $params, $method = 'GET', $header = array(), $multi = false){
		$opts = array(
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_HTTPHEADER     => $header
		);

		/* 根据请求类型设置特定参数 */
		switch(strtoupper($method)){
			case 'GET':
				$opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
				break;
			case 'POST':
				//判断是否传输文件
				$params = $multi ? $params : http_build_query($params);
				$opts[CURLOPT_URL] = $url;
				$opts[CURLOPT_POST] = 1;
				$opts[CURLOPT_POSTFIELDS] = $params;
				break;
			default:
				throw new Exception('不支持的请求方式！');
		}
		
		/* 初始化并执行curl请求 */
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		$data  = curl_exec($ch);
		$error = curl_error($ch);
		curl_close($ch);
		if($error) throw new Exception('请求发生错误：' . $error);
		return  $data;
	}
	
    /* 错误日志记录 */
    private function logger($errMsg){
        $logger = fopen('./ErrorLog.txt', 'a+');
        fwrite($logger, date('Y-m-d H:i:s')." Error Info : ".$errMsg."\r\n");
    }
}