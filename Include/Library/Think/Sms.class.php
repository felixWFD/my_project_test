<?php
/*--------------------------------
功能:		PHP HTTP接口 发送短信
修改日期:	2013-05-08
说明:		http://m.5c.com.cn/api/send/?username=用户名&password=密码&mobile=手机号&content=内容&apikey=apikey
状态:
	发送成功	success:msgid
	发送失败	error:msgid

注意，需curl支持。

返回值									    说明
success:msgid								提交成功，发送状态请见4.1
error:msgid								    提交失败
error:Missing username						用户名为空
error:Missing password						密码为空
error:Missing apikey						APIKEY为空
error:Missing recipient					    手机号码为空
error:Missing message content				短信内容为空
error:Account is blocked					帐号被禁用
error:Unrecognized encoding				    编码未能识别
error:APIKEY or password error				APIKEY 或密码错误
error:Unauthorized IP address				未授权 IP 地址
error:Account balance is insufficient		余额不足
error:Black keywords is:党中央				屏蔽词
--------------------------------*/
namespace Think;

class Sms {
	private $account  = '18923788090';                       //用户账号
	private $password = 'asdf12345';                         //密码
	private $apikey   = 'c4456f94fe87a2f87d63082c6ffe24c3';  //应用KEY
	private $content  = '';                                  //短信内容
	private $template = array(
		'public'      => '【草包网】您的验证码：{code}，8小时内有效，请尽快完成相关操作。',
		'reg1'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg2'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg3'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg4'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg5'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg6'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg7'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'reg8'         => '【草包网】您的注册验证码：{code}，8小时内有效，请尽快完成注册。',
		'back'        => '【草包网】您的验证码：{code}，8小时内有效，请尽快完成修改。',
		'bind'        => '【草包网】您的验证码：{code}，8小时内有效，请尽快完成绑定。',
		'unbind'      => '【草包网】您的验证码：{code}，8小时内有效，请尽快完成解绑。',
		'joiny1'      => '【草包网】恭喜你成功入驻草包网，请搜索微信公众号“草包网”并关注，以便及时接收订单推送信息。',
		'joiny2'      => '【草包网】2.请务必完善好店铺信息，信息完善度越高平台将给予优先首页显示权以及免费维修海报和授权工作证发放。',
		'joiny3'      => '【草包网】您未设置需要维修的机型和故障客户无法下单，请登录www.caobao.com,进入维修系统服务设置里添加维修故障。',
		'joinn'       => '【草包网】您好，您在草包网申请的维修师加盟审核失败，失败原因：{reason}。',
		'order'       => '【草包网】您有新的订单，联系人：{name}，联系电话：{tel}，请及时登录系统与顾客联系并处理，谢谢！',
		'uorder'      => '【草包网】下单成功，亲！修好了记得回来付款(www.caobao.com)，登陆会员中心我的订单可看到此订单。',
		'offer'       => '【草包网】您有新的报价通知，请及时登录(www.caobao.com)维修系统参与报价，谢谢！',
		'uoffer'      => '【草包网】您有新的维修师报价，请登录(www.caobao.com)查看订单最新消息，谢谢！',
		'rcolseorder' => '【草包网】您的订单：{order}已被取消，非协商取消可投诉该店铺，投诉电话：4008506838。',
		'ucolseorder' => '【草包网】订单:{order}已被顾客取消交易，请暂停此订单服务。',
		'pay'         => '【草包网】订单:{order}已付款成功，48小时后可随意提现。',
		'password'    => '【草包网】您好，您注册成为草包网用户，账号：{mobile}，密码：{password}，请及时修改您的密码！',
		'post'        => '【草包网】您在草包网的提问“{subject}”收到1条新回答,详情请点击:http://bbs.caobao.com/view/{id}.html',
		'send'        => '【草包网】尊敬的客户：你的订单已发货，{name}：{number}  请注意签收。',
	);
	
	function setContent($content){
		$this->content = '【草包网】'.$content;
	}
	
	function sendSMS($mobile,$tempId,$params=array()){
		if($this->content == '' || empty($this->content)){
			if(empty($tempId)){
				return 'error:Missing temp Id';       //模板名为空
			}
			$this->content = $this->template[$tempId];
			if(!empty($params)){
				foreach($params as $key=>$value){
					$pattern[] = '/{'.$key.'}/';
					$replacement[] = $value;
				}
				$this->content = preg_replace($pattern, $replacement, $this->content);
			}
		}
		$url = 'http://m.5c.com.cn/api/send/?';
		$data = array(
			'username' =>$this->account,			//用户账号
			'password' =>$this->password,			//密码
			'mobile'   =>$mobile,					//号码
			'content'  =>$this->content,		    //内容
			'apikey'   =>$this->apikey,				//apikey
			'encode'   =>1,                         //当使用UTF-8发送短信出现乱码时,请传入参数:encode=1
		);
		$result = $this->curlSMS($url,$data);	    //POST方式提交
		return $result;
	}

	function curlSMS($url,$post_fields=array()){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 3600); //60秒 
			curl_setopt($ch, CURLOPT_HEADER,1);
			curl_setopt($ch, CURLOPT_REFERER,'http://www.yourdomain.com');
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields);
			$data = curl_exec($ch);
			curl_close($ch);
			$res = explode("\r\n\r\n",$data);
			return $res[2]; 
	}

} 