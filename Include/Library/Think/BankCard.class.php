<?php
/*--------------------------------
功能:		PHP HTTP接口 银行卡实名认证（二/三/四元素）接口
修改日期:	2016-09-09
说明:		http://api.id98.cn/api/v2/bankcard?appkey=Appkey&name=姓名&idcardno=身份证号&bankcardno=银行卡号&tel=银行预留手机号

注意，需curl支持。

返回参数
{
"isok":1,
"code":1,
"data":{
"bankcardno":"6228481552887309119",
"name":"邓永望",
"idcardno":"610922197401232578",
"tel":"18622523252"}
}

isok 	是否查询成功0：查询失败 ， 1：查询成功
code 	查询结果
 1：核查一致
 2：核查不一致
11：参数不正确
12: 商户余额不足
13: appkey不存在
14: IP被拒绝
20: 银联系统维护中
--------------------------------*/
namespace Think;

class BankCard {
	private $Appkey = '0074a4e363a8e37b95644606ffc0baca';  //应用KEY
	
	public function verifyCard($params=array()){
		if(empty($params)){
			return false;
		}
		$url = 'http://api.id98.cn/api/v2/bankcard';
		$data = array(
			'appkey'     => $this->Appkey,		    // Appkey
			'name'       => $params['name'],	    // 姓名
			'idcardno'   => $params['idcardno'],    // 身份证号
			'bankcardno' => $params['bankcardno'],	// 银行卡号
			'tel'        => $params['tel']	        // 银行预留手机号
		);
		$result = $this->curlCard($url,$data);	    //POST方式提交
		$data = json_decode($result, true);
		return $data;
	}

	private function curlCard($url,$post_fields=array()){
		$ch = curl_init();
		$url = $url . '?' . http_build_query($post_fields);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, '悦园科技referer');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

} 