<?php
class WeixinSDK extends ThinkOauth
{
    /**
     * 获取扫码授权的api接口
     * @var string
     */
    protected $GetQrconnectURL = 'https://open.weixin.qq.com/connect/qrconnect';
	
	/**
     * 获取网页授权的api接口
     * @var string
     */
    protected $GetOauth2URL = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    
    /**
     * 获取access_token的api接口
     * @var string
     */
    protected $GetAccessTokenURL = 'https://api.weixin.qq.com/sns/oauth2/access_token';
    
    /**
     * 获取用户基本信息（包括UnionID机制）
     * @var string
     */
    protected $GetInfoURL = 'https://api.weixin.qq.com/cgi-bin/user/info';
	
	/**
     * 拉取用户信息(需scope为 snsapi_userinfo)
     * @var string
     */
    protected $GetUserInfoURL = 'https://api.weixin.qq.com/sns/userinfo';
	
    /**
     * API根路径
     * @var string
     */
    protected $ApiBase = 'https://api.weixin.qq.com/';
    
    public function getQrconnectURL()
    {
        $this->config();
        $params = array(
			'appid'         => $this->AppKey,          
			'redirect_uri'  => $this->Callback,
			'response_type' => 'code',
			'scope'         => 'snsapi_login',
			'state'         => 'STATE'
        );
        return $this->GetQrconnectURL . '?' . http_build_query($params) . '#wechat_redirect';
    }
	
	public function getOauth2URL()
    {
        $this->config();
        $params = array(
			'appid'         => $this->AppKey,          
			'redirect_uri'  => $this->Callback,
			'response_type' => 'code',
			'scope'         => 'snsapi_login',
			'state'         => 'STATE'
        );
        return $this->GetOauth2URL . '?' . http_build_query($params) . '#wechat_redirect';
    }
    
    /**
     * 获取access_token
     * @param string $code 上一步请求到的code
     */
    public function getAccessToken($code, $extend = null){
        $params = array(
			'appid'      => $this->AppKey,
			'secret'     => $this->AppSecret,
			'grant_type' => $this->GrantType,
			'code'       => $code,
        );
        $data = $this->http($this->GetAccessTokenURL, $params, 'POST');
        $this->Token = $this->parseToken($data, $extend);
        return $this->Token;
    }
    
    /**
     * 获取用户基本信息
     * @return date
     */
    public function getInfo(){
        $params = array(
            'access_token' => $this->Token['access_token'],
            'openid'       => $this->openid(),
			'lang'         => 'zh_CN'
        );
        $data = $this->http($this->GetInfoURL, $params, 'POST');
        return json_decode($data, true);
    }
    
    /**
     * 拉取用户信息
     * @return date
     */
    public function getUserInfo(){
        $params = array(
            'access_token' => $this->Token['access_token'],
            'openid'       => $this->openid(),
			'lang'         => 'zh_CN'
        );
        $data = $this->http($this->GetUserInfoURL, $params, 'POST');
        return json_decode($data, true);
    }
    
    /**
     * 组装接口调用参数 并调用接口
     * @param  string $api    微博API
     * @param  string $param  调用API的额外参数
     * @param  string $method HTTP请求方法 默认为GET
     * @return json
     */
    public function call($api, $param = '', $method = 'GET', $multi = false){
        /* 腾讯微博调用公共参数 */
        $params = array(
            'access_token' => $this->Token['access_token'],
            'openid'       => $this->openid(),
        );

        $vars = $this->param($params, $param);
        $data = $this->http($this->url($api), $vars, $method, array(), $multi);
        return json_decode($data, true);
    }
    
    /**
     * 解析access_token方法请求后的返回值
     */
    protected function parseToken($result, $extend)
    {
        $data = json_decode($result,true);
        //parse_str($result, $data);
        if($data['access_token'] && $data['expires_in']){
            $this->Token    = $data;
            $data['openid'] = $this->openid();
            return $data;
        } else
            throw new Exception("获取微信 ACCESS_TOKEN 出错：{$result}");
    }
    
    /**
     * 获取当前授权应用的openid
     */
    public function openid()
    {
        $data = $this->Token;
        if(!empty($data['openid']))
            return $data['openid'];
        else
            exit('没有获取到微信用户ID！');
    }
}

?>