<?php
/**
 * 
 * 极客修API类
 * @author HJP
 *
 */
class jikexiuApi
{
	private $apikey  = 'JiKeXiuApi';                                 // 应用KEY  da39a3ee5e6b4b0d3255bfef95601890afd80709
	private $keyPath = './Cert/jikexiu_rsa_private_key_pkcs8.pem';   // 商户私钥文件路径
	
	/**
	 * 
	 * 推送方法
	 * @param array $params  订单信息数组
	 */
	public function push($params=array()){
		if(!empty($params)){
			
			$orders  = D('Orders');
			$repairs = D('Repairs');
			$cat     = D('ProductCat');
			
			if($params['order_type'] == 1){
				$server = $orders->getOrderServerByOrderId($params['id']);
				if($server['relation_id'] > 0){
					$relation = $repairs->getRepairByRelationId($server['relation_id']);
					$type     = $cat->getCat($relation['type_id']);
					$brand    = $cat->getCat($relation['brand_id']);
					$params['remarks'] = $type['cat_name'].$brand['cat_name'].$relation['pro_name'].$server['name'].$relation['plan_name'].' '.'草包网报价'.$params['deail_price'].'元'.' '.$params['remarks'];
				}
			}elseif($params['order_type'] == 2){
				$recovery = $orders->getOrderRecoveryByOrderId($params['id']);
				if($recovery['reco_id'] > 0){
					$repair = $repairs->getRepairById($recovery['reco_id']);
					$attrs  = implode('|', $repairs->getAttribute($recovery['reco_attr']));
					$type   = $cat->getCat($repair['type_id']);
					$brand  = $cat->getCat($repair['brand_id']);
					$params['remarks'] = $type['cat_name'].$brand['cat_name'].$repair['pro_name'].' '.$attrs.' '.'草包网报价'.$params['deail_price'].'元'.' '.$params['remarks'];
				}
			}else{
				return;
			}
			
			//签名
			$sign = $this->rsaSign($params);
			
			$arrayData = array(
				'partner'       => '360',                                // 正式360 测试232
				'timestamp'     => time(),
				'sign'          => $sign,
				'sign_type'     => 'RSA',
				'name'          => $params['reciver_user'],              // 姓名
				'mobile'        => $params['reciver_phone'],             // 联系电话
				'addr'          => $params['pro_addr'].$params['city_addr'].$params['addr'].$params['address'],  // 地址信息
				'message'       => trim($params['remarks']),                       // 备注留言
				'thirtyOrderId' => $params['order_sn']                   // 第三方订单号
			);
			return $this->post(json_encode($arrayData));
		}
		return;
	}

	/**
	 * http://www.caobao.com/api/external.html?name=jikexiu&sn=149872731854100985&apikey=da39a3ee5e6b4b0d3255bfef95601890afd80709&money=250&type=order&status=1
	 * http://www.caobao.com/api/external.html?name=jikexiu&sn=150468176452545352&apikey=da39a3ee5e6b4b0d3255bfef95601890afd80709&money=0&type=order&status=0
	 * 接收方法
	 * @param array $params  传递过来的所有参数
	 * @return true 回调状态
	 */
	public function receive($params=array()){		
		$sn      = $params['sn'];
		$type    = $params['type'];
		$apikey1 = $params['apikey'];
		$apikey2 = sha1($this->apikey);
		$money   = $params['money'];
		$status  = $params['status'];
		$res     = array('response' => 500, 'msg' => 'Server Error');

		$orders = D('Orders');
		
		if($type == 'order'){
			if($apikey1 === $apikey2){
				$order = $orders->getOrderBySn($sn);
				if(empty($order)){
					$res['response'] = 404;
					$res['msg'] = 'Not Order';
				}else{
					if($order['order_status'] == 2){
						$res['response'] = 403;
						$res['msg'] = 'Repeat Order';
					}else{
						if($status > 0){
							$data = array(
								'pay_date'     => time(), 
								'ok_date'      => time(), 
								'deail_price'  => $money, 
								'order_status' => 2, 
								'pay_status'   => 1,
								'pay_type'     => 1
							);
							$orders->updateOrders($order['id'], $data);
							if($order['order_type'] == 1){
								$orders->updateOrderServer($order['id'], array('server_status'=>3));
							}elseif($order['order_type'] == 2){
								$orders->updateOrderRecovery($order['id'], array('reco_status'=>3));
							}
						}else{
							$data = array(
								'cancel_date'  => time(), 
								'order_status' => 0
							);
							$orders->updateOrders($order['id'], $data);
						}
						
						$res['response'] = 200;
						$res['msg'] = 'ok';
					}
				}
			}else{
				$res['response'] = 401;
				$res['msg'] = 'Unauthorized';
			}
		}else{
			$res['response'] = 400;
			$res['msg'] = 'Bad Request';
		}
		echo json_encode($res);
	}
	  
	/**
	 * RSA签名
	 * @param $data 待签名数据 
	 * return 签名结果
	 */
	private function rsaSign($data) {
		$arrayData = array(
			'partner'       => '360',                                         // 正式360 测试232
			'timestamp'     => time(),
			'name'          => urlencode($data['reciver_user']),              // 姓名
			'mobile'        => urlencode($data['reciver_phone']),             // 联系电话
			'addr'          => urlencode($data['pro_addr'].$data['city_addr'].$data['addr'].$data['address']),  // 地址信息
			'message'       => urlencode(trim($data['remarks'])),             // 备注留言
			'thirtyOrderId' => $data['order_sn']                              // 第三方订单号
		);
		$arrayData = $this->argSort($arrayData);
		$myJson    = urldecode(json_encode($arrayData));
		$priKey    = file_get_contents($this->keyPath);
		$res = openssl_get_privatekey($priKey);
		openssl_sign($myJson, $sign, $res, OPENSSL_ALGO_SHA1);
		openssl_free_key($res);
		//base64编码
		$sign = base64_encode($sign);
		return $sign;
	}
	
	/**
	 * 对数组排序
	 * @param $para 排序前的数组
	 * return 排序后的数组
	 */
	private function argSort($para) {
		ksort($para);
		reset($para);
		return $para;
	}
	
	/**
	 * 发送请求至快捷支付地址
	 * 只支持post方式
	 * 测试时，请确认本地curl环境是否可用
	 * @param 请求参数
	 */
	private function post($param) {  
		$url = 'http://open.jikexiu.com/v1/api/repair/quickSaveOrder';
		//$url = 'http://c.jikexiu.com:18083/v1/api/repair/quickSaveOrder';
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_POST, 1);  
        curl_setopt($ch, CURLOPT_URL, $url);  
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);  
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
            'Content-Type: application/json; charset=utf-8',  
            'Content-Length: ' . strlen($param))  
        );  
        ob_start();  
        curl_exec($ch);
        $return_content = ob_get_contents();  
        ob_end_clean();  
  
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
        return array($return_code, $return_content);
    }
}