﻿<?php
header ( 'Content-type:text/html;charset=utf-8' );
class UpacpayCommon{
	
	const SDK_CVN2_ENC = 0;       // cvn2加密 1：加密 0:不加密
	const SDK_DATE_ENC = 0;       // 有效期加密 1:加密 0:不加密
	const SDK_PAN_ENC = 0;        // 卡号加密 1：加密 0:不加密
	
	const SDK_SIGN_CERT_PATH = './Public/Upacpay/certs/caobaowang.pfx';    // 签名证书路径
	const SDK_SIGN_CERT_PWD = '201507';       // 签名证书密码
	const SDK_ENCRYPT_CERT_PATH = './Public/Upacpay/certs/encryptpub.cer';        // 密码加密证书（这条用不到的请随便配）
	const SDK_VERIFY_CERT_DIR = './Public/Upacpay/certs/';         // 验签证书路径（请配到文件夹，不要配到具体文件）
	const SDK_FILE_DOWN_PATH = './Public/Upacpay/file/';         //文件下载目录 
	
	public function __construct(){
		
	}
	
	/**
	 * 数组 排序后转化为字体串
	 *
	 * @param array $params        	
	 * @return string
	 */
	function coverParamsToString($params) {
		$sign_str = '';
		// 排序
		ksort ( $params );
		foreach ( $params as $key => $val ) {
			if ($key == 'signature') {
				continue;
			}
			$sign_str .= sprintf ( "%s=%s&", $key, $val );
			// $sign_str .= $key . '=' . $val . '&';
		}
		return substr ( $sign_str, 0, strlen ( $sign_str ) - 1 );
	}
	/**
	 * 字符串转换为 数组
	 *
	 * @param unknown_type $str        	
	 * @return multitype:unknown
	 */
	function coverStringToArray($str) {
		$result = array ();

		if (! empty ( $str )) {
			$temp = preg_split ( '/&/', $str );
			if (! empty ( $temp )) {
				foreach ( $temp as $key => $val ) {
					$arr = preg_split ( '/=/', $val, 2 );
					if (! empty ( $arr )) {
						$k = $arr ['0'];
						$v = $arr ['1'];
						$result [$k] = $v;
					}
				}
			}
		}
		return $result;
	}
	/**
	 * 处理返回报文 解码客户信息 , 如果编码为utf-8 则转为utf-8
	 *
	 * @param unknown_type $params        	
	 */
	function deal_params(&$params) {
		/**
		 * 解码 customerInfo
		 */
		if (! empty ( $params ['customerInfo'] )) {
			$params ['customerInfo'] = base64_decode ( $params ['customerInfo'] );
		}
		
		if (! empty ( $params ['encoding'] ) && strtoupper ( $params ['encoding'] ) == 'utf-8') {
			foreach ( $params as $key => $val ) {
				$params [$key] = iconv ( 'utf-8', 'UTF-8', $val );
			}
		}
	}

	/**
	 * 压缩文件 对应java deflate
	 *
	 * @param unknown_type $params        	
	 */
	function deflate_file(&$params) {
		//global $log;
		foreach ( $_FILES as $file ) {
			//$log->LogInfo ( "---------处理文件---------" );
			if (file_exists ( $file ['tmp_name'] )) {
				$params ['fileName'] = $file ['name'];
				
				$file_content = file_get_contents ( $file ['tmp_name'] );
				$file_content_deflate = gzcompress ( $file_content );
				
				$params ['fileContent'] = base64_encode ( $file_content_deflate );
				//$log->LogInfo ( "压缩后文件内容为>" . base64_encode ( $file_content_deflate ) );
			} else {
				//$log->LogInfo ( ">>>>文件上传失败<<<<<" );
			}
		}
	}

	/**
	 * 处理报文中的文件
	 *
	 * @param unknown_type $params        	
	 */
	function deal_file($params) {
		//global $log;
		if (isset ( $params ['fileContent'] )) {
			//$log->LogInfo ( "---------处理后台报文返回的文件---------" );
			$fileContent = $params ['fileContent'];
			
			if (empty ( $fileContent )) {
				//$log->LogInfo ( '文件内容为空' );
			} else {
				// 文件内容 解压缩
				$content = gzuncompress ( base64_decode ( $fileContent ) );
				$root = UpacpayCommon::SDK_FILE_DOWN_PATH;
				$filePath = null;
				if (empty ( $params ['fileName'] )) {
					//$log->LogInfo ( "文件名为空" );
					$filePath = $root . $params ['merId'] . '_' . $params ['batchNo'] . '_' . $params ['txnTime'] . '.txt';
				} else {
					$filePath = $root . $params ['fileName'];
				}
				$handle = fopen ( $filePath, "w+" );
				if (! is_writable ( $filePath )) {
					//$log->LogInfo ( "文件:" . $filePath . "不可写，请检查！" );
				} else {
					file_put_contents ( $filePath, $content );
					//$log->LogInfo ( "文件位置 >:" . $filePath );
				}
				fclose ( $handle );
			}
		}
	}

	/**
	 * 构造自动提交表单
	 *
	 * @param unknown_type $params        	
	 * @param unknown_type $action        	
	 * @return string
	 */
	function create_html($params, $action) {
		$encodeType = isset ( $params ['encoding'] ) ? $params ['encoding'] : 'UTF-8';

		$html = '<html>';
		$html .= '<head>';
		$html .= '<meta http-equiv="Content-Type" content="text/html; charset='.$encodeType.'" />';
		$html .= '</head>';
		$html .= '<body onload="javascript:document.pay_form.submit();">';
		$html .= '<form id="pay_form" name="pay_form" action="'.$action.'" method="post">';
		foreach ( $params as $key => $value ) {
			$html .= '    <input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" /><br />';
		}
		$html .= '<input type="submit" type="hidden">';
		$html .= '</form>';
		$html .= '</body>';
		$html .= '</html>';
		
		return $html;
	}






	/**
	 * ------------secureUtil.php---------------------------------------------------------------------
	 *
	 */
	/**
	 * 签名
	 *
	 * @param String $params_str
	 */
	function sign(&$params) {
		//global $log;
		//$log->LogInfo ( '=====签名报文开始======' );
		if(isset($params['transTempUrl'])){
			unset($params['transTempUrl']);
		}
		// 转换成key=val&串
		$params_str = $this->coverParamsToString ( $params );
		//$log->LogInfo ( "签名key=val&...串 >" . $params_str );
		
		$params_sha1x16 = sha1 ( $params_str, FALSE );
		//$log->LogInfo ( "摘要sha1x16 >" . $params_sha1x16 );
		// 签名证书路径
		$cert_path = UpacpayCommon::SDK_SIGN_CERT_PATH;
		$private_key = $this->getPrivateKey ( $cert_path );
		// 签名
		$sign_falg = openssl_sign ( $params_sha1x16, $signature, $private_key, OPENSSL_ALGO_SHA1 );
		if ($sign_falg) {
			$signature_base64 = base64_encode ( $signature );
			//$log->LogInfo ( "签名串为 >" . $signature_base64 );
			$params ['signature'] = $signature_base64;
		} else {
			//$log->LogInfo ( ">>>>>签名失败<<<<<<<" );
		}
		//$log->LogInfo ( '=====签名报文结束======' );
	}

	/**
	 * 验签
	 *
	 * @param String $params_str        	
	 * @param String $signature_str        	
	 */
	function verify($params) {
		//global $log;
		// 公钥
		$public_key = $this->getPulbicKeyByCertId ( $params ['certId'] );	
	//	echo $public_key.'<br/>';
		// 签名串
		$signature_str = $params ['signature'];
		unset ( $params ['signature'] );
		$params_str = $this->coverParamsToString ( $params );
		//$log->LogInfo ( '报文去[signature] key=val&串>' . $params_str );
		$signature = base64_decode ( $signature_str );
	//	echo date('Y-m-d',time());
		$params_sha1x16 = sha1 ( $params_str, FALSE );
		//$log->LogInfo ( '摘要shax16>' . $params_sha1x16 );	
		$isSuccess = openssl_verify ( $params_sha1x16, $signature,$public_key, OPENSSL_ALGO_SHA1 );
		//$log->LogInfo ( $isSuccess ? '验签成功' : '验签失败' );
		return $isSuccess;
	}

	/**
	 * 根据证书ID 加载 证书
	 *
	 * @param unknown_type $certId        	
	 * @return string NULL
	 */
	function getPulbicKeyByCertId($certId) {
		//global $log;
		//$log->LogInfo ( '报文返回的证书ID>' . $certId );
		// 证书目录
		$cert_dir = UpacpayCommon::SDK_VERIFY_CERT_DIR;
		//$log->LogInfo ( '验证签名证书目录 :>' . $cert_dir );
		$handle = opendir ( $cert_dir );
		if ($handle) {
			while ( $file = readdir ( $handle ) ) {
				clearstatcache ();
				$filePath = $cert_dir . '/' . $file;
				if (is_file ( $filePath )) {
					if (pathinfo ( $file, PATHINFO_EXTENSION ) == 'cer') {
						if ($this->getCertIdByCerPath ( $filePath ) == $certId) {
							closedir ( $handle );
							//$log->LogInfo ( '加载验签证书成功' );
							return $this->getPublicKey ( $filePath );
						}
					}
				}
			}
			//$log->LogInfo ( '没有找到证书ID为[' . $certId . ']的证书' );
		} else {
			//$log->LogInfo ( '证书目录 ' . $cert_dir . '不正确' );
		}
		closedir ( $handle );
		return null;
	}

	/**
	 * 取证书ID(.pfx)
	 *
	 * @return unknown
	 */
	function getCertId($cert_path) {
		$pkcs12certdata = file_get_contents ( $cert_path );

		openssl_pkcs12_read ( $pkcs12certdata, $certs, UpacpayCommon::SDK_SIGN_CERT_PWD );
		$x509data = $certs ['cert'];
		openssl_x509_read ( $x509data );
		$certdata = openssl_x509_parse ( $x509data );
		$cert_id = $certdata ['serialNumber'];
		return $cert_id;
	}

	/**
	 * 取证书ID(.cer)
	 *
	 * @param unknown_type $cert_path        	
	 */
	function getCertIdByCerPath($cert_path) {
		$x509data = file_get_contents ( $cert_path );
		openssl_x509_read ( $x509data );
		$certdata = openssl_x509_parse ( $x509data );
		$cert_id = $certdata ['serialNumber'];
		return $cert_id;
	}

	/**
	 * 签名证书ID
	 *
	 * @return unknown
	 */
	function getSignCertId() {
		// 签名证书路径
		return $this->getCertId ( UpacpayCommon::SDK_SIGN_CERT_PATH );
	}
	function getEncryptCertId() {
		// 签名证书路径
		return $this->getCertIdByCerPath ( UpacpayCommon::SDK_ENCRYPT_CERT_PATH );
	}

	/**
	 * 取证书公钥 -验签
	 *
	 * @return string
	 */
	function getPublicKey($cert_path) {
		return file_get_contents ( $cert_path );
	}
	/**
	 * 返回(签名)证书私钥 -
	 *
	 * @return unknown
	 */
	function getPrivateKey($cert_path) {
		$pkcs12 = file_get_contents ( $cert_path );
		openssl_pkcs12_read ( $pkcs12, $certs, UpacpayCommon::SDK_SIGN_CERT_PWD );
		return $certs ['pkey'];
	}

	/**
	 * 加密 卡号
	 *
	 * @param String $pan
	 *        	卡号
	 * @return String
	 */
	function encryptPan($pan) {
		$cert_path = MPI_ENCRYPT_CERT_PATH;
		$public_key = $this->getPublicKey ( $cert_path );
		
		openssl_public_encrypt ( $pan, $cryptPan, $public_key );
		return base64_encode ( $cryptPan );
	}
	/**
	 * pin 加密
	 *
	 * @param unknown_type $pan        	
	 * @param unknown_type $pwd        	
	 * @return Ambigous <number, string>
	 */
	function encryptPin($pan, $pwd) {
		$cert_path = UpacpayCommon::SDK_ENCRYPT_CERT_PATH;
		$public_key = $this->getPublicKey ( $cert_path );

		return EncryptedPin ( $pwd, $pan, $public_key );
	}
	/**
	 * cvn2 加密
	 *
	 * @param unknown_type $cvn2        	
	 * @return unknown
	 */
	function encryptCvn2($cvn2) {
		$cert_path = UpacpayCommon::SDK_ENCRYPT_CERT_PATH;
		$public_key = $this->getPublicKey ( $cert_path );
		
		openssl_public_encrypt ( $cvn2, $crypted, $public_key );
		
		return base64_encode ( $crypted );
	}
	/**
	 * 加密 有效期
	 *
	 * @param unknown_type $certDate        	
	 * @return unknown
	 */
	function encryptDate($certDate) {
		$cert_path = UpacpayCommon::SDK_ENCRYPT_CERT_PATH;
		$public_key = $this->getPublicKey ( $cert_path );
		
		openssl_public_encrypt ( $certDate, $crypted, $public_key );
		
		return base64_encode ( $crypted );
	}

	/**
	 * 加密 数据
	 *
	 * @param unknown_type $certDatatype
	 * @return unknown
	 */
	function encryptDateType($certDataType) {
		$cert_path = UpacpayCommon::SDK_ENCRYPT_CERT_PATH;
		$public_key = $this->getPublicKey ( $cert_path );

		openssl_public_encrypt ( $certDataType, $crypted, $public_key );

		return base64_encode ( $crypted );
	}






	/**
	 * ------------httpClient.php---------------------------------------------------------------------
	 *
	 */
	/**
	 * 后台交易 HttpClient通信
	 * @param unknown_type $params
	 * @param unknown_type $url
	 * @return mixed
	 */
	function sendHttpRequest($params, $url) {
		$opts = $this->getRequestParamString ( $params );
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);//不验证证书
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false);//不验证HOST
		curl_setopt ( $ch, CURLOPT_SSLVERSION, 3);
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
				'Content-type:application/x-www-form-urlencoded;charset=UTF-8' 
		) );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $opts );
		
		/**
		 * 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
		 */
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		
		// 运行cURL，请求网页
		$html = curl_exec ( $ch );
		// close cURL resource, and free up system resources
		curl_close ( $ch );
		return $html;
	}

	/**
	 * 组装报文
	 *
	 * @param unknown_type $params        	
	 * @return string
	 */
	function getRequestParamString($params) {
		$params_str = '';
		foreach ( $params as $key => $value ) {
			$params_str .= ($key . '=' . (!isset ( $value ) ? '' : urlencode( $value )) . '&');
		}
		return substr ( $params_str, 0, strlen ( $params_str ) - 1 );
	}






	/**
	 * ------------PinBlock.php---------------------------------------------------------------------
	 *
	 */
	 
	/**
	 * Author: gu_yongkang 
	 * data: 20110510
	 * 密码转PIN 
	 * Enter description here ...
	 * @param $spin
	 */
	function  Pin2PinBlock( &$sPin )
	{
	//	$sPin = "123456";
		$iTemp = 1;
		$sPinLen = strlen($sPin);
		$sBuf = array();
		//密码域大于10位
		$sBuf[0]=intval($sPinLen, 10);

		if($sPinLen % 2 ==0)
		{
			for ($i=0; $i<$sPinLen;)
			{
				$tBuf = substr($sPin, $i, 2);
				$sBuf[$iTemp] = intval($tBuf, 16);
				unset($tBuf);
				if ($i == ($sPinLen - 2))
				{
					if ($iTemp < 7)
					{
						$t = 0;
						for ($t=($iTemp+1); $t<8; $t++)
						{
							$sBuf[$t] = 0xff;
						}
					}
				}
				$iTemp++;
				$i = $i + 2;	//linshi
			}
		}
		else
		{
			for ($i=0; $i<$sPinLen;)
			{
				if ($i ==($sPinLen-1))
				{
					$mBuf = substr($sPin, $i, 1) . "f";
					$sBuf[$iTemp] = intval($mBuf, 16);
					unset($mBuf);
					if (($iTemp)<7)
					{
						$t = 0;
						for ($t=($iTemp+1); $t<8; $t++)
						{
							$sBuf[$t] = 0xff;
						}
					}
				}
				else 
				{
					$tBuf = substr($sPin, $i, 2);
					$sBuf[$iTemp] = intval($tBuf, 16);
					unset($tBuf);
				}
				$iTemp++;
				$i = $i + 2;
			}
		}
		return $sBuf;
	}
	/**
	 * Author: gu_yongkang
	 * data: 20110510
	 * Enter description here ...
	 * @param $sPan
	 */
	function FormatPan(&$sPan)
	{
		$iPanLen = strlen($sPan);
		$iTemp = $iPanLen - 13;
		$sBuf = array();
		$sBuf[0] = 0x00;
		$sBuf[1] = 0x00;
		for ($i=2; $i<8; $i++)
		{
			$tBuf = substr($sPan, $iTemp, 2);
			$sBuf[$i] = intval($tBuf, 16);
			$iTemp = $iTemp + 2;		
		}
		return $sBuf;
	}

	function Pin2PinBlockWithCardNO(&$sPin, &$sCardNO)
	{
		//global $log;
		$sPinBuf = $this->Pin2PinBlock($sPin);
		$iCardLen = strlen($sCardNO);
		//$log->LogInfo("CardNO length : " . $iCardLen);
		if ($iCardLen <= 10)
		{
			return (1);
		}
		elseif ($iCardLen==11){
			$sCardNO = "00" . $sCardNO;
		}
		elseif ($iCardLen==12){
			$sCardNO = "0" . $sCardNO;
		}
		$sPanBuf = $this->FormatPan($sCardNO);
		$sBuf = array();
		
		for ($i=0; $i<8; $i++)
		{
	//			$sBuf[$i] = $sPinBuf[$i] ^ $sPanBuf[$i];	//十进制
	//			$sBuf[$i] = vsprintf("%02X", ($sPinBuf[$i] ^ $sPanBuf[$i]));
			$sBuf[$i] = vsprintf("%c", ($sPinBuf[$i] ^ $sPanBuf[$i]));
		}
		unset($sPinBuf);
		unset($sPanBuf);
	//		return $sBuf;
		$sOutput = implode("", $sBuf);	//数组转换为字符串
		return $sOutput;
	}







	/**
	 * ------------PublicEncrypte.php---------------------------------------------------------------------
	 *
	 */
	function EncryptedPin($sPin, $sCardNo ,$sPubKeyURL)
	{
		//global $log;
		$sPubKeyURL = trim(UpacpayCommon::SDK_ENCRYPT_CERT_PATH," ");
	//	$log->LogInfo("DisSpaces : " . PubKeyURL);
		$fp = fopen($sPubKeyURL, "r");
		if ($fp != NULL)
		{
			$sCrt = fread($fp, 8192);
	//		$log->LogInfo("fread PubKeyURL : " . $sCrt);
			fclose($fp);
		}
		$sPubCrt = openssl_x509_read($sCrt);
		if ($sPubCrt === FALSE)
		{
			print("openssl_x509_read in false!");
			return (-1);
		}
	//	$log->LogInfo($sPubCrt);
	//	$sPubKeyId = openssl_x509_parse($sCrt);
	//	$log->LogInfo($sPubKeyId);
		$sPubKey = openssl_x509_parse($sPubCrt);
	//	$log->LogInfo($sPubKey);
	//	openssl_x509_free($sPubCrt);
	//	print_r(openssl_get_publickey($sCrt));

		$sInput = $this->Pin2PinBlockWithCardNO($sPin, $sCardNo);
		if ($sInput == 1)
		{
			print("Pin2PinBlockWithCardNO Error ! : " . $sInput);
			return (1);
		}
		$iRet = openssl_public_encrypt($sInput, $sOutData, $sCrt, OPENSSL_PKCS1_PADDING);
		if ($iRet === TRUE)
		{
	//		$log->LogInfo($sOutData);
			$sBase64EncodeOutData = base64_encode($sOutData);		
			//print("PayerPin : " . $sBase64EncodeOutData);
			return $sBase64EncodeOutData;
		}
		else 
		{
			print("openssl_public_encrypt Error !");
			return (-1);
		}
	}
}
?>