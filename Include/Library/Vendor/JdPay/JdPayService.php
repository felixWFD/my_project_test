<?php
/**
 * 京东快捷支付业务类
 * 版本：0.1
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付接口使用，只是提供一个参考。
 *
 * 提示：1.快捷支付代码流程一致，只是各种交易类型的参数略有不同，详细参数请参考文档
 *
 */
class JdPayService{
	//版本号
	protected $version  = '1.0.0';
	//终端号
	protected $terminal = '00000001';
	//商户号
	protected $merchant = '110231267002';
	//DES密钥
	protected $desKey   = 'a9OGkm0IMd/T1YBkMR+A+w2A3AEpuj4I';
	//md5密钥
	protected $md5Key   = 'luaomMtOarZdv6FaEypqBtUAQtbGswaT';
	
	/**
	 * 发起快捷支付方法
	 * @param $data_xml交易的xml格式数据
	 */
	public function trade($data_xml){
		//把data元素des加密
		$desObj = new \JdPayDes($this->desKey);
		$dataDES = $desObj->encrypt($data_xml);
		$base = new \JdPayDataBase();
		$sign = $base->myMd5($this->version.$this->merchant.$this->terminal.$dataDES,$this->md5Key);
		$xml = $base->createXml($this->version,$this->merchant,$this->terminal,$dataDES,$sign);
		//使用方法
		$param ='charset=UTF-8&req='.urlencode(base64_encode($xml));
		$resp = $this->post($param);
		return $resp;
	}
	
	/**
	 * @param $resp 网银在线返回的数据
	 * 数据的解析步骤：
	 * 1：截取resp=后面的xml数据
	 * 2: base64解码
	 * 3: 验证签名
	 * 4: 解析交易数据处理自己的业务逻辑
	 */
	public function operate($resp){
		$rd = array('status'=>0);
		$temResp = base64_decode(substr($resp,5));
		$xml = simplexml_load_string($temResp);
		//验证签名, version.merchant.terminal.data
		$text = $xml->VERSION.$xml->MERCHANT.$xml->TERMINAL.$xml->DATA;  //echo "service.php=======text=========".$text."<br>";
		$base = new \JdPayDataBase();
		if(!$base->md5Verify($text,$this->md5Key,$xml->SIGN)){    // 没通过验证
			return $rd;//表示没通过验证
		}
		// ---------------------通过验证的业务逻辑---------------------
		//des密钥要网银在线后台设置
		$desObj = new \JdPayDes($this->desKey);
		$decodedXML = $desObj->decrypt($xml->DATA);
		$dataXml    = simplexml_load_string($decodedXML);
		$data       = array_change_key_case(array_merge(CBWOjectArray($dataXml->TRADE), CBWOjectArray($dataXml->RETURN)),CASE_LOWER); // 对象转数组后,合并数组,然后大写键转小写
		
		if($data['code'] == '0000'){            // 交易返回码 成功[0000], 处理中[0001], 系统异常[EEE0001].......
			if($data['type'] == 'V'){           // 网银在线签约
				
			}elseif($data['type'] == 'Q'){      // 网银在线查询
				
			}elseif($data['type'] == 'S'){      // 网银在线消费
				
				$orders = D('Orders');
				
				$sn    = $data['id'];
				$order = $orders->getOrderBySn($sn);
				
				if($order['pay_status'] == 0){
					$rt = $orders->updateOrders($order['id'], array('pay_status'=>1, 'pay_date'=>time(), 'pay_mode'=>5));
					if($rt['status'] > 0){
						
						// 支付完成后,订单操作
						$orders->successPayOrderOperation($order['id']);
						
						$data['pay_link'] = $order['pay_link'];
					}
				}
			}elseif($data['type'] == 'R'){      // 网银在线退款
				
			}
		}
		return $data;
	}
	
	/**
	 * 发送请求至快捷支付地址
	 * 只支持post方式
	 * 测试时，请确认本地curl环境是否可用
	 * @param 请求参数
	 */
	public function post($param){
		$url = "https://quick.chinabank.com.cn/express.htm";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_PORT, 443);
		curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //信任任何证书
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // 检查证书中是否设置域名,0不验证
		//curl_setopt($ch, CURLOPT_VERBOSE, 1); //debug模式
		//curl_setopt($ch, CURLOPT_SSLCERT, dirname(__FILE__).'/quick.cer'); //client.crt文件路径
		//curl_setopt($ch, CURLOPT_SSLCERTPASSWD, ""); //client证书密码
		//curl_setopt($ch, CURLOPT_SSLKEY, "chinabank");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);// 显示输出结果
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		$file_contents = curl_exec($ch); // 执行操作
		curl_close($ch);
		return $file_contents;
	}
	
	/**
	 * 发送请求至快捷支付地址
	 * 只支持post方式
	 * 测试时，请确认本地curl环境是否可用
	 * @param 请求参数
	 * 此方法废弃
	 */
	public function post1($param){//curl
		$url = "https://quick.chinabank.com.cn/express.htm";
		//$url = "http://localhost:8080/web_motopay_express/express.htm";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		return $file_contents;
	}
}
?>
