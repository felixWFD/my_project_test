<?php
/**
 * 数据对象基础类，该类中定义数据类最基本的行为，包括：
 * MD5加密/验证签名/生成XML等
 * @author HJP
 *
 */
class JdPayDataBase{
	/**
	 * md5加密方法
	 */
    public function myMd5($text,$key){
		return md5($text.$key);
    }
	
    /**
     * 验证签名方法
     */
	public function md5Verify($text,$key,$md5){
		$md5Text = $this->myMd5($text,$key);
		return $md5Text==$md5;
	}
	
	/**
     * 生成XML
     */
	public function createXml($version,$merchant,$terminal,$data,$sign){
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><chinabank/>');
		$xml->addChild('version',$version);
		$xml->addChild('merchant',$merchant);
		$xml->addChild('terminal',$terminal);
		$xml->addChild('data',$data);
		$xml->addChild('sign',$sign);

		return $xml->asXML();
	}
	
	/**
     * 生成网银在线签约XML
     */
	public function signXml($card_bank,$card_type,$card_no,
								$card_exp,$card_cvv2,$card_name,
								$card_idtype,$card_idno,$card_phone,
								$trade_type,$trade_id,$trade_amount,$trade_currency){
		$data = '<?xml version="1.0" encoding="UTF-8"?>'.
					'<DATA>'.
						'<CARD>'.
							'<BANK>'.$card_bank.'</BANK>'.
							'<TYPE>'.$card_type.'</TYPE>'.
							'<NO>'.$card_no.'</NO>'.
							'<EXP>'.$card_exp.'</EXP>'.
							'<CVV2>'.$card_cvv2.'</CVV2>'.
							'<NAME>'.$card_name.'</NAME>'.
							'<IDTYPE>'.$card_idtype.'</IDTYPE>'.
							'<IDNO>'.$card_idno.'</IDNO>'.
							'<PHONE>'.$card_phone.'</PHONE>'.
						'</CARD>'.
						'<TRADE>'.
							'<TYPE>'.$trade_type.'</TYPE>'.
							'<ID>'.$trade_id.'</ID>'.
							'<AMOUNT>'.$trade_amount.'</AMOUNT>'.
							'<CURRENCY>'.$trade_currency.'</CURRENCY>'.
						'</TRADE>'.
					'</DATA>';
		return $data;
	}
	
	/**
     * 生成网银在线消费XML
     */
	public function payXml($card_bank,$card_type,$card_no,
								$card_exp,$card_cvv2,$card_name,
								$card_idtype,$card_idno,$card_phone,
								$trade_type,$trade_id,$trade_amount,$trade_currency,
								$trade_date,$trade_time,$trade_notice,$trade_note,$trade_code){
		$data = '<?xml version="1.0" encoding="UTF-8"?>'.
					'<DATA>'.
						'<CARD>'.
							'<BANK>'.$card_bank.'</BANK>'.
							'<TYPE>'.$card_type.'</TYPE>'.
							'<NO>'.$card_no.'</NO>'.
							'<EXP>'.$card_exp.'</EXP>'.
							'<CVV2>'.$card_cvv2.'</CVV2>'.
							'<NAME>'.$card_name.'</NAME>'.
							'<IDTYPE>'.$card_idtype.'</IDTYPE>'.
							'<IDNO>'.$card_idno.'</IDNO>'.
							'<PHONE>'.$card_phone.'</PHONE>'.
						'</CARD>'.
						'<TRADE>'.
							'<TYPE>'.$trade_type.'</TYPE>'.
							'<ID>'.$trade_id.'</ID>'.
							'<AMOUNT>'.$trade_amount.'</AMOUNT>'.
							'<CURRENCY>'.$trade_currency.'</CURRENCY>'.
							'<DATE>'.$trade_date.'</DATE>'.
							'<TIME>'.$trade_time.'</TIME>'.
							'<NOTICE>'.$trade_notice.'</NOTICE>'.
							'<NOTE>'.$trade_note.'</NOTE>'.
							'<CODE>'.$trade_code.'</CODE>'.
						'</TRADE>'.
					'</DATA>';
		return $data;
	}
	
	/**
     * 生成网银在线退款XML
     */
	public function refundXml($trade_type,$trade_id,$trade_oid,
								$trade_amount,$trade_currency,$trade_date,
								$trade_time,$trade_notice,$trade_note){
		$data = '<?xml version="1.0" encoding="UTF-8"?>'.
			'<DATA>'.
				'<TRADE>'.
					'<TYPE>'.$trade_type.'</TYPE>'.
					'<ID>'.$trade_id.'</ID>'.
					'<OID>'.$trade_oid.'</OID>'.
					'<AMOUNT>'.$trade_amount.'</AMOUNT>'.
					'<CURRENCY>'.$trade_currency.'</CURRENCY>'.
					'<DATE>'.$trade_date.'</DATE>'.
					'<TIME>'.$trade_time.'</TIME>'.
					'<NOTICE>'.$trade_notice.'</NOTICE>'.
					'<NOTE>'.$trade_note.'</NOTE>'.
				'</TRADE>'.
			'</DATA>';
		return $data;
	}
	
	/**
     * 生成网银在线查询XML
     */
	public function queryXml($trade_type,$trade_id){
		$data = '<?xml version="1.0" encoding="UTF-8"?>'.
			'<DATA>'.
				'<TRADE>'.
					'<TYPE>'.$trade_type.'</TYPE>'.
					'<ID>'.$trade_id.'</ID>'.
				'</TRADE>'.
			'</DATA>';
		return $data;
	}
}
?>
